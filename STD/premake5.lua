configurations
{
  "Debug",
  "Release",
  "Dist"
}

-- Set variable inside /etc/environment
VULKAN_SDK_PATH = "/home/bies/BigData/SDKs/Vulkan/1.3.283.0/x86_64/" --os.getenv("VULKAN_SDK")
print("Vulkan SDK Path: " .. VULKAN_SDK_PATH)
assert(VULKAN_SDK_PATH ~= nil, "Can't get Vulkan SDK!")


outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

group "Dependencies"
 include "../Dependencies/MiniAudio"
 include "Dependencies/stb"
 include "../Dependencies/Chipmunk2D"

project "STD"
    cdialect "C99"
    kind "StaticLib"
    language "C"
    staticruntime "on"
    targetdir ("bin/" .. outputdir .. "/%{prj.name}")
    objdir ("bin/Intermidiates/" .. outputdir .. "/%{prj.name}")
    architecture "x64"

    files
    {
      "src/**.h",
      "src/**.c",
    }

    defines
    {
       "_GNU_SOURCE"
    }

    includedirs
    {
      "src",
      "../Dependencies/MiniAudio/src/",
      "../Dependencies/stb/src/",
      "../Dependencies/Chipmunk2D/src/",
      VULKAN_SDK_PATH .. "/include/vulkan",
      VULKAN_SDK_PATH .. "/include/"
    }

    filter "system:linux"
      defines {
	 "PLATFORM_LINUX",
	 "SWL_X11_BACKEND",
	 "SVL_X11",
      }

    filter "system:windows"
      defines {
	  "PLATFORM_WINDOWS",
	  "_CRT_SECURE_NO_WARNINGS",
	  "SVL_WIN64",
      }
      links{"opengl32"}

    filter "configurations:Debug"
      defines { "ENGINE_DEBUG" }
      symbols "On"

    filter "configurations:Release"
      defines { "ENGINE_RELEASE" }
      optimize "Speed"
