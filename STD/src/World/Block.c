#include "Block.h"

#include "EntitySystem/StdVisibleEntity.h"


Block
block_new(StdVisibleEntity* aVisibleEntities)
{
    Block block = {.aVisibleEntities = aVisibleEntities};
    return block;
}
