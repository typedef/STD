#ifndef BLOCK_H
#define BLOCK_H

struct StdVisibleEntity;

typedef struct Block
{
    struct StdVisibleEntity* aVisibleEntities;
} Block;

Block block_new(struct StdVisibleEntity* aVisibleEntities);

#endif // BLOCK_H
