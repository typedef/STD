#ifndef SOUND_LAYER_H
#define SOUND_LAYER_H


struct ma_engine;


void sound_layer_on_attach();
void sound_layer_on_destroy();
struct ma_engine* sound_layer_get_engine();

#endif // SOUND_LAYER_H
