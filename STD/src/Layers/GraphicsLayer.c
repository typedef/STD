#include "GraphicsLayer.h"

#include <Core/SimpleApplication.h>
#include <Core/SimpleStandardLibrary.h>
#include <Core/SimpleWindowLibrary.h>
#include <Core/SimpleAtlasFont.h>
#include <Core/SimpleMath.h>
#include <Std/StdAssetManager.h>
#include <Std/StdInput.h>
#include <EntitySystem/StdScene.h>
#include <Deps/miniaudio.h>

#include "Std/StdRenderer.h"


typedef struct GraphicsInternal
{
    SafFont Font;
    u32 AtlasTextureId;
} GraphicsInternal;

static GraphicsInternal gInternal = {};


void
graphics_layer_attach()
{
    SwlWindow* pWindow = simple_application_get_window();

    StdCameraEntity camera;
    camera = std_camera_entity_new((StdCameraEntitySettings) {
	    .WindowSize = { pWindow->Size.Width, pWindow->Size.Height },
	    .IsMain = 1
	});

    std_scene_create((StdSceneSettings) { .MainCamera = camera });

    slog_error("ws: %v2\n", pWindow->Size);

    StdRendererSettings set = {
	.WindowSize = (v2i) { .Width = pWindow->Size.Width, .Height = pWindow->Size.Height },
	.pWindowBackend = pWindow->pBackend,
	.MaxItemsCount = 1000,
	.MaxCharsCount = 10000,
	.MaxImageCount = 100,
	.IsDebug = 1,
	.IsVsync = 1,
    };
    GINFO("Create renderer\n");
    std_renderer_create(&set);

    StdCameraEntity* pMainCamera = std_scene_get_main_camera();
    std_renderer_set_camera(&pMainCamera->ViewProjection);

    // DOCS: Set ui camera
    StdCameraEntitySettings caset = {
	.IsMain = 0,
	.WindowSize = {pWindow->Size.Width, pWindow->Size.Height},
	.Position = {}
    };
    StdCameraEntity uiCamera = std_camera_entity_new(caset);
    std_renderer_set_ui_camera(&uiCamera.ViewProjection);

    std_input_init();
}

void
graphics_layer_after_update()
{
    std_renderer_update();
}

void
graphics_layer_event(SwlEvent* pEvent)
{
    SwlEventType type = pEvent->Type;
    SwlWindow* pWindow = simple_application_get_window();

    switch (type)
    {
    case SwlEventType_WindowResized:
    {
	GINFO("GraphicsLayer: WindowResize\n");
	std_camera_entity_resize(std_scene_get_main_camera(),
				 v2_new(pWindow->Size.Width, pWindow->Size.Height));
	break;
    }

    default:
	break;

    }

}

void
graphics_layer_destroy()
{
    std_input_deinit();
    std_scene_destroy();
}
