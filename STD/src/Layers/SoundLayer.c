#include "SoundLayer.h"

#include <Deps/miniaudio.h>
#include <Core/SimpleStandardLibrary.h>


static ma_engine* pEngine = NULL;
static ma_engine_config gConfig = {};


void
sound_layer_on_attach()
{
    pEngine = memory_allocate(sizeof(*pEngine));
    gConfig = ma_engine_config_init();

    ma_result result = ma_engine_init(&gConfig, pEngine);
    if (result != MA_SUCCESS)
    {
	GERROR("Failed to initialize engine!\n");
	vassert_break();
    }
}

void
sound_layer_on_destroy()
{
    ma_engine_uninit(pEngine);
    memory_free(pEngine);
}

struct ma_engine*
sound_layer_get_engine()
{
    return pEngine;
}
