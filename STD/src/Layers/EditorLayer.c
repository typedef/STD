#include "EditorLayer.h"
#include <Core/SimpleApplication.h>
#include <Core/SimpleWindowLibrary.h>
#include <Core/SimpleMath.h>
#include <EntitySystem/StdScene.h>
#include <Std/StdUi.h>
#include <Std/StdUiCustom.h>
#include <Std/StdRenderer.h>


typedef struct StdEditor
{
    i64 SelectedIndex;
} StdEditor;

static StdEditor gEditor = {};


// DOCS: Forward-declared
void _editor_selection_logic();


void
std_editor_attach()
{
    gEditor.SelectedIndex = -1;

    { // docs: init custom ui
	std_ui_custom_init();
    }

}

void
std_editor_destroy()
{
}

void
std_editor_update()
{
}

void
std_editor_ui()
{
    _editor_selection_logic();
}

void
std_editor_event(SwlEvent* pEvent)
{

}

void
editor_layer_register()
{
    SimpleLayer layer = {
	.OnAttach = std_editor_attach,
	.OnDestroy = std_editor_destroy,
	.OnUpdate = std_editor_update,
	.OnUi = std_editor_ui,
	.OnEvent = std_editor_event,
    };

    simple_application_add_layer(layer);
}



void
_editor_selection_logic()
{
    StdVisibleEntity* aVisibleEntities = std_scene_get_visible_entities();

    i32 selectedIndex = 0;
    StdVisibleEntity* pSelectedEntity = &aVisibleEntities[selectedIndex];

    v2 newPos = pSelectedEntity->Position.XY;
    StdUiGizmoSettings gizmoSettings = {
	.pLabel = "Гизмо",
	.Flags = StdUiGizmoFlags_Position
    };

    StdUiState state = std_ui_gizmo(gizmoSettings, &newPos);
    if (state == StdUiState_Grabbed)
    {
	v3 pos = { newPos.X, newPos.Y, pSelectedEntity->Position.Z - 0.3f };
	std_renderer_draw_color_image(pos, pSelectedEntity->Size, v4_new(1, 1, 1, 0.5), pSelectedEntity->AssetId);
    }
    else if (state == StdUiState_Changed)
    {
	pSelectedEntity->Position.XY = newPos;
    }

}
