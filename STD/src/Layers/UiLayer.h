#ifndef UI_LAYER_H
#define UI_LAYER_H

struct SwlEvent;

void ui_layer_attach();
void ui_layer_before_update();
void ui_layer_update();
void ui_layer_after_update();
void ui_layer_event(struct SwlEvent* pEvent);
void ui_layer_destroy();



#endif // UI_LAYER_H
