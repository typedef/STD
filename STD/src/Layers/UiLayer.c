#include "UiLayer.h"
#include <Core/SimpleAtlasFont.h>
#include <Std/StdAssetManager.h>
#include <Std/StdRenderer.h>
#include <Core/SimpleMath.h>
#include <Core/SimpleApplication.h>
#include <Core/SimpleWindowLibrary.h>
#include <Core/SimpleStandardLibrary.h>
#include <EntitySystem/StdCameraEntity.h>
#include <EntitySystem/StdScene.h>
#include <Std/StdUi.h>


/* void std_renderer_set_ui_camera(m4* pViewProjection); */
/* void std_renderer_set_ui_viewport(StdViewport viewport); */
/* void std_renderer_set_ui_fonts(); */
/* void std_renderer_draw_ui_rect(v3 position, v2 size, v4 color); */
/* void std_renderer_draw_ui_image(v3 position, v2 size, i64 assetId); */
/* void std_renderer_draw_ui_text(v3 position, v4 color, char* pText); */
/* // todo: */
/* void std_renderer_draw_ui_image_ext(v3 position, v2 size, i64 assetId, v2 aUvs[4], v4 color); */

void
ui_back_draw_rect(v3 position, v2 size, v4 color)
{
    position.Y -= size.Y;
    std_renderer_draw_ui_rect(position, size, color);
}

void
ui_back_draw_empty_rect(v3 position, v2 size, real thickness, v4 color)
{
    position.Y -= size.Y;
    std_renderer_draw_ui_empty_rect(position, size, thickness, color);
}

void
ui_back_draw_line(v3 start, v3 end, real thickness, v4 color)
{
    std_renderer_draw_ui_line(start, end, thickness, color);
}

void
ui_back_draw_image(v3 position, v2 size, v4 color, i64 assetId)
{
    position.Y -= size.Y;
    v2 aUvs[4] = { v2_new(0,0), v2_new(0,1), v2_new(1,1), v2_new(1,0) };
    std_renderer_draw_ui_image_ext(position, size, assetId, aUvs, color);
}

void
ui_back_draw_atlas_item(v3 position, v2 size, v4 color, i64 assetId, i32 index)
{
    StdAtlas atlas = std_asset_manager_get_atlas(assetId);
    position.Y -= size.Y;
    StdAtlasItem item = atlas.aAtlasItems[index];
    v2 aUvs[4] = {
	v2_new(item.UvMin.X, item.UvMin.Y),
	v2_new(item.UvMin.X, item.UvMax.Y),
	v2_new(item.UvMax.X, item.UvMax.Y),
	v2_new(item.UvMax.X, item.UvMin.Y),
    };
    std_renderer_draw_ui_image_ext(position, size, atlas.TextureId, aUvs, color);
}

void
ui_back_set_viewport(i32 x0, i32 y0, i32 width, i32 height)
{
    StdViewport v = {
	.X0 = x0,
	.Y0 = y0,
	.Width = width,
	.Height = height,
    };
    std_renderer_set_ui_viewport(v);
}

void
ui_back_draw_text(char* pText, i32 fontSize, v3 position, v4 color)
{
    std_renderer_draw_ui_text_w_fontsize(position, color, pText, fontSize);
}

v2
ui_back_get_text_metrics(char* pText, i32 fontSize, char* pFontPath)
{
    // note/todo: we could work with fontId
    StdFont* aFonts = std_asset_manager_get_all_fonts();
    i64 ind = sva_index_of(aFonts, item.Size == fontSize &&
			   (pFontPath == NULL || string_compare(item.pPath, pFontPath)));
    if (ind == -1)
    {
	return (v2) {};
    }

    StdFont font = aFonts[ind];
    v2 metrics = saf_font_get_metrics_generic(aFonts, pText, fontSize);
    return metrics;
}

void
ui_layer_attach()
{
    SwlWindow* pWindow = simple_application_get_window();

    StdUiSettings uiSet = { .DisplaySize = v2_new(pWindow->Size.Width, pWindow->Size.Height) };

    std_ui_create(uiSet);

    // todo: place backend function into special folder (good idea)
    // todo: add set_viewport function
    StdUiBackendSettings stdUiBackSet = {
	.Funcs = {
	    .DrawRect = ui_back_draw_rect,
	    .DrawEmptyRect = ui_back_draw_empty_rect,
	    .DrawLine = ui_back_draw_line,
	    .DrawText = ui_back_draw_text,
	    .DrawImage = ui_back_draw_image,
	    .DrawImageAtlasItem = ui_back_draw_atlas_item,

	    .SetViewport = ui_back_set_viewport,

	    .GetTextMetrics = ui_back_get_text_metrics,

	    .PlayHoverSound = NULL, //ui_back_play_hover_sound,
	    .PlayClickSound = NULL, //ui_back_play_click_sound
	},

	.DebugConfig = {
	    .IsButtonDrawn = 1,
	    .IsTextDrawn = 1,
	    .IsCheckboxDrawn = 1,
	    .IsSliderDrawn = 1,
	    .IsImageDrawn = 1,
	    .IsFontDebug = 1,

	    .IsDivDebugMode = 0,
	}

    };

    std_ui_backend_new(stdUiBackSet);

}


void
ui_layer_before_update()
{
}

void
ui_layer_update()
{
    std_ui_frame_begin();

    LayerAction* aUis = simple_application_get_uis();
    i32 uiCount = sva_count(aUis); //slog_success("uiCount: %d\n", uiCount);
    for (i32 i = 0; i < uiCount; ++i)
    {
	LayerAction onUi = aUis[i];
	onUi();
    }
    std_ui_process_input();

#if defined(BUILD_DEBUG)
    // note: comment for now
    // std_ui_debug_validation();
#endif // BUILD_DEBUG

    std_ui_backend_draw();
    std_ui_backend_sound();

    std_ui_frame_end();
}

void
ui_layer_after_update()
{
}

force_inline StdUiKey
convert_key(SwlKey key)
{
    StdUiKey ckey;

    switch (key)
    {

    case SwlKey_Tab      : return StdUiKey_Tab      ;
    case SwlKey_Backspace: return StdUiKey_Backspace;

    case SwlKey_Left     : return StdUiKey_LeftArrow;
    case SwlKey_Right    : return StdUiKey_RightArrow;
    case SwlKey_Up       : return StdUiKey_UpArrow;
    case SwlKey_Down     : return StdUiKey_DownArrow;

    case SwlKey_Home     : return StdUiKey_Home;
    case SwlKey_End      : return StdUiKey_End;
    case SwlKey_Delete   : return StdUiKey_Delete;

    default: return StdUiKey_Undefined;

    }
}

force_inline StdUiKey
convert_mouse_key(SwlMouseKey key)
{
    StdUiKey ckey;

    switch (key)
    {

    case SwlMouseKey_Left : return StdUiKey_Mouse_Left_Button ;
    case SwlMouseKey_Right: return StdUiKey_Mouse_Right_Button;
    default: return StdUiKey_Undefined;

    }
}


void
ui_layer_event(struct SwlEvent* pEvent)
{
    StdUiInput* pInput = std_ui_get_input();
    SwlEventType type = pEvent->Type;
    SwlWindow* pWindow = simple_application_get_window();

    switch (type)
    {

    case SwlEventType_KeyPress:
    {
	SwlKeyPressEvent* pKey = (SwlKeyPressEvent*) pEvent;
	StdUiKey stdKey = convert_key(pKey->Key);
	pInput->aKeys[stdKey] = (StdUiKeyData) { .State = StdUiKeyState_Pressed };

	break;
    }

    case SwlEventType_KeyRelease:
    {
	SwlKeyReleaseEvent* pKey = (SwlKeyReleaseEvent*) pEvent;
	StdUiKey stdKey = convert_key(pKey->Key);
	pInput->aKeys[stdKey] = (StdUiKeyData) { .State = StdUiKeyState_Released };

	break;
    }

    case SwlEventType_MousePress:
    {
	SwlMousePressEvent* pMouse = (SwlMousePressEvent*) pEvent;
	StdUiKey stdKey = convert_mouse_key(pMouse->Key);
	pInput->aKeys[stdKey] = (StdUiKeyData) { .State = StdUiKeyState_Pressed };
	break;
    }

    case SwlEventType_MouseRelease:
    {
	SwlMouseReleaseEvent* pMouse = (SwlMouseReleaseEvent*) pEvent;
	StdUiKey stdKey = convert_mouse_key(pMouse->Key);
	pInput->aKeys[stdKey] = (StdUiKeyData) { .State = StdUiKeyState_Released };
	break;
    }

    case SwlEventType_MouseMove:
    {
	SwlMouseMoveEvent* pMove = (SwlMouseMoveEvent*) pEvent;
	pInput->MousePosition = v2_new(pMove->X,  pWindow->Size.Height - pMove->Y);
	break;
    }

    case SwlEventType_WindowResized:
    {
	GINFO("GraphicsLayer: WindowResize\n");

	StdUiDisplay* pDisplay = std_ui_get_display();
	SwlWindowResizeEvent* pWindowResized = (SwlWindowResizeEvent*)pEvent;
	pDisplay->Size = v2_new(pWindowResized->Width, pWindowResized->Height);
	std_camera_entity_resize(std_scene_get_main_camera(), pDisplay->Size);
	break;
    }

    default:
	break;

    }

}

void
ui_layer_destroy()
{
    std_ui_destroy();
}








#if 0
void
ui_play_sound(StdSoundId* pSoundId, const char* pSoundPath)
{
    StdSoundId soundId = *pSoundId;

    if (soundId == -1)
    {
	soundId = std_asset_manager_load_sound_by_id(pSoundPath);
    }

    ma_sound* pSound = NULL;
    if (soundId != -1)
    {
	pSound = std_asset_manager_get_sound(soundId);
	ma_sound_start(pSound);
    }
    else
    {
	GWARNING("Can't found hover-sound %s %d\n", pSoundPath, soundId);
    }

    *pSoundId = soundId;
}

void
ui_back_play_hover_sound()
{
    static StdSoundId hoverId = -1;
    ui_play_sound(&hoverId, "Assets/Interface/HoverSound.mp3");
}

void
ui_back_play_click_sound()
{
    static StdSoundId clickId = -1;
    ui_play_sound(&clickId, "Assets/Interface/ClickSound.mp3");
}
#endif


void
ui_layer_register()
{

}
