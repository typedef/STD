#ifndef GRAPHICS_LAYER_H
#define GRAPHICS_LAYER_H

struct SwlEvent;

void graphics_layer_attach();
void graphics_layer_after_update();
void graphics_layer_event(struct SwlEvent* pEvent);
void graphics_layer_destroy();

#endif // GRAPHICS_LAYER_H
