#ifndef SIMPLE_OBB_HELPER_H
#define SIMPLE_OBB_HELPER_H

#include <Core/Types.h>

void simple_obb_helper_ss_to_ray(v2 mouse, v2 screen, m4 view, m4 proj, v3* pOrigin, v3* pDirection);
i32 simple_obb_helper_is_collide(v3 rayOrigin, v3 rayDirection, v3 aabbMin, v3 aabbMax, m4 pModelMatrix, f32* intersectionDistance);

#endif // SIMPLE_OBB_HELPER_H
