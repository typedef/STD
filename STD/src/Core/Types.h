#ifndef TYPES_H
#define TYPES_H

/*
  ############################################
  DOCS: Declare our own types as preprocessors
  ############################################
*/

typedef char i8;
typedef short i16;
typedef int i32;
typedef long long int i64;
typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned long long u64;
typedef float f32;
typedef double f64;


// todo: validate sizeof(i8) == 1, etc
/*
  ############################################
  DOCS: Convert values
  ############################################
*/

#define to_i8(x)  ((i8)(x))
#define to_i16(x) ((i16)(x))
#define to_i32(x) ((i32)(x))
#define to_i64(x) ((i64)(x))
#define to_u8(x)  ((u8)(x))
#define to_u16(x) ((u16)(x))
#define to_u32(x) ((u32)(x))
#define to_u64(x) ((u64)(x))
#define to_f32(x) ((f32)(x))
#define to_f64(x) ((f64)(x))

#define I8_MAX 127
#define I16_MAX 32767
#define I32_MAX 2147483647
#define I64_MAX (9223372036854775807LL) /*9 223 372 036 854 775 807=9e+18*/
#define U8_MAX  255
#define U16_MAX 65535
#define U32_MAX 4294967295
#define U64_MAX (18446744073709551615ULL) /*18 446 744 073 709 551 615=1.8*e+19*/
#define F32_MAX (340282346638528859811704183484516925440.000000)
#define F64_MAX (340282346638528859811704183484516925440.000000)

#define I32_MAX_HALF 1123241323

/*
  ############################################
  DOCS: Platform definition
  ############################################
*/

#define DEBUG_WO_FORCE_INLINE 0
#if DEBUG_WO_FORCE_INLINE == 1
   #warning "Slow mode enabled!\n #define DEBUG_WO_FORCE_INLINE 1"
#endif

#if defined(PLATFORM_WINDOWS)
#define force_inline static
#elif defined(PLATFORM_LINUX)
#define force_inline static inline __attribute((always_inline))
#else
#error "Platform not supported"
#endif

/*
  ############################################
  DOCS: Float precision, by default 32 bit
  #define FLOAT_64_BIT for better accuracy
  ############################################
*/

// todo: make 64 bit work
//#define FLOAT_64_BIT

#if !defined(FLOAT_64_BIT)
#define FLOAT_32_BIT
#endif

/*
  ############################################
  DOCS: Build configuration
  ############################################
*/

// todo: use BUILD_DEBUG instead of ENGINE_DEBUG in lib code
#if defined(ENGINE_DEBUG)
#define BUILD_DEBUG
#elif defined(ENGINE_RELEASE)
#define BUILD_RELEASE
#endif

/*
  ############################################
  DOCS: System byte order
  ############################################
*/

#if !defined(SYSTEM_BIG) && !defined(SYSTEM_LIT)
 /*🐨
   1 in endian system:
	 16                0
   BigEndian   : 1000 0000 0000 0000
	 0                16
   LittleEndian: 0000 0000 0000 0001

   general order: 123 => little: 0000 0123
	     big   : 0123 0000
 */

 #define SYSTEM_LIT (*((u8*) &(u32){1}))
 #define SYSTEM_BIG (!(SYSTEM_LIT))

#endif




/*
  #############################################
  DOCS: Helper methods abs, elvis, swap, minmax
  #############################################
*/

// DOCS: True on first run
#define helper_tofr()				\
    ({						\
	static i32 isFlag = 1;			\
	i32 returnValue = isFlag;		\
	if (isFlag)				\
	    isFlag = 0;				\
	returnValue;				\
    })
#define helper_fofr()				\
    ({						\
	static i32 isFlag = 0;			\
	i32 returnValue = isFlag;		\
	if (!isFlag)				\
	    isFlag = 1;				\
	returnValue;				\
    })

#define i32_abs(x) ({ i32 temp = (x); temp < 0 ? -temp : temp; })

force_inline f32
f32_abs(f32 value)
{
    union {f32 a; u32 b; } r;
    r.a = value;
    r.b &= 0x7fffffff;
    return r.a;
}

#define IfFalseThen(x, t)                               \
    ({ __typeof__((x)) xv = (x); (xv) ? xv : (t); })
#define IfNullThen(x, t)                                        \
    ({ __typeof__((x)) xv = (x); (xv != NULL) ? xv : (t); })
#define IsSingEqual(x, y)			\
    ({	((x * y) >= 0) ? 1 : 0;	})

#define Swap(x, y)                                      \
    ({ __typeof__(x) temp = x; x = y; y = temp; })
#define Max(x,y)                                                        \
    ({ __typeof__(x) l = x; __typeof__(y) r = y; (l > r) ? l : r; })
#define Min(x,y)                                                        \
    ({ __typeof__(x) l = x; __typeof__(y) r = y; (l > r) ? r : l; })
#define MinMax(x, min, max)                                             \
    ({ __typeof__(x) l = x; __typeof__(min) mn = min; __typeof__(max) mx = max; ((l > mx) ? mx : (l < mn ? mn : l)); })
#define MinMaxV2(x, v2v) MinMax(x, v2v.Min, v2v.Max)

#define Align(byte) __attribute__((aligned(byte)))
#define KB(x) ((i64)1024 * (i64)x)
#define MB(x) ((i64)1024 * KB(x))
#define GB(x) ((i64)1024 * MB(x))
#define ABS(x)					\
    ({ ((x) > 0) ? (x) : -(x); })
#define ToKB(x) (((f64) x) / 1024)
#define ToMB(x) (((f64) TOKB(x)) / 1024)
#define ToGB(x) (((f64) TOMB(x)) / 1024)
#define ToString(x) #x
#define OffsetOf(Type, Field) ( (u64) (&(((Type*)0)->Field)) )
#define AlignOf(Type) ((u64)&(((struct{char c; Type i;}*)0)->i))
#define ArrayCount(x) (sizeof(x) / sizeof(x[0]))

#define BitAnd(flag, bit) (((flag) & (bit)))
#define BitNotAnd(flag, bit) (BitAnd(flag, bit) == 0)

/*
  ############################################
  DOCS: Structure types
  ############################################
*/

typedef struct SimpleString
{
    i32 Length;
    i32 Size;
    char* pData;
} SimpleString;

typedef enum ResultType
{
    ResultType_Error = 0,
    ResultType_Success = 1,
    ResultType_Warning = 2
} ResultType;

typedef struct SimpleColor //((r << 24) | (g << 16) | (b << 8) | (a))
{
    u8 R;
    u8 G;
    u8 B;
    u8 A;
} SimpleColor;

#define SimpleColor_R(r) ((u32) (((SimpleColor*) (r))->R))
#define SimpleColor_G(g) ((u32) (((SimpleColor*) (g))->G))
#define SimpleColor_B(b) ((u32) (((SimpleColor*) (b))->B))
#define SimpleColor_A(a) ((u32) (((SimpleColor*) (a))->A))
#define SimpleColor_RGBA(r, g, b, a) ((a << 24) | (b << 16) | (g << 8) | (r))

#if defined(FLOAT_32_BIT)
typedef f32 real;
#else
typedef f64 real;
#endif

typedef real v3a[3];
typedef real v4a[4];
typedef v3a m3a[3];
typedef v4a m4a[4];

typedef struct v2
{
    union
    {
    struct { real X; real Y; };
    struct { real Width; real Height; };
    struct { real Min; real Max; };
    real V[2];
    };
} v2;

typedef struct v2i
{
    union
    {
    struct { i32 X; i32 Y; };
    struct { i32 Width; i32 Height; };
    struct { i32 Min; i32 Max; };
    i32 V[2];
    };
} v2i;

typedef struct v3
{
    union
    {
    struct { real X; real Y; real Z; };
    struct { real R; real G; real B; };
    struct { real Hue; real Saturation; real Value; };
    struct { real Pitch; real Yaw; real Roll; };
    struct { v2 XY; real T3; };
    struct { real T0; v2 YZ; };
    real V[3];
    };
} v3;

typedef struct v3i
{
    union
    {
    struct { i32 X; i32 Y; i32 Z; };
    i32 V[3];
    };
} v3i;

typedef struct v4
{
    union
    {
    struct { real X; real Y; real Z; real W; };
    struct { real R; real G; real B; real A; };
    real V[4];
    };
} v4;

typedef struct v4i
{
    union
    {
    struct { i32 X; i32 Y; i32 Z; i32 W; };
    struct { i32 R; i32 G; i32 B; i32 A; };
    i32 V[4];
    };
} v4i;

typedef struct m3
{
    union
    {
    struct
    {
	real M00; real M01; real M02;
	real M10; real M11; real M12;
	real M20; real M21; real M22;
    };
    struct
    {
	real R0[3];
	real R1[3];
	real R2[3];
    };
    m3a M;
    };
} m3;

typedef struct m4
{
    union
    {
    struct
    {
	real M00; real M01; real M02; real M03;
	real M10; real M11; real M12; real M13;
	real M20; real M21; real M22; real M23;
	real M30; real M31; real M32; real M33;
    };
    struct
    {
	real R0[4];
	real R1[4];
	real R2[4];
	real R3[4];
    };
    v4 V[4];
    m4a M;
    };
} m4;

typedef struct quat
{
    union
    {
    struct { real X; real Y; real Z; real W; };
    v4 V4;
    real V[4];
    };
} quat;

typedef struct Aabb
{
    v3 Min;
    v3 Max;
} Aabb;

typedef struct SimpleArena
{
    i64 Id;
    u64 Offset;
    u64 Size;
    i32 Line;
    const char* File;
    void* Data;
} SimpleArena;

typedef struct IString
{
    i32 Length;
    char* Buffer;
} IString;

typedef struct IStringPool
{
    IString** Strings;
} IStringPool;

typedef struct SimpleTimer
{
    f32 _CountSeconds;
    f32 WaitSeconds;
} SimpleTimer;


/*
  ############################################
  DOCS: Conditional break, assert, guard
  ############################################
*/

void _vbreak(const char* msg, const char* file, int line, const char* condition);

#if defined(BUILD_DEBUG)
#define vassert(a)							\
    ({									\
    if (!(a))							\
    {								\
	_vbreak("[ASSERT] %s:%d condition: %s is false!\n", __FILE__, __LINE__, #a); \
    }								\
    })

#define vassert_not_null(ptr) vassert(ptr != NULL)
#define vassert_null(ptr) vassert((ptr == NULL))
#define vassert_break() vassert(0 && "We shouldn't get here!!!")
#else // BUILD_DEBUG
#define vassert(a) ((void)0)
#define vassert_not_null(ptr) ((void)0)
#define vassert_null(ptr) ((void)0)
#define vassert_break() ((void)0)
#define vassert_null_offset(ptr) ((void)0)
#endif // BUILD_RELEASE

/*
  DOCS(typedef): Always exist, independent of the build config
*/
#define vguard(ptr)							\
    ({									\
	if (!(ptr))							\
	{								\
	    _vbreak("[GUARD] %s:%d condition: %s is false!\n", __FILE__, __LINE__, #ptr); \
	}								\
    })
#define vguard_not_null(ptr)			\
    ({						\
	vguard(ptr != NULL && #ptr);		\
    })
#define vguard_null(ptr)			\
    ({						\
	vguard(ptr == NULL);			\
    })
#define vguard_legacy(func)				\
    ({							\
	vguard(0 && #func " - legacy function!");	\
    })
#define vguard_not_impl()			\
    ({						\
	vguard(0 && "not implementsd!");	\
    })

#define vguard_call_once()				\
    if (helper_fofr())					\
	vguard(0 && "Called more then one time!");	\

/*
  #############################################
  DOCS: Asset/Resource path
  #############################################
*/

// note: not sure we will need this
#define BASE_ASSET_PATH "assets/"
#define asset(path) BASE_ASSET_PATH path
#define asset_shader(shader) asset("shaders/") shader
#define asset_texture(texture) asset("textures/") texture
#define asset_cubemap(cubemap) asset("cubemaps/") cubemap
#define asset_font(font) asset("fonts/") font
#define asset_model(model) asset("models/") model
#define asset_sound(sound) asset("sounds/") sound
#define asset_scene(scene) asset("scenes/") scene
#define asset_db(db) asset("databases/") db
#define asset_localization(local) asset("localization/") local

#define BASE_RESOURCE_PATH "Resources/"
#define resource(path) BASE_RESOURCE_PATH path
#define resource_shader(path) resource("Shaders/") path
#define resource_font(path) resource("Fonts/") path
#define resource_texture(path) resource("Textures/") path
#define resource_model(path) resource("Models/") path


/*
  #############################################
  DOCS: Dont now what to do with this
  #############################################
*/

#define void_to_i32(ptr) ((ptr) ? (*((i32*)ptr)) : ({assert(0 && "Wrong argument void_to_i32!"); 0; }))
#define void_to_f32(ptr) (*((f32*)ptr))
#define void_to_string(ptr) ((const char*)ptr)
#define i32_to_void_ptr(i32v) ((void*)(&(i32v)))
#ifndef NULL
#define NULL ((void*)0)
#endif
#define EMPTY(type) (type){0}
#define I32T(t, h) t##h
#define I32M(m, t, h) m##t##h
#define I32B(b, m, t, h) b##m##t##h


#endif // Types.h
