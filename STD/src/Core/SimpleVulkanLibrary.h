#ifndef SIMPLE_VULKAN_LIBRARY_H
#define SIMPLE_VULKAN_LIBRARY_H

#include "vulkan_core.h"
#include <vulkan.h>

/*
  DOCS:
  For usage:
  #define SVL_CRASH_ON_ERROR 1
  #define SVL_X11 1
  #define SIMPLE_VULKAN_LIBRARY_IMPLEMENTATION
  #include "SimpleVulkanLibrary.h"

*/

#define SVL_X11 1

#ifndef svl_i32
//DOCS: No Types declared, so declare one
#define svl_i8 char
#define svl_i32 int
#define svl_i64 long long int
#define svl_u8 unsigned char
#define svl_u32 unsigned int
#define svl_u64 unsigned long
#define svl_f32 float
#define svl_f64 double
#define svl_size svl_u64
#endif

#if !defined(svl_i64) || !defined(svl_u8) || !defined(svl_u32) || !defined(svl_u64) || !defined(svl_f32) || !defined(svl_f64) || !defined(svl_size)
#error "Some stupid lib include svl_i32 and now we don't have types, fix it!"
#endif


typedef struct svl_v2i
{
    svl_i32 X;
    svl_i32 Y;
} svl_v2i;

typedef enum SvlErrorType
{
    SvlErrorType_None = 0,
    SvlErrorType_Instance_CantCreate,
    SvlErrorType_Instance_CantGetExts,
    SvlErrorType_Instance_CantCreateInstance,
    SvlErrorType_Instance_CantCreateDebugUtils,

    SvlErrorType_Surface_CantLoadCreate,
    SvlErrorType_Surface_CantCreate,

    SvlErrorType_PhysicalDevice_CantFind,
    SvlErrorType_SwapChainSupport_CantGet,
    SvlErrorType_SwapChain_CantCreate,

    SvlErrorType_Device,

    SvlErrorType_Queue,

    SvlErrorType_Sync,
    SvlErrorType_Cmd,

    SvlErrorType_RenderPass,
    SvlErrorType_Pipeline,
    SvlErrorType_Pipeline_Layout,
    SvlErrorType_Framebuffer_Create,

    SvlErrorType_Image,
    SvlErrorType_ImageView,
    SvlErrorType_SwapImageView,

    SvlErrorType_NoRenderPass,

    SvlErrorType_Shader_NoPath,
    SvlErrorType_Shader_BytecodeInvalid,

    SvlErrorType_Count,
} SvlErrorType;

typedef struct SvlLog
{
    void (*fInfo)(const char* pFormat, ...);
    void (*fWarn)(const char* pFormat, ...);
    void (*fError)(const char* pFormat, ...);
} SvlLog;

// todo: delete
typedef struct SvlExtent
{
    svl_i32 Width;
    svl_i32 Height;
} SvlExtent;

typedef struct SvlPaths
{
    const char* pShaderDirectory;
    const char* pRootDirectory;
} SvlPaths;

typedef struct SvlSettings
{
    svl_i8 IsDebug;
    svl_i8 IsVsync;
    svl_i8 IsPostProcess;
    // DOCS: Nothing Important
    svl_i8 IsDevicePropsPrintable;
    svl_u32 VulkanVersion;
    const char* pName;
    void* pWindowBackend;
    SvlExtent Extent;
    SvlPaths Paths;
} SvlSettings;

typedef struct SvlGraphSupport
{
    SvlErrorType Error;
    svl_i32 SurfaceFormatsCount;
    svl_i32 PresentModesCount;
    VkSurfaceCapabilitiesKHR SurfaceCapabilities;
    VkSurfaceFormatKHR* SurfaceFormats;
    VkPresentModeKHR* PresentModes;

    svl_i32 GraphicsIndex;
    svl_i32 PresentationIndex;
    // Compute?
} SvlGraphSupport;

typedef struct SvlDevice
{
    SvlErrorType Error;
    VkDevice Device;
} SvlDevice;

typedef struct SvlSync
{
    SvlErrorType Error;
    VkSemaphore ImageAvailableSemaphore;
    VkSemaphore RenderFinishedSemaphore;
    VkFence FrameFence;
} SvlSync;

typedef struct SvlSwapchainSettings
{
    svl_i8 IsVsync;
    svl_i8 IsDebug;
    svl_i8 IsPostProcess;
    VkSurfaceFormatKHR SurfaceFormat;
    VkExtent2D Resolution;
    VkSwapchainKHR PrevSwapchain;
} SvlSwapchainSettings;

typedef struct SvlSwapchain
{
    SvlErrorType Error;
    VkSwapchainKHR Swapchain;
    VkSurfaceFormatKHR SurfaceFormat;
    VkExtent2D Resolution;
} SvlSwapchain;

typedef struct SvlCmd
{
    VkCommandPool Pool;
    VkCommandBuffer Buffer;
} SvlCmd;

typedef enum SvlAttachmentType
{
    SvlAttachmentType_Default = 0,
    SvlAttachmentType_ResolveMultisample,
    SvlAttachmentType_Depth,
    SvlAttachmentType_SwapChained,
} SvlAttachmentType;

typedef struct SvlAttachment
{
    SvlAttachmentType Type;
    VkAttachmentDescription Description;
    VkAttachmentReference Reference;
} SvlAttachment;

typedef struct SvlImageSettings
{
    svl_i32 Width;
    svl_i32 Height;
    svl_i32 Depth;
    svl_i32 MipLevels;
    VkSampleCountFlagBits SamplesCount;
    VkFormat Format;
    VkImageTiling ImageTiling;
    VkImageUsageFlags ImageUsageFlags;
    VkMemoryPropertyFlags MemoryPropertyFlags;
} SvlImageSettings;

typedef struct SvlSwapImageView
{
    VkImage Image;
    VkImageView View;
} SvlSwapImageView;

typedef struct SvlImageViewSettings
{
    SvlImageSettings ImageSettings;
    VkFormat Format;
    VkImageAspectFlags ImageAspectFlags;
    svl_i32 MipLevels;
} SvlImageViewSettings;

typedef struct SvlImageView
{
    VkImage Image;
    VkImageView View;
    VkDeviceMemory Memory;
    svl_i32 MipLevels;
} SvlImageView;


typedef enum SvlTextureFlags
{
    SvlTextureFlags_None = 0,
    SvlTextureFlags_Linear     = 1 << 2,
    SvlTextureFlags_Nearest    = 1 << 3,

    SvlTextureFlags_Anisotropy_2x = 1 << 4,
    SvlTextureFlags_Anisotropy_4x = 1 << 5,
    SvlTextureFlags_Anisotropy_8x = 1 << 6,
    SvlTextureFlags_Anisotropy_16x = 1 << 7,

    SvlTextureFlags_Repeat = 1 << 8,
    SvlTextureFlags_ClampToEdge = 1 << 9,
    SvlTextureFlags_ClampToBorder = 1 << 10,

    SvlTextureFlags_Anisotropy = SvlTextureFlags_Anisotropy_2x | SvlTextureFlags_Anisotropy_4x | SvlTextureFlags_Anisotropy_8x | SvlTextureFlags_Anisotropy_16x,

} SvlTextureFlags;

// DOCS: TextureSettings: For User-Space Code
typedef struct SvlTextureSettings
{
    svl_i32 Width;
    svl_i32 Height;
    SvlTextureFlags Flags;
    svl_i8 Channels;
    svl_i8 IsMipMaped;
    svl_i8 IsAntisotropic;
    void* pData;
} SvlTextureSettings;

// DOCS: Texture: For User-Space Code
typedef struct SvlTexture
{
    VkImage Image;
    VkImageView View;
    VkDeviceMemory Memory;
    VkSampler Sampler;
    svl_i32 MipLevels;
} SvlTexture;

typedef enum SvlShaderType
{
    SvlShaderType_Vertex = 0,
    SvlShaderType_Fragment,
    SvlShaderType_Geometry,
    SvlShaderType_Compute,
    SvlShaderType_TesselationControl,
    SvlShaderType_TesselationEvaluation,
    SvlShaderType_Count,
} SvlShaderType;

static VkShaderStageFlagBits
svl_shader_type_to_flag(SvlShaderType type)
{
    switch (type)
    {

    case SvlShaderType_Vertex: return VK_SHADER_STAGE_VERTEX_BIT;
    case SvlShaderType_Fragment: return VK_SHADER_STAGE_FRAGMENT_BIT;
    case SvlShaderType_Geometry: return VK_SHADER_STAGE_GEOMETRY_BIT;
    case SvlShaderType_Compute: return VK_SHADER_STAGE_COMPUTE_BIT;
    case SvlShaderType_TesselationControl: return VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT;
    case SvlShaderType_TesselationEvaluation: return VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT;
    case SvlShaderType_Count: break;

    }

    return (VkShaderStageFlagBits) 0;
}

typedef struct SvlFrame
{
    svl_u32 ImageIndex;
} SvlFrame;

typedef struct SvlDescriptor
{
    char* pName;
    VkDescriptorType Type;
    svl_i64 Count;
    size_t Size;

    // DOCS: Private: Setup Internally In Shader
    svl_i32 Binding;
    // DOCS: Can be sampler or image
    // VkDescriptorImageInfo* aImageInfos;
} SvlDescriptor;

typedef struct SvlShaderStage
{
    SvlShaderType Type;
    const char* ShaderSourcePath;
    SvlDescriptor aDescriptors[15];
    svl_i32 DescriptorsCount;
} SvlShaderStage;

typedef struct SvlShaderBytecode
{
    void* Bytecode;
    size_t Size;
} SvlShaderBytecode;

typedef struct SvlShaderGroupSettings
{
    const char* aPaths[SvlShaderType_Count];
} SvlShaderGroupSettings;

typedef struct SvlShaderGroup
{
    SvlShaderBytecode aBytecodes[SvlShaderType_Count];
} SvlShaderGroup;

typedef struct SvlBufferSettings
{
    svl_size Size;
    VkBufferUsageFlagBits Usage;
} SvlBufferSettings;

typedef struct SvlBuffer
{
    svl_size Size;
    VkBuffer Staging;
    VkBuffer Gpu;
    VkDeviceMemory StagingMemory;
    VkDeviceMemory GpuMemory;
} SvlBuffer;


typedef enum SvlPipelineFlags
{
    SvlPipelineFlags_DepthStencil    = 1 << 1,
    SvlPipelineFlags_BackFaceCulling = 1 << 2,
    SvlPipelineFlags_IndirectDraw    = 1 << 3,
    //SvlPipelineFlags_Batched    = 1 << 4,
} SvlPipelineFlags;

typedef struct SvlShaderAttribute
{
    VkFormat Format;
    svl_i32 Offset;
    svl_i32 GroupBinding;
} SvlShaderAttribute;

typedef struct SvlUniformSettings
{
    svl_size Size;
    svl_size ItemSize;
    svl_i32 BindIndex;
    const char* pName;
    VkBufferUsageFlagBits Usage;
} SvlUniformSettings;

typedef struct SvlUniform
{
    svl_size Size;
    svl_size ItemSize;
    // note: we dont need this
    //svl_size Offset;
    svl_size Count;
    const char* pName;
    svl_i32 BindIndex;

    SvlBuffer Buffer;
} SvlUniform;


typedef struct SvlPipelineSettings
{
    // DOCS: Some easy flags
    SvlPipelineFlags Flags;

    // DOCS: Used RenderPass index
    svl_i32 RenderPassGroupIndex;

    // DOCS: Render pipeline description
    svl_i32 Stride;

    svl_i32 StagesCount;
    SvlShaderStage* aStages;

    svl_i32 AttributesCount;
    SvlShaderAttribute* aAttributes;

    VkPolygonMode PolygonMode;

    // DOCS: Some shader constants
    svl_i64 MaxInstanceCount;
} SvlPipelineSettings;

typedef struct SvlSsbo
{
    svl_size Size;
    svl_size ItemSize;
    svl_size Count;
    const char* pName;
    svl_i32 BindIndex;

    SvlBuffer Buffer;
} SvlSsbo;

typedef struct SvlBinding
{
    SvlUniform* aUniforms;
    SvlDescriptor* aDescriptors;
    SvlSsbo* aSsbos;

    VkDescriptorSet DescriptorSet;
    VkDescriptorSetLayout DescriptorSetLayout;
    VkDescriptorPool DescriptorPool;
} SvlBinding;

typedef struct SvlPipeline
{
    int IsActive;
    SvlBinding Binding;
    VkPipeline Handle;
    VkPipelineLayout Layout;

    SvlBuffer Vertex;
    SvlBuffer Index;
    svl_i64 MaxInstanceCount;
    svl_i64 VerticesCount;
    svl_i64 IndicesCount;

    // DOCS: Dynamic/Private
    VkViewport Viewport;
    VkRect2D Scissor;
} SvlPipeline;

typedef enum SvlRenderPassType
{

    // DOCS: Prior, Previous Render Pass (Not final)
    SvlRenderPassType_Prior = 0,

    /*
      DOCS: RenderPass that use SwapChain images as image views for framebuffer.
      The main Render Target, something for rendering to the screen.
      Needs to be last render pass

      RenderScenePass (Prior) -> PostProccessPass(Prior) -> SwapChainedPass
    */
    SvlRenderPassType_SwapChained,

} SvlRenderPassType;

typedef struct SvlRenderPassSettings
{
    SvlRenderPassType Type;
    VkSubpassDependency Dependency;
    size_t AttachmentsCount;
    SvlAttachment aAttachments[16];
} SvlRenderPassSettings;

typedef struct SvlRenderPass
{
    svl_i32 Id;
    svl_i32 IsDisabled;
    VkRenderPass Handle;
    SvlRenderPassType Type;
    VkSampleCountFlagBits Samples;
    SvlAttachment* aAttachments;

    // DOCS: CanBeUpdated: aFramebuffers, aVsaImageViews, aSwapChainedImageViews
    VkFramebuffer* aFramebuffers;
    //SvlImageView* aVsaImageViews;
    //svl_i32 SwapImageViewsCount;
    svl_i32 ImageViewsCount;
    // todo: remove from here, it's swapchain zone
    //SvlSwapImageView* aSwapImageViews;
    SvlImageView* aImageViews;

    svl_i32 PipelinesCount;
    SvlPipeline** apPipelines;

    // DOCS: Dynamic/Private
    VkClearColorValue ClearColor;
    SvlFrame Frame;
} SvlRenderPass;

typedef struct SvlInstance
{
    svl_i32 IsInitialized;

    // DOCS: Internal Part
    struct {
	svl_i8 IsDebugMode;
	SvlErrorType Error;
	svl_u32 VersionMajor;
	svl_u32 VersionMinor;
	svl_u32 VersionPatch;
	VkInstance Handle;
	VkDebugUtilsMessengerEXT DebugMessenger;
	VkAllocationCallbacks* pAllocator;
	void* pWindowBackend;
    };

    // DOCS: Surface for window
    VkSurfaceKHR Surface;
    // DOCS: Physical Device info and properties
    struct {
	VkPhysicalDevice PhysicalDevice;
	VkPhysicalDeviceProperties Properties;
	VkExtensionProperties* aExts;
    };

    // DOCS: Graphics/Compute capabilities
    struct {
	svl_i32 SurfaceFormatsCount;
	svl_i32 PresentModesCount;
	VkSurfaceCapabilitiesKHR SurfaceCapabilities;
	VkSurfaceFormatKHR* SurfaceFormats;
	VkPresentModeKHR* PresentModes;

	svl_i32 GraphicsIndex;
	svl_i32 PresentationIndex;
    };
    // SvlGraphSupport GraphSupport;

    // DOCS: Logical device
    VkDevice Device;

    // TODO: Remove struct, keep it raw
    // SvlDevice Device;

    struct {
	VkQueue PresentationQueue;
	VkQueue GraphicsQueue;
    };

    // note: Some presentation stuff
    //SvlSync Sync;
    struct {
	VkSemaphore ImageAvailableSemaphore;
	VkSemaphore RenderFinishedSemaphore;
	VkFence FrameFence;
    };

    // SvlSwapchain Swapchain;
    struct {
	VkSwapchainKHR Swapchain;
	VkSurfaceFormatKHR SurfaceFormat;
	VkExtent2D Resolution;
	svl_i32 SwapImageViewsCount;
	SvlSwapImageView* aSwapImageViews;
    };

    SvlCmd Cmd;
    SvlPaths Paths;

    // DOCS: Dynamic
    SvlRenderPass* aRenderPasses;
} SvlInstance;

#define SvlReturnError(error) {pInstance->Error = error; return;}

/*#######################
  DOCS(typedef): Svl Api
  #######################*/

SvlInstance svl_create(SvlSettings* pSettings);
void svl_destroy(SvlInstance* pInstance);


/*#######################
  DOCS(typedef): Buffer
  #######################*/

SvlBuffer svl_buffer_create(SvlInstance* pInstance, SvlBufferSettings set, SvlErrorType* pError);
void svl_buffer_set_data(SvlInstance* pInstance, SvlBuffer* pBuffer, void* pData, svl_size offset, svl_size size);
void svl_buffer_destroy(SvlInstance* pInstance, SvlBuffer* pBuffer);


/*#######################
  DOCS(typedef): Uniform
  #######################*/

SvlUniform svl_uniform_create(SvlInstance* pInstance, SvlUniformSettings set);
void svl_uniform_reset(SvlUniform* pUniform);
void svl_uniform_destroy(SvlInstance* pInstance, SvlUniform* pUniform);
void svl_uniform_set(SvlInstance* pInstance, SvlPipeline* pPipeline, SvlUniform* pUniform, void* pData, svl_size offset, svl_size size);

/*#######################
  DOCS(typedef): RenderPass
  #######################*/
SvlRenderPass svl_render_pass_create(SvlInstance* pInstance, SvlRenderPassSettings settings);
SvlRenderPass svl_render_pass_create_ext(SvlInstance* pInstance);
SvlRenderPass svl_default_2d_render_pass_create(SvlInstance* pInstance);
SvlRenderPass svl_default_render_pass_create(SvlInstance* pInstance);
void svl_render_pass_destroy(SvlInstance* pInstance, SvlRenderPass renderPass);
void svl_render_pass_update_images_framebuffers(SvlInstance* pInstance, SvlRenderPass* pRenderPass);
void svl_render_pass_draw(SvlInstance* pInstance, SvlRenderPass* pRenderPass);
SvlRenderPass* svl_render_pass(SvlInstance* pInstance, svl_i32 id);

/*#######################
  DOCS(typedef): Pipeline
  #######################*/
SvlPipeline* svl_pipeline_create(SvlInstance* pInstance, SvlPipelineSettings settings);
void svl_pipeline_create_vertex_buffer(SvlInstance* pInstance, SvlPipeline* pPipe, svl_size totalSize);
void svl_pipeline_create_index_buffer(SvlInstance* pInstance, SvlPipeline* pPipe, svl_size singleInstanceSize);
SvlUniform* svl_pipeline_get_uniform(SvlPipeline* pPipe, const char* pUniformName);


/*#######################
  DOCS(typedef): Svl Public Api
  #######################*/

void svl_set_clear_color(SvlRenderPass* pRenderPass, svl_f32 r, svl_f32 g, svl_f32 b, svl_f32 a);
void svl_set_viewport(SvlPipeline* pPipeline, svl_v2i from, svl_v2i size);
void svl_set_scissor(SvlPipeline* pPipeline, svl_v2i from, svl_v2i to);


/*#######################
  DOCS(typedef): Cmd
  #######################*/

VkCommandBuffer svl_cmd_begin(SvlInstance* pInstance);
void svl_cmd_end(SvlInstance* pInstance, VkCommandBuffer vkCmdBuffer);


/*#######################
  DOCS(typedef): Frame
  #######################*/

typedef enum SvlFrameResult
{
    SvlFrameResult_Success = 0,
    SvlFrameResult_RecreateSwapchain,
    // DOCS: needs surface recreation
    SvlFrameResult_SurfaceChanged,
} SvlFrameResult;

SvlFrameResult svl_frame_start(SvlInstance* pInstance, SvlFrame* pFrame);
void svl_frame_end(SvlInstance* pInstance, SvlFrame* pFrame);

/*#######################
  DOCS(typedef): Helpers Api
  #######################*/

const char* svl_result_to_string(VkResult vkResult);
svl_i32 svl_result_check(VkResult vkResult, const char* pMessage);

/*#######################
  DOCS(typedef): Public Api
  #######################*/
SvlTexture svl_texture_create(SvlInstance* pInstance, SvlTextureSettings set);
void svl_texture_destroy(SvlInstance* pInstance, SvlTexture* pTexture);

#endif // SIMPLE_VULKAN_LIBRARY_H
