#ifndef SIMPLE_ATLAS_FONT_H
#define SIMPLE_ATLAS_FONT_H

#include <Core/Types.h>


typedef struct SafFontSettings
{
    const char* pPath;
    i32 FontSize;

    i32 RangeMin;
    i32 RangeMax;
} SafFontSettings;

typedef struct SafChar
{
    // DOCS: Unicode codepoint
    i32 Codepoint;
    u16 X0, Y0, X1, Y1;
    //f32 Fx0, Fy0, Fx1, Fy1;

    // leftSideBearing - is the offset from the current horizontal position to the left edge of the character.
    // advanceWidth - is the offset from the current horizontal position to the next horizontal position.
    // these are expressed in unscaled coordinates
    f32 Xoff, Yoff, Xadvance;
} SafChar;

typedef struct SafCharTable
{
    i32 Key;
    SafChar Value;
} SafCharTable;

typedef struct SafFont
{
    // DOCS: Font name
    char* pName;
    // DOCS: Atlas .png to save
    char* pAtlasPath;
    // DOCS: Characters table
    SafCharTable* hCharTable;
    // DOCS: Unicode-Ranges
    i32 RangeMin;
    i32 RangeMax;
    // DOCS: Bitmap to put inside image
    void* pBitmap;
    // DOCS: Bitmap size
    v2 Size;
    // DOCS: Font size in pixel 32px
    i32 FontSize;
    // DOCS: Max font character height
    i32 MaxHeight;

    // note: not sure we need this
    i32 MaxYOffset;

} SafFont;

SafFont saf_font_create(SafFontSettings set);
void saf_font_destroy(SafFont safFont);

v2 saf_font_get_space_metrics(SafFont safFont);
v2 saf_font_get_metrics(SafFont safFont, char* pText);

#endif // SIMPLE_ATLAS_FONT_H
