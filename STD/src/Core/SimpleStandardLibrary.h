#ifndef SIMPLE_STANDARD_LIBRARY_H
#define SIMPLE_STANDARD_LIBRARY_H

// todo: implement deflate
// todo: write png_read, png_write,

/*
  Content:
  [Environment.h]

  [BasicHelpers.h] - code helper functions

  [SimpleTimer.h] - run inside app loop, pass delta time for correct work.

  [Core.h] - .

  [Bitset.h] - bitset for huge bitsets

  [FpsCounter.h] - counter for fps, should be run inside app loop,
  pass function for geting app time to fps count and you can get fps

  [Logger.h] - logger to console/file.

  [SimpleThread.h] - pthread && win thread abstraction, cross-platform

  [MemoryAllocator.h] - base allocator for all apps, easy stack arena allocator create function.

  [GlobalHelpers.h] - just some shit without group

  [Array.h] - dynamic array implementation, like List<T> in C#, but faster and implemented in C99 (modern, cool, easy to use and fast enought to not worry about performance)

  [RingQueue.h] - ring queue based on Array.h

  [String.h] - ascii-string api, works fine

  [SimpleString.h] - utf-8 strings + length + size

  [StringBuilder.h] - ascii string builder

  [HashTable.h] - hash table for int/string/wide string keys

  [IO.h] - file read/write api

  [Path.h] - path combine like in C# (not that smart but something)

  [Profiler.h] - easy to use profiler_start/profiler_end/profiler_calc

  [SimpleImage.h] - rgba image, very inefficient

  [SimpleSocket.h] - abstraction over linux/windows Berkley sockets, cross-platform as all code in this SSL

*/

//#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wreserved-user-defined-literal"
#pragma clang diagnostic ignored "-Wunused-function"


#include "Types.h"


//#pragma clang diagnostic pop

#if defined(PLATFORM_LINUX)
#include <net/if.h>
#include <sys/ioctl.h>
#elif defined(PLATFORM_WINDOWS)
#include <Windows.h>
#include <shlwapi.h>
#include <profileapi.h>
// DOCS: Get current ip address from ipconfig util
#include <iphlpapi.h>
#endif

#define STATIC_ANALIZER_CHECK 1

/*

  ###################################
  ###################################
  Environment.h
  ###################################
  ###################################

*/

#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H

void sleep_for(u64 seconds);
void linux_set_current_stack_size(i64 currentBytesNumber, i64 maxBytesNumber);

#if defined(PLATFORM_LINUX)
#define NEW_LINE "\n"
#define PATH_SEPARATOR '/'
#define PATH_SEPARATOR_STRING "/"
#define ROOT_DIRECTORY "/"
#elif defined(PLATFORM_WINDOWS)
#define NEW_LINE "\r\n"
#define PATH_SEPARATOR "\\"
#define PATH_SEPARATOR_STRING "\\"
#define ROOT_DIRECTORY "C:\\"
#else
#error "Unknown platform!"
#endif

#endif // Environment.h


/*

  ###################################
  ###################################
  BasicHelpers.h
  ###################################
  ###################################

*/

#ifndef BASIC_HELPERS_H
#define BASIC_HELPERS_H

#define DO_SINGLE_TIME(action)			\
    {						\
    static int lock = 1;			\
    if (lock)				\
    {					\
	lock = 0;				\
	action;				\
    }					\
    }

#define DO_MANY_TIME(action, count)		\
    {						\
    static int lock = count;		\
    if (lock)				\
    {					\
	--lock;				\
	action;				\
    }					\
    }

#define IS_NULL_OFFSET(ptr)						\
    ({									\
    vassert(sizeof(ptr) == 8 && "IS_NULL_OFFSET takes pointer as parameter!"); \
    u64 address = (u64) ptr;					\
    address < 10000 ? 1 : 0;					\
    })

#define DO_ONES(func) { static int flag = 1; if (flag) { flag = 0; func; } }
#define DO_ONES3(func, msg, out) { static int flag = 1; if (flag) { flag = 0; func(msg, out); } }

#define CALL_N(n)					\
    {							\
    static int start = 1;				\
    if (start > n)					\
    {						\
	vassert(0 && "More then "#n" call");	\
    }						\
    ++start;					\
    }

#define FlagSwitchDefine(enableFunction, disableFunction)	\
    ({								\
    static int flag = 0;					\
    if (!flag)						\
    {							\
	flag = 1;						\
	enableFunction;					\
    }							\
    else							\
    {							\
	flag = 0;						\
	disableFunction;					\
    }							\
    })

#define StructEquals(s1, s2)					\
    ({								\
    int result = 0;						\
    u64 size1 = sizeof((s1));				\
    u64 size2 = sizeof((s2));				\
				\
    if (size1 == size2)					\
    {							\
	result = memcmp(&(s1), &(s2), size1) == 0 ? 1 : 0;	\
    }							\
				\
    result;							\
    })

#define elvis_m1(x, y)				\
    ({						\
    __typeof__(x) xv = (x);			\
    __typeof__(y) yv = (y);			\
    (xv != -1) ? xv : yv;			\
    })
#define elvis(x, y)				\
    ({						\
    __typeof__(x) xv = (x);			\
    __typeof__(y) yv = (y);			\
    (xv) ? xv : yv;				\
    })

#define nullptr ((void*) 0)

#define ptr_diff(ptr1, ptr2)					\
    ({								\
    u64 length = ((u64)ptr2) - ((u64)ptr1);	\
    length;							\
    })

#endif // BasicHelpers.h


/*

  ###################################
  ###################################
  Core.h
  ###################################
  ###################################

*/

#ifndef CORE_H
#define CORE_H

// DOCS: Get size in bytes of Unicde string
u64 simple_core_utf8_unicode_size(u32* aUnicodes, u64 length);
// DOCS: Get count of unicode character's encoded into utf8 string.
i64 simple_core_utf8_length(char* pInput);
// DOCS: Get size in bytes of utf8 string
u64 simple_core_utf8_size(char* pInput);
// DOCS: Encode codepoint as utf8 into pOutput buffer.
// Return number of bytes been encoded.
i32 simple_core_utf8_encode(char* pOutput, u32 codepoint);
// DOCS: Return unicode codepoint from pInput string, and writes
// number of bytes been decoded into pBytesCount if not NULL.
u32 simple_core_utf8_decode(char* pInput, i32* pBytesCount);
// DOCS: Writes unicode codepoints into already allocated array pUnicodeText.
// Also writes count of unicode symbols into pTextLength if not NULL.
void simple_core_utf8_get_unicode_text(i32* pUnicodeText, char* pUtf8Text, i64* pTextLength);

// DOCS: Get string hash as i64 value.
i64 simple_core_string_hash(char* pStr, i32 length);

// DOCS: Get count of digits needed to represent number.
i32 simple_core_get_int_digit(i64 intValue);
// DOCS: Convert intValue into char* pBuffer, 1234 -> "1234"
// Returns number of bytes written.
i64 simple_core_int_to_string(char* pBuffer, i64 bufferLength, i64 intValue);
// DOCS: Parse float with threshold/accuracy
i64 simple_core_float_to_string_ext(char* pBuffer, i64 bufferLength, f64 floatValue, f64 threshold);
// DOCS: Parse floatValue into char* pBuffer, 1234.5678 -> "1234.5678"
// Returns number of bytes written.
i64 simple_core_float_to_string(char* pBuffer, i64 bufferLength, f64 floatValue);
// DOCS: Get intValue to the power of powerValue
i64 simple_core_int_power(i64 intValue, i32 powerValue);

// DOCS: Parse from string to int.
i64 simple_core_parse_int(char* pBuffer, i64 bufferLength);
// DOCS: Parse from string to float.
f64 simple_core_parse_float(char* pBuffer, i64 bufferLength);

// DOCS: Parsing for slog.
typedef struct SimpleCoreIntParsed
{
    i64 IntValue;
    char* pInput;
    char* pOutput;
} SimpleCoreIntParsed;

f64 simple_core_float_npow(i32 exponent);
SimpleCoreIntParsed simple_core_parse_int_while_int(char* pInput);

// DOCS: Inlined functions

force_inline i32
simple_core_is_char_digit(char c)
{
    return (c >= '0' && c <= '9');
}


// DOCS: Tests
/*
  simple_core_int_power_test();
  simple_core_get_int_digit_test();
  simple_core_parse_i64_test();
*/
void simple_core_get_int_digit_test();
void simple_core_int_power_test();
void simple_core_int_to_string_test();
void simple_core_float_to_string_test();

void simple_core_parse_float_test();

#endif // Core.h


/*

  ###################################
  ###################################
  Time.h
  ###################################
  ###################################

*/

typedef struct SimpleTime
{
    u8 DayOfTheWeek;
    u8 Seconds;
    u8 Minutes;
    u8 Hours;

    u8 Day;
    u8 Month;
    u16 Year;
} SimpleTime;

/*

  ###################################
  ###################################
  Time.h
  ###################################
  ###################################

*/

SimpleTime simple_time_new();
void simple_time_now(SimpleTime* pTime);
void simple_time_print(SimpleTime* pTime);
i64 simple_time_get_seconds();
const char* simple_time_to_string(SimpleTime st);
const char* simple_time_to_date_string(SimpleTime st);
const char* simple_time_to_time_string(SimpleTime st);

/*

  ###################################
  ###################################
  SimpleTimer.h
  ###################################
  ###################################

*/

i32 simple_timer_interval(SimpleTimer* pTimerData, f32 timestep);
void simple_timer_reset(SimpleTimer* pTimerData);
void simple_timer_sleep(i64 sec);
void simple_timer_mssleep(i64 ms);
void simple_timer_usleep(i64 usec);
f64 simple_timer_get_time();

/*

  ###################################
  ###################################
  Bitset.h
  ###################################
  ###################################

*/

#ifndef BITSET_H
#define BITSET_H

typedef struct Bitset
{
    u64* Chunks;
    i32 ChunksCount;
} Bitset;


Bitset bitset_new(i32 bitsCount);
void bitset_reset(Bitset cbs);
void bitset_set(Bitset cbs, i32 bit);
void bitset_unset(Bitset cbs, i32 bit);
i32 bitset_get(Bitset cbs, i32 bit);

#endif // BITSET

/*

  ###################################
  ###################################
  FpsCounter.h
  ###################################
  ###################################

*/

#ifndef FPS_COUNTER_H
#define FPS_COUNTER_H

typedef double (*GetTimeDelegate)();
typedef struct FpsCounter
{
    u32 Fps;
    u32 Frames;
    f64 Since;

    GetTimeDelegate GetTime;
} FpsCounter;

FpsCounter fps_counter_create(GetTimeDelegate getTimeDelegate);
void fps_counter_update(FpsCounter* fpsCounter);

#endif //FpsCounter.h


/*

  #####################################
  #####################################
  Slog.h - simple logger
  #####################################
  #####################################

*/

#ifndef SLOG_H
#define SLOG_H

/*
  DOCS: Store format

  format: ${level} ${file} ${function} ${line} ${time} ${message}

  Как позиционировать? Format представлен в char*

*/

typedef enum SlogMessageType
{
    SlogMessageType_Info = 0,
    SlogMessageType_Success,
    SlogMessageType_Warn,
    SlogMessageType_Error,
    SlogMessageType_Debug,
    SlogMessageType_Count
} SlogMessageType;


typedef enum SlogFormat
{
    SlogFormat_None = 0,

    SlogFormat_Level = 1 << 1,
    SlogFormat_File = 1 << 2,
    SlogFormat_Function = 1 << 3,
    SlogFormat_Line = 1 << 4,
    SlogFormat_Time = 1 << 5,

    SlogFormat_File_Short = 1 << 6,
} SlogFormat;

typedef enum SlogLevel
{
    SlogLevel_Info = 0,
    SlogLevel_Debug, // Verbose
    SlogLevel_Success,
    SlogLevel_Warn,
    SlogLevel_Error,
    SlogLevel_Count
} SlogLevel;

typedef struct SlogItem
{
    SlogLevel Level;
    const char* pFile;
    i32 Line;
    char* pSbMessage;
    char* pFunction;
    SimpleTime DateTime;
} SlogItem;

typedef void (*SlogBackendDelegate)(SlogFormat format, SlogItem slogItem);
typedef struct SlogBackend
{
    SlogBackendDelegate Action;
    SlogFormat Format;
} SlogBackend;

typedef struct SlogConfig
{
    SlogBackend* aBackends;
} SlogConfig;

/*
  DOCS: Base Functions
*/
void slog_init(SlogConfig config);
void slog_deinit();
const char* slog_level_to_string(SlogLevel type);
const char* slog_level_to_color(SlogLevel type);
void slog_base(SlogLevel level, const char* pFile, i32 line, const char* pFunction, const char* pFormat, ...);

#define slog_info(format, ...)						\
    ({									\
	slog_base(SlogLevel_Info, __FILE__, __LINE__, __FUNCTION__, format, ##__VA_ARGS__); \
    })

#define slog_debug(format, ...)						\
    ({									\
	slog_base(SlogLevel_Debug, __FILE__, __LINE__, __FUNCTION__, format, ##__VA_ARGS__); \
    })

#define slog_success(format, ...)					\
    ({									\
	slog_base(SlogLevel_Success, __FILE__, __LINE__, __FUNCTION__, format, ##__VA_ARGS__); \
    })

#define slog_warn(format, ...)						\
    ({									\
	slog_base(SlogLevel_Warn, __FILE__, __LINE__, __FUNCTION__, format, ##__VA_ARGS__); \
    })

#define slog_error(format, ...)						\
    ({									\
	slog_base(SlogLevel_Error, __FILE__, __LINE__, __FUNCTION__, format, ##__VA_ARGS__); \
    })

/*
  DOCS: Backends
*/
void slog_back_terminal(SlogFormat format, SlogItem slogItem);
void slog_back_file(SlogFormat format, SlogItem slogItem);

/*
  DOCS: AutoTest's
*/
void slog_auto_tests();

#endif // Slog.h



/*

  #####################################
  #####################################
  Logger.h (CONSOLE LOGGER)
  #####################################
  #####################################

*/

#ifndef LOGGER_H
#define LOGGER_H

/*
  ==================
  ==================
  ==              ==
  ==    COLORS    ==
  ==              ==
  ==================
  ==================

  Name            BG  FG
  Black           30  40
  Red             31  41
  Green           32  42
  Yellow          33  43
  Blue            34  44
  Magenta         35  45
  Cyan            36  46
  White           37  47
  Bright Black    90  100
  Bright Red      91  101
  Bright Green    92  102
  Bright Yellow   93  103
  Bright Blue     94  104
  Bright Magenta  95  105
  Bright Cyan     96  106
  Bright White    97  107

  [a;b;cmTEXT\033[0m
  a - mode
  b - background color
  c - foreground color
  \033 - foreground & background

  examples:
  printf("\033[36mTexting\033[0m \n");

  //Background
  printf("\033[1;42;30mTexting\033[0m\t\t");
  printf("\033[3;44;30mTexting\033[0m\t\t");
  printf("\033[4;104;30mTexting\033[0m\t\t");
  printf("\033[5;100;30mTexting\033[0m\n");

  printf("\033[4;47;35mTexting\033[0m\t\t");
  printf("\033[3;47;35mTexting\033[0m\t\t");
  printf("\033[3;43;30mTexting\033[0m\t\t");

  107


*/

#define WHITEBG(x) "\e[30;107m"x"\e[0m"

#define BLACK(x) "\x1B[30m"x"\033[0m"
#define RED(x) "\x1B[31m"x"\033[0m"
#define GREEN(x) "\x1B[32m"x"\033[0m"
#define GREENQ(x) "\x1B[32m"x"\033[0m"
#define ORANGE(x) "\x1B[33m"x"\033[0m"
#define YELLOW(x) "\x1B[93m"x"\033[0m"
#define BLUE(x) "\x1B[34m"x"\033[0m"
#define BLUEM(x) "\x1B[94m"x"\033[0m"
#define MAGNETA(x) "\x1B[35m"x"\033[0m"
#define CYAN(x) "\x1B[36m"x"\033[0m"
#define WHITE(x) "\x1B[37m"x"\033[0m"

#define BRIGHTBLACK(x) "\x1B[90m"x"\033[0m"
#define BRIGHTRED(x) "\x1B[91m"x"\033[0m"
#define BRIGHTGREEN(x) "\x1B[92m"x"\033[0m"
#define BRIGHTYELLOW(x) "\x1B[93m"x"\033[0m"
#define BRIGHTBLUE(x) "\x1B[94m"x"\033[0m"
#define BRIGHTMAGNETA(x) "\x1B[95m"x"\033[0m"
#define BRIGHTCYAN(x) "\x1B[96m"x"\033[0m"
#define BRIGHTWHITE(x) "\x1B[97m"x"\033[0m"

#define RED5(x) "\033[5;1;31m"x"\033[0m"
#define REDBG5(x) "\033[5;101;30m"x"\033[0m"
#define GREEN5(x) "\033[5;1;32m"x"\033[0m"

typedef enum LoggerType
{
    LoggerType_Terminal = 1 << 0,
    LoggerType_File     = 1 << 1
} LoggerType;

typedef enum LoggerFlags
{
    LoggerFlags_None = 0,
    LoggerFlags_PrintType         = 1 << 0,
    LoggerFlags_PrintFile         = 1 << 1,
    LoggerFlags_PrintShortFile    = 1 << 2,
    LoggerFlags_PrintLine         = 1 << 3,
    LoggerFlags_PrintShortLine    = 1 << 4,
    LoggerFlags_PrintTime         = 1 << 5,
    LoggerFlags_PrintShortTime    = 1 << 6,
    LoggerFlags_PrintFunc         = 1 << 7,

    LoggerFlags_Short = LoggerFlags_PrintType,
    LoggerFlags_Full = LoggerFlags_PrintType | LoggerFlags_PrintFile | LoggerFlags_PrintLine | LoggerFlags_PrintShortTime | LoggerFlags_PrintFunc

} LoggerFlags;

extern LoggerType gLogOutput;

typedef enum SimpleLogType
{
    SimpleLogType_Log = 0,
    SimpleLogType_Debug,
    SimpleLogType_Sucess,
    SimpleLogType_Info,
    SimpleLogType_Warn,
    SimpleLogType_Error,
} SimpleLogType;

force_inline const char*
simple_log_type_to_string(SimpleLogType type)
{
    switch (type)
    {
    case SimpleLogType_Log: return "[LOG]";
    case SimpleLogType_Debug: return "[DEBUG]";
    case SimpleLogType_Sucess: return "[SUCESS]";
    case SimpleLogType_Info: return "[INFO]";
    case SimpleLogType_Warn: return "[WARN]";
    case SimpleLogType_Error: return "[ERROR]";
    }
}

typedef struct SimpleLogItem
{
    SimpleLogType Type;
    i32 Line;
    const char* pFile;
    const char* pFunc;
    const char* pMessage;
    SimpleTime Time;
} SimpleLogItem;

void simple_log_output(SimpleLogType type, const char* pFormat, const char* pFile, const char* pFunc, i32 line, ...);

void logger_init();
void logger_to_terminal();
void logger_to_file();
void logger_output(const char* levelMessage, const char* format, const char* file, const char* pFunc, i32 line, ...);
void logger_set_flags(LoggerFlags flags);
LoggerFlags logger_get_flags();
void logger_set_short();
void logger_set_long();

#define GLOG(format, ...)                                               \
    simple_log_output(SimpleLogType_Log, format, __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define GSUCCESS(format, ...)                                           \
    simple_log_output(SimpleLogType_Sucess, format, __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define GERROR(format, ...)                                             \
    simple_log_output(SimpleLogType_Error, format, __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define GWARNING(format, ...)                                           \
    simple_log_output(SimpleLogType_Warn, format, __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define GINFO(format, ...)                                              \
    simple_log_output(SimpleLogType_Info, format, __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__)
#ifdef DEBUG
#define GDEBUG(format, ...)
#else
#define GDEBUG(format, ...)                                             \
    simple_log_output(SimpleLogType_Debug, format, __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__)
#endif
#define GFORMAT(string, format, ...) sprintf(string, format, __VA_ARGS__)

#endif // Logger.h


/*###################################
  # DOCS: Sva.h (Simple void* array)
  ###################################*/

typedef struct SvaHeader
{
    i64 Count;
    i64 Capacity;
    void* pData;
} SvaHeader;

force_inline SvaHeader*
sva_header(void* pArr)
{
    vassert_not_null(pArr);
    SvaHeader* pHdr = (SvaHeader*) (((char*)pArr) - sizeof(SvaHeader));
    return pHdr;
}

force_inline i64
sva_count(void* pArr)
{
    if (pArr == NULL)
	return 0;

    SvaHeader* pHdr = sva_header(pArr);
    return pHdr->Count;
}

force_inline i64
sva_capacity(void* pArr)
{
    if (pArr == NULL)
	return 0;

    SvaHeader* pHdr = sva_header(pArr);
    return pHdr->Capacity;
}

void  sva_free(void* pArr);
void* _sva_add(void* pArr, void* pData, i32 stride);
void* _sva_add_multiple(void* pArr, void* pData, i32 stride, i64 count);
void* _sva_remove(void* pArr, void* item, i32 stride);
void* _sva_remove_index(void* pArr, i32 ind, i32 stride);
void* _sva_reserve(void* pArr, i32 stride, i64 count);
void* _sva_clear(void* pArr, i32 stride);
void* _sva_deep_copy(void* pArr, i32 stride);

#define sva_add(pArr, item)						\
    ({									\
	__typeof__(*pArr) localItem = item;				\
	pArr = _sva_add(pArr, (void*) &localItem, sizeof(*pArr));	\
	pArr;								\
    })

#define sva_add_multiple(pArr, pData, stride, count)			\
    ({									\
	pArr = _sva_add_multiple(pArr, (void*)pData, stride, count);	\
	pArr;								\
    })


// todo: test it
#define sva_remove(pArr, item)						\
    ({									\
	__typeof__(*pArr) localItem = item;				\
	pArr = _sva_remove(pArr, (void*) &localItem, sizeof(*pArr));	\
	pArr;								\
    })

#define sva_remove_index(pArr, index)					\
    ({									\
	pArr = _sva_remove_index(pArr, index, sizeof(*pArr));		\
	pArr;								\
    })


#define sva_reserve(pArr, count)				\
    ({								\
	pArr = _sva_reserve(pArr, sizeof(*pArr), count);	\
    })

#define sva_clear(pArr)				\
    ({						\
	pArr = _sva_clear(pArr, sizeof(*pArr));	\
    })

#define sva_deep_copy(pArr)				\
    ({							\
	pArr = _sva_deep_copy(pArr, sizeof(*pArr));	\
	pArr;						\
    })

#define sva_foreach(pArr, action)				\
    ({								\
	for (i64 iter = 0; iter < sva_count((pArr)); ++iter)	\
	{							\
	    __typeof__(*(pArr)) item = (pArr)[iter];		\
	    action;						\
	}							\
    })
#define sva_foreach_ptr(pArr, action)				\
    ({								\
	for (i64 iter = 0; iter < sva_count((pArr)); ++iter)	\
	{							\
	    __typeof__(*(pArr))* item = &(pArr)[iter];		\
	    action;						\
	}							\
    })

#define sva_index_of(pArr, indexOfCondition)		\
    ({                                                  \
	i64 result = -1;                                \
							\
	if ((pArr) != NULL)				\
	{						\
	    for (i64 i = 0; i < sva_count((pArr)); ++i)	\
	    {						\
		__typeof__((pArr[0])) item = (pArr)[i];	\
		if (indexOfCondition)			\
		{					\
		    result = i;				\
		    break;				\
		}					\
	    }						\
	}						\
							\
	result;						\
    })



/*

  ###################################
  ###################################
  Array.h (ARRAY)
  ###################################
  ###################################

*/

#ifndef ARRAY_H
#define ARRAY_H

#include <string.h>

typedef struct ArrayHeader
{
    // Only one value for now
    i32 IsReserved;
    i32 ElementSize;
    i64 Count;
    i64 Capacity;
    void* Buffer;
} ArrayHeader;

static i32 StartSize = 1;

#define array_header(b) ((ArrayHeader*) (((char*)b) - sizeof(ArrayHeader)))
#define array_len(b) ((b != NULL) ? array_header(b)->Count : 0)
#define array_count(b) ((b != NULL) ? array_header(b)->Count : 0)
#define array_cap(b) ((b != NULL) ? array_header(b)->Capacity : 0)
#define array_capacity(b) ((b != NULL) ? array_header(b)->Capacity : 0)
#define array_element_size(b) ((b != NULL) ? array_header(b)->ElementSize : 0)
#define array_esize(b) array_element_size(b)

#define array_reserve(b, elementsCount)                                 \
    ({                                                                  \
    (b) = internal_array_reserve((const void*)b, elementsCount, sizeof(*b)); \
    (b);                                                            \
    })
#define array_reserve_w_alloc(b, elementsCount, alloc)                  \
    ({                                                                  \
    internal_array_reserve_w_alloc((const void*)b, elementsCount, sizeof(*b), alloc); \
    })

#define array_grow(b)                                           \
    ({                                                          \
    b = internal_array_grow((const void*)b, sizeof(*b));    \
    })

#define array_set(b, v)                         \
    {                                           \
    assert(b);                              \
    int count = array_count(b);             \
    for (int i = 0; i < count; i++)         \
    {                                       \
	b[i] = v;                           \
    };                                      \
    }

/* void _array_push(void* pArr, void* pData, u64 size) */
/* { */
/*     b[array_count(b)] = (__VA_ARGS__); */
/*     ++array_header(b)->Count; */
/* } */

#define array_push(b, ...)                                              \
    {                                                                   \
    if ((b) == NULL || array_len(b) >= array_cap(b))                \
    {                                                               \
	(b) = internal_array_grow((const void*)b, sizeof(__typeof__(b[0]))); \
    }                                                               \
				    \
    b[array_count(b)] = (__VA_ARGS__);                              \
    ++array_header(b)->Count;                                       \
    }

#define array_push_array(b, otherArray)                                 \
    {                                                                   \
    b = array_reserve_w_alloc(b, array_capacity(otherArray), memory_allocate); \
    memcpy((void*)b, (void*)otherArray, array_count(otherArray) * array_element_size(otherArray)); \
    array_header(b)->Count = array_header(otherArray);              \
    }

#define array_push_void(b, voidData, sizeofsb)                          \
    ({                                                                  \
    if ((b) == NULL || array_len(b) >= array_cap(b))                \
    {                                                               \
	(b) = internal_array_grow((const void*)b, sizeofsb);        \
	array_header(b)->IsReserved = 0;                            \
    }                                                               \
				    \
    void* returnValue = (b) + array_count(b) * sizeofsb;            \
    memcpy(returnValue, voidData, sizeofsb);                        \
    /* b[array_count(b)] = (voidData); */                           \
    ++array_header(b)->Count;                                       \
				    \
    returnValue;                                                    \
    })


#define array_push_empty_with_size(b, size)                             \
    {                                                                   \
    if ((b) == NULL || array_len(b) >= array_cap(b))                \
    {                                                               \
	(b) = internal_array_grow((const void*)b, size);            \
	array_header(b)->IsReserved = 0;                            \
    }                                                               \
				    \
    assert(array_header(b)->IsReserved != 1 && "This array reserved, you can't use push here, use add instead!"); \
				    \
    ++array_header(b)->Count;                                       \
    }

#define array_add(b, i, ...)                    \
    {                                           \
    b[i] = (__VA_ARGS__);                   \
    }

#define array_needs_realloc(b)                          \
    ({                                                  \
    ((b) == NULL || array_len(b) >= array_cap(b));  \
    })

#define array_push_w_alloc(b, alloc, free, ...)                         \
    {                                                                   \
    if (array_needs_realloc(b))                                     \
    {                                                               \
	(b) = internal_array_grow_w_alloc((const void*)b, sizeof(*b), alloc, free); \
	array_header(b)->IsReserved = 0;                            \
    }                                                               \
    b[array_count(b)] = (__VA_ARGS__);                              \
    ++array_header(b)->Count;                                       \
    }
#define array_insert(b, ind, ...)                                       \
    ({                                                                  \
    vassert(b && "array should be allocated before usage!");        \
    ArrayHeader* hdr = array_header(b);                             \
    vassert(ind >= 0 && ind < hdr->Capacity && "Wrong array_insert index!"); \
    (b)[ind] = (__VA_ARGS__);                                       \
    ++hdr->Count;                                                   \
    })
#define array_insert_w_func(b, func, ...)                               \
    ({                                                                  \
    vassert(b && "array should be allocated before usage!");        \
				    \
    ArrayHeader* hdr = array_header(b);                             \
    if (hdr->Count >= (hdr->Capacity - 1))                          \
    {                                                               \
	func;                                                       \
	hdr->Count = 0;                                             \
    }                                                               \
				    \
    (b)[hdr->Count] = (__VA_ARGS__);                                \
    ++hdr->Count;                                                   \
    })
#define array_push_front(b, ...)                                        \
    ({                                                                  \
    if ((b) == NULL || array_len(b) >= array_cap(b))                \
    {                                                               \
	(b) = internal_array_grow((const void*)b, sizeof(*b));      \
    }                                                               \
    else                                                            \
    {                                                               \
	i32 i, count = array_count(b);                              \
	for (i = (count - 1); i >= 1; --i)                          \
	{                                                           \
	b[i] = b[i - 1];                                        \
	}                                                           \
    }                                                               \
    b[0] = (__VA_ARGS__);                                           \
    ++array_header(b)->Count;                                       \
    })

//NOTE(bies): Used for PriorityQueue
#define array_push_at(b, i, ...)                                        \
    {                                                                   \
    if ((b) == NULL || ((array_len(b) + 1) >= array_cap(b)))        \
    {                                                               \
	(b) = internal_array_grow((const void*)b, sizeof(*b));      \
    }                                                               \
				    \
    b = internal_array_shift_right(b, i);                           \
    b[i] = (__VA_ARGS__);                                           \
				    \
    ++array_header(b)->Count;                                       \
    }
#define array_push_at_w_alloc(b, i, alloc, free, ...)                   \
    {                                                                   \
    if ((b) == NULL || array_len(b) >= array_cap(b))                \
    {                                                               \
	(b) = internal_array_grow_w_alloc((const void*)b, sizeof(*b), alloc, free); \
    }                                                               \
    b[i] = (__VA_ARGS__);                                           \
    ++array_header(b)->Count;                                       \
    }

#define array_sort(b, condition)		\
    ({						\
    i64 count = array_count(b);		\
    array_sort_for(b, condition, count);	\
    })

#define array_sort_for(b, count, condition)		\
    ({                                                  \
    i64 i, j;					\
    for (i = 0; i < count; ++i)                     \
    {                                               \
	for (j = 0; j < (count-1); ++j)             \
	{                                           \
	__typeof__(b[0]) item0 = (b)[j];        \
	__typeof__(b[0]) item1 = (b)[j + 1];    \
	if (condition)                          \
	{                                       \
	    (b)[j + 1] = item0;			\
	    (b)[j    ] = item1;			\
	}                                       \
	}                                           \
    }                                               \
    })

#define array_remove_condition(b, condition)    \
    {                                           \
    i32 i, j, count = array_count(b);       \
    for (i = 0; i < count; ++i)             \
    {                                       \
	__typeof__(b[0]) item = (b)[i];     \
	if (condition)                      \
	{                                   \
	for (j = i; j < count; ++j)     \
	{                               \
	    b[j] = b[j + 1];            \
	}                               \
	--array_header(b)->Count;       \
			\
	break;                          \
	}                                   \
    }                                       \
    }

void array_remove(void* array, void* itemPtr);

#define array_remove_at(b, i)                                           \
    {                                                                   \
    if ((b) != NULL)                                                \
    {                                                               \
	_array_remove_at(b, i);                                 \
	--array_header(b)->Count;                                   \
    }                                                               \
				    \
    }

#define array_peek(b)                                   \
    ({                                                  \
    vassert_not_null((b));                          \
    i64 count = array_count(b);                     \
    vassert (count > 0 && "Array is empty!");       \
    __typeof__((b)[0]) item = (b)[count - 1];       \
    item;                                           \
    })
#define array_pop(b)                                                    \
    ({                                                                  \
    vassert_not_null((b));                                          \
				    \
    ArrayHeader* pHdr = array_header(b);                            \
    i64 count = pHdr->Count;                                        \
				    \
    vassert (count > 0 && "Array is empty!");                       \
				    \
    __typeof__((b)[0]) item = (b)[count - 1];                       \
    memset(((void*)b) + (pHdr->Count - 1) * pHdr->ElementSize, 0, pHdr->ElementSize); \
    --pHdr->Count;                                                  \
				    \
    item;                                                           \
    })
#define array_pop_begin(b)                                      \
    ({                                                          \
    vassert((b) && "Array can't be null or undefined!!!");  \
    vassert (array_count(b) > 0 && "Array is empty!");      \
				\
    __typeof__((b)[0]) item = (b)[0];                       \
				\
    memset((void*)b, 0, array_esize(b));                    \
				\
    if (array_count(b) > 1)                                 \
    {                                                       \
	memcpy((void*)b,                                    \
	   (((void*)b) + array_esize(b)),               \
	   (array_count(b) - 1) * array_esize(b));      \
    }                                                       \
				\
    --array_header(b)->Count;                               \
				\
    item;                                                   \
    })


//#define array_end(b) (((void*)b) + array_count(b) * sizeof(*b))
#define array_end(b) (&b[array_count(b) - 1])
#define array_copy(src)         internal_array_copy(src)
#define array_copy_w_alloc(src, alloc) internal_array_copy_w_alloc(src, alloc)
#define array_clearv(b, val)                    \
    ({                                          \
    if ((b) != NULL)                        \
    {                                       \
	int i, count = array_count(b);      \
	for (i = 0; i < count; ++i)         \
	{                                   \
	b[i] = val;                     \
	}                                   \
	array_header(b)->Count = 0;         \
    }                                       \
    })

#define array_find(b, v)                        \
    ({                                          \
    __typeof__((b)[0]) result = {0};        \
    if ((b) != NULL)                        \
    {                                       \
	i32 i, count = array_count(b);      \
	for (i = 0; i < count; ++i)         \
	{                                   \
	if ((b)[i] == v)                \
	{                               \
	    result = (b)[i];            \
	    break;                      \
	}                               \
	}                                   \
    }                                       \
    result;                                 \
    })
#define array_find_index(b, v)                  \
    ({                                          \
    i32 result = -1;                        \
    if ((b) != NULL)                        \
    {                                       \
	i32 i, count = array_count(b);      \
	for (i = 0; i < count; ++i)         \
	{                                   \
	if ((b)[i] == v)                \
	{                               \
	    result = i;                         \
	    break;                      \
	}                               \
	}                                   \
    }                                       \
    result;                                 \
    })
#define array_index_of(b, indexOfCondition)             \
    ({                                                  \
    i64 result = -1;                                \
			    \
    if ((b) != NULL)                                \
    {                                               \
	i64 i, count = array_count(b);              \
	for (i = 0; i < count; ++i)                 \
	{                                           \
	__typeof__((b[0])) item = (b)[i];       \
	if (indexOfCondition)                   \
	{                                       \
	    result = i;                         \
	    break;                              \
	}                                       \
	}                                           \
    }                                               \
			    \
    result;                                         \
    })

/*
  Usage:
  array_find_predicate(array, tempValue, item == tempValue)
*/
#define array_find_predicate(b, predicate)              \
    ({                                                  \
    __typeof__((b[0])) result = {0};                \
    if ((b) != NULL)                                \
    {                                               \
	i32 i, count = array_count(b);              \
	for (i = 0; i < count; ++i)                         \
	{                                           \
	__typeof__((b[0])) item = (b)[i];       \
	if (predicate)                          \
	{                                       \
	    result = b[i];                      \
	    break;                              \
	}                                       \
	}                                           \
    }                                               \
    result;                                                 \
    })

#define array_free(b)                                                   \
    ({                                                                  \
    if ((b))                                                        \
    {                                                               \
	_memory_free(array_header((b)), __LINE__, __FILE__);        \
    }                                                               \
    (b) = NULL;                                                     \
    })

#define array_free_w_item(b)                                    \
    ({                                                          \
    if ((b))                                                \
    {                                                       \
	i32 i, count = array_count(b);                      \
	for (i = 0; i < count; ++i)                                 \
	{                                                   \
	_memory_free((b)[i], __LINE__, __FILE__);       \
	}                                                   \
	array_free(b);                                      \
    }                                                       \
    })

#define array_for(b, count)						\
    __typeof__((b[0])) item;						\
    i64 ind;								\
    for (ind = 0, item = (b)[ind]; ind < count; ++ind)

#define array_foreach(b, code)                  \
    ({                                          \
    i64 i, count = array_count(b);          \
    for (i = 0; i < count; ++i)             \
    {                                       \
	__typeof__((b[0])) item = (b)[i];   \
	code;                               \
    }                                       \
    })
#define array_foreach_ptr(b, code)                      \
    ({                                                  \
    i32 i, count = array_count(b);                  \
    for (i = 0; i < count; ++i)                     \
    {                                               \
	__typeof__((b[0]))* item = &((b)[i]);       \
	code;                                       \
    }                                               \
    })

#define array_filter(b, condition)                      \
    ({                                                  \
    __typeof__((b[0])) filteredItem = (b)[0];       \
    i32 i, count = array_count(b);                  \
    for (i = 1; i < count; ++i)                     \
    {                                               \
	__typeof__((b[0])) item = (b)[i];           \
	if ((condition))                            \
	{                                           \
	filteredItem = item;                    \
	}                                           \
    }                                               \
    filteredItem;                                   \
    })

#define array_any(b) ({ (b) != NULL ? (array_header((b))->Count > 0) : 0; })
#define array_any_cond(b, cond)                                 \
    ({                                                  \
    i32 i, res = -1, count = array_count(b);        \
    for (i = 0; i < count; ++i)                     \
    {                                               \
	__typeof__((b[0])) item = (b)[i];           \
	if (cond)                                   \
	{                                           \
	res = i;                                \
	break;                                  \
	}                                           \
    }                                               \
    res;                                            \
    })

#define array_distinct(b, distinctCondition)                            \
    ({                                                                  \
    __typeof__((b)) distinctArr = NULL;                             \
    i32 i, count = array_count(b);                                  \
    for (i = 0; i < count; ++i)                                     \
    {                                                               \
	__typeof__((b[0])) distinctItem = (b)[i];                   \
	if (array_index_of((distinctArr), distinctCondition) == -1)         \
	{                                                           \
	array_push(distinctArr, distinctItem);                  \
	}                                                           \
    }                                                               \
    distinctArr;                                                    \
    })

#define array_where(b, condition)                               \
    ({                                                          \
    vassert(array_any(b) && "array_where: Empty array!");   \
    __typeof__((b)) resultArr = NULL;                       \
				\
    for (i32 i = 0; i < array_count(b); ++i)                \
    {                                                       \
	__typeof__((b)[0]) item = (b)[i];                   \
	if (condition)                                      \
	{                                                   \
	array_push(resultArr, item);                    \
	}                                                   \
    }                                                       \
				\
    resultArr;                                              \
    })

#define array_clear(b)                          \
    ({                                          \
    if ((b) != NULL)                        \
    {                                       \
	internal_array_clear(b);            \
    }                                       \
    })

#define internal_array_reserve(array, elementsCount,elementSize)        \
    ({ internal_array_reserve_w_alloc(array, elementsCount, elementSize, _memory_allocate); })
#define internal_array_grow(array, elementSize)                         \
    ({ internal_array_grow_w_alloc(array, elementSize, _memory_allocate, _memory_free); })
#define internal_array_copy(src)                                \
    ({ internal_array_copy_w_alloc(src, _memory_allocate); })

void _array_remove_at(void* b, i32 i);

void* internal_array_reserve_w_alloc(const void* array, int elementsCount, int elementSize, void* (*allocDelegate)(u64 n, i32 line, const char* file));
void* internal_array_grow_w_alloc(const void* array, i32 elementSize, void* (*allocDelegate)(u64 n, i32 line, const char* file), void (*freeDelegate)(void* data, i32 line, const char* file));
void* internal_array_copy_w_alloc(const void* src, void* (*allocDelegate)(u64 n, i32 line, const char* file));
void* internal_array_shift_right(void* b, i32 i);
void* internal_array_pop(void* b);
void* internal_array_clear(void* array);

#endif // ARRAY_H

/*

  ###################################
  ###################################
  RingQueue.h (RING_QUEUE)
  ###################################
  ###################################

*/
#ifndef RING_QUEUE_H
#define RING_QUEUE_H

/*
  DOCS(typedef):
  RingQueue only for small size collection, max count = 2^15 (32K)

  RingQueue work as default queue except that we change StartIndex
  every call:ring_queue_add() to ++StartIndex, and after Count == Capacity,
  we set it back to zero, StartIndex = 0, so we replace old member with
  newer one;


  InsertIndex: 0
  0 1 2 3 4 5 6 7 8 9
  a b c d d d d d d d
  ^

  pop()

  InsertIndex: 9
  0 1 2 3 4 5 6 7 8 9
  a * c d d d d d d d
  ^


*/
typedef struct RingQueueHeader
{
    i16 Count;
    i16 Capacity;
    i16 InsertIndex;
    i16 ElementSize;
    void* Buffer;
} RingQueueHeader;

#define ring_queue_header(rq) ((RingQueueHeader*) (((char*)rq) - sizeof(RingQueueHeader)))
#define ring_queue_count(rq) ((rq) ? ring_queue_header(rq)->Count : 0)
#define ring_queue_capacity(rq) ((rq) ? ring_queue_header(rq)->Capacity : 0)
#define ring_queue_insert_index(rq) ((rq)? ring_queue_header(rq)->InsertIndex:0)

void* _ring_queue_new(const void*, i16 capacity, u64 size);
void _ring_queue_free(void* ringQueue);
i64 _ring_queue_push(void*);
i16 _rinq_queue_get_popped_index(void*);
void _ring_queue_pop(void*, i16);
void ring_queue_test();

#define rinq_queue_new(rq, capacity)                                    \
    ({                                                                  \
    (rq) = _ring_queue_new((const void*)rq, capacity, sizeof(*rq));         \
    })

#define ring_queue_push(rq, ...)                \
    ({                                          \
    i64 ind = _ring_queue_push(rq);         \
    (rq)[ind] = (__VA_ARGS__);              \
    })

#define ring_queue_pop(rq)                              \
    ({                                                  \
    __typeof__((rq)[0]) item;                       \
    i16 popInd = _rinq_queue_get_popped_index(rq);  \
    item = (rq)[popInd];                            \
    _ring_queue_pop(rq, popInd);                    \
			    \
    item;                                           \
    })

#define ring_queue_peek(rq)                             \
    ({                                                  \
    i32 popInd = _ring_queue_get_poped_index(rq);   \
    (rq)[popInd];                                   \
    })


#endif // RING_QUEUE_H

/*

  ###################################
  ###################################
  PriorityQueue.h
  ###################################
  ###################################

*/
#ifndef PRIORITY_QUEUE_H
#define PRIORITY_QUEUE_H

#define priority_queue_count(b) ({array_count((b));})

#define priority_queue_clear(b)                 \
    ({                                          \
    array_clear(b);                         \
    })

#define priority_queue_index_of(b, newItem)                             \
    ({                                                                  \
    i32 i, result = -1, count = array_count(b);                     \
    for (i = 0; i < count; ++i)                                     \
    {                                                               \
	__typeof__((b)[0]) item = (b)[i];                           \
	if (memcmp(&newItem.Value, &item.Value, sizeof(item.Value)) == 0) \
	{                                                           \
	result = 1;                                             \
	break;                                                  \
	}                                                           \
    }                                                               \
    result;                                                         \
    })

#define priority_queue_push(b, newItem)                                         \
    ({                                                                  \
    if ((b) == NULL || array_len(b) >= array_cap(b))                \
    {                                                               \
	(b) = internal_array_grow((const void*)b, sizeof(*b));      \
    }                                                               \
				    \
    i32 ind = priority_queue_index_of(b, newItem);                  \
    if (ind == -1)                                                  \
    {                                                               \
	i32 i, count = array_count(b);                              \
	for (i = 0; i < count; ++i)                                         \
	{                                                           \
	__typeof__((b)[0]) item = (b)[i];                       \
	if (newItem.Priority < item.Priority)                   \
	{                                                       \
	    break;                                              \
	}                                                       \
	}                                                           \
				    \
	++array_header(b)->Count;                                   \
	array_push_at(b, i, newItem);                               \
	--array_header(b)->Count;                                   \
    }                                                               \
    })

#define priority_queue_pop(b)                   \
    ({                                          \
    __typeof__((b)[0]) result = (b)[0];     \
    (b) = internal_array_pop((b));          \
    result;                                 \
    })

#define priority_queue_get(b)                   \
    ({                                          \
    __typeof__((b)[0]) result = (b)[0];     \
    result;                                 \
    })


#endif // PriorityQueue.h


/*

  ###################################
  ###################################
  SimpleThread.h
  ###################################
  ###################################

*/
#ifndef SIMPLE_THREAD_H
#define SIMPLE_THREAD_H

#if defined(PLATFORM_LINUX)
#include <pthread.h>
#elif defined(PLATFORM_WINDOWS)
#define _WINSOCKAPI_
#include <Windows.h>
#endif

typedef struct SimpleThread
{
#if defined(PLATFORM_WINDOWS)
//#error "Platform not supported!"
    HANDLE ID;
#elif defined(PLATFORM_LINUX)
    pthread_t ID;
#endif
} SimpleThread;

typedef struct SimpleMutex
{
#if defined(PLATFORM_WINDOWS)
    HANDLE ID;
#elif defined(PLATFORM_LINUX)
    pthread_mutex_t ID;
#endif
} SimpleMutex;

typedef void* (*SimpleThreadDelegate)(void* arg);

SimpleThread simple_thread_create(SimpleThreadDelegate threadFunction, u64 stackSize, void* data);
void simple_thread_attach(SimpleThread* thread);
ResultType simple_thread_cancel(SimpleThread thread);

SimpleMutex simple_thread_mutex_create();
ResultType simple_thread_mutex_destroy(SimpleMutex* simpleMutex);
ResultType simple_thread_mutex_lock(SimpleMutex* simpleMutex);
ResultType simple_thread_mutex_unlock(SimpleMutex* simpleMutex);

#endif // SimpleThread.h

/*

  ###################################
  ###################################
  MemoryAllocator.h
  ###################################
  ###################################

*/
#ifndef MEMORY_ALLOCATOR_H
#define MEMORY_ALLOCATOR_H

#define simple_arena_create(size) _arena_create(size, __LINE__, __FILE__)
#define simple_arena_create_and_set(size) _arena_create_and_set(size, __LINE__, __FILE__)

SimpleArena* _arena_create(u64 size, i32 line, const char* file);
SimpleArena* _arena_create_and_set(u64 size, i32 line, const char* file);
void simple_arena_clear(SimpleArena* pArena);
void simple_arena_destroy(SimpleArena* arena);
void simple_arena_print(SimpleArena* arena);
void simple_arena_format(char* buffer, SimpleArena* arena);

typedef struct MemoryBlock
{
    const char* File;
    i32 Line;
    //NOTE(bies): this is size wo header
    i64 AllocatedSize;
    void* Address;
} MemoryBlock;

#define memory_block_header(b) ((MemoryBlock*) (((char*)b) - sizeof(MemoryBlock)))

typedef enum PrintAllocationSourceType
{
    PrintAllocationSourceType_None = 0,
    PrintAllocationSourceType_Terminal
} PrintAllocationSourceType;

#define memory_allocate(size) ({vassert((size > 0) && "Size should be > 0!!!");_memory_allocate(size, __LINE__, __FILE__);})
#define memory_allocate_type(type) (type*) _memory_allocate(sizeof(type), __LINE__, __FILE__)
#define memory_free(data) _memory_free(data, __LINE__, __FILE__)
#define memory_free_bytes(count) memory_helper_free_bytes(count, __LINE__, __FILE__)
#define memory_reallocate(data, size) memory_helper_reallocate(data, size, __LINE__, __FILE__)

void memory_set_arena(SimpleArena* arena);
SimpleArena* memory_get_arena();
void memory_bind_current_arena();
void memory_bind_arena(SimpleArena* pArena);
void memory_unbind_current_arena();
void* _memory_allocate(u64 size, i32 line, const char* file);
MemoryBlock** memory_helper_get_memory_blocks();
void* memory_helper_reallocate(void* data, i32 size, i32 line, const char* file);
void _memory_free(void* data, i32 line, const char* file);
i32 memory_helper_get_allocated_size();
void memory_helper_format_size(char* buf, u64 bytes);
void memory_set_print(PrintAllocationSourceType type);
void* memory_helper_malloc(u64 size, i32 line, const char* file);
void  memory_helper_free(void* data, i32 line, const char* file);

#endif // MEMORY_ALLOCATOR_H MemoryAllocator.h

/*
  ############################################
  DOCS: String.h
  ############################################
*/

#ifndef STRING_H
#define STRING_H

#define STRING_EMPTY ""
#define STRING_NULL "\0"

#include <stdio.h>
#define string_format(out, format, ...) sprintf(out, format, __VA_ARGS__)

/* Connect this to String */

#define istring_header(istr) ((IString*) (((char*)istr) - sizeof(IString)))
#define istring_length(istr) ((istr) ? istring_header(istr)->Length : -1)
#define istring(str)                            \
    ({                                          \
    char* istr = istring_get_buffer(str);   \
    (istr) ? istr : istring_allocate(str);  \
    })

#define istring_ext(stringPool, str)                            \
    ({                                                          \
    char* istr = istring_get_buffer_ext(stringPool, str);   \
    (istr) ? istr : istring_allocate_ext(stringPool, str);  \
    })


typedef struct String
{
    i64 Length;
    char* Buffer;
} String;

/* DOCS(typedef): IString local api */

IStringPool* istring_pool_create();
void istring_pool_destroy(IStringPool*);

IString* istring_new_ext(IStringPool* stringPool, const char* src);
char* istring_allocate_ext(IStringPool* stringPool, const char* src);
char* istring_get_buffer_ext(IStringPool* stringPool, const char* src);
IString* istring_get_ext(IStringPool* stringPool, const char* src);
void istring_free_ext(IStringPool* stringPool, char* istring);
void istring_free_headers_ext(IStringPool* stringPool);

/* DOCS(typedef): Global global api */
IString* istring_new(const char* src);
char* istring_allocate(const char* src);
char* istring_get_buffer(const char* src);
IString* istring_get(const char* src);
IString** istring_get_headers();
void istring_free(char* istring);
void istring_free_headers();


char* string(const char* string);
void string_i32(char* input, i32 number);
char* string_allocate(i32 length);
i32 string_count_of_fast(const char* string, i32 length, char c);
i32 string_count_of(const char* string, char c);
i32 string_count_upper(const char* string);
void string_set(char* string, char c, u32 length);
i64 string_length(const char* str);
u64 string_length_to_delimiters(const char* str, char delimeters[], u64 delimetersLength);
char* string_copy(const char* oth, i32 length);
char* string_copy_bigger(const char* oth, i32 length, i32 bigLength);
char* string_concat(const char* left, const char* right);
char* string_concat_with_space_between(const char* left, const char* right, i32 length, char c);
char* string_concat3(const char* left, const char* middle, const char* right);
char* string_concat3l(const char* left, const char* middle, const char* right, i32 leftLength, i32 middleLength, i32 rightLength);
i32 string_compare(const char* left, const char* right);
i32 string_compare_length(const char* left, const char* right, i64 length);
i32 string_compare_length_safe(const char* left, const char* right, i32 length);
i32 string_compare_w_length(const char* left, const char* right, i32 lengthLeft, i32 lengthRight);
char* string_to_upper(const char* input);
char* string_to_lower(const char* input);
i32 string_index_of(const char* input, char character);
i32 string_index_of_string(const char* input, const char* string);
i32 string_last_index_of(const char* input, char character);
i32 string_last_index_of_string(const char* input, const char* string);
i32 string_last_index_of_upper(const char* input, i32 length);
char* string_substring(const char* input, i32 startIndex);
char* string_substring_length(const char* input, u64 inputLength, i32 length);
char* string_substring_range(const char* input, i32 startIndex, i32 endIndex);
char* string_after(const char* input, i32 length, char c);
char* string_replace_string(char* input, u64 inputLength, char* replaceStr, u64 replaceStrLength, char* newString, u64 newStringLength);

/*
  3, 5
  "01234567" - len: 8
  "01267" - len: 5
*/
char* string_cut(const char* input, u32 begin, u32 end);
void string_replace_character(char* pInput, char oldC, char newC);
char* string_replace_char(char* input, char c);
char* string_trim_char(char* input, u64 length, u64* newLength, char c);
char** string_split(char* input, char splitCharacter);
char** string_split_length(char* input, u64 inputLength, char splitCharacter);
char* string_join(const char** list, char joinCharacter);
char* string_join_i32(const i32* list, char joinCharacter);
void string_i64(char* input, i64 number);
i32 string_is_integer(char* input, u64 length);
i32 string_to_i32(char* input);
i32 string_to_i32_length(char* input, i32 length);
f32 string_to_f32_length(char* input, u64 length);
f32 string_to_f32(char* input);

void string_i64(char* input, i64 number);
void string_f32(char* input, f32 number);
void string_f64(char* input, f64 number);

force_inline char
char_to_upper(char character)
{
    if (character >= 'a' && character <= 'z')
    return (character - 'a' + 'A');
    else
    return character;
}

force_inline char
char_to_lower(char character)
{
    if (character >= 'A' && character <= 'Z')
    return (character - 'A' + 'a');
    else
    return character;
}

force_inline i32
char_is_upper(char character)
{
    if (character >= 'A' && character <= 'Z')
    return 1;
    return 0;
}
force_inline i32
char_is_lower(char character)
{
    if (character >= 'a' && character <= 'z')
    return 1;
    return 0;
}

force_inline i32
char_is_integer(char character)
{
    if (character >= '0' && character <= '9')
    return 1;
    return 0;
}

#endif // String.h


/*
  ############################################
  DOCS: SimpleString.h
  ############################################
*/

#ifndef SIMPLE_STRING_H
#define SIMPLE_STRING_H

#define simple_string_header(pStr)					\
    ({									\
    (SimpleString*) (((char*)pStr) - sizeof(SimpleString));		\
    })
#define simple_string_length(pStr)					\
    ({									\
    (pStr != NULL) ? simple_string_header(pStr)->Length : 0;	\
    })

SimpleString* simple_string(const char* pStr);
char* simple_string_new(const char* pStr);
char* simple_string_copy(const char* pStr, i64 length, i64 size);
void simple_string_destroy(char* pStr);
char* simple_string_format(const char* pStr, ...);

#endif // SimpleString.h


/*

  ###################################
  ###################################
  GlobalHelpers.h
  ###################################
  ###################################

*/
#ifndef GLOBAL_HELPERS_H
#define GLOBAL_HELPERS_H

#define DOUBLE_ARRAY_CREATE(r, c, type) (type**)_double_array_create(r, c, sizeof(type))
#define DOUBLE_ARRAY_DESTROY(darr, rows) _double_array_destroy((u64*)darr, rows)

void* _double_array_create(i32 rows, i32 cols, u64 size);
void* _double_array_destroy(u64* darr, i32 rows);
i32 string_get_next_i32(char* stream, i32 skipChars, i32* index);

#endif // GlobalHelpers.h

/*

  ###################################
  ###################################
  StringBuilder.h
  ###################################
  ###################################

*/
#ifndef STRING_BUILDER_H
#define STRING_BUILDER_H

#define StringBuilderAllocateDelegate(size) memory_allocate(size)
#define StringBuilderFreeDelegate(data) memory_free(data)

typedef struct StringBuilderHeader
{
    i64 Count;
    i64 Capacity;
    char* Buffer;
} StringBuilderHeader;

#define sb_string_int_to_string(input, number)                          \
    ({                                                                  \
    i8 isNumberNegative = ((number < 0) ? 1 : 0);                   \
    i32 i, rank = sb_string_number_rank(number), numberLength = rank + isNumberNegative + 1; \
				    \
    if (isNumberNegative)                                           \
    {                                                               \
	input[0] = '-';                                             \
    }                                                               \
				    \
    for (i = isNumberNegative; i < numberLength; ++i)               \
    {                                                               \
	input[i] = sb_string_number_of_digit(number, rank) + 48;    \
	--rank;                                                     \
    }                                                               \
    })
#define sb_string_i32_to_string(input, number) sb_string_int_to_string(input, number)
#define sb_string_i64_to_string(input, number) sb_string_int_to_string(input, number)
#define sb_string_f64_to_string(input, number) sprintf(input, "%f", number)

#define string_builder_header(s) ((StringBuilderHeader*) (((char*)s) - sizeof(StringBuilderHeader)))
#define string_builder_count(s) ((s) != NULL ? string_builder_header((s))->Count : 0)
#define string_builder_capacity(s) ((s) != NULL ? string_builder_header((s))->Capacity : 0)
#define string_builder_buffer(s) ((s) != NULL ? string_builder_header((s))->Buffer : NULL)
#define string_builder_free(s) StringBuilderFreeDelegate(string_builder_header((s)))

#define string_builder_clear(s)				\
    ({							\
	if (s) {					\
	    memset(s, 0, string_builder_count(s));	\
	    string_builder_header(s)->Count = 0;	\
	}						\
    })

#define string_builder_appendc(s, c)                                    \
    ({                                                                  \
    string_builder_append_base((s), 1);                             \
    StringBuilderHeader* header = string_builder_header((s));       \
    header->Buffer[header->Count] = (c);                            \
    ++header->Count;                                                \
    })
#define string_builder_appends(s, str)                                  \
    ({                                                                  \
    vassert_not_null(str && "string_builder_appends (s, NULL) !!!"); \
    i32 strLength = strlen((str));                                  \
    string_builder_appendsl(s, str, strLength);                     \
    })
#define string_builder_appendsl(s, str, strLength)                      \
    ({                                                                  \
    vassert_not_null(str && "string_builder_appends (s, NULL) !!!"); \
    string_builder_append_base((s), strLength);                     \
    StringBuilderHeader* header = string_builder_header((s));       \
    memcpy((header->Buffer + header->Count), (str), strLength);     \
    header->Count += strLength;                                     \
    })
#define string_builder_appendf(s, f, ...)                       \
    ({                                                          \
    (s) = _string_builder_appendf((s), (f), ##__VA_ARGS__);         \
    })
#define string_builder_append_base(s, count)                    \
    {                                                           \
    i32 hdrCount = string_builder_count(s);                 \
    i32 hdrCapacity = string_builder_capacity(s);           \
				\
    if ((s) == NULL                                         \
	|| (hdrCapacity == 0)                               \
	|| (count + hdrCount) >= hdrCapacity)               \
    {                                                       \
	u64 newCapacity = 2 * (hdrCount + count) + 1;       \
	(s) = _string_builder_grow(s, newCapacity);         \
    }                                                       \
    }

char* _string_builder_grow(char* builder, u64 newCapacity);
char* _string_builder_appendf(char* builder, const char* format, ...);

#endif // StringBuilder.h

/*

  ###################################
  ###################################
  HashTable.h
  ###################################
  ###################################

  Can we use memcpr() ???
*/
#ifndef HASH_TABLE_H
#define HASH_TABLE_H

#define HashTableAllocate(size) memory_allocate(size)
#define HashTableFree(d) memory_free(d)

/*
  Hash Table Statistics (for profiling purposes)
*/
#if HASH_TABLE_PROFILING == 1
typedef struct TableStatistics
{
    i32 PutAttempt;
    i32 GetAttempt;
} TableStatistics;
TableStatistics table_get_statistics();
#endif // HASH_TABLE_PROFILING

typedef struct TableHeader
{
    u64 ElementSize;
    i64 Count;
    i64 Capacity;
    i64 Index;
    i32 NextPrime;
    void* Buffer;
} TableHeader;

#define table_header(b) ((TableHeader*) (((char*)b) - sizeof(TableHeader)))
#define table_count(b) ((b != NULL) ? table_header(b)->Count : 0)
#define table_capacity(b) ((b != NULL) ? table_header(b)->Capacity : 0)
#define table_element_size(b) ((b != NULL) ? table_header(b)->ElementSize : 0)
#define table_index(b) ((b != NULL) ? table_header(b)->Index : 0)
#define table_next_prime(b) ((b != NULL) ? table_header(b)->NextPrime : 0)
#define table_free(b) ((b) ? HashTableFree(table_header(b)) : 0)
#define shash_free(table) table_free((table))
#define hash_free(table) table_free((table))

/*
  Base
*/

force_inline i32
i32_comparer(i32 key)
{
    return key != -1;
}

force_inline i32
string_comparer(const char* key)
{
    return key != NULL;
}

#define base_ghash_put(table, key, value, hashPutDelegate, defValComparer, isInt) \
    ({                                                                  \
    TableHeader* hdr = table_header(table);                         \
				    \
    if (table == NULL)                                              \
    {                                                               \
	table = _table_new(table, sizeof(*table), isInt ? -1 : 0);  \
    }                                                               \
    else if (hdr->Count >= to_i64(0.7 * hdr->Capacity))             \
    {                                                               \
	__typeof__((table)) newTable = _table_grow((table), sizeof(*table), isInt ? -1 : 0); \
	TableHeader* newHeader = table_header((newTable));          \
	for (i32 j = 0; j < hdr->Capacity; ++j)                     \
	{                                                           \
	if (defValComparer((table)[j].Key))                     \
	{                                                       \
	    hashPutDelegate((newTable),(table)[j].Key);         \
	    (newTable)[newHeader->Index].Key = ((table)[j].Key); \
	    (newTable)[newHeader->Index].Value = ((table)[j].Value); \
	}                                                       \
	}                                                           \
				    \
	table_free(table);                                          \
				    \
	table = newTable;                                           \
    }                                                               \
				    \
    hashPutDelegate(table, key);                                    \
    hdr = table_header(table);                                      \
    (table)[(hdr->Index)].Key   = (key);                            \
    (table)[(hdr->Index)].Value = (value);                          \
    })

#define base_ghash_get(table, key, hashGetDelegate)     \
    ({                                                  \
    hashGetDelegate(table, key);                    \
    TableHeader* hdr = table_header(table);                 \
    ((table) != NULL && (hdr->Index != -1))                 \
	? (table)[hdr->Index].Value                         \
	: ((__typeof__(table[0].Value)) { 0 });     \
    })

void* _table_new(void* table, u64 elemSize, i32 defVar);
void* _table_grow(void* table, u64 elemSize, i32 defVar);


/*
  DOCS(typedef): String Hash Table (string Key)
*/

#define shash_put(table, key, value)                                    \
    ({                                                                  \
    base_ghash_put(table, key, value, _base_shash_put, string_comparer, 0); \
    })
#define shash_get(table, key)                           \
    ({                                                  \
    base_ghash_get(table, key, _base_shash_get);    \
    })
#define shash_geti(table, key)                  \
    ({                                          \
    i64 ind;                                \
    if (table != NULL)                      \
    {                                       \
	_base_shash_get(table, key);        \
	ind = table_header(table)->Index;   \
    }                                       \
    else                                    \
    {                                       \
	ind = -1;                           \
    }                                       \
			\
    ind;                                    \
    })

void _base_shash_put(void* table, const char* key);
void _base_shash_get(void* table, const char* key);

/*
  DOCS(typedef): Int Hash Table (int Key)
*/
#define hash_put(table, key, value)                                     \
    ({                                                                  \
	base_ghash_put((table), (key), (value), _base_hash_put, i32_comparer, 1); \
    })
#define hash_get(table, key)                            \
    ({                                                  \
    base_ghash_get((table), (key), _base_hash_get);         \
    })
// BUG(typedef): Енто костыль, убрать, нужно юзать get а потом чекать Index == -1
#define hash_geti(table, key)                   \
    ({                                          \
    i64 ind;                                \
    if (table != NULL)                      \
    {                                       \
	_base_hash_get(table, key);         \
	ind = table_header(table)->Index;   \
    }                                       \
    else                                    \
    {                                       \
	ind = -1;                           \
    }                                       \
			\
    ind;                                    \
    })

void* _base_hash_put(void* table, i32 key);
void* _base_hash_get(void* table, i32 key);



/*
  key is integer, string, custom struct

  can be backend for shash_/hash_
  svt_init({TypeInfo});

  svt_add();
  svt_add();

  svt_get();

*/

void svt_add(void* pTable);
void svt_get(void* pTable);

#endif // HashTable.h

/*

  #####################################
  #####################################
  IO.h
  #####################################
  #####################################

*/
#ifndef IO_H
#define IO_H

char* file_read_string_ext(const char* filePath, u64* length);
char* file_read_string(const char* filePath);
char* file_get_name_with_extension(const char* path);
void file_write_string(const char* filePath, char* data, u64 len);
void file_write_bytes(const char* filePath, u8* data, u64 len);
i32 file_write_string_exe(const char* filePath, char* data, u64 len);
void file_append_string(const char* filePath, char* data, u64 len);
u8* file_read_bytes_ext(const char* filePath, u64* sizePtr);
u8* file_read_bytes(const char* filePath);
i32 file_get_size(const char* filePath);
i32 platform_directory_create(const char* name);
void file_delete(const char* pFilePath);

#endif // IO.h

/*

  #####################################
  #####################################
  Path.h
  #####################################
  #####################################

*/
#ifndef PATH_H
#define PATH_H

typedef struct IElement
{
    i32 AbsolutePathLength;
    i32 NameLength;
    i32 DirLength;
    char* AbsolutePath;
    char* Directory;
    char* Name;
    // NOTE(bies); Extension is part of AbsolutePath
    char* NameWithExtension;
    // NOTE(bies); Extension is part of Filename
    char* Extension;
} IElement;

#define ielement_header(e) ((IElement*) (((char*)e) - sizeof(IElement)))
#define ielement_absolute_length(e) ((e != NULL)? ielement_header(e)->AbsolutePathLength : 0)
#define ielement_name_length(e) ((e != NULL)? ielement_header(e)->NameLength : 0)
#define ielement_directory_length(e) ((e != NULL)? ielement_header(e)->DirLength : 0)
#define ielement_absolute_path(e) ((e != NULL)? ielement_header(e)->AbsolutePath : 0)
#define ielement_directory(e) ((e != NULL)? ielement_header(e)->Directory : 0)
#define ielement_name(e) ((e != NULL)? ielement_header(e)->Name : 0)
#define ielement_name_with_extension(e) ((e != NULL)? ielement_header(e)->NameWithExtension : 0)
#define ielement_extension(e) ((e != NULL)? ielement_header(e)->Extension : 0)

enum Path
{
    PATH_IS_SOMETHING = 0,
    PATH_IS_FILE,
    PATH_IS_DIRECTORY
};

char* ielement(const char* directory, const char* name);
void ielement_free_all();
u8 path(const char* path);
char* path_get_home_directory();
const char* path_get_extension(const char* path);
const char* path_get_name(const char* path);
char* path_get_name_wo_extension(const char* path);
char* path_get_directory(const char* path);
char* path_combine(const char* left, const char* right);
char* path_combine3(const char* left, const char* mid, const char* right);
char* path_combine_directory_and_name(const char* path, char* name);
const char* path_combine_interning(const char* left, const char* right);
i32 path_contains_slash(const char* path, i64 pathLength);
const char* path_get_current_directory();
char* path_get_absolute(char* path);
i32 path_is_file_exist(const char* path);
i32 path_is_directory_exist(const char* path);
char* path_get_filename(const char* path);
const char* path_get_filename_interning(const char* path);
char* path_get_prev_directory(const char* currentDirectory);
const char* path_get_prev_directory_interning(const char* currentDirectory);
i32 path_directory_create(const char* path);
const char** path_directory_get_files(const char* directory);
const char** path_directory_get_directories(const char* directory);
i32 path_is_inside(const char* path, const char* item);

u64 path_get_last_access_time_raw(const char* path);
u64 path_get_last_modification_time_raw(const char* path);
u64 path_get_last_creation_time_raw(const char* path);


#if defined(PLATFORM_LINUX)

#include <assert.h>
#include <unistd.h>
#include <pwd.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#define PATH_SEPARATOR_STRING "/"
#define ROOT_DIRECTORY "/"

#elif defined(PLATFORM_WINDOWS)

#ifndef PATH_SEPARATOR_STRING
#define PATH_SEPARATOR_STRING "/"
#endif

#ifndef ROOT_DIRECTORY
#define ROOT_DIRECTORY "/"
#endif

#else
#error "Platform unsupported"
#endif

#endif //Path.h

/*

  #####################################
  #####################################
  Profiler.h
  #####################################
  #####################################

*/
#ifndef PROFILER_H
#define PROFILER_H

/*

  USAGE(bies):

  TimeState state;
  simple_profiler_start(&state);
  // do smth we want profile
  simple_profiler_end(&state);

  state.Result; <- time in ns
  i64 ms = simple_profiler_get_microseconds(&state);

*/

#define SIMPLE_PROFILER_NS_TO_S(ns)   (ns / (1000 * 1000 * 1000))
#define SIMPLE_PROFILER_NS_TO_MS(ns)  (ns / (1000 * 1000))
#define SIMPLE_PROFILER_NS_TO_MCS(ns) (ns / (1000))

typedef enum ProfilerTimeType
{
    SIMPLE_PROFILER_TIME_NS = 0,
    SIMPLE_PROFILER_TIME_MCS,
    SIMPLE_PROFILER_TIME_MS,
    SIMPLE_PROFILER_TIME_S,
} ProfilerTimeType;

#define _MAKE_U(a, b) a##b
#define MAKE_U(a, b) _MAKE_U(a, b)
#define MAKE_UNIQUE(a) MAKE_U(a##_, __LINE__)

#define profiler_calc(x)                        \
    {                                           \
    TimeState state;                        \
    simple_profiler_start(&state);                 \
    x;                                      \
    simple_profiler_end(&state);                   \
    printf("Time for executing: %s\n", #x);         \
    profiler_print(&state);                 \
    }

#ifdef PLATFORM_LINUX

#include <time.h>


typedef struct SimpleProfiler
{
    struct timespec Start;
    struct timespec End;
    i64 Result;
} SimpleProfiler;

void simple_profiler_start(SimpleProfiler* state);
void simple_profiler_end(SimpleProfiler* state);

#elif defined(PLATFORM_WINDOWS)

// NOTE: check if delta in nanoseconds

typedef struct SimpleProfiler
{
    LARGE_INTEGER Start;
    LARGE_INTEGER End;
    i64 Result;
} SimpleProfiler;

void simple_profiler_start(SimpleProfiler* state);
void simple_profiler_end(SimpleProfiler* state);

#endif

ProfilerTimeType profiler_get_time_type(SimpleProfiler* state);
i64 profiler_get_nanoseconds(SimpleProfiler* state);
i64 simple_profiler_get_microseconds(SimpleProfiler* state);
i64 simple_profiler_get_milliseconds(SimpleProfiler* state);
i64 simple_profiler_get_seconds(SimpleProfiler* state);
f64 simple_profiler_get_microseconds_as_float(SimpleProfiler* state);
f64 simple_profiler_get_milliseconds_as_float(SimpleProfiler* state);
f64 simple_profiler_get_seconds_as_float(SimpleProfiler* state);
void simple_profiler_print(SimpleProfiler* state);
void simple_profiler_print_as_float(SimpleProfiler* state);
char* simple_profiler_get_string(SimpleProfiler* state);
char* simple_profiler_get_string_as_float(SimpleProfiler* state);

#endif // Profiler.h

/*

  #####################################
  #####################################
  SimpleSocket.h
  #####################################
  #####################################

*/

#if defined(PLATFORM_LINUX)
#include <netinet/in.h> // sockaddr_in
#elif defined(PLATFORM_WINDOWS)
//#define WIN32_LEAN_AND_MEAN
#define _WINSOCKAPI_
#include <windows.h>
//#include <winsock2.h>
//#include <ws2tcpip.h>
#endif

/*
  DOCS(typedef): Все данные по сети передаются как big endian,
  так что их нужно htonl() чтобы конвертнуть назад в little endian.
  Данные функции, как и все api-функции работают в стандартной архитектуре,
  то есть little endian
*/
u32 ip_as_integer(u8 ip3, u8 ip2, u8 ip1, u8 ip0);
void ip_as_string(char str[], u32 ip);
char* ip_to_user_string(u32 address);
void ip_print(u32 address);
char* ip_to_string(u32 address);
u32 htonf(f32 f);
f32 ntohf(u32 p);

typedef enum ConnectionProtocolType
{
    ConnectionProtocolType_UDP = 0,
    ConnectionProtocolType_TCP,
} ConnectionProtocolType;


typedef enum IpType
{
    IpType_V4 = 0,
    IpType_V6,
} IpType;

typedef struct Socket
{
    i32 IsAsync;
#if defined(PLATFORM_LINUX)
    i32 Descriptor;
    struct sockaddr_in Address;
    struct sockaddr_in ServerAddress;
#elif defined(PLATFORM_WINDOWS)
    SOCKET Descriptor;
    struct sockaddr_in Address;
    struct sockaddr_in ServerAddress;
#endif
} Socket;

typedef enum SocketType
{
    SocketType_UDP = 0,
    SocketType_TCP,
} SocketType;

typedef struct SocketAddress
{
    u32 Ip;
    u16 Port;
} SocketAddress;

typedef struct SocketSettings
{
    SocketType Type;
    IpType IpType;
    i32 Protocol;

    SocketAddress Address;
    SocketAddress ServerAddress;
} SocketSettings;

Socket socket_new(SocketSettings settings);
i32 socket_is_invalid(Socket* pSocket);
i32 socket_is_valid(Socket* pSocket);

i32 socket_connect(Socket* pSocket);
i32 socket_connect_to(Socket* pSocket, u32 ip, u16 port);
void socket_make_async(Socket* pSocket);
i32 socket_bind(Socket* pSocket);
Socket socket_accept(Socket* pSocket);
i32 socket_listen(Socket* pSocket, i32 clientsCount);
void socket_close(Socket* pSocket);

// DOCS(typedef): TCP Operations
i32 socket_send_ext(i32 descriptor, void* data, u64 size, i32 flags);
i32 socket_send(Socket* pSocket, void* data, u64 size);
i32 socket_recv_ext(i32 descriptor, void* data, u64 size, i32 flags);
i32 socket_recv(Socket* pSocket, void* data, u64 size);
i32 socket_peek(Socket* pSocket, void* data, u64 size);

// DOCS(typedef): UDP Operations
i32 socket_send_to_ext(i32 descriptor, void* data, u64 size, i32 flags, struct sockaddr_in* pAddress);
i32 socket_recv_from_ext(i32 descriptor, void* data, u64 size, i32 flags, struct sockaddr_in* pAddress);
i32 socket_send_to(Socket* pSocket, void* data, u64 size);
i32 socket_recv_from(Socket* pSocket, void* data, u64 size);
i32 socket_peek_from(Socket* pSocket, void* data, u64 size);

// DOCS(typedef): Ip manipulations
u32 socket_parse_ipv4(char* str, u64 length);
u16 socket_parse_port(char* str, u64 length);
i32 socket_parse_ip_port(char* address, u32* pIp, u16* pPort);
struct sockaddr_in socket_address_v4(u32 ip, u16 port);

u32 socket_get_ip();

i32 socket_compare_address(Socket* a, Socket* b);
i32 socket_compare_raw_address(struct sockaddr_in* a, struct sockaddr_in* b);
i32 socket_compare_server_address(Socket* a, Socket* b);

// DOCS: Debug only
void socket_set_debug_mode();
void socket_set_release_mode();

// NOTE: WINDOWS ONLY
void socket_init();
void socket_deinit();

// End of SimpleSocket.h


/*

  ###################################
  ###################################
  SimpleStandardLibrary.c
  ###################################
  ###################################

*/
#if defined(SSL_IMPLEMENTATION)

#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <time.h>

#if defined(PLATFORM_LINUX)
#include <fcntl.h>
#include <sys/time.h>
#elif defined(PLATFORM_WINDOWS)
#include <Windows.h>
#endif

/*

  ###################################
  ###################################
  Environment.c
  ###################################
  ###################################

*/
void
sleep_for(u64 seconds)
{
    u64 waitTo = time(0) + seconds;
    while (time(0) < waitTo);
}

/*

  ###################################
  ###################################
  Core.c
  ###################################
  ###################################

*/

u64
simple_core_utf8_unicode_size(u32* aUnicodes, u64 length)
{
    u64 size = 0;

    u32* ptr = aUnicodes;
    for (u64 i = 0; i < length; ++ i)
    {
    u32 codepoint = *ptr;

    if (codepoint < 0x80)
	size += 1;
    else if (codepoint < 0x800)
	size += 2;
    else if (codepoint < 0x10000)
	size += 3;
    else if (codepoint < 0x110000)
	size += 4;

    ++ptr;
    }

    return size;
}

i64
simple_core_utf8_length(char* pInput)
{
    u64 length = 0;
    char* ptr = (char*) pInput;
    char c = *ptr;
    while (c)
    {
    length += (c & 0xc0) != 0x80;

    ++ptr;
    c = *ptr;
    }

    return length;
}

u64
simple_core_utf8_size(char* pInput)
{
    u64 size = 0;
    char* ptr = (char*) pInput;
    char c = *ptr;

    while (c)
    {
    if ((c >> 7) == 0)
    {
	size += 1;
	ptr  += 1;
	c = *ptr;
    }
    else if (((c & 0xF0) == 0xF0)) // 4 byte
    {
	size += 4;
	ptr  += 4;
	c = *ptr;
    }
    else if ((c & 0xE0) == 0xE0) // 3 byte
    {
	size += 3;
	ptr  += 3;
	c = *ptr;
    }
    else if ((c & 0xC0) == 0xC0) // 2 byte
    {
	size += 2;
	ptr  += 2;
	c = *ptr;
    }
    else
    {
	// crash
	break;
    }

    }

    return size;
}

i32
simple_core_utf8_encode(char* pOutput, u32 codepoint)
{
    /*
      https://www.rapidtables.org/convert/number/binary-to-decimal.html

      starts with
      0) 0 - 1 byte utf8 -> 7 bits long
      1) 110   (0xC0) - 2 byte utf8 -> 11 bits long
      2) 1110  (0xE0) - 3 byte utf8 -> 16 bits long
      3) 11110 (0XF0) - 4 byte utf8 -> 21 bits long
      4) 10 - 2nd, 3rd, 4th byte of 0) - 3)

      2 byte:
      |      5| |      6|
      0xC0 - 192|10 -> 1100 0000|2

      3 byte:
      |      4| |      6| |      6|
      1110 0000 1000 0000 1000 0000

      0xE0 - 224|10 -> 1110 0000|2

    */

    i32 bytesCount = 0;
    if (codepoint < 0x80 /* 1b / 7 bit */)
    {
	*pOutput = codepoint;

	bytesCount = 1;
    }
    else if (codepoint < 0x800 /* 2b / 11 bits */)
    {
	*pOutput = (codepoint >> 6) | 0xC0;
	++pOutput;

	*pOutput = (codepoint & 0x3F) | 0x80;

	bytesCount = 2;
    }
    else if (codepoint < 0x10000 /* 3b / 16 bits */)
    {
	*pOutput = (codepoint >> 12) | 0xE0;
	++pOutput;

	*pOutput = ((codepoint >> 6) & 0x3f) | 0x80;
	++pOutput;

	*pOutput = (codepoint & 0x3F) | 0x80;

	bytesCount = 3;
    }
    else if (codepoint < 0x110000 /* 4b / 21 bits */)
    {
	*pOutput = (codepoint >> 18) | 0XF0;
	++pOutput;

	*pOutput = ((codepoint >> 12) & 0x3F) | 0x80;
	++pOutput;

	*pOutput = ((codepoint >> 6) & 0x3F) | 0x80;
	++pOutput;

	*pOutput = (codepoint & 0x3F) | 0x80;

	bytesCount = 4;
    }

    return bytesCount;
}

u32
simple_core_utf8_decode(char* pInput, i32* pBytes)
{
    u32 codepoint;

    /*
      starts with
      0) 0 - 1 byte utf8 -> 7 bits long
      1) 110 - 2 byte utf8 -> 11 bits long
      2) 1110 - 3 byte utf8 -> 16 bits long
      3) 11110 - 4 byte utf8 -> 21 bits long
      4) 10 - 2nd, 3rd, 4th byte of 0) - 3)

      |      4| |      6| |      6|
      1110 0000 1000 0000 1000 0000

    */

    // defining what's the size

    // DOCS: fb - first byte
    char fb = *pInput;

    if ((fb >> 7) == 0)
    {
	codepoint = fb;

	if (pBytes)
	    *pBytes = 1;
    }
    else if (((fb & 0xF0) == 0xF0)) // 4 byte
    {
	codepoint  = ((pInput[0]) & 0x07) << 18;
	codepoint |= ((pInput[1]) & 0x3F) << 12;
	codepoint |= ((pInput[2]) & 0x3F) << 6;
	codepoint |= ((pInput[3]) & 0x3F);

	if (pBytes)
	    *pBytes = 4;
    }
    else if ((fb & 0xE0) == 0xE0) // 3 byte
    {
	//   |       | |       | |       |
	// 0 xxxx 1001 1010 0000 1001 1001
	// 1 1110 1001 xx10 0000 1001 1001
	// 2 1110 1001 1010 0000 1001 1001
	//
	// r 1111 1111 1111 1111
	//

	codepoint  = ((pInput[0]) & 0x0F) << 12;
	codepoint |= ((pInput[1]) & 0x3F) << 6 ;
	codepoint |= ((pInput[2]) & 0x3F);

	if (pBytes)
	    *pBytes = 3;
    }
    else if ((fb & 0xC0) == 0xC0) // 2 byte
    {
	codepoint = ((pInput[0]) & 0x1F) << 6;
	codepoint |= ((pInput[1]) & 0x3F);

	if (pBytes)
	    *pBytes = 2;
    }

    return codepoint;
}

void
simple_core_utf8_get_unicode_text(i32* pUnicodeText, char* pUtf8Text, i64* pTextLength)
{
    i32 size = simple_core_utf8_size(pUtf8Text);
    i32 unicodeTextLength = simple_core_utf8_length(pUtf8Text);
    //i32* pUnicodeText = (i32*) memory_allocate(unicodeTextLength * sizeof(u32));

    for (i32 i = 0, ind = 0; i < size; ++ind)
    {
	i32 bytesCount = 0;
	u32 unicode = simple_core_utf8_decode(&pUtf8Text[i], &bytesCount);

	if (ind < unicodeTextLength)
	    pUnicodeText[ind] = unicode;

	i += bytesCount;
    }

    if (pTextLength)
	*pTextLength = unicodeTextLength;
}

i64
simple_core_string_hash(char* pStr, i32 length)
{
    i64 hash = 5381;

    char* ptr = (char*) pStr;
    char* end = ptr + length;
    while (ptr != end)
    {
	hash = ((hash << 5) + hash) + *ptr;
	++ptr;
    }

    return hash;
}

i32
simple_core_get_int_digit(i64 intValue)
{
    /*
      I32_MAX == 2147483647, digit == 10
      I64_MAX 9223372036854775807LL, digit == 19 // 9 223 372 036 854 775 807=9e+18
    */

    if (intValue < 0)
	intValue = -intValue;

    if (intValue < 10) return 1;
    if (intValue < 100) return 2;
    if (intValue < 1000) return 3;
    if (intValue < 10000) return 4;
    if (intValue < 100000) return 5;
    if (intValue < 1000000) return 6;
    if (intValue < 10000000) return 7;
    if (intValue < 100000000) return 8;
    if (intValue < 1000000000) return 9;
    if (intValue < 10000000000) return 10;
    if (intValue < 100000000000) return 11;
    if (intValue < 1000000000000) return 12;
    if (intValue < 10000000000000) return 13;
    if (intValue < 100000000000000) return 14;
    if (intValue < 1000000000000000) return 15;
    if (intValue < 10000000000000000) return 16;
    if (intValue < 100000000000000000) return 17;
    if (intValue < 1000000000000000000) return 18;

    return 19;
}

i64
simple_core_int_to_string(char* pBuffer, i64 bufferLength, i64 intValue)
{
    i32 isNumberNegative = ((intValue < 0) ? 1 : 0);
    i64 i,
	rank = simple_core_get_int_digit(intValue) - 1,
	numberLength = rank + isNumberNegative + 1;

    if (isNumberNegative)
    {
	pBuffer[0] = '-';
	intValue = -intValue;
    }

    /*
      DOCS: 12937 -> "12937"; number = 12937; simple_core_get_int_digit(number) == 5;
    */

    for (i = isNumberNegative; i < numberLength; ++i)
    {
	if (i >= bufferLength)
	    break;

	i64 divider = simple_core_int_power(10, rank);
	if (divider == -1)
	    break;
	i32 value = intValue / divider % 10;
	char cvalue = ((char)value) + '0';

	pBuffer[i] = cvalue;
	--rank;
    }

    return numberLength;
}

i64
simple_core_float_to_string_ext(char* pBuffer, i64 bufferLength, f64 floatValue, f64 threshold)
{
    i64   intPart = (i64) floatValue;
    f64 floatPart = floatValue - intPart;

    //GINFO("intPart: %lld\n", intPart);
    //GINFO("floatPart: %f\n", floatPart);

    i64 bytesWritten = simple_core_int_to_string(pBuffer, bufferLength, intPart);
    i64 ind = 0;

    if (threshold == 0)
	goto ParseFloatExtReturnLabel;

    char* pFloatBuffer = pBuffer + bytesWritten;
    pFloatBuffer[ind] = '.';
    ++ind;

    i32 shouldContinue = (floatPart > threshold);
    if (!shouldContinue)
    {
	while (threshold < 1)
	{
	    pFloatBuffer[ind] = '0';
	    ++ind;
	    threshold *= 10;
	}

	goto ParseFloatExtReturnLabel;
    }

    while (shouldContinue)
    {
	if (bytesWritten + ind >= bufferLength)
	{
	    break;
	}

	floatPart *= 10;
	i32 ivalue = (i32) floatPart;
	simple_core_int_to_string(&pFloatBuffer[ind], 1, ivalue);
	++ind;

	floatPart -= ivalue;
	threshold *= 10;

	shouldContinue = (floatPart > threshold);
    }

ParseFloatExtReturnLabel:
    return bytesWritten + ind;
}

i64
simple_core_float_to_string(char* pBuffer, i64 bufferLength, f64 floatValue)
{
    return simple_core_float_to_string_ext(pBuffer, bufferLength, floatValue, 1E-12);
}

// simple_core_int_power(10, 6)
i64
simple_core_int_power(i64 intValue, i32 powerValue)
{
    if (powerValue == 0)
	return 1;
    else if (powerValue < 0)
	return -1;

    i64 result = intValue;
    --powerValue;
    while (powerValue)
    {
	result *= intValue;
	--powerValue;
    }

    return result;
}

// DOCS: Parse from string to int.
i64
simple_core_parse_int(char* pBuffer, i64 bufferLength)
{
    i64 result = 0;
    i64 lastInd = bufferLength - 1;
    i64 multiplier = 1;
    char* ptr = pBuffer + lastInd;

    for (i64 i = lastInd; i >= 0; --i)
    {
	char c = *ptr;
	if (c >= '0' && c <= '9')
	{
	    i32 intValue = (i32) (c - '0');
	    result = intValue * multiplier + result;
	    multiplier *= 10;
	}
	else
	{
	    vguard(0);
	}

	--ptr;
    }

    return result;
}

// DOCS: Parse from string to float.
f64
simple_core_parse_float(char* pBuffer, i64 bufferLength)
{
    f64 result = 0;
    i64 lastInd = bufferLength - 1;
    i64 multiplier = 1;
    i32 flag = 0;
    i64 ind = 0;
    char* ptr = pBuffer + lastInd;

    for (i64 i = lastInd; i >= 0; --i)
    {
	char c = *ptr;
	i32 intValue = (i32) (c - '0');

	if (c == '.')
	{
	    flag = 1;
	    ind = i;
	    continue;
	}

	if (flag == 1)
	{
	    if (c >= '0' && c <= '9')
	    {
		result += intValue * 10;
		multiplier *= 10;
	    }
	    else
	    {
		vguard(0);
	    }
	}

	--ptr;
    }

    ptr = pBuffer + lastInd;
    for (i64 i = lastInd; i >= 0; --i)
    {
	char c = *ptr;
	if (c == '.')
	    break;

	if (c < '0' || c > '9')
	    vguard(0);

	i32 intValue = (i32) (c - '0');
	multiplier = 1.0 / simple_core_int_power(10, i-ind);
	result += intValue * multiplier;

	--ptr;
    }

    return result;
}



f64
simple_core_float_npow(i32 exponent)
{
    f64 v = 1.0;
    while (exponent > 0)
    {
	v /= 10;
	--exponent;
    }
    return v;
}

SimpleCoreIntParsed
simple_core_parse_int_while_int(char* pInput)
{
    char* iterPtr = pInput;
    char ic = *iterPtr;
    while ((ic >= '0' && ic <= '9'))
    {
	++iterPtr;
	ic = *iterPtr;
    }

    i64 intValueLength = (i64) (u64) (iterPtr - pInput);
    if (intValueLength <= 0)
    {
	return (SimpleCoreIntParsed) {
	    .IntValue = 0,
	    .pInput = pInput,
	    .pOutput = NULL,
	};
    }

    i64 intValue = simple_core_parse_int(pInput, intValueLength);

    SimpleCoreIntParsed intParsed = {
	.IntValue = intValue,
	.pInput = pInput,
	.pOutput = iterPtr,
    };

    return intParsed;
}


// DOCS: Tests
void
simple_core_get_int_digit_test_unit(i64 intValue, i32 expected)
{
    i32 digit = simple_core_get_int_digit(intValue);
    if (digit == expected)
	GSUCCESS("intValue(%ld) with expected digit count = %d\n", intValue, digit);
    else
	GERROR("intValue(%ld) with notexpected digit count = %d\n", intValue, digit);
}

void
simple_core_get_int_digit_test()
{
    simple_core_get_int_digit_test_unit(1997, 4);
    simple_core_get_int_digit_test_unit(11, 2);
    simple_core_get_int_digit_test_unit(11000, 5);
}

void
simple_core_int_power_test_unit(i64 intValue, i32 powerValue, i64 expected)
{
    i64 result = simple_core_int_power(intValue, powerValue);
    if (result == expected)
	GSUCCESS("%lld^%d as expected = %lld\n", intValue, powerValue, result);
    else
	GERROR("%lld^%d NOT expected = %lld (right = %lld)\n", intValue, powerValue, result, expected);
}

void
simple_core_int_power_test()
{
    simple_core_int_power_test_unit(10, 3, 1000);
    simple_core_int_power_test_unit(10, 5, 100000);
    simple_core_int_power_test_unit(10, 6, 1000000);
    simple_core_int_power_test_unit(3, 3, 27);
    simple_core_int_power_test_unit(4, 4, 256);
    simple_core_int_power_test_unit(5, 5, 3125);
    simple_core_int_power_test_unit(6, 6, 46656);
    simple_core_int_power_test_unit(7, 7, 823543);
}

void
simple_core_int_to_string_test_unit(i64 intValue, const char* pExpectedValue)
{
    char buf[256] = {};
    simple_core_int_to_string(buf, 256, intValue);

    if (string_compare(buf, pExpectedValue))
	GSUCCESS("intValue(%lld) -> \"%s\" as expected\n", intValue, buf);
    else
	GERROR("intValue(%lld) -> \"%s\" NOT expected right = %s\n", intValue, buf, pExpectedValue);
}

void
simple_core_int_to_string_test()
{
    simple_core_int_to_string_test_unit(1024, "1024");
    simple_core_int_to_string_test_unit(107724, "107724");
    simple_core_int_to_string_test_unit(9, "9");
    simple_core_int_to_string_test_unit(0, "0");
    simple_core_int_to_string_test_unit(99999, "99999");

    simple_core_int_to_string_test_unit(-1, "-1");
    simple_core_int_to_string_test_unit(-125, "-125");
}

void
simple_core_float_to_string_test_unit(f64 floatValue, const char* pExpected, f64 threshold)
{
    char buf[128] = {};
    simple_core_float_to_string_ext(buf, 128, floatValue, threshold);

    if (string_compare(buf, pExpected))
	GSUCCESS("FloatValue(%f) parsed as expected %s\n", floatValue, pExpected);
    else
	GERROR("FloatValue(%f) NOT expected %s CORRECT IS %s\n", floatValue, buf, pExpected);
}

void
simple_core_float_to_string_test()
{
    f64 fv = 1234.5678001234569;
    simple_core_float_to_string_test_unit(fv, "1234.567800123456", 1E-12);
    simple_core_float_to_string_test_unit(1234.5678, "1234.5678", 1E-4);
    simple_core_float_to_string_test_unit(1234.567800, "1234.56", 1E-2);
}

// WIP
void
simple_core_parse_float_test_unit(const char* pValue, f64 expectedValue)
{
    f64 val = simple_core_parse_float(pValue, string_length(pValue));
    if ((val - expectedValue) > 0.000001)
    {
	GSUCCESS("AS EXPECTED %f\n", val);
    }
    else
    {
	GERROR("NOT EXPECTED %f (right %f)\n", val, expectedValue);
    }
}

void
simple_core_parse_float_test()
{
    simple_core_parse_float_test_unit("3.14", 3.14);
}

// Core.c


/*

  ###################################
  ###################################
  Slog.c
  ###################################
  ###################################

*/

static SlogConfig gSlogConfig;


/*
  TODO/WIP
  DOCS:
  * Create array<structure of logs>
  * Log into console or file depend on config
  * Build config
*/

/*
  DOCS: internal functions
*/

force_inline i32
_slog_is_argument_special(char c)
{
    switch (c)
    {
	// DOCS: Basic types
    case 'c': // DOCS: Character
    case 's': // DOCS: String
    case 'd': // DOCS: Integer 32 bit
    case 'b': // DOCS: Byte?
    case 'f': // DOCS: Float 32/64 bit
    case 'p': // DOCS: Pointer
	// DOCS: Math types
    case 'v': // DOCS: Vector
    case 'm': // DOCS: Matrix ???
	return 1;

    default:
	return 0;
    }
}

f64
_slog_parse_vector_value(va_list vaList)
{
    f64 value;
#if defined(FLOAT_32_BIT)
    value = (f64)(f32) va_arg(vaList, f64);
#else
    value = va_arg(vaList, f64);
#endif // !defined(FLOAT_32_BIT)

    return value;
}

f64
_slog_accuracy_to_float(char* ptr)
{
    const f32 c_default_accuracy = 1E-2;

    char c = *ptr;
    if (c != '0')
	return c_default_accuracy;

    while ((c >= '0' && c <= '9') || (c == '.'))
    {
	++ptr;
	c = *ptr;
    }

    return 0;
}

const char*
slog_level_to_string(SlogLevel level)
{
    switch (level)
    {
    case SlogLevel_Info: return "[INFO]";
    case SlogLevel_Success: return "[SUCCESS]";
    case SlogLevel_Warn: return "[WARN]";
    case SlogLevel_Error: return "[ERROR]";
    case SlogLevel_Debug: return "[DEBUG]";
    }

    return "";
}

const char*
slog_level_to_color(SlogLevel type)
{
    switch (type)
    {
    case SlogLevel_Info: return "\x1B[95m";
    case SlogLevel_Success: return "\x1B[92m";
    case SlogLevel_Warn: return "\x1B[93m";
    case SlogLevel_Error: return "\x1B[31m";
    case SlogLevel_Debug: return "\x1B[37m";

    default: return "\x1B[33m";
    }
}

void
slog_back_terminal(SlogFormat format, SlogItem slogItem)
{
    if (BitAnd(format, SlogFormat_Level))
    {
	const char* pClr = slog_level_to_color(slogItem.Level);
	printf("%s", pClr);

	const char* pLevelStr = slog_level_to_string(slogItem.Level);
	printf("%s\033[0m ", pLevelStr);
    }

    if (BitAnd(format, SlogFormat_File))
    {
	printf("%s ", slogItem.pFile);
    }
    else if (BitAnd(format, SlogFormat_File_Short))
    {
	printf("%s ", path_get_name(slogItem.pFile));
    }

    if (BitAnd(format, SlogFormat_Function))
    {
	if (BitAnd(format, SlogFormat_Line))
	{
	    printf("%s:%d ", slogItem.pFunction, slogItem.Line);
	}
	else
	{
	    printf("%s ", slogItem.Line);
	}
    }
    else if (BitAnd(format, SlogFormat_Line))
    {
	printf("l:%d ", slogItem.Line);
    }

    if (BitAnd(format, SlogFormat_Time))
    {
	const char* pTimeStr = simple_time_to_time_string(slogItem.DateTime);
	printf("%s ", pTimeStr);
    }

    printf("%s", slogItem.pSbMessage);
}

void
slog_back_file(SlogFormat format, SlogItem slogItem)
{
    const char* pSlogsDirectory = "Slogs";

    // note: should we check it :?
    if (!path_is_directory_exist(pSlogsDirectory))
    {
	path_directory_create(pSlogsDirectory);
    }

    SimpleTime time = simple_time_new();
    char buf[32] = {};
    snprintf(buf, 32, "slog_%2d.%2d.%4d", time.Day, time.Month, time.Year);

    char* pFilePath = path_combine(pSlogsDirectory, buf);
    char* sb = NULL;

    if (BitAnd(format, SlogFormat_Level))
    {
	const char* pLevelStr = slog_level_to_string(slogItem.Level);
	string_builder_appendf(sb, "%s ", pLevelStr);
    }

    if (BitAnd(format, SlogFormat_File))
    {
	string_builder_appendf(sb, "%s ", slogItem.pFile);
    }
    else if (BitAnd(format, SlogFormat_File_Short))
    {
	string_builder_appendf(sb, "%s ", path_get_name(slogItem.pFile));
    }

    if (BitAnd(format, SlogFormat_Function))
    {
	if (BitAnd(format, SlogFormat_Line))
	{
	    string_builder_appendf(sb, "%s:%d ", slogItem.pFunction, slogItem.Line);
	}
	else
	{
	    string_builder_appendf(sb, "%s ", slogItem.Line);
	}
    }
    else if (BitAnd(format, SlogFormat_Line))
    {
	string_builder_appendf(sb, "l:%d ", slogItem.Line);
    }

    if (BitAnd(format, SlogFormat_Time))
    {
	const char* pTimeStr = simple_time_to_time_string(slogItem.DateTime);
	string_builder_appendf(sb, "%s ", pTimeStr);
    }

    string_builder_appends(sb, slogItem.pSbMessage);

    i64 sbCount = string_builder_count(sb);
    file_append_string(pFilePath, sb, sbCount);

    string_builder_free(sb);
    memory_free(pFilePath);
}

void
slog_base(SlogLevel level, const char* pFile, i32 line, const char* pFunction, const char* pFormat, ...)
{
    /*
      todo:
      * %0.2f
      * %2d

      */

    typedef enum FormatState {
	FormatState_Char = 0,
	FormatState_SpecialChar = 1,
	FormatState_LongSpecialChar = 2,
	FormatState_Offset = 3,
	FormatState_Accuracy = 4,
    } FormatState;

#if 1
    i32 argumentsCount = 0;
    FormatState state = FormatState_Char;

    { // DOCS: Calc arguments in format
	char pc = '0';
	for (char* ptr = (char*)pFormat; *ptr != '\0'; ++ptr)
	{
	    char c = *ptr;

	    if (c == '%' && pc != '%')
	    {
		state = FormatState_SpecialChar;
	    }
	    else if ((state == FormatState_SpecialChar)
		     && _slog_is_argument_special(c))
	    {
		state = FormatState_Char;
		++argumentsCount;
	    }
	    else
	    {
		state = FormatState_SpecialChar;
	    }

	    pc = c;
	}

    }

    // DOCS: Parsing string from format

    char* sb = NULL;
    char* ptr = (char*) pFormat;
    va_list vaList;
    va_start(vaList, pFormat);
    state = FormatState_Char;

    i64 offsetValue = 0;
    i64 accuracyValue = 0;

    char pc = '0';
    char c = *ptr;
    while (c != '\0')
    {
	switch (c)
	{
	case '%':
	{
	    if (pc != '%')
	    {
		state = FormatState_SpecialChar;
		accuracyValue = 0;
		offsetValue = 0;
	    }

	    break;
	}

	case '.':
	{
	    break;
	}

	case '0': case '1': case '2':
	case '3': case '4': case '5':
	case '6': case '7': case '8': case '9':
	{
	    // parse everything here

	    if (state != FormatState_SpecialChar)
	    {
		string_builder_appendc(sb, c);
		break;
	    }

	    SimpleCoreIntParsed offsetParsed = simple_core_parse_int_while_int(ptr);
	    offsetValue = offsetParsed.IntValue;

	    if (offsetParsed.pOutput != NULL)
	    {
		char* tempPtr = offsetParsed.pOutput + 1;
		if (simple_core_is_char_digit(*tempPtr))
		{
		    SimpleCoreIntParsed accuracyParsed =
			simple_core_parse_int_while_int(tempPtr);
		    accuracyValue = accuracyParsed.IntValue;
		    ptr = accuracyParsed.pOutput - 1;
		}
	    }
	    else
	    {
		ptr = offsetParsed.pOutput - 1;
	    }

	    //printf("Offset: %lld Accuracy: %lld \n", offsetValue, accuracyValue);
	    break;
	}

	case 'c':
	{
	    if (state == 1)
	    {
		char charArg = (char) va_arg(vaList, i32);
		string_builder_appendc(sb, c);
		state = 0;
	    }
	    else
		string_builder_appendc(sb, c);

	    break;
	}
	case 's':
	{
	    if (state == 1)
	    {
		const char* pStringArg = va_arg(vaList, const char *);
		if (pStringArg == NULL)
		{
		    string_builder_appends(sb, "<NULL>");
		}
		else
		{
		    string_builder_appends(sb, (char*) pStringArg);
		}

		state = 0;
	    }
	    else
		string_builder_appendc(sb, c);

	    break;
	}
	case 'l':
	{
	    if (state == FormatState_SpecialChar)
	    {
		state = FormatState_LongSpecialChar;
	    }
	    else
		string_builder_appendc(sb, c);

	    break;
	}
	case 'd':
	{
	    i64 intValue = 0;

	    if (state == FormatState_SpecialChar)
	    {
		intValue = va_arg(vaList, i32);
		state = 0;
	    }
	    else if (state == FormatState_LongSpecialChar)
	    {
		intValue = va_arg(vaList, i64);
		state = 0;
	    }
	    else
	    {
		string_builder_appendc(sb, c);
		break;
	    }

	    char str[64] = {};
	    i32 length = simple_core_int_to_string(str, 64, intValue);

	    if (offsetValue >= accuracyValue)
	    {
		offsetValue -= length;
		while (offsetValue > 0)
		{
		    string_builder_appendc(sb, ' ');
		    --offsetValue;
		}
	    }
	    else if (offsetValue < accuracyValue)
	    {
		accuracyValue -= length;
		while (accuracyValue > 0)
		{
		    string_builder_appendc(sb, '0');
		    --accuracyValue;
		}
	    }

	    string_builder_appends(sb, str);

	    break;
	}
	case 'b':
	{
	    if (state == 1)
	    {
		u8 byteArg = (u8) va_arg(vaList, int);
		char str[4] = {};
		simple_core_int_to_string(str, 64, byteArg);
		string_builder_appends(sb, str);

		state = 0;
	    }
	    else
		string_builder_appendc(sb, c);

	    break;
	}
	case 'f':
	{ // WIP
	    if (state != FormatState_SpecialChar)
	    {
		string_builder_appendc(sb, c);
		break;
	    }

	    state = 0;

	    f64 f64Number = va_arg(vaList, f64);
	    char str[64] = {};
	    //sb_string_f64_to_string(str, f64Number);
	    //string_builder_appends(sb, str);

	    f64 threshold = simple_core_float_npow(accuracyValue);
	    i64 length = simple_core_float_to_string_ext(str, 64, f64Number, threshold);

	    if (offsetValue >= accuracyValue)
	    {
		offsetValue -= length;
		while (offsetValue > 0)
		{
		    string_builder_appendc(sb, ' ');
		    --offsetValue;
		}
	    }
	    else if (offsetValue < accuracyValue)
	    {
		accuracyValue -= length;
		while (accuracyValue > 0)
		{
		    string_builder_appendc(sb, '0');
		    --accuracyValue;
		}
	    }

	    string_builder_appends(sb, str);

	    break;
	}

	case 'v':
	{
	    if (state == 1)
	    {
		++ptr;
		c = *ptr;
		state = 0;

		switch (c)
		{
		case '2':
		{
		    v2 v = va_arg(vaList, v2);

		    f32 accuracy = 0.001;

		    string_builder_appends(sb, "{");
		    {
			char buf[64] = {};
			simple_core_float_to_string_ext(buf, 64, v.X, accuracy);
			string_builder_appends(sb, buf);
		    }

		    string_builder_appends(sb, ", ");
		    {
			char buf[64] = {};
			simple_core_float_to_string_ext(buf, 64, v.Y, accuracy);
			string_builder_appends(sb, buf);
		    }
		    string_builder_appends(sb, "}");

		    break;
		}

		case '3':
		{
		    v3 v = va_arg(vaList, v3);

		    f32 accuracy = 0.001;

		    string_builder_appends(sb, "{");
		    {
			char buf[64] = {};
			simple_core_float_to_string_ext(buf, 64, v.X, accuracy);
			string_builder_appends(sb, buf);
		    }

		    string_builder_appends(sb, ", ");
		    {
			char buf[64] = {};
			simple_core_float_to_string_ext(buf, 64, v.Y, accuracy);
			string_builder_appends(sb, buf);
		    }

		    string_builder_appends(sb, ", ");
		    {
			char buf[64] = {};
			simple_core_float_to_string_ext(buf, 64, v.Z, accuracy);
			string_builder_appends(sb, buf);
		    }

		    string_builder_appends(sb, "}");

		    break;
		}
		case '4':
		{
		    v4 v = va_arg(vaList, v4);

		    f32 accuracy = 0.001;

		    string_builder_appends(sb, "{");
		    {
			char buf[64] = {};
			simple_core_float_to_string_ext(buf, 64, v.X, accuracy);
			string_builder_appends(sb, buf);
		    }

		    string_builder_appends(sb, ", ");
		    {
			char buf[64] = {};
			simple_core_float_to_string_ext(buf, 64, v.Y, accuracy);
			string_builder_appends(sb, buf);
		    }

		    string_builder_appends(sb, ", ");
		    {
			char buf[64] = {};
			simple_core_float_to_string_ext(buf, 64, v.Z, accuracy);
			string_builder_appends(sb, buf);
		    }

		    string_builder_appends(sb, ", ");
		    {
			char buf[64] = {};
			simple_core_float_to_string_ext(buf, 64, v.W, accuracy);
			string_builder_appends(sb, buf);
		    }

		    string_builder_appends(sb, "}");

		    break;
		}
		} // c == '2', in str "%v2 format"

	    }
	    else
		string_builder_appendc(sb, c);

	    break;
	}

	default:
	{
	    string_builder_appendc(sb, c);
	    state = 0;
	    break;
	}

	} // DOCS: switch (c)

	pc = c;
	++ptr;
	c = *ptr;
    }

    i64 backendsCount = sva_count(gSlogConfig.aBackends);
    for (i64 i = 0; i < backendsCount; ++i)
    {
	SlogBackend slogBackend = gSlogConfig.aBackends[i];

	SlogItem item = {
	    .Level = level,
	    .pFile = pFile,
	    .Line = line,
	    .pSbMessage = sb,
	    .pFunction = pFunction,
	    .DateTime = simple_time_new()
	};

	slogBackend.Action(slogBackend.Format, item);
    }

    //GSUCCESS("log: %s\n", sb);
    string_builder_free(sb);

    va_end(vaList);
#endif// 0
}

void
slog_auto_tests()
{
    SlogBackend terminalBackend = {
	.Format = SlogFormat_Level | SlogFormat_Time | SlogFormat_File_Short | SlogFormat_Line | SlogFormat_Function,
	.Action = slog_back_terminal,
    };
    SlogBackend* aBackends = NULL;
    sva_add(aBackends, terminalBackend);

    SlogBackend fileBackend = {
	.Format = SlogFormat_Level | SlogFormat_Time | SlogFormat_File_Short | SlogFormat_Line | SlogFormat_Function,
	.Action = slog_back_file,
    };
    sva_add(aBackends, fileBackend);

    SlogConfig config = {
	.aBackends = aBackends,
    };

    /* SlogFormat_Level = 1 << 1, */
    /* SlogFormat_File = 1 << 2, */
    /* SlogFormat_Function = 1 << 3, */
    /* SlogFormat_Line = 1 << 4, */
    /* SlogFormat_Time = 1 << 5, */

    slog_init(config);

    /*
      %10d -
      означает что output будет размером минимум 10 значений,
      (но при этом может привышать указанные значения)
      formatValue: 9, значимая цифра только одна, поэтому
      остальное заполняется пустотой.

      %0.10d - добить output нулями до размера 10 символов.
    */
    // printf("\n\nPRINTF MECH: %.d\n\n", 9991);
    // printf("\n\nPRINTF MECH: %0.50d\n\n", 9);

    slog_info("Hello, info world!\n");
    slog_debug("Hello, debug world!\n");
    slog_warn("Hello, warn world!\n");
    slog_error("Hello, error world!\n");

    //slog_base(SlogLevel_Info, __FILE__, __LINE__, __FUNCTION__, "%s P = %f Pos: %v2 end.", "Hello, world!", 3.14f, v2_new(100, 250));

    // DOCS: print single i32
    slog_base(SlogLevel_Info, __FILE__, __LINE__, __FUNCTION__, "I64: %0.5d\n", 120);
    slog_base(SlogLevel_Info, __FILE__, __LINE__, __FUNCTION__ , "I64: %0.1d\n", 120);
    slog_base(SlogLevel_Info, __FILE__, __LINE__, __FUNCTION__, "I64: %0.0d\n", 10120);
    slog_base(SlogLevel_Info, __FILE__, __LINE__, __FUNCTION__, "I64: %0.d\n", 20120);
    slog_base(SlogLevel_Info, __FILE__, __LINE__, __FUNCTION__, "I64: %0.0d\n", 30120);

    slog_base(SlogLevel_Info, __FILE__, __LINE__, __FUNCTION__, "F64: %0.4f\n", 3.14f);
    // BUG
    slog_base(SlogLevel_Info, __FILE__, __LINE__, __FUNCTION__, "F64: %4.0f\n", 3.14f);

    // DOCS: print single i32
    //slog_base(SlogLevel_Info, __FILE__, __LINE__, __FUNCTION__, "%0.5d", 120);

    printf("\n\n\n");
    GSUCCESS("Everything is goood!\n");
    printf("\n\n\n");
    vassert_break();
}

/*

  ###################################
  ###################################
  Logger.c
  ###################################
  ###################################

*/

//TODO(typedef): Replace printf with more low level func

typedef struct SimpleLogSettings
{
    LoggerType Type;
    LoggerFlags Flags;
    const char* Path;

    SimpleLogItem* aLogItems;
} SimpleLogSettings;


static SimpleLogSettings gLoggerSettings = {
    .Type = LoggerType_Terminal,
    .Flags = LoggerFlags_Full,
    .Path = "default.log"
};


/*
  DOCS: Public api
*/


void
slog_init(SlogConfig config)
{
    gSlogConfig = config;
}

void
slog_deinit()
{
    sva_free(gSlogConfig.aBackends);
}

const char*
simple_log_type_to_color(SimpleLogType type)
{
    switch (type)
    {
    case SimpleLogType_Log: return "\x1B[97m";
    case SimpleLogType_Debug: return "\x1B[37m";
    case SimpleLogType_Sucess: return "\x1B[92m";
    case SimpleLogType_Info: return "\x1B[95m";
    case SimpleLogType_Warn: return "\x1B[93m";
    case SimpleLogType_Error: return "\x1B[31m";
    default: return "\x1B[33m";
    }
}

void
simple_log_output(SimpleLogType type, const char* pFormat, const char* pFile, const char* pFunc, i32 line, ...)
{
    vassert(gLoggerSettings.Flags != LoggerFlags_None && "Logger flags should be set to some value!");

    // todo: continue development
    SimpleLogItem item = {
	.Type = type,
	.pFile = pFile,
	.pFunc = pFunc,
	.Line = line,
	.Time = simple_time_new(),
    };

    array_push(gLoggerSettings.aLogItems, item);

    FILE* pOutputHandle;

    typedef int (*WriteToLogDelegate)(FILE* pHandle, const char* format, ...);
    WriteToLogDelegate write_to_log = fprintf;
    static char* sb = NULL;

    if (gLoggerSettings.Type == LoggerType_Terminal)
    {
	pOutputHandle = stdout;
    }
    else if (gLoggerSettings.Type == LoggerType_File)
    {
	pOutputHandle = fopen(gLoggerSettings.Path, "a+");
    }
    else
    {
	GERROR("No impl for file logger for now!\n");
    }

    if (gLoggerSettings.Flags & LoggerFlags_PrintType)
    {
	if (gLoggerSettings.Type == LoggerType_Terminal)
	{
	    const char* pColor = simple_log_type_to_color(type);
	    string_builder_appends(sb, pColor);
	}

	string_builder_appendf(sb, "%s ", simple_log_type_to_string(type));

	if (gLoggerSettings.Type == LoggerType_Terminal)
	{
	    string_builder_appends(sb, "\033[0m");
	}
    }

    if (gLoggerSettings.Flags & LoggerFlags_PrintFile)
    {
	string_builder_appendf(sb, "file: %s", pFile);
    }
    else if (gLoggerSettings.Flags & LoggerFlags_PrintShortFile)
    {
	string_builder_appendf(sb, "f:%s", pFile);
    }

    if (gLoggerSettings.Flags & LoggerFlags_PrintFunc)
    {
	if (gLoggerSettings.Flags & LoggerFlags_PrintFile)
	    string_builder_appends(sb, ", ");
	string_builder_appendf(sb, "call: %s", pFunc);
    }

    if (gLoggerSettings.Flags & LoggerFlags_PrintLine)
    {
	if (gLoggerSettings.Flags & LoggerFlags_PrintFunc)
	{
	    string_builder_appends(sb, ", ");
	}

	string_builder_appendf(sb, "line: %d ", line);
    }
    else if (gLoggerSettings.Flags & LoggerFlags_PrintShortLine)
    {
	if (gLoggerSettings.Flags & LoggerFlags_PrintFunc)
	{
	    string_builder_appends(sb, ", ");
	}

	string_builder_appendf(sb, "l:%d ", line);
    }

    //  LoggerFlags_PrintTime     = 1 << 5,
    // LoggerFlags_PrintShortTime = 1 << 6,
    if (gLoggerSettings.Flags & LoggerFlags_PrintTime)
    {
	SimpleTime time;
	simple_time_now(&time);

	char timeBuf[256] = {};
	snprintf(timeBuf, 256, "time: %0.2d:%0.2d:%0.2d %0.2d.%0.2d.%0.4d ",
		 time.Hours, time.Minutes, time.Seconds,
		 time.Day, time.Month, time.Year);

	string_builder_appends(sb, timeBuf);
    }
    else if (gLoggerSettings.Flags & LoggerFlags_PrintShortTime)
    {
	SimpleTime time;
	simple_time_now(&time);

	char timeBuf[256] = {};
	snprintf(timeBuf, 256, "time: %0.2d:%0.2d:%0.2d ",
		 time.Hours, time.Minutes, time.Seconds);

	string_builder_appends(sb, timeBuf);
    }

    va_list variadicList;
    va_start(variadicList, line);

    if (gLoggerSettings.Type == LoggerType_Terminal)
    {
	printf("%s", sb);
	vprintf(pFormat, variadicList);
    }
    else if (gLoggerSettings.Type == LoggerType_File)
    {
	vfprintf(pOutputHandle, pFormat, variadicList);

	fclose(pOutputHandle);
    }

    va_end(variadicList);

    string_builder_clear(sb);
}


void
logger_init()
{
    if (path_is_file_exist(gLoggerSettings.Path))
    {
    remove(gLoggerSettings.Path);
    }
}

void
logger_to_terminal()
{
    gLoggerSettings.Type = LoggerType_Terminal;
}
void
logger_to_file()
{
    gLoggerSettings.Type = LoggerType_File;
}

void
logger_output(const char* levelMessage, const char* format, const char* file, const char* pFunc, i32 line, ...)
{
    FILE* pOutputHandle;

    typedef int (*WriteToLogDelegate)(FILE* pHandle, const char* format, ...);
    WriteToLogDelegate write_to_log = fprintf;
    char cleanedBuf[64] = {};
    static char* sb = NULL;

    if (gLoggerSettings.Type == LoggerType_Terminal)
    {
    pOutputHandle = stdout;
    }
    else if (gLoggerSettings.Type == LoggerType_File)
    {
    pOutputHandle = fopen(gLoggerSettings.Path, "a+");

    // Clean message from color codes
    char* ptr = (char*) levelMessage;
    while (*ptr != 0)
    {
	if (*ptr == '[' && char_is_upper(*(ptr + 1)))
	{
	char* oth = ptr;
	while (*oth != ']' && *oth != 0)
	    ++oth;
	u64 cleanLength = (u64)(oth - ptr + 1);
	memcpy(cleanedBuf, ptr, cleanLength);
	break;
	}

	++ptr;
    }

    levelMessage = (const char*) cleanedBuf;

    }
    else
    {
    GERROR("No impl for file logger for now!\n");
    }


    vassert(gLoggerSettings.Flags != LoggerFlags_None && "Logger flags should be set to some value!");

    if (gLoggerSettings.Flags & LoggerFlags_PrintType)
    {
    string_builder_appendf(sb, "%s ", levelMessage);
    }

    i32 isPrintFile = gLoggerSettings.Flags & LoggerFlags_PrintFile;
    if (isPrintFile)
    {
    string_builder_appendf(sb, "file: %s", file);
    }

    i32 isPrintShortFile = gLoggerSettings.Flags & LoggerFlags_PrintShortFile;
    if (isPrintShortFile)
    {
    string_builder_appendf(sb, "f:%s", file);
    }

    string_builder_appendf(sb, " func: %s", pFunc);

    if (gLoggerSettings.Flags & LoggerFlags_PrintLine)
    {
    if (isPrintFile || isPrintShortFile)
    {
	string_builder_appends(sb, ", ");
    }

    string_builder_appendf(sb, "line: %d ", line);
    }

    if (gLoggerSettings.Flags & LoggerFlags_PrintShortLine)
    {
    if (isPrintFile || isPrintShortFile)
    {
	string_builder_appends(sb, ", ");
    }

    string_builder_appendf(sb, "l:%d ", line);
    }

    //  LoggerFlags_PrintTime     = 1 << 5,
    // LoggerFlags_PrintShortTime = 1 << 6,
    if (gLoggerSettings.Flags & LoggerFlags_PrintTime)
    {
    SimpleTime time;
    simple_time_now(&time);

    char timeBuf[256] = {};
    snprintf(timeBuf, 256, "time: %0.2d:%0.2d:%0.2d %0.2d.%0.2d.%0.4d ",
	 time.Hours, time.Minutes, time.Seconds,
	 time.Day, time.Month, time.Year);

    string_builder_appends(sb, timeBuf);
    }
    else if (gLoggerSettings.Flags & LoggerFlags_PrintShortTime)
    {
    SimpleTime time;
    simple_time_now(&time);

    char timeBuf[256] = {};
    snprintf(timeBuf, 256, "time: %0.2d:%0.2d:%0.2d ",
	 time.Hours, time.Minutes, time.Seconds);

    string_builder_appends(sb, timeBuf);
    }

    va_list variadicList;
    va_start(variadicList, line);

    if (gLoggerSettings.Type == LoggerType_Terminal)
    {
    printf("%s", sb);
    vprintf(format, variadicList);
    }
    else if (gLoggerSettings.Type == LoggerType_File)
    {
    vfprintf(pOutputHandle, format, variadicList);

    fclose(pOutputHandle);
    }

    va_end(variadicList);

    string_builder_clear(sb);
}

void
logger_set_terminal()
{
    gLoggerSettings.Type = LoggerType_Terminal;
}

void
logger_set_file()
{
    gLoggerSettings.Type = LoggerType_File;
}

void
logger_set_flags(LoggerFlags flags)
{
    gLoggerSettings.Flags = flags;
}

LoggerFlags
logger_get_flags()
{
    return gLoggerSettings.Flags;
}

#if 0

void
logger_set_short()
{
    LogOutputType* type = logger_get_type();

    LogOutputType value = 0;
    value |= LogOutputType_Long;
    value &= ~LogOutputType_Short;

    *type = LogOutputType_Short;
}

void
logger_set_long()
{
    LogOutputType* type = logger_get_type();

    LogOutputType value = 0;
    value |= LogOutputType_Long;
    value &= ~LogOutputType_Short;

    *type = value;
}

LogOutputType*
logger_get_type()
{
    static LogOutputType logOutputType = LogOutputType_Terminal;
    return &logOutputType;
}
#endif

/*

  ###################################
  ###################################
  Time.c
  ###################################
  ###################################

*/

#include <time.h>


SimpleTime
simple_time_new()
{
    SimpleTime time = {};
    simple_time_now(&time);
    return time;
}

void
simple_time_now(SimpleTime* pTime)
{
    time_t rawtime = time(NULL);

    if (rawtime == -1)
    {
    return;
    }

    /* struct tm { */
/*     int tm_sec;         /\* seconds *\/ */
/*     int tm_min;         /\* minutes *\/ */
/*     int tm_hour;        /\* hours *\/ */

/*     int tm_mday;        /\* day of the month *\/ */
/*     int tm_mon;         /\* month *\/ */
/*     int tm_year;        /\* year *\/ */
/*     int tm_wday;        /\* day of the week *\/ */
/*     int tm_yday;        /\* day in the year *\/ */
/*     int tm_isdst;       /\* daylight saving time *\/ */
/* }; */

    struct tm* ptr = localtime(&rawtime);

    pTime->DayOfTheWeek = ptr->tm_wday;

    pTime->Seconds = ptr->tm_sec;
    pTime->Minutes = ptr->tm_min;
    pTime->Hours = ptr->tm_hour;

    pTime->Day = ptr->tm_mday;
    pTime->Month = ptr->tm_mon + 1;
    pTime->Year = 1900 + ptr->tm_year;
}

void
simple_time_print(SimpleTime* pTime)
{
    GINFO("%d:%d:%d %0.2d.%0.2d.%0.4d\n", pTime->Hours, pTime->Minutes, pTime->Seconds, pTime->Day, pTime->Month, pTime->Year);
}

i64
simple_time_get_seconds()
{
    time_t rawtime = time(NULL);
    return (i64)rawtime;
}

const char*
simple_time_to_string(SimpleTime st)
{
    static char buf[64] = {};
    snprintf(buf, 64, "%2d.%2d.%4d %2d:%2d:%2d",
	     st.Day, st.Month, st.Year,
	     st.Hours, st.Minutes, st.Seconds);
    return (const char*) buf;
}

const char*
simple_time_to_date_string(SimpleTime st)
{
    static char buf[64] = {};
    snprintf(buf, 64, "%2d:%2d:%2d", st.Day, st.Month, st.Year);
    return (const char*) buf;
}

const char*
simple_time_to_time_string(SimpleTime st)
{
    static char buf[64] = {};
    snprintf(buf, 64, "%0.2d:%0.2d:%0.2d", st.Hours, st.Minutes, st.Seconds);
    return (const char*) buf;
}


/*

  ###################################
  ###################################
  SimpleTimer.c
  ###################################
  ###################################

*/

i32
simple_timer_interval(SimpleTimer* pTimerData, f32 timestep)
{
    pTimerData->_CountSeconds += timestep;
    if (pTimerData->_CountSeconds >= pTimerData->WaitSeconds)
    {
    pTimerData->_CountSeconds = 0.0;
    return 1;
    }

    return 0;
}

void
simple_timer_reset(SimpleTimer* pTimerData)
{
    pTimerData->_CountSeconds = 0.0;
}

void
simple_timer_sleep(i64 sec)
{
    simple_timer_mssleep(1000 * sec);
}

void
simple_timer_mssleep(i64 ms)
{
#if defined(PLATFORM_LINUX)
    simple_timer_usleep(1000 * ms);
#elif defined(PLATFORM_WINDOWS)
    Sleep(ms);
#endif
}

void
simple_timer_usleep(i64 usec)
{
#if defined(PLATFORM_LINUX)
    usleep(usec);
#elif defined(PLATFORM_WINDOWS)
    HANDLE timer;
    LARGE_INTEGER ft;

    ft.QuadPart = -(10 * usec); // Convert to 100 nanosecond interval, negative value indicates relative time

    timer = CreateWaitableTimer(NULL, TRUE, NULL);
    SetWaitableTimer(timer, &ft, 0, NULL, NULL, 0);
    WaitForSingleObject(timer, INFINITE);
    CloseHandle(timer);
#endif
}

/*
  DOCS: Return time in seconds
*/


f64
simple_timer_get_time()
{
#if defined(PLATFORM_LINUX)
    struct timespec ts = {};

    u64 frequency,
    totalNs;
    if (clock_gettime(CLOCK_MONOTONIC, &ts) == 0)
    {
    frequency = 1000000000;
    totalNs = ((u64) ts.tv_sec) * ((u64) frequency) + ((u64) ts.tv_nsec);
    }
    else
    {
    frequency = 1000000;
    struct timeval tv;
    gettimeofday(&tv, NULL);
    totalNs = ((u64) tv.tv_sec) * ((u64) frequency) + ((u64) tv.tv_usec);
    }

    return ((f64)totalNs) / frequency;

#elif defined(PLATFORM_WINDOWS)

    static u64 frequency = 0;
    static i8 gIsPcFixed = 0;

    if (frequency == 0)
    {
    if (!QueryPerformanceFrequency((LARGE_INTEGER*) &frequency))
    {
	frequency = 1000;
	gIsPcFixed = 1;
    }
    }

    u64 value;
    if (!gIsPcFixed)
    {
    QueryPerformanceCounter((LARGE_INTEGER*) &value);
    }
    else
    {
    value = (u64) timeGetTime();
    }

    // ticks per second
    return ((f64)value) / frequency;

#endif
}

/*

  ###################################
  ###################################
  Bitset.c
  ###################################
  ###################################

*/
#define bitset_devide_to_bigger(bv) ((((bv) % 64) == 0) ? ((bv) / 64) : ((bv) / 64 + 1))

Bitset
bitset_new(i32 bitsCount)
{
    i32 chunksCount = bitset_devide_to_bigger(bitsCount);

    u64 size = chunksCount * sizeof(u64);
    u64* chunks = (u64*) memory_allocate(size);
    memset((void*)chunks, 0, size);

    Bitset bs = {
    .Chunks = chunks,
    .ChunksCount = chunksCount
    };

    return bs;
}

void
bitset_reset(Bitset cbs)
{
    u64 size = cbs.ChunksCount * sizeof(u64);
    memset((void*)cbs.Chunks, 0, size);
}

void
bitset_set(Bitset cbs, i32 bit)
{
    i32 chunkInd = bit / 65;
    vassert(chunkInd < cbs.ChunksCount && "Bit out of range!");
    i32 chunkBit = bit % 65;
    cbs.Chunks[chunkInd] |= (1 << chunkBit);
}
void
bitset_unset(Bitset cbs, i32 bit)
{
    i32 chunkInd = bit / 65;
    vassert(chunkInd < cbs.ChunksCount && "Bit out of range!");
    i32 chunkBit = bit % 65;
    cbs.Chunks[chunkInd] &= ~(1 << chunkBit);
}

i32
bitset_get(Bitset cbs, i32 bit)
{
    i32 chunkInd = bit / 64;
    vassert(chunkInd < cbs.ChunksCount && "Bit out of range!");
    i32 chunkBit = bit % 65;
    i32 value = cbs.Chunks[chunkInd] & (1 << chunkBit);
    return value;
}

void
bitset_test()
{
    Bitset bitset = bitset_new(247);
    bitset_set(bitset, 18);
    bitset_set(bitset, 1);
    bitset_set(bitset, 2);
    bitset_set(bitset, 3);
    bitset_set(bitset, 8);
    bitset_set(bitset, 19);
    bitset_set(bitset, 19);
    bitset_set(bitset, 60);
    bitset_set(bitset, 61);
    bitset_set(bitset, 62);
    bitset_set(bitset, 63);
    bitset_set(bitset, 64);
    bitset_set(bitset, 78);
    bitset_set(bitset, 129);
    bitset_set(bitset, 223);
    bitset_set(bitset, 246);
    GINFO("Chunks Count: %d\n", bitset.ChunksCount);
    GINFO("1 << 0 == %d\n", 1 << 0);
    for (i32 i = 0; i < bitset.ChunksCount; ++i)
    {
    i64 chunk = bitset.Chunks[i];
    printf("Chunk[%d]: ", i);
    for (i32 b = 0; b < 64; ++b)
    {
	i32 bit = (chunk & (1LL << b)) > 0 ? 1 : 0;
	if (bit == 1)
	printf("%d[%d] ", bit, b);
    }

    printf("\n");
    }
}

/*

  ###################################
  ###################################
  FPSCounter.c
  ###################################
  ###################################

*/
FpsCounter
fps_counter_create(GetTimeDelegate getTimeDelegate)
{
    FpsCounter counter = {0};
    counter.Fps = 0;
    counter.Frames = 0;
    counter.Since = 0.0;
    counter.GetTime = getTimeDelegate;
    return counter;
}

void
fps_counter_update(FpsCounter* fpsCounter)
{
#define SimpleRound(x) ( (i32) ((x) + 1) )

    ++fpsCounter->Frames;
    f64 now = fpsCounter->GetTime();
    f64 elapsed = now - fpsCounter->Since;
    if (elapsed >= 1)
    {
    fpsCounter->Fps = SimpleRound(fpsCounter->Frames / elapsed);
    fpsCounter->Frames = 0;
    fpsCounter->Since = now;
    }
}

/*###################################
  # DOCS: Sva.c
  ###################################*/

void
sva_free(void* pArr)
{
    if (pArr == NULL)
	return;

    SvaHeader* pHdr = sva_header(pArr);
    memory_free(pHdr);
}

void*
_sva_add(void* pArr, void* pData, i32 stride)
{
#define SvaData(pHdr) ({ ((void*)pHdr) + sizeof(SvaHeader); })

    // DOCS: Create new
    if (pArr == NULL)
    {
	SvaHeader* pHdr = (SvaHeader*) memory_allocate(sizeof(SvaHeader) + stride);
	pHdr->Count = 1;
	pHdr->Capacity = 1;
	pHdr->pData = SvaData(pHdr);
	memcpy(pHdr->pData, pData, stride);
	return pHdr->pData;
    }

    // DOCS: Realloc
    SvaHeader* pHdr = sva_header(pArr);
    if (pHdr->Count >= pHdr->Capacity)
    {
	i64 newCapacity = 2 * pHdr->Count + 1;
	SvaHeader* pNewHdr = (SvaHeader*) memory_allocate(sizeof(SvaHeader) + newCapacity * stride);
	pNewHdr->Count = pHdr->Count + 1;
	pNewHdr->Capacity = newCapacity;
	pNewHdr->pData = SvaData(pNewHdr);
	memcpy(pNewHdr->pData, pHdr->pData, pHdr->Count * stride);
	memcpy(pNewHdr->pData + pHdr->Count * stride, pData, stride);
	memory_free(pHdr);
	return pNewHdr->pData;
    }

    // DOCS: Just add new data
    memcpy(pHdr->pData + pHdr->Count * stride, pData, stride);
    ++pHdr->Count;
    return pHdr->pData;
}

void*
_sva_add_multiple(void* pArr, void* pData, i32 stride, i64 count)
{
    void* pRead = pData;
    for (i64 i = 0; i < count; ++i)
    {
	pArr = _sva_add(pArr, pRead, stride);
	pRead += stride;
    }

    return pArr;
}

void*
_sva_remove(void* pArr, void* item, i32 stride)
{
    SvaHeader* pHdr = sva_header(pArr);

    // DOCS: find
    void* pItem = pArr;
    i64 i;
    for (i = 0; i < pHdr->Count; ++i)
    {
	void* pItem = pArr + i*stride;
	if (memcmp(pItem, item, stride) == 0)
	{
	    break;
	}

	pArr += stride;
    }

    pArr = _sva_remove_index(pArr, i, stride);
    return pArr;
}

void*
_sva_remove_index(void* pArr, i32 ind, i32 stride)
{
    SvaHeader* pHdr = sva_header(pArr);
    if (ind < 0 || ind >= pHdr->Count)
	return pArr;

    i64 lastInd = pHdr->Count - 1;

    // DOCS: Ind pointing not to last item
    if ((ind + 1) < pHdr->Count)
    {
	// DOCS: Items left
	i64 itemsLeft = lastInd - ind;
	// DOCS: Copy items left
	memcpy(pArr + ind*stride, pArr + (ind + 1), itemsLeft * stride);
    }

    // DOCS: set last item to 0|null
    memset(pArr + lastInd*stride, 0, stride);

    return pArr;
}

void*
_sva_reserve(void* pArr, i32 stride, i64 count)
{
    SvaHeader* pHdr = (SvaHeader*) memory_allocate(sizeof(SvaHeader) + stride * count);
    pHdr->Count = 0;
    pHdr->Capacity = count;
    pHdr->pData = ((void*)pHdr) + sizeof(SvaHeader);
    return pHdr->pData;
}

void*
_sva_clear(void* pArr, i32 stride)
{
    if (pArr == NULL)
	return NULL;

    SvaHeader* pHdr = sva_header(pArr);
    memset(pHdr->pData, '\0', pHdr->Capacity * stride);
    pHdr->Count = 0;

    return pHdr->pData;
}

void*
_sva_deep_copy(void* pArr, i32 stride)
{
    if (pArr == NULL || stride == 0)
	return NULL;

    SvaHeader* pHdr = sva_header(pArr);
    u64 oldArrSize = pHdr->Capacity * stride;
    SvaHeader* pNewHdr = (SvaHeader*) memory_allocate(sizeof(SvaHeader) + oldArrSize);

    *pNewHdr = *pHdr;

    pNewHdr->pData = ((void*)pNewHdr) + sizeof(SvaHeader);
    memcpy(pNewHdr->pData, pHdr->pData, oldArrSize);

    return pNewHdr->pData;
}



/*

  ###################################
  ###################################
  Array.c
  ###################################
  ###################################

*/

void
array_remove(void* array, void* itemPtr)
{
    vassert_not_null(array);
    ArrayHeader* pHdr = array_header(array);

    i64 count = array_count(array);
    for (i64 i = 0; i < count; ++i)
    {
    void* iterPtr = (void*) (((char*)array) + pHdr->ElementSize * i);
    if (memcmp(iterPtr, itemPtr, pHdr->ElementSize) == 0)
    {
	_array_remove_at(array, i);
	break;
    }
    }

}

void
_array_remove_at(void* array, i32 i)
{
    ArrayHeader* pHdr = array_header(array);

    void* iterPtr = (void*) (((char*)array) + pHdr->ElementSize * i);
    memset(iterPtr, 0, pHdr->ElementSize);

    if ((pHdr->Count > 1) && (i < (pHdr->Count - 1)))
    {
    void* nextIterPtr = (void*) (((char*)iterPtr) + pHdr->ElementSize);
    u64 copySize = (pHdr->Count - (i + 1)) * pHdr->ElementSize;
    memmove(iterPtr, nextIterPtr, copySize);
    }

    --pHdr->Count;
}

void*
internal_array_reserve_w_alloc(const void* array, int elementsCount, int elementSize, void* (*allocDelegate)(u64 n, i32 line, const char* file))
{
    vassert_null(array);

    ArrayHeader* newHeader = (ArrayHeader*) allocDelegate(elementsCount * elementSize + sizeof(ArrayHeader), __LINE__, __FILE__);
    newHeader->Buffer = (void*) (((u8*)newHeader) + sizeof(ArrayHeader));
    newHeader->Count = 0;
    newHeader->Capacity = elementsCount;
    newHeader->ElementSize = elementSize;

    return newHeader->Buffer;
}

void*
internal_array_grow_w_alloc(const void* array, i32 elementSize, void* (*allocDelegate)(u64 n, i32 line, const char* file), void (*freeDelegate)(void* data, i32 line, const char* file))
{
    if (array != NULL)
    {
    u64 newCapacity = 2 * array_cap(array) + 1;
    u64 newSize = newCapacity * elementSize + sizeof(ArrayHeader);
    ArrayHeader* header = array_header(array);

    ArrayHeader* newHeader = NULL;
    newHeader = (ArrayHeader*) allocDelegate(newSize, __LINE__, __FILE__);
    newHeader->Buffer = ((char*)newHeader) + sizeof(ArrayHeader);
    newHeader->ElementSize = elementSize;
    newHeader->Count = header->Count;
    newHeader->Capacity = newCapacity;
    newHeader->IsReserved = 0;

    u64 copySize = header->Count * elementSize;
    memcpy(newHeader->Buffer, array, copySize);
    assert(copySize < newSize && "CopySize >= NewSize!!!!!!");

    freeDelegate(header, __LINE__, __FILE__);

    return newHeader->Buffer;
    }

    return internal_array_reserve_w_alloc(array, StartSize, elementSize, allocDelegate);
}

void*
internal_array_copy_w_alloc(const void* src, void* (*allocDelegate)(u64 n, i32 line, const char* file))
{
    if ((src) != NULL)
    {
    ArrayHeader* header = array_header(src);
    header->IsReserved = 0;
    i32 count = header->Count;
    i32 elementSize = header->ElementSize;
    void* result = internal_array_reserve_w_alloc(NULL, count, elementSize, allocDelegate);

    array_header(result)->Count = header->Count;
    memcpy(result, src, count * elementSize);

    return result;
    }

    return NULL;
}

void*
internal_array_shift_right(void* b, i32 i)
{
    ArrayHeader* header = array_header(b);
    assert(i < header->Count && "Header remove index >= array_count!!!");
    assert(header->Count < header->Capacity && "Capacity should be exceeded before write!");

    u64 writeOffset = (i + 1) * header->ElementSize;
    u64 copySize = header->ElementSize * (header->Count - i);
    void* dataToCopy = b + i * header->ElementSize;

    memcpy(b + writeOffset, dataToCopy, copySize);

    return b;
}

void*
internal_array_pop(void* b)
{
    vassert_not_null(b);

#if STATIC_ANALIZER_CHECK == 1
    if (!b) return b;
#endif

    ArrayHeader* header = array_header(b);

    memset(b, 0, header->ElementSize);

    if (header->Count > 0)
    {
    u64 nextCopySize = header->ElementSize * (header->Count - 1);
    memmove(b, (b + header->ElementSize), nextCopySize);
    --header->Count;
    }

    return b;
}

void*
internal_array_clear(void* array)
{
    ArrayHeader* hdr = array_header(array);
    if (!hdr->Buffer)
    return hdr->Buffer;
    memset(hdr->Buffer, 0, hdr->Capacity * hdr->ElementSize);
    hdr->Count = 0;
    return hdr->Buffer;
}

/*

  ###################################
  ###################################
  RingQueue.c
  ###################################
  ###################################

*/

void*
_ring_queue_new(const void* array, i16 capacity, u64 size)
{
    vassert_null(array);

    u64 ringQueueHeaderSize = sizeof(RingQueueHeader);

    RingQueueHeader* hdr = (RingQueueHeader*) memory_allocate(capacity * size + ringQueueHeaderSize);
    hdr->Count = 0;
    hdr->Capacity = capacity;
    hdr->InsertIndex = 0;
    hdr->ElementSize = size;
    hdr->Buffer = ((char*)hdr) + ringQueueHeaderSize;

    return hdr->Buffer;
}

void
_ring_queue_free(void* ringQueue)
{
    vassert_null(ringQueue);
    RingQueueHeader* hdr = ring_queue_header(ringQueue);
    memory_free(hdr);
}

i64
_ring_queue_push(void* rq)
{
    vassert_not_null(rq);

    RingQueueHeader* hdr = ring_queue_header(rq);
    i16 cap = hdr->Capacity;

    if (hdr->Count < cap)
    {
    ++hdr->Count;
    }

    i16 ind = hdr->InsertIndex;

    ++hdr->InsertIndex;
    if (hdr->InsertIndex >= cap)
    {
    hdr->InsertIndex = 0;
    }

    return ind;
}

i16
_rinq_queue_get_popped_index(void* rq)
{
    vassert_not_null((rq));

    RingQueueHeader* hdr = ring_queue_header(rq);
    vassert(hdr->Count > 0 && "Ring Queue is empty!");

    i16 ind = hdr->InsertIndex;
    if (ind > 0)
    --ind;
    else
    ind = hdr->Capacity - 1;

    return ind;
}

void
_ring_queue_pop(void* rq, i16 popInd)
{
    RingQueueHeader* hdr = ring_queue_header(rq);
    i16 cnt       = hdr->Count;
    i16 size      = hdr->ElementSize;
    i16 lastInd   = (cnt - 1);
    i16 elemToCpy = lastInd - popInd;

    memmove(rq + popInd * size,
       rq + (popInd + 1) * size,
       elemToCpy * size);

    hdr->InsertIndex = popInd;
    --hdr->Count;
}

void
ring_queue_test()
{
    i32* rq = NULL;
    rinq_queue_new(rq, 6);

    for (i32 i = 0; i < 24; ++i)
    {
    ring_queue_push(rq, i);
    }
    for (i32 i = 0; i < 6; ++i)
    {
    printf("rq[%d] = %d\n", i, rq[i]);
    }

    vassert(0);
}

// End of RingQueue.c

/*

  ###################################
  ###################################
  SimpleThread.c
  ###################################
  ###################################

*/

SimpleThread
simple_thread_create(SimpleThreadDelegate threadFunction, u64 stackSize, void* pData)
{
    SimpleThread thread;

#ifdef PLATFORM_WINDOWS
    typedef unsigned long (*WinCallback)(void*);
    thread.ID = CreateThread(NULL, stackSize, (WinCallback) threadFunction, pData, 0, NULL);

#elif defined(PLATFORM_LINUX)
    pthread_attr_t threadAttributes;
    pthread_attr_init(&threadAttributes);
    i32 result = pthread_attr_setguardsize(&threadAttributes, stackSize);
    i32 status = pthread_create(&thread.ID, &threadAttributes, threadFunction, pData);
    if (status == -1)
    {
    perror("Mutex error: ");
    vassert(0 && "Thread creation error!");
    }
    pthread_attr_destroy(&threadAttributes);
#endif

    //pthread_exit(void* retval);

    return thread;
}

void
simple_thread_attach(SimpleThread* thread)
{
#if defined(PLATFORM_WINDOWS)
    DWORD result = WaitForSingleObject(thread->ID, INFINITE); // NOTE(typedef): INFINITE -> no time interval, declared in some windows files
    if (result == WAIT_FAILED)
    {
    vassert(0 && "Thread can't join!");
    }
#elif defined(PLATFORM_LINUX)
    i32 status = pthread_join(thread->ID, NULL);
    if (status != 0)
    {
    vassert(0 && "Thread can't join!");
    }
#endif
}

ResultType
simple_thread_cancel(SimpleThread thread)
{
#if defined(PLATFORM_WINDOWS)
    BOOL result = TerminateThread(thread.ID, 0);
    if (result)
    {
    return ResultType_Success;
    }

    return ResultType_Error;
#elif defined(PLATFORM_LINUX)
    i32 result = pthread_cancel(thread.ID);
    if (result == 0)
    {
    return ResultType_Success;
    }

    return ResultType_Error;
#endif
}

SimpleMutex
simple_thread_mutex_create()
{
    SimpleMutex simpleMutex;

#if defined(PLATFORM_WINDOWS)
    simpleMutex.ID = CreateMutexA(NULL, FALSE, NULL);
    if (simpleMutex.ID == NULL)
    {
    GWARNING("Can't create mutex!\n");
    vassert_break();
    }
#elif defined(PLATFORM_LINUX)
    i32 result = pthread_mutex_init(&simpleMutex.ID, NULL);
    if (result != 0)
    {
    GWARNING("Can't create mutex!\n");
    vassert_break();
    }
#endif

    return simpleMutex;
}

ResultType
simple_thread_mutex_destroy(SimpleMutex* simpleMutex)
{
#if defined(PLATFORM_WINDOWS)
    BOOL result = CloseHandle(simpleMutex->ID);
    if (result == FALSE)
    {
    vassert("Can't close handle!");
    return ResultType_Error;
    }
    return ResultType_Success;
#elif defined(PLATFORM_LINUX)

    i32 result = pthread_mutex_destroy(&simpleMutex->ID);
    if (result != 0)
    {
    GWARNING("Can't create mutex!\n");
    return ResultType_Error;
    }
    return ResultType_Success;

#endif
}

ResultType
simple_thread_mutex_lock(SimpleMutex* simpleMutex)
{
#if defined(PLATFORM_WINDOWS)
    DWORD result = WaitForSingleObject(simpleMutex->ID, INFINITE);
    if (result == WAIT_FAILED)
    {
    vassert(0 && "Thread can't join!");
    return ResultType_Error;
    }
    return ResultType_Success;
#elif defined(PLATFORM_LINUX)
    i32 result = pthread_mutex_lock(&simpleMutex->ID);
    if (result != 0)
    {
    GWARNING("Can't create mutex!\n");
    return ResultType_Error;
    }
    return ResultType_Success;
#endif
}

ResultType
simple_thread_mutex_unlock(SimpleMutex* simpleMutex)
{
#if defined(PLATFORM_WINDOWS)
    BOOL result = ReleaseMutex(simpleMutex->ID);
    if (result == FALSE)
    {
    vassert(0 && "Thread can't join!");
    return ResultType_Error;
    }
    return ResultType_Success;
#elif defined(PLATFORM_LINUX)
    i32 result = pthread_mutex_unlock(&simpleMutex->ID);
    if (result != 0)
    {
    GWARNING("Can't create mutex!\n");
    return ResultType_Error;
    }
    return ResultType_Success;
#endif
}

// End Of SimpleThread.c


/*

  ###################################
  ###################################
  MemoryAllocator.c
  ###################################
  ###################################

*/

static void
block_create(MemoryBlock* this, i64 size, const char* file, i32 line)
{
    this->AllocatedSize = size;
    this->File = file;
    this->Line = line;
    this->Address = (void*) (((char*)this) + sizeof(MemoryBlock));

    assert(this == memory_block_header(this->Address) && "Wrong MemoryBlock->Address!!!");
    assert((((u64)(this->Address) - (u64)(this)) == sizeof(MemoryBlock)) && "Wrong MemoryBlock->Address!!!");
}

static void
print_address(const char* text, void* address)
{
    u64 addr = (u64)address;
    if (addr)
    {
    u64 lowAddress = addr % 1000;
    char add[3];
    if (lowAddress < 100 && lowAddress >= 10)
    {
	add[0] = '0';
	add[1] = '\0';
    }
    else if (lowAddress < 10)
    {
	add[0] = '0';
	add[1] = '0';
	add[2] = '\0';
    }
    else
    {
	add[0] = '\0';
	add[1] = ' ';
	add[2] = ' ';
    }

    //printf("%s: %lu"YELLOW("%s")YELLOW("%lu "), text, addr / 1000,  add, lowAddress);
    }
    else
    {
    //printf("%s: "RED("      NULL      "), text);
    }
}

static void
block_show(MemoryBlock* block)
{
    printf("File: %s, Line: %d, Size: %lld ", block->File, block->Line, block->AllocatedSize);
    print_address("Current", (void*) block);
    printf("\n");
}

static i64 GlobalAllocatedSize = 0;
static MemoryBlock** MemoryBlocks = NULL;
static PrintAllocationSourceType PrintSourceType = PrintAllocationSourceType_None;
static i32 AllocCalls = 0;
static i32 FreeCalls  = 0;
SimpleArena* CurrentArena = NULL;
static SimpleArena** Arenas = NULL;
static i64 ArenasGlobalId = 0;

SimpleArena*
_arena_create(u64 size, i32 line, const char* file)
{
    u64 arenaSize = sizeof(SimpleArena);
    SimpleArena* arena = (SimpleArena*) memory_helper_malloc(size + arenaSize, __LINE__, __FILE__);
    arena->Id = ArenasGlobalId;
    arena->Offset = 0;
    arena->Size = size;
    arena->Line = line;
    arena->File = file;
    arena->Data = ((void*)arena) + arenaSize;

    ++ArenasGlobalId;

    return arena;
}

void
simple_arena_clear(SimpleArena* pArena)
{
    pArena->Offset = 0;
    memset(pArena->Data, 0, pArena->Size);
}

SimpleArena*
_arena_create_and_set(u64 size, i32 line, const char* file)
{
    SimpleArena* arena = _arena_create(size, line, file);
    memory_set_arena(arena);
    return arena;
}

void
simple_arena_destroy(SimpleArena* arena)
{
    memset(arena->Data, 0, arena->Size);

    if (CurrentArena == arena)
    {
    memory_set_arena(NULL);
    }

    memory_helper_free(arena, __LINE__, __FILE__);
}

void
simple_arena_format(char* buffer, SimpleArena* arena)
{
    const i32 size = 128;
    char offsetBuf[size];
    char sizeBuf[size];
    memory_helper_format_size(offsetBuf, arena->Offset);
    memory_helper_format_size(sizeBuf, arena->Size);
    string_format(buffer, "[id: %lld line: %d file: %s] %s / %s", arena->Id, arena->Line, arena->File, offsetBuf, sizeBuf);
}

void
simple_arena_print(SimpleArena* arena)
{
    if (arena == NULL)
    {
    GWARNING("Arena is NULL!\n");
    return;
    }

    char buf[256];
    simple_arena_format(buf, arena);
    GWARNING("Arena: %s\n", buf);
}

void
memory_bind_current_arena()
{
    if (!array_any(Arenas))
    {
	return;
    }

    CurrentArena = Arenas[array_count(Arenas) - 1];
}

void
memory_bind_arena(SimpleArena* pArena)
{
    CurrentArena = pArena;
}

void
memory_unbind_current_arena()
{
    CurrentArena = NULL;
}

static void*
_arena_alloc(u64 n, i32 line, const char* pFile)
{
    return malloc(n);
}

static void
_arena_free(void* pData, i32 line, const char* pFile)
{
    free(pData);
}

void
memory_set_arena(SimpleArena* arena)
{
    CurrentArena = NULL;

    if (arena != NULL)
    {
	array_push_w_alloc(Arenas, _arena_alloc, _arena_free, arena);
	CurrentArena = arena;
    }
    else if (array_any(Arenas))
    {
	array_pop(Arenas);
	if (array_any(Arenas))
	    CurrentArena = array_pop(Arenas);
    }
}

SimpleArena*
memory_get_arena()
{
    return CurrentArena;
}

void*
_memory_allocate(u64 size, i32 line, const char* file)
{
    if (PrintSourceType == PrintAllocationSourceType_Terminal)
    {
    printf("[MemoryAllocate] Allocated memory Size: %lld, FILE: %s LINE: %d\n", (i64)size, file, line);
    }

    if (CurrentArena != NULL)
    {
    if (CurrentArena->Offset >= CurrentArena->Size)
    {
	printf("[MemoryAllocate-Arena] Need more memory for arena offset:%lld size:%lld !!!\n", (i64)CurrentArena->Offset, (i64)CurrentArena->Size);
	vassert(CurrentArena->Offset < CurrentArena->Size && "Need more memory for arena!");
    }

    vassert(CurrentArena->Offset >= 0 && "Wrong arena offset");
    void* data = CurrentArena->Data + CurrentArena->Offset;
    CurrentArena->Offset += size;

    return data;
    }

    ++AllocCalls;

    GlobalAllocatedSize += size;

    u64 newSize = size + sizeof(MemoryBlock);
    assert(size > 0 && "memory_allocate(size) where size > 0 !!!");

    MemoryBlock* header = (MemoryBlock*) malloc(newSize);
    block_create(header, size, file, line);
    array_push_w_alloc(MemoryBlocks, memory_helper_malloc, memory_helper_free, header);

    return header->Address;
}

MemoryBlock**
memory_helper_get_memory_blocks()
{
    return MemoryBlocks;
}

void*
memory_helper_reallocate(void* data, i32 size, i32 line, const char* file)
{
    printf("[Realloc] Do no use realloc!\n");
    return _memory_allocate(size, line, file);
}

void
_memory_free(void* data, i32 line, const char* file)
{
    if (!data)
    return;

    if (CurrentArena != NULL)
    {
    //arena_print(CurrentArena);
    vassert(CurrentArena->Offset < CurrentArena->Size && "Need more memory for arena!");
    vassert(CurrentArena->Offset >= 0 && "Wrong arena offset");
    return;
    }

    ++FreeCalls;
    //MemoryBlock* block = list_find(&g_AllocList, data);
    //list_remove(&g_AllocList, block);
    MemoryBlock* block = memory_block_header(data);

    vassert(!IS_NULL_OFFSET(block->Address));

    array_remove(MemoryBlocks, block);

    if (PrintSourceType == PrintAllocationSourceType_Terminal)
    {
    //printf("[Free memory] Size: %lld, FILE: %s LINE: %d\n", block->AllocatedSize, file, line);
    }

    GlobalAllocatedSize -= block->AllocatedSize;

    static i32 ind = 0;
    static u64 addrs[128] = {};

    for (i32 i = 0; i < 128; ++i)
    {
    if (addrs[i] == ((u64)block))
    {
	//printf("Double free?!!!\nPointer: %p\n", block);
	//printf("[Free memory] Size: %lld, FILE: %s LINE: %d\n", block->AllocatedSize, file, line);
    }
    }

    free(block);

    addrs[ind++] = (u64)block;
    if (ind == 127)
    ind = 0;

    block = NULL;
}

void
memory_helper_free_bytes(u64 size, i32 line, const char* file)
{
    if (CurrentArena != NULL)
    {
    vassert(CurrentArena->Offset < CurrentArena->Size && "Need more memory for arena!");
    vassert(CurrentArena->Offset >= 0 && "Wrong arena offset");
    CurrentArena->Offset -= size;

    if (PrintSourceType == PrintAllocationSourceType_Terminal)
    {
	GSUCCESS("Free memory: Size: %d, FILE: %s LINE: %d\n", size, file, line);
    }
    }
}

i32
memory_helper_get_allocated_size()
{
    return GlobalAllocatedSize;
}

void
memory_helper_format_size(char* buf, u64 bytes)
{
    i64 kb = KB(1),
    mb = MB(1),
    gb = GB(1);
    if (bytes >= kb && bytes < mb)
    {
    string_format(buf, "%0.2f kb", ((f32)bytes) / kb);
    }
    else if (bytes >= mb && bytes < gb)
    {
    string_format(buf, "%0.2f mb", ((f32)bytes) / mb);
    }
    else if (bytes >= gb)
    {
    string_format(buf, "%0.2f gb", ((f32)bytes) / gb);
    }
    else
    {
    string_format(buf, "%llu bytes", bytes);
    }
}

//TODO(bies): rename this as soon as possible
void
memory_set_print(PrintAllocationSourceType type)
{
    PrintSourceType = type;
}

void*
memory_helper_malloc(u64 size, i32 line, const char* file)
{
    return malloc(size);
}

void
memory_helper_free(void* data, i32 line, const char* file)
{
    vassert_not_null(data);
#if STATIC_ANALIZER_CHECK == 1
    if (!data) return;
#endif

    free(data);
}


/*

  ###################################
  ###################################
  String.c
  ###################################
  ###################################

*/

IStringPool*
istring_pool_create()
{
    IStringPool* iStringPool = (IStringPool*) memory_allocate(sizeof(IStringPool));

    iStringPool->Strings = NULL;

    return iStringPool;
}

// note: this shit exist forever
void
istring_pool_destroy(IStringPool* pPool)
{
#if 0
    if (pPool == NULL)
    return;

    i64 cnt = array_count(pPool->Strings);
    for (i64 i = 0; i < cnt; ++i)
    {
    IString* istr = pPool->Strings[i];
    if (istr != NULL)
	istring_free_ext(pPool, istr->Buffer);
    }

    array_free(pPool->Strings);

    memory_free(pPool);
#endif
}

/* DOCS(typedef): IString local api */
IString*
istring_new_ext(IStringPool* stringPool, const char* src)
{
    IString* istr = istring_get(src);
    if (istr)
    {
    return istr;
    }

    i32 ind = array_count(stringPool->Strings);
    istring_allocate_ext(stringPool, src);
    vassert(&stringPool->Strings[ind] && "Problem with getting IString!");

    return stringPool->Strings[ind];
}

char*
istring_allocate_ext(IStringPool* stringPool, const char* src)
{
    vassert_not_null(src);

    i64 length = string_length(src);
    u64 size = sizeof(IString) + (length + 1) * sizeof(*src);
    IString* interning = (IString*) memory_allocate(size);
    char* buffer = (char*) (((char*)interning) + sizeof(IString));
    vassert_not_null(buffer);
    memcpy(buffer, src, length * sizeof(*src));
    buffer[length] = '\0';

    interning->Buffer = (char*) buffer;
    interning->Length = length;

    vassert(interning->Buffer == ((void*)interning) + sizeof(*interning));

    array_push(stringPool->Strings, interning);

    return interning->Buffer;
}

char*
istring_get_buffer_ext(IStringPool* stringPool, const char* src)
{
    i32 i;
    i32 count = array_count(stringPool->Strings);

    for (i = 0; i < count; ++i)
    {
    if (string_compare(src, stringPool->Strings[i]->Buffer))
    {
	return stringPool->Strings[i]->Buffer;
    }
    }

    return NULL;
}

IString*
istring_get_ext(IStringPool* stringPool, const char* src)
{
    i32 i;
    i32 count = array_count(stringPool->Strings);

    for (i = 0; i < count; ++i)
    {
    if (string_compare(src, stringPool->Strings[i]->Buffer))
    {
	return stringPool->Strings[i];
    }
    }

    return NULL;
}

void
istring_free_ext(IStringPool* stringPool, char* istring)
{
    IString* iheader = istring_header(istring);
    array_remove(stringPool->Strings, iheader);
    memory_free(iheader);
}

void
istring_free_headers_ext(IStringPool* stringPool)
{
    if (!stringPool->Strings)
    return;

    array_foreach(stringPool->Strings, memory_free(item););
    array_free(stringPool->Strings);
}

static IString** g_IStrings = NULL;

IString*
istring_new(const char* src)
{
    IStringPool stringPool = { .Strings = g_IStrings };
    return istring_new_ext(&stringPool, src);
}

char*
istring_allocate(const char* src)
{
    IStringPool stringPool = { .Strings = g_IStrings };
    return istring_allocate_ext(&stringPool, src);
}

IString*
istring_get(const char* src)
{
    IStringPool stringPool = { .Strings = g_IStrings };
    return istring_get_ext(&stringPool, src);
}

char*
istring_get_buffer(const char* src)
{
    IStringPool stringPool = { .Strings = g_IStrings };
    return istring_get_buffer_ext(&stringPool, src);
}

IString**
istring_get_headers()
{
    return g_IStrings;
}

void
istring_free(char* istring)
{
    IStringPool stringPool = { .Strings = g_IStrings };
    istring_free_ext(&stringPool, istring);
}

void
istring_free_headers()
{
    IStringPool stringPool = { .Strings = g_IStrings };
    istring_free_headers_ext(&stringPool);
}

//Internal
force_inline i32
_string_number_rank(i32 number)
{
    i32 rank = 0;
    for (; ;)
    {
    number /= 10;
    if (number == 0)
    {
	return rank;
    }

    ++rank;
    }
}

force_inline i32
_string_number_of_rank(i32 number, i32 rank)
{
    if (rank <= 0)
    {
    return number;
    }

    for (i32 i = 0; i < rank; i++)
    {
    number *= 10;
    }

    return number;
}

force_inline i32
_string_number_of_digit(i32 number, i32 digit)
{
    i32 i;

    if (_string_number_rank(number) < digit)
    {
    return 0;
    }

    if (number < 0)
    {
    number *= -1;
    }

    if (digit == 0)
    {
    return (number % 10);
    }

    number %= _string_number_of_rank(1, (digit + 1));
    number /= _string_number_of_rank(1, digit);

    return number;
}

#define _string_int(input, number)                                      \
    ({                                                                  \
    i8 isNumberNegative = ((number < 0) ? 1 : 0);                   \
    i32 i, rank = _string_number_rank(number), numberLength = rank + isNumberNegative + 1; \
				    \
    if (isNumberNegative)                                           \
    {                                                               \
	input[0] = '-';                                             \
    }                                                               \
				    \
    for (i = isNumberNegative; i < numberLength; ++i)               \
    {                                                               \
	input[i] = _string_number_of_digit(number, rank) + 48;      \
	--rank;                                                     \
    }                                                               \
    })


char*
string(const char* string)
{
    i64 length = string_length(string);
    char* newString = (char*) memory_allocate((length + 1) * sizeof(char));
    newString[length] = '\0';
    memcpy(newString, string, length * sizeof(char));

    return newString;
}

void
string_i32(char* input, i32 number)
{
    _string_int(input, number);
}

char*
string_allocate(i32 length)
{
    i32 size = (length + 1) * sizeof(char);
    char* newString = (char*) memory_allocate(size);
    memset(newString, '\0', size);

    return newString;
}

i32
string_count_of_fast(const char* string, i32 length, char c)
{
    i32 count = 0;
    //char* ptr = (char*) string;
    for (i32 i = 0; i < length; ++i)
    {
    if (string[i] == c)
	++count;
    }

    return count;
}

i32
string_count_of(const char* string, char c)
{
    i64 length = string_length(string);
    i32 count = string_count_of_fast(string, length, c);

    return count;
}

i32
string_count_upper(const char* string)
{
    i32 count = 0;
    char* optr = (char*) string;
    char oc;
    oc = *optr;

    while (oc != '\0')
    {
    if (char_is_upper(oc))
    {
	++count;
    }

    ++optr;
    oc = *optr;
    }

    return count;
}

void
string_set(char* string, char c, u32 length)
{
    i32 i;
    for (i = 0; i < length; ++i)
    {
    string[i] = c;
    }
}

i64
string_length(const char* str)
{
    vassert_not_null(str);

    char* ptr;
    for (ptr = (char*) str; *ptr != '\0'; ++ptr);

    return (i64) ((u64)(ptr - str));
}

u64
string_length_to_delimiters(const char* str, char delimeters[], u64 delimetersLength)
{
    vassert_not_null(str);

    char* ptr = (char*) str;
    char c = *ptr;
    while (c != '\0')
    {
    i32 interrupt = 0;
    for (i32 i = 0; i < delimetersLength; ++i)
    {
	if (c == delimeters[i])
	{
	interrupt = 1;
	break;
	}
    }

    if (interrupt)
	break;

    ++ptr;
    c = *ptr;
    }

    u64 length = ptr - str;
    return length;
}

char*
string_copy(const char* oth, i32 length)
{
    assert(oth);

    char* result = (char*) memory_allocate((length + 1) * sizeof(char));
    memcpy(result, oth, length);
    result[length] = '\0';
    return result;
}
char*
string_copy_bigger(const char* oth, i32 length, i32 bigLength)
{
    vassert_not_null(oth);
    vassert(bigLength > length && "bigLength <= length!");

    char* result = (char*) memory_allocate((bigLength + 1) * sizeof(char));
    memcpy(result, oth, length);
    result[bigLength] = '\0';
    return result;
}

char*
string_concat(const char* left, const char* right)
{
    i32 leftLength = string_length(left);
    i32 rightLength = string_length(right);
    i32 bothLength = leftLength + rightLength;

    if (bothLength > 100000)
    {
    GERROR("LENGTH: %d\n", bothLength);
    }
    char* newString = (char*) memory_allocate((bothLength + 1) * sizeof(char));

    memcpy(newString, left, leftLength);
    memcpy(newString + leftLength, right, rightLength);
    newString[bothLength] = '\0';

    return newString;
}

char*
string_concat_with_space_between(const char* left, const char* right, i32 length, char c)
{
    vassert_break();
    assert(left && "left is NULL or undefined!");
    assert(right && "right is NULL or undefined!");

    i32 leftLength = string_length(left);
    i32 rightLength = string_length(right);
    i32 bothLength = leftLength + rightLength;

    assert((length > bothLength) && "length >");

    i32 totalLength = (length + rightLength + 1);
    char* result = (char*) memory_allocate(totalLength * sizeof(char));
    //memset(result, c, totalLength);
    memcpy(result, left, leftLength);
    memcpy(result + length, right, rightLength);

    result[bothLength] = '\0';

    return result;
}

char*
string_concat3(const char* left, const char* middle, const char* right)
{
    assert(left && "left is NULL or undefined!");
    assert(middle && "middle is NULL or undefined!");
    assert(right && "right is NULL or undefined!");

    i32 leftLength = string_length(left);
    i32 middleLength = string_length(middle);
    i32 rightLength = string_length(right);
    i32 bothLength = leftLength + middleLength + rightLength;

    char* newString = (char*) memory_allocate(bothLength + 1);

    memcpy(newString, left, leftLength);
    memcpy(newString + leftLength, middle, middleLength);
    memcpy(newString + leftLength + middleLength, right, rightLength);
    newString[bothLength] = '\0';

    return newString;
}

char*
string_concat3l(const char* left, const char* middle, const char* right, i32 leftLength, i32 middleLength, i32 rightLength)
{
    assert(left && "left is NULL or undefined!");
    assert(middle && "middle is NULL or undefined!");
    assert(right && "right is NULL or undefined!");

    i32 bothLength = leftLength + middleLength + rightLength;
    char* newString = (char*) memory_allocate(bothLength + 1);

    memcpy(newString, left, leftLength);
    memcpy(newString + leftLength, middle, middleLength);
    memcpy(newString + leftLength + middleLength, right, rightLength);

    newString[bothLength] = '\0';

    return newString;
}

i32
string_compare(const char* left, const char* right)
{
    vassert_not_null(left);
    vassert_not_null(right);

    i64 i = 0,
    leftLength  = string_length(left),
    rightLength = string_length(right);

    if ((leftLength != rightLength)
    || (leftLength == 0)
    || (rightLength == 0))
    {
    return 0;
    }

    char* ptrl = (char*) left;
    char* ptrr = (char*) right;
    for ( ;i < leftLength; )
    {
    if (*ptrl != *ptrr)
    {
	return 0;
    }

    ++i;
    ++ptrl;
    ++ptrr;
    }

    return 1;
}

i32
string_compare_length(const char* left, const char* right, i64 length)
{
    vassert_not_null(left);
    vassert_not_null(right);

    i64 i = 0;
    char* ptrl = (char*) left;
    char* ptrr = (char*) right;
    for ( ;i < length; )
    {
    char lc = *ptrl;
    char rc = *ptrr;
    if (lc != rc)
    {
	return 0;
    }

    ++i;
    ++ptrl;
    ++ptrr;
    }

    return 1;
}

i32
string_compare_length_safe(const char* left, const char* right, i32 length)
{
    assert(left && "left is NULL or undefined!");
    assert(right && "right is NULL or undefined!");

    i32 llength = string_length(left);
    i32 rlength = string_length(right);
    if (llength != rlength)
    return 0;

    if (length > llength || length > rlength)
    return 0;

    i32 i;
    for (i = 0; i < length; ++i)
    {
    if (left[i] != right[i])
    {
	return 0;
    }
    }

    return 1;
}


i32
string_compare_w_length(const char* left, const char* right, i32 leftLength, i32 rightLength)
{
    assert(left && "left is NULL or undefined!");
    assert(right && "right is NULL or undefined!");

    if (leftLength != rightLength)
    {
    return 0;
    }

    char* eptr = ((char*) left + leftLength);
    char* lptr = (char*) left;
    char* rptr = (char*) right;

    for (;lptr != eptr;)
    {
    char lc = *lptr;
    char rc = *rptr;

    if (lc != rc)
	return 0;

    if (lc == '\0' || rc == '\0')
	return 0;

    ++lptr;
    ++rptr;
    }

    return 1;
}

char*
string_to_upper(const char* input)
{
    assert(input && "input is NULL or undefined!");

    i32 i;
    i32 inputLength = string_length(input);
    char* result = memory_allocate((inputLength + 1) * sizeof(char));

    for (i = 0; i < inputLength; ++i)
    {
    char element = input[i];
    if (element >= 'a' && element <= 'z')
    {
	result[i] = element - 'a' + 'A';
    }
    else
    {
	result[i] = element;
    }
    }
    result[inputLength] = '\0';
    return result;
}

char*
string_to_lower(const char* input)
{
    assert(input && "input is NULL or undefined!");

    i32 i, input_length;
    char  element;
    char* result;

    if (input == NULL)
    {
    return NULL;
    }

    input_length = string_length(input);
    result = memory_allocate((input_length + 1) * sizeof(char));
    for (i = 0; i < input_length; ++i)
    {
    element = input[i];
    if (element >= 'A' && element <= 'Z')
    {
	result[i] = element - 'A' + 'a';
    }
    else
    {
	result[i] = element;
    }
    }
    result[input_length] = '\0';
    return result;
}

i32
string_index_of(const char* input, char character)
{
    assert(input && "input is NULL or undefined!");

    i32 i;

    if (input == NULL)
    {
    return -1;
    }

    for (i = 0; input[i] != '\0'; ++i)
    {
    if (input[i] == character)
    {
	return i;
    }
    }

    return -1;
}

i32
string_index_of_string(const char* input, const char* string)
{
    assert(input && "input is NULL or undefined!");
    assert(string && "string is NULL or undefined!");

    i32 i, j, flag, inputLength, stringLength;

    assert(string != NULL);

    inputLength = string_length(input);
    stringLength = string_length(string);

    if (inputLength <= 0 || stringLength <= 0)
    {
    return -1;
    }

    flag = -1;
    for (i = 0; i < inputLength; ++i)
    {
    for (j = 0; j < stringLength; ++j)
    {
	if (input[i + j] == string[j])
	{
	flag = 1;
	}
	else
	{
	flag = -1;
	break;
	}
    }

    if (flag == 1)
    {
	return i;
    }
    }

    return -1;
}

i32
string_last_index_of(const char* input, char character)
{
    assert(input && "Input can't be NULL!!!");

    i32 i, startIndex;

    startIndex = string_length(input) - 1;

    for (i = startIndex; i >= 0; i--)
    {
    if (input[i] == character)
    {
	return i;
    }
    }

    return -1;
}

i32
string_last_index_of_string(const char* input, const char* string)
{
    vassert_not_null(input);
    vassert_not_null(string);

    i32 i, j, flag, inputLength, stringLength;

    inputLength = string_length(input);
    stringLength = string_length(string);

    if (inputLength <= 0 || stringLength <= 0)
    {
    return -1;
    }

    flag = -1;
    i32 temp = 0;
    for (i = inputLength; i >= 0; i--)
    {
    for (j = stringLength; j >= 0; j--)
    {
	if (input[i - temp] == string[j])
	{
	flag = 1;
	}
	else
	{
	flag = -1;
	break;
	}
	++temp;
    }

    if (flag == 1)
    {
	return temp;
    }
    }

    return -1;
}

i32
string_last_index_of_upper(const char* input, i32 length)
{
    i32 i;
    char* ptr = (char*) input;

    for (i = (length - 1); i >= 0; --i)
    {
    char c = ptr[i];
    if (char_is_upper(c))
    {
	return i;
    }
    }

    return -1;
}

char*
string_substring(const char* input, i32 startIndex)
{
    vassert(input && "input length is NULL !!!");

    i32 i, newLength, inputLength;
    char* result;

    inputLength = string_length(input);
    assert(startIndex < inputLength && "start index >= input length !!!");
    assert(startIndex > 0 && "start index < 0!!!");

    newLength = inputLength - startIndex;
    result = memory_allocate((newLength + 1) * sizeof(char));
    memcpy(result, input + startIndex, newLength);
    result[newLength] = '\0';

    return result;
}

char*
string_substring_length(const char* input, u64 inputLength, i32 length)
{
    vassert_not_null(input);
    vassert(length <= inputLength && "start index >= input length !!!");

    char* result = memory_allocate((length + 1) * sizeof(char));
    memcpy(result, input, length);
    result[length] = '\0';

    return result;
}

char*
string_substring_range(const char* input, i32 startIndex, i32 endIndex)
{
    i32 i, inputLength, newLength;
    char* result;

    assert(input && input != NULL && "input can't be NULL!!!");

    inputLength = string_length(input);
    newLength = endIndex - startIndex + 1;
    result = memory_allocate((newLength + 1) * sizeof(char));

    vassert(startIndex < inputLength);
    vassert(startIndex >= 0);
    vassert(inputLength > endIndex && "Out of input string range!");
    vassert(startIndex <= endIndex && "Out of input string range!");

    memcpy(result, input + startIndex, newLength);
    result[newLength] = '\0';

    return result;
}

char*
string_after(const char* input, i32 length, char c)
{
    char* end = (char*) (input + length);
    for (char* ptr = (char*) input; ptr != end; ++ptr)
    {
    if (*ptr == c)
    {
	return string_copy(ptr, end - ptr);
    }
    }

    vassert_break();
    return NULL;
}

char*
string_replace_string(char* input, u64 inputLength, char* replaceStr, u64 replaceStrLength, char* newString, u64 newStringLength)
{
    typedef struct ReplaceStringRecord
    {
    u64 StartIndex;
    } ReplaceStringRecord;

    ReplaceStringRecord* records = NULL;

    {
    i32 i = 0;
    char* ptr = input;
    while (*ptr != '\0')
    {
	if (string_compare_length(ptr, replaceStr, replaceStrLength))
	{
	ReplaceStringRecord record = {
	    .StartIndex = i
	};
	array_push(records, record);
	}

	++i;
	++ptr;
    }
    }

    if (records == NULL || newString == NULL || newStringLength == 0)
    return string_copy(input, inputLength);

    i32 i, count = array_count(records),
    lengthDiff = count * (inputLength - replaceStrLength);
    vassert(count > 0 && "count > 0!");

    u64 size = inputLength + lengthDiff + 1;
    char* result = memory_allocate(size);
    memset(result, 0, size);
    memcpy(result, input, records->StartIndex);
    //GINFO("Result: %.*s\n", records->StartIndex, result);
    char* wStr = result + records->StartIndex;
    //GINFO("ReplaceLength: %d\n", replaceStrLength);

    for (i = 0; i < count; ++i)
    {
    ReplaceStringRecord record = records[i];

    memcpy(wStr, newString, newStringLength);
    wStr += newStringLength;

    i32 newReadPos = record.StartIndex + replaceStrLength;
    i32 len;
    if (i != (count - 1))
    {
	ReplaceStringRecord nextRecord = records[i + 1];
	len = nextRecord.StartIndex - newReadPos;
    }
    else
    {
	len = inputLength - newReadPos;
    }

    //GINFO("newReadPos: %d, len: %d\n", newReadPos, len);
    memcpy(wStr, input + newReadPos, len);
    wStr += len;

    //GERROR("Result: %.*s\n", i32(wStr - result), result);
    }

    return result;
}

/*
  3, 5
  "01234567" - len: 8
  "01267" - len: 5
*/
char*
string_cut(const char* input, u32 begin, u32 end)
{
    i32 i, inputLength = string_length(input), resultLength = inputLength - (end - begin + 1);
    char* result;

    assert(input);
    assert(begin >= 0);
    assert(end < inputLength);
    assert(begin < end);
    assert(inputLength);
    assert(resultLength);

    result = memory_allocate(resultLength);
    for (i = 0; i < begin; ++i)
    {
    result[i] = input[i];
    }

    for (i = (end + 1); i < inputLength; ++i)
    {
    result[begin + i - end - 1] = input[i];
    }

    return result;
}

void
string_replace_character(char* pInput, char oldC, char newC)
{
    char* ptr = pInput;

    char c = *pInput;

    while (c != '\0')
    {
    if (c == oldC)
    {
	*ptr = newC;
    }

    ++ptr;
    c = *ptr;
    }

}

char*
string_replace_char(char* input, char c)
{
    vassert_break();
    return NULL;

    /* i32 length = string_length(input); */
    /* i32 ind = 0; */
    /* const i32 bufLen = 2048; */
    /* vassert(length > bufLength && "2048 max size!"); */
    /* char buf[bufLen]; */
    /* memset(buf, '\0', bufLen * sizeof(char)); */

    /* for (i32 i = 0; i < length; ++i) */
    /* { */
    /*  char ch = input[i]; */
    /*  if (ch != c) */
    /*  { */
    /*      buf[ind] = ch; */
    /*      ++ind; */
    /*  } */
    /* } */

    /* return string(buf); */
}

char*
string_trim_char(char* input, u64 length, u64* newLength, char c)
{
    u64 cCount = 0;
    char* ptr = input;
    char pc = *ptr;
    while (pc != '\0')
    {
    if (pc == c)
    {
	++cCount;
    }

    ++ptr;
    pc = *ptr;
    }

    u64 nLength = length - cCount;
    char* newStr = memory_allocate((nLength + 1) * sizeof(char));
    newStr[nLength] = '\0';

    *newLength = nLength;

    ptr = input;
    char* wptr = newStr;
    pc = *ptr;
    while (pc != '\0')
    {
    if (pc != c)
    {
	*wptr = pc;
	++wptr;
    }

    ++ptr;
    pc = *ptr;
    }

    return newStr;
}


char**
string_split(char* input, char splitCharacter)
{
    vassert_not_null(input);

    char** result = string_split_length(input, string_length(input), splitCharacter);

    return result;
}

char**
string_split_length(char* input, u64 inputLength, char splitCharacter)
{
    i32 i,
	wordBeginIndex = 0,
	isWordIndexSet = 0;
    char** result = NULL;

    if (string_index_of(input, splitCharacter) == -1)
    {
	sva_add(result, string(input));
	return result;
    }

    for (i = 0; i < inputLength; ++i)
    {
	char character = input[i];
	if (character != splitCharacter && !isWordIndexSet)
	{
	    isWordIndexSet = 1;
	    wordBeginIndex = i;
	}

	char* word;
	if (character == splitCharacter && isWordIndexSet)
	{
	    isWordIndexSet = 0;

	    word = string_substring_range(input, wordBeginIndex, i - 1);
	    sva_add(result, word);
	}
	else if (i == (inputLength - 1))
	{
	    word = string_substring(input, wordBeginIndex);
	    sva_add(result, word);
	}
    }

    return result;
}

char*
string_join(const char** list, char joinCharacter)
{
    i32 i, listCount, finalLength, curLength = 0, strLength;
    char* finalString = NULL;
    const char* str = NULL;

    assert(list && "List is NULL or Undefined!!!");
    listCount = array_count(list);
    assert(listCount && "List is empty !!!");

    finalLength = listCount;
    for (i = 0; i < listCount; ++i)
    {
    finalLength += string_length(list[i]);
    }

    finalString = (char*) memory_allocate(finalLength);
    for (i = 0; i < (listCount - 1); ++i)
    {
    str = list[i];
    strLength = string_length(str);
    memcpy(finalString + curLength, str, strLength);
    finalString[curLength + strLength] = joinCharacter;
    curLength += strLength + 1;
    }

    str = list[listCount - 1];
    strLength = string_length(str);
    memcpy(finalString + curLength, str, strLength);

    finalString[finalLength - 1] = '\0';

    return finalString;
}

char*
string_join_i32(const i32* list, char joinCharacter)
{
    assert(0 && "Not tested yet, let's write some Unit Test's!");

    char* result = NULL;
    char stringValue[32];

    i32 i, count = array_count(list), el;
    for (i = 0; i < count; ++i)
    {
    el = list[i];
    string_i32(stringValue, el);
    }

    return NULL;
}

void
string_i64(char* input, i64 number)
{
    _string_int(input, number);
}

void
string_f32(char* input, f32 number)
{
    sprintf(input, "%f", number);
}

void
string_f64(char* input, f64 number)
{
    sprintf(input, "%f", number);
}

i32
string_is_integer(char* input, u64 length)
{
    u64 ind = 0;
    i32 flag = 1;
    char* ptr = input;

    while (*ptr)
    {
    if (ind >= length)
	break;

    if (*ptr < '0' || *ptr > '9')
    {
	flag = 0;
	break;
    }

    ++ind;
    }

    return flag;
}

i32
string_to_i32(char* input)
{
    return string_to_i32_length(input, string_length(input));
}

i32
string_to_i32_length(char* input, i32 length)
{
    i32 digit,
    i = 0, result = 0, isNegative = 0,
    multiplier = 10,
    rank = length;

    if (input[0] == '-')
    {
    isNegative = 1;
    i = 1;
    }

    for (; i < rank; ++i)
    {
    digit = input[i] - '0';
    result = (result * multiplier) + digit;
    }

    if (isNegative)
    result *= -1;

    return result;
}

f32
string_to_f32_length(char* input, u64 length)
{
    i32 digit,
    i = 0, isNegative = 0,
    multiplier = 10,
    rank = length;
    f32 intResult = 0,
    floatMultiplier = 0.1;
    char c;

    if (input[0] == '-')
    {
    isNegative = 1;
    i = 1;
    }

    for (; i < rank; ++i)
    {
    c = input[i];
    if (c == '.')
    {
	++i;
	break;
    }

    digit = c - '0';
    intResult = (intResult * multiplier) + digit;
    }

    f32 floatResult = 0.0f;
    for (i32 f = (rank - 1); f >= i; --f)
    {
    c = input[f];

    digit = c - '0';

    /*
      12.318
      ^
      0.8
      0.08 + 0.1 = 0.18
    */

    floatResult = (floatResult * floatMultiplier) + digit * floatMultiplier;
    }

    intResult += floatResult;

    if (isNegative)
    intResult *= -1;

    return intResult;
}

f32
string_to_f32(char* input)
{
    f32 value = string_to_f32_length(input, string_length(input));
    return value;
}

/*

  ###################################
  ###################################
  GlobalHelpers.c
  ###################################
  ###################################

*/
void*
_double_array_create(i32 rows, i32 cols, u64 size)
{
    u64 ptrSize = sizeof(u64);
    u64 colSize = cols * size;
    u64* data = (u64*) memory_allocate(rows * ptrSize);
    for (i32 r = 0; r < rows; ++r)
    {
    data[r] = (u64)memory_allocate(colSize);
    }

    return data;
}

void*
_double_array_destroy(u64* darr, i32 rows)
{
    for (i32 r = 0; r < rows; ++r)
    {
    memory_free((void*)darr[r]);
    }

    memory_free(darr);
    return NULL;
}

i32
string_get_next_i32(char* stream, i32 skipChars, i32* index)
{
    /*
      0123
      ^
    */
    i32 intIndex = 0;
    stream = stream + skipChars;
    char c = *stream;
    const i32 intBufferLength = 128;
    char intBuffer[intBufferLength];

    memset(intBuffer, '\0', intBufferLength * sizeof(char));

    while (c == ' ' || c == '\t' || c == '\n' || c == 'r')
    {
    ++stream;
    c = *stream;
    }

    while (c >= '0' && c <= '9')
    {
    switch (c)
    {
    case '0': case '1': case '2':
    case '3': case '4': case '5':
    case '6': case '7': case '8':
    case '9':
    {
	intBuffer[intIndex] = c;
	++intIndex;
	break;
    }
    }

    ++stream;
    c = *stream;
    }

    i32 result = string_to_i32(intBuffer);

    *index = skipChars + (intIndex + 1);

    return result;
}

/*
  ############################################
  DOCS: SimpleString.h
  ############################################
*/


SimpleString*
simple_string(const char* pStr)
{
    char* pData = simple_string_new(pStr);
    SimpleString* pHeader = simple_string_header(pData);
    return pHeader;
}

char*
simple_string_new(const char* pStr)
{
    if (pStr == NULL)
	return NULL;

    i64 len = simple_core_utf8_length((char*)pStr);
    u64 size = simple_core_utf8_size((char*)pStr);
    char* pAllocatedData = simple_string_copy(pStr, len, size);
    return pAllocatedData;
}

char*
simple_string_copy(const char* pStr, i64 length, i64 size)
{
    SimpleString* pHeader = memory_allocate(sizeof(SimpleString) + size + 1);
    pHeader->pData = ((char*)pHeader) + sizeof(SimpleString);
    memcpy(pHeader->pData, pStr, size);
    pHeader->pData[size] = '\0';

    pHeader->Length = length;
    pHeader->Size = size;

    return pHeader->pData;
}

void
simple_string_destroy(char* pStr)
{
    SimpleString* pHdr = simple_string_header(pStr);
    memory_free(pHdr);
}

char*
simple_string_format(const char* pStr, ...)
{
    va_list list;
    va_start(list, pStr);

    char buf[1024] = {};
    vsnprintf(buf, 1024, pStr, list);
    char* pAllocatedStr = simple_string_new(buf);

    va_end(list);

    return pAllocatedStr;
}

// SimpleString.h

/*

  ###################################
  ###################################
  StringBuilder.c
  ###################################
  ###################################

*/
force_inline i32
sb_string_number_rank(i32 number)
{
    i32 rank = 0;
    for (; ;)
    {
    number /= 10;
    if (number != 0)
    {
	++rank;
    }
    else
    {
	return rank;
    }
    }
}

force_inline i32
sb_string_number_of_rank(i32 number, i32 rank)
{
    if (rank <= 0)
    {
    return number;
    }

    for (i32 i = 0; i < rank; i++)
    {
    number *= 10;
    }

    return number;
}

force_inline i32
sb_string_number_of_digit(i64 number, i8 digit)
{
    i32 i;

    if (sb_string_number_rank(number) < digit)
    {
	return 0;
    }

    if (number < 0)
    {
	number *= -1;
    }

    if (digit == 0)
    {
	return (number % 10);
    }

    number %= sb_string_number_of_rank(1, (digit + 1));
    number /= sb_string_number_of_rank(1, digit);

    return number;
}

char*
_string_builder_grow(char* pBuilder, u64 newCapacity)
{
    u64 headerSize = sizeof(StringBuilderHeader);
    u64 newSize = newCapacity + headerSize;

    StringBuilderHeader* pHeader = NULL;
    if (pBuilder != NULL)
    pHeader = string_builder_header(pBuilder);
    StringBuilderHeader* pNewHeader = (StringBuilderHeader*) memory_allocate(newSize);
    pNewHeader->Capacity = newCapacity;
    pNewHeader->Count = (pBuilder != NULL) ? pHeader->Count : 0;
    pNewHeader->Buffer = (char*) (((char*)pNewHeader) + headerSize);
    memset(pNewHeader->Buffer, '\0', newCapacity);

    if (pBuilder != NULL)
    {
    memcpy(pNewHeader->Buffer, pHeader->Buffer, (pHeader->Count * sizeof(char)));
    memory_free(pHeader);
    }

    return pNewHeader->Buffer;
}

char*
_string_builder_appendf(char* builder, const char* format, ...)
{
    i32 state = 0;
    i32 argumentsCount = 0;
    va_list valist;

    for (char* ptr = (char*)format; *ptr != '\0'; ptr++)
    {
	char c = *ptr;
	if (c == '%')
	{
	    state = 1;
	}
	else if ((state == 1) && (c == 'c' || c == 's' || c == 'd' || c == 'b' || c == 'f'))
	{
	    state = 0;
	    ++argumentsCount;
	}
	else
	{
	    state = 0;
	}
    }

    vassert(argumentsCount > 0 && "No arguments provided!");

    state = 0;
    va_start(valist, format);
    while (*format != '\0')
    {
	char f = *format;

	switch (f)
	{
	case '%':
	{
	    state = 1;
	    break;
	}
	case 'c':
	{
	    if (state == 1)
	    {
		char elementc = (char) va_arg(valist, i32);
		string_builder_appendc(builder, elementc);

		state = 0;
	    }
	    else
	    {
		string_builder_appendc(builder, f);
	    }

	    break;
	}
	case 's':
	{
	    if (state == 1)
	    {
		const char* elements = va_arg(valist, const char *);
		string_builder_appends(builder, (char*) elements);

		state = 0;
	    }
	    else
	    {
		string_builder_appendc(builder, f);
	    }

	    break;
	}
	case 'l':
	{
	    if (state == 1)
	    {
		state = 2;
	    }
	    else
	    {
		string_builder_appendc(builder, f);
	    }

	    break;
	}
	case 'd':
	{
	    if (state == 1)
	    {
		i32 number = va_arg(valist, i32);
		char str[64];
		memset(str, '\0', 64);
		sb_string_i32_to_string(str, number);
		string_builder_appends(builder, str);

		state = 0;
	    }
	    else if (state == 2)
	    {
		i64 number = va_arg(valist, i64);
		char str[64];
		memset(str, '\0', 64);
		sb_string_i64_to_string(str, number);
		string_builder_appends(builder, str);

		state = 0;
	    }
	    else
	    {
		string_builder_appendc(builder, f);
	    }

	    break;
	}
	case 'b':
	{
	    if (state == 1)
	    {
		u8 number = (u8) va_arg(valist, i32);
		char str[4];
		memset(str, '\0', 4);
		sb_string_i32_to_string(str, number);
		string_builder_appends(builder, str);

		state = 0;
	    }
	    else
	    {
		string_builder_appendc(builder, f);
	    }

	    break;
	}
	case 'f':
	{
	    if (state == 1)
	    {
		f64 f64Number = va_arg(valist, f64);
		char str[64];
		memset(str, '\0', 64);
		sb_string_f64_to_string(str, f64Number);
		string_builder_appends(builder, str);

		state = 0;
	    }
	    else
	    {
		string_builder_appendc(builder, f);
	    }

	    break;
	}
	default:
	{
	    string_builder_appendc(builder, f);
	    state = 0;

	    break;
	}

	}

	format++;
    }

    va_end(valist);

    return builder;
}

/*

  ###################################
  ###################################
  HashTable.c
  ###################################
  ###################################

*/
i32
hash_table_i32_pow(i32 x, i32 n)
{
    i32 result = 1;
    while (n)
    {
    result *= x;
    --n;
    }

    return result;
}


/*
  Hash Table Statistics (for profiling purposes)
*/
#if HASH_TABLE_PROFILING == 1

TableStatistics gStatistics;

TableStatistics
table_get_statistics()
{
    return gStatistics;
}
#endif

/*
  Base
*/


/*
  DOCS(typedef): String Hash Table (const char* Key)
*/
typedef struct InternalStringElement
{
    const char* Key;
    void* Data;
} InternalStringElement;

typedef struct InternalIntElement
{
    i32 Key;
    void* Data;
} InternalIntElement;

i32
_get_prime(TableHeader* header)
{
    static i32 HashTablePrimes[] = {
    53, 97, 193, 389,
    769, 1543, 3079, 6151,
    12289, 24593, 49157, 98317,
    196613, 393241, 786433, 1572869,
    3145739, 6291469, 12582917, 25165843,
    50331653, 100663319, 201326611, 402653189,
    805306457, 1610612741
    };

    i32 nextIndex = (header == NULL) ? 0 : MinMax(header->NextPrime, 0, 24);
    i32 prime = HashTablePrimes[nextIndex];
    return prime;
}

void*
_table_new(void* table, u64 elemSize, i32 defVal)
{
    i32 prime = _get_prime(NULL);
    u64 newSize = prime * elemSize;
    TableHeader* newHeader = (TableHeader*) HashTableAllocate(newSize + sizeof(TableHeader));

    TableHeader tableHeader = {
    .Count = 0,
    .Capacity = prime,
    .ElementSize = elemSize,
    .NextPrime = 1,
    .Buffer = ((void*)newHeader) + sizeof(TableHeader)
    };
    *newHeader = tableHeader;

    memset(newHeader->Buffer, defVal, newSize);

    return newHeader->Buffer;
}

void*
_table_grow(void* table, u64 elemSize, i32 defVal)
{
    vassert_not_null(table);
#if STATIC_ANALIZER_CHECK == 1
    if (!table) return table;
#endif

    TableHeader* prevHeader = table_header(table);
    i32 prime = _get_prime(prevHeader);
    u64 newSize = prime * elemSize;
    TableHeader* newHeader = (TableHeader*) HashTableAllocate(newSize + sizeof(TableHeader));
    TableHeader tableHeader = {
    .Count = 0,
    .Capacity = prime,
    .ElementSize = elemSize,
    .NextPrime = prevHeader->NextPrime + 1,
    .Buffer = ((void*)newHeader) + sizeof(TableHeader)
    };
    *newHeader = tableHeader;

    memset(newHeader->Buffer, defVal, newSize);

    return newHeader->Buffer;
}

force_inline u64
shash(const char* key, i32 prime, i32 bucketNumber)
{
#if USE_OLD_GET_SHASH_ALGO

    vassert(bucketNumber != 0);
    u64 shash = 0;
    i32 keyLength = string_length(key);
    for (i32 i = 0; i < keyLength; i++)
    {
    shash += hash_table_i32_pow(prime, (keyLength - (i + 1))) * key[i];
    shash %= bucketNumber;
    }
    return shash;

#else // djb2
    u64 hash = 5381;

    u64 length = string_length(key);
    char* end = ((char*)key) + length;
    for (char* ptr = (char*) key; ptr != end; ++ptr)
    {
    hash = ((hash << 5) + hash) + *ptr;
    }
    return hash;

#endif

}

force_inline i32
_get_shash(const char* key, i32 bucketNumber, i32 attempt)
{
    const i32 PRIME_1 = 117;
    const i32 PRIME_2 = 119;

    u64 hashA = shash(key, PRIME_1, bucketNumber);
    u64 hashB = shash(key, PRIME_2, bucketNumber);
    return (hashA + (attempt * (hashB + 1))) % bucketNumber;
}

void
_base_shash_put(void* table, const char* key)
{
    vassert_not_null(table);

    TableHeader* header = table_header(table);

    i32 i = 0,
	index,
	keyLength = string_length(key);
    char* itemsKey = (char*) key;
    do
    {
	index = _get_shash(itemsKey, header->Capacity, i);
	itemsKey = *((char**) (table + index * header->ElementSize));
	++i;

	if (itemsKey == NULL)
	{
	    ++header->Count;
	    break;
	}

	if (i >= header->Capacity)
	{
	    index = -1;
	    break;
	}
    }
    while (!string_compare_length(itemsKey, key, keyLength));

    // NOTE(typedef): always set to index wo any checks
    header->Index = index;

#if HASH_TABLE_PROFILING == 1
    gStatistics.PutAttempt = i;
#endif
}

void
_base_shash_get(void* table, const char* key)
{
    vassert_not_null(table);

    TableHeader* header = table_header(table);

    char* keyValue = (char*) key;
    char* itemsKey = (char*) key;
    i64 i = 0,
	index,
	keyLength = simple_core_utf8_length(keyValue);

    do
    {
	index = _get_shash(itemsKey, header->Capacity, i);
	itemsKey = *((char**) (table + index * header->ElementSize));
	++i;

	if (itemsKey == NULL || i >= header->Capacity)
	{
	    index = -1;
	    break;
	}

    }
    while (!string_compare_length(itemsKey, keyValue, keyLength));

    header->Index = index;
#if HASH_TABLE_PROFILING == 1
    gStatistics.GetAttempt = i;
#endif
}

/*
  DOCS(typedef): Int Hash Table (int Key)
*/

force_inline i32
hash(i32 key)
{
#if 1

    u32 ukey = (u32) key;
    ukey = ((ukey >> 16) ^ ukey) * 0x45d9f3b;
    ukey = ((ukey >> 16) ^ ukey) * 0x45d9f3b;
    ukey = (ukey >> 16) ^ ukey;
    return to_i32(key % I32_MAX_HALF);

#else

    u32 ukey = (u32) key;
    ukey += ~(key << 9);
    ukey ^= ((key >> 14) | (key << 18));
    ukey += (key << 4);
    ukey ^= ((key >> 10) | (key << 22));
    return to_i32(key);

#endif
}

force_inline i32
get_hash(i32 key, i32 bucketNumber, i32 attempt)
{
    i32 hashA = hash(key);
    i32 hashB = attempt * (hash(key) + 1);
    return (hashA + hashB) % bucketNumber;
}

void*
_base_hash_put(void* table, i32 key)
{
    vassert_not_null(table);

    TableHeader* header = table_header(table);

    i32 i = 0,
	index,
	itemsKey = key;
    do
    {
	index = get_hash(itemsKey, header->Capacity, i);
	itemsKey = *((i32*) (table + index * header->ElementSize));
	++i;

	if (itemsKey == -1)
	{
	    ++header->Count;
	    break;
	}

	if (i >= header->Capacity)
	{
	    index = -1;
	    break;
	}

    } while (itemsKey != key);

    header->Index = index;

#if HASH_TABLE_PROFILING == 1
    gStatistics.PutAttempt = i;
#endif

    return table;
}

void*
_base_hash_get(void* table, i32 key)
{
    vassert_not_null(table);

    TableHeader* header = table_header(table);
    i32 i = 0, index, itemsKey = key;
    do
    {
	index = get_hash(itemsKey, header->Capacity, i);
	itemsKey = *((i32*) (table + index * header->ElementSize));
	++i;

	if (itemsKey == -1 || i >= header->Capacity)
	{
	    index = -1;
	    break;
	}

    } while (itemsKey != -1 && itemsKey != key);

    header->Index = index;

#if HASH_TABLE_PROFILING == 1
    gStatistics.GetAttempt = i;
#endif

    return table;
}

/*

  #####################################
  #####################################
  IO.c
  #####################################
  #####################################

*/

#if defined(PLATFORM_LINUX)

#include <sys/stat.h>  // mkdir
#include <sys/types.h>  // mkdir

char*
file_read_string_ext(const char* filePath, u64* length)
{
    FILE* file;
    char* result;
    i32 fileLength;

    file = fopen(filePath, "r");
    if (file)
    {
    fseek(file, 0, SEEK_END);
    fileLength = (ftell(file));
    fseek(file, 0, SEEK_SET);
    result = memory_allocate((fileLength + 1) * sizeof(char));

    fread(result, sizeof(char), (fileLength), file);
    result[fileLength] = '\0';

    fclose(file);

    *length = fileLength;
    return((char*)result);
    }

    *length = 0;
    return NULL;
}

char*
file_read_string(const char* filePath)
{
    u64 length;
    return file_read_string_ext(filePath, &length);
}

char*
file_get_name_with_extension(const char* path)
{
    char* pName = path_get_name(path);
    i32 len = string_length(pName);
    char* pAllocated = memory_allocate(len);
    memcpy(pAllocated, pName, len);
    return pAllocated;
}

void
file_write_string(const char* filePath, char* data, u64 len)
{
    FILE* file = fopen(filePath, "w");
    vassert_not_null(file);
#if STATIC_ANALIZER_CHECK == 1
    if (!file) return;
#endif
    fwrite(data, 1, len, file);
    fclose(file);
}

void
file_write_bytes(const char* filePath, u8* data, u64 len)
{
    FILE* file = fopen(filePath, "wb");
    vassert_not_null(file);
#if STATIC_ANALIZER_CHECK == 1
    if (!file) return;
#endif
    fwrite(data, 1, len, file);
    fclose(file);
}

i32
file_write_string_exe(const char* filePath, char* data, u64 len)
{
    i32 fileDescriptor = open(filePath, O_RDWR | O_CREAT | O_EXCL, S_IRWXU);
    if (fileDescriptor >= 0)
    {
    write(fileDescriptor, data, len);
    close(fileDescriptor);
    return 1;
    }
    return 0;
}

void
file_append_string(const char* filePath, char* data, u64 len)
{
    FILE* file;
    file = fopen(filePath, "a+");
    fwrite(data, 1, len, file);
    fclose(file);
}

u8*
file_read_bytes_ext(const char* filePath, u64* sizePtr)
{
    FILE* file;
    u8* result;
    u64 size;

    file = fopen(filePath, "rb");
    if (file)
    {
    fseek(file, 0, SEEK_END);
    size = (i32)ftell(file);
    fseek(file, 0, SEEK_SET);
    result = memory_allocate(size * sizeof(u8));

    fread(result, sizeof(u8), size, file);
    *sizePtr = size;

    fclose(file);
    return (u8*) result;
    }

    return NULL;
}

u8*
file_read_bytes(const char* filePath)
{
    u64 size;
    u8* result = file_read_bytes_ext(filePath, &size);
    return result;
}

i32
file_get_size(const char* filePath)
{
    FILE* file = fopen(filePath, "rb");
    if (file)
    {
    fseek(file, 0, SEEK_END);
    i32 fileLength = (i32)ftell(file);
    fclose(file);
    return fileLength;
    }

    return 0;
}

i32
platform_directory_create(const char* name)
{
    /*
      #define S_IRWXU 0000700    RWX mask for owner
      #define S_IRUSR 0000400    R for owner
      #define S_IWUSR 0000200    W for owner
      #define S_IXUSR 0000100    X for owner

      #define S_IRWXG 0000070    RWX mask for group
      #define S_IRGRP 0000040    R for group
      #define S_IWGRP 0000020    W for group
      #define S_IXGRP 0000010    X for group

      #define S_IRWXO 0000007    RWX mask for other
      #define S_IROTH 0000004    R for other
      #define S_IWOTH 0000002    W for other
      #define S_IXOTH 0000001    X for other

      #define S_ISUID 0004000    set user id on execution
      #define S_ISGID 0002000    set group id on execution
      #define S_ISVTX 0001000    save swapped text even after use
    */
    i32 result = mkdir(name, S_IRWXU);
    if (result != 0)
    {

    return 0;
    }

    return 1;
}

void
file_delete(const char* pFilePath)
{
    unlink(pFilePath);
}

#elif defined(PLATFORM_WINDOWS)

char*
file_read_string_ext(const char* filePath, u64* length)
{
    HANDLE fileHandle = CreateFileA(filePath, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    if (fileHandle == INVALID_HANDLE_VALUE)
    {
    *length = 0;
    return NULL;
    }

    LARGE_INTEGER byteSize;
    BOOL isSucessed = GetFileSizeEx(fileHandle, &byteSize);

    u64 newSize = byteSize.QuadPart + 1;
    *length = byteSize.QuadPart;

    char* fileBuffer = (char*) memory_allocate(newSize);
    fileBuffer[byteSize.QuadPart] = '\0';
    BOOL isReadSuccessed = ReadFile(fileHandle, fileBuffer, byteSize.QuadPart, NULL, NULL);

    CloseHandle(fileHandle);

    return (char*) fileBuffer;
}

char*
file_read_string(const char* filePath)
{
    u64 length;
    return file_read_string_ext(filePath, &length);
}

char*
file_get_name_with_extension(const char* path)
{
    return NULL;
}

void
file_write_string(const char* filePath, char* data, u64 len)
{
    file_write_bytes(filePath, (u8*)data, len);
}

void
file_write_bytes(const char* filePath, u8* data, u64 len)
{
    HANDLE fileHandle = CreateFileA(filePath, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
    if (fileHandle == INVALID_HANDLE_VALUE)
    return;

    BOOL isWriteFile = WriteFile(fileHandle, data, len, NULL, NULL);
}

i32
file_write_string_exe(const char* filePath, char* data, u64 len)
{
    HANDLE fileHandle = CreateFileA(filePath, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
    if (fileHandle == INVALID_HANDLE_VALUE)
    return 0;

    BOOL isWriteFile = WriteFile(fileHandle, data, len, NULL, NULL);
    return isWriteFile;
}

void
file_append_string(const char* filePath, char* data, u64 len)
{
}

u8*
file_read_bytes_ext(const char* filePath, u64* length)
{
    HANDLE fileHandle = CreateFileA(filePath, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    if (fileHandle == INVALID_HANDLE_VALUE)
    {
    *length = 0;
    return NULL;
    }

    LARGE_INTEGER byteSize;
    BOOL isSucessed = GetFileSizeEx(fileHandle, &byteSize);

    u64 newSize = byteSize.QuadPart;
    *length = newSize;

    char* fileBuffer = (char*) memory_allocate(newSize);
    BOOL isReadSuccessed = ReadFile(fileHandle, fileBuffer, newSize, NULL, NULL);

    CloseHandle(fileHandle);

    return (u8*) fileBuffer;
}

u8*
file_read_bytes(const char* filePath)
{
    u64 size;
    u8* bytes = file_read_bytes_ext(filePath, &size);
    return bytes;
}

i32
file_get_size(const char* filePath)
{
    HANDLE fileHandle = CreateFileA(filePath, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    if (fileHandle == INVALID_HANDLE_VALUE)
    return -1;

    LARGE_INTEGER byteSize;
    BOOL isSucessed = GetFileSizeEx(fileHandle, &byteSize);
    if (!isSucessed)
    return -1;

    return byteSize.QuadPart;
}

i32
platform_directory_create(const char* name)
{
    if (!CreateDirectoryA(name, NULL))
    return 0;
    return 1;
}

void
file_delete(const char* pFilePath)
{
    // todo: impl for windows
    //unlink(pFilePath);

    //The following list identifies some tips for deleting, removing, or closing files:

    // To delete a read-only file, first you must remove the read-only attribute.
    // To delete or rename a file, you must have either delete permission on the file, or delete child permission in the parent directory.
    // To recursively delete the files in a directory, use the SHFileOperation function.
    // To remove an empty directory, use the RemoveDirectory function.
    // To close an open file, use the CloseHandle function.

    DeleteFile(pFilePath);
}


#else
#error "No platform support!"
#endif

/*

  #####################################
  #####################################
  Path.c
  #####################################
  #####################################

*/

static char CurrentDirectory[4096] = "\0";
static IElement** Elements = NULL;

char*
ielement(const char* directory, const char* name)
{
    i32 i, count = array_count(Elements);
    for (i = 0; i < count; ++i)
    {
    IElement* ielement = Elements[i];
    if (string_compare(ielement->Directory, directory)
	&& string_compare(ielement->Name, name))
    {
	return ielement->AbsolutePath;
    }
    }

    i32 lastSlashIndex = string_last_index_of(name, '/');
    i32 extIndex = string_last_index_of(name, '.');

    i32 dirLength = string_length(directory);
    i32 nameLength = string_length(name);
    i32 absoluteLength = dirLength + 1 + nameLength;

    if (lastSlashIndex != -1)
    {
    --absoluteLength;
    }

    u64 headerSize = sizeof(IElement);
    i32 dirSize = dirLength * sizeof(char);
    u64 nameSize = nameLength * sizeof(char);
    u64 absolutePathSize = absoluteLength * sizeof(char);

    /*
      /home/bies/dir/file.ext

      char* AbsolutePath;       /home/bies/dir/file.ext => new
      char* Name;               file\0                  => new
      char* Directory;          /home/bies/dir/\0       => new
      char* NameWithExtension;  file.ext\0              => poap
      char* Extension;          .ext\0                  => poap
    */

    IElement* element = memory_allocate(headerSize
		    + absolutePathSize + 1
		    + nameSize + 1
		    + dirSize + 1);
    element->AbsolutePathLength = absoluteLength;
    element->DirLength = dirLength;
    element->NameLength = nameLength;
    element->AbsolutePath = ((void*)element) + headerSize;
    element->Directory = ((void*)element->AbsolutePath) + absolutePathSize + 1;
    element->Name = ((void*)element->Directory) + dirSize + 1;
    if (lastSlashIndex != -1)
    {
    element->NameWithExtension = element->AbsolutePath + lastSlashIndex + 1;
    }
    else
    {
    element->NameWithExtension = STRING_NULL;
    }
    if (extIndex != -1 && extIndex != 0)
    {
    element->Extension = element->Name + extIndex + 1;
    }
    else
    {
    element->Extension = STRING_NULL;
    }

    memset(element->AbsolutePath, '\0', (absolutePathSize + 1));
    memset(element->Directory, '\0', (dirSize + 1));
    memset(element->Name, '\0', (nameSize + 1));

    char lastChar = directory[dirLength - 1];
    memcpy(element->AbsolutePath, directory, dirSize);
    if (lastChar != '/' && lastChar != '\\')
    {
    memcpy(element->AbsolutePath + dirSize, "/", sizeof(char));
    memcpy(element->AbsolutePath + dirSize + 1, name, nameSize);
    }
    else
    {
    memcpy(element->AbsolutePath + dirSize, name, nameSize);
    }
    memcpy(element->Directory, directory, dirSize);
    memcpy(element->Name, name, nameSize);

    array_push(Elements, element);

    return element->AbsolutePath;
}

void
ielement_free_all()
{
    i32 i, count = array_count(Elements);
    for (i = 0; i < count; ++i)
    {
    IElement* element = Elements[i];
    memory_free(element);
    }

    array_free(Elements);

    Elements = NULL;
}

u8
path(const char* path)
{
#if defined(PLATFORM_LINUX)
    struct stat fileInfo;

    if (stat(path, &fileInfo) != 0)
    {
    return PATH_IS_SOMETHING;
    }

    if (S_ISDIR(fileInfo.st_mode))
    {
    return PATH_IS_DIRECTORY;
    }
    else if (S_ISREG(fileInfo.st_mode))
    {
    return PATH_IS_FILE;
    }

    return PATH_IS_SOMETHING;
#elif defined(PLATFORM_WINDOWS)
    DWORD fileAttribute = GetFileAttributes(path);
    if (fileAttribute == INVALID_FILE_ATTRIBUTES)
    {
    GERROR("GetFileAttributes failed!\n");
    vassert_break();
    }

    if (fileAttribute == FILE_ATTRIBUTE_DIRECTORY)
    {
    return PATH_IS_DIRECTORY;
    }
    else
    {
    return PATH_IS_FILE;
    }
#else
#error "Unsupported platform!"
#endif
}

#if defined(PLATFORM_WINDOWS)
//TODO(typedef): create this function
//TODO(typedef): make this function cross platform
static void
io_get_file_times_ms(const char* path, i64* accessTime, i64* creationTime, i64* writeTime)
{
    HANDLE file = CreateFileA(path, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    if (file == INVALID_HANDLE_VALUE)
    {
	GERROR("Can't open file: %s\n", path);
	vassert_break();
    }
    FILETIME ct, at, wt;
    BOOL result = GetFileTime(file, &ct, &at, &wt);
    if (result == FALSE)
    {
	GERROR("Can't get file time: %s\n", path);
	vassert_break();
    }

    SYSTEMTIME systemAccessTime;
    FileTimeToSystemTime(&at, &systemAccessTime);
    SYSTEMTIME systemCreationTime;
    FileTimeToSystemTime(&ct, &systemCreationTime);
    SYSTEMTIME systemWriteTime;
    FileTimeToSystemTime(&wt, &systemWriteTime);

    *accessTime = systemAccessTime.wMilliseconds;
    *creationTime = systemCreationTime.wMilliseconds;
    *writeTime = systemWriteTime.wMilliseconds;
}
#endif

u64
path_get_last_access_time_raw(const char* path)
{
#if defined(PLATFORM_LINUX)
    struct stat fileItemInfo;
    stat(path, &fileItemInfo);
    return fileItemInfo.st_atime;
#elif defined(PLATFORM_WINDOWS)
//#warning "This function should return generic cross platform struct FileTime"
    i64 at, ct, wt;
    io_get_file_times_ms(path, &at, &ct, &wt);
    return at;
#else
#error "Unsupported platform!"
#endif
}

u64
path_get_last_modification_time_raw(const char* path)
{
#if defined(PLATFORM_LINUX)
    struct stat fileItemInfo;
    stat(path, &fileItemInfo);
    return fileItemInfo.st_mtime;
#elif defined(PLATFORM_WINDOWS)
    i64 at, ct, wt;
    io_get_file_times_ms(path, &at, &ct, &wt);
    return wt;
#else
#error "Unsupported platform!"
#endif
}

u64
path_get_last_creation_time_raw(const char* path)
{
#if defined(PLATFORM_LINUX)
    struct stat fileItemInfo;
    stat(path, &fileItemInfo);
    return fileItemInfo.st_ctime;
#elif defined(PLATFORM_WINDOWS)
    i64 at, ct, wt;
    io_get_file_times_ms(path, &at, &ct, &wt);
    return ct;
#else
#error "Unsupported platform!"
#endif
}

char*
path_get_home_directory()
{
#if defined(PLATFORM_LINUX)
    static struct passwd* g_UserInfo = NULL;
    if (!g_UserInfo)
    {
    g_UserInfo = getpwuid(geteuid());
    }

    return g_UserInfo->pw_dir;
#elif defined(PLATFORM_WINDOWS)
    return "C:/";
#else
#error "Unsupported platform!"
#endif
}

const char*
path_get_extension(const char* path)
{
    /*
      DOCS(typedef): this("path/to/file.txt") -> ".txt"
    */
    i32 extensionIndex = string_last_index_of(path, '.');
    return (const char*)(path + extensionIndex * sizeof(char));
}

const char*
path_get_name(const char* path)
{
    i32 extensionIndex = string_last_index_of(path, '/');
    return (const char*)(path + (extensionIndex + 1) * sizeof(char));
}

char*
path_get_name_wo_extension(const char* path)
{
    const char* nameWithExt = path_get_name(path);
    i32 ind = string_index_of(nameWithExt, '.');
    char* nameWoExt = string_substring_length(
    nameWithExt,
    string_length(nameWithExt),
    ind);
    return nameWoExt;
}

char*
path_get_directory(const char* path)
{
    //"path/one/t";
    //;0123456789;;
    i32 extensionIndex = string_last_index_of(path, '/');
    char* newPath = (char*)memory_allocate((extensionIndex + 1) * sizeof(char));
    newPath[extensionIndex] = '\0';
    memcpy(newPath, path, extensionIndex * sizeof(char));
    return newPath;
}

#if defined(PLATFORM_LINUX)
char*
path_combine(const char* left, const char* right)
{
    vassert_not_null(left );
    vassert_not_null(right);

    /*
      1. Adding paths:
      * path_combine("Image/", "a.png");
      * path_combine("Image/", "/a.png");
      * path_combine("Image/", "./a.png");

      */

    i64 leftLength = string_length(left);
    i64 rightLength = string_length(right);

    i32 isLeftSlashed = path_contains_slash(left, leftLength);

    char* leftMod  = NULL;
    char* rightMod = NULL;
    i64 leftModLen;
    i64 rightModLen;

    if (isLeftSlashed)
    {
    leftModLen = leftLength - 1;
    leftMod = string_copy(left, leftModLen);
    }
    else
    {
    leftModLen = leftLength;
    leftMod = string_copy(left, leftLength);
    }

    { // DOCS(typedef): set rightMod
    char* ptr = (char*) right;
    char c = *ptr;
    while (c == '.' || c == '/' || c == '\\' || c == ' ' || c == '\n' || c == '\t' || c == '\r')
    {
	++ptr;
	c = *ptr;
    }

    // DOCS(typedef): right is empty string for us
    if (*ptr == '\0')
    {
	if (leftMod)
	memory_free(leftMod);
	return string(right);
    }

    rightMod = string(ptr);
    rightModLen = string_length(rightMod);
    }

    char* result = string_concat3l(
    leftMod   , "/", rightMod,
    leftModLen,  1 , rightModLen);
    // GINFO("Result: %s\n", result);

    if (isLeftSlashed)
    {
    memory_free(leftMod);
    }

    memory_free(rightMod);

    return result;
}

#elif defined(PLATFORM_WINDOWS)

char*
path_combine(const char* left, const char* right)
{
    vassert_not_null(left);
    vassert_not_null(right);

    char outPath[MAX_PATH] = {};

    PathCombineA(outPath, left, right);
    i64 resultLen = string_length(outPath);
    char* resultAllocated = memory_allocate(resultLen + 1);
    resultAllocated[resultLen] = '\0';
    memcpy(resultAllocated, outPath, resultLen);
    return resultAllocated;
}

#endif

char*
path_combine3(const char* left, const char* mid, const char* right)
{
    char* lm = path_combine(left, mid);
    char* lmr = path_combine(lm, right);
    memory_free(lm);
    return lmr;
}

char*
path_combine_directory_and_name(const char* path, char* name)
{
    char* dirPath = path_get_directory(path);
    char* result = path_combine(dirPath, name);
    memory_free((void*)dirPath);
    return result;
}

const char*
path_combine_interning(const char* left, const char* right)
{
    assert(left != NULL && "Left can't be NULL!");
    assert(right != NULL && "Right can't be NULL!");

    char* path = path_combine(left, right);
    const char* iPath = istring(path);
    memory_free(path);
    return iPath;
}

i32
path_contains_slash(const char* path, i64 pathLength)
{
    char windowsSlash = '\\';
    char normalSlash = '/';
    i64 slashInd;

#define SlashCheck(p, s)                                        \
    {                                                           \
    slashInd = string_last_index_of(p, s);                  \
    if (slashInd != -1 && (slashInd == (pathLength - 1)))   \
    {                                                       \
	return 1;                                           \
    }                                                       \
    }

    SlashCheck(path, normalSlash);
    SlashCheck(path, windowsSlash);

    return 0;
}


const char*
path_get_current_directory()
{
#if defined(PLATFORM_LINUX)
    if (CurrentDirectory[0] == '\0')
    getcwd(CurrentDirectory, 4096);
#elif defined(PLATFORM_WINDOWS)
    if (CurrentDirectory[0] == '\0')
    GetModuleFileName(NULL, CurrentDirectory, MAX_PATH);
#endif

    return (const char*)CurrentDirectory;
}

char*
path_get_absolute(char* path)
{
    const char* currentDirectory = path_get_current_directory();
    char* absolutePath = string_concat3(currentDirectory, PATH_SEPARATOR_STRING, path);
    return absolutePath;
}

i32
path_is_file_exist(const char* path)
{
#if defined(PLATFORM_LINUX)
    struct stat buf;
    i32 result = stat(path, &buf);
    return result != -1;
#elif defined(PLATFORM_WINDOWS)
    BOOL isFileExist = PathFileExistsA(path);
    return isFileExist;
#endif
}

i32
path_is_directory_exist(const char* path)
{
#if defined(PLATFORM_LINUX)
    struct stat buf;
    i32 result = stat(path, &buf);
    return result != -1;
#elif defined(PLATFORM_WINDOWS)
    DWORD result = GetFileAttributesA(path);
    return (result != INVALID_FILE_ATTRIBUTES && (result & FILE_ATTRIBUTE_DIRECTORY));
#endif
}

char*
path_get_filename(const char* path)
{
    i32 index = string_last_index_of(path, '/');
    if (index)
    {
    char* result = string_substring(path, (index + 1));
    return result;
    }

    return NULL;
}

const char*
path_get_filename_interning(const char* path)
{
    //index can be -1
    i32 index = string_last_index_of(path, '/');
    const char* iPath = istring(path + index + 1);
    return iPath;
}

char*
path_get_prev_directory(const char* currentDirectory)
{
    if (!string_compare(currentDirectory, ROOT_DIRECTORY))
    {
    i32 index = string_last_index_of(currentDirectory, '/');
    if (index != 0)
    {
	--index;
    }

    char* prevDirectoryPath = string_substring_range(currentDirectory, 0, index);
    return prevDirectoryPath;
    }

    return NULL;
}

const char*
path_get_prev_directory_interning(const char* currentDirectory)
{
    char* prevDirectory = path_get_prev_directory(currentDirectory);
    const char* iPrevDirectory = istring(prevDirectory);
    memory_free(prevDirectory);
    return iPrevDirectory;
}

#if defined(PLATFORM_LINUX)
static i32
path_string_comparer(const struct dirent** a, const struct dirent** b)
{
    char* left = (char*)(*a)->d_name;
    char* right = (char*)(*b)->d_name;
    u32 leftLength = string_length(left);
    u32 rightLength = string_length(right);

    for (u32 i = 0; i < leftLength; i++)
    {
    char l = char_to_lower(left[i]);
    char r = char_to_lower(right[i]);

    if (l < r)
    {
	return 1;
    }
    else if (l > r)
    {
	return -1;
    }
    }

    return (rightLength - leftLength);
}

i32
directory_create(const char* name)
{
    return platform_directory_create(name);
}

i32
path_directory_create(const char* path)
{
    return platform_directory_create(path);
}

force_inline const char**
_directory_get(const char* directory, i32 elemCode)
{
    const char** elements = NULL;
    struct dirent** namelist = NULL;
    i32 n = scandir(directory, &namelist, 0, path_string_comparer);

    while (n > 0)
    {
    const char* dName = namelist[n - 1]->d_name;
    assert(dName);

    if (dName[0] == '.')
    {
	--n;
	continue;
    }

    char* absolutePath = ielement(directory, dName);
    if (path(absolutePath) == elemCode)
    {
	array_push(elements, absolutePath);
    }

    free(namelist[n - 1]);

    --n;
    }

    free(namelist);

    return elements;
}

const char**
path_directory_get_files(const char* directory)
{
    const char** files = _directory_get(directory, PATH_IS_FILE);
    return files;
}

const char**
path_directory_get_directories(const char* directory)
{
    const char** dirs = _directory_get(directory, PATH_IS_DIRECTORY);
    return dirs;
}
#elif defined(PLATFORM_WINDOWS)

i32
path_directory_create(const char* dirPath)
{
    i32 result = platform_directory_create(dirPath);
    return result;
}

static char**
_directory_get(const char* directory, i32 elemCode)
{
    char** elements = NULL;
    WIN32_FIND_DATA findData;
    HANDLE firstFile = FindFirstFile(directory, &findData);
    if (firstFile == INVALID_HANDLE_VALUE)
    {
    return NULL;
    }

    do
    {
    if (elemCode == 0)
    {
	if (!(findData.dwFileAttributes & elemCode))
	{
	array_push(elements, findData.cFileName);
	}
    }
    else
    {
	if (findData.dwFileAttributes & elemCode)
	{
	array_push(elements, findData.cFileName);
	}
    }
    } while (FindNextFile(firstFile, &findData) != 0);

    return elements;
}

const char**
path_directory_get_files(const char* directory)
{
    const char** files = (const char**) _directory_get(directory, 0);
    return files;
}

const char**
path_directory_get_directories(const char* directory)
{
    const char** dirs = (const char**) _directory_get(directory, FILE_ATTRIBUTE_DIRECTORY);
    return dirs;
}
#endif // PLATFORM_WINDOWS

i32
path_is_inside(const char* path, const char* item)
{
    // NOTE(): Only for Linux now
    char** parentDirs = string_split((char*)path, '/');
    char** itemDirs = string_split((char*)item, '/');

    i32 parentCnt = array_count(parentDirs);
    i32 itemCnt = array_count(itemDirs);

    if (parentCnt <= 0 || itemCnt <= 0)
    return 0;

    if (parentCnt >= itemCnt)
    return 0;

    for (i32 i = 0; i < parentCnt; ++i)
    {
    char* pdir = parentDirs[i];
    char* idir = itemDirs[i];

    if (!string_compare(pdir, idir))
	return 0;
    }

    return 1;
}

/*

  #####################################
  #####################################
  Profiler.c
  #####################################
  #####################################

*/

void
simple_profiler_start(SimpleProfiler* state)
{
#if defined(PLATFORM_LINUX)
    clock_gettime(CLOCK_REALTIME, &state->Start);
#elif defined(PLATFORM_WINDOWS)
    QueryPerformanceCounter(&state->Start);
#endif
}

void
simple_profiler_end(SimpleProfiler* state)
{
#if defined(PLATFORM_LINUX)
    clock_gettime(CLOCK_REALTIME, &state->End);
    state->Result = (1000 * 1000 * 1000 * (state->End.tv_sec - state->Start.tv_sec)) + (state->End.tv_nsec - state->Start.tv_nsec);
#elif defined(PLATFORM_WINDOWS)
    QueryPerformanceCounter(&state->End);
    state->Result = state->End.QuadPart - state->Start.QuadPart;
#endif
}

ProfilerTimeType
simple_profiler_get_time_type(SimpleProfiler* state)
{
    if (SIMPLE_PROFILER_NS_TO_S(state->Result))
    {
    return SIMPLE_PROFILER_TIME_S;
    }
    else if (SIMPLE_PROFILER_NS_TO_MS(state->Result))
    {
    return SIMPLE_PROFILER_TIME_MS;
    }
    else if (SIMPLE_PROFILER_NS_TO_MCS(state->Result))
    {
    return SIMPLE_PROFILER_TIME_MCS;
    }
    else if (state->Result)
    {
    return SIMPLE_PROFILER_TIME_NS;
    }

    assert(0 && "");
    return SIMPLE_PROFILER_TIME_NS;
}

i64
simple_profiler_get_nanoseconds(SimpleProfiler* state)
{
    return state->Result;
}

i64
simple_profiler_get_microseconds(SimpleProfiler* state)
{
    return state->Result / 1000;
}

i64
simple_profiler_get_milliseconds(SimpleProfiler* state)
{
    return state->Result / (1000 * 1000);
}

i64
simple_profiler_get_seconds(SimpleProfiler* state)
{
    return state->Result / (1000 * 1000 * 1000);
}

f64
simple_profiler_get_microseconds_as_float(SimpleProfiler* state)
{
    return ((f64)state->Result) / 1000;
}

f64
simple_profiler_get_milliseconds_as_float(SimpleProfiler* state)
{
    return ((f64)state->Result) / (1000 * 1000);
}

f64
simple_profiler_get_seconds_as_float(SimpleProfiler* state)
{
    return ((f64)state->Result) / (1000 * 1000 * 1000);
}

void
simple_profiler_print(SimpleProfiler* state)
{
    ProfilerTimeType timeType = simple_profiler_get_time_type(state);
    switch (timeType)
    {
    case SIMPLE_PROFILER_TIME_NS:
    printf("%lld %s\n", simple_profiler_get_nanoseconds(state), "ns");
    break;
    case SIMPLE_PROFILER_TIME_MCS:
    printf("%lld %s\n", simple_profiler_get_microseconds(state), "mcs");
    break;
    case SIMPLE_PROFILER_TIME_MS:
    printf("%lld %s\n", simple_profiler_get_milliseconds(state), "ms");
    break;
    case SIMPLE_PROFILER_TIME_S:
    printf("%lld %s\n", simple_profiler_get_seconds(state), "s");
    break;
    default:
    assert(0 && "Just a thing to delete compiler warning message, this code never ever ll be executed!");
    break;
    }
}

void
simple_profiler_print_as_float(SimpleProfiler* state)
{
    ProfilerTimeType timeType = simple_profiler_get_time_type(state);

    switch (timeType)
    {
    case SIMPLE_PROFILER_TIME_NS:
    printf("%lld %s\n", simple_profiler_get_nanoseconds(state), "ns");
    break;
    case SIMPLE_PROFILER_TIME_MCS:
    printf("%.4f %s\n", simple_profiler_get_microseconds_as_float(state), "mcs");
    break;
    case SIMPLE_PROFILER_TIME_MS:
    printf("%.4f %s\n", simple_profiler_get_milliseconds_as_float(state), "ms");
    break;
    case SIMPLE_PROFILER_TIME_S:
    printf("%.4f %s\n", simple_profiler_get_seconds_as_float(state), "s");
    break;
    default:
    assert(0 && "Just a thing to delete compiler warning message, this code never ever ll be executed!");
    break;
    }
}

static char g_TimeString[512];

char*
simple_profiler_get_string(SimpleProfiler* state)
{
    ProfilerTimeType timeType = simple_profiler_get_time_type(state);

    switch (timeType)
    {
    case SIMPLE_PROFILER_TIME_NS:
    string_format(g_TimeString, "%lld %s", simple_profiler_get_nanoseconds(state), "ns");
    break;
    case SIMPLE_PROFILER_TIME_MCS:
    string_format(g_TimeString, "%lld %s", simple_profiler_get_microseconds(state), "mcs");
    break;
    case SIMPLE_PROFILER_TIME_MS:
    string_format(g_TimeString, "%lld %s", simple_profiler_get_milliseconds(state), "ms");
    break;
    case SIMPLE_PROFILER_TIME_S:
    string_format(g_TimeString, "%lld %s", simple_profiler_get_seconds(state), "s");
    break;
    default:
    assert(0 && "Just a thing to delete compiler warning message, this code never ever ll be executed!");
    break;
    }

    return (char*)g_TimeString;
}

char*
simple_profiler_get_string_as_float(SimpleProfiler* state)
{
    ProfilerTimeType timeType = simple_profiler_get_time_type(state);

    switch (timeType)
    {
    case SIMPLE_PROFILER_TIME_NS:
    {
    f64 temp = simple_profiler_get_nanoseconds(state);
    string_format(g_TimeString, "%f %s\n", temp, "ns");
    break;
    }
    case SIMPLE_PROFILER_TIME_MCS:
    {
    f64 temp = simple_profiler_get_microseconds_as_float(state);
    string_format(g_TimeString, "%f %s\n", temp, "mcs");
    break;
    }
    case SIMPLE_PROFILER_TIME_MS:
    {
    f64 temp = simple_profiler_get_milliseconds_as_float(state);
    string_format(g_TimeString, "%f %s\n", temp, "ms");
    break;
    }
    case SIMPLE_PROFILER_TIME_S:
    {
    f64 temp = simple_profiler_get_seconds_as_float(state);
    string_format(g_TimeString, "%f %s\n", temp, "s");
    break;
    }
    default:
    {
    assert(0 && "Just a thing to delete compiler warning message, this code never ever ll be executed!");
    break;
    }
    }

    return (char*)g_TimeString;
}

/*

  #####################################
  #####################################
  SimpleSocket.c
  #####################################
  #####################################

*/

static i32 gSocketIsInDebugMode = 0;

u32
ip_as_integer(u8 ip3, u8 ip2, u8 ip1, u8 ip0)
{
    return ip3 << 24 | ip2 << 16 | ip1 << 8 | ip0;
}

void
ip_as_string(char str[], u32 ip)
{
    u8* ptr = (u8*)&ip;
    u8 byte3 = *(ptr+3);
    u8 byte2 = *(ptr+2);
    u8 byte1 = *(ptr+1);
    u8 byte0 = *(ptr);
    sprintf(str, "%d.%d.%d.%d", byte3, byte2, byte1, byte0);
}

void
ip_print(u32 address)
{
    char ips[32] = {};
    ip_as_string(ips, address);
    printf("ip=%s\n", ips);
}

char*
ip_to_string(u32 address)
{
    static char ips[32] = {};
    ip_as_string(ips, address);
    return (char*) ips;
}

char*
ip_to_user_string(u32 address)
{
    return ip_to_string(htonl(address));
}

#if defined(PLATFORM_LINUX)
u32
htonf(f32 f)
{
    u32 p;
    u32 sign;

    if (f < 0)
    {
    sign = 1;
    f = -f;
    }
    else
    {
    sign = 0;
    }

    p  = ((((u32)f) & 0x7fff) <<16) | (sign << 31); // whole part and sign
    p |= (u32) (((f - (i32)f) * 65536.0f)) & 0xffff; // fraction

    return p;
}

f32
ntohf(u32 p)
{
    f32 f = ((p >> 16) & 0x7fff); // whole part
    f += (p & 0xffff) / 65536.0f; // fraction

    if (((p >> 31) & 0x1) == 0x1)
    {
    f = -f; // sign bit set
    }

    return f;
}
#endif

void
socket_init()
{
#if defined(PLATFORM_LINUX)
    // do nothing, cause we dont have to
#elif defined(PLATFORM_WINDOWS)
    WSADATA wsaData;
    i32 initializedResult = WSAStartup(MAKEWORD(2,2), &wsaData);
    if (initializedResult != 0)
    {
	GERROR("WSAStartup failed with error: %d\n", initializedResult);
	vassert_break();
    }
#endif

}

Socket
socket_new(SocketSettings settings)
{
    if (settings.IpType != IpType_V4)
    {
    GERROR("IpV6 isnt supported!\n");
    vassert(0 && "Wrong ip type!");
    }

#if defined(PLATFORM_LINUX)
    i32 socketDs;
#elif defined(PLATFORM_WINDOWS)
    SOCKET socketDs;
#endif

    if (settings.Type == SocketType_UDP)
    {
    socketDs = socket(AF_INET, SOCK_DGRAM, settings.Protocol);
    }
    else if (settings.Type == SocketType_TCP)
    {
    socketDs = socket(AF_INET, SOCK_STREAM, settings.Protocol);
    }
    else
    {
    GERROR("Socket type raw isnt supported!\n");
    vassert(0 && "Wrong socket type!");
    }

    if (gSocketIsInDebugMode /* && settings.Type == SocketType_TCP */)
    {
#if defined(PLATFORM_LINUX)
    i32 setOptionResult = setsockopt(socketDs, SOL_SOCKET, SO_REUSEADDR, &(i32){1}, sizeof(i32));
    setOptionResult = setsockopt(socketDs, SOL_SOCKET, SO_REUSEPORT, &(i32){1}, sizeof(i32));
#elif defined(WINDOW_PLATFORM)
    i32 setOptionResult = setsockopt(socketDs, SOL_SOCKET, SO_REUSEADDR, (void*) (&((i32) { 1 })), sizeof(i32));
    setOptionResult = setsockopt(socketDs, SOL_SOCKET, SO_REUSEPORT, (void*) (&(i32) { 1 }), sizeof(i32));
#endif
    }

    Socket socket = {
    .Descriptor = socketDs,
    };

    socket.Address = (struct sockaddr_in) {
    .sin_family = AF_INET,
    .sin_addr.s_addr = htonl(settings.Address.Ip),
    .sin_port = htons(settings.Address.Port),
    };

    socket.ServerAddress = (struct sockaddr_in) {
    .sin_family = AF_INET,
    .sin_addr.s_addr = htonl(settings.ServerAddress.Ip),
    .sin_port = htons(settings.ServerAddress.Port),
    };

    return socket;
}

i32
socket_is_invalid(Socket* pSocket)
{
    Socket invalidOne = (Socket) {0};
    if (memcmp(pSocket, &invalidOne, sizeof(Socket)) == 0)
    {
    return 1;
    }

    return 0;
}

i32
socket_is_valid(Socket* pSocket)
{
    return !socket_is_invalid(pSocket);
}


void
socket_make_async(Socket* pSocket)
{
    pSocket->IsAsync = 1;
#if defined(PLATFORM_LINUX)
    fcntl(pSocket->Descriptor, F_SETFL, O_NONBLOCK);
#elif defined(PLATFORM_WINDOWS)
    u_long flag = 1;
    ioctlsocket(pSocket->Descriptor, FIONBIO, &flag);
#endif
}

i32
socket_connect(Socket* pSocket)
{
    i32 result = connect(pSocket->Descriptor, (struct sockaddr*) &pSocket->ServerAddress, sizeof(struct sockaddr_in));
    if (result < 0)
    {
    return 0;
    }

    return 1;
}

i32
socket_connect_to(Socket* pSocket, u32 ip, u16 port)
{
    struct sockaddr_in connectTo = (struct sockaddr_in) {
    .sin_family = AF_INET,
    .sin_port = htons(port),
    .sin_addr.s_addr = htonl(ip)
    };

    i32 result = connect(pSocket->Descriptor, (struct sockaddr*) &connectTo, sizeof(struct sockaddr_in));
    if (result < 0)
    return 0;

    return 1;
}

i32
socket_bind(Socket* pSocket)
{
    i32 result = bind(pSocket->Descriptor, (struct sockaddr*) &pSocket->Address, sizeof(struct sockaddr_in));
    if (result < 0)
    return 0;
    return 1;
}

Socket
socket_accept(Socket* pSocket)
{
    struct sockaddr_in addr = {};
    i32 sockAddrLen = sizeof(addr);
#if defined(PLATFORM_LINUX)
    i32 sock = accept(pSocket->Descriptor, (struct sockaddr*) &addr, (socklen_t*) &sockAddrLen);
#elif defined(PLATFORM_WINDOWS)
    i32 sock = accept(pSocket->Descriptor, (struct sockaddr*)&addr, &sockAddrLen);
#endif

    Socket socket = {
    .Descriptor = sock,
    .ServerAddress = addr
    };

    return socket;
}

i32
socket_listen(Socket* pSocket, i32 clientsCount)
{
    i32 result = listen(pSocket->Descriptor, clientsCount);
    if (result < 0)
    return 0;
    return 1;
}

void
socket_close(Socket* pSocket)
{
#if defined(PLATFORM_LINUX)
    close(pSocket->Descriptor);
#elif defined(PLATFORM_WINDOWS)
    closesocket(pSocket->Descriptor);
#endif
}

void
socket_deinit()
{
#if defined(PLATFORM_WINDOWS)
    WSACleanup();
#endif
}

i32
socket_send_ext(i32 descriptor, void* data, u64 size, i32 flags)
{
    i32 sendedBytesCount, total = 0;

    while (total < size)
    {
    i32 byteToSend = size - total;
    sendedBytesCount = send(descriptor, data + total, byteToSend, flags);

    if (sendedBytesCount == -1)
    {
	return 0;
    }

    total += sendedBytesCount;
    }

    return 1;
}

i32
socket_send(Socket* pSocket, void* data, u64 size)
{
    return socket_send_ext(pSocket->Descriptor, data, size, 0);
}

i32
socket_recv_ext(i32 descriptor, void* data, u64 size, i32 flags)
{
    i32 recvBytesCount, total = 0;
    while (total < size)
    {
    i32 byteToRecv = size - total;
    recvBytesCount = recv(descriptor, data + total, byteToRecv, flags);

    if (recvBytesCount == 0)
	return 0;
    else if (recvBytesCount == -1)
	break; // NOTE: Obviously we read to the end

    total += recvBytesCount;
    }

    return total;
}

i32
socket_recv(Socket* pSocket, void* data, u64 size)
{
    return socket_recv_ext(pSocket->Descriptor, data, size, 0);
}

i32
socket_peek(Socket* pSocket, void* data, u64 size)
{
    return socket_recv_ext(pSocket->Descriptor, data, size, MSG_PEEK);
}

i32
socket_send_to_ext(i32 descriptor, void* data, u64 size, i32 flags, struct sockaddr_in* pAddress)
{
    i32 sendedBytesCount, total = 0;
    while (total < size)
    {
    i32 byteToSend = size - total;
    sendedBytesCount = sendto(descriptor, data + total, byteToSend, flags, (struct sockaddr*) pAddress, sizeof(struct sockaddr_in));

    if (sendedBytesCount == -1)
    {
	return 0;
    }

    total += sendedBytesCount;
    }

    return 1;
}

i32
socket_recv_from_ext(i32 descriptor, void* data, u64 size, i32 flags, struct sockaddr_in* pAddress)
{
    i32 recvBytesCount, total = 0;
    i32 addrSize = sizeof(struct sockaddr_in);
    while (total < size)
    {
	i32 byteToRecv = size - total;
	recvBytesCount = recvfrom(descriptor, data + total, byteToRecv, flags, (struct sockaddr*) pAddress, &addrSize);

	if (recvBytesCount == 0)
	    return 0;
	else if (recvBytesCount == -1)
	    return -1;

	total += recvBytesCount;
    }

    return total;
}

i32
socket_send_to(Socket* pSocket, void* data, u64 size)
{
    return socket_send_to_ext(pSocket->Descriptor, data, size, 0, &pSocket->ServerAddress);
}

i32
socket_recv_from(Socket* pSocket, void* data, u64 size)
{
    return socket_recv_from_ext(pSocket->Descriptor, data, size, 0, &pSocket->ServerAddress);
}

i32
socket_peek_from(Socket* pSocket, void* data, u64 size)
{
    return socket_recv_from_ext(pSocket->Descriptor, data, size, MSG_PEEK, &pSocket->ServerAddress);
}

u32
socket_parse_ipv4(char* str, u64 length)
{
    i32 ip0, ip1, ip2, ip3;
    i32 ind = 0;
    char* sb = NULL;

    if (string_compare(str, "l") || string_compare(str, "localhost"))
    {
    return ip_as_integer(127, 0, 0, 1);
    }

    SimpleArena* pArena = simple_arena_create(KB(1));
    memory_set_arena(pArena);

#define ParseIpFor(ipi)                                                 \
    if (string_is_integer(sb, string_builder_count(sb)))        \
    {                                                           \
    ipi = string_to_i32(sb);                                \
    ++ind;                                                  \
    }                                                           \
				\
    string_builder_clear(sb);

    for (i32 i = 0; i < length; ++i)
    {
    char c = str[i];

    switch (c)
    {

    case '0': case '1': case '2': case '3': case '4':
    case '5': case '6': case '7': case '8': case '9':
    {
	string_builder_appendc(sb, c);
	break;
    }

    case '.':
    {
	switch (ind)
	{
	case 0:
	{
	ParseIpFor(ip0);
	break;
	}

	case 1:
	{
	ParseIpFor(ip1);
	break;
	}

	case 2:
	{
	ParseIpFor(ip2);
	break;
	}

	}

	break;
    }

    }
    }

    if (ind == 3)
    {
    ParseIpFor(ip3);
    }

    memory_set_arena(NULL);
    simple_arena_destroy(pArena);

#define IpNotValid(ip) ({(ip > 255 || ip < 0);})

    u32 ipAddr;
    if (IpNotValid(ip0) || IpNotValid(ip1) || IpNotValid(ip2) || IpNotValid(ip3))
    {
    ipAddr = 0;
    }
    else
    {
    ipAddr = ip_as_integer(ip0, ip1, ip2, ip3);
    }

#undef ParseIpFor
#undef IpNotValid

    return ipAddr;
}

u16
socket_parse_port(char* str, u64 length)
{
    i32 isInt = string_is_integer(str, length);
    if (!isInt)
    return 0;

#define PortNotValid(port) ({(port < 0 || port > 65535);})

    i32 port = string_to_i32(str);
    if (PortNotValid(port))
    return 0;

    return (u16) port;
}

i32
socket_parse_ip_port(char* address, u32* pIp, u16* pPort)
{
    i32 result = 0;

    char** splited = string_split(address, ':');
    if (array_count(splited) != 2)
    {
    GERROR("Socket address (%s) is not correct!\n", address);
    goto SocketParseIpPortEnd;
    }

    char* ipStr   = splited[0];
    char* portStr = splited[1];
    u32 addr = socket_parse_ipv4((char*)ipStr, string_length(ipStr));
    u16 port = socket_parse_port((char*)portStr, string_length(portStr));

    if (addr == 0 || port == 0)
    {
    GERROR("Address (%s) or port (%d) isnt correct!\n", ip_to_string(addr), port);
    goto SocketParseIpPortEnd;
    }

    *pIp = addr;
    *pPort = port;
    result = 1;

SocketParseIpPortEnd:
    array_foreach(splited, memory_free(item));
    array_free(splited);

    return result;
}

struct sockaddr_in
socket_address_v4(u32 ip, u16 port)
{
    struct sockaddr_in address = {
    .sin_family = AF_INET,
    .sin_port = htons(port),
    .sin_addr.s_addr = htonl(ip)
    };
    return address;
}

u32
socket_get_ip()
{
#if defined(PLATFORM_LINUX)
    u32 addr = 0;

    i32 sd = socket(PF_INET, SOCK_DGRAM, 0);
    if (sd > 0)
    {
	struct ifreq ifr[10] = {};
	struct ifconf ifc = {};
	ifc.ifc_len = sizeof(ifr);
	ifc.ifc_ifcu.ifcu_buf = (caddr_t)ifr;

	if (ioctl(sd, SIOCGIFCONF, &ifc) != 0)
	    goto GetIpEndLabel;

	i32 ifc_num = ifc.ifc_len / sizeof(struct ifreq);

	for (i32 i = 0; i < ifc_num; ++i)
	{
	    if (ifr[i].ifr_addr.sa_family != AF_INET)
		continue;

	    if (ioctl(sd, SIOCGIFADDR, &ifr[i]) == 0)
	    {
		u32 taddr = ((struct sockaddr_in *)(&ifr[i].ifr_addr))->sin_addr.s_addr;
		if ((taddr & 0xFF) == 127)
		    continue;

		addr = ntohl(taddr);

		goto GetIpEndLabel;
	    }
	}
    }

GetIpEndLabel:
    close(sd);

    return addr;

#elif defined(PLATFORM_WINDOWS)

    char buf[128] = {};
    i32 result = gethostname(buf, 128);
    struct hostent* pHost = gethostbyname(buf);
    struct in_addr* pIpAddr = (struct in_addr*) (pHost->h_addr);
    u32 addr = ntohl(pIpAddr->S_un.S_addr);
    printf("Hostname: %s resolved to %s\n", buf, ip_to_string(addr));

    return addr;
#endif // PLATFORM_WINDOWS

}

i32
socket_compare_address(Socket* a, Socket* b)
{
    i32 isEqual = (memcmp(&a->Address, &b->Address, sizeof(struct sockaddr_in)) == 0);
    return isEqual;
}

i32
socket_compare_raw_address(struct sockaddr_in* a, struct sockaddr_in* b)
{
    if (a->sin_port == b->sin_port &&
    a->sin_addr.s_addr == b->sin_addr.s_addr)
    {
    return 1;
    }

    return 0;
}

i32
socket_compare_server_address(Socket* a, Socket* b)
{
    i32 isEqual = (memcmp(&a->ServerAddress, &b->ServerAddress, sizeof(struct sockaddr_in)) == 0);
    return isEqual;
}

void
socket_set_debug_mode()
{
    gSocketIsInDebugMode = 1;
}

void
socket_set_release_mode()
{
    gSocketIsInDebugMode = 0;
}


// End of SimpleSocket.c


void
pack_example()
{
#pragma pack(push, 1)
    struct B
    {
    char c;
    i32 d;
    };
#pragma pack(pop)

    struct B a = {};
    GINFO("Size: %d\n", sizeof(a));
    vassert_break();
}

#endif //SSL_IMPLEMENTATION

#endif //SIMPLE_STANDARD_LIBRARY_H
