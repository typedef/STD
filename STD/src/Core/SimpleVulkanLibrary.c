#include "SimpleVulkanLibrary.h"
#include "vulkan_core.h"

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>


#pragma clang diagnostic ignored "-Wswitch"


#if !defined (SVL_X11)
 #define SVL_X11 0
#endif

#if !defined (SVL_XCB)
 #define SVL_XCB 0
#endif

#if !defined (SVL_WIN32)
 #define SVL_WIN32 0
#endif

#if SVL_X11 == 1
 #define VK_USE_PLATFORM_XLIB_KHR
 #define SVL_LINUX_PLATFORM
#elif SVL_XCB == 1
 #error "Not impl"
#elif SVL_WIN32 == 1
 #error "Not impl"
 #define SVL_WINDOWS_PLATFORM
#else
//#error "Not supported platform!!!"
#endif

#define SvlTerminalRed(x) "\x1B[31m"x"\033[0m"
#define SvlTerminalMagneta(x) "\x1B[35m"x"\033[0m"
#define SvlTerminalYellow(x) "\x1B[93m"x"\033[0m"

#define SvlMinMax(v, min, max)			\
    ({						\
	__typeof__(v) r = v;			\
	if (r < min) { r = min; }		\
	else if (r > max) {r = max; }		\
	r;					\
    })


#define svl_info(pMsg, ...)						\
    _svl_info(pMsg, __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define svl_warn(pMsg, ...)						\
    _svl_info(pMsg, __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define svl_error(pMsg, ...)						\
    _svl_info(pMsg, __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__)

static void
_svl_info(const char* pMsg, const char* pFile, const char* pFunction, svl_i32 line, ...)
{
    va_list args;
    va_start(args, line);

    printf(SvlTerminalMagneta("[SvlInfo]") " file:%s func:%s line:%d ", pFile, pFunction, line);
    vprintf(pMsg, args);

    va_end(args);
}

static void
_svl_warn(const char* pMsg, const char* pFile, const char* pFunction, svl_i32 line, ...)
{
    va_list args;
    va_start(args, line);

    printf(SvlTerminalYellow("[SvlWarn] ")" file:%s func:%s line:%d ", pFile, pFunction, line);
    vprintf(pMsg, args);

    va_end(args);
}

static void
_svl_error(const char* pMsg, const char* pFile, const char* pFunction, svl_i32 line, ...)
{
    va_list args;
    va_start(args, line);

    printf(SvlTerminalRed("[SvlError] ")" file:%s func:%s line:%d ", pFile, pFunction, line);
    vprintf(pMsg, args);

    va_end(args);
}


static void
_vbreak(const char* pFormat, const char* pFile, int line, const char* pCondition)
{
    printf("[ASSERT] %s:%d condition: %s is false!\n", pFile, line, pCondition);
    volatile int* cptr = NULL;
    *cptr = 0;
}

#ifndef svl_assert
#define svl_assert(a) ({						\
	    if (!(a))							\
		_vbreak("[ASSERT] %s:%d condition: %s is false!\n", __FILE__, __LINE__, #a); \
	})
#endif // svl_assert


#if defined(SVL_LINUX_PLATFORM)
#include <sys/stat.h>
#elif defined(SVL_WINDOWS_PLATFORM)
#endif

#if SVL_X11 == 1
#include <X11/Xlib.h>

typedef struct SvlX11Backend
{
    Display* pDisplay;
    Window Window;
} SvlX11Backend;

#elif SVL_XCB == 1
#error "Not impl"
#elif SVL_WIN32 == 1
#error "Not impl"
#else
#error "Platform not supported, or you fogot to declare platform flag, like #define SVL_X11 1!"
#endif


#if SVL_CRASH_ON_ERROR == 1
#define svl_guard(cond) _svl_guard(cond, #cond, __LINE__, __FILE__)
#else
#define svl_guard(cond) ((void)(cond))
#endif
void _svl_guard(svl_i32 condition, const char* pCond, svl_i32 line, const char* pFile);
#define SvlArrayCount(arr) (sizeof(arr) / sizeof(*arr))

svl_i32 _svl_string_compare(const char* s0, const char* s1);
svl_i64 _svl_string_length(const char* p);
char* _svl_path_combine(const char* left, const char* right);
const char* _svl_path_get_name(const char* p);
char* _svl_path_get_name_wo_ext(const char* p);
const char* _svl_path_get_ext(const char* p);
char _svl_char_to_upper(char c);
char* _svl_string(const char* s);
char* _svl_string_concat(const char* p0, const char* p1);
void* _svl_file_read_bytes(const char* pPath, svl_i64* pLength);
svl_i32 _svl_path_file_exist(const char* path);
svl_i32 _svl_path_directory_exist(const char* pPath);
svl_i32 _svl_directory_create(const char* pPath);
svl_size _svl_path_get_last_mod_time(const char* pPath);
char svl_char_to_upper(char c);


/*#######################
  DOCS(typedef): Some utils/structures like svl_array_push
  #######################*/
typedef struct SvlArrayHeader
{
    svl_size ItemSize;
    svl_i64 Count;
    svl_i64 Capacity;
    void* pBuffer;
} SvlArrayHeader;

#define svl_array_header(b) ((SvlArrayHeader*) (((char*)b) - sizeof(SvlArrayHeader)))
#define svl_array_count(b) ((b != NULL) ? svl_array_header(b)->Count : 0)
#define svl_array_capacity(b) ((b != NULL) ? svl_array_header(b)->Capacity : 0)

static void*
svl_array_grow(const void* pBuffer, svl_size itemSize, svl_size cnt)
{
    if (pBuffer != NULL)
    {
	svl_size newCapacity = 2 * svl_array_capacity(pBuffer) + 1;
	svl_size newSize = newCapacity * itemSize + sizeof(SvlArrayHeader);
	SvlArrayHeader* pOld = svl_array_header(pBuffer);

	SvlArrayHeader* pHdr = NULL;
	pHdr = (SvlArrayHeader*) malloc(newSize);
	pHdr->pBuffer = ((char*)pHdr) + sizeof(SvlArrayHeader);
	pHdr->ItemSize = itemSize;
	pHdr->Count = pOld->Count;
	pHdr->Capacity = newCapacity;

	size_t copySize = pOld->Count * itemSize;
	memcpy(pHdr->pBuffer, pBuffer, copySize);

	free(pOld);

	return pHdr->pBuffer;
    }

    SvlArrayHeader* pHdr = (SvlArrayHeader*) malloc(cnt * itemSize + sizeof(SvlArrayHeader));
    pHdr->pBuffer = (((void*)pHdr) + sizeof(SvlArrayHeader));
    pHdr->Count = 0;
    pHdr->Capacity = cnt;
    pHdr->ItemSize = itemSize;

    return pHdr->pBuffer;
}

#define svl_array_free(b) if (b) free(svl_array_header(b));

#define svl_array_push(b, ...)						\
    ({									\
	if ((b) == NULL || svl_array_count(b) >= svl_array_capacity(b))	\
	{								\
	    (b) = svl_array_grow((const void*)b, sizeof(__typeof__(b[0])), 1); \
	}								\
									\
	b[svl_array_count(b)] = (__VA_ARGS__);				\
	++svl_array_header(b)->Count;					\
    })
#define svl_array_reserve(b, cnt) ({b = svl_array_grow((const void*)b, sizeof(__typeof__(b[0])), cnt);})

/*#######################
  DOCS(typedef): Internal Structures
  #######################*/

typedef struct _VkInstanceCreateInfo
{
    const char* pName;
    svl_u32 VulkanVersion;
    svl_i32 IsDebug;
    void* pWindowBackend;
} _VkInstanceCreateInfo;


/*#######################
  DOCS(typedef): Forward Declaration's
  #######################*/

void _svl_image_view_create(SvlInstance* pInstance, VkImage vkImage, VkFormat vkFormat, VkImageAspectFlags vkImageAspectFlags, svl_i32 mipLevels, VkImageView* pImageView);
svl_i32 svl_find_memory_type_index(SvlInstance* pInstance, svl_u32 typeBit, VkMemoryPropertyFlags vkMemoryPropertyFlags);
VkBuffer _svl_buffer_create(SvlInstance* pInstance, VkBufferUsageFlags usage, VkDeviceSize size, VkMemoryPropertyFlags memoryPropertyFlags, VkDeviceMemory* deviceMemory);
SvlSwapImageView* _svl_swapchain_image_views_create(SvlInstance* pInstance, VkFormat vkFormat);
SvlImageView svl_image_view_create(SvlInstance* pInstance, SvlImageViewSettings settings);


/*#######################
  DOCS(typedef): Platform dependent stuff
  #######################*/

static char**
_svl_get_required_extensions(svl_i32 isDebug)
{
#if 0
    u32 extCount = 5;
    char** result = NULL;
    char** aExts = window_get_required_exts(&extCount);

    svl_array_push(result, VK_EXT_DEBUG_UTILS_EXTENSION_NAME);

    for (svl_i32 i = 0; i < extCount; ++i)
    {
	char*item=aExts[i];

	printf("Ext: %s\n", item);
	svl_array_push(result, item);
    }

    return result;
#else

    char** aExts = NULL;

    const char* pSurfaceExtName = NULL;

#if SVL_X11 == 1
    //pSurfaceExtName = "VK_KHR_xcb_surface";
    pSurfaceExtName = "VK_KHR_xlib_surface";
#elif SVL_XCB == 1
    pSurfaceExtName = "VK_KHR_xcb_surface";
#elif SVL_WIN32 == 1
    pSurfaceExtName = "VK_KHR_win32_surface";
#else
#error "Platform not supported, or you fogot to declare platform flag, like #define SVL_X11 1!"
#endif

    svl_u32 count;
    vkEnumerateInstanceExtensionProperties(NULL, &count, NULL);
    VkExtensionProperties* aExtProperties = malloc(sizeof(VkExtensionProperties)*count);

    VkResult enumResult = vkEnumerateInstanceExtensionProperties(NULL, &count, aExtProperties);
    if (enumResult != VK_SUCCESS)
    {
	free(aExtProperties);
	return NULL;
    }

    for (svl_i32 i = 0; i < count; ++i)
    {
	VkExtensionProperties prop = aExtProperties[i];
	if (strcmp(prop.extensionName, "VK_KHR_surface") == 0)
	{
	    svl_array_push(aExts, (char*)"VK_KHR_surface");
	}
	// DOCS: include only if exist in extProps
	else if (strcmp(prop.extensionName, pSurfaceExtName) == 0)
	{
	    svl_array_push(aExts, (char*)pSurfaceExtName);
	}
    }

    if (isDebug)
    {
	svl_array_push(aExts, VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
    }

    free(aExtProperties);

    return aExts;
#endif
}

/*
  DOCS: Internal functions, client are should *NOT* be interersted in this
*/

static VkBool32
vk_validation_layers_error_callback(
    VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
    VkDebugUtilsMessageTypeFlagsEXT messageTypes,
    const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
    void* pUserData)
{
    if (messageSeverity == VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT)
    {
	printf(SvlTerminalMagneta("[VULKAN INFO]"));
    }
    else if (messageSeverity == VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT)
    {
	printf(SvlTerminalYellow("[VULKAN WARNING]"));
    }
    else if (messageSeverity == VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT)
    {
	printf(SvlTerminalRed("[VULKAN ERROR] "));
    }

    printf("%s \n", pCallbackData->pMessage);

    return VK_TRUE;
}

void
_svl_instance_create(SvlInstance* pInstance, _VkInstanceCreateInfo createInfo)
{
    if (createInfo.pName == NULL || createInfo.pWindowBackend == NULL)
    {
	svl_guard(createInfo.pName != NULL);
	svl_guard(createInfo.pWindowBackend != NULL);
	SvlReturnError(SvlErrorType_Instance_CantCreate);
    }

    svl_u32 version;
    VkResult getVersionResult = vkEnumerateInstanceVersion(&version);

    if (svl_result_check(getVersionResult, "Failed vkEnumerateInstanceVersion"))
    {
	SvlReturnError(SvlErrorType_Instance_CantCreate);
    }

    svl_u32 vulkanVersion = createInfo.VulkanVersion;
    if (vulkanVersion == 0)
	vulkanVersion = VK_MAKE_API_VERSION(0, 1, 3, 0);

    char** aRequiredExts = _svl_get_required_extensions(createInfo.IsDebug);
    if (aRequiredExts == NULL)
    {
	SvlReturnError(SvlErrorType_Instance_CantGetExts);
    }

    // DOCS: Allocators callbacks
    // TODO: Provide interface for setting callback from user space
    VkAllocationCallbacks* pAllocatorCallbacks = NULL;

    /* // NOTE(typedef): create validation layers */
    char** validationLayerNames = NULL;
    VkDebugUtilsMessengerCreateInfoEXT debug = {};
    if (createInfo.IsDebug)
    {
	svl_array_push(validationLayerNames, "VK_LAYER_KHRONOS_validation");

	VkDebugUtilsMessageSeverityFlagsEXT severity = /* VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT | */
	    VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
	    VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
	VkDebugUtilsMessageTypeFlagsEXT messageType =
	    VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
	    VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
	    VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;

	debug = (VkDebugUtilsMessengerCreateInfoEXT) {
	    .sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT,
	    .flags = 0,
	    .messageSeverity = severity,
	    .messageType = messageType,
	    .pfnUserCallback = vk_validation_layers_error_callback,
	    .pUserData = NULL
	};
    }

    VkDebugUtilsMessengerCreateInfoEXT* pDebug = NULL;
    if (createInfo.IsDebug)
	pDebug = &debug;

    VkApplicationInfo applicationInfo = {
	.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
	.pNext = NULL,
	.pApplicationName = createInfo.pName,
	.applicationVersion = VK_MAKE_API_VERSION(0, 1, 0, 0),
	.pEngineName = createInfo.pName,
	.engineVersion = VK_MAKE_API_VERSION(0, 1, 0, 0),
	.apiVersion = vulkanVersion
    };

    VkInstanceCreateInfo instanceCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
	.pNext = pDebug,
	.flags = 0,
	.pApplicationInfo = &applicationInfo,
	.enabledLayerCount = svl_array_count(validationLayerNames),
	.ppEnabledLayerNames = (const char* const*)validationLayerNames,
	.enabledExtensionCount = svl_array_count(aRequiredExts),
	.ppEnabledExtensionNames = (const char* const*)aRequiredExts
    };

    VkInstance vkInstance;
    VkResult createInstanceResult = vkCreateInstance(&instanceCreateInfo, pAllocatorCallbacks, &vkInstance);
    if (svl_result_check(createInstanceResult, "Can't create instance"))
    {
	SvlReturnError(SvlErrorType_Instance_CantCreateInstance);
    }

    VkDebugUtilsMessengerEXT messenger;

    if (createInfo.IsDebug)
    {
	PFN_vkCreateDebugUtilsMessengerEXT vkCreateDebugUtilsMessenger =
	    (PFN_vkCreateDebugUtilsMessengerEXT)
	    vkGetInstanceProcAddr(vkInstance,
				  "vkCreateDebugUtilsMessengerEXT");

	VkResult createResult = vkCreateDebugUtilsMessenger(vkInstance, pDebug, pAllocatorCallbacks, &messenger);

	if (svl_result_check(createResult, "Can't create debug utils"))
	{
	    SvlReturnError(SvlErrorType_Instance_CantCreateDebugUtils);
	}
    }

    pInstance->IsDebugMode = createInfo.IsDebug;
    pInstance->Error = SvlErrorType_None;
    pInstance->VersionMajor = VK_API_VERSION_MAJOR(version);
    pInstance->VersionMinor = VK_API_VERSION_MINOR(version);
    pInstance->VersionPatch = VK_API_VERSION_PATCH(version);
    pInstance->Handle = vkInstance;
    pInstance->pAllocator = pAllocatorCallbacks;
    pInstance->DebugMessenger = messenger;
    pInstance->pWindowBackend = createInfo.pWindowBackend;
}

#if SVL_X11 == 1
typedef VkFlags VkXlibSurfaceCreateFlagsKHR;

typedef struct VkXlibSurfaceCreateInfoKHR
{
    VkStructureType             sType;
    const void*                 pNext;
    VkXlibSurfaceCreateFlagsKHR flags;
    Display*                    dpy;
    Window                      window;
} VkXlibSurfaceCreateInfoKHR;

typedef VkResult (*PFN_vkCreateXlibSurfaceKHR)(VkInstance, const VkXlibSurfaceCreateInfoKHR*, const VkAllocationCallbacks*, VkSurfaceKHR*);

#elif SVL_WIN32 == 1

#endif // SVL_X11; SVL_WIN32

void
_svl_surface_create(SvlInstance* pInstance)
{
    VkSurfaceKHR vkSurface;

#if SVL_X11 == 1

    SvlX11Backend* pWindowBackend = (SvlX11Backend*) pInstance->pWindowBackend;

    VkXlibSurfaceCreateInfoKHR surfaceCI = {
	.sType = VK_STRUCTURE_TYPE_XLIB_SURFACE_CREATE_INFO_KHR,
	.dpy = pWindowBackend->pDisplay,
	.window = pWindowBackend->Window,
    };

    PFN_vkCreateXlibSurfaceKHR vkCreateXlibSurfaceKHR =
	(PFN_vkCreateXlibSurfaceKHR)
	vkGetInstanceProcAddr(pInstance->Handle, "vkCreateXlibSurfaceKHR");
    if (!vkCreateXlibSurfaceKHR)
    {
	pInstance->Error = SvlErrorType_Surface_CantLoadCreate;
	return;
    }

    VkResult createSurfaceResult = vkCreateXlibSurfaceKHR(pInstance->Handle, &surfaceCI, pInstance->pAllocator, &vkSurface);
    if (svl_result_check(createSurfaceResult, "Can't create surface!"))
    {
	pInstance->Error = SvlErrorType_Surface_CantCreate;
	return;
    }

    pInstance->Surface = vkSurface;

#elif SVL_XCB == 1

#elif SVL_WIN32 == 1

#else
#error "Platform not supported, or you fogot to declare platform flag, like #define SVL_X11 1!"
#endif

}

void
_svl_physical_device_create(SvlInstance* pInstance)
{
    /*
      DOCS(typedef):Pick physical device
    */
    svl_u32 physicalDevicesCount;
    VkResult enumeratePhysicalDeviceResult =
	vkEnumeratePhysicalDevices(pInstance->Handle, &physicalDevicesCount, NULL);

    if (svl_result_check(enumeratePhysicalDeviceResult, "Physical device enumeration error") || physicalDevicesCount <= 0)
    {
	SvlReturnError(SvlErrorType_PhysicalDevice_CantFind);
    }

    if (enumeratePhysicalDeviceResult == VK_ERROR_INITIALIZATION_FAILED)
    {
	printf("Can't enemerate physical devices, maybe installable client driver (ICD) is not installed on your machine!\n");
	SvlReturnError(SvlErrorType_PhysicalDevice_CantFind);
    }

    VkPhysicalDevice* aPhysicalDevices = malloc(physicalDevicesCount * sizeof(VkPhysicalDevice));
    enumeratePhysicalDeviceResult =
	vkEnumeratePhysicalDevices(pInstance->Handle, &physicalDevicesCount, aPhysicalDevices);
    if (svl_result_check(enumeratePhysicalDeviceResult, "Problem with getting physical device throught vkEnumeratePhysicalDevices"))
    {
	SvlReturnError(SvlErrorType_PhysicalDevice_CantFind);
    }

    VkPhysicalDevice vkPhysicalDevice = VK_NULL_HANDLE;
    VkPhysicalDeviceProperties vkProperties;

    svl_i32 isPicked = 0;
    for (svl_i32 i = 0; i < physicalDevicesCount; ++i)
    {
	VkPhysicalDeviceProperties physicalDeviceProperties;
	VkPhysicalDevice device = aPhysicalDevices[i];
	vkGetPhysicalDeviceProperties(device, &physicalDeviceProperties);

	VkPhysicalDeviceFeatures deviceFeatures;
	vkGetPhysicalDeviceFeatures(device, &deviceFeatures);

	if ((physicalDeviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
	    && deviceFeatures.samplerAnisotropy
	    && deviceFeatures.multiDrawIndirect)
	{
	    vkPhysicalDevice = device;
	    vkProperties = physicalDeviceProperties;
	    isPicked = 1;
	    break;
	}
    }

    if (!isPicked || vkPhysicalDevice == VK_NULL_HANDLE)
    {
	SvlReturnError(SvlErrorType_PhysicalDevice_CantFind);
    }

    svl_u32 physicalDeviceExtCount;
    VkResult getPhysicalDeviceExtResult = vkEnumerateDeviceExtensionProperties(vkPhysicalDevice, NULL, &physicalDeviceExtCount, NULL);
    if (svl_result_check(getPhysicalDeviceExtResult, "Can't enumerate vkEnumerateDeviceExtensionProperties"))
    {
	SvlReturnError(SvlErrorType_PhysicalDevice_CantFind);
    }

    VkExtensionProperties* aExts = NULL;
    svl_array_reserve(aExts, physicalDeviceExtCount);

    getPhysicalDeviceExtResult = vkEnumerateDeviceExtensionProperties(vkPhysicalDevice, NULL, &physicalDeviceExtCount, aExts);
    if (svl_result_check(getPhysicalDeviceExtResult, "Can't enumerate vkEnumerateDeviceExtensionProperties"))
    {
	SvlReturnError(SvlErrorType_PhysicalDevice_CantFind);
    }

    svl_array_header(aExts)->Count = physicalDeviceExtCount;

    free(aPhysicalDevices);

    pInstance->PhysicalDevice = vkPhysicalDevice;
    pInstance->Properties = vkProperties;
    pInstance->aExts = aExts;
}

static const char*
_svl_physical_device_to_string(VkPhysicalDeviceType type)
{
    switch (type)
    {
    case VK_PHYSICAL_DEVICE_TYPE_OTHER: return "Other";
    case VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU: return "Integrated GPU";
    case VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU: return "GPU";
    case VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU: return "Virtual GPU";
    case VK_PHYSICAL_DEVICE_TYPE_CPU: return "CPU";
    default: return "";
    }
}

void
_svl_physical_device_print(VkPhysicalDeviceProperties props)
{
    printf("[Physical Device] { \n\t"
	   ".apiVersion = %d.%d.%d ,\n\t"
	   ".driverVersion = %d.%d.%d,\n\t"
	   ".vendorID = %d,\n\t"
	   ".deviceID = %d,\n\t"
	   ".deviceType = %s,\n\t"
	   ".deviceName = %s\n}\n",

	   // api version
	   VK_API_VERSION_MAJOR(props.apiVersion),
	   VK_API_VERSION_MINOR(props.apiVersion),
	   VK_API_VERSION_PATCH(props.apiVersion),

	   // driver version
	   VK_API_VERSION_MAJOR(props.driverVersion),
	   VK_API_VERSION_MINOR(props.driverVersion),
	   VK_API_VERSION_PATCH(props.driverVersion),

	   props.vendorID,
	   props.deviceID,
	   _svl_physical_device_to_string(props.deviceType),
	   props.deviceName
	);

}

static SvlGraphSupport
_svl_swap_chain_support_details(VkPhysicalDevice pDevice, VkSurfaceKHR surface)
{
    SvlGraphSupport svlSupport = { };

    VkResult getSurfaceCapabilitiesResult =
	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(
	    pDevice,
	    surface,
	    &svlSupport.SurfaceCapabilities);
    VkSurfaceCapabilitiesKHR caps = svlSupport.SurfaceCapabilities;

//vkPrintSurfaceCapabilitiesLine(supportDetails.SurfaceCapabilities);

    // NOTE(typdef): get surface formats
    svl_u32 formatsCount;
    VkResult getSurfaceFormatResult =
	vkGetPhysicalDeviceSurfaceFormatsKHR(pDevice, surface, &formatsCount, NULL);
    svlSupport.SurfaceFormats = malloc(formatsCount * sizeof(VkSurfaceFormatKHR));

    getSurfaceFormatResult =
	vkGetPhysicalDeviceSurfaceFormatsKHR(pDevice, surface, &formatsCount, svlSupport.SurfaceFormats);

    if (svl_result_check(getSurfaceFormatResult, "Failed to get Surface Format Result"))
	return svlSupport;

    // NOTE(typedef): get surface modes
    svl_u32 presentModesCount;
    VkResult getSurfaceModeResult =
	vkGetPhysicalDeviceSurfacePresentModesKHR(
	    pDevice,
	    surface,
	    &presentModesCount,
	    NULL);
    if (svl_result_check(getSurfaceFormatResult, "Failed get PresentMode"))
	return svlSupport;

    svlSupport.PresentModes = malloc(sizeof(VkPresentModeKHR) * presentModesCount);

    getSurfaceModeResult =
	vkGetPhysicalDeviceSurfacePresentModesKHR(pDevice, surface, &presentModesCount, svlSupport.PresentModes);
    if (svl_result_check(getSurfaceFormatResult, "Failed get PresentMode"))
	return svlSupport;

    svlSupport.SurfaceFormatsCount = formatsCount;
    svlSupport.PresentModesCount = presentModesCount;

    return svlSupport;
}

void
_svl_swap_chain_support_details_destroy(SvlGraphSupport support)
{
    free(support.PresentModes);
    free(support.SurfaceFormats);
}

void
svl_queue_indices(SvlInstance* pInstance /* VkSurfaceKHR surface, _VkPhysicalDevice physicalDevice */)
{
    /*
      DOCS(typedef): Get Queue's
    */
    svl_u32 familyPropertyCount;
    vkGetPhysicalDeviceQueueFamilyProperties(pInstance->PhysicalDevice, &familyPropertyCount, NULL);
    VkQueueFamilyProperties* familyProperties = NULL;
    svl_array_reserve(familyProperties, familyPropertyCount);
    vkGetPhysicalDeviceQueueFamilyProperties(pInstance->PhysicalDevice, &familyPropertyCount, familyProperties);

    //svl_info("Families count: %d\n", familyPropertyCount);

    svl_i32 graphicsIndex = -1, presentationIndex = -1;
    SvlGraphSupport support;
    for (svl_i32 i = 0; i < familyPropertyCount; ++i)
    {
	char* flagAsStr = NULL;
	VkQueueFamilyProperties familyProperty = familyProperties[i];

	VkBool32 isSurfaceKhrSupported;
	VkResult getSupportForKhrResult =
	    vkGetPhysicalDeviceSurfaceSupportKHR(pInstance->PhysicalDevice, i, pInstance->Surface, &isSurfaceKhrSupported);
	if (svl_result_check(getSupportForKhrResult, "vkGetPhysicalDeviceSurfaceSupportKHR"))
	{
	    SvlReturnError(SvlErrorType_SwapChainSupport_CantGet);
	}

	support =
	    _svl_swap_chain_support_details(pInstance->PhysicalDevice, pInstance->Surface);

	svl_i8 isSwapChainSupported = support.SurfaceFormatsCount
	    && support.PresentModesCount;
	if (!isSwapChainSupported)
	{
	    _svl_swap_chain_support_details_destroy(support);
	    continue;
	}

	if (familyProperty.queueFlags & VK_QUEUE_GRAPHICS_BIT
	    && familyProperty.queueFlags & VK_QUEUE_COMPUTE_BIT)
	{
	    graphicsIndex = i;
	}

	if (isSurfaceKhrSupported == VK_TRUE)
	{
	    presentationIndex = i;
	}

	if (graphicsIndex != -1 && presentationIndex != -1)
	{
	    break;
	}
    }

    /*
      DOCS(typedef): yes, on most gpu's this queue indices is the same
    */
    if (graphicsIndex < 0 || presentationIndex < 0)
    {
	_svl_swap_chain_support_details_destroy(support);
	SvlReturnError(SvlErrorType_SwapChainSupport_CantGet);
    }

    pInstance->SurfaceFormatsCount = support.SurfaceFormatsCount;
    pInstance->PresentModesCount = support.PresentModesCount;
    pInstance->SurfaceCapabilities = support.SurfaceCapabilities;
    pInstance->SurfaceFormats = support.SurfaceFormats;
    pInstance->PresentModes = support.PresentModes;
    pInstance->GraphicsIndex = graphicsIndex;
    pInstance->PresentationIndex = presentationIndex;
}

void
_svl_device_create(SvlInstance* pInstance /* _VkInstance _vkInstance, _VkPhysicalDevice physicalDevice, SvlGraphSupport graphSupport */)
{
    /*
      DOCS(typedef): Create (Logical) Device
    */
    const char* deviceExts[] = {
	VK_KHR_SWAPCHAIN_EXTENSION_NAME
    };

    for (svl_i32 d = 0; d < SvlArrayCount(deviceExts); ++d)
    {
	const char* ext = deviceExts[d];

	svl_i32 ind = -1, extCnt = svl_array_count(pInstance->aExts);
	for (svl_i32 e = 0; e < extCnt; ++e)
	{
	    VkExtensionProperties prop = pInstance->aExts[e];

	    if (_svl_string_compare(prop.extensionName, ext))
	    {
		ind = e;
		break;
	    }
	}

	// note: We need it all
	if (ind == -1)
	{
	    SvlReturnError(SvlErrorType_Device);
	}
    }

    // TODO(typedef): Check if video card support it
    // TODO(typedef): Move before physical device creation
    VkPhysicalDeviceFeatures physcialDeviceFeatures = {
	/*
	  DOCS(typedef): we need special features
	*/

	// DOCS(typedef): For antisotropic filtering for textures x8, x16
	.samplerAnisotropy = VK_TRUE,

	// DOCS(typedef): For seting descriptors inside render pass, mb we can avoid this
	//.shaderSampledImageArrayDynamicIndexing = VK_TRUE,

	// DOCS(typedef): For enabling none VK_POLYGON_MODE_FILL fill mode
	.fillModeNonSolid = VK_TRUE,

	// DOCS(typedef): MultiDrawIndirect feature for modern renderer
	.multiDrawIndirect = VK_TRUE
    };

    VkPhysicalDeviceVulkan12Features physicalFeatures2 = {
	.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_2_FEATURES,
	.shaderSampledImageArrayNonUniformIndexing = VK_TRUE,
    };

    /*
      DOCS(typedef): Create info for creating Queue's.
      As Queue's created within VkDevice we need make CreateInfo
      structs for them as well, before creating VkDevice.
    */
    svl_f32 queuePriority = 1.0f;
    VkDeviceQueueCreateInfo* queuesCreateInfos = NULL;
    VkDeviceQueueCreateInfo graphicsQueueCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
	.pNext = NULL,
	.flags = 0,
	.queueFamilyIndex = pInstance->GraphicsIndex,
	.queueCount = 1,
	.pQueuePriorities = &queuePriority
    };
    svl_array_push(queuesCreateInfos, graphicsQueueCreateInfo);

    /*
      DOCS(typedef): If .GraphicsIndex == .PresentationIndex then
      we need to pass it only once, if in app we have differentiation
    */
    if (pInstance->GraphicsIndex != pInstance->PresentationIndex)
    {
	VkDeviceQueueCreateInfo presentationQueueCreateInfo = {
	    .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
	    .pNext = NULL,
	    .flags = 0,
	    .queueFamilyIndex = pInstance->PresentationIndex,
	    .queueCount = 1,
	    .pQueuePriorities = &queuePriority
	};
	svl_array_push(queuesCreateInfos, presentationQueueCreateInfo);
    }

    VkDeviceCreateInfo deviceCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
	.pNext = &physicalFeatures2,
	.flags = 0,
	.queueCreateInfoCount = svl_array_count(queuesCreateInfos),
	.pQueueCreateInfos = queuesCreateInfos,
	.enabledExtensionCount = SvlArrayCount(deviceExts),
	.ppEnabledExtensionNames = deviceExts,
	.pEnabledFeatures = &physcialDeviceFeatures
    };

    VkDevice vkDevice;
    VkResult deviceCreateResult = vkCreateDevice(pInstance->PhysicalDevice, &deviceCreateInfo, pInstance->pAllocator, &vkDevice);
    if (svl_result_check(deviceCreateResult, "// Failed Device Creation //")
	|| vkDevice == VK_NULL_HANDLE)
    {
	SvlReturnError(SvlErrorType_Device);
    }

    pInstance->Device = vkDevice;

    svl_array_free(queuesCreateInfos);
}

void
_svl_queue_create(SvlInstance* pInstance /* SvlDevice svlDevice, svl_i32 queueFamilyIndex, VkQueue* pQueue */)
{
    // note: compute queue should be created here
    vkGetDeviceQueue(pInstance->Device, pInstance->GraphicsIndex, 0, &pInstance->GraphicsQueue);
    if (pInstance->GraphicsQueue == VK_NULL_HANDLE)
	SvlReturnError(SvlErrorType_Queue);

    vkGetDeviceQueue(pInstance->Device, pInstance->PresentationIndex, 0, &pInstance->PresentationQueue);
    if (pInstance->PresentationQueue == VK_NULL_HANDLE)
	SvlReturnError(SvlErrorType_Queue);
}

void
svl_sync_create(SvlInstance* pInstance /* _VkInstance _vkInstance, SvlDevice svlDevice */)
{
    VkSemaphoreCreateInfo semaphoreCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
	.pNext = NULL,
	.flags = 0
    };

    VkFenceCreateInfo frameFenceCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
	.pNext = NULL,
	//NOTE(typedef): this will become unsignaled at the 2 line (vkResetFences) of the render loop
	.flags = VK_FENCE_CREATE_SIGNALED_BIT
    };

    VkResult createImageSemaphoreResult =
	vkCreateSemaphore(pInstance->Device, &semaphoreCreateInfo, pInstance->pAllocator, &pInstance->ImageAvailableSemaphore);
    if (svl_result_check(createImageSemaphoreResult, "Failed to create semaphore!"))
    {
	SvlReturnError(SvlErrorType_Sync);
    }

    VkResult createRenderSemaphoreResult =
	vkCreateSemaphore(pInstance->Device, &semaphoreCreateInfo, pInstance->pAllocator, &pInstance->RenderFinishedSemaphore);
    if (svl_result_check(createRenderSemaphoreResult, "Failed to create semaphore!"))
    {
	SvlReturnError(SvlErrorType_Sync);
    }

    VkResult createFrameFenceResult =
	vkCreateFence(pInstance->Device, &frameFenceCreateInfo, pInstance->pAllocator, &pInstance->FrameFence);
    if (svl_result_check(createFrameFenceResult, "Failed to create fence!"))
    {
	SvlReturnError(SvlErrorType_Sync);
    }
}

void
svl_swapchain_create(SvlInstance* pInstance, SvlSwapchainSettings settings)
{
    VkPresentModeKHR prefPresentMode;
    if (settings.IsVsync)
	prefPresentMode = VK_PRESENT_MODE_FIFO_KHR;
    else
	prefPresentMode = VK_PRESENT_MODE_IMMEDIATE_KHR;

    svl_u32 queueIndices[] = {
	pInstance->GraphicsIndex,
	pInstance->PresentationIndex,
    };

    /* VkSwapchainKHR */
/* vsa_swapchain_create(VsaSwapChainSettings set, VkSwapChainSupportDetails supportDetails, VsaQueueFamily queueFamily) */

    VkSurfaceFormatKHR pickedSurfaceFormat = pInstance->SurfaceFormats[0];
    VkPresentModeKHR pickedPresentationMode = pInstance->PresentModes[0];

    for (svl_i32 f = 0; f < pInstance->SurfaceFormatsCount; ++f)
    {
	VkSurfaceFormatKHR surfaceFormat = pInstance->SurfaceFormats[f];
	if (surfaceFormat.format == settings.SurfaceFormat.format
	    && surfaceFormat.colorSpace == settings.SurfaceFormat.colorSpace)
	{
	    pickedSurfaceFormat = surfaceFormat;
	    break;
	}
    }

    for (svl_i32 m = 0; m < pInstance->PresentModesCount; ++m)
    {
	VkPresentModeKHR presentMode = pInstance->PresentModes[m];
	if (presentMode == prefPresentMode)
	{
	    pickedPresentationMode = presentMode;
	    break;
	}
    }

    svl_i32 minImagesInSwapChain = SvlMinMax(
	pInstance->SurfaceCapabilities.minImageCount + 1, pInstance->SurfaceCapabilities.minImageCount, pInstance->SurfaceCapabilities.maxImageCount);

    VkExtent2D min = pInstance->SurfaceCapabilities.minImageExtent;
    VkExtent2D max = pInstance->SurfaceCapabilities.maxImageExtent;

// printf("Min(%d,%d) Max(%d,%d)\n", min.width, min.height, max.width, max.height);

    if (settings.IsDebug)
    {
	// DOCS: Validation layers cache surface data, wo this
	// we ll get error Validation Error: pCreateInfo->imageExtent (1999, 1200), which is outside the bounds returned by vkGetPhysicalDeviceSurfaceCapabilitiesKHR(): currentExtent = (2000,1200), minImageExtent = (2000,1200), maxImageExtent = (2000,1200). (https://vulkan.lunarg.com/doc/view/1.3.268.0/linux/1.3-extensions/vkspec.html#VUID-VkSwapchainCreateInfoKHR-pNext-07781)
	VkResult getSurfaceCapabilitiesResult =
	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(
	    pInstance->PhysicalDevice,
	    pInstance->Surface,
	    &pInstance->SurfaceCapabilities);
	//vkPrintSurfaceCapabilitiesLine(supportDetails.SurfaceCapabilities);
    }

    /* VkSwapchainPresentScalingCreateInfoEXT scalingExt = { */
    /*	.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_PRESENT_SCALING_CREATE_INFO_EXT, */
    /*	.pNext = NULL, */
    /*	/\* VkPresentScalingFlagsEXT *\/ */
    /*	.scalingBehavior = VK_PRESENT_SCALING_ASPECT_RATIO_STRETCH_BIT_EXT, */
    /*	//VK_PRESENT_SCALING_ONE_TO_ONE_BIT_EXT, */
    /*	/\* VkPresentGravityFlagsEXT *\/ */
    /*	.presentGravityX = VK_PRESENT_GRAVITY_CENTERED_BIT_EXT, */
    /*	/\* VkPresentGravityFlagsEXT *\/ */
    /*	.presentGravityY = VK_PRESENT_GRAVITY_CENTERED_BIT_EXT, */
    /* }; */

    VkSwapchainCreateInfoKHR swapChainCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
	.pNext = NULL,// todo: &scalingExt,
	.flags = 0,
	.surface = pInstance->Surface,
	.minImageCount = minImagesInSwapChain,
	.imageFormat = pickedSurfaceFormat.format,
	.imageColorSpace = pickedSurfaceFormat.colorSpace,
	.imageExtent = settings.Resolution,
	.imageArrayLayers = 1,
	/*DOCS:
	  Use VK_IMAGE_USAGE_TRANSFER_DST_BIT if post-processing is involved
	  render image to post-processing color_attach and then copy with
	  memory operation back to swap_chain_image
	*/
	.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
	.preTransform = pInstance->SurfaceCapabilities.currentTransform,
	/*DOCS:
	  If the alpha channel should be used for blending with other windows in the window system set other value, else VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR
	*/
	.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
	.presentMode = pickedPresentationMode,
	.clipped = VK_TRUE,
	// DOCS: For resizing
	.oldSwapchain = settings.PrevSwapchain,
    };

    if (queueIndices[0] != queueIndices[1])
    {
	swapChainCreateInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
	swapChainCreateInfo.queueFamilyIndexCount = 2;
	swapChainCreateInfo.pQueueFamilyIndices = queueIndices;
    }
    else
    {
	swapChainCreateInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
    }

    // todo: impl post-processing
    if (settings.IsPostProcess)
    {
	/*DOCS:
	  Use VK_IMAGE_USAGE_TRANSFER_DST_BIT if post-processing is involved
	  render image to post-processing color_attach and then copy with
	  memory operation back to swap_chain_image
	*/
	swapChainCreateInfo.imageUsage = VK_IMAGE_USAGE_TRANSFER_DST_BIT;
    }

    VkSwapchainKHR swapchain;
    VkResult swapChainCreateResult =
	vkCreateSwapchainKHR(pInstance->Device, &swapChainCreateInfo, pInstance->pAllocator, &swapchain);
    if (svl_result_check(swapChainCreateResult, "Failed Swapchain creation!"))
	SvlReturnError(SvlErrorType_SwapChain_CantCreate);

    pInstance->Swapchain = swapchain;
    pInstance->SurfaceFormat = pickedSurfaceFormat;
    pInstance->Resolution = settings.Resolution;
    pInstance->aSwapImageViews = _svl_swapchain_image_views_create(pInstance, pickedSurfaceFormat.format);
    pInstance->SwapImageViewsCount = svl_array_count(pInstance->aSwapImageViews);
}

SvlCmd
svl_cmd_create(SvlInstance* pInstance /* SvlDevice svlDevice, _VkInstance _vkInstance, SvlGraphSupport graphSupport */)
{
    VkCommandPool vkCommandPool;
    VkCommandPoolCreateInfo commandPoolCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
	.pNext = NULL,
	.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
	.queueFamilyIndex = pInstance->GraphicsIndex
    };
    VkResult createCommandPoolResult =
	vkCreateCommandPool(pInstance->Device, &commandPoolCreateInfo, pInstance->pAllocator, &vkCommandPool);
    if (svl_result_check(createCommandPoolResult, "Can't create Command Pool!"))
	return (SvlCmd) {};

    VkCommandBuffer vkCommandBuffer;

    VkCommandBufferAllocateInfo commandBufferAllocatedInfo = {
	.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
	.pNext = NULL,
	.commandPool = vkCommandPool,
	.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
	.commandBufferCount = 1
    };
    VkResult allocatingCommandBufferResult =
	vkAllocateCommandBuffers(pInstance->Device, &commandBufferAllocatedInfo, &vkCommandBuffer);
    if (svl_result_check(allocatingCommandBufferResult, "Can't allocate Command Buffer!"))
	return (SvlCmd) {};

    SvlCmd svlCmd = {
	.Pool = vkCommandPool,
	.Buffer = vkCommandBuffer
    };

    return svlCmd;
}

/*#######################
  DOCS(typedef): Svl Api
  #######################*/

SvlInstance
svl_create(SvlSettings* pSettings)
{
    if (pSettings->Paths.pRootDirectory == NULL)
    {
	svl_result_check(1, "Root directory not set.");
	return (SvlInstance) {};
    }

    SvlInstance svlInstance = {};

    _svl_instance_create(&svlInstance,
			 (_VkInstanceCreateInfo) {
			     .pName = pSettings->pName,
			     .VulkanVersion = pSettings->VulkanVersion,
			     .IsDebug = pSettings->IsDebug,
			     .pWindowBackend = pSettings->pWindowBackend
			 });
    int value = 10;

    if (svlInstance.Error != SvlErrorType_None)
	return svlInstance;

    _svl_surface_create(&svlInstance);
    if (svlInstance.Error != SvlErrorType_None)
	return svlInstance;

    _svl_physical_device_create(&svlInstance);
    if (svlInstance.Error != SvlErrorType_None)
	return svlInstance;

    if (pSettings->IsDevicePropsPrintable)
	_svl_physical_device_print(svlInstance.Properties);

    /* vsa_queue_family_create(&gVulkanSupportDetails); */
    svl_queue_indices(&svlInstance);

    _svl_device_create(&svlInstance);
    if (svlInstance.Device == VK_NULL_HANDLE || svlInstance.Error != SvlErrorType_None)
	return svlInstance;

    _svl_queue_create(&svlInstance);
    if (svlInstance.Error != SvlErrorType_None)
	return svlInstance;

    svl_sync_create(&svlInstance);
    if (svlInstance.Error != SvlErrorType_None)
	return svlInstance;

    SvlSwapchainSettings svlSettings = {
	.IsVsync = pSettings->IsVsync,
	.IsDebug = pSettings->IsDebug,
	.IsPostProcess = pSettings->IsPostProcess,
	.SurfaceFormat = (VkSurfaceFormatKHR) {
	    .format = VK_FORMAT_B8G8R8A8_SRGB,
	    .colorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR
	},
	.Resolution = (VkExtent2D) {
	    .width = pSettings->Extent.Width,
	    .height = pSettings->Extent.Height
	},
    };

    svl_swapchain_create(&svlInstance, svlSettings);

    SvlCmd svlCmd = svl_cmd_create(&svlInstance);
    if (svlCmd.Pool == VK_NULL_HANDLE || svlCmd.Buffer == VK_NULL_HANDLE)
    {
	svlInstance.Error = SvlErrorType_Cmd;
	return svlInstance;
    }

    const char* pShaderDirectory = "Resources/Shaders";
    if (pSettings->Paths.pShaderDirectory != NULL)
    {
	pShaderDirectory = pSettings->Paths.pShaderDirectory;
    }
    const char* pRootDirectory = pSettings->Paths.pRootDirectory;

    svlInstance.IsInitialized = 1;
    svlInstance.Cmd = svlCmd;
    svlInstance.Paths = (SvlPaths) {
	.pShaderDirectory = pShaderDirectory,
	.pRootDirectory = pRootDirectory
    };

    return svlInstance;
}

void
svl_destroy(SvlInstance* pInstance)
{
    { // DOCS: Destroy swapchain image views
	for (svl_i32 i = 0; i < pInstance->SwapImageViewsCount; ++i)
	{
	    SvlSwapImageView image = pInstance->aSwapImageViews[i];
	    vkDestroyImageView(pInstance->Device, image.View, pInstance->pAllocator);
	}
    }

    { // DOCS: Destroy swapchain
	vkDestroySwapchainKHR(pInstance->Device, pInstance->Swapchain, pInstance->pAllocator);
    }

    { // DOCS: Destroy render passes
	for (svl_i32 r = 0; r < svl_array_count(pInstance->aRenderPasses); ++r)
	{
	    svl_render_pass_destroy(pInstance, pInstance->aRenderPasses[r]);
	}
    }

    { // DOCS: Destroy sync primitives
	vkDestroySemaphore(pInstance->Device, pInstance->ImageAvailableSemaphore, pInstance->pAllocator);
	vkDestroySemaphore(pInstance->Device, pInstance->RenderFinishedSemaphore, pInstance->pAllocator);
	vkDestroyFence(pInstance->Device, pInstance->FrameFence, pInstance->pAllocator);

    }

    { // DOCS: Destroy cmd
	vkDestroyCommandPool(pInstance->Device, pInstance->Cmd.Pool, pInstance->pAllocator);
    }

    { // DOCS: Destroy (logical) device
	// todo/bug:
	vkDestroyDevice(pInstance->Device, pInstance->pAllocator);
    }

    { // DOCS: Destroy debug utils
	if (pInstance->IsDebugMode)
	{
	    PFN_vkDestroyDebugUtilsMessengerEXT vkDestroyDebugUtilsMessengerEXT
		= (PFN_vkDestroyDebugUtilsMessengerEXT)
		vkGetInstanceProcAddr(pInstance->Handle, "vkDestroyDebugUtilsMessengerEXT");

	    vkDestroyDebugUtilsMessengerEXT(pInstance->Handle, pInstance->DebugMessenger, pInstance->pAllocator);
	}

    }

    { // DOCS: Destroy surface
	vkDestroySurfaceKHR(pInstance->Handle, pInstance->Surface, pInstance->pAllocator);
    }

    { // DOCS: Destroy instance
	// todo/bug:
	vkDestroyInstance(pInstance->Handle, pInstance->pAllocator);
    }

}

static VkShaderModule
_svl_shader_module_create_ext(VkDevice device, svl_u32* byteCode, size_t byteCodeSize)
{
    VkShaderModuleCreateInfo shaderModuleCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
	.pNext = NULL,
	.flags = 0,
	.codeSize = byteCodeSize,
	.pCode = byteCode
    };

    VkShaderModule shaderModule;
    VkResult createShaderModuleResult =
	vkCreateShaderModule(device, &shaderModuleCreateInfo,  NULL /* gAllocatorCallbacks */, &shaderModule);
    //vkValidResult(createShaderModuleResult, "Can't create shader!");
    if (svl_result_check(createShaderModuleResult, "Can't create shader"))
    {
	// return error;
	return shaderModule;
    }

    return shaderModule;
}

SvlRenderPass*
_svl_get_render_pass(SvlInstance* pInstance, svl_i32 index, SvlErrorType* pError)
{
    svl_i32 cnt = svl_array_count(pInstance->aRenderPasses);
    if (index >= cnt || index < 0)
    {
	*pError = SvlErrorType_NoRenderPass;
	return NULL;
    }

    SvlRenderPass* pRenderPass = &pInstance->aRenderPasses[index];
    return pRenderPass;
}

SvlShaderGroupSettings
_svl_get_shader_group_settings(SvlShaderStage* aStages, svl_i32 stagesCount)
{
    SvlShaderGroupSettings groupSettings = {};

    for (svl_i32 i = 0; i < stagesCount; ++i)
    {
	SvlShaderStage stage = aStages[i];
	svl_guard(stage.Type != SvlShaderType_Count && "SvlShaderType_Count used in pipe settings.");
	groupSettings.aPaths[stage.Type] = stage.ShaderSourcePath;
    }

    return groupSettings;
}

svl_i32
_svl_string_compare(const char* s0, const char* s1)
{
    char* p0 = (char*) s0;
    char* p1 = (char*) s1;

    if (s0 == NULL || s1 == NULL)
	return 0;

    char c0, c1;
    while (1)
    {
	c0 = *p0;
	c1 = *p1;

	if (c0 != c1)
	    return 0;
	else if (c0 == '\0' || c1 == '\0')
	    return 1;

	p0++;
	p1++;
    }

}

svl_i64
_svl_string_length(const char* p)
{
    char* ptr;
    for (ptr = (char*) p; *ptr != '\0'; ++ptr);

    return (svl_i64) ((svl_size)(ptr - p));
}

char*
_svl_path_combine(const char* left, const char* right)
{
    svl_guard(left != NULL);
    svl_guard(right != NULL);

    /*
      1. Adding paths:
      * path_combine("Image/", "a.png");
      * path_combine("Image/", "/a.png");
      * path_combine("Image/", "./a.png");
      */

    svl_i64 leftLength  = _svl_string_length(left);

    char leftLastChar = left[leftLength-1];
    svl_i32 isLeftSlashed = (leftLastChar == '/' || leftLastChar == '\\');

    char* rightMod = NULL;
    svl_i64 leftModLen;
    svl_i64 rightModLen;

    if (isLeftSlashed)
	leftModLen = leftLength - 1;
    else
	leftModLen = leftLength;

    { // DOCS(typedef): set rightMod
	char* ptr = (char*) right;
	char c = *ptr;
	while (c == '.' || c == '/' || c == '\\' || c == ' ' || c == '\n' || c == '\t' || c == '\r')
	{
	    ++ptr;
	    c = *ptr;
	}

	// DOCS(typedef): right is empty string for us
	if (*ptr == '\0')
	{
	    return _svl_string(left);
	}

	rightModLen = _svl_string_length(ptr);
	rightMod = ptr;
    }

    svl_i64 resultLength = leftModLen + rightModLen + 1 /* slash between */ + 1 /*\0*/;
    char* result = (char*) malloc(resultLength);

    char* ptr = result;
    memcpy(ptr, left, leftModLen);

    ptr += leftModLen;
    *ptr = '/';

    ++ptr;
    memcpy(ptr, rightMod, rightModLen);

    ptr += rightModLen;
    *ptr = '\0';

    return result;
}

const char*
_svl_path_get_name(const char* p)
{
    if (p == NULL)
	return 0;

    svl_i64 plen = _svl_string_length(p);
    svl_i64 last = plen - 1;

    char* ptr = (char*) p;
    char* pEnd = (char*) (ptr + plen - 1);

    for (svl_i64 i = last; i >= 0; --i)
    {
	char c = *pEnd;
	if (c == '/' || c == '\\')
	{
	    return (const char*) (pEnd + 1);
	}

	--pEnd;
    }

    return p;
}

char*
_svl_path_get_name_wo_ext(const char* p)
{
    if (p == NULL)
	return 0;

    svl_i64 plen = _svl_string_length(p);
    svl_i64 last = plen - 1;
    svl_i64 dotIndex = -1;
    char* ptr = (char*) p;

    char* pEnd = (char*) (ptr + plen - 1);

    for (svl_i64 i = last; i >= 0; --i)
    {
	char c = *pEnd;
	if (c == '.')
	{
	    dotIndex = i;
	}
	else if (c == '/' || c == '\\')
	{
	    // 0123456789
	    //     456
	    // /pat/a.txt
	    svl_i64 nameInd = (i + 1);
	    svl_i64 nameWoExtLen = dotIndex - nameInd;
	    char* res = (char*) malloc(nameWoExtLen+1);
	    res[nameWoExtLen] = '\0';
	    memcpy(res, pEnd + 1, nameWoExtLen);

	    return (char*) res;
	}

	--pEnd;
    }

    return _svl_string(p);
}

const char*
_svl_path_get_ext(const char* p)
{
    if (p == NULL)
	return 0;

    svl_i64 plen = _svl_string_length(p);
    svl_i64 last = plen - 1;
    char* ptr = (char*) p;

    char* pEnd = (char*) (ptr + plen - 1);

    for (svl_i64 i = last; i >= 0; --i)
    {
	char c = *pEnd;
	if (c == '.')
	{
	    return (const char*) pEnd;
	}
	--pEnd;
    }

    return p;
}

char
_svl_char_to_upper(char c)
{
    if (c >= 'a' && c <= 'z')
	return 'A' + (c - 'a');
    return c;
}

char*
_svl_string(const char* s)
{
    svl_i64 len = _svl_string_length(s) + 1;
    char* str = malloc(len);
    memcpy(str, s, len);
    return str;
}

char*
_svl_string_concat(const char* p0, const char* p1)
{
    svl_i64 leftLength = _svl_string_length(p0);
    svl_i64 rightLength = _svl_string_length(p1);
    svl_i64 bothLength = leftLength + rightLength;

    char* newString = (char*) malloc(bothLength + 1);

    memcpy(newString, p0, leftLength);
    memcpy(newString + leftLength, p1, rightLength);
    newString[bothLength] = '\0';

    return newString;
}

void*
_svl_file_read_bytes(const char* pPath, svl_i64* pLength)
{
    FILE* file;
    void* pResult;
    svl_i64 size;

    file = fopen(pPath, "rb");
    if (file)
    {
	fseek(file, 0, SEEK_END);
	size = (svl_i64) ftell(file);
	fseek(file, 0, SEEK_SET);
	pResult = malloc(size);

	fread(pResult, 1, size, file);
	*pLength = size;

	fclose(file);
	return pResult;
    }

    return NULL;
}

svl_i32
_svl_path_file_exist(const char* path)
{
#if defined(SVL_LINUX_PLATFORM)
    struct stat buf;
    svl_i32 result = stat(path, &buf);
    return (result != -1);
#elif defined(SVL_WINDOWS_PLATFORM)
    BOOL isFileExist = PathFileExistsA(path);
    return isFileExist;
#endif
}

svl_i32
_svl_path_directory_exist(const char* pPath)
{
#if defined(SVL_LINUX_PLATFORM)
    struct stat buf;
    svl_i32 result = stat(pPath, &buf);
    return (result != -1);
#elif defined(SVL_WINDOWS_PLATFORM)
    DWORD result = GetFileAttributesA(path);
    return (result != INVALID_FILE_ATTRIBUTES && (result & FILE_ATTRIBUTE_DIRECTORY));
#endif
}

svl_i32
_svl_directory_create(const char* pPath)
{
#if defined(SVL_LINUX_PLATFORM)

    /*
      #define S_IRWXU 0000700    RWX mask for owner
      #define S_IRUSR 0000400    R for owner
      #define S_IWUSR 0000200    W for owner
      #define S_IXUSR 0000100    X for owner

      #define S_IRWXG 0000070    RWX mask for group
      #define S_IRGRP 0000040    R for group
      #define S_IWGRP 0000020    W for group
      #define S_IXGRP 0000010    X for group

      #define S_IRWXO 0000007    RWX mask for other
      #define S_IROTH 0000004    R for other
      #define S_IWOTH 0000002    W for other
      #define S_IXOTH 0000001    X for other

      #define S_ISUID 0004000    set user id on execution
      #define S_ISGID 0002000    set group id on execution
      #define S_ISVTX 0001000    save swapped text even after use
    */
    svl_i32 result = mkdir(pPath, S_IRWXU);
    if (result != 0)
	return 0;
    return 1;

#elif defined(SVL_WINDOW_PLATFORM)

    if (!CreateDirectoryA(pPath, NULL))
	return 0;
    return 1;

#endif
}

svl_size
_svl_path_get_last_mod_time(const char* pPath)
{
#if defined(SVL_LINUX_PLATFORM)

    struct stat fileItemInfo;
    stat(pPath, &fileItemInfo);
    return fileItemInfo.st_mtime;

#elif defined(SVL_WINDOWS_PLATFORM)

    svl_i64 accessTime, creationTime, writeTime;
    io_get_file_times_ms(path, &at, &ct, &wt);

    HANDLE file = CreateFileA(path, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    if (file == INVALID_HANDLE_VALUE)
    {
	GERROR("Can't open file: %s\n", path);
	svl_assert_break();
    }
    FILETIME ct, at, wt;
    BOOL result = GetFileTime(file, &ct, &at, &wt);
    if (result == FALSE)
    {
	GERROR("Can't get file time: %s\n", path);
	svl_assert_break();
    }

    SYSTEMTIME systemAccessTime;
    FileTimeToSystemTime(&at, &systemAccessTime);
    SYSTEMTIME systemCreationTime;
    FileTimeToSystemTime(&ct, &systemCreationTime);
    SYSTEMTIME systemWriteTime;
    FileTimeToSystemTime(&wt, &systemWriteTime);

    *accessTime = systemAccessTime.wMilliseconds;
    *creationTime = systemCreationTime.wMilliseconds;
    *writeTime = systemWriteTime.wMilliseconds;

    return wt;

#endif
}

char
svl_char_to_upper(char c)
{
    if (c >= 'a' && c <= 'z')
	return (c - 'a' + 'A');
    else
	return c;
}

static svl_i32
_svl_is_shader_compilation_needed(SvlInstance* pInstance, const char* path, const char* spvPath)
{
    if (_svl_path_file_exist(spvPath) == 0)
	return 1;

    svl_size lastModificationTimeRaw = _svl_path_get_last_mod_time(path);
    svl_size lastCreationTimeRaw = _svl_path_get_last_mod_time(spvPath);

    if (lastModificationTimeRaw > lastCreationTimeRaw)
	return 1;

    return 0;
}


SvlShaderBytecode
svl_shader_compile_single_unit_new(SvlInstance* pInstance,
				   const char* pPath,
				   const char* pSourceDir,
				   const char* pBinDir)
{
    // note: commit with shaderc inside code, not util d735f717589aa0c023c73b5b82946690844f826d

    SvlShaderBytecode byteCode = {};

    const char* binDir = pBinDir;
    const char* sourceDir = pSourceDir;

    const char* pName = _svl_path_get_name(pPath);/*Vsr3dd.frag*/
    char* sourceDirA = _svl_path_combine(sourceDir, pName);

    const char* nameWoExt = _svl_path_get_name_wo_ext(pPath); // Vsr3dd
    char* ext = _svl_string(_svl_path_get_ext(pPath) + 1); //frag
    ext[0] = _svl_char_to_upper(ext[0]);
    char* ending = _svl_string_concat(ext, ".spv"); // Frag.spv
    char* fullNameWoDir = _svl_string_concat(nameWoExt, ending);
    char* spvPath = _svl_path_combine(binDir, fullNameWoDir); //full path spv

    svl_i32 isRecompilationNeeded =
	_svl_is_shader_compilation_needed(pInstance, sourceDirA, spvPath);
    if (!isRecompilationNeeded)
    {
	svl_i64 size;
	void* bytes = _svl_file_read_bytes(spvPath, &size);
	byteCode = (SvlShaderBytecode) {
	    .Bytecode = bytes,
	    .Size = size,
	};

	goto EndSvlShaderCompileSingleUnitLabel;
    }

    // glslc Vsr3dd.frag -O -o Vsr3ddFrag.spv --target-env=vulkan1.3 -w -x glsl
    // glslc Vsr3dd.vert -O -o Vsr3ddVert.spv --target-env=vulkan1.3 -w -x glsl

    const char* optimization = "";
    svl_i32 isOptimizationNeeded = 1;
    if (isOptimizationNeeded)
    {
	optimization = "-O";
    }

    printf("sourceDirA: %s\n", sourceDirA);
    printf("optimization: %s\n", optimization);
    printf("spvPath: %s\n", spvPath);

    char cmd[1024] = {};
    sprintf(cmd, "glslc %s %s -o %s --target-env=vulkan1.3 -w -x glsl", sourceDirA, optimization, spvPath);
    system(cmd);

    //svl_assert(path_is_file_exist(spvPath) && "Path for spv is not exist!");

    svl_i64 size = 0;
    void* pBytecode = _svl_file_read_bytes(spvPath, &size);
    byteCode = (SvlShaderBytecode) {
	.Bytecode = pBytecode,
	.Size = size,
    };

EndSvlShaderCompileSingleUnitLabel:
    free(sourceDirA);
    free(fullNameWoDir);
    free((void*)nameWoExt);
    free(ext);
    free(ending);
    free(spvPath);

    return byteCode;

}

SvlShaderGroup
_svl_shader_group_create(SvlInstance* pInstance, SvlShaderGroupSettings* pGroupSettings, SvlErrorType* pError)
{
    const char* pShaderDir = pInstance->Paths.pShaderDirectory;
    const char* pRootDir = pInstance->Paths.pRootDirectory;

    SvlShaderGroup group = {};

    if (pInstance->IsDebugMode)
    {
	svl_i32 isAllNull = 1;
	for (svl_i32 i = 0; i < SvlShaderType_Count; ++i)
	{
	    if (pGroupSettings->aPaths[i] != NULL)
	    {
		isAllNull = 0;
		break;
	    }
	}

	if (isAllNull)
	{
	    svl_result_check(isAllNull, "Compile Settings all paths for shader units is NULL!\n");
	    *pError = SvlErrorType_Shader_NoPath;
	}
    }

    char* pSourceDir = _svl_path_combine(pRootDir, pShaderDir);
    const char* dirs[] = {
	[0] = pSourceDir,
	[1] = _svl_path_combine(pSourceDir, "Bin"),
    };

    for (svl_i32 i = 0; i < SvlArrayCount(dirs); ++i)
    {
	const char* dir = dirs[i];
	svl_i32 isDirectoryExist = _svl_path_directory_exist(dir);
	if (!isDirectoryExist)
	{
	    _svl_directory_create(dir);
	}
    }

    for (svl_i32 i = 0; i < SvlShaderType_Count; ++i)
    {
	SvlShaderType type = (SvlShaderType) i;
	const char* pPath = pGroupSettings->aPaths[type];
	if (pPath == NULL)
	    continue;

	SvlShaderBytecode byte =
	    svl_shader_compile_single_unit_new(pInstance, pPath, dirs[0], dirs[1]);
	group.aBytecodes[type] = byte;

	if (byte.Bytecode == NULL || byte.Size == 0)
	{
	    *pError = SvlErrorType_Shader_BytecodeInvalid;
	}
    }

    for (svl_i32 i = 0; i < SvlArrayCount(dirs); ++i)
    {
	free((void*)dirs[i]);
    }

    return group;
}

SvlBinding
_svl_binding_create(SvlInstance* pInstance, SvlShaderStage* aStages, svl_i32 stagesCount)
{
    svl_i32 bindingIndex;
    VkDescriptorSetLayoutBinding* aBindings = NULL;
    VkDescriptorPoolSize* aPoolSizes = NULL;
    SvlBinding svlBinding = {};

    bindingIndex = 0;

    for (svl_i32 i = 0; i < stagesCount; ++i)
    {
	SvlShaderStage stage = aStages[i];
	VkShaderStageFlagBits stageFlag = svl_shader_type_to_flag(stage.Type);

	for (svl_i32 d = 0; d < stage.DescriptorsCount; ++d)
	{
	    SvlDescriptor svlDescr = stage.aDescriptors[d];
	    svl_guard(svlDescr.Count > 0);
	    svlDescr.Binding = bindingIndex;
	    svl_i32 descrCnt = 1;

	    switch (svlDescr.Type)
	    {

	    case VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER:
	    {
		SvlUniformSettings uniformSet = {
		    .Size = svlDescr.Size * svlDescr.Count,
		    .ItemSize = svlDescr.Size,
		    .BindIndex = svlDescr.Binding,
		    .pName = svlDescr.pName,
		};

		SvlUniform uniform =
		    svl_uniform_create(pInstance, uniformSet);
		svl_array_push(svlBinding.aUniforms, uniform);
		break;
	    }

	    case VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE:
	    { // DOCS: Probably a texture
		// svl_guard(svlDescr.aImageInfos == NULL);
		// svl_array_reserve(svlDescr.aImageInfos, svlDescr.Count);
		// svl_array_header(svlDescr.aImageInfos)->Count = svlDescr.Count;
		descrCnt = svlDescr.Count;

		svl_array_push(svlBinding.aDescriptors, svlDescr);
		break;
	    }

	    case VK_DESCRIPTOR_TYPE_SAMPLER:
	    {
		// svl_guard(svlDescr.aImageInfos == NULL);
		// svl_array_reserve(svlDescr.aImageInfos, svlDescr.Count);
		// svl_array_header(svlDescr.aImageInfos)->Count = svlDescr.Count;
		descrCnt = svlDescr.Count;

		svl_array_push(svlBinding.aDescriptors, svlDescr);
		break;
	    }

	    case VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER:
	    {
		descrCnt = svlDescr.Count;
		svl_array_push(svlBinding.aDescriptors, svlDescr);
		break;
	    }

	    default:
		svl_guard(0);
		break;

	    }

	    /* VkDescriptorSetLayoutBinding* aBindings = NULL; */
	    /* VkDescriptorPoolSize* aPoolSizes = NULL; */
	    VkDescriptorSetLayoutBinding binding = {
		.binding = bindingIndex,
		.descriptorType = svlDescr.Type,
		.descriptorCount = descrCnt,
		.stageFlags = stageFlag,
		.pImmutableSamplers = NULL
	    };

	    VkDescriptorPoolSize poolSize = {
		.type = svlDescr.Type,
		.descriptorCount = descrCnt
	    };

	    svl_array_push(aBindings, binding);
	    svl_array_push(aPoolSizes, poolSize);

	    ++bindingIndex;

	}
    }

    VkDescriptorSetLayoutCreateInfo descriptorCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
	.pNext = NULL,
	.flags = 0,//VK_DESCRIPTOR_SET_LAYOUT_CREATE_UPDATE_AFTER_BIND_POOL_BIT,//0,
	.bindingCount = svl_array_count(aBindings),
	.pBindings = aBindings
    };//VkDescriptorSetLayoutBinding

    VkDescriptorSetLayout vkDescriptorSetLayout;
    VkResult creteDescriptorSetLayout =
	vkCreateDescriptorSetLayout(pInstance->Device, &descriptorCreateInfo, pInstance->pAllocator, &vkDescriptorSetLayout);
    if (svl_result_check(creteDescriptorSetLayout, "Failed descriptor set layout creation!"))
    {
    }

    VkDescriptorPoolCreateInfo vkPoolCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
	.pNext = NULL,
	.flags = VK_DESCRIPTOR_POOL_CREATE_UPDATE_AFTER_BIND_BIT,
	.maxSets = 1,
	.poolSizeCount = svl_array_count(aPoolSizes),
	.pPoolSizes = aPoolSizes,
    };

    VkDescriptorPool vkDescriptorPool;
    VkResult descriptorPoolCreateResult =
	vkCreateDescriptorPool(pInstance->Device, &vkPoolCreateInfo, pInstance->pAllocator, &vkDescriptorPool);

    if (svl_result_check(descriptorPoolCreateResult, "Failed Descriptor Pool Creation!!!"))
    {
    }

    VkDescriptorSet vkDescriptorSet;
    VkDescriptorSetAllocateInfo vkDescriptorSetAllocateInfo = {
	.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
	.pNext = NULL,
	.descriptorPool = vkDescriptorPool,
	.descriptorSetCount = 1,
	.pSetLayouts = &vkDescriptorSetLayout,
    };

    VkResult allocateDescritorSetsResult =
	vkAllocateDescriptorSets(pInstance->Device, &vkDescriptorSetAllocateInfo, &vkDescriptorSet);
    if (svl_result_check(allocateDescritorSetsResult, "Failed allocate descriptor sets!"))
    {
    }

    svl_array_free(aBindings);
    svl_array_free(aPoolSizes);

    svlBinding.DescriptorSet = vkDescriptorSet;
    svlBinding.DescriptorSetLayout = vkDescriptorSetLayout;
    svlBinding.DescriptorPool = vkDescriptorPool;

    return svlBinding;
}

void
svl_binding_destroy(SvlInstance* pInstance, SvlBinding binding)
{
    for (svl_i32 i = 0; i < svl_array_count(binding.aUniforms); ++i)
    {
	svl_uniform_destroy(pInstance, &binding.aUniforms[i]);
    }

    svl_array_free(binding.aUniforms);

    vkDestroyDescriptorSetLayout(pInstance->Device, binding.DescriptorSetLayout, pInstance->pAllocator);
    vkDestroyDescriptorPool(pInstance->Device, binding.DescriptorPool, pInstance->pAllocator);
}

SvlPipeline*
svl_pipeline_create(SvlInstance* pInstance, SvlPipelineSettings settings)
{
    VkPipeline vkPipeline;
    VkPipelineLayout vkPipelineLayout;

#if SVL_DEBUG
    if (settings.StagesCount == 0 && settings.aStages != NULL)
    {
	GWARNING("Weird param settings.StagesCount == 0 when settings.aStages != NULL!\n");
    }
    if (settings.AttributesCount == 0 && settings.aAttributes != NULL)
    {
	GWARNING("Weird param settings.AttributesCount == 0 when settings.aAttributes != NULL!\n");
    }
#endif

    SvlErrorType renderPassError = 0;
    SvlRenderPass* pRenderPass =
	_svl_get_render_pass(pInstance, settings.RenderPassGroupIndex, &renderPassError);

    SvlBinding svlBinding = _svl_binding_create(pInstance, settings.aStages, settings.StagesCount);

    SvlShaderGroupSettings groupSet =
	_svl_get_shader_group_settings(settings.aStages,
				       settings.StagesCount);
    //todo: shader compilation error
    SvlErrorType error = 0;
    SvlShaderGroup shaderGroup = _svl_shader_group_create(pInstance, &groupSet, &error);
    // todo:
    svl_guard(error == SvlErrorType_None);

    VkPipelineShaderStageCreateInfo* aStageCreateInfos = NULL;
    for (svl_i32 si = 0; si < settings.StagesCount; ++si)
    {
	SvlShaderStage stage = settings.aStages[si];

	svl_guard(stage.Type >= 0 && stage.Type <= SvlShaderType_Count);

	SvlShaderBytecode byteCode = shaderGroup.aBytecodes[stage.Type];

	VkShaderModule vkShaderModule =
	    _svl_shader_module_create_ext(pInstance->Device, byteCode.Bytecode, byteCode.Size);

	VkPipelineShaderStageCreateInfo stageCreateInfo = {
	    .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
	    .pNext = NULL,
	    .flags = 0,
	    .stage = svl_shader_type_to_flag(stage.Type),
	    .module = vkShaderModule,
	    .pName = "main",
	    /* NOTE(typedef): use this thing if we need constants in shader */
	    .pSpecializationInfo = NULL
	};

	svl_array_push(aStageCreateInfos, stageCreateInfo);
    }

    // NOTE(typedef): Vertex Buffer description
    VkVertexInputBindingDescription vkVertexInputBindingDescription = {
	.binding = 0,
	.stride = settings.Stride,
	.inputRate = VK_VERTEX_INPUT_RATE_VERTEX
    };

    VkVertexInputAttributeDescription* aAttributeDescrs = NULL;
    for (svl_i32 i = 0; i < settings.AttributesCount; ++i)
    {
	SvlShaderAttribute attr = settings.aAttributes[i];

	VkVertexInputAttributeDescription descr = {
	    .location = i,
	    .binding = attr.GroupBinding,
	    .format = attr.Format,
	    .offset = attr.Offset
	};

	svl_array_push(aAttributeDescrs, descr);
    }

    VkPipelineVertexInputStateCreateInfo pipelineVertexInputCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
	.vertexBindingDescriptionCount = 1,
	.pVertexBindingDescriptions = &vkVertexInputBindingDescription,
	.vertexAttributeDescriptionCount = svl_array_count(aAttributeDescrs),
	.pVertexAttributeDescriptions = aAttributeDescrs
    };

    VkDynamicState dynamicStates[2] = {
	[0] = VK_DYNAMIC_STATE_VIEWPORT,
	[1] = VK_DYNAMIC_STATE_SCISSOR
    };

    VkPipelineDynamicStateCreateInfo pipelineDynamicStateCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
	.pNext = NULL,
	.flags = 0,
	.dynamicStateCount = SvlArrayCount(dynamicStates),
	.pDynamicStates = dynamicStates,
    };

    VkPipelineInputAssemblyStateCreateInfo pipelineInputAssemblyCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
	.pNext = NULL,
	.flags = 0,
	.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
	.primitiveRestartEnable = VK_FALSE
    };

    // DOCS(typedef): passing viewport, scissor here make them immutable
    VkPipelineViewportStateCreateInfo pipelineViewportCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
	.pNext = NULL,
	.flags = 0,
	.viewportCount = 1,
	.scissorCount = 1
    };

    VkPipelineRasterizationStateCreateInfo pipelineRasterizationCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
	.pNext = NULL,
	.flags = 0,
	// NOTE(typedef): enabling that requires gpu feature
	.depthClampEnable = VK_FALSE,
	.rasterizerDiscardEnable = VK_FALSE,
	// NOTE(typedef): requires enabling gpu feature
	.polygonMode = settings.PolygonMode,
	.cullMode =
#if 1 // NOTE(): best option
	VK_CULL_MODE_NONE,
#else // DOCS: Using this for now
	VK_CULL_MODE_BACK_BIT,
#endif

	.frontFace =
#if 1 // NOTE(): best option
	VK_FRONT_FACE_COUNTER_CLOCKWISE,
#else
	VK_FRONT_FACE_CLOCKWISE,
#endif

	.depthBiasEnable = VK_FALSE,
	.depthBiasConstantFactor = 0.0f,
	.depthBiasClamp = 0.0f,
	.depthBiasSlopeFactor = 0.0f,
	// NOTE(typedef): anything other then 1.0f requires enabling GPU feature
	.lineWidth = 1.0f
    };

    /* if (!settings.EnableBackFaceCulling) */
    /* { */
    /*	pipelineRasterizationCreateInfo.cullMode = VK_CULL_MODE_NONE; */
    /* } */

    VkPipelineMultisampleStateCreateInfo pipelineMultisampleCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
	.pNext = NULL,
	.flags = 0,
	.rasterizationSamples = pRenderPass->Samples,
	.sampleShadingEnable = VK_FALSE,
	.minSampleShading = 1.0f,
	.pSampleMask = NULL,
	.alphaToCoverageEnable = VK_FALSE,
	.alphaToOneEnable = VK_FALSE
    };

    // NOTE(typedef): Depth Stencil code creation here
    VkPipelineDepthStencilStateCreateInfo depthStencilCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
	.pNext = NULL,
	.flags = 0,
	.depthTestEnable = VK_TRUE,
	.depthWriteEnable = VK_TRUE,
	.depthCompareOp = VK_COMPARE_OP_LESS,
	.depthBoundsTestEnable = VK_FALSE,
	.stencilTestEnable = VK_FALSE,
	.front = 0.0f,
	.back = 0.0f,
	.minDepthBounds = -1.0f,
	.maxDepthBounds =  1.0f
    };

    /* if (!settings.EnableDepthStencil) */
    /* { */
    //depthStencilCreateInfo = (VkPipelineDepthStencilStateCreateInfo) {};
    /*	vguard(0 && "Wtf, you foget .EnableDepthStencil = 1 !!!"); */
    /* } */

    // NOTE(typedef): Blending not configured now
#if 1
    VkPipelineColorBlendAttachmentState pipelineColorBlendAttachment = {
	.blendEnable = VK_TRUE,
	.colorBlendOp = VK_BLEND_OP_ADD,
	.alphaBlendOp = VK_BLEND_OP_ADD,

	.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA,
	.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,

	.srcAlphaBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA,
	.dstAlphaBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,

	.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT,
    };

#else
    VkPipelineColorBlendAttachmentState pipelineColorBlendAttachment = {
	.blendEnable = VK_TRUE,
	.colorBlendOp = VK_BLEND_OP_ADD,
	.alphaBlendOp = VK_BLEND_OP_ADD,

	.srcColorBlendFactor = VK_BLEND_FACTOR_ONE,
	.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO,
	.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE,
	.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO,

	.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT,
    };

#endif

    VkPipelineColorBlendStateCreateInfo pipelineColorBlendCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
	.logicOpEnable = VK_FALSE,
	.attachmentCount = 1,
	.pAttachments = &pipelineColorBlendAttachment,
    };

    VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
	.pNext = NULL,
	.flags = 0,
	.setLayoutCount = 1,
	.pSetLayouts = &svlBinding.DescriptorSetLayout,
	.pushConstantRangeCount = 0,
	.pPushConstantRanges = NULL
    };

    VkResult createPipelineLayoutResult =
	vkCreatePipelineLayout(pInstance->Device, &pipelineLayoutCreateInfo, pInstance->pAllocator, &vkPipelineLayout);
    if (svl_result_check(createPipelineLayoutResult, "Can't create pipeline layout!"))
    {
	pInstance->Error = SvlErrorType_Pipeline_Layout;
	return NULL;
    }

    VkGraphicsPipelineCreateInfo graphicsPipelineCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
	.pNext = NULL,
	.flags = 0,
	.stageCount = svl_array_count(aStageCreateInfos),
	.pStages = aStageCreateInfos,
	.pVertexInputState = &pipelineVertexInputCreateInfo,
	.pInputAssemblyState = &pipelineInputAssemblyCreateInfo,
	.pTessellationState = NULL,
	.pViewportState = &pipelineViewportCreateInfo,
	.pRasterizationState = &pipelineRasterizationCreateInfo,
	.pMultisampleState = &pipelineMultisampleCreateInfo,
	.pDepthStencilState = &depthStencilCreateInfo,
	.pColorBlendState = &pipelineColorBlendCreateInfo,
	.pDynamicState = &pipelineDynamicStateCreateInfo,
	.layout = vkPipelineLayout,
	.renderPass = pRenderPass->Handle,
	.subpass = 0,
	.basePipelineHandle = VK_NULL_HANDLE,
	.basePipelineIndex = -1
    };

    VkResult createGraphicsPipelinesResult =
	vkCreateGraphicsPipelines(pInstance->Device, VK_NULL_HANDLE, 1, &graphicsPipelineCreateInfo, pInstance->pAllocator, &vkPipeline);
    if (svl_result_check(createGraphicsPipelinesResult, "Can't create Graphics Pipeline!"))
    {
	pInstance->Error = SvlErrorType_Pipeline;
	return NULL;
    }

    SvlPipeline* pPipe = NULL;
    {
	SvlPipeline svlPipeline = {
	    .Binding = svlBinding,
	    .Handle = vkPipeline,
	    .Layout = vkPipelineLayout,
	    .MaxInstanceCount = settings.MaxInstanceCount
	};

	pPipe = malloc(sizeof(SvlPipeline));
	memcpy(pPipe, &svlPipeline, sizeof(SvlPipeline));
	svl_array_push(pRenderPass->apPipelines, pPipe);
	++pRenderPass->PipelinesCount;
    }

    // DOCS(typedef): Clean up
    svl_array_free(aAttributeDescrs);

    // note: free bytecode mem
    for (SvlShaderType i = SvlShaderType_Vertex; i < SvlShaderType_Count; ++i)
    {
	SvlShaderBytecode svlBytecode = shaderGroup.aBytecodes[i];
	free(svlBytecode.Bytecode);
    }

    for (svl_i32 i = 0; i < svl_array_count(aStageCreateInfos); ++i)
    {
	VkPipelineShaderStageCreateInfo stageCreateInfo = aStageCreateInfos[i];
	vkDestroyShaderModule(pInstance->Device, stageCreateInfo.module, pInstance->pAllocator);
    }

    svl_array_free(aStageCreateInfos);

    return pPipe;
}

void
svl_pipeline_destroy(SvlInstance* pInstance, SvlPipeline* pPipe)
{
    svl_binding_destroy(pInstance, pPipe->Binding);

    svl_buffer_destroy(pInstance, &pPipe->Vertex);
    svl_buffer_destroy(pInstance, &pPipe->Index);

    vkDestroyPipeline(pInstance->Device, pPipe->Handle, pInstance->pAllocator);
    vkDestroyPipelineLayout(pInstance->Device, pPipe->Layout, pInstance->pAllocator);

    free(pPipe);
}

void
svl_pipeline_create_vertex_buffer(SvlInstance* pInstance, SvlPipeline* pPipe, svl_size totalSize)
{
    //svl_i64 maxVerticesCount = 4 * pPipe->MaxInstanceCount;
    //svl_size vertexBufferSize = maxVerticesCount * singleInstanceSize;

    SvlErrorType verr = 0;
    SvlBufferSettings vset = {
	.Size = totalSize,
	.Usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
    };

    SvlBuffer vbuf = svl_buffer_create(pInstance, vset, &verr);
    if (verr != 0)
    {
	pInstance->Error = verr;
	return;
    }

    pPipe->Vertex = vbuf;
}

void
svl_pipeline_create_index_buffer(SvlInstance* pInstance, SvlPipeline* pPipe, svl_size totalSize)
{
    //svl_i64 maxIndicesCount = 6 * pPipe->MaxInstanceCount;
    //svl_size indexBufferSize = maxIndicesCount * singleInstanceSize;

    SvlErrorType ierr = 0;
    SvlBufferSettings iset = {
	.Size = totalSize,
	.Usage = VK_BUFFER_USAGE_INDEX_BUFFER_BIT
    };
    SvlBuffer ibuf = svl_buffer_create(pInstance, iset, &ierr);
    if (ierr != 0)
    {
	pInstance->Error = ierr;
	return;
    }

    pPipe->Index = ibuf;
    pPipe->IndicesCount = 0;
}

SvlUniform*
svl_pipeline_get_uniform(SvlPipeline* pPipe, const char* pUniformName)
{
    SvlUniform* aUniforms = pPipe->Binding.aUniforms;

    for (svl_i32 i = 0; i < svl_array_count(aUniforms); ++i)
    {
	SvlUniform* pUniform = &aUniforms[i];
	if (_svl_string_compare(pUniform->pName, pUniformName))
	    return pUniform;
    }

    return NULL;
}

/*#######################
  DOCS(typedef): Svl Public Api
  #######################*/

void
svl_set_clear_color(SvlRenderPass* pRenderPass, svl_f32 r, svl_f32 g, svl_f32 b, svl_f32 a)
{
    VkClearColorValue clearColor = { r, g, b, a };

    pRenderPass->ClearColor = clearColor;
}

void
svl_set_viewport(SvlPipeline* pPipeline, svl_v2i from, svl_v2i size)
{
    VkViewport viewport = {
	.x = (svl_f32) from.X,
	.y = (svl_f32) from.Y,
	.width = (svl_f32) size.X,
	.height = (svl_f32) size.Y,
	.minDepth = 0,
	.maxDepth = 1
    };

    pPipeline->Viewport = viewport;
}

void
svl_set_scissor(SvlPipeline* pPipeline, svl_v2i from, svl_v2i to)
{
    VkRect2D scissor = {
	.offset = {
	    .x = from.X,
	    .y = from.Y,
	},
	.extent = {
	    .width  = (svl_u32) to.X,
	    .height = (svl_u32) to.Y,
	}
    };

    pPipeline->Scissor = scissor;
}

SvlSwapImageView*
_svl_swapchain_image_views_create(SvlInstance* pInstance, VkFormat vkFormat)
{
    VkDevice device = pInstance->Device;
    VkSwapchainKHR swapchain = pInstance->Swapchain;

    VkImage* aImages = NULL;

    svl_u32 imagesCount;
    VkResult getSwapChainImagesResult = vkGetSwapchainImagesKHR(device, swapchain, &imagesCount, NULL);
    if (svl_result_check(getSwapChainImagesResult, "Can't get swap chain images!"))
    {
	pInstance->Error = SvlErrorType_SwapImageView;
	return NULL;
    }

    svl_array_reserve(aImages, imagesCount);
    svl_array_header(aImages)->Count = imagesCount;

    getSwapChainImagesResult = vkGetSwapchainImagesKHR(device, swapchain, &imagesCount, aImages);
    if (svl_result_check(getSwapChainImagesResult, "Can't get swap chain images!"))
    {
	pInstance->Error = SvlErrorType_SwapImageView;
	return NULL;
    }

    svl_guard((imagesCount > 0) && "Images Count should be > 0 !!!");
    //svl_info("IMAGES COUNT: %d\n", imagesCount);

    SvlSwapImageView* aSwapImageViews = NULL;

    for (svl_i64 i = 0; i < imagesCount; ++i)
    {
	VkImageView vkImageView;
	_svl_image_view_create(pInstance,
			       aImages[i],
			       vkFormat,
			       VK_IMAGE_ASPECT_COLOR_BIT,
			       1,
			       &vkImageView);
	if (pInstance->Error != SvlErrorType_None)
	{
#if defined(SVL_DEBUG)
	    svl_guard((imagesCount > 0) && "Images Count should be > 0 !!!");
#endif
	    svl_warn("Can't create image view for swapchain\n");
	    return NULL;
	}

	SvlSwapImageView vsaImageView = {
	    .Image = aImages[i],
	    .View = vkImageView,
	};

	svl_array_push(aSwapImageViews, vsaImageView);
    }

    return aSwapImageViews;
}

svl_i32
svl_find_memory_type_index(SvlInstance* pInstance, svl_u32 typeBit, VkMemoryPropertyFlags vkMemoryPropertyFlags)
{
    VkPhysicalDeviceMemoryProperties dMemoryProperties;
    vkGetPhysicalDeviceMemoryProperties(pInstance->PhysicalDevice, &dMemoryProperties);

    for (svl_i32 i = 0; i < dMemoryProperties.memoryTypeCount; ++i)
    {
	VkMemoryType memoryType =
	    dMemoryProperties.memoryTypes[i];
	if ((typeBit & (1 << i)) && (memoryType.propertyFlags & vkMemoryPropertyFlags))
	{
	    return i;
	}
    }

    svl_guard(0 && "Can't find memory type index!");
    return -1;
}


void
_svl_image_create(SvlInstance* pInstance, SvlImageSettings settings, VkImage* pVkImage, VkDeviceMemory* pDeviceMemory, SvlErrorType* pError)
{
    VkDevice vkDevice = pInstance->Device;
    VkAllocationCallbacks* pAllocator = pInstance->pAllocator;
    VkImageCreateInfo vkImageCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
	.pNext = NULL,
	.flags = 0,
	.imageType = VK_IMAGE_TYPE_2D,
	.format = settings.Format,
	.extent = {
	    .width = settings.Width,
	    .height = settings.Height,
	    .depth = settings.Depth
	},
	.mipLevels = settings.MipLevels,
	.arrayLayers = 1,
	.samples = settings.SamplesCount,
	.tiling = settings.ImageTiling,
	.usage = settings.ImageUsageFlags,
	.sharingMode = VK_SHARING_MODE_EXCLUSIVE,
	.queueFamilyIndexCount = 0, // NOTE(): should we set this value
	.pQueueFamilyIndices = NULL,
	.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED
    };

    VkResult vkCreateImageResult = vkCreateImage(
	vkDevice, &vkImageCreateInfo, pAllocator, pVkImage);
    if (svl_result_check(vkCreateImageResult, "Failed image/texture creation!"))
    {
	*pError = SvlErrorType_Image;
    }

    VkImage tempVkImage = *pVkImage;
    VkMemoryRequirements memRequirements;
    vkGetImageMemoryRequirements(vkDevice, tempVkImage, &memRequirements);

    svl_i32 index = svl_find_memory_type_index(
	pInstance,
	memRequirements.memoryTypeBits,
	settings.MemoryPropertyFlags);

    VkMemoryAllocateInfo allocateInfo = {
	.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
	.pNext = NULL,
	.allocationSize = memRequirements.size,
	.memoryTypeIndex = index
    };

    VkResult vkAllocateMemoryResult =
	vkAllocateMemory(vkDevice, &allocateInfo, pAllocator, pDeviceMemory);
    if (svl_result_check(vkAllocateMemoryResult, "Failed texture memory allocation!"))
    {
	*pError = SvlErrorType_Image;
    }

    VkResult vkBindImageMemoryResult =
	vkBindImageMemory(vkDevice, tempVkImage, *pDeviceMemory, 0);
    if (svl_result_check(vkBindImageMemoryResult, "Failed bind texture memory!"))
    {
	*pError = SvlErrorType_Image;
    }
}

void
_svl_image_view_create(SvlInstance* pInstance, VkImage vkImage, VkFormat vkFormat, VkImageAspectFlags vkImageAspectFlags, svl_i32 mipLevels, VkImageView* pImageView)
{
    VkImageView vkImageView;

    VkImageViewCreateInfo imageViewCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
	.pNext = NULL,
	.flags = 0,
	.image = vkImage,
	.viewType = VK_IMAGE_VIEW_TYPE_2D,
	.format = vkFormat,
	.components = {
	    .r = VK_COMPONENT_SWIZZLE_IDENTITY,
	    .g = VK_COMPONENT_SWIZZLE_IDENTITY,
	    .b = VK_COMPONENT_SWIZZLE_IDENTITY,
	    .a = VK_COMPONENT_SWIZZLE_IDENTITY
	},
	.subresourceRange = {
	    .aspectMask = vkImageAspectFlags, //VK_IMAGE_ASPECT_COLOR_BIT,
	    .baseMipLevel = 0,
	    .levelCount = mipLevels,
	    .baseArrayLayer = 0,
	    .layerCount = 1
	}
    };

    VkResult createImageViewResult =
	vkCreateImageView(pInstance->Device, &imageViewCreateInfo, pInstance->pAllocator, &vkImageView);
    if (svl_result_check(createImageViewResult, "Can't create vk image view!"))
    {
	pInstance->Error = SvlErrorType_ImageView;
	pImageView = NULL;
    }
    else
    {
	*pImageView = vkImageView;
    }
}

SvlImageView
svl_image_view_create(SvlInstance* pInstance, SvlImageViewSettings settings)
{
    VkImage vkImage;
    VkDeviceMemory vkDeviceMemory;
    SvlErrorType error = 0;

    _svl_image_create(pInstance, settings.ImageSettings, &vkImage, &vkDeviceMemory, &error);
    if (error != SvlErrorType_None)
    {
	pInstance->Error = error;
	return (SvlImageView) {};
    }

    VkImageView vkImageView;

    _svl_image_view_create(pInstance, vkImage,
			   settings.Format,
			   settings.ImageAspectFlags,
			   settings.MipLevels,
			   &vkImageView);
    if (error != SvlErrorType_None)
	return (SvlImageView) {};

    SvlImageView imageView = {
	.Image = vkImage,
	.View = vkImageView,
	.Memory = vkDeviceMemory
    };

    return imageView;
}

void
svl_image_create_mipmaps(SvlInstance* pInstance, VkImage vkImage, svl_i32 width, svl_i32 height, svl_i32 mipLevels)
{
    VkFormatProperties formatProperties;
    vkGetPhysicalDeviceFormatProperties(pInstance->PhysicalDevice, VK_FORMAT_R8G8B8A8_SRGB, &formatProperties);
    svl_assert(
	formatProperties.optimalTilingFeatures
	& VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_LINEAR_BIT
	&& "Linear filtering not supported!");

    VkCommandBuffer cmdBuffer = svl_cmd_begin(pInstance);

    VkImageMemoryBarrier vkImageMemoryBarrier = {
	.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
	.pNext = NULL,
	.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
	.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
	.image = vkImage,
	.subresourceRange = {
	    .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
	    .levelCount = 1,
	    .baseArrayLayer = 0,
	    .layerCount = 1,
	}
    };

    svl_i32 mipWidth = width;
    svl_i32 mipHeight = height;
    for (svl_i32 i = 1; i < mipLevels; ++i)
    {
	svl_i32 dstWidth, dstHeight;
	if (mipWidth > 1)
	{
	    dstWidth = mipWidth / 2;
	}
	else
	{
	    dstWidth = 1;
	}

	if (mipHeight > 1)
	{
	    dstHeight = mipHeight / 2;
	}
	else
	{
	    dstHeight = 1;
	}

	vkImageMemoryBarrier.subresourceRange.baseMipLevel = i - 1;
	vkImageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
	vkImageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
	vkImageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
	vkImageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;

	vkCmdPipelineBarrier(
	    cmdBuffer,
	    VK_PIPELINE_STAGE_TRANSFER_BIT,
	    VK_PIPELINE_STAGE_TRANSFER_BIT,
	    0,
	    0, NULL,
	    0, NULL,
	    1, &vkImageMemoryBarrier);

	VkImageBlit vkImageBlit = {
	    .srcSubresource = {
		.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
		.mipLevel = i - 1,
		.baseArrayLayer = 0,
		.layerCount = 1
	    },
	    .srcOffsets = {
		[0] = { 0, 0, 0 },
		[1] = { mipWidth, mipHeight, 1 },
	    },
	    .dstSubresource = {
		.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
		.mipLevel = i,
		.baseArrayLayer = 0,
		.layerCount = 1
	    },
	    .dstOffsets = {
		[0] = { 0, 0, 0 },
		[1] = { dstWidth, dstHeight, 1 }
	    }
	};

	vkCmdBlitImage(
	    cmdBuffer,
	    vkImage, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
	    vkImage, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
	    1, &vkImageBlit,
	    VK_FILTER_LINEAR);

	vkImageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
	vkImageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
	vkImageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
	vkImageMemoryBarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

	vkCmdPipelineBarrier(
	    cmdBuffer,
	    VK_PIPELINE_STAGE_TRANSFER_BIT,
	    VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
	    0,
	    0, NULL,
	    0, NULL,
	    1, &vkImageMemoryBarrier);

	if (dstWidth > 1)
	    mipWidth = dstWidth;
	if (dstHeight > 1)
	    mipHeight = dstHeight;
    }

    vkImageMemoryBarrier.subresourceRange.baseMipLevel = mipLevels - 1;
    vkImageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    vkImageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    vkImageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    vkImageMemoryBarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
    vkCmdPipelineBarrier(
	cmdBuffer,
	VK_PIPELINE_STAGE_TRANSFER_BIT,
	VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
	0,
	0, NULL,
	0, NULL,
	1, &vkImageMemoryBarrier);

    svl_cmd_end(pInstance, cmdBuffer);
}

VkSampler
svl_sampler_create(SvlInstance* pInstance, SvlTextureFlags flags, svl_i32 mipLevels)
{
    VkSampler vkSampler;

    VkFilter magFilter;
    VkFilter minFilter;

#define BitAnd(flag, bit) (((flag) & (bit)))

    if (BitAnd(flags, SvlTextureFlags_Linear))
    {
	magFilter = VK_FILTER_LINEAR;
	minFilter = VK_FILTER_LINEAR;
    }
    else
    {
	magFilter = VK_FILTER_NEAREST;
	minFilter = VK_FILTER_NEAREST;
    }

    VkBool32 isAnisotropyFilterEnabled;
    if (BitAnd(flags, SvlTextureFlags_Anisotropy))
	isAnisotropyFilterEnabled = VK_TRUE;
    else
	isAnisotropyFilterEnabled = VK_FALSE;

    VkSamplerAddressMode addressMode;
    if (flags & SvlTextureFlags_Repeat)
    {
	addressMode = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    }
    else if (flags & SvlTextureFlags_ClampToEdge)
    {
	addressMode = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
    }
    else if (flags & SvlTextureFlags_ClampToBorder)
    {
	addressMode = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
    }

    VkPhysicalDeviceProperties properties;
    vkGetPhysicalDeviceProperties(pInstance->PhysicalDevice, &properties);
    svl_f32 maxAnisotropy = properties.limits.maxSamplerAnisotropy;

    VkBool32 compareEnabled = VK_FALSE; // NOTE(typedef): used for PCF in shadow map technique
    VkCompareOp compareOp = VK_COMPARE_OP_ALWAYS; // NOTE(typedef): used for PCF in shadow map technique

    VkSamplerCreateInfo vkSamplerCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
	.pNext = NULL,
	.flags = 0,
	.magFilter = magFilter,
	.minFilter = minFilter,
	.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR,
	.addressModeU = addressMode,
	.addressModeV = addressMode,
	.addressModeW = addressMode,
	.mipLodBias = 0.0f,
	.anisotropyEnable = isAnisotropyFilterEnabled,
	.maxAnisotropy = maxAnisotropy,
	.compareEnable = compareEnabled,
	.compareOp = compareOp,
	.minLod = 0.0f, //NOTE(typedef): Multisample trick (f32) (mipLevels)/8,
	.maxLod = (svl_f32) mipLevels,
	.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK,
	.unnormalizedCoordinates = VK_FALSE
    };

    VkResult vkCreateSamplerResult = vkCreateSampler(pInstance->Device, &vkSamplerCreateInfo, pInstance->pAllocator, &vkSampler);

#undef BitAnd

    return vkSampler;
}

void
svl_image_change_layout(SvlInstance* pInstance, VkImage vkImage, VkImageLayout vkOldLayout, VkImageLayout vkNewLayout, VkFormat vkFormat, svl_i32 mipLevels)
{
    VkCommandBuffer vkCommandBuffer = svl_cmd_begin(pInstance);

    VkImageMemoryBarrier vkImageMemoryBarrier = {
	.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
	.pNext = NULL,
	.srcAccessMask = 0, // TODO(typedef): empty for now
	.dstAccessMask = 0, // TODO(typedef): empty for now
	.oldLayout = vkOldLayout,
	.newLayout = vkNewLayout,
	.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
	.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
	.image = vkImage,
	.subresourceRange = {
	    .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
	    .baseMipLevel = 0,
	    .levelCount = mipLevels,
	    .baseArrayLayer = 0,
	    .layerCount = 1,
	}
    };

    VkPipelineStageFlags vkSrcPipelineStageFlags;
    VkPipelineStageFlags vkDstPipelineStageFlags;
    if (vkOldLayout == VK_IMAGE_LAYOUT_UNDEFINED
	&& vkNewLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
    {
	vkImageMemoryBarrier.srcAccessMask = 0;
	vkImageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
	vkSrcPipelineStageFlags = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
	vkDstPipelineStageFlags = VK_PIPELINE_STAGE_TRANSFER_BIT;
    }
    else if (vkOldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
	     && vkNewLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
    {
	vkImageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
	vkImageMemoryBarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
	vkSrcPipelineStageFlags = VK_PIPELINE_STAGE_TRANSFER_BIT;
	vkDstPipelineStageFlags = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
    }
    else
    {
	svl_assert(0 && "Wrong new||old layout picked !!!");
    }

    vkCmdPipelineBarrier(vkCommandBuffer,
			 vkSrcPipelineStageFlags,
			 vkDstPipelineStageFlags,
			 VK_DEPENDENCY_BY_REGION_BIT,
			 0,NULL, 0,NULL,
			 1,
			 &vkImageMemoryBarrier
	);

    svl_cmd_end(pInstance, vkCommandBuffer);
}

void
svl_buffer_to_image(SvlInstance* pInstance, VkBuffer vkBuffer, VkImage vkImage, svl_i32 width, svl_i32 height)
{
    VkCommandBuffer vkCommandBuffer = svl_cmd_begin(pInstance);

    VkBufferImageCopy vkBufferImageCopy = {
	.bufferOffset = 0,
	.bufferRowLength = 0,
	.bufferImageHeight = 0,
	.imageSubresource = {
	    .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
	    .mipLevel = 0,
	    .baseArrayLayer = 0,
	    .layerCount = 1
	},
	.imageOffset = {
	    .x = 0,
	    .y = 0,
	    .z = 0
	},
	.imageExtent = {
	    .width = width,
	    .height = height,
	    .depth = 1
	}
    };

    vkCmdCopyBufferToImage(vkCommandBuffer, vkBuffer, vkImage, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &vkBufferImageCopy);

    svl_cmd_end(pInstance, vkCommandBuffer);
}

// DOCS: Texture: For User-Space Code
SvlTexture
svl_texture_create(SvlInstance* pInstance, SvlTextureSettings set)
{
    svl_assert(pInstance != NULL);
    svl_assert(set.Width > 0);
    svl_assert(set.Height > 0);

    const VkFormat vkFormat = VK_FORMAT_R8G8B8A8_SRGB;
    const svl_i32 channels = 4;

    set.IsMipMaped = 1;

    svl_i32 mipLevels = 1;
    if (set.IsMipMaped)
    { // note: find mipLevels
#define SvlMin(a, b) ({(a > b) ? b : a;})
	svl_f32 logValue = log2f(SvlMin(set.Width, set.Height));
	mipLevels = SvlMin(((svl_i32)(logValue)) + 1, 10);
#undef SvlMin
    }

    SvlTexture texture = {
	.MipLevels = mipLevels,
    };

    VkImage vkImage;
    VkDeviceMemory vkDeviceMemory;
    SvlErrorType error = 0;

    SvlImageSettings imageSet = {
	.Width = set.Width,
	.Height = set.Height,
	.Depth = 1,
	.MipLevels = mipLevels,
	.SamplesCount = VK_SAMPLE_COUNT_1_BIT,
	.Format = vkFormat,
	.ImageTiling = VK_IMAGE_TILING_OPTIMAL,
	.ImageUsageFlags = VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
	.MemoryPropertyFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
    };
    _svl_image_create(pInstance, imageSet, &vkImage, &vkDeviceMemory, &error);

    if (error != SvlErrorType_None)
    {
	pInstance->Error = error;
	return (SvlTexture) {};
    }

    { // DOCS: Push data into gpu memory + buffer creation

	/*
	  NOTE(typedef): Make this thing faster with *_lite() function wo cmd buffers
	*/
	size_t imageSize = set.Width * set.Height * channels;
	VkDeviceMemory stagingBufferMemory;

	VkBuffer stagingBuffer =_svl_buffer_create(pInstance,
			   VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
			   imageSize,
			   (VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT),
			   &stagingBufferMemory);

	// DOCS: Set memory
	void* pBufferData;
	vkMapMemory(pInstance->Device, stagingBufferMemory, 0, imageSize, 0, &pBufferData);
	memcpy(pBufferData, set.pData, imageSize);
	vkUnmapMemory(pInstance->Device, stagingBufferMemory);

	svl_image_change_layout(pInstance, vkImage, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, vkFormat, mipLevels);
	svl_buffer_to_image(pInstance, stagingBuffer, vkImage, set.Width, set.Height);

	if (texture.MipLevels == 1)
	{
	    //DOCS(typedef): imageBlit cmd will transfer image to SHADER format
	    // NOTE(typedef): This is for using image in shader
	    svl_image_change_layout(pInstance, vkImage,
				    VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
				    VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
				    vkFormat,
				    1);
	}
	else
	{
	    svl_image_create_mipmaps(pInstance, vkImage, set.Width, set.Height, mipLevels);
	}

	vkDestroyBuffer(pInstance->Device, stagingBuffer, pInstance->pAllocator);
	vkFreeMemory(pInstance->Device, stagingBufferMemory, pInstance->pAllocator);
    }

    VkImageView vkImageView;

    _svl_image_view_create(pInstance,
			   vkImage,
			   vkFormat,
			   VK_IMAGE_ASPECT_COLOR_BIT,
			   mipLevels,
			   &vkImageView);
    if (error != SvlErrorType_None)
    {
	return (SvlTexture) {};
    }

    texture.Image = vkImage;
    texture.View = vkImageView;
    texture.Memory = vkDeviceMemory;
    texture.Sampler = svl_sampler_create(pInstance, set.Flags, mipLevels);

    return texture;
}

void
svl_texture_destroy(SvlInstance* pInstance, SvlTexture* pTexture)
{
    vkDestroyImage(pInstance->Device, pTexture->Image, pInstance->pAllocator);
    vkFreeMemory(pInstance->Device, pTexture->Memory, pInstance->pAllocator);
    vkDestroyImageView(pInstance->Device, pTexture->View, pInstance->pAllocator);
    if (pTexture->MipLevels > 0)
    {
	vkDestroySampler(pInstance->Device, pTexture->Sampler, pInstance->pAllocator);
    }
}

void
_svl_render_pass_add_framebuffer_new(SvlInstance* pInstance, SvlRenderPass* pRenderPass, VkImageView* aAttachments, svl_i32 attachmentsCount)
{
    VkFramebufferCreateInfo frabufferCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
	.renderPass = pRenderPass->Handle,
	.attachmentCount = attachmentsCount,
	.pAttachments = aAttachments,
	.width = pInstance->Resolution.width,
	.height = pInstance->Resolution.height,
	.layers = 1
    };

    VkFramebuffer vkFramebuffer;
    VkResult createFramebufferResult =
	vkCreateFramebuffer(pInstance->Device, &frabufferCreateInfo, pInstance->pAllocator, &vkFramebuffer);
    if (svl_result_check(createFramebufferResult, "Can't create framebuffer!"))
    {
	pInstance->Error = SvlErrorType_Framebuffer_Create;
	return;
    }

    svl_array_push(pRenderPass->aFramebuffers, vkFramebuffer);
}


void
_svl_render_pass_add_framebuffer(SvlInstance* pInstance, SvlRenderPass* pRenderPass, VkImageView* aAttachments)
{
    VkFramebufferCreateInfo frabufferCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
	.renderPass = pRenderPass->Handle,
	.attachmentCount = svl_array_count(aAttachments),
	.pAttachments = aAttachments,
	.width = pInstance->Resolution.width,
	.height = pInstance->Resolution.height,
	.layers = 1
    };

    VkFramebuffer vkFramebuffer;
    VkResult createFramebufferResult =
	vkCreateFramebuffer(pInstance->Device, &frabufferCreateInfo, pInstance->pAllocator, &vkFramebuffer);
    if (svl_result_check(createFramebufferResult, "Can't create framebuffer!"))
    {
	pInstance->Error = SvlErrorType_Framebuffer_Create;
	return;
    }

    svl_array_push(pRenderPass->aFramebuffers, vkFramebuffer);
}

// todo: free aImageViews, be sure we clean everything
void
svl_render_pass_clear_framebuffer(SvlInstance* pInstance, SvlRenderPass* pRenderPass)
{
    for (svl_i32 i = 0; i < svl_array_count(pRenderPass->aFramebuffers); ++i)
    {
	VkFramebuffer framebuffer = pRenderPass->aFramebuffers[i];
	vkDestroyFramebuffer(pInstance->Device, framebuffer, pInstance->pAllocator);
    }

    for (svl_i32 i = 0; i < svl_array_count(pRenderPass->aImageViews); ++i)
    {
	SvlImageView imageView = pRenderPass->aImageViews[i];
	vkDestroyImageView(pInstance->Device, imageView.View, pInstance->pAllocator);
	//vsa_texture_image_destroy(item.Image, item.View, item.Memory);
    }

    svl_array_free(pRenderPass->aImageViews);
    svl_array_free(pRenderPass->aFramebuffers);
}

/*
  BUG:
  Вместо данной функции следует создать функция обновления фраймбуферов для текущей
  Swapchain'ы.
  todo: check for clean code
*/
void
svl_render_pass_create_framebuffers(SvlInstance* pInstance, SvlRenderPass* pRenderPass)
{
    //
    svl_i32 shouldCleanImageViews = (pRenderPass->aFramebuffers || pRenderPass->aImageViews != NULL);
    if (shouldCleanImageViews)
    {
#if defined(SVL_DEBUG)
	vguard(0 && "Yout fogot to call svl_render_pass_destroy_framebuffers()!");
#endif // SVL_DEBUG
	svl_warn("Creating framebuffer's while they already exists in render pass\n");
	return;
    }

    SvlImageView* aImageViews = NULL;
    SvlSwapImageView* aSwapImageViews = NULL;

    svl_i32 attachCount = svl_array_count(pRenderPass->aAttachments);
    for (svl_i32 i = 0; i < attachCount; ++i)
    {
	SvlAttachment attach = pRenderPass->aAttachments[i];

	switch (attach.Type)
	{

	case SvlAttachmentType_Default:
	{
	    // NOTE: Mb this cause bug
	    SvlImageViewSettings settings = {
		.ImageSettings = {
		    .Width = pInstance->Resolution.width,
		    .Height = pInstance->Resolution.height,
		    .Depth = 1.0f,
		    .MipLevels = 1,
		    .SamplesCount = attach.Description.samples,
		    .Format = attach.Description.format,
		    .ImageTiling = VK_IMAGE_TILING_OPTIMAL,
		    .ImageUsageFlags = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
		    .MemoryPropertyFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
		},
		.Format = attach.Description.format,
		.ImageAspectFlags = VK_IMAGE_ASPECT_COLOR_BIT,
		.MipLevels = 1,
	    };

	    // todo: refactor
	    SvlImageView imageView = svl_image_view_create(pInstance, settings);
	    if (pInstance->Error != SvlErrorType_None)
	    {
		svl_guard(0);
	    }

	    svl_array_push(aImageViews, imageView);

	    break;
	}

	case SvlAttachmentType_ResolveMultisample:
	{
	    SvlImageViewSettings settings = {
		.ImageSettings = {
		    .Width = pInstance->Resolution.width,
		    .Height = pInstance->Resolution.height,
		    .Depth = 1.0f,
		    .MipLevels = 1,
		    .SamplesCount = attach.Description.samples,
		    .Format = attach.Description.format,
		    .ImageTiling = VK_IMAGE_TILING_OPTIMAL,
		    .ImageUsageFlags = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
		    .MemoryPropertyFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
		},
		.Format = attach.Description.format,
		.ImageAspectFlags = VK_IMAGE_ASPECT_COLOR_BIT,
		.MipLevels = 1,
	    };

	    SvlImageView imageView = svl_image_view_create(pInstance, settings);
	    if (pInstance->Error != SvlErrorType_None)
	    {
		svl_guard(0);
	    }

	    svl_array_push(aImageViews, imageView);
	    break;
	}

	case SvlAttachmentType_Depth:
	{
	    // TODO: Write find_format for depth instead of using concrete type
	    VkFormat depthAttachmentFormat = VK_FORMAT_D32_SFLOAT;

	    SvlImageViewSettings settings = {
		.ImageSettings = {
		    .Width = pInstance->Resolution.width,
		    .Height = pInstance->Resolution.height,
		    .Depth = 1.0f,
		    .MipLevels = 1,
		    .SamplesCount = attach.Description.samples,
		    .Format = depthAttachmentFormat,
		    .ImageTiling = VK_IMAGE_TILING_OPTIMAL,
		    .ImageUsageFlags = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
		    .MemoryPropertyFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
		},
		.Format = depthAttachmentFormat,
		.ImageAspectFlags = VK_IMAGE_ASPECT_DEPTH_BIT,
		.MipLevels = 1,
	    };

	    SvlErrorType error = 0;
	    SvlImageView imageView = svl_image_view_create(pInstance, settings);
	    if (pInstance->Error != SvlErrorType_None)
	    {
		svl_guard(0);
	    }

	    svl_array_push(aImageViews, imageView);

	    break;
	}

	case SvlAttachmentType_SwapChained:
	{
	    VkFormat vkFormat = attach.Description.format;
	    //aSwapImageViews = _svl_swapchain_image_views_create(pInstance, vkFormat);

	    break;
	}

	}
    }

    pRenderPass->aImageViews = aImageViews;
    //pRenderPass->aSwapImageViews = aSwapImageViews;
    pRenderPass->ImageViewsCount = svl_array_count(aImageViews);
    // pRenderPass->SwapImageViewsCount = svl_array_count(aSwapImageViews);

    /*
      todo DOCS:
      Framebuffer count depends on SwapImageViewsCount
     */

    if (/*DOCS: For now it's always true*/
	1
	||
	pInstance->aSwapImageViews)
    {
	svl_i32 framebuffersCount = svl_array_count(pInstance->aSwapImageViews);
	for (svl_i32 f = 0; f < framebuffersCount; ++f)
	{
	    SvlSwapImageView swapImageView = pInstance->aSwapImageViews[f];

	    VkImageView* aAttachments = NULL;
	    {
		svl_i64 i, count = svl_array_count(pRenderPass->aImageViews);
		for (i = 0; i < count; ++i)
		{
		    SvlImageView imageView = pRenderPass->aImageViews[i];
		    svl_array_push(aAttachments, imageView.View);
		}

		svl_array_push(aAttachments, swapImageView.View);

	    }

	    _svl_render_pass_add_framebuffer(pInstance, pRenderPass, aAttachments);

	    svl_array_free(aAttachments);
	}

	return;
    }

    VkImageView* aAttachments = NULL;
    svl_i64 i, count = svl_array_count(pRenderPass->aImageViews);
    for (i = 0; i < count; ++i)
    {
	SvlImageView imageView = pRenderPass->aImageViews[i];
	svl_array_push(aAttachments, imageView.View);
    }

    _svl_render_pass_add_framebuffer(pInstance, pRenderPass, aAttachments);


}

void
svl_render_pass_destroy_framebuffers(SvlInstance* pInstance, SvlRenderPass* pRenderPass)
{
    svl_i32 shouldCleanImageViews = (pRenderPass->aFramebuffers != NULL || pRenderPass->aImageViews != NULL);
    if (!shouldCleanImageViews)
	return;

    for (svl_i32 i = 0; i < svl_array_count(pRenderPass->aFramebuffers); ++i)
    {
	VkFramebuffer framebuffer = pRenderPass->aFramebuffers[i];
	vkDestroyFramebuffer(pInstance->Device, framebuffer, pInstance->pAllocator);
    }

    for (svl_i32 i = 0; i < svl_array_count(pRenderPass->aImageViews); ++i)
    {
	SvlImageView imageView = pRenderPass->aImageViews[i];
	vkDestroyImageView(pInstance->Device, imageView.View, pInstance->pAllocator);
	vkDestroyImage(pInstance->Device, imageView.Image, pInstance->pAllocator);
	vkFreeMemory(pInstance->Device, imageView.Memory, pInstance->pAllocator);
    }

    svl_array_free(pRenderPass->aImageViews);
    svl_array_free(pRenderPass->aFramebuffers);
}

void
svl_render_pass_update_images_framebuffers(SvlInstance* pInstance, SvlRenderPass* pRenderPass)
{
    VkAllocationCallbacks* pAllocator = pInstance->pAllocator;
    VkExtent2D extent = pInstance->Resolution;

    SvlImageView* aImageViews = NULL;
    SvlSwapImageView* aSwapImageViews = NULL;

    svl_i32 isSwapChainFirst = 0;

    svl_i32 attachCount = svl_array_count(pRenderPass->aAttachments);
    for (svl_i32 i = 0; i < attachCount; ++i)
    {
	SvlAttachment attach = pRenderPass->aAttachments[i];

	switch (attach.Type)
	{

	case SvlAttachmentType_Default:
	{
	    // NOTE: Mb this cause bug
	    SvlImageViewSettings settings = {
		.ImageSettings = {
		    .Width = extent.width,
		    .Height = extent.height,
		    .Depth = 1.0f,
		    .MipLevels = 1,
		    .SamplesCount = attach.Description.samples,
		    .Format = attach.Description.format,
		    .ImageTiling = VK_IMAGE_TILING_OPTIMAL,
		    .ImageUsageFlags = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
		    .MemoryPropertyFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
		},
		.Format = attach.Description.format,
		.ImageAspectFlags = VK_IMAGE_ASPECT_COLOR_BIT,
		.MipLevels = 1,
	    };

	    // todo: refactor
	    SvlImageView imageView = svl_image_view_create(pInstance, settings);
	    if (pInstance->Error != SvlErrorType_None)
	    {
		svl_guard(0);
	    }

	    svl_array_push(aImageViews, imageView);

	    break;
	}

	case SvlAttachmentType_ResolveMultisample:
	{
	    SvlImageViewSettings settings = {
		.ImageSettings = {
		    .Width = extent.width,
		    .Height = extent.height,
		    .Depth = 1.0f,
		    .MipLevels = 1,
		    .SamplesCount = attach.Description.samples,
		    .Format = attach.Description.format,
		    .ImageTiling = VK_IMAGE_TILING_OPTIMAL,
		    .ImageUsageFlags = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
		    .MemoryPropertyFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
		},
		.Format = attach.Description.format,
		.ImageAspectFlags = VK_IMAGE_ASPECT_COLOR_BIT,
		.MipLevels = 1,
	    };

	    SvlImageView imageView = svl_image_view_create(pInstance, settings);
	    if (pInstance->Error != SvlErrorType_None)
	    {
		svl_guard(0);
	    }

	    svl_array_push(aImageViews, imageView);
	    break;
	}

	case SvlAttachmentType_Depth:
	{
	    // TODO: Write find_format for depth instead of using concrete type
	    VkFormat depthAttachmentFormat = VK_FORMAT_D32_SFLOAT;

	    SvlImageViewSettings settings = {
		.ImageSettings = {
		    .Width = extent.width,
		    .Height = extent.height,
		    .Depth = 1.0f,
		    .MipLevels = 1,
		    .SamplesCount = attach.Description.samples,
		    .Format = depthAttachmentFormat,
		    .ImageTiling = VK_IMAGE_TILING_OPTIMAL,
		    .ImageUsageFlags = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
		    .MemoryPropertyFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
		},
		.Format = depthAttachmentFormat,
		.ImageAspectFlags = VK_IMAGE_ASPECT_DEPTH_BIT,
		.MipLevels = 1,
	    };

	    SvlErrorType error = 0;
	    SvlImageView imageView = svl_image_view_create(pInstance, settings);
	    if (pInstance->Error != SvlErrorType_None)
	    {
		svl_guard(0);
	    }

	    svl_array_push(aImageViews, imageView);

	    break;
	}

	case SvlAttachmentType_SwapChained:
	{
	    if (i == 0)
		isSwapChainFirst = 1;

	    // VkFormat vkFormat = attach.Description.format;
	    // aSwapImageViews = _svl_swapchain_image_views_create(pInstance, vkFormat);

	    break;
	}

	}
    }

    pRenderPass->aImageViews = aImageViews;
    //pRenderPass->aSwapImageViews = aSwapImageViews;
    pRenderPass->ImageViewsCount = svl_array_count(aImageViews);
    // pRenderPass->SwapImageViewsCount = svl_array_count(aSwapImageViews);

    // note: Framebuffer count depends on SwapImageViewsCount
    svl_i32 framebuffersCount = svl_array_count(pInstance->aSwapImageViews);

    VkImageView* aAttachments;

    for (svl_i32 f = 0; f < framebuffersCount; ++f)
    {
	aAttachments = NULL;

	SvlSwapImageView swapImageView = pInstance->aSwapImageViews[f];

	if (isSwapChainFirst)
	    svl_array_push(aAttachments, swapImageView.View);

	svl_i32 count = svl_array_count(pRenderPass->aImageViews);
	svl_info("aImageView count: %d\n", count);
	for (svl_i32 i = 0; i < count; ++i)
	{
	    SvlImageView imageView = pRenderPass->aImageViews[i];
	    svl_array_push(aAttachments, imageView.View);
	}

	if (!isSwapChainFirst)
	    svl_array_push(aAttachments, swapImageView.View);

	_svl_render_pass_add_framebuffer(pInstance, pRenderPass, aAttachments);

	svl_array_free(aAttachments);
    }

}

typedef struct SvlRenderPassAttachments
{
    VkAttachmentDescription* aAllDescrs;
    VkAttachmentReference* aColorRefs;
    VkAttachmentReference* pResolvedRef;
    VkAttachmentReference* pDepthRef;
    SvlAttachment* aAttachments;
    VkSampleCountFlagBits Samples;
} SvlRenderPassAttachments;

SvlRenderPassAttachments
_svl_render_pass_create_attachment(SvlInstance* pInstance, SvlRenderPassSettings settings)
{
    VkAttachmentDescription* allDescrs = NULL;
    VkAttachmentReference* aColorRefs = NULL;
    VkAttachmentReference* pResolvedRef = NULL;
    VkAttachmentReference* pDepthRef = NULL;

    SvlAttachment* aAttachments = NULL;
    VkSampleCountFlagBits samples = VK_SAMPLE_COUNT_1_BIT;

    for (svl_i64 i = 0; i < settings.AttachmentsCount; ++i)
    {
	settings.aAttachments[i].Reference.attachment = i;
	SvlAttachment attach = settings.aAttachments[i];
	svl_array_push(aAttachments, attach);

	if (settings.aAttachments[i].Description.samples > samples)
	    samples = settings.aAttachments[i].Description.samples;

	switch (attach.Type)
	{

	case SvlAttachmentType_Default:
	    svl_array_push(aColorRefs, settings.aAttachments[i].Reference);
	    break;

	case SvlAttachmentType_Depth:
	    svl_guard(pDepthRef == NULL);
	    pDepthRef = &settings.aAttachments[i].Reference;
	    break;

	case SvlAttachmentType_ResolveMultisample:
	    svl_guard(pResolvedRef == NULL);
	    pResolvedRef = &settings.aAttachments[i].Reference;
	    break;

	case SvlAttachmentType_SwapChained:
	    // todo: bug?
	    svl_guard(pResolvedRef == NULL);
	    pResolvedRef = &settings.aAttachments[i].Reference;
	    break;

	}

	svl_array_push(allDescrs, attach.Description);
    }

    if (pInstance->IsDebugMode)
    {
	svl_i32 attachmentSwapChainedExist = 0;
	svl_i32 attachmentMultisampleResolveExist = 0;

	for (svl_i32 i = 0; i < svl_array_count(aAttachments); ++i)
	{
	    SvlAttachment item = aAttachments[i];
	    if (item.Type == SvlAttachmentType_SwapChained)
	    {
		attachmentSwapChainedExist = 1;
	    }

	    if (item.Type == SvlAttachmentType_ResolveMultisample)
	    {
		attachmentMultisampleResolveExist = 1;
	    }
	}

	if (attachmentSwapChainedExist && attachmentMultisampleResolveExist)
	{
	    // GERROR("For some weird reason u have Multisample Resolve Attachment and Swap Chained Attachment, it's not allowed!\n");
#if defined(SVL_DEBUG)
	    svl_guard(0 && "For some weird reason u have Multisample Resolve Attachment and Swap Chained Attachment, it's not allowed!");
#endif // SVL_DEBUG

	    pInstance->Error = SvlErrorType_RenderPass;
	    return (SvlRenderPassAttachments) {};
	}
    }

    SvlRenderPassAttachments att = {
	.aAllDescrs = allDescrs,
	.aColorRefs = aColorRefs,
	.pResolvedRef = pResolvedRef,
	.pDepthRef = pDepthRef,
	.aAttachments = aAttachments,
	.Samples = samples,
    };

    return att;
}

typedef struct SvlAttachmentCreateInfo
{
    VkAttachmentDescription Description;
    VkAttachmentReference Reference;
} SvlAttachmentCreateInfo;

typedef struct SvlAttachmentSettings
{

} SvlAttachmentSettings;

SvlAttachmentCreateInfo
svl_attachment_create_output_color(VkSampleCountFlagBits samples)
{
    return (SvlAttachmentCreateInfo) {};
}

SvlRenderPass
svl_default_2d_render_pass_create(SvlInstance* pInstance)
{
    VkSampleCountFlagBits samples = VK_SAMPLE_COUNT_1_BIT;
    VkFormat depthAttachmentFormat = VK_FORMAT_D32_SFLOAT;

    VkAttachmentDescription aAllDescrs[2] = {
	[0] = (VkAttachmentDescription) {
	    .format = pInstance->SurfaceFormat.format,
	    .samples = samples,
	    .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
	    .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
	    .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
	    .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
	    .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
	    .finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
	},
	[1] = (VkAttachmentDescription) {
	    .format = depthAttachmentFormat,
	    .samples = samples,
	    .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
	    .storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
	    .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
	    .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
	    .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
	    .finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
	}
    };

    VkAttachmentReference aColorRefs[] = {
	[0] = {
	    .attachment = 0,
	    .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
	}
    };

    VkAttachmentReference* pResolvedRef = NULL;
    VkAttachmentReference depthRef = {
	.attachment = 1,
	.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
    };

    VkSubpassDescription subpassDescription = {
	.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
	.colorAttachmentCount = 1,
	.pColorAttachments = aColorRefs,
	.pResolveAttachments = pResolvedRef,
	.pDepthStencilAttachment = &depthRef,
    };

    VkSubpassDependency subpassDependency = {
	.srcSubpass = VK_SUBPASS_EXTERNAL,
	.dstSubpass = 0,
	.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT,
	.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT,
	.srcAccessMask = 0,
	.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT,
	.dependencyFlags = 0,
    };

    VkRenderPassCreateInfo renderPassCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
	.pNext = NULL,
	// NOTE: Maybe in some cases it may cause a problem
	.attachmentCount = SvlArrayCount(aAllDescrs),
	.pAttachments = aAllDescrs,
	.subpassCount = 1,
	.pSubpasses = &subpassDescription,
	.dependencyCount = 1,
	.pDependencies = &subpassDependency
    };

    VkRenderPass vkRenderPass;
    VkResult createRenderPassResult = vkCreateRenderPass(pInstance->Device, &renderPassCreateInfo, pInstance->pAllocator, &vkRenderPass);
    if (svl_result_check(createRenderPassResult, "Can't create RenderPass!"))
    {
	pInstance->Error = SvlErrorType_RenderPass;
	return (SvlRenderPass) {};
    }

    SvlRenderPass renderPass = {
	.Handle = vkRenderPass,
	.Samples = samples,
    };

    // DOCS: Create image views and framebuffers
    {
	SvlImageViewSettings settings = {
	    .ImageSettings = {
		.Width = pInstance->Resolution.width,
		.Height = pInstance->Resolution.height,
		.Depth = 1.0f,
		.MipLevels = 1,
		.SamplesCount = samples,
		.Format = depthAttachmentFormat,
		.ImageTiling = VK_IMAGE_TILING_OPTIMAL,
		.ImageUsageFlags = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
		.MemoryPropertyFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
	    },
	    .Format = depthAttachmentFormat,
	    .ImageAspectFlags = VK_IMAGE_ASPECT_DEPTH_BIT,
	    .MipLevels = 1,
	};

	SvlImageView depthImageView = svl_image_view_create(pInstance, settings);
	if (pInstance->Error != SvlErrorType_None)
	{
	    svl_guard(0);
	}

	// note: Framebuffer count depends on SwapImageViewsCount
	svl_i32 framebuffersCount = svl_array_count(pInstance->aSwapImageViews);

	for (svl_i32 f = 0; f < framebuffersCount; ++f)
	{
	    SvlSwapImageView swapImageView = pInstance->aSwapImageViews[f];

	    VkImageView aFramebufferAttachments[] = {
		[0] = swapImageView.View,
		[1] = depthImageView.View,
	    };

	    _svl_render_pass_add_framebuffer_new(pInstance, &renderPass, aFramebufferAttachments, 2);
	}

	svl_array_push(renderPass.aImageViews, depthImageView);
    }


    // DOCS: Get id for renderpass and register it
    svl_i64 renderPassCount = svl_array_count(pInstance->aRenderPasses);
    renderPass.Id = renderPassCount;
    svl_array_push(pInstance->aRenderPasses, renderPass);

    return renderPass;
}

SvlRenderPass
svl_default_render_pass_create(SvlInstance* pInstance)
{
    SvlRenderPassSettings renderPassSet = {
	.Type = SvlRenderPassType_Prior,
	.Dependency = (VkSubpassDependency) {
	    .srcSubpass = VK_SUBPASS_EXTERNAL,
	    .dstSubpass = 0,
	    .srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT,
	    .dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT,
	    .srcAccessMask = 0,
	    .dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT,
	    .dependencyFlags = 0,
	},
	.AttachmentsCount = 3,

	.aAttachments = {
	    [0] = {
		.Type = SvlAttachmentType_Default,
		.Description = {
		    .format = pInstance->SurfaceFormat.format,
		    .samples = VK_SAMPLE_COUNT_8_BIT,
		    .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
		    .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
		    .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
		    .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
		    .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
		    .finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
		},
		.Reference = {
		    //.attachment = 0, AUTO DUE TO INDEX IN ARRAY
		    .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
		}
	    },

	    [1] = {
		.Type = SvlAttachmentType_Depth,
		.Description = {
		    .format = VK_FORMAT_D32_SFLOAT,
		    .samples = VK_SAMPLE_COUNT_8_BIT,
		    .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
		    .storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
		    .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
		    .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
		    .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
		    .finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
		},
		.Reference = {
		    .layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
		}
	    },

	    [2] = {
		.Type = SvlAttachmentType_SwapChained,
		.Description = {
		    .format = pInstance->SurfaceFormat.format,
		    .samples = VK_SAMPLE_COUNT_1_BIT,
		    .loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
		    .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
		    .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
		    .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
		    .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
		    .finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
		},
		.Reference = {
		    .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
		}
	    }
	}
    };

    SvlRenderPass svlRenderPass = svl_render_pass_create(pInstance, renderPassSet);
    return svlRenderPass;
}

SvlRenderPass
svl_render_pass_create(SvlInstance* pInstance, SvlRenderPassSettings settings)
{
    /*
      DEPENDS:
      * pInstance->Resolution (/Extent)
      * pInstance->SurfaceFormats
      * pInstance->SamplesCount
      * pInstance->Device
    */

    SvlRenderPassAttachments attachments = _svl_render_pass_create_attachment( pInstance, settings);

    VkSubpassDescription subpassDescription = {
	.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
	.colorAttachmentCount = svl_array_count(attachments.aColorRefs),
	.pColorAttachments = attachments.aColorRefs,
	.pResolveAttachments = attachments.pResolvedRef,
	.pDepthStencilAttachment = attachments.pDepthRef,

	// NOTE: Just for doc
	.inputAttachmentCount = 0,
	.pInputAttachments = NULL,
	.preserveAttachmentCount = 0,
	.pPreserveAttachments = NULL
    };

    VkRenderPassCreateInfo renderPassCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
	.pNext = NULL,
	// NOTE: Maybe in some cases it may cause a problem
	.attachmentCount = svl_array_count(attachments.aAllDescrs),
	.pAttachments = attachments.aAllDescrs,
	.subpassCount = 1,
	.pSubpasses = &subpassDescription,
	.dependencyCount = 1,
	.pDependencies = &settings.Dependency
    };

    VkRenderPass vkRenderPass;
    VkResult createRenderPassResult =
	vkCreateRenderPass(pInstance->Device, &renderPassCreateInfo, pInstance->pAllocator, &vkRenderPass);
    if (svl_result_check(createRenderPassResult, "Can't create RenderPass!"))
    {
	pInstance->Error = SvlErrorType_RenderPass;
	return (SvlRenderPass) {};
    }

    SvlRenderPass renderPass = {
	.Type = settings.Type,
	.Handle = vkRenderPass,
	.aAttachments = attachments.aAttachments,
	.Samples = attachments.Samples,
    };

    // DOCS: Create image views and framebuffers
    svl_render_pass_update_images_framebuffers(pInstance, &renderPass);

    svl_array_free(attachments.aAllDescrs);
    svl_array_free(attachments.aColorRefs);

    // DOCS: Get id for renderpass and register it
    svl_i64 renderPassCount = svl_array_count(pInstance->aRenderPasses);
    renderPass.Id = renderPassCount;
    svl_array_push(pInstance->aRenderPasses, renderPass);

    return renderPass;
}

void
svl_render_pass_destroy(SvlInstance* pInstance, SvlRenderPass renderPass)
{
    // todo: impl better structure for render_pass_id + save ids that was deleted

    {
	// DOCS: Destroy framebuffer's
	svl_render_pass_destroy_framebuffers(pInstance, &renderPass);
    }

    {
	// DOCS: Destroy pipeline's
	for (svl_i32 i = 0; i < renderPass.PipelinesCount; ++i)
	    svl_pipeline_destroy(pInstance, renderPass.apPipelines[i]);
    }

    vkDestroyRenderPass(pInstance->Device, renderPass.Handle, pInstance->pAllocator);
}

void
svl_render_pass_draw(SvlInstance* pInstance, SvlRenderPass* pRenderPass)
{
    // DOCS: variable decl
    VkCommandBuffer cmd = pInstance->Cmd.Buffer;
    VkExtent2D vkExtent = pInstance->Resolution;

    /*
      DOCS(typedef): Recording Commands BEGIN
    */

    VkCommandBufferBeginInfo commandBufferBeginInfo = {
	.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
	.pNext = NULL,
	.flags = 0,
	.pInheritanceInfo = NULL,
    };

    VkResult beginCmdResult = vkBeginCommandBuffer(cmd, &commandBufferBeginInfo);
    // todo: log result beginCmdResult on debug only


    VkClearValue aClearValues[2] = {
	[0] = (VkClearValue) {
	    .color = pRenderPass->ClearColor,
	},
	[1] = (VkClearValue) {
	    .depthStencil = { 1.0f, 0.0f }
	}
    };

    VkFramebuffer currentFramebuffer = pRenderPass->aFramebuffers[pRenderPass->Frame.ImageIndex];

    VkRenderPassBeginInfo renderPassBeginInfo = {
	.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
	.pNext = NULL,
	.renderPass = pRenderPass->Handle,
	.framebuffer = currentFramebuffer,
	.renderArea = {
	    .offset = { 0, 0 },
	    .extent = vkExtent
	},
	.clearValueCount = 2,
	.pClearValues = aClearValues
    };
    vkCmdBeginRenderPass(cmd, &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

    for (svl_i32 p = 0; p < pRenderPass->PipelinesCount; ++p)
    {
	SvlPipeline* pPipe = pRenderPass->apPipelines[p];

	//DOCS PIPELINE
	//TODO(typedef): add for (view in viewports)
	VkViewport viewport = {
	    .x = 0,
	    .y = 0,
	    .width = vkExtent.width,
	    .height = vkExtent.height,
	    .minDepth = 0,
	    .maxDepth = 1
	};
	vkCmdSetViewport(cmd, 0, 1, &viewport);

	//TODO(typedef): set in pipeline
	VkRect2D scissor = {
	    .offset = { 0, 0 },
	    .extent = vkExtent
	};
	vkCmdSetScissor(cmd, 0, 1, &scissor);

	vkCmdBindPipeline(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, pPipe->Handle);
	vkCmdBindDescriptorSets(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, pPipe->Layout, 0, 1, &pPipe->Binding.DescriptorSet, 0, NULL);

	VkDeviceSize offsets[1] = { 0 };
	vkCmdBindVertexBuffers(cmd, 0, 1, &pPipe->Vertex.Gpu, offsets);
	vkCmdBindIndexBuffer(cmd, pPipe->Index.Gpu, 0, VK_INDEX_TYPE_UINT32);

	vkCmdDrawIndexed(cmd, pPipe->IndicesCount, 1, 0, 0, 0);

	pPipe->VerticesCount = 0;
	pPipe->IndicesCount = 0;
    }

    vkCmdEndRenderPass(cmd);
    vkEndCommandBuffer(cmd);
}

SvlRenderPass*
svl_render_pass(SvlInstance* pInstance, svl_i32 id)
{
    svl_i64 renderPassCount = svl_array_count(pInstance->aRenderPasses);
    if (id >= 0 && id < renderPassCount)
	return &pInstance->aRenderPasses[id];
    return NULL;
}

/*#######################
  DOCS(typedef): Buffer
  #######################*/

VkBuffer
_svl_buffer_create(SvlInstance* pInstance, VkBufferUsageFlags usage, VkDeviceSize size, VkMemoryPropertyFlags memoryPropertyFlags, VkDeviceMemory* deviceMemory)
{
    VkBuffer vkBuffer;

    VkBufferCreateInfo vkCreateBufferCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
	.pNext = NULL,
	.flags = 0,
	.size = size,
	.usage = usage,
	.sharingMode = VK_SHARING_MODE_EXCLUSIVE,
	.queueFamilyIndexCount = 0,
	.pQueueFamilyIndices = NULL
    };

    VkResult createBufferResult = vkCreateBuffer(pInstance->Device, &vkCreateBufferCreateInfo, pInstance->pAllocator, &vkBuffer);
    if (svl_result_check(createBufferResult, "Failed Vertex Buffer creation!"))
    {
    }

    //NOTE(typedef): Allocating memory for VkBuffer
    VkMemoryRequirements vertexBufferMemoryRequirements;
    vkGetBufferMemoryRequirements(pInstance->Device, vkBuffer, &vertexBufferMemoryRequirements);

    // NOTE(typedef): Find suitable memory type
    svl_i32 index = svl_find_memory_type_index(
	pInstance,
	vertexBufferMemoryRequirements.memoryTypeBits,
	memoryPropertyFlags);

    VkMemoryAllocateInfo memoryAllocateInfo = {
	.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
	.pNext = NULL,
	.allocationSize = vertexBufferMemoryRequirements.size,
	.memoryTypeIndex = index
    };

    VkResult allocateMemoryResult = vkAllocateMemory(pInstance->Device, &memoryAllocateInfo, pInstance->pAllocator, deviceMemory);
    if (svl_result_check(allocateMemoryResult, "Failed triangle deivce memory allocation!"))
    {
    }

    VkResult bindBufferMemoryResult = vkBindBufferMemory(pInstance->Device, vkBuffer, *deviceMemory, 0);
    if (svl_result_check(bindBufferMemoryResult, "Failed binding buffer memory with buffer!"))
    {
    }

    return vkBuffer;

}

void
svl_buffer_stage_and_gpu_create(SvlInstance* pInstance, VkBufferUsageFlags usage, size_t size, VkBuffer* pGpuBuffer, VkDeviceMemory* pDeviceMemory, VkBuffer* pStagingBuffer, VkDeviceMemory* pStagingMemory, SvlErrorType* pError)
{
    VkBuffer stagingBuffer = _svl_buffer_create(
	pInstance,
	VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
	size,
	(VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT),
	pStagingMemory);

    VkBuffer gpuBuffer = _svl_buffer_create(
	pInstance,
	(VK_BUFFER_USAGE_TRANSFER_DST_BIT | usage),
	size,
	(VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT),
	pDeviceMemory);

    *pGpuBuffer = gpuBuffer;
    *pStagingBuffer = stagingBuffer;
}

SvlBuffer
svl_buffer_create(SvlInstance* pInstance, SvlBufferSettings set, SvlErrorType* pError)
{
    VkBuffer stage, gpu;
    VkDeviceMemory stageMem, gpuMem;

    svl_buffer_stage_and_gpu_create(pInstance,
				    set.Usage, set.Size,
				    &gpu, &gpuMem,
				    &stage, &stageMem,
				    pError);

    SvlBuffer buffer = {
	.Size = set.Size,
	.Staging = stage,
	.Gpu = gpu,
	.StagingMemory = stageMem,
	.GpuMemory = gpuMem,
    };

    return buffer;
}

void
svl_buffer_set_data(SvlInstance* pInstance, SvlBuffer* pBuffer, void* pData, svl_size offset, svl_size size)
{
    svl_guard(pData != NULL);
    svl_guard(size > 0);

    VkDevice device = pInstance->Device;
    VkCommandPool pool = pInstance->Cmd.Pool;

    void* data;
    VkResult vkMapResult = vkMapMemory(device, pBuffer->StagingMemory, offset, size, 0, &data);
    if (svl_result_check(vkMapResult, "Failed mapping memory!"))
    {
    }

    memcpy(data, pData, size);
    vkUnmapMemory(device, pBuffer->StagingMemory);

    VkCommandBuffer cmdBuffer = svl_cmd_begin(pInstance);

    VkBufferCopy copyRegion = {
	.srcOffset = offset,
	.dstOffset = offset,
	.size = size,
    };

    vkCmdCopyBuffer(cmdBuffer, pBuffer->Staging, pBuffer->Gpu, 1, &copyRegion);

    svl_cmd_end(pInstance, cmdBuffer);
}

void
svl_buffer_destroy(SvlInstance* pInstance, SvlBuffer* pBuffer)
{
    VkDevice device = pInstance->Device;
    VkAllocationCallbacks* pAllocator = pInstance->pAllocator;

    vkDestroyBuffer(device, pBuffer->Staging, pAllocator);
    vkFreeMemory(device, pBuffer->StagingMemory, pAllocator);
    vkDestroyBuffer(device, pBuffer->Gpu, pAllocator);
    vkFreeMemory(device, pBuffer->GpuMemory, pAllocator);
}


/*#######################
  DOCS(typedef): Uniform
  #######################*/

SvlUniform
svl_uniform_create(SvlInstance* pInstance, SvlUniformSettings set)
{
    SvlErrorType error = 0;

    SvlBuffer svlBuffer = svl_buffer_create(
	pInstance,
	(SvlBufferSettings) {
	    .Size = set.Size,
	    .Usage =  VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
	}, &error);

    svl_guard(error == SvlErrorType_None);

    SvlUniform svlUniform = {
	.Size = set.Size,
	.ItemSize = set.ItemSize,
	.pName = set.pName,
	.BindIndex = set.BindIndex,
	.Buffer = svlBuffer,
    };

    return svlUniform;
}


void
svl_uniform_destroy(SvlInstance* pInstance, SvlUniform* pUniform)
{
    svl_buffer_destroy(pInstance, &pUniform->Buffer);
}

/*#######################
  DOCS(typedef): Pipeline
  #######################*/

void
svl_uniform_set(SvlInstance* pInstance, SvlPipeline* pPipeline, SvlUniform* pUniform, void* pData, svl_size offset, svl_size size)
{
    if ((offset + size) > pUniform->Size)
	return;

    VkDevice vkDevice = pInstance->Device;

    svl_buffer_set_data(pInstance, &pUniform->Buffer, pData, offset, size);

    svl_i64 allCount = pUniform->Size / pUniform->ItemSize;

    VkDescriptorBufferInfo bufferInfo = {
	.buffer = pUniform->Buffer.Gpu,
	.offset = offset,
	.range = size
    };

    // NOTE(FEATURE0): VK_DESCRIPTOR_BINDING_UPDATE_AFTER_BIND_BIT VkDescriptorSetLayoutBindingFlagsCreateInfo
    VkWriteDescriptorSet vkWriteDescriptorSet = {
	.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
	.pNext = NULL,
	.dstSet = pPipeline->Binding.DescriptorSet,
	.dstBinding = pUniform->BindIndex,
	.dstArrayElement = 0,
	.descriptorCount = 1,
	.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
	.pImageInfo = NULL,
	.pBufferInfo = &bufferInfo, //bufferInfos,
	.pTexelBufferView = NULL
    };

    // NOTE/TODO(): Can update everything in one call
    VkWriteDescriptorSet vkWriteDescriptorSets[] = {
	[0] = vkWriteDescriptorSet
    };

    vkUpdateDescriptorSets(vkDevice, 1, vkWriteDescriptorSets, 0, NULL);

}


/*#######################
  DOCS(typedef): Cmd
  #######################*/

VkCommandBuffer
svl_cmd_begin(SvlInstance* pInstance)
{
    VkCommandBufferAllocateInfo vkCmdBufferAllocatedInfo = {
	.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
	.pNext = NULL,
	.commandPool = pInstance->Cmd.Pool,
	.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
	.commandBufferCount = 1
    };

    VkCommandBuffer vkCmdBuffer;
    VkResult vkAllocateCmdBuffersResult =
	vkAllocateCommandBuffers(pInstance->Device, &vkCmdBufferAllocatedInfo, &vkCmdBuffer);
    if (svl_result_check(vkAllocateCmdBuffersResult, "Failed cmd buffer allocation!\n"))
    {
    }

    VkCommandBufferBeginInfo beginInfo = {
	.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
	.pNext = NULL,
	.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
	.pInheritanceInfo = NULL
    };
    vkBeginCommandBuffer(vkCmdBuffer, &beginInfo);

    return vkCmdBuffer;
}

void
svl_cmd_end(SvlInstance* pInstance, VkCommandBuffer vkCmdBuffer)
{
    VkResult vkEndCmdBufferResult = vkEndCommandBuffer(vkCmdBuffer);
    if (svl_result_check(vkEndCmdBufferResult, "Failed end cmd buffer!"))
    {
    }

    // Execute command
    VkSubmitInfo submitInfo = {
	.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
	.pNext = NULL,
	.waitSemaphoreCount = 0,
	.pWaitSemaphores = NULL,
	.pWaitDstStageMask = NULL,
	.commandBufferCount = 1,
	.pCommandBuffers = &vkCmdBuffer,
	.signalSemaphoreCount = 0,
	.pSignalSemaphores = NULL
    };

    vkQueueSubmit(pInstance->GraphicsQueue, 1, &submitInfo, VK_NULL_HANDLE);
    vkQueueWaitIdle(pInstance->GraphicsQueue);

    // Free command for buffer
    vkFreeCommandBuffers(pInstance->Device, pInstance->Cmd.Pool, 1, &vkCmdBuffer);
}


/*#######################
  DOCS(typedef): Frame
  #######################*/

SvlFrameResult
svl_frame_start(SvlInstance* pInstance, SvlFrame* pFrame)
{
    /*
      NOTE(typedef): Render Loop
      * Wait for the previous frame to finish
      * Acquire an image from the swap chain
      * Reset cmd buffer
      */

    VkDevice device = pInstance->Device;
    VkSwapchainKHR swapchain = pInstance->Swapchain;
    VkSemaphore imageAvailable = pInstance->ImageAvailableSemaphore;
    VkFence* pFrameFence = &pInstance->FrameFence;
    VkCommandBuffer cmdBuf = pInstance->Cmd.Buffer;

    static svl_u64 noTime = (18446744073709551615ULL);
    vkWaitForFences(device, 1, pFrameFence, VK_TRUE, noTime);

    VkResult accuireNextImageResult =
	vkAcquireNextImageKHR(device, swapchain, noTime, imageAvailable, VK_NULL_HANDLE, &pFrame->ImageIndex);
    if (accuireNextImageResult == VK_ERROR_OUT_OF_DATE_KHR)
    {
	// todo: impl
	// GINFO("Recreate!\n");
	// vsa_swapchain_objects_recreate();
	return SvlFrameResult_RecreateSwapchain;
    }
    else if (accuireNextImageResult == VK_SUBOPTIMAL_KHR)
    {
	// DOCS(typedef): this thing is not an error or smth, just continue there
	// GWARNING("Swapchain is VK_SUBOPTIMAL_KHR!\n");
	return SvlFrameResult_SurfaceChanged;
    }
    else
    {
	// vkValidResult(accuireNextImageResult, "Can't acquire next image khr!");
    }

    vkResetFences(device, 1, pFrameFence);

    const svl_i32 no_flag = 0;
    vkResetCommandBuffer(cmdBuf, no_flag);

    return SvlFrameResult_Success;
}

void
svl_frame_end(SvlInstance* pInstance, SvlFrame* pFrame)
{
    /*
      NOTE(typedef):
      * Submit the recorded command buffer
      * Present the swap chain image
      */

    VkPipelineStageFlags pipelineStageFlag = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;

    VkSubmitInfo submitInfo = {
	.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
	.pNext = NULL,
	.waitSemaphoreCount = 1,
	.pWaitSemaphores = &pInstance->ImageAvailableSemaphore,
	.pWaitDstStageMask = &pipelineStageFlag,
	.commandBufferCount = 1,
	.pCommandBuffers = &pInstance->Cmd.Buffer,
	.signalSemaphoreCount = 1,
	.pSignalSemaphores = &pInstance->RenderFinishedSemaphore
    };

    // NOTE(typedef): Submiting Recorded Commands
    VkResult queueSubmitResult =
	vkQueueSubmit(pInstance->GraphicsQueue, 1, &submitInfo, pInstance->FrameFence);
    //vkValidResult(queueSubmitResult, "Can't Submit Queue!");

    VkPresentInfoKHR presentInfoKhr = {
	.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
	.pNext = NULL,
	.waitSemaphoreCount = 1,
	.pWaitSemaphores = &pInstance->RenderFinishedSemaphore,
	.swapchainCount = 1,
	.pSwapchains = &pInstance->Swapchain,
	.pImageIndices = (const svl_u32*) &pFrame->ImageIndex,
	.pResults = NULL
    };

    VkResult queuePresentKhr =
	vkQueuePresentKHR(pInstance->GraphicsQueue, &presentInfoKhr);
    if (queuePresentKhr == VK_ERROR_OUT_OF_DATE_KHR || queuePresentKhr == VK_SUBOPTIMAL_KHR /* || gIsWindowBeenResized */)
    {
	//GINFO("Recreate!\n");
	//gIsWindowBeenResized = 0;
	//vsa_swapchain_objects_recreate();
	return;
    }
    else
    {
	// vkValidResult(queuePresentKhr, "Queue Present KHR!\n");
    }

}

/*#######################
  DOCS(typedef): Helpers Api
  #######################*/

const char*
svl_result_to_string(VkResult vkResult)
{
    switch (vkResult)
    {

    case VK_ERROR_OUT_OF_HOST_MEMORY: return "VK_ERROR_OUT_OF_HOST_MEMORY";
    case VK_ERROR_OUT_OF_DEVICE_MEMORY: return "VK_ERROR_OUT_OF_DEVICE_MEMORY";

    case VK_SUCCESS: return "VK_SUCCESS";
    case VK_NOT_READY: return "VK_NOT_READY";
    case VK_TIMEOUT: return "VK_TIMEOUT";
    case VK_EVENT_SET: return "VK_EVENT_SET";
    case VK_EVENT_RESET: return "VK_EVENT_RESET";
    case VK_INCOMPLETE: return "VK_INCOMPLETE";
    case VK_ERROR_INITIALIZATION_FAILED: return "VK_ERROR_INITIALIZATION_FAILED";

    case VK_ERROR_DEVICE_LOST: return "VK_ERROR_DEVICE_LOST";
    case VK_ERROR_MEMORY_MAP_FAILED: return "VK_ERROR_MEMORY_MAP_FAILED";
    case VK_ERROR_LAYER_NOT_PRESENT: return "VK_ERROR_LAYER_NOT_PRESENT";
    case VK_ERROR_EXTENSION_NOT_PRESENT: return "VK_ERROR_EXTENSION_NOT_PRESENT";
    case VK_ERROR_FEATURE_NOT_PRESENT: return "VK_ERROR_FEATURE_NOT_PRESENT";
    case VK_ERROR_INCOMPATIBLE_DRIVER: return "VK_ERROR_INCOMPATIBLE_DRIVER";
    case VK_ERROR_TOO_MANY_OBJECTS: return "VK_ERROR_TOO_MANY_OBJECTS";
    case VK_ERROR_FORMAT_NOT_SUPPORTED: return "VK_ERROR_FORMAT_NOT_SUPPORTED";
    case VK_ERROR_FRAGMENTED_POOL: return "VK_ERROR_FRAGMENTED_POOL";
    case VK_ERROR_UNKNOWN: return "VK_ERROR_UNKNOWN";
    case VK_ERROR_OUT_OF_POOL_MEMORY: return "VK_ERROR_OUT_OF_POOL_MEMORY";
    case VK_ERROR_INVALID_EXTERNAL_HANDLE: return "VK_ERROR_INVALID_EXTERNAL_HANDLE";

    case VK_ERROR_FRAGMENTATION: return "VK_ERROR_FRAGMENTATION";
    case VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS: return "VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS";
    case VK_PIPELINE_COMPILE_REQUIRED: return "VK_PIPELINE_COMPILE_REQUIRED";
    case VK_ERROR_SURFACE_LOST_KHR: return "VK_ERROR_SURFACE_LOST_KHR";
    case VK_ERROR_NATIVE_WINDOW_IN_USE_KHR: return "VK_ERROR_NATIVE_WINDOW_IN_USE_KHR";
    case VK_SUBOPTIMAL_KHR: return "K_SUBOPTIMAL_KHR";
    case VK_ERROR_OUT_OF_DATE_KHR: return "VK_ERROR_OUT_OF_DATE_KHR";
    case VK_ERROR_INCOMPATIBLE_DISPLAY_KHR: return "VK_ERROR_INCOMPATIBLE_DISPLAY_KHR";
    case VK_ERROR_VALIDATION_FAILED_EXT: return "VK_ERROR_VALIDATION_FAILED_EXT";
    case VK_ERROR_INVALID_SHADER_NV: return "VK_ERROR_INVALID_SHADER_NV";
    case VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT: return "VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT";
    case VK_ERROR_NOT_PERMITTED_KHR: return "VK_ERROR_NOT_PERMITTED_KHR";
    case VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT: return "VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT";

    case VK_THREAD_IDLE_KHR: return "VK_THREAD_IDLE_KHR";
    case VK_THREAD_DONE_KHR: return "VK_THREAD_DONE_KHR";
    case VK_OPERATION_DEFERRED_KHR: return "VK_OPERATION_DEFERRED_KHR";
    case VK_OPERATION_NOT_DEFERRED_KHR: return "VK_OPERATION_NOT_DEFERRED_KHR";
    case VK_ERROR_COMPRESSION_EXHAUSTED_EXT: return "VK_ERROR_COMPRESSION_EXHAUSTED_EXT";

    case VK_ERROR_VIDEO_STD_VERSION_NOT_SUPPORTED_KHR: return "VK_ERROR_VIDEO_STD_VERSION_NOT_SUPPORTED_KHR";
    case VK_ERROR_VIDEO_PROFILE_CODEC_NOT_SUPPORTED_KHR: return "VK_ERROR_VIDEO_PROFILE_CODEC_NOT_SUPPORTED_KHR";
    case VK_ERROR_VIDEO_PROFILE_FORMAT_NOT_SUPPORTED_KHR: return "VK_ERROR_VIDEO_PROFILE_FORMAT_NOT_SUPPORTED_KHR";
    case VK_ERROR_VIDEO_PROFILE_OPERATION_NOT_SUPPORTED_KHR: return "VK_ERROR_VIDEO_PROFILE_OPERATION_NOT_SUPPORTED_KHR";
    case VK_ERROR_VIDEO_PICTURE_LAYOUT_NOT_SUPPORTED_KHR: return "VK_ERROR_VIDEO_PICTURE_LAYOUT_NOT_SUPPORTED_KHR";
    case VK_ERROR_IMAGE_USAGE_NOT_SUPPORTED_KHR: return "VK_ERROR_IMAGE_USAGE_NOT_SUPPORTED_KHR";
    case VK_ERROR_INCOMPATIBLE_SHADER_BINARY_EXT: return "VK_ERROR_INCOMPATIBLE_SHADER_BINARY_EXT";
    case VK_RESULT_MAX_ENUM: return "VK_RESULT_MAX_ENUM";

    }

    return "Unknown result! Mb update is required for vkResultToString";
}

void
_svl_guard(svl_i32 condition, const char* pCond, svl_i32 line, const char* pFile)
{
    if (condition)
	return;

    printf(SvlTerminalRed("Error")" %s on line %d in file %s\n", pCond, line, pFile);
    svl_i32* i = (void*)0;
    *i = 1337;
}

svl_i32
svl_result_check(VkResult vkResult, const char* pMessage)
{
    if (vkResult == VK_SUCCESS)
	return 0;

    const char* pDescr = svl_result_to_string(vkResult);
    printf(SvlTerminalRed("Svl-Error: %s, message: %s")"\n", pDescr, pMessage);
    svl_guard(vkResult == VK_SUCCESS);

    return 1;
}
