#ifndef SYSTEM_INFO_H
#define SYSTEM_INFO_H

typedef enum OsType
{
    OsType_Linux = 0,
    OsType_Windows,

    OsType_Count
} OsType;

typedef enum SystemType
{
    SystemType_LittleEndian = 0,
    SystemType_BigEndian
} SystemType;

typedef enum FloatType
{
    FloatType_32Bit = 0,
    FloatType_64Bit
} FloatType;

typedef enum BuildType
{
    BuildType_Debug,
    BuildType_Release
} BuildType;

typedef struct SystemInfo
{
    OsType Os;
    SystemType System;
    FloatType Float;
    BuildType Build;
} SystemInfo;

extern SystemInfo gSystemInfo;

const char* os_type_to_string(OsType type);
const char* system_type_to_string(SystemType type);
const char* float_type_to_string(FloatType type);
const char* build_type_to_string(BuildType type);

#endif // SYSTEM_INFO_H
