#include "SimpleApplication.h"

#include <locale.h>
#include <Core/SimpleStandardLibrary.h>
#include <Core/SimpleWindowLibrary.h>
#include <Std/StdAssetManager.h>
#include <Layers/GraphicsLayer.h>
#include <Layers/SoundLayer.h>
#include <Layers/UiLayer.h>
#include <Std/StdRenderer.h>


typedef struct SimpleApplication
{
    char* pName;

    i8 IsRunning;
    i8 IsDebug;
    i8 IsVsync;
    i8 IsMinimized;

    SimpleLayer* aLayers;

    LayerAction* aOnUpdates;
    LayerAction* aOnUis;
    LayerFunction* aOnEvents;
    LayerAction* aOnBeforeUpdates;
    LayerAction* aOnAfterUpdates;

    SimpleRuntimeStats RuntimeStats;
    SwlWindow Window;
} SimpleApplication;

static SimpleApplication g = {};


// DOCS: Forward declaration
void simple_application_on_event(struct SwlEvent* pEvent);
SimpleRuntimeStats simple_runtime_stats_create();
void simple_runtime_stats_update(SimpleRuntimeStats* pStats, f64 timestep);

void
_init_slog()
{
    // DOCS: Initialize log
    SlogBackend* aBackends = NULL;

    SlogBackend terminalBackend = {
	.Format = SlogFormat_Level | SlogFormat_Time | SlogFormat_File_Short | SlogFormat_Line | SlogFormat_Function,
	.Action = slog_back_terminal,
    };
    sva_add(aBackends, terminalBackend);

    SlogBackend fileBackend = {
	.Format = SlogFormat_Level | SlogFormat_Time | SlogFormat_File_Short | SlogFormat_Line | SlogFormat_Function,
	.Action = slog_back_file,
    };
    sva_add(aBackends, fileBackend);

    SlogConfig config = { aBackends };
    slog_init(config);
}

void
simple_application_create(SimpleApplicationSettings* pSettings)
{
    // DOCS: Set locale to utf-8
    setlocale(LC_ALL, ".UTF8");
    _init_slog();

    g = (SimpleApplication) {
	.pName = simple_string_new(pSettings->pName),
	.IsDebug = pSettings->IsDebug,
	.IsVsync = pSettings->IsVsync,
    };

    SimpleString* pHdr = simple_string_header(g.pName);

    // DOCS: Creating window
    swl_init((SwlInit) { .MemoryAllocate = malloc, .MemoryFree = free, });
    SwlWindowSettings set = {
	.pName = pHdr->pData,
	.NameLength = pHdr->Size,
	.Position = { .X = pSettings->Position.X, .Y = pSettings->Position.Y },
	.Size = {
	    .Width = IfFalseThen(pSettings->Size.Width, 100),//968,
	    .Height = IfFalseThen(pSettings->Size.Height, 100),
	    .MinHeight = IfFalseThen(pSettings->MinSize.X, 100),
	    .MinWidth = IfFalseThen(pSettings->MinSize.Y, 100)
	},
    };
    swl_window_create(&g.Window, set);

    if (pSettings->ScreenMode == SimpleScreenMode_F11)
    {
	swl_window_set_maximized(&g.Window);
	//SwlResolution resolution = swl_window_get_resolution(&g.Window);
	//g.Window.Size.Height = resolution.Height;
	//g.Window.Size.Width = resolution.Width;
	//slog_warn("%d %d \n", g.Window.Size.Height, g.Window.Size.Width);
    }
    else if (pSettings->ScreenMode == SimpleScreenMode_FullScreen)
    {
	swl_window_set_fullscreen(&g.Window);
    }

    swl_window_set_event_call(&g.Window, simple_application_on_event);

    std_asset_manager_create();

    SimpleLayer soundLayer = {
	.pName = simple_string_new("Звуковой слой"),
	.OnAttach = sound_layer_on_attach,
	.OnDestroy = sound_layer_on_destroy,
    };
    simple_application_add_layer(soundLayer);

    SimpleLayer uiLayer = {
	.pName = simple_string_new("Интерфейсный слой"),
	.OnAttach = ui_layer_attach,
	.OnUpdate = ui_layer_update,
	.OnBeforeUpdate = ui_layer_before_update,
	.OnAfterUpdate = ui_layer_after_update,
	.OnEvent = ui_layer_event,
	.OnDestroy = ui_layer_destroy,
    };
    simple_application_add_layer(uiLayer);

    SimpleLayer graphLayer = {
	.pName = simple_string_new("Графический слой"),
	.OnAttach = graphics_layer_attach,
	.OnAfterUpdate = graphics_layer_after_update,
	.OnEvent = graphics_layer_event,
	.OnDestroy = graphics_layer_destroy,
    };
    simple_application_add_layer(graphLayer);
}

void
simple_application_destroy()
{
    i64 count = sva_count(g.aLayers);
    for (i64 i = 0; i < count; ++i)
    {
	SimpleLayer layer = g.aLayers[i];
	layer.OnDestroy();
    }

    swl_window_destroy(&g.Window);

    // DOCS: Need to call vkDeviceWaitIdle
    std_renderer_prepare_for_destruction();

    std_asset_manager_destroy();

    std_renderer_destroy();

    slog_deinit();
}

void
simple_application_run()
{
    g.RuntimeStats = simple_runtime_stats_create();

    g.IsRunning = 1;
    while (g.IsRunning)
    {
	SimpleProfiler profiler = {};
	simple_profiler_start(&profiler);

	if (!g.IsMinimized)
	{
	    i32 beforesCount = sva_count(g.aOnBeforeUpdates);
	    for (i32 l = 0; l < beforesCount; ++l)
	    {
		LayerAction onBeforeUpdate = g.aOnBeforeUpdates[l];
		onBeforeUpdate();
	    }

	    i32 updatesCount = sva_count(g.aOnUpdates);
	    for (i32 l = 0; l < updatesCount; ++l)
	    {
		LayerAction onUpdate = g.aOnUpdates[l];
		onUpdate();
	    }

	    i32 aftersCount = sva_count(g.aOnAfterUpdates);
	    for (i32 l = 0; l < aftersCount; ++l)
	    {
		LayerAction onAfterUpdate = g.aOnAfterUpdates[l];
		onAfterUpdate();
	    }

	}

	simple_profiler_end(&profiler);
	f64 timestep = simple_profiler_get_seconds_as_float(&profiler);
	simple_runtime_stats_update(&g.RuntimeStats, timestep);

	swl_window_update(&g.Window);

	// note: ?, we need app even if a window is destroyed or closed
	if (g.IsRunning)
	    g.IsRunning = g.Window.IsAlive;
    }
}

SimpleLayer*
simple_application_get_layers()
{
    return g.aLayers;
}

void
simple_application_on_event(SwlEvent* pEvent)
{
    // note: temp (for demo only)
    if (pEvent->Type == SwlEventType_KeyPress)
    {
	SwlKeyPressEvent* pKeyEvent = (SwlKeyPressEvent*) pEvent;
	if (pKeyEvent->Key == SwlKey_Escape)
	{
	    g.IsRunning = 0;
	}
    }

    i32 onEventsCount = sva_count(g.aOnEvents);
    for (i32 l = 0; l < onEventsCount; ++l)
    {
	LayerFunction onEvent = g.aOnEvents[l];

	onEvent(pEvent);

	if (pEvent->IsHandled)
	    return;
    }
}

void
simple_application_add_layer(SimpleLayer newLayer)
{
    // todo: validate layer

    newLayer.IsActive = 1;

    if (newLayer.OnAttach)
	newLayer.OnAttach();

    if (newLayer.OnUpdate)
	sva_add(g.aOnUpdates, newLayer.OnUpdate);

    if (newLayer.OnEvent)
	sva_add(g.aOnEvents, newLayer.OnEvent);

    if (newLayer.OnAfterUpdate)
	sva_add(g.aOnAfterUpdates, newLayer.OnAfterUpdate);

    if (newLayer.OnBeforeUpdate)
	sva_add(g.aOnBeforeUpdates, newLayer.OnBeforeUpdate);

    if (newLayer.OnUi)
	sva_add(g.aOnUis, newLayer.OnUi);

    sva_add(g.aLayers, newLayer);
}

SwlWindow*
simple_application_get_window()
{
    return &g.Window;
}

SimpleRuntimeStats
simple_application_get_stats()
{
    return g.RuntimeStats;
}

SimpleRuntimeStats
simple_runtime_stats_create()
{
    SimpleRuntimeStats stats = {
	.StartedAt = simple_time_get_seconds(),
    };
    return stats;
}

LayerAction*
simple_application_get_uis()
{
    return g.aOnUis;
}

void
simple_runtime_stats_update(SimpleRuntimeStats* pStats, f64 timestep)
{
    pStats->Timestep = timestep;
    pStats->SecondElapsed += timestep;

    if (pStats->SecondElapsed >= 1.0)
    {
	pStats->Fps = 0;
	pStats->SecondElapsed -= 1.0;
    }

    ++pStats->Fps;
}
