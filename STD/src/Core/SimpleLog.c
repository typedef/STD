#include "SimpleLog.h"

#include <Core/Types.h>


void
simple_log(const char* pFormat, ...)
{
    const char separator = '%';
    // simple_log("Number: %d\n");

    i32 argsCount = 0;

    // DOCS: Calculate input params in format

#define IsMetaChar(c)                           \
    ({                                          \
        i32 isMetaChar = c == 'c' || c == 's'   \
            || c == 'd' || c == 'i'             \
            || c == 'b'                         \
            || c == 'f';                        \
        isMetaChar;                             \
    })

    i32 state = 0;
    for (char* ptr = (char*)pFormat; *ptr != '\0'; ++ptr)
    {
        char c = *ptr;
        if (c == '%')
        {
            state = 1;
        }
        else if ((state == 1) && IsMetaChar(c))
        {
            state = 0;
            ++argsCount;
        }
    }

    // DOCS: Do the job

    char* ptr = (char*) pFormat;
    char c = *ptr;

    typedef enum StateType {
        StateType_
    } StateType;

    while (c != '\0')
    {

        switch (c)
        {
        case '%':
        {
            break;
        }
        }

    }

}
