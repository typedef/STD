#ifndef SIMPLE_LOG_H
#define SIMPLE_LOG_H

void simple_log(const char* pFormat, ...);

#endif // SIMPLE_LOG_H
