#ifndef SIMPLE_INFO_H
#define SIMPLE_INFO_H

typedef enum OsType
{
    OsType_Linux = 0,
    OsType_Windows,

    OsType_Count
} OsType;

typedef enum SystemType
{
    SystemType_LittleEndian = 0,
    SystemType_BigEndian
} SystemType;

typedef enum FloatType
{
    FloatType_32Bit = 0,
    FloatType_64Bit
} FloatType;

typedef enum BuildConfigType
{
    BuildConfigType_Debug,
    BuildConfigType_Release
} BuildConfigType;

void environment_();

#endif // SIMPLE_INFO_H
