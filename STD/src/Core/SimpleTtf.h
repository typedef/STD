#ifndef SIMPLE_TTF_H
#define SIMPLE_TTF_H


typedef struct SimpleTtfSettings
{
    const char* pPath;
} SimpleTtfSettings;

void sttf_parse(SimpleTtfSettings set);

#endif // SIMPLE_TTF_H
