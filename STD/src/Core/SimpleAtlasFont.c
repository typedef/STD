#include "SimpleAtlasFont.h"

#include <Deps/stb_truetype.h>
#include <Deps/stb_image.h>
#include <Deps/stb_image_write.h>
#include <math.h>

#include <Core/SimpleStandardLibrary.h>


#define c32(c) ((i32)(c))

static i32* simple_font_get_range(i32 rangeMin, i32 rangeMax);
static i32 simple_font_make_bitmap(unsigned char* pData, i32 offset, f32 pixel_height, unsigned char* pBitmap, i32 bitmapWidth, i32 bitmapHeight, SafCharTable** phCharTable, i32* aRanges);

SafFont
saf_font_create(SafFontSettings set)
{
    const i32 c_print_debug_info = 0;

    char* pFileContent = file_read_string(set.pPath);

    i32* aSymbolsRange = simple_font_get_range(set.RangeMin, set.RangeMax);
    i64 symbolsCount = sva_count(aSymbolsRange);

    i32 half = symbolsCount / 2;
    i32 otherHalf = symbolsCount - half;

    //slog_info("c:%d h:%d oh:%d\n", symbolsCount, half, otherHalf);

    i32 slices = sqrt(symbolsCount) + 1;

    const f32 c_bitmap_width  = slices * set.FontSize;
    const f32 c_bitmap_height = slices * set.FontSize;
    void* pBitmap = memory_allocate(c_bitmap_width * c_bitmap_height*4);

    if (c_print_debug_info)
    {
	slog_warn("Pixel dim for bitmap is %0.0fx%0.0f\n", c_bitmap_width, c_bitmap_height);
    }

    stbtt_bakedchar* aCharDatas = NULL;
    SafCharTable* hCharTable = NULL;

    i32 result = simple_font_make_bitmap(
	(unsigned char*) pFileContent,
	0,
	set.FontSize,
	pBitmap, c_bitmap_width, c_bitmap_height,
	&hCharTable,
	aSymbolsRange);

    sva_free(aSymbolsRange);

    // DOCS: 1 byte r -> 4 byte rgba
    void* pRgbaBitmap;
    if (1)
    {
	pRgbaBitmap = memory_allocate(c_bitmap_width * c_bitmap_height * 4);
	i32 width = c_bitmap_width;
	i32 height = c_bitmap_height;

	struct Rgba
	{
	    u8 R;
	    u8 G;
	    u8 B;
	    u8 A;
	};

	struct R
	{
	    u8 R;
	};

	struct R* pRead = pBitmap;
	struct Rgba* pWrite = pRgbaBitmap;

	for (i32 y = 0; y < height; ++y)
	{
	    for (i32 x = 0; x < width; ++x)
	    {
		u8 r = pRead->R;

		if (r == 0)
		{
		    pWrite->R = 0;
		    pWrite->G = 0;
		    pWrite->B = 0;
		    pWrite->A = 0;
		}
		else
		{
		    //GINFO("rgb (%d %d %d)\n", r, g, b);
		    pWrite->R = r;
		    pWrite->G = r;
		    pWrite->B = r;
		    pWrite->A = r;
		}

		++pWrite;
		++pRead;
	    }
	}
    }

    if (result <= 0)
    {
	slog_error("Problem with font %s\n", set.pPath);
	vguard(0);
    }

    char buf[1024] = {};
    snprintf(buf, 1024, "%s-%d-r-%d-%d.png", set.pPath, set.FontSize, set.RangeMin, set.RangeMax);
    // todo: try to make the bitmap transparent
    stbi_write_png(buf, c_bitmap_width, c_bitmap_height, 4, pRgbaBitmap, c_bitmap_width*4);

    memory_free(pBitmap);

    SafFont safFont = {
	// DOCS: Font name
	.pName = (char*) set.pPath,
	// DOCS: Atlas .png to save
	.pAtlasPath = (char*) string(buf),
	// DOCS: Characters table
	.hCharTable = hCharTable,
	// DOCS: Bitmap to put inside image
	.pBitmap = pRgbaBitmap,
	// DOCS: Bitmap size
	.Size = { c_bitmap_width, c_bitmap_height },
	// DOCS: Font size in pixel 32px
	.FontSize = set.FontSize,

	.RangeMin = set.RangeMin,
	.RangeMax = set.RangeMax,

	// DOCS: Max font character height
	//i32 MaxHeight;
	// note: not sure we need this
	//i32 MaxYOffset;
    };

    return safFont;
}

void
saf_font_destroy(SafFont safFont)
{
    memory_free(safFont.pBitmap);
    table_free(safFont.hCharTable);
}

static i32*
simple_font_get_range(i32 rangeMin, i32 rangeMax)
{
    i32* aSymbols = NULL;

    for (i32 c = rangeMin; c <= rangeMax; ++c)
	sva_add(aSymbols, c);
    sva_add(aSymbols, 32);

    return aSymbols;
}

static i32
simple_font_make_bitmap(unsigned char* pData,
			i32 offset,  /* DOCS: font location (use offset=0 for plain .ttf) */
			f32 pixel_height,                     // height of font in pixels
			unsigned char* pBitmap, i32 bitmapWidth, i32 bitmapHeight,  // bitmap to be filled in
			SafCharTable** phCharTable,

			i32* aRanges
    )
{
    SafCharTable* hCharTable = NULL;

    int x, y, bottom_y;

    stbtt_fontinfo f;
    f.userdata = NULL;

    if (!stbtt_InitFont(&f, pData, offset))
	return 0;
    //STBTT_memset(pixels, 0, pw*ph); // background of 0 around pixels

    memset(pBitmap, 0, bitmapWidth*bitmapHeight);

    x=y=1;
    bottom_y = 1;

    f32 scale = stbtt_ScaleForPixelHeight(&f, pixel_height);

    i64 i;
    i64 rangesCount = sva_count(aRanges);

    for (i = 0; i < rangesCount; ++i)
    {
	i32 unicode = aRanges[i];

	int advance, lsb, x0,y0,x1,y1,gw,gh;
	int g = stbtt_FindGlyphIndex(&f, unicode);

	if (g == 0 && unicode == 32)
	{
	    slog_error("No space char!!!\n");
	    vguard(0);
	}

	if (g == 0)
	    continue;

	stbtt_GetGlyphHMetrics(&f, g, &advance, &lsb);
	stbtt_GetGlyphBitmapBox(&f, g, scale,scale, &x0,&y0,&x1,&y1);
	gw = x1-x0;
	gh = y1-y0;
	if (x + gw + 1 >= bitmapWidth)
	    y = bottom_y, x = 1; // advance to next row
	if (y + gh + 1 >= bitmapHeight) // check if it fits vertically AFTER potentially moving to next row
	    return -i;
	// STBTT_assert(x+gw < pw);
	// STBTT_assert(y+gh < ph);

	stbtt_MakeGlyphBitmap(&f, pBitmap+x+y*bitmapWidth, gw,gh,bitmapWidth, scale,scale, g);

	SafChar safChar = {
	    .Codepoint = unicode,

	    .X0 = (u16) x,
	    .Y0 = (u16) y,
	    .X1 = (u16) (x + gw),
	    .Y1 = (u16) (y + gh),

	    .Xadvance = scale * advance,
	    .Xoff = (f32) x0,
	    .Yoff = (f32) y0,
	};
	hash_put(hCharTable, unicode, safChar);

	x = x + gw + 1;

	if (y+gh+1 > bottom_y)
	    bottom_y = y+gh+1;
    }

    *phCharTable = hCharTable;

    return bottom_y;
}

v2
saf_font_get_space_metrics(SafFont safFont)
{
    SafChar safChar = hash_get(safFont.hCharTable, safFont.RangeMin + 1);
    return (v2) { safChar.Xadvance, to_f32(safChar.Y1 - safChar.Y0) };
}

v2
_get_metrics(SafFont safFont, i32* aUnicodeText, i64 textLength)
{
    vguard_not_null(aUnicodeText);

    f32 width  = 0.0f;
    f32 height = 0.0f;
    const i32 is_newline_char = (i32)'\n';
    const i32 is_whitespace_char = (i32)' ';

    for (i32 i = 0; i < textLength; ++i)
    {
	i32 wc = aUnicodeText[i];

	SafChar safChar = hash_get(safFont.hCharTable, (i32)wc);
	i32 tableInd = table_index(safFont.hCharTable);

	if (wc == is_newline_char)
	{
	    //height += safChar.->MaxHeight + value.Offset.Y;
	}
	else if (wc != is_whitespace_char)
	{
	    if (tableInd == -1)
	    {
		wchar_t charBuf[2] = {
		    [0] = wc,
		    [1] = '\0'
		};
		GERROR("Can't find %d symbol in hash table!\n", (i32) wc);
		GERROR("i: %d ind: %d wci: %d char: %ls\n", i, tableInd, wc, charBuf);
		vguard(tableInd != -1 && "No char!");
	    }

#if 1
	    width += safChar.Xadvance;
#else
	    width += safChar.X1 - safChar.X0;
#endif

	    f32 cHeight = (safChar.Y1 - safChar.Y0);
	    height = Max(height, cHeight);
	}
	else
	{
	    v2 spaceMetrics = saf_font_get_space_metrics(safFont);
	    width  += spaceMetrics.X;
	}
    }

    v2 metrics = {width, height};

    return metrics;
}

v2
saf_font_get_metrics(SafFont safFont, char* pText)
{
    i64 textLength = simple_core_utf8_length(pText);

    i32* aUnicodeText = memory_allocate(textLength * sizeof(i32));
    simple_core_utf8_get_unicode_text(aUnicodeText, pText, &textLength);

    v2 metrics = _get_metrics(safFont, aUnicodeText, textLength);

    memory_free(aUnicodeText);

    return metrics;
}
