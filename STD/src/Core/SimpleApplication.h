#ifndef SIMPLE_APPLICATION_H
#define SIMPLE_APPLICATION_H

#include <Core/Types.h>


struct SwlEvent;
struct SwlWindow;

typedef void (*LayerAction)();
typedef void (*LayerFunction)(struct SwlEvent* pEvent);

typedef struct SimpleLayer
{
    i8 IsActive;
    char* pName;

    LayerAction OnAttach;
    LayerAction OnUpdate;
    LayerAction OnBeforeUpdate;
    LayerAction OnAfterUpdate;
    LayerFunction OnEvent;
    LayerAction OnUi;
    LayerAction OnDestroy;
} SimpleLayer;

typedef enum SimpleScreenMode
{
    SimpleScreenMode_Custom = 0,
    SimpleScreenMode_F11,
    SimpleScreenMode_FullScreen,
} SimpleScreenMode;

typedef struct SimpleApplicationSettings
{
    const char* pName;

    i32 ArgsCount;
    char** aArgs;

    i8 IsDebug;
    i8 IsVsync;
    SimpleScreenMode ScreenMode;
    v2 Size;
    v2 MinSize;
    v2 Position;
} SimpleApplicationSettings;

typedef struct SimpleRuntimeStats
{
    // DOCS(typedef): Frame time (Timestep) related
    f64 Timestep;

    // DOCS(typedef): Second Counter
    f32 SecondElapsed;
    i32 Fps;

    // DOCS(typedef): Start Date as seconds
    i64 StartedAt;

    // DOCS(typedef): Ui updates related
    u8 UiFpsSyncRatio;
    u8 FpsBeforeUiUpdate;
} SimpleRuntimeStats;

void simple_application_create(SimpleApplicationSettings* pSettings);
void simple_application_destroy();
void simple_application_add_layer(SimpleLayer newLayer);
void simple_application_run();

SimpleLayer* simple_application_get_layers();
SimpleRuntimeStats simple_application_get_stats();
struct SwlWindow* simple_application_get_window();
LayerAction* simple_application_get_uis();

#endif // SIMPLE_APPLICATION_H
