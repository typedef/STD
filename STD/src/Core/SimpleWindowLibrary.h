#ifndef SIMPLE_WINDOW_LIBRARY_H
#define SIMPLE_WINDOW_LIBRARY_H

// temporart, ll be removed as soon as i finish base layer
#include <stdio.h>
#include <stdlib.h>

// todo: get rid of preprocessors

/*
  DOCS:
  Swl - lib abstract native window creation mechanism. Xlib and Win64 api abstraction for window/input/software rendering behaviour

  For usage (inside implementation/Engine.h):
  #define SWL_X11_BACKEND 1
  #define SWL_IMPLEMENTATION
  #include "SimpleWindowLibrary.h"

  For opengl renderer
  #define SWL_RENDERER


  Use swl_ prefix for function.
  Swl for type


  extern void swl_init();

  DOCS: Create SwlWindow with concrete set
  SwlWindow swl_window_create(SwlWindowSettings set);

  DOCS: Set window above other, work on linux, windows wm is stupid.
  void swl_window_set_above(SwlWindow* pWindow);

  DOCS: Window occupy all space except 'taskbar'.
  void swl_window_set_maximized(SwlWindow* pWindow);

  DOCS: Window occupy all space.
  void swl_window_set_fullscreen(SwlWindow* pWindow);

  DOCS: Remove header from window.
  void swl_window_set_wo_header(SwlWindow* pWindow);

  DOCS: Set background color using r,g,b as value from 0 to 255.
  void swl_window_set_background(SwlWindow* pWindow, swl_u8 r255, swl_u8 g255, swl_u8 b255);


*/

//DOCS: Default flags, enabled by default
#if !defined(SWL_DEBUG)
 #define SWL_DEBUG 1
#endif

#if !defined(SWL_X11_BACKEND)
 #define SWL_X11_BACKEND 0
#endif

#if !defined(SWL_XCB_BACKEND)
 #define SWL_XCB_BACKEND 0
#endif

#if !defined(SWL_WIN64_BACKEND)
 #define SWL_WIN64_BACKEND 0
#endif

#if defined(SWL_X11_BACKEND) || defined(SWL_XCB_BACKEND)
 #define SWL_LINUX 1
#elif defined(SWL_WIN64_BACKEND)
 #define SWL_WINDOWS 1
#else
 #error "Platform not supported!"
#endif

#ifndef swl_i8
 #define swl_i8 char
#endif

#ifndef swl_i16
 #define swl_i16 short
#endif

#ifndef swl_i32
 #define swl_i32 int
#endif

#ifndef swl_i64
 #define swl_i64 long long int
#endif

#ifndef swl_u8
 #define swl_u8 unsigned char
#endif

#ifndef swl_u32
#define swl_u32 unsigned int
#endif

#ifndef swl_u64
 #if defined(PLATFORM_LINUX)
  #define swl_u64 unsigned long
 #elif defined(PLATFORM_WINDOWS)
  #define swl_u64 unsigned long long
 #endif
#endif // swl_u64

#ifndef swl_f32
#define swl_f32 float
#endif

#ifndef swl_f64
#define swl_f64 double
#endif

#ifndef swl_size
#define swl_size swl_u64
#endif

     /*
       ###################################
       ###################################
       SWL_DECLARATION
       ###################################
       ###################################
     */

     //////                               //
     //               Input               //
     //                               //////

typedef enum SwlKey
{
    SwlKey_None = -1,

    // DOCS: Numbers
    SwlKey_0 = 0,
    SwlKey_1 = 1,
    SwlKey_2 = 2,
    SwlKey_3 = 3,
    SwlKey_4 = 4,
    SwlKey_5 = 5,
    SwlKey_6 = 6,
    SwlKey_7 = 7,
    SwlKey_8 = 8,
    SwlKey_9 = 9,

    // DOCS: Numpad Numbers
    SwlKey_NP_0 = 10,
    SwlKey_NP_1 = 11,
    SwlKey_NP_2 = 12,
    SwlKey_NP_3 = 13,
    SwlKey_NP_4 = 14,
    SwlKey_NP_5 = 15,
    SwlKey_NP_6 = 16,
    SwlKey_NP_7 = 17,
    SwlKey_NP_8 = 18,
    SwlKey_NP_9 = 19,

    // DOCS: Numpad Keys
    SwlKey_NP_Lock = 20,
    SwlKey_NP_Div = 21,
    SwlKey_NP_Mul = 22,
    SwlKey_NP_Min = 23,
    SwlKey_NP_Add = 24,
    SwlKey_NP_Enter = 25,
    SwlKey_NP_Comma = 26,

    // DOCS: Upper right panel
    SwlKey_PrintScreen = 27,
    SwlKey_ScrollLock = 28,
    SwlKey_PauseBreak = 29,

    // DOCS: Mid right panel
    SwlKey_Insert = 30,
    SwlKey_Delete = 31,
    SwlKey_Home = 32,
    SwlKey_End = 33,
    SwlKey_PageUp = 34,
    SwlKey_PageDown = 35,
    SwlKey_CapsLock = 36,

    // DOCS: Lower right panel
    SwlKey_Up = 37,
    SwlKey_Left = 38,
    SwlKey_Down = 39,
    SwlKey_Right = 40,

    // DOCS: Characters

    SwlKey_Q = 41,
    SwlKey_W = 42,
    SwlKey_E = 43,
    SwlKey_R = 44,
    SwlKey_T = 45,
    SwlKey_Y = 46,
    SwlKey_U = 47,
    SwlKey_I = 48,
    SwlKey_O = 49,
    SwlKey_P = 50,

    SwlKey_A = 51,
    SwlKey_S = 52,
    SwlKey_D = 53,
    SwlKey_F = 54,
    SwlKey_G = 55,
    SwlKey_H = 56,
    SwlKey_J = 57,
    SwlKey_K = 58,
    SwlKey_L = 59,

    SwlKey_Z = 60,
    SwlKey_X = 61,
    SwlKey_C = 62,
    SwlKey_V = 63,
    SwlKey_B = 64,
    SwlKey_N = 65,
    SwlKey_M = 66,

    // DOCS: Functional
    SwlKey_Grave = 67,  /* ' */
    SwlKey_Min = 68,
    SwlKey_Equal = 69, // +/=
    SwlKey_Tab = 70,
    SwlKey_Shift = 71,
    SwlKey_Ctrl = 72,
    SwlKey_Alt = 73,
    SwlKey_Super = 74,
    SwlKey_Space = 75,

    SwlKey_Comma = 76,
    SwlKey_Dot = 77,
    SwlKey_Slash = 78, /*/*/

    SwlKey_Shift_Right = 79,
    SwlKey_Alt_Right = 80,
    SwlKey_Ctrl_Right = 81,
    SwlKey_Super_Right = 82,
    SwlKey_Command_Right = 83,

    SwlKey_Escape = 84,

    // DOCS: Press F
    SwlKey_F1  = 85,
    SwlKey_F2  = 86,
    SwlKey_F3  = 87,
    SwlKey_F4  = 88,
    SwlKey_F5  = 89,
    SwlKey_F6  = 90,
    SwlKey_F7  = 91,
    SwlKey_F8  = 92,
    SwlKey_F9  = 93,
    SwlKey_F10 = 94,
    SwlKey_F11 = 95,
    SwlKey_F12 = 96,

    // DOCS: Extra
    SwlKey_Bracket_Left = 97,  /* [ */
    SwlKey_Bracket_Right = 98,  /* ] */
    SwlKey_Semicolon = 99,  /* ; */
    SwlKey_Apostrophe = 100,  /* ' */
    SwlKey_Backslash = 101,  /* \ */
    SwlKey_Enter = 102,

    SwlKey_F13 = 103,
    SwlKey_F14 = 104,
    SwlKey_F15 = 105,
    SwlKey_F16 = 106,
    SwlKey_F17 = 107,
    SwlKey_F18 = 108,
    SwlKey_F19 = 109,
    SwlKey_F20 = 110,
    SwlKey_F21 = 111,
    SwlKey_F22 = 112,
    SwlKey_F23 = 113,
    SwlKey_F24 = 114,
    SwlKey_F25 = 115,

    SwlKey_Backspace = 116,
    SwlKey_Menu,

    SwlKey_Undefined,
    SwlKey_Count
} SwlKey;

swl_i32 swl_key_is_printable(SwlKey key);
const char* swl_key_to_string(SwlKey swlKey);

typedef enum SwlMouseKey
{
    SwlMouseKey_Left = 0,
    SwlMouseKey_Right = 1,
    SwlMouseKey_Middle = 2,

    SwlMouseKey_Ext_0,
    SwlMouseKey_Ext_1,
    SwlMouseKey_Ext_2,
    SwlMouseKey_Ext_3,
    SwlMouseKey_Ext_4,
    SwlMouseKey_Ext_5,
    SwlMouseKey_Ext_6,
    SwlMouseKey_Ext_7,
    SwlMouseKey_Ext_8,
    SwlMouseKey_Ext_9,

    SwlMouseKey_Count,
} SwlMouseKey;

typedef enum SwlKeyMod
{
    SwlKeyMod_None = 0,

    SwlKeyMod_Alt = 1 << 0,
    SwlKeyMod_Super = 1 << 1,
    SwlKeyMod_Ctrl = 1 << 2,
    SwlKeyMod_Shift = 1 << 3,
    SwlKeyMod_CapsLock = 1 << 4,
    SwlKeyMod_NumLock = 1 << 5,

    SwlKeyMod_Count,
} SwlKeyMod;

typedef enum SwlCursorMode
{
    SwlCursorMode_Normal = 0,
    SwlCursorMode_Hidden = 1,
    SwlCursorMode_Locked = 2,
    SwlCursorMode_Count
} SwlCursorMode;

typedef enum SwlAction
{
    SwlAction_Press = 0,
    SwlAction_Release = 1,
    SwlAction_Repeat = 2,
    SwlAction_Count = 2,
} SwlAction;


//////                               //
//              Window               //
//                               //////

typedef struct swl_v2
{
    swl_f32 X;
    swl_f32 Y;
} swl_v2;

typedef struct swl_v3
{
    swl_f32 X;
    swl_f32 Y;
    swl_f32 Z;
} swl_v3;

typedef struct swl_v4
{
    swl_f32 R;
    swl_f32 G;
    swl_f32 B;
    swl_f32 A;
} swl_v4;

typedef struct SwlSize
{
    union {
	struct {
	    swl_i32 Width;
	    swl_i32 Height;
	};
	swl_v2 Default;
    };

    swl_i32 MinWidth;
    swl_i32 MinHeight;

    swl_i32 MaxWidth;
    swl_i32 MaxHeight;
} SwlSize;

typedef struct SwlPosition
{
    swl_i32 X;
    swl_i32 Y;
} SwlPosition;

typedef struct SwlWindowSettings
{
    const char* pName;
    swl_i32 NameLength;
    SwlSize Size;
    SwlPosition Position;
} SwlWindowSettings;


typedef enum SwlEventType
{
    SwlEventType_None = 0,

    SwlEventType_KeyType,
    SwlEventType_KeyPress,
    SwlEventType_KeyRelease,
    SwlEventType_MousePress,
    SwlEventType_MouseRelease,

    SwlEventType_MouseMove,
    SwlEventType_MouseScroll,

    SwlEventType_WindowMoved,
    SwlEventType_WindowResized,

    SwlEventType_WindowMaximized,
    SwlEventType_WindowFocused,
    SwlEventType_WindowEnter,
    SwlEventType_WindowHidden,

    SwlEventType_Count,
} SwlEventType;

typedef struct SwlEvent
{
    swl_i32 IsHandled;
    SwlEventType Type;
} SwlEvent;

typedef struct SwlWindowFocusedEvent
{
    SwlEvent Base;
    swl_i32 Focused;
} SwlWindowFocusedEvent;

typedef struct SwlWindowEnterEvent
{
    SwlEvent Base;
    swl_i32 IsInside;
} SwlWindowEnterEvent;

typedef struct SwlWindowMaximizedEvent
{
    SwlEvent Base;
} SwlWindowMaximizedEvent;

typedef struct SwlWindowHiddenEvent
{
    SwlEvent Base;
} SwlWindowHiddenEvent;

typedef struct SwlKeyPressEvent
{
    SwlEvent Base;
    SwlKey Key;
    SwlKeyMod KeyMod;
} SwlKeyPressEvent;

typedef struct SwlKeyTypeEvent
{
    SwlEvent Base;
    swl_u32 CodePoint;
} SwlKeyTypeEvent;

typedef struct SwlKeyReleaseEvent
{
    SwlEvent Base;
    SwlKey Key;
    SwlKeyMod KeyMod;
} SwlKeyReleaseEvent;

typedef struct SwlMousePressEvent
{
    SwlEvent Base;
    SwlMouseKey Key;
} SwlMousePressEvent;

typedef struct SwlMouseScrollEvent
{
    SwlEvent Base;
    swl_i32 Value;
} SwlMouseScrollEvent;

typedef struct SwlMouseReleaseEvent
{
    SwlEvent Base;
    SwlMouseKey Key;
} SwlMouseReleaseEvent;

typedef struct SwlMouseMoveEvent
{
    SwlEvent Base;
    swl_i32 X;
    swl_i32 Y;
} SwlMouseMoveEvent;

typedef struct SwlWindowResizeEvent
{
    SwlEvent Base;
    swl_i32 Width;
    swl_i32 Height;
} SwlWindowResizeEvent;

typedef struct SwlWindowPositionEvent
{
    SwlEvent Base;
    swl_i32 X;
    swl_i32 Y;
} SwlWindowPositionEvent;

typedef struct SwlCall
{
    void (*OnEvent)(SwlEvent*);
} SwlCall;

typedef struct SwlFontSettings
{
    const char* pFontPath;
    swl_size FontSize;
    swl_f32 Padding;
    swl_f32 OnEdgeValue;
    swl_f32 Width;
    swl_f32 EdgeTransition;
    swl_v2 BitmapSize;
    swl_i32* aCodePoints;
    swl_size CodePointsCount;
} SwlFontSettings;

#endif // SWL_RENDERER

typedef struct SwlResolution
{
    swl_i32 Width;
    swl_i32 Height;
} SwlResolution;

typedef struct SwlResolutions
{
    swl_i32 Count;
    SwlResolution* aResolutions;
} SwlResolutions;

typedef struct SwlWindow
{
    const char* pName;
    SwlSize Size;
    SwlPosition Position;

    SwlCall Call;
    swl_i8 IsAlive;
    swl_i8 IsMaximized;
    swl_i8 IsFullscreen;
    SwlAction aKeys[SwlKey_Count];
    SwlAction aMouseKeys[SwlMouseKey_Count];
    void* pBackend;
} SwlWindow;

// todo: SwlInitSettings wo IsInitialized

typedef struct SwlInit
{
    swl_i32 IsInitialized;
    void* (*MemoryAllocate)(swl_size size);
    void (*MemoryFree)(void* pData);
} SwlInit;

void swl_init(SwlInit swlInit);

void swl_window_create(SwlWindow* pSwlWindow, SwlWindowSettings set);
void swl_window_destroy(SwlWindow* pWindow);
void swl_window_update(SwlWindow* pWindow);

SwlResolutions swl_window_get_resolutions(SwlWindow* pWindow);
SwlResolution swl_window_get_resolution(SwlWindow* pWindow);
swl_v2 swl_window_get_cursor_position(SwlWindow* pWindow);

void swl_window_set_event_call(SwlWindow* pWindow, void (*onEvent)(SwlEvent*));

/*
  DOCS: Set window above other, work on linux, windows wm is stupid
*/
void swl_window_set_above(SwlWindow* pWindow);
void swl_window_set_maximized(SwlWindow* pWindow);
void swl_window_set_fullscreen(SwlWindow* pWindow);
/*
  DOCS: Remove header from window
*/
void swl_window_set_wo_header(SwlWindow* pWindow);
/*
  DOCS: Set background color using r,g,b as value from 0 to 255.
*/
void swl_window_set_background(SwlWindow* pWindow, swl_u8 r255, swl_u8 g255, swl_u8 b255);

/*
  ###################################
  ###################################
  SWL_IMPLEMENTATION
  ###################################
  ###################################
*/

#if defined(SWL_IMPLEMENTATION)

// NOTE: Declare every preproc inside .c file,
// keep it local for object file
#define swl_assert(cond) ({ if (!cond) { int* i=NULL; *i = 1; } })
#define SwlMinMax(v, min, max) ({(v >= min && v <= max) ? v : (v < min) ? min : max; })
#define SwlNull ((void*)0)

#if SWL_DEBUG == 1
#define swl_init_was_called_check() ({ if (gSwlInit.IsInitialized != 1){swl_assert(0);}})
#else
#define swl_init_was_called_check()
#endif

typedef struct SwlToDescTuple
{
    SwlKey Key;
    const char* pName;
} SwlToDescTuple;

swl_i32
swl_key_is_printable(SwlKey key)
{
    static SwlKey checkTable[SwlKey_Count] = {};
    static swl_i32 isNotInit = 1;
    if (isNotInit)
    {
	for (swl_i32 i = SwlKey_0; i <= SwlKey_9; ++i)
	    checkTable[i] = 1;

	for (swl_i32 i = SwlKey_NP_0; i <= SwlKey_NP_9; ++i)
	    checkTable[i] = 1;

	for (swl_i32 i = SwlKey_Q; i <= SwlKey_M; ++i)
	    checkTable[i] = 1;

	checkTable[SwlKey_NP_Div] = 1;
	checkTable[SwlKey_NP_Mul] = 1;
	checkTable[SwlKey_NP_Min] = 1;
	checkTable[SwlKey_NP_Add] = 1;
	checkTable[SwlKey_NP_Comma] = 1;

	checkTable[SwlKey_Grave] = 1;
	checkTable[SwlKey_Min] = 1;
	checkTable[SwlKey_Equal] = 1;
	checkTable[SwlKey_Comma] = 1;
	checkTable[SwlKey_Dot] = 1;
	checkTable[SwlKey_Slash] = 1;
	checkTable[SwlKey_Bracket_Left] = 1;
	checkTable[SwlKey_Bracket_Right] = 1;
	checkTable[SwlKey_Semicolon] = 1;
	checkTable[SwlKey_Apostrophe] = 1;
	checkTable[SwlKey_Backslash] = 1;

	isNotInit = 0;
    }

    swl_i32 isPrintable = checkTable[key];
    return isPrintable;
}

const char*
swl_key_to_string(SwlKey swlKey)
{
    switch (swlKey)
    {

    case SwlKey_None: return "None";

	// DOCS: Numbers
    case SwlKey_0: return "0";
    case SwlKey_1: return "1";
    case SwlKey_2: return "2";
    case SwlKey_3: return "3";
    case SwlKey_4: return "4";
    case SwlKey_5: return "5";
    case SwlKey_6: return "6";
    case SwlKey_7: return "7";
    case SwlKey_8: return "8";
    case SwlKey_9: return "9";

	// DOCS: Numpad Numbers
    case SwlKey_NP_0: return "NP_0";
    case SwlKey_NP_1: return "NP_1";
    case SwlKey_NP_2: return "NP_2";
    case SwlKey_NP_3: return "NP_3";
    case SwlKey_NP_4: return "NP_4";
    case SwlKey_NP_5: return "NP_5";
    case SwlKey_NP_6: return "NP_6";
    case SwlKey_NP_7: return "NP_7";
    case SwlKey_NP_8: return "NP_8";
    case SwlKey_NP_9: return "NP_9";

	// DOCS: Numpad Keys
    case SwlKey_NP_Lock:  return "NP_Lock";
    case SwlKey_NP_Div:   return "NP_Div";
    case SwlKey_NP_Mul:   return "NP_Mul";
    case SwlKey_NP_Min:   return "NP_Min";
    case SwlKey_NP_Add:   return "NP_Add";
    case SwlKey_NP_Enter: return "NP_Enter";
    case SwlKey_NP_Comma: return "NP_Comma";

	// DOCS: Upper right panel
    case SwlKey_PrintScreen: return "PrintScreen";
    case SwlKey_ScrollLock:  return "ScrollLock";
    case SwlKey_PauseBreak:  return "PauseBreak";

	// DOCS: Mid right panel
    case SwlKey_Insert:   return "Insert";
    case SwlKey_Delete:   return "Delete";
    case SwlKey_Home:     return "Home";
    case SwlKey_End:      return "End";
    case SwlKey_PageUp:   return "PageUp";
    case SwlKey_PageDown: return "PageDown";
    case SwlKey_CapsLock: return "CapsLock";

	// DOCS: Lower right panel
    case SwlKey_Up:    return "Up";
    case SwlKey_Left:  return "Left";
    case SwlKey_Down:  return "Down";
    case SwlKey_Right: return "Right";

	// DOCS: Characters

    case SwlKey_Q: return "Q";
    case SwlKey_W: return "W";
    case SwlKey_E: return "E";
    case SwlKey_R: return "R";
    case SwlKey_T: return "T";
    case SwlKey_Y: return "Y";
    case SwlKey_U: return "U";
    case SwlKey_I: return "I";
    case SwlKey_O: return "O";
    case SwlKey_P: return "P";

    case SwlKey_A: return "A";
    case SwlKey_S: return "S";
    case SwlKey_D: return "D";
    case SwlKey_F: return "F";
    case SwlKey_G: return "G";
    case SwlKey_H: return "H";
    case SwlKey_J: return "J";
    case SwlKey_K: return "K";
    case SwlKey_L: return "L";

    case SwlKey_Z: return "Z";
    case SwlKey_X: return "X";
    case SwlKey_C: return "C";
    case SwlKey_V: return "V";
    case SwlKey_B: return "B";
    case SwlKey_N: return "N";
    case SwlKey_M: return "M";

	// DOCS: Functional
    case SwlKey_Grave: return "Grave";
    case SwlKey_Min:   return "Min";
    case SwlKey_Equal: return "Equal";
    case SwlKey_Tab:   return "Tab";
    case SwlKey_Shift: return "Shift";
    case SwlKey_Ctrl:  return "Ctrl";
    case SwlKey_Alt:   return "Alt";
    case SwlKey_Super: return "Super";
    case SwlKey_Space: return "Space";

    case SwlKey_Comma: return "Comma";
    case SwlKey_Dot:   return "Dot";
    case SwlKey_Slash: return "Slash";

    case SwlKey_Shift_Right:   return "Shift_Right";
    case SwlKey_Alt_Right:     return "Alt_Right";
    case SwlKey_Ctrl_Right:    return "Ctrl_Right";
    case SwlKey_Super_Right:   return "Super_Right";
    case SwlKey_Command_Right: return "Command_Right";

    case SwlKey_Escape: return "Escape";

	// DOCS: Press F
    case SwlKey_F1 : return "F1";
    case SwlKey_F2 : return "F2";
    case SwlKey_F3 : return "F3";
    case SwlKey_F4 : return "F4";
    case SwlKey_F5 : return "F5";
    case SwlKey_F6 : return "F6";
    case SwlKey_F7 : return "F7";
    case SwlKey_F8 : return "F8";
    case SwlKey_F9 : return "F9";
    case SwlKey_F10: return "F10";
    case SwlKey_F11: return "F11";
    case SwlKey_F12: return "F12";

	// DOCS: Extra
    case SwlKey_Bracket_Left:  return "Bracket_Left";
    case SwlKey_Bracket_Right: return "Bracket_Right";
    case SwlKey_Semicolon:     return "Semicolon";
    case SwlKey_Apostrophe:    return "Apostrophe";
    case SwlKey_Backslash:     return "Backslash";
    case SwlKey_Enter:         return "Enter";

    case SwlKey_F13: return "F13";
    case SwlKey_F14: return "F14";
    case SwlKey_F15: return "F15";
    case SwlKey_F16: return "F16";
    case SwlKey_F17: return "F17";
    case SwlKey_F18: return "F18";
    case SwlKey_F19: return "F19";
    case SwlKey_F20: return "F20";
    case SwlKey_F21: return "F21";
    case SwlKey_F22: return "F22";
    case SwlKey_F23: return "F23";
    case SwlKey_F24: return "F24";
    case SwlKey_F25: return "F25";

    case SwlKey_Backspace: return "Backspace";
    case SwlKey_Menu     : return "Menu";

    case SwlKey_Count: return "Count";

    default: return "Undefined";
    }

}

void
_swl_handle_resize_event(SwlWindow* pWindow, swl_i32 nw, swl_i32 nh)
{
    swl_i32 w = pWindow->Size.Width;
    swl_i32 h = pWindow->Size.Height;
    swl_i32 isTrue = (w != nw || h != nh);

    if (!isTrue)
	return;

    pWindow->Size.Width  = nw;
    pWindow->Size.Height = nh;
    SwlWindowResizeEvent resizeEvent = {
	.Base = {
	    .IsHandled = 0,
	    .Type = SwlEventType_WindowResized,
	},
	.Width  = nw,
	.Height = nh,
    };

    pWindow->Call.OnEvent(&resizeEvent.Base);
}

void
_swl_handle_reposition_event(SwlWindow* pWindow, swl_i32 nx, swl_i32 ny)
{
    swl_i32 x = pWindow->Position.X;
    swl_i32 y = pWindow->Position.Y;

    swl_i32 isTrue = (x != nx || y != ny);
    if (!isTrue)
	return;

    pWindow->Position.X = nx;
    pWindow->Position.Y = ny;

    SwlWindowPositionEvent positionEvent = {
	.Base = {
	    .IsHandled = 0,
	    .Type = SwlEventType_WindowMoved,
	},
	.X = nx,
	.Y = ny,
    };

    pWindow->Call.OnEvent(&positionEvent.Base);
}

void
_swl_focused_event(SwlWindow* pWindow, swl_i32 focused)
{
    SwlWindowFocusedEvent focusedEvent = {
	.Base = {
	    .Type = SwlEventType_WindowFocused
	},
	.Focused = focused
    };

    pWindow->Call.OnEvent((SwlEvent*) &focusedEvent);
}

void
_swl_enter_event(SwlWindow* pWindow, swl_i32 isInside)
{
    SwlWindowEnterEvent enterEvent = {
	.Base = {
	    .Type = SwlEventType_WindowEnter
	},
	.IsInside = isInside,
    };

    pWindow->Call.OnEvent((SwlEvent*) &enterEvent);
}

static SwlInit gSwlInit = {};

static swl_i32
_swl_str_length(const char* pStr)
{
    char* ptr = (char*) pStr;
    char c = *ptr;
    while (c != '\0')
    {
	++ptr;
	c = *ptr;
    }
    swl_i32 len = ptr - pStr;
    return len;
}

static swl_i32
_swl_str_comapre(const char* str0, const char* str1, swl_i32 length)
{
    char* p0 = (char*) str0;
    char* p1 = (char*) str1;

    if (p0 == NULL || p1 == NULL)
	return 0;

    //printf("%.*s %.*s %d\n", length, p0, length, p1, length);

    for (swl_i32 i = 0; i < length; ++i)
    {
	char c0 = p0[i];
	char c1 = p1[i];

	if (c0 == '\0' || c1 == '\0')
	    break;

	if (c0 != c1)
	    return 0;
    }

    return 1;
}

static swl_u32
_swl_utf8_decode(char* pStr, swl_i32* pBytes)
{
    swl_u32 codepoint;

    // DOCS: fb - first byte
    char* pInput = pStr;
    char fb = *pInput;

    if ((fb >> 7) == 0)
    {
	codepoint = fb;
	*pBytes = 1;
    }
    else if (((fb & 0xF0) == 0xF0)) // 4 byte
    {
	codepoint  = ((pInput[0]) & 0x07) << 18;
	codepoint |= ((pInput[1]) & 0x3F) << 12;
	codepoint |= ((pInput[2]) & 0x3F) << 6;
	codepoint |= ((pInput[3]) & 0x3F);
	*pBytes = 4;
    }
    else if ((fb & 0xE0) == 0xE0) // 3 byte
    {
	codepoint  = ((pInput[0]) & 0x0F) << 12;
	codepoint |= ((pInput[1]) & 0x3F) << 6 ;
	codepoint |= ((pInput[2]) & 0x3F);
	*pBytes = 3;
    }
    else if ((fb & 0xC0) == 0xC0) // 2 byte
    {
	codepoint  = ((pInput[0]) & 0x1F) << 6;
	codepoint |= ((pInput[1]) & 0x3F);
	*pBytes = 2;
    }

    return codepoint;
}

void
swl_init(SwlInit swlInit)
{
    swl_i32 backendSum = SWL_X11_BACKEND + SWL_XCB_BACKEND + SWL_WIN64_BACKEND;

    if (backendSum == 0)
    {
	printf("No back-end defined!\n");
	swl_assert(0);
    }
    else if (backendSum != 1)
    {
	printf("Defined more then one backend!\n");
	swl_assert(0);
    }

    swl_assert(swlInit.MemoryAllocate);
    swl_assert(swlInit.MemoryFree);

    // Enable later on
    // XInitThreads();

    gSwlInit = swlInit;
    gSwlInit.IsInitialized = 1;
}

void
_swl_position_first_or_default(SwlPosition position, swl_i32* pX, swl_i32* pY)
{
    swl_i32 x = position.X;
    if (x < 0)
	x = 150;
    swl_i32 y = position.Y;
    if (y < 0)
	y = 150;

    *pX = x;
    *pY = y;
}

#if SWL_X11_BACKEND == 1

// DOCS: Declare it only inside SWL_IMPLEMENTATION
#include <X11/Xlib.h>
#include <X11/Xutil.h> // for XSizeHints
#include <X11/XKBlib.h> // for XKB - keyboard layout independent
#include <X11/extensions/Xrandr.h>

typedef struct SwlX11Backend
{
    Display* pDisplay;
    Window Window;
    XIM InputMethod;
    XIC InputContext;

    // DOCS: Atoms
    Atom AtomFocused;
    Atom AtomMaxH;
    Atom AtomMaxV;
    Atom AtomHidden;
    Atom AtomWindowManager;
    Atom AtomDeleteWindow;

    // DOCS: aScanToKeys[scancode_from_x] == SwlKey
    SwlKey* aScanToKeys; // note: allocated
    Time* aKeyPressedTimes;
} SwlX11Backend;

// DOCS: Forward declaration
void _swl_xkb_set_translation_table(SwlX11Backend* pX11Backend);
void _swl_position_first_or_default(SwlPosition position, swl_i32* pX, swl_i32* pY);

void
swl_window_create(SwlWindow* pSwlWindow, SwlWindowSettings set)
{
    swl_init_was_called_check();

    swl_i32 x, y;
    _swl_position_first_or_default(set.Position, &x, &y);

    SwlWindow swlWindow = {
	.IsAlive = 1,
	.Size = set.Size,
	.Position = (SwlPosition) { .X = x, .Y = y },
    };

    Display* pDisplay;
    {
	pDisplay = XOpenDisplay(NULL);
	if (pDisplay == NULL)
	{
	    printf("Display creation error!\n");
	    swl_i32* i = NULL;
	    *i = 0;
	    return;
	}
    }
    swl_i32 defaultScreen = DefaultScreen(pDisplay);
    Window windowRoot = RootWindow(pDisplay, defaultScreen);

    Window window;
    { // DOCS: Create window, setting size

	// todo: extend in future
	swl_u64 attribute_mask = CWEventMask | CWBackPixel | CWBorderPixel;
	swl_u64 event_mask = ExposureMask
	    | ButtonPressMask | ButtonReleaseMask
	    | PointerMotionMask
	    | KeyPressMask | KeyReleaseMask
	    | FocusChangeMask
	    | EnterWindowMask | LeaveWindowMask
	    | VisibilityChangeMask /*It just maximized*/
	    | StructureNotifyMask
	    /* | ResizeRedirectMask  do *NOT* handle it*/
	    | StructureNotifyMask | PropertyChangeMask /* for minify, maximize-hor, max-ver, */
	    ;

	XSetWindowAttributes attributes = {
	    .event_mask = event_mask,
	    .border_pixel = BlackPixel(pDisplay, defaultScreen),
	    .background_pixel = WhitePixel(pDisplay, defaultScreen),
	};

	Visual* pVisual = CopyFromParent;
	swl_i32 someWeirdFlagFromX = InputOutput;
	swl_i32 borderWidth = 1;

	swl_i32 w = set.Size.Width;
	swl_i32 h = set.Size.Height;

	window = XCreateWindow(
	    pDisplay, windowRoot, x, y, w, h,
	    borderWidth, CopyFromParent, someWeirdFlagFromX, pVisual,
	    attribute_mask, &attributes);

	XSizeHints sizeHints = {
	    .x = x,
	    .y = y,
	    .width = w,
	    .height = h,
	    .base_width = w,
	    .base_height = h,
	    .flags = USPosition | USSize | PBaseSize,
	};

	if (set.Size.MinWidth > 0 && set.Size.MinHeight > 0)
	{
	    sizeHints.min_width  = set.Size.MinWidth;
	    sizeHints.min_height = set.Size.MinHeight;
	    sizeHints.flags |= PMinSize;
	}
	if (set.Size.MaxWidth > 0 && set.Size.MaxHeight > 0)
	{
	    sizeHints.max_width  = set.Size.MaxWidth;
	    sizeHints.max_height = set.Size.MaxHeight;
	    sizeHints.flags |= PMaxSize;
	}

	XSetWMNormalHints(pDisplay, window, &sizeHints);
    }

    { // DOCS: Setting name, WM Hints
	char* pWindowName = (char*) set.pName;
	char* pAppClass = "example_class";
	XClassHint xClassHints = {
	    .res_class = pAppClass,
	    .res_name = pWindowName,
	};
	XStoreName(pDisplay, window, pWindowName);
	XSetClassHint(pDisplay, window, &xClassHints);
	Atom netWm = XInternAtom(pDisplay, "_NET_WM_NAME", 0);
	Atom utf8String = XInternAtom(pDisplay, "UTF8_STRING", 0);
	swl_i32 charStringBitsCount = 8;
	XChangeProperty(pDisplay, window, netWm, utf8String, charStringBitsCount, PropModeReplace, (unsigned char*)pWindowName, set.NameLength);

	XWMHints windowManagerHints = {
	    .flags = InputHint | StateHint,
	    .initial_state = NormalState,
	    .input = 1,
	};
	XSetWMHints(pDisplay, window, &windowManagerHints);
    }

    // note: Diplay window
    XMapRaised(pDisplay, window);
    XFlush(pDisplay);

    XIM inputMethod;
    XIC inputContext;
    if (XSupportsLocale())
    {
	XSetLocaleModifiers("");

	inputMethod = XOpenIM(pDisplay, 0, NULL, NULL);

	inputContext = XCreateIC(inputMethod,
				 XNInputStyle,
				 XIMPreeditNothing | XIMStatusNothing,
				 XNClientWindow,
				 window,
				 XNFocusWindow,
				 window,
				 NULL);
    }

    // DOCS: Setup backend
    SwlX11Backend back = {
	.pDisplay = pDisplay,
	.Window = window,
	.InputMethod = inputMethod,
	.InputContext = inputContext,

	// Atoms
	.AtomFocused = XInternAtom(pDisplay, "_NET_WM_STATE_FOCUSED", 1),
	.AtomMaxH = XInternAtom(pDisplay, "_NET_WM_STATE_MAXIMIZED_HORZ", 1),
	.AtomMaxV = XInternAtom(pDisplay, "_NET_WM_STATE_MAXIMIZED_VERT", 1),
	.AtomHidden = XInternAtom(pDisplay, "_NET_WM_STATE_HIDDEN", 1),
	.AtomWindowManager = XInternAtom(pDisplay, "_NET_WM_STATE", 1),
	.AtomDeleteWindow = XInternAtom(pDisplay, "WM_DELETE_WINDOW", 1),
    };

    XSetWMProtocols(pDisplay, window, &back.AtomDeleteWindow, 1);

    swlWindow.pBackend = gSwlInit.MemoryAllocate(sizeof(SwlX11Backend));
    *((SwlX11Backend*)swlWindow.pBackend) = back;

    for (swl_i32 i =0; i < SwlKey_Count; ++i)
    {
	swlWindow.aKeys[i] = -1;
    }

    _swl_xkb_set_translation_table((SwlX11Backend*) swlWindow.pBackend);

    *pSwlWindow = swlWindow;
}

void
swl_window_destroy(SwlWindow* pWindow)
{
    SwlX11Backend* pX11Backend = (SwlX11Backend*) pWindow->pBackend;
    Display* pDisplay = pX11Backend->pDisplay;
    // note: this crashed for some reason
    //XCloseDisplay(pDisplay);
}

// NOTE: This is bad, prbly not working
SwlKey
_swl_array_find_item_by_name(SwlToDescTuple* aTuples, swl_size tuplesCount, const char* pName)
{
    //const swl_i32 maxLen = XkbKeyNameLength;
    //vguard(XkbKeyNameLength == 4 && "Wrong xkb key length");

    if (!pName)
	return SwlKey_Undefined;

    for (swl_i32 t = 0; t < tuplesCount; ++t)
    {
	SwlToDescTuple tuple = aTuples[t];
	swl_i32 tupleLength = _swl_str_length(tuple.pName);

	if (_swl_str_comapre(pName, tuple.pName, tupleLength))
	{
	    SwlKey key = tuple.Key;
	    return key;
	}
    }

    return SwlKey_Undefined;
}

void
_swl_xkb_set_translation_table(SwlX11Backend* pX11Backend)
{
    Display* pDisplay = pX11Backend->pDisplay;
    Window window = pX11Backend->Window;

    SwlToDescTuple aSwlToDescTuples[] = {

	{ .Key = SwlKey_0,     .pName = "AE10" },
	{ .Key = SwlKey_1,     .pName = "AE01" },
	{ .Key = SwlKey_2,     .pName = "AE02" },
	{ .Key = SwlKey_3,     .pName = "AE03" },
	{ .Key = SwlKey_4,     .pName = "AE04" },
	{ .Key = SwlKey_5,     .pName = "AE05" },
	{ .Key = SwlKey_6,     .pName = "AE06" },
	{ .Key = SwlKey_7,     .pName = "AE07" },
	{ .Key = SwlKey_8,     .pName = "AE08" },
	{ .Key = SwlKey_9,     .pName = "AE09" },
	{ .Key = SwlKey_Min,   .pName = "AE11" },
	{ .Key = SwlKey_Equal, .pName = "AE12" },

	{ .Key = SwlKey_A, .pName = "AC01" },
	{ .Key = SwlKey_B, .pName = "AB05" },
	{ .Key = SwlKey_C, .pName = "AB03" },
	{ .Key = SwlKey_D, .pName = "AC03" },
	{ .Key = SwlKey_E, .pName = "AD03" },
	{ .Key = SwlKey_F, .pName = "AC04" },

	{ .Key = SwlKey_G, .pName = "AC05" },
	{ .Key = SwlKey_H, .pName = "AC06" },
	{ .Key = SwlKey_I, .pName = "AD08" },
	{ .Key = SwlKey_J, .pName = "AC07" },
	{ .Key = SwlKey_K, .pName = "AC08" },
	{ .Key = SwlKey_L, .pName = "AC09" },

	{ .Key = SwlKey_M, .pName = "AB07" },
	{ .Key = SwlKey_N, .pName = "AB06" },
	{ .Key = SwlKey_O, .pName = "AD09" },
	{ .Key = SwlKey_P, .pName = "AD10" },

	{ .Key = SwlKey_Q, .pName = "AD01" },
	{ .Key = SwlKey_R, .pName = "AD04" },
	{ .Key = SwlKey_S, .pName = "AC02" },
	{ .Key = SwlKey_T, .pName = "AD05" },

	{ .Key = SwlKey_U, .pName = "AD07" },
	{ .Key = SwlKey_V, .pName = "AB04" },
	{ .Key = SwlKey_W, .pName = "AD02" },
	{ .Key = SwlKey_X, .pName = "AB02" },
	{ .Key = SwlKey_Y, .pName = "AD06" },
	{ .Key = SwlKey_Z, .pName = "AB01" },

	{ .Key = SwlKey_Bracket_Left, .pName = "AD11" },
	{ .Key = SwlKey_Bracket_Right, .pName = "AD12" },
	{ .Key = SwlKey_Semicolon, .pName = "AC10" },
	{ .Key = SwlKey_Apostrophe, .pName = "AC11" },

	{ .Key = SwlKey_Comma, .pName = "AB08" },
	{ .Key = SwlKey_Dot, .pName = "AB09" },
	{ .Key = SwlKey_Slash, .pName = "AB10" },
	{ .Key = SwlKey_Backslash, .pName = "BKSL" },

	{ .Key = SwlKey_Space, .pName = "SPCE" },
	{ .Key = SwlKey_Escape, .pName = "ESC" },
	{ .Key = SwlKey_Enter, .pName = "RTRN" },
	{ .Key = SwlKey_Tab, .pName = "TAB" },
	{ .Key = SwlKey_Backspace, .pName = "BKSP" },

	{ .Key = SwlKey_Insert, .pName = "INS" },
	{ .Key = SwlKey_Delete, .pName = "DELE" },
	{ .Key = SwlKey_Home, .pName = "HOME" },
	{ .Key = SwlKey_End, .pName = "END" },
	{ .Key = SwlKey_PageUp, .pName = "PGUP" },
	{ .Key = SwlKey_PageDown, .pName = "PGDN" },
	{ .Key = SwlKey_CapsLock, .pName = "CAPS" },

	{ .Key = SwlKey_Right, .pName = "RGHT" },
	{ .Key = SwlKey_Left, .pName = "LEFT" },
	{ .Key = SwlKey_Up, .pName = "UP" },
	{ .Key = SwlKey_Down, .pName = "DOWN" },

	{ .Key = SwlKey_ScrollLock, .pName = "SCLK" },
	{ .Key = SwlKey_NP_Lock, .pName = "NMLK" },
	{ .Key = SwlKey_PrintScreen, .pName = "PRSC" },
	{ .Key = SwlKey_PauseBreak, .pName = "PAUS" },

	{ .Key = SwlKey_F1 , .pName = "FK01" },
	{ .Key = SwlKey_F2 , .pName = "FK02" },
	{ .Key = SwlKey_F3 , .pName = "FK03" },
	{ .Key = SwlKey_F4 , .pName = "FK04" },
	{ .Key = SwlKey_F5 , .pName = "FK05" },
	{ .Key = SwlKey_F6 , .pName = "FK06" },
	{ .Key = SwlKey_F7 , .pName = "FK07" },
	{ .Key = SwlKey_F8 , .pName = "FK08" },
	{ .Key = SwlKey_F9 , .pName = "FK09" },
	{ .Key = SwlKey_F10, .pName = "FK10" },
	{ .Key = SwlKey_F11, .pName = "FK11" },
	{ .Key = SwlKey_F12, .pName = "FK12" },
	{ .Key = SwlKey_F13, .pName = "FK13" },
	{ .Key = SwlKey_F14, .pName = "FK14" },
	{ .Key = SwlKey_F15, .pName = "FK15" },
	{ .Key = SwlKey_F16, .pName = "FK16" },
	{ .Key = SwlKey_F17, .pName = "FK17" },
	{ .Key = SwlKey_F18, .pName = "FK18" },
	{ .Key = SwlKey_F19, .pName = "FK19" },
	{ .Key = SwlKey_F20, .pName = "FK20" },
	{ .Key = SwlKey_F21, .pName = "FK21" },
	{ .Key = SwlKey_F22, .pName = "FK22" },
	{ .Key = SwlKey_F23, .pName = "FK23" },
	{ .Key = SwlKey_F24, .pName = "FK24" },
	{ .Key = SwlKey_F25, .pName = "FK25" },

	{ .Key = SwlKey_NP_0, .pName = "KP0" },
	{ .Key = SwlKey_NP_1, .pName = "KP1" },
	{ .Key = SwlKey_NP_2, .pName = "KP2" },
	{ .Key = SwlKey_NP_3, .pName = "KP3" },
	{ .Key = SwlKey_NP_4, .pName = "KP4" },
	{ .Key = SwlKey_NP_5, .pName = "KP5" },
	{ .Key = SwlKey_NP_6, .pName = "KP6" },
	{ .Key = SwlKey_NP_7, .pName = "KP7" },
	{ .Key = SwlKey_NP_8, .pName = "KP8" },
	{ .Key = SwlKey_NP_9, .pName = "KP9" },

	{ .Key = SwlKey_NP_Comma , .pName = "KPDL" },
	{ .Key = SwlKey_NP_Div  , .pName = "KPDV" },
	{ .Key = SwlKey_NP_Mul  , .pName = "KPMU" },
	{ .Key = SwlKey_NP_Min  , .pName = "KPSU" },
	{ .Key = SwlKey_NP_Add  , .pName = "KPAD" },
	{ .Key = SwlKey_NP_Enter, .pName = "KPEN" },
	//{ .Key = SwlKey_NP_Equal, .pName = "KPEQ" },

	{ .Key = SwlKey_Shift      , .pName = "LFSH" },
	{ .Key = SwlKey_Shift_Right, .pName = "RTSH" },
	{ .Key = SwlKey_Ctrl       , .pName = "LCTL" },
	{ .Key = SwlKey_Ctrl_Right , .pName = "RCTL" },
	{ .Key = SwlKey_Super      , .pName = "LWIN" },
	{ .Key = SwlKey_Super_Right, .pName = "RWIN" },
	{ .Key = SwlKey_Alt        , .pName = "LALT" },
	{ .Key = SwlKey_Alt_Right  , .pName = "RALT" },
	{ .Key = SwlKey_Alt_Right  , .pName = "LVL3" },
	{ .Key = SwlKey_Menu  , .pName = "MENU" },

    };

#define SwlArrayCount(arr) (sizeof(arr)/sizeof(arr[0]))

    swl_size count = SwlArrayCount(aSwlToDescTuples);

    XkbDescRec* pDescRec = XkbGetMap(pDisplay, 0, XkbUseCoreKbd);
    XkbGetNames(pDisplay, XkbKeyNamesMask | XkbKeyAliasesMask, pDescRec);

    swl_i32 scancodeMin = pDescRec->min_key_code;
    swl_i32 scancodeMax = pDescRec->max_key_code;

    pX11Backend->aScanToKeys = gSwlInit.MemoryAllocate(scancodeMax * sizeof(SwlKey));
    pX11Backend->aKeyPressedTimes = gSwlInit.MemoryAllocate(SwlKey_Count * sizeof(Time));
    for (swl_i32 i = 0; i < SwlKey_Count; ++i)
	pX11Backend->aKeyPressedTimes[i] = -1;

    swl_i32 maxLen = XkbKeyNameLength;

    for (swl_i32 i = scancodeMin; i < scancodeMax; ++i)
    {
	const char* pName = (const char*) pDescRec->names->keys[i].name;

	SwlKey swlKey = SwlKey_Undefined;
	swlKey = _swl_array_find_item_by_name(aSwlToDescTuples, count, pName);

	// DOCS: We don't care about aliases right now
	/* if (swlKey == SwlKey_Undefined) */
	/* { */
	/*     const char* pAliasName = (const char*) pDescRec->names->key_aliases[i].alias; */
	/*     swlKey = _swl_array_find_item_by_name(aSwlToDescTuples, count, pAliasName); */
	/* } */

	pX11Backend->aScanToKeys[i] = swlKey;
    }

}

SwlMouseKey
_swl_x_mouse_to_swl(swl_u32 mouseBtn)
{
    static swl_u32 xToSwl[] = {
	[1] = SwlMouseKey_Left,
	[2] = SwlMouseKey_Middle,
	[3] = SwlMouseKey_Right,
	[6] = SwlMouseKey_Ext_0,
	[7] = SwlMouseKey_Ext_1,
	[8] = SwlMouseKey_Ext_2,
	[9] = SwlMouseKey_Ext_3,
	[10] = SwlMouseKey_Ext_4,
	[11] = SwlMouseKey_Ext_5,
	[12] = SwlMouseKey_Ext_6,
	[13] = SwlMouseKey_Ext_7,
	[14] = SwlMouseKey_Ext_8,
	[15] = SwlMouseKey_Ext_9,
    };

    SwlMouseKey swlMouseKey = xToSwl[mouseBtn];
    return swlMouseKey;
}

SwlKeyMod
swl_modifier_from_x(swl_i32 xModState)
{
    SwlKeyMod keyMod = SwlKeyMod_None;

    if (xModState & Mod1Mask)
	keyMod |= SwlKeyMod_Alt;

    if (xModState & Mod4Mask)
	keyMod |= SwlKeyMod_Super;

    if (xModState & ControlMask)
	keyMod |= SwlKeyMod_Ctrl;

    if (xModState & ShiftMask)
	keyMod |= SwlKeyMod_Shift;

    if (xModState & LockMask)
	keyMod |= SwlKeyMod_CapsLock;

    if (xModState & Mod2Mask)
	keyMod |= SwlKeyMod_NumLock;

    return keyMod;
}

void
swl_window_update(SwlWindow* pWindow)
{
    SwlX11Backend* pBackend = (SwlX11Backend*) pWindow->pBackend;

    Display* pDisplay = pBackend->pDisplay;
    Window window = pBackend->Window;
    XIM inputMethod  = pBackend->InputMethod;
    XIC inputContext = pBackend->InputContext;

    XEvent event, pevent;

    if (pWindow->Call.OnEvent == NULL)
    {
	return;
    }

    // DOCS: Set to default
    {
	for (swl_i32 i = 0; i < SwlKey_Count; ++i)
	{
	    if (pWindow->aKeys[i] == SwlAction_Release)
		pWindow->aKeys[i] = -1;
	}

	for (swl_i32 i = 0; i < SwlMouseKey_Count; ++i)
	{
	    if (pWindow->aMouseKeys[i] == SwlAction_Release)
		pWindow->aMouseKeys[i] = -1;
	}
    }

    XPending(pDisplay);
    while (XQLength(pDisplay))
    {
	XNextEvent(pDisplay, &event);

	//printf("event->type: %d\n", event.type);
	// printf("Is Filtered: %d\n", XFilterEvent(&event, None));

	switch (event.type)
	{

	case ClientMessage:
	{
	    if (event.xclient.data.l[0] == pBackend->AtomDeleteWindow)
	    {
		pWindow->IsAlive = 0;
	    }
	    break;
	}

	case KeyPress:
	{
	    int counter = 0;
	    swl_u32 xKey = event.xkey.keycode;
	    SwlKeyMod keyMod = swl_modifier_from_x(event.xkey.state);
	    SwlKey swlKey = pBackend->aScanToKeys[xKey];

	    Time diff = event.xkey.time - pBackend->aKeyPressedTimes[swlKey];
	    if (diff != -1 && diff <= 10)
	    {
		//printf("Skipped: %d\n", diff);
		break;
	    }
	    else
	    {
		//printf("diff: %d\n", diff);
	    }

	    pBackend->aKeyPressedTimes[swlKey] = event.xkey.time;

	    SwlKeyPressEvent keyEvent = {
		.Base = {
		    .Type = SwlEventType_KeyPress
		},
		.Key = swlKey,
		.KeyMod = keyMod,
	    };

	    pWindow->Call.OnEvent((SwlEvent*)&keyEvent);

	    pWindow->aKeys[swlKey] = SwlAction_Press;
	    //printf("Pressed %s\n", swl_key_to_string(swlKey));

	    if (swl_key_is_printable(swlKey))
	    { // type event, only for key press
		char buffer[64] = {};
		char* chars = buffer;
		Status status;

		int count = Xutf8LookupString(inputContext,
					      &event.xkey,
					      buffer,
					      sizeof(buffer),
					      NULL,
					      &status);

		swl_i32 byteCnt = 0;
		swl_u32 codepoint = _swl_utf8_decode(&chars[0], &byteCnt);
		SwlKeyTypeEvent keyTypeEvents = {
		    .Base = {
			.IsHandled = 0,
			.Type = SwlEventType_KeyType,
		    },
		    .CodePoint = codepoint,
		};

		pWindow->Call.OnEvent((SwlEvent*) &keyTypeEvents);

		// printf("chars: %d\n", codepoint);
	    }

	    break;
	}

	case KeyRelease:
	{
	    int counter = 0;
	    swl_u32 xKey = event.xkey.keycode;
	    SwlKeyMod keyMod = swl_modifier_from_x(event.xkey.state);
	    SwlKey swlKey = pBackend->aScanToKeys[xKey];
	    pWindow->aKeys[swlKey] = SwlAction_Release;

	    SwlKeyReleaseEvent keyEvent = {
		.Base = {
		    .Type = SwlEventType_KeyRelease
		},
		.Key = swlKey,
		.KeyMod = keyMod,
	    };

	    //printf("Released %s\n", swl_key_to_string(swlKey));
	    pWindow->Call.OnEvent((SwlEvent*)&keyEvent);

	    break;
	}

	case ButtonPress:
	{
	    // 4 - forward, 5 - backward
	    swl_u32 mouseBtn = event.xbutton.button;
	    if (mouseBtn == 4 || mouseBtn == 5)
	    {
		SwlMouseScrollEvent scrollEvent = {
		    .Base = {
			.Type = SwlEventType_MouseScroll,
		    },
		    .Value = (mouseBtn == 4) ? 1 : -1
		};
		pWindow->Call.OnEvent((SwlEvent*) &scrollEvent);

		continue;
	    }

	    SwlMouseKey swlMouseKey = _swl_x_mouse_to_swl(mouseBtn);

	    pWindow->aMouseKeys[swlMouseKey] = SwlAction_Press;

	    SwlMousePressEvent mouseEvent = {
		.Base = {
		    .Type = SwlEventType_MousePress
		},
		.Key = swlMouseKey,
	    };

	    pWindow->Call.OnEvent((SwlEvent*)&mouseEvent);
	    break;
	}

	case ButtonRelease:
	{
	    swl_u32 mouseBtn = event.xbutton.button;
	    if (mouseBtn == 4 || mouseBtn == 5)
		continue;

	    SwlMouseKey swlMouseKey = _swl_x_mouse_to_swl(mouseBtn);

	    pWindow->aMouseKeys[swlMouseKey] = SwlAction_Release;

	    SwlMouseReleaseEvent mouseEvent = {
		.Base = {
		    .Type = SwlEventType_MouseRelease
		},
		.Key = swlMouseKey,
	    };

	    pWindow->Call.OnEvent(&mouseEvent.Base);
	    break;
	}

	case MotionNotify:
	{
	    SwlMouseMoveEvent mouseMoveEvent = {
		.Base = {
		    .Type = SwlEventType_MouseMove
		},
		.X = event.xmotion.x,
		.Y = event.xmotion.y,
	    };

	    pWindow->Call.OnEvent(&mouseMoveEvent.Base);
	    break;
	}

	case FocusIn:
	{
	    // we can use this as un-hidden event
	    _swl_focused_event(pWindow, 1);
	    break;
	}

	case FocusOut:
	{
	    _swl_focused_event(pWindow, 0);
	    break;
	}

	case EnterNotify:
	{
	    _swl_enter_event(pWindow, 1);
	    break;
	}

	case LeaveNotify:
	{
	    _swl_enter_event(pWindow, 0);
	    break;
	}

	case VisibilityNotify:
	{
	    break;
	}

	case ConfigureNotify:
	{
	    //printf("Resize good!\n");
	    XConfigureEvent conf = event.xconfigure;
	    //printf("p(%d, %d) s(%d, %d)\n", conf.x, conf.y, conf.width, conf.height);
	    // SwlWindowResizeEvent

	    _swl_handle_resize_event(pWindow, conf.width, conf.height);
	    _swl_handle_reposition_event(pWindow, conf.x, conf.y);

	    break;
	}

	case PropertyNotify:
	{
	    XPropertyEvent ptyEvent = event.xproperty;
	    Atom atom = ptyEvent.atom;
	    char* pAtomName = XGetAtomName(pDisplay, atom);
	    // printf("XA_ATOM: %s\n", pAtomName);

	    if (atom != pBackend->AtomWindowManager)
		break;


	    swl_u64 after = 1;
	    do {
		Atom type;
		swl_i32 format;
		swl_u64 length;
		swl_u8* dp;
		Status status = XGetWindowProperty(
		    pDisplay, window, atom, 0, after, 0,
		    4, &type, &format, &length, &after, &dp);

		swl_i32 isTrue = status == Success
		    && type == 4 /* XA_ATOM */
		    && dp
		    && format == 32 /*atom for type of the property*/;

		if (!isTrue)
		    continue;

		swl_i32 maxH = 0, maxV = 0;
		for (int i = 0; i < length; i++)
		{
		    Atom prop = ((Atom*)dp)[i];

		    if (prop == pBackend->AtomFocused)
		    {
			SwlWindowFocusedEvent focusedEvent = {
			    .Base = {
				.Type = SwlEventType_WindowFocused,
			    }
			};
			pWindow->Call.OnEvent((SwlEvent*) &focusedEvent);
		    }

		    if (prop == pBackend->AtomMaxH)
		    {
			maxH = 1;
		    }

		    if (prop == pBackend->AtomMaxV)
		    {
			maxV = 1;
		    }

		    if (prop == pBackend->AtomHidden)
		    {
			SwlWindowFocusedEvent hiddenEvent = {
			    .Base = {
				.Type = SwlEventType_WindowHidden,
			    }
			};
			pWindow->Call.OnEvent((SwlEvent*) &hiddenEvent);
		    }

		    if (maxH && maxV)
		    {
			SwlWindowMaximizedEvent maximizedEvent = {
			    .Base = {
				.Type = SwlEventType_WindowMaximized,
			    }
			};
			pWindow->Call.OnEvent((SwlEvent*) &maximizedEvent);
		    }
		}
	    }
	    while (after);

	}

	break;

	}

	pevent = event;
    }

    XFlush(pDisplay);

}

SwlResolutions
swl_window_get_resolutions(SwlWindow* pWindow)
{
    SwlX11Backend* pBackend = (SwlX11Backend*) pWindow->pBackend;

    swl_i32 resoulutionsCount = 0;
    XRRScreenSize* aXrrs = XRRSizes(pBackend->pDisplay, 0, &resoulutionsCount);

    SwlResolution* aResolutions = (SwlResolution*) gSwlInit.MemoryAllocate(resoulutionsCount * sizeof(SwlResolution));
    SwlResolutions swlResolutions = {
	.Count = resoulutionsCount,
	.aResolutions = aResolutions
    };

    for (swl_i32 i = 0; i < resoulutionsCount; ++i)
    {
	XRRScreenSize screenSize = aXrrs[i];
	SwlResolution swlResolution = {
	    .Width = screenSize.width,
	    .Height = screenSize.height,
	};
	swlResolutions.aResolutions[i] = swlResolution;
    }

    return swlResolutions;
}

SwlResolution
swl_window_get_resolution(SwlWindow* pWindow)
{
    SwlX11Backend* pX11Backend = (SwlX11Backend*)pWindow->pBackend;
    swl_i32 defaultScreen = DefaultScreen(pX11Backend->pDisplay);

    long hints = 0;
    XSizeHints* size_hints = XAllocSizeHints();

    if (XGetWMSizeHints(
	    pX11Backend->pDisplay,
	    pX11Backend->Window,
	    size_hints,
	    &hints,
	    XInternAtom(pX11Backend->pDisplay, "WM_SIZE_HINTS", False)) == 0)
    {
	return (SwlResolution) { -1337, -1337 };
    }

    SwlResolution res = (SwlResolution) {
	.Width = size_hints->width,
	.Height = size_hints->height
    };
    return res;
}

swl_v2
swl_window_get_cursor_position(SwlWindow* pWindow)
{
    Window root, child;
    swl_i32 rx, ry, x, y;
    swl_u32 mask;

    SwlX11Backend* pBackend = (SwlX11Backend*) pWindow->pBackend;
    XQueryPointer(pBackend->pDisplay, pBackend->Window,
		  &root, &child,
		  &rx, &ry, &x, &y,
		  &mask);

    swl_v2 pos = { .X = x, .Y = y };

    return pos;
}

void
swl_window_set_event_call(SwlWindow* pWindow, void (*onEvent)(SwlEvent*))
{
    pWindow->Call.OnEvent = onEvent;
}

void
swl_window_set_above(SwlWindow* pWindow)
{
    SwlX11Backend* pX11Backend = (SwlX11Backend*) pWindow->pBackend;
    Display* pDisplay = pX11Backend->pDisplay;
    Window window = pX11Backend->Window;

    Atom wm_state  =  XInternAtom(pDisplay, "_NET_WM_STATE", False);
    Atom wmStateAbove = XInternAtom(pDisplay, "_NET_WM_STATE_ABOVE", 1 );

#define _NET_WM_STATE_REMOVE        0    // remove/unset property
#define _NET_WM_STATE_ADD           1    // add/set property
#define _NET_WM_STATE_TOGGLE        2    // toggle property

    XEvent xev = {
	.xclient = {
	    .type = ClientMessage,
	    .window = window,
	    .message_type = wm_state,
	    .format = 32,
	    .data.l = {
		[0] = _NET_WM_STATE_ADD,
		[1] = wmStateAbove,
		[2] = 0,
	    }
	}
    };

    XSendEvent(pDisplay, DefaultRootWindow(pDisplay), False, SubstructureNotifyMask , &xev);
}

void
swl_window_set_maximized(SwlWindow* pWindow)
{
    pWindow->IsMaximized = !pWindow->IsMaximized;

    SwlX11Backend* pX11Backend = (SwlX11Backend*) pWindow->pBackend;
    Display* pDisplay = pX11Backend->pDisplay;
    Window window = pX11Backend->Window;

    Atom wm_state  =  XInternAtom(pDisplay, "_NET_WM_STATE", False);
    Atom max_horz  =  XInternAtom(pDisplay, "_NET_WM_STATE_MAXIMIZED_HORZ", False);
    Atom max_vert  =  XInternAtom(pDisplay, "_NET_WM_STATE_MAXIMIZED_VERT", False);

#define _NET_WM_STATE_REMOVE        0    // remove/unset property
#define _NET_WM_STATE_ADD           1    // add/set property
#define _NET_WM_STATE_TOGGLE        2    // toggle property

    XEvent xev = {
	.xclient = {
	    .type = ClientMessage,
	    .window = window,
	    .message_type = wm_state,
	    .format = 32,
	    .data.l = {
		[0] = _NET_WM_STATE_TOGGLE,//_NET_WM_STATE_ADD,
		[1] = max_horz,
		[2] = max_vert,
	    }
	}
    };

    XSendEvent(pDisplay, DefaultRootWindow(pDisplay), False, SubstructureNotifyMask , &xev);
}

void
swl_window_set_fullscreen(SwlWindow* pWindow)
{
    pWindow->IsFullscreen = !pWindow->IsFullscreen;

    SwlX11Backend* pX11Backend = (SwlX11Backend*) pWindow->pBackend;
    Display* pDisplay = pX11Backend->pDisplay;
    Window window = pX11Backend->Window;

    Atom aWm = XInternAtom(pDisplay, "_NET_WM_STATE", False);
    Atom aFullScreen = XInternAtom(pDisplay, "_NET_WM_STATE_FULLSCREEN", False);

#define _NET_WM_STATE_REMOVE        0    // remove/unset property
#define _NET_WM_STATE_ADD           1    // add/set property
#define _NET_WM_STATE_TOGGLE        2    // toggle property

    XEvent xev = {
	.xclient = {
	    .type = ClientMessage,
	    .window = window,
	    .message_type = aWm,
	    .format = 32,
	    .data.l = {
		[0] = _NET_WM_STATE_TOGGLE,//_NET_WM_STATE_ADD,
		[1] = aFullScreen,
	    }
	}
    };

    XSendEvent(pDisplay, DefaultRootWindow(pDisplay), False, SubstructureNotifyMask , &xev);

}

void
swl_window_set_wo_header(SwlWindow* pWindow)
{
    SwlX11Backend* pX11Backend = (SwlX11Backend*) pWindow->pBackend;
    Display* pDisplay = pX11Backend->pDisplay;
    Window window = pX11Backend->Window;

    Atom wmAtom = XInternAtom(pDisplay, "_MOTIF_WM_HINTS", True);

    if (wmAtom != None)
    {
	enum {
	    HintsType_DecorNone = 0,
	    HintsType_Decorations = (1L << 1),
	};

	typedef struct Hints
	{
	    unsigned long   flags;
	    unsigned long   functions;
	    unsigned long   decorations;
	    long            inputMode;
	    unsigned long   status;
	} Hints;

	Hints hints = {
	    .flags = HintsType_Decorations,
	    .functions = 0,
	    .decorations = HintsType_DecorNone,
	    .inputMode = 0,
	    .status = 0
	};

	XChangeProperty(pDisplay, window, wmAtom, wmAtom, 32,
			PropModeReplace,
			(swl_u8*)&hints, 4);
    }

}

void
swl_window_set_background(SwlWindow* pWindow, swl_u8 r255, swl_u8 g255, swl_u8 b255)
{
    SwlX11Backend* pX11Backend = (SwlX11Backend*) pWindow->pBackend;
    Display* pDisplay = pX11Backend->pDisplay;
    Window window = pX11Backend->Window;

    swl_f32 ratio = 65535 / 255.0f;

    XColor color = {
	.red   = ratio * r255, // 1 - 65535, 0 - 0
	.green = ratio * g255,
	.blue  = ratio * b255,
    };

    XAllocColor(pDisplay, DefaultColormap(pDisplay,0), &color);
    XSetWindowBackground(pDisplay, window, color.pixel);

    /* XColor color; */
    /* char green[] = "#00FF00"; */
    /* Colormap colormap = DefaultColormap(pDisplay, 0); */
    /* XParseColor(pDisplay, colormap, green, &color); */
    /* XAllocColor(pDisplay, colormap, &color); */

}

#endif // SWL_X11_BACKEND == 1

#if SWL_XCB_BACKEND == 1

#include <xcb/xcb.h>

typedef struct SwlXcbBackend
{
    xcb_connection_t* pConnection;
    xcb_screen_t* pScreen;
    swl_u32 WindowId;
    xcb_gcontext_t GraphicsContext;
} SwlXcbBackend;


// DOCS: Forward declaration
void _swl_xkb_set_translation_table(/* SwlX11Backend* pX11Backend */);

void
swl_window_create(SwlWindow* pSwlWindow, SwlWindowSettings set)
{
    swl_init_was_called_check();

    swl_i32 x, y;
    _swl_position_first_or_default(set.Position, &x, &y);

    swl_i32 screenNum = 0;
    xcb_connection_t* pXcbConnection = xcb_connect(NULL, &screenNum);

    xcb_setup_t* pXcbSetup = xcb_get_setup(pXcbConnection);
    xcb_screen_iterator_t iterator = xcb_setup_roots_iterator(pXcbSetup);
    xcb_screen_t* pScreen = iterator.data;

    swl_u32 propertyName = XCB_CW_BACK_PIXEL | XCB_CW_EVENT_MASK;
    swl_u32 aProperyValues[] = {
	[0] = pScreen->white_pixel,
	[1] = XCB_EVENT_MASK_KEY_PRESS |
	XCB_EVENT_MASK_KEY_RELEASE |
	XCB_EVENT_MASK_BUTTON_PRESS |
	XCB_EVENT_MASK_BUTTON_RELEASE |
	XCB_EVENT_MASK_ENTER_WINDOW |
	XCB_EVENT_MASK_LEAVE_WINDOW |
	XCB_EVENT_MASK_EXPOSURE |
	XCB_EVENT_MASK_VISIBILITY_CHANGE |
	XCB_EVENT_MASK_STRUCTURE_NOTIFY |
	XCB_EVENT_MASK_FOCUS_CHANGE |
	XCB_EVENT_MASK_PROPERTY_CHANGE
    };


    swl_u32 xcbWindowId = xcb_generate_id(pXcbConnection);
    xcb_create_window(pXcbConnection,
		      XCB_COPY_FROM_PARENT,
		      xcbWindowId,
		      pScreen->root,
		      x, y,
		      set.Size.Width,
		      set.Size.Height,
		      1,
		      XCB_WINDOW_CLASS_INPUT_OUTPUT,
		      pScreen->root_visual,
		      propertyName,
		      aProperyValues);

    xcb_map_window(pXcbConnection, xcbWindowId);
    xcb_flush(pXcbConnection);

    // DOCS: Creating gc
    xcb_gcontext_t graphicsContext = xcb_generate_id(pXcbConnection);
    swl_u32 mask = XCB_GC_FOREGROUND;
    swl_u32 aValues[1] = {};
    xcb_create_gc(pXcbConnection, graphicsContext, pScreen->root, mask, aValues);

    SwlWindow swlWindow = {
    };

    // DOCS: Setup backend
    SwlXcbBackend back = {
	.pConnection = pXcbConnection,
	.pScreen = pScreen,
	.WindowId = xcbWindowId,
	.GraphicsContext = graphicsContext,
    };

    swlWindow.pBackend = gSwlInit.MemoryAllocate(sizeof(SwlXcbBackend));
    *((SwlXcbBackend*)swlWindow.pBackend) = back;

    return swlWindow;
}

void
swl_window_destroy(SwlWindow* pWindow)
{
    SwlXcbBackend* pXcbBackend = (SwlXcbBackend*) pWindow->pBackend;
    xcb_connection_t* pConnection = pXcbBackend->pConnection;
    xcb_gcontext_t gc = pXcbBackend->GraphicsContext;

    xcb_disconnect(pConnection);
}

SwlKey
_swl_array_find_item_by_name(SwlToDescTuple* aTuples, swl_size tuplesCount, const char* pName)
{
    return SwlKey_Undefined;
}

void
_swl_xkb_set_translation_table(/* SwlX11Backend* pX11Backend */)
{
}

SwlMouseKey
_swl_x_mouse_to_swl(swl_u32 mouseBtn)
{
    static swl_u32 xToSwl[] = {
	[1] = SwlMouseKey_Left,
	[2] = SwlMouseKey_Middle,
	[3] = SwlMouseKey_Right,
	[6] = SwlMouseKey_Ext_0,
	[7] = SwlMouseKey_Ext_1,
	[8] = SwlMouseKey_Ext_2,
	[9] = SwlMouseKey_Ext_3,
	[10] = SwlMouseKey_Ext_4,
	[11] = SwlMouseKey_Ext_5,
	[12] = SwlMouseKey_Ext_6,
	[13] = SwlMouseKey_Ext_7,
	[14] = SwlMouseKey_Ext_8,
	[15] = SwlMouseKey_Ext_9,
    };

    SwlMouseKey swlMouseKey = xToSwl[mouseBtn];
    return swlMouseKey;
}

SwlKeyMod
swl_modifier_from_x(swl_i32 xModState)
{
    SwlKeyMod keyMod = SwlKeyMod_None;

    /* if (xModState & Mod1Mask) */
    /*	keyMod |= SwlKeyMod_Alt; */

    /* if (xModState & Mod4Mask) */
    /*	keyMod |= SwlKeyMod_Super; */

    /* if (xModState & ControlMask) */
    /*	keyMod |= SwlKeyMod_Ctrl; */

    /* if (xModState & ShiftMask) */
    /*	keyMod |= SwlKeyMod_Shift; */

    /* if (xModState & LockMask) */
    /*	keyMod |= SwlKeyMod_CapsLock; */

    /* if (xModState & Mod2Mask) */
    /*	keyMod |= SwlKeyMod_NumLock; */

    return keyMod;
}

void
swl_window_update(SwlWindow* pWindow)
{
    xcb_generic_event_t *event;

    while ( (event = xcb_poll_for_event (connection, 0)) ) {
	/* ...handle the event */
    }
}

void
swl_window_set_event_call(SwlWindow* pWindow, void (*onEvent)(SwlEvent*))
{
    pWindow->Call.OnEvent = onEvent;
}

void
swl_window_set_above(SwlWindow* pWindow)
{
}

void
swl_window_set_maximized(SwlWindow* pWindow)
{
}

void
swl_window_set_fullscreen(SwlWindow* pWindow)
{
}

void
swl_window_set_wo_header(SwlWindow* pWindow)
{
}

void
swl_window_set_background(SwlWindow* pWindow, swl_u8 r255, swl_u8 g255, swl_u8 b255)
{
    SwlXcbBackend* pXcbBackend = (SwlXcbBackend*) pWindow->pBackend;
    xcb_connection_t* pConnection = pXcbBackend->pConnection;
    xcb_gcontext_t gc = pXcbBackend->GraphicsContext;

    swl_u32 fullColor = r255 << 24 | g255 << 16 | b255 << 8 | 255;

    xcb_change_gc(pConnection,
		  gc,
		  XCB_GC_BACKGROUND,
		  &fullColor);
}

#endif // SWL_XCB_BACKEND == 1

#if defined(SWL_RENDERER)

#include <Core/SimpleStandardLibrary.h>
#include <Deps/stb_truetype.h>

/*
  DOCS: Rendering (OpenGL3.3 as backend)
  #define SWL_RENDERER - for implementation
*/

void swl_renderer_create(SwlWindow* pWindow);
void swl_renderer_destroy(SwlWindow* pWindow);

swl_i32 swl_renderer_texture_create(void* pData, swl_v2 size, swl_i32 channels);
void swl_renderer_resize(SwlWindow* pWindow);
void swl_renderer_set_swap_iterval(SwlWindow* pWindow, swl_i32 interval);
void swl_renderer_clear_color(SwlWindow* pWindow, swl_f32 r, swl_f32 g, swl_f32 b, swl_f32 a);
void swl_renderer_draw_rectangle(SwlWindow* pWindow, swl_v3 position, swl_v2 size, swl_v4 color);
void swl_renderer_draw_texture_uv(SwlWindow* pWindow, swl_v3 position, swl_v2 size, swl_v4 color, swl_i32 texture, swl_v2 uvs[4]);
void swl_renderer_draw_texture(SwlWindow* pWindow, swl_v3 position, swl_v2 size, swl_v4 color, swl_i32 texture);
void swl_renderer_draw_text(SwlWindow* pWindow, swl_i32* pText, swl_size textLength, swl_v3 position, swl_v4 color);
void swl_renderer_flush(SwlWindow* pWindow);

void* swl_renderer_font_get_bitmap();
void swl_renderer_font_load(SwlFontSettings* pSettings);

swl_f32 swl_renderer_get_text_width(SwlWindow* pWindow, swl_i32* pText, swl_i32 textLength);


typedef void* (*SwlGetProcDelegate)(const char* pFuncName);
#if SWL_LINUX == 1
struct Display;
typedef void (*GlXSwapBuffersDelegate)(struct Display *dpy, swl_u64 xWindowHandle);
typedef void (*GlXSwapIntervalDelegate)(struct Display *dpy, swl_u64 xWindowHandle, swl_i32 interval);
#endif


typedef struct SwlGlContext
{
#if SWL_LINUX == 1
    void* pLibGL;
    SwlGetProcDelegate GetProc; // /gladGetProcAddressPtr
    GlXSwapBuffersDelegate SwapBuffers;
    GlXSwapIntervalDelegate SwapInterval;

#elif SWL_WINDOWS == 1
    HMODULE pLibGL;
    SwlGetProcDelegate GetProc;
    // Swapbuffers alternative;
#endif
} SwlGlContext;

typedef struct SwlRenderPipe
{
    swl_u32 VertexBuffer;
    swl_u32 IndexBuffer;
    swl_u32 VertexArray;
    swl_u32 Shader;
    swl_u32 DefaultTexture;

    swl_size VertexOffset;
    swl_size IndexOffset;
    swl_i8 TextureOffset;

    swl_i32 WidthLocation;
    swl_i32 EdgeTransitionLocation;
    swl_i32 TexturesLocation;
} SwlRenderPipe;

typedef struct SwlFramePipe
{
    swl_u32 Shader;
    swl_u32 Vertex;
    swl_u32 VertexArray;

    swl_u32 Framebuffer;
    swl_u32 ColorAttachment;
    swl_u32 DepthAttachment;
} SwlFramePipe;

typedef struct SwlGl
{
    SwlRenderPipe RenderPipe;
    SwlFramePipe FramePipe;

    struct Color
    {
	swl_f32 R;
	swl_f32 G;
	swl_f32 B;
	swl_f32 A;
    } ClearColor;
} SwlGl;

SwlGlContext gGlContext;
SwlGl gGl;

#if !defined(SWL_X11_BACKEND)
#error "We support only linux with xlib for now, so ... :("
#endif

#if SWL_LINUX == 1
/***/#include <dlfcn.h>
#elif SWL_WINDOWS == 1
/***/#include <windows.h>
#endif

     /* DOCS: Type definition*/
typedef swl_i32 GLsizei;
typedef swl_i32 GLint;
typedef swl_u32 GLuint;
typedef swl_u32 GLenum;
typedef void GLvoid;
typedef swl_i8 GLbyte;
typedef swl_u8 GLubyte;
typedef swl_i16 GLshort;
typedef swl_i64 GLsizeiptr;
typedef swl_i64 GLintptr;
typedef swl_f32 GLfloat;
typedef char GLchar;
typedef swl_u8 GLboolean;

#define GL_ARRAY_BUFFER 0x8892
#define GL_DYNAMIC_DRAW 0x88E8
#define GL_STATIC_DRAW 0x88E4
#define GL_ELEMENT_ARRAY_BUFFER 0x8893
#define GL_FLOAT 0x1406
#define GL_VERTEX_SHADER 0x8B31
#define GL_FRAGMENT_SHADER 0x8B30
#define GL_TRIANGLES 0x0004
#define GL_UNSIGNED_INT 0x1405
#define GL_COLOR_BUFFER_BIT 0x00004000
#define GL_DEPTH_BUFFER_BIT 0x00000100
#define GL_STENCIL_BUFFER_BIT 0x00000400
#define GL_DEPTH_TEST 0x0B71
#define GL_COMPILE_STATUS 0x8B81
#define GL_LINK_STATUS 0x8B82
#define GL_LESS 0x0201

// DOCS: Framebuffer
#define GL_FRAMEBUFFER 0x8D40
#define GL_DEPTH_STENCIL 0x84F9
#define GL_TEXTURE_2D_MULTISAMPLE 0x9100
#define GL_RGBA8 0x8058
#define GL_FALSE 0
#define GL_TRUE 1
#define GL_COLOR_ATTACHMENT0 0x8CE0
#define GL_COLOR_ATTACHMENT1 0x8CE1
#define GL_COLOR_ATTACHMENT2 0x8CE2
#define GL_COLOR_ATTACHMENT3 0x8CE3
#define GL_COLOR_ATTACHMENT4 0x8CE4
#define GL_COLOR_ATTACHMENT5 0x8CE5
#define GL_COLOR_ATTACHMENT6 0x8CE6
#define GL_COLOR_ATTACHMENT7 0x8CE7
#define GL_COLOR_ATTACHMENT8 0x8CE8
#define GL_COLOR_ATTACHMENT9 0x8CE9
#define GL_COLOR_ATTACHMENT10 0x8CEA
#define GL_COLOR_ATTACHMENT11 0x8CEB
#define GL_COLOR_ATTACHMENT12 0x8CEC
#define GL_COLOR_ATTACHMENT13 0x8CED
#define GL_COLOR_ATTACHMENT14 0x8CEE
#define GL_COLOR_ATTACHMENT15 0x8CEF
#define GL_COLOR_ATTACHMENT16 0x8CF0
#define GL_COLOR_ATTACHMENT17 0x8CF1
#define GL_COLOR_ATTACHMENT18 0x8CF2
#define GL_COLOR_ATTACHMENT19 0x8CF3
#define GL_COLOR_ATTACHMENT20 0x8CF4
#define GL_COLOR_ATTACHMENT21 0x8CF5
#define GL_COLOR_ATTACHMENT22 0x8CF6
#define GL_COLOR_ATTACHMENT23 0x8CF7
#define GL_COLOR_ATTACHMENT24 0x8CF8
#define GL_COLOR_ATTACHMENT25 0x8CF9
#define GL_COLOR_ATTACHMENT26 0x8CFA
#define GL_COLOR_ATTACHMENT27 0x8CFB
#define GL_COLOR_ATTACHMENT28 0x8CFC
#define GL_COLOR_ATTACHMENT29 0x8CFD
#define GL_COLOR_ATTACHMENT30 0x8CFE
#define GL_COLOR_ATTACHMENT31 0x8CFF
#define GL_DEPTH_ATTACHMENT 0x8D00
#define GL_STENCIL_ATTACHMENT 0x8D20
#define GL_DEPTH24_STENCIL8 0x88F0
#define GL_UNSIGNED_INT_24_8 0x84FA
#define GL_DEPTH_STENCIL_ATTACHMENT 0x821A

// DOCS: Texture
#define GL_TEXTURE_2D 0x0DE1
#define GL_TEXTURE_MAG_FILTER 0x2800
#define GL_TEXTURE_MIN_FILTER 0x2801
#define GL_TEXTURE_WRAP_S 0x2802
#define GL_TEXTURE_WRAP_T 0x2803
#define GL_NEAREST 0x2600
#define GL_LINEAR 0x2601
#define GL_RED 0x1903
#define GL_GREEN 0x1904
#define GL_BLUE 0x1905
#define GL_ALPHA 0x1906
#define GL_RGB 0x1907
#define GL_RGBA 0x1908
#define GL_REPEAT 0x2901
#define GL_CLAMP_TO_EDGE 0x812F

// DOCS: Texture->Slots
#define GL_TEXTURE0 0x84C0
#define GL_TEXTURE1 0x84C1
#define GL_TEXTURE2 0x84C2
#define GL_TEXTURE3 0x84C3
#define GL_TEXTURE4 0x84C4
#define GL_TEXTURE5 0x84C5
#define GL_TEXTURE6 0x84C6
#define GL_TEXTURE7 0x84C7
#define GL_TEXTURE8 0x84C8
#define GL_TEXTURE9 0x84C9
#define GL_TEXTURE10 0x84CA
#define GL_TEXTURE11 0x84CB
#define GL_TEXTURE12 0x84CC
#define GL_TEXTURE13 0x84CD
#define GL_TEXTURE14 0x84CE
#define GL_TEXTURE15 0x84CF
#define GL_TEXTURE16 0x84D0
#define GL_TEXTURE17 0x84D1
#define GL_TEXTURE18 0x84D2
#define GL_TEXTURE19 0x84D3
#define GL_TEXTURE20 0x84D4
#define GL_TEXTURE21 0x84D5
#define GL_TEXTURE22 0x84D6
#define GL_TEXTURE23 0x84D7
#define GL_TEXTURE24 0x84D8
#define GL_TEXTURE25 0x84D9
#define GL_TEXTURE26 0x84DA
#define GL_TEXTURE27 0x84DB
#define GL_TEXTURE28 0x84DC
#define GL_TEXTURE29 0x84DD
#define GL_TEXTURE30 0x84DE
#define GL_TEXTURE31 0x84DF
#define GL_ACTIVE_TEXTURE 0x84E0

// DOCS: Types
#define GL_BYTE 0x1400
#define GL_UNSIGNED_BYTE 0x1401
#define GL_SHORT 0x1402
#define GL_UNSIGNED_SHORT 0x1403
#define GL_INT 0x1404
#define GL_UNSIGNED_INT 0x1405
#define GL_FLOAT 0x1406

// DOCS: Errors
#define GL_NO_ERROR 0
#define GL_INVALID_ENUM 0x0500
#define GL_INVALID_VALUE 0x0501
#define GL_INVALID_OPERATION 0x0502
#define GL_OUT_OF_MEMORY 0x0505
#define GL_INVALID_FRAMEBUFFER_OPERATION 0x0506

static const char*
gl_error_to_str(swl_i32 err)
{
    switch (err)
    {
    case GL_NO_ERROR: return "Success";
    case GL_INVALID_ENUM: return "invalid enum";
    case GL_INVALID_VALUE: return "invalid value";
    case GL_INVALID_OPERATION: return "invalid operation";
    case GL_OUT_OF_MEMORY: return "out of memory";
    case GL_INVALID_FRAMEBUFFER_OPERATION: return "GL_INVALID_FRAMEBUFFER_OPERATION";
    default: return "Empty";
    }
}

typedef void (*GlGetIntegervDelegate)(GLenum pname, GLint *data);
typedef GLenum (*GlGetErrorDelegate)(void);
typedef void (*GlGenBuffersDelegate)(GLsizei n, GLuint* buffers);
typedef void (*GlBindBufferDelegate)(GLenum target, GLuint buffer);
typedef void (*GlBufferDataDelegate)(GLenum target, GLsizeiptr size, const void *data, GLenum usage);
typedef void (*GlBufferSubDataDelegate)(GLenum target, GLintptr offset, GLsizeiptr size, const void *data);
typedef void (*GlCreateTexturesDelegate)(GLenum target, GLsizei n, GLuint* textures);
typedef void (*GlBindTextureDelegate)(GLenum target, GLuint texture);
typedef void (*GlTexParameteriDelegate)(GLenum target, GLenum pname, GLint param);
typedef void (*GlTexImage2DDelegate)(GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type, const void *pixels);
typedef void (*GlTexImage2DMultisampleDelegate)(GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height, GLboolean fixedsamplelocations);
typedef void (*GlGenerateMipmapDelegate)(GLenum target);
typedef void (*GlActiveTextureDelegate)(GLenum texture);
typedef void (*GlTexSubImage2DDelegate)(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLenum type, const GLvoid * data);
typedef void (*GlDeleteTexturesDelegate)(GLsizei n, const GLuint* textures);
typedef void (*GlCreateFramebuffersDelegate)(GLsizei n, GLuint *framebuffers);
typedef void (*GlBindFramebufferDelegate)(GLenum target, GLuint framebuffer);
typedef void (*GlFramebufferTexture2DDelegate)(GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level);
typedef void (*GlDeleteFramebuffersDelegate)(GLsizei n, GLuint* framebuffers);

typedef void (*GlGenVertexArraysDelegate)(GLsizei n, GLuint *arrays);
typedef void (*GlDeleteVertexArraysDelegate)(GLsizei n, const GLuint* arrays);
typedef void (*GlBindVertexArrayDelegate)(GLuint array);
typedef void (*GlEnableVertexAttribArrayDelegate)(GLuint index);
typedef void (*GlDisableVertexAttribArrayDelegate)(GLuint index);
typedef void (*GlVertexAttribPointerDelegate)(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid* pointer);
typedef GLint (*GlGetUniformLocationDelegate)(GLuint program, const GLchar* name);
typedef void (*GlUniformMatrix4fvDelegate)(GLint location, GLsizei	count, GLboolean transpose, const GLfloat* value);
typedef void (*GlUniform1ivDelegate)(GLint location, GLsizei count, const GLint* value);
typedef void (*GlUniform1fDelegate)(GLint location, GLfloat v0);
typedef void (*GlUniform1iDelegate)(GLint location, GLint v0);
typedef void (*GlGetShaderivDelegate)(GLuint shader, GLenum pname, GLint* params);
typedef void (*GlGetProgramivDelegate)(GLuint program, GLenum pname, GLint* params);
typedef void (*GlGetShaderInfoLogDelegate)(GLuint shader, GLsizei maxLength, GLsizei* length, GLchar* infoLog);
typedef void (*GlGetProgramInfoLogDelegate)(GLuint program, GLsizei maxLength, GLsizei* length, GLchar* infoLog);
typedef GLuint (*GlCreateShaderDelegate)(GLenum shaderType);
typedef void (*GlDeleteShaderDelegate)(GLuint shader);
typedef void (*GlShaderSourceDelegate)(GLuint shader, GLsizei count, const GLchar** string, const GLint* length);
typedef void (*GlCompileShaderDelegate)(GLuint shader);
typedef GLuint (*GlCreateProgramDelegate)(void);
typedef void (*GlDeleteProgramDelegate)(GLuint program);
typedef void (*GlAttachShaderDelegate)(GLuint program, GLuint shader);
typedef void (*GlDetachShaderDelegate)(GLuint program, GLuint shader);
typedef void (*GlLinkProgramDelegate)(GLuint program);
typedef void (*GlUseProgramDelegate)(GLuint program);
typedef void (*GlDrawArraysDelegate)(GLenum mode, GLint first, GLsizei count);
typedef void (*GlDrawElementsDelegate)(GLenum mode, GLsizei count, GLenum type, const GLvoid* indices);
typedef void (*GlClearColorDelegate)(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha);
typedef void (*GlClearDelegate)(swl_u32 mask);
typedef void (*GlEnableDelegate)(GLenum cap);
typedef void (*GlDisableDelegate)(GLenum cap);
typedef void (*GlDepthFuncDelegate)(GLenum func);
typedef void (*GlViewportDelegate)(GLint x,GLint y,GLsizei width,GLsizei height);

// DOCS: Basic
GlGetIntegervDelegate glGetIntegerv;
GlGetErrorDelegate glGetError;

// DOCS: Functions for buffers
GlGenBuffersDelegate glGenBuffers;
GlBindBufferDelegate glBindBuffer;
GlBufferDataDelegate glBufferData;
GlBufferSubDataDelegate glBufferSubData;

// DOCS: Textures
GlCreateTexturesDelegate glCreateTextures;
GlBindTextureDelegate glBindTexture;
GlTexParameteriDelegate glTexParameteri;
GlTexImage2DDelegate glTexImage2D;
GlTexImage2DMultisampleDelegate glTexImage2DMultisample;
GlGenerateMipmapDelegate glGenerateMipmap;
GlActiveTextureDelegate glActiveTexture;
GlTexSubImage2DDelegate glTexSubImage2D;
GlDeleteTexturesDelegate glDeleteTextures;

// DOCS: Framebuffers
GlCreateFramebuffersDelegate glCreateFramebuffers;
GlBindFramebufferDelegate glBindFramebuffer;
GlFramebufferTexture2DDelegate glFramebufferTexture2D;
GlDeleteFramebuffersDelegate glDeleteFramebuffers;

// DOCS: Vertex Arrays
// ALternative to glCreateVertexArrays();
GlGenVertexArraysDelegate glGenVertexArrays;
GlDeleteVertexArraysDelegate glDeleteVertexArrays;
GlBindVertexArrayDelegate glBindVertexArray;
GlEnableVertexAttribArrayDelegate glEnableVertexAttribArray;
GlDisableVertexAttribArrayDelegate glDisableVertexAttribArray;
GlVertexAttribPointerDelegate glVertexAttribPointer;

// DOCS: Shader Load
GlGetShaderivDelegate glGetShaderiv;
GlGetProgramivDelegate glGetProgramiv;
GlGetShaderInfoLogDelegate glGetShaderInfoLog;
GlGetProgramInfoLogDelegate glGetProgramInfoLog;
GlCreateShaderDelegate glCreateShader;
GlDeleteShaderDelegate glDeleteShader;
GlUseProgramDelegate glUseProgram;
GlShaderSourceDelegate glShaderSource;
GlCompileShaderDelegate glCompileShader;
GlCreateProgramDelegate glCreateProgram;
GlDeleteProgramDelegate glDeleteProgram;
GlAttachShaderDelegate glAttachShader;
GlDetachShaderDelegate glDetachShader;
GlLinkProgramDelegate glLinkProgram;
GlGetUniformLocationDelegate glGetUniformLocation;
GlUniformMatrix4fvDelegate glUniformMatrix4fv;
GlUniform1ivDelegate glUniform1iv;
GlUniform1fDelegate glUniform1f;
GlUniform1iDelegate glUniform1i;



// DOCS: Drawing
GlDrawArraysDelegate glDrawArrays;
GlDrawElementsDelegate glDrawElements;
GlClearColorDelegate glClearColor;
GlClearDelegate glClear;
GlEnableDelegate glEnable;
GlDisableDelegate glDisable;
GlDepthFuncDelegate glDepthFunc;
GlViewportDelegate glViewport;

void
_swl_gl_load(SwlWindow* pWindow)
{
    SwlGlContext glContext = {};
    swl_i32 isLoadSuccess = 0;

#if SWL_LINUX == 1

    // NOTE: OpenGL .so
    void* pLibGL = dlopen("libGL.so", RTLD_NOW | RTLD_GLOBAL);
    SwlGetProcDelegate swlGetProc;
    if (pLibGL != NULL)
    {
	swlGetProc = (SwlGetProcDelegate) dlsym(pLibGL, "glXGetProcAddressARB");
	isLoadSuccess = (swlGetProc != NULL);
    }

    gGlContext = (SwlGlContext) {
	.pLibGL = pLibGL,
	.GetProc = swlGetProc,
    };

#elif SWL_WINDOWS == 1

    // NOTE: OpenGL .dll
    HMODULE pLibGL = LoadLibraryW(L"opengl32.dll");
    SwlGetProcDelegate swlGetProc;
    if (pLibGL != NULL)
    {
	swlGetProc = (SwlGetProcDelegate) GetProcAddress(pLibGL, "wglGetProcAddress");
	isLoadSuccess = (swlGetProc != NULL);
    }

    gGlContext = (SwlGlContext) {
	.pLibGL = pLibGL,
	.GetProc = swlGetProc,
    };

#endif

    if (!isLoadSuccess)
    {
	//handle error
    }

    SwlGetProcDelegate load = gGlContext.GetProc;

#define GlLoad(t, f) ({ f = (t) load(#f); })

    {

#define GLX_RGBA_BIT 0x00000001
#define GLX_WINDOW_BIT 0x00000001
#define GLX_DRAWABLE_TYPE 0x8010
#define GLX_RENDER_TYPE 0x8011
#define GLX_RGBA_TYPE 0x8014
#define GLX_DOUBLEBUFFER 5
#define GLX_RED_SIZE 8
#define GLX_GREEN_SIZE 9
#define GLX_BLUE_SIZE 10
#define GLX_ALPHA_SIZE 11
#define GLX_DEPTH_SIZE 12
#define GLX_STENCIL_SIZE 13

	// note: pick framebuffer configuration
	swl_i32 aVisualAttributes[] = {
	    GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
	    GLX_RENDER_TYPE,   GLX_RGBA_BIT,
	    GLX_RED_SIZE,      8,
	    GLX_GREEN_SIZE,    8,
	    GLX_BLUE_SIZE,     8,
	    GLX_ALPHA_SIZE,    8,
	    GLX_DEPTH_SIZE,    24,
	    GLX_STENCIL_SIZE,  8,
	    GLX_DOUBLEBUFFER,  True,
	    None
	};

	// DOCS: GLX spec
	typedef XID GLXWindow;
	typedef XID GLXDrawable;
	typedef struct __GLXFBConfig* GLXFBConfig;
	typedef struct __GLXcontext* GLXContext;

	typedef GLXContext (*GlXCreateContextAttribsARBDelegate)(Display*,GLXFBConfig,GLXContext,swl_i32,const swl_i32*);
	typedef GLXFBConfig* (*GlXChooseFBConfigDelegate)(Display* dpy, swl_i32 screen, const swl_i32* attrib_list, swl_i32* nelements);

	SwlX11Backend* pBack = (SwlX11Backend*) pWindow->pBackend;
	swl_i32 defaultScreen = DefaultScreen(pBack->pDisplay);
	swl_i32 configsCount;

	GlXChooseFBConfigDelegate glXChooseFBConfig = (GlXChooseFBConfigDelegate) load("glXChooseFBConfig");
	if (!glXChooseFBConfig)
	{
	    // error
	    printf("Can't load glXChooseFBConfig");
	    return;
	}

	GLXFBConfig* pFramebufferConf = glXChooseFBConfig(pBack->pDisplay, defaultScreen, aVisualAttributes, &configsCount);
	if (pFramebufferConf == NULL || configsCount < 1)
	{
	    //error
	    printf("error: glXChooseFBConfig\n");
	}

	GlXCreateContextAttribsARBDelegate glXCreateContextAttribsARB =
	    (GlXCreateContextAttribsARBDelegate)
	    load("glXCreateContextAttribsARB");
	if (!glXCreateContextAttribsARB)
	{
	    // error
	    printf("error: load glXCreateContextAttribsARB\n");
	}

#define GLX_CONTEXT_PROFILE_MASK_ARB 0x9126
#define GLX_CONTEXT_CORE_PROFILE_BIT_ARB 0x00000001
#define GLX_CONTEXT_MAJOR_VERSION_ARB 0x2091
#define GLX_CONTEXT_MINOR_VERSION_ARB 0x2092

	// Display*,GLXFBConfig,GLXContext,Bool,const int*
	const swl_i32 majorVersion = 3, minorVersion = 3;
	swl_i32 aContextAttributes[] = {
	    GLX_CONTEXT_PROFILE_MASK_ARB,  GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
	    GLX_CONTEXT_MAJOR_VERSION_ARB, majorVersion,
	    GLX_CONTEXT_MINOR_VERSION_ARB, minorVersion,
	    0, 0
	};
	GLXContext glxContext;
	glxContext = glXCreateContextAttribsARB(pBack->pDisplay, pFramebufferConf[0], NULL, True, aContextAttributes);

	typedef Bool (*GlXMakeCurrentDelegate)(Display *dpy, GLXDrawable
					       draw, GLXContext ctx);

	GlXMakeCurrentDelegate glXMakeCurrent =
	    (GlXMakeCurrentDelegate)
	    load("glXMakeCurrent");
	if (!glXCreateContextAttribsARB)
	{
	    // error
	    printf("error: load glXMakeCurrent\n");
	}

	if (glXMakeCurrent(pBack->pDisplay, pBack->Window, glxContext) != True)
	{
	    //err
	    printf("error: glXMakeCurrent\n");
	    return;
	}

	GlXSwapBuffersDelegate glXSwapBuffers =
	    (GlXSwapBuffersDelegate) load("glXSwapBuffers");
	if (!glXSwapBuffers)
	{
	    //err
	    return;
	}
	gGlContext.SwapBuffers = glXSwapBuffers;

	// DOCS: Check if we have ext for making

	typedef const char* (*GlXQueryExtensionsStringDelegate)(Display* dpy, swl_i32 screen);

	gGlContext.SwapInterval = NULL;

	GlXQueryExtensionsStringDelegate glXQueryExtensionsString =
	    (GlXQueryExtensionsStringDelegate)
	    load("glXQueryExtensionsString");
	const char* pExts =
	    glXQueryExtensionsString(pBack->pDisplay, defaultScreen);
	if (pExts)
	{
	    const char* pExtName = "GLX_EXT_swap_control";
	    char* ptr = (char*)pExts;
	    char c = *ptr;
	    while (c != '\0')
	    {
		if (c == *pExtName)
		{
		    swl_i32 ind = 0;
		    for (char tc; tc != '\0' && ind < 20; ++ind)
		    {
			tc = ptr[ind];
			if (tc != pExtName[ind])
			{
			    continue;
			}

			++ind;
		    }

		    GlXSwapIntervalDelegate glXSwapIntervalEXT =
			(GlXSwapIntervalDelegate)
			load("glXSwapIntervalEXT");

		    if (glXSwapIntervalEXT)
		    {
			gGlContext.SwapInterval = glXSwapIntervalEXT;
		    }

		    break;
		}

		c = *ptr;
		++ptr;
	    }
	}

	XFree(pFramebufferConf);
    }

    // DOCS: Basic
    GlLoad(GlGetIntegervDelegate, glGetIntegerv);
    GlLoad(GlGetErrorDelegate, glGetError);

// DOCS: Functions for buffers
    // note: version 1.5
    GlLoad(GlGenBuffersDelegate, glGenBuffers);
    GlLoad(GlBindBufferDelegate, glBindBuffer);
    GlLoad(GlBufferDataDelegate, glBufferData);
    GlLoad(GlBufferSubDataDelegate, glBufferSubData);

// DOCS: Textures
    GlLoad(GlCreateTexturesDelegate, glCreateTextures);
    GlLoad(GlBindTextureDelegate, glBindTexture);
    GlLoad(GlTexParameteriDelegate, glTexParameteri);
    GlLoad(GlTexImage2DDelegate, glTexImage2D);
    GlLoad(GlTexImage2DMultisampleDelegate, glTexImage2DMultisample);
    GlLoad(GlGenerateMipmapDelegate, glGenerateMipmap);
    GlLoad(GlActiveTextureDelegate, glActiveTexture);
    GlLoad(GlTexSubImage2DDelegate, glTexSubImage2D);
    GlLoad(GlDeleteTexturesDelegate, glDeleteTextures);

// DOCS: Framebuffers
    GlLoad(GlCreateFramebuffersDelegate, glCreateFramebuffers);
    GlLoad(GlBindFramebufferDelegate, glBindFramebuffer);
    GlLoad(GlFramebufferTexture2DDelegate, glFramebufferTexture2D);
    GlLoad(GlDeleteFramebuffersDelegate, glDeleteFramebuffers);

// DOCS: Vertex Arrays
    GlLoad(GlGenVertexArraysDelegate, glGenVertexArrays); // note: ALternative to glCreateVertexArrays();
    GlLoad(GlDeleteVertexArraysDelegate, glDeleteVertexArrays);
    GlLoad(GlBindVertexArrayDelegate, glBindVertexArray);
    GlLoad(GlEnableVertexAttribArrayDelegate, glEnableVertexAttribArray);
    GlLoad(GlDisableVertexAttribArrayDelegate, glDisableVertexAttribArray);
    GlLoad(GlVertexAttribPointerDelegate, glVertexAttribPointer);

// DOCS: Shader Load
    GlLoad(GlGetShaderivDelegate, glGetShaderiv);
    GlLoad(GlGetProgramivDelegate, glGetProgramiv);
    GlLoad(GlGetShaderInfoLogDelegate, glGetShaderInfoLog);
    GlLoad(GlGetProgramInfoLogDelegate, glGetProgramInfoLog);
    GlLoad(GlCreateShaderDelegate, glCreateShader);
    GlLoad(GlDeleteShaderDelegate, glDeleteShader);
    GlLoad(GlShaderSourceDelegate, glShaderSource);
    GlLoad(GlUseProgramDelegate, glUseProgram);
    GlLoad(GlCompileShaderDelegate, glCompileShader);
    GlLoad(GlCreateProgramDelegate, glCreateProgram);
    GlLoad(GlDeleteProgramDelegate, glDeleteProgram);
    GlLoad(GlAttachShaderDelegate, glAttachShader);
    GlLoad(GlDetachShaderDelegate, glDetachShader);
    GlLoad(GlLinkProgramDelegate, glLinkProgram);
    GlLoad(GlGetUniformLocationDelegate, glGetUniformLocation);
    GlLoad(GlUniformMatrix4fvDelegate, glUniformMatrix4fv);
    GlLoad(GlUniform1ivDelegate, glUniform1iv);
    GlLoad(GlUniform1fDelegate, glUniform1f);
    GlLoad(GlUniform1iDelegate, glUniform1i);

// DOCS: Drawing
    GlLoad(GlDrawArraysDelegate, glDrawArrays);
    GlLoad(GlDrawElementsDelegate, glDrawElements);
    GlLoad(GlClearColorDelegate, glClearColor);
    GlLoad(GlClearDelegate, glClear);

    GlLoad(GlEnableDelegate, glEnable);
    GlLoad(GlDisableDelegate, glDisable);
    GlLoad(GlDepthFuncDelegate, glDepthFunc);
    GlLoad(GlViewportDelegate, glViewport);

#undef GlLoad
}

typedef struct SwlVertex2D
{
    swl_v3 Position;
    swl_v4 Color;
    swl_v2 Uv;
    swl_v2 Indices; //TextureIndex;
} SwlVertex2D;

typedef swl_f32 swl_v4a[4];
typedef swl_v4a swl_m4a[4];

typedef struct swl_m4
{
    union
    {
	struct
	{
	    swl_f32 M00;
	    swl_f32 M01;
	    swl_f32 M02;
	    swl_f32 M03;

	    swl_f32 M10;
	    swl_f32 M11;
	    swl_f32 M12;
	    swl_f32 M13;

	    swl_f32 M20;
	    swl_f32 M21;
	    swl_f32 M22;
	    swl_f32 M23;

	    swl_f32 M30;
	    swl_f32 M31;
	    swl_f32 M32;
	    swl_f32 M33;
	};

	swl_v4 V[4];
	swl_m4a M;
    };
} swl_m4;

swl_m4 swl_m4_ortho(swl_f32 l, swl_f32 r, swl_f32 b, swl_f32 t, swl_f32 n, swl_f32 f);
swl_m4 swl_m4_translate_identity(swl_v3 tr);
swl_m4 swl_m4_mul(swl_m4 m1, swl_m4 m2);

void
_swl_framebuffer_create_texture(SwlWindow* pWindow, swl_u32 framebuffer, swl_u32* pColorAttachment, swl_u32* pDepthAttachment, swl_i32 isMultisampled, swl_i32 samplesCount)
{
    swl_i32 w = pWindow->Size.Width,
	h = pWindow->Size.Height;

    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);

#define TextureParams(textureType)					\
    {									\
	glTexParameteri(textureType, GL_TEXTURE_MIN_FILTER, GL_LINEAR); \
	glTexParameteri(textureType, GL_TEXTURE_MAG_FILTER, GL_LINEAR); \
	glTexParameteri(textureType, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); \
	glTexParameteri(textureType, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); \
    }

    swl_i32 textureType = isMultisampled ? GL_TEXTURE_2D_MULTISAMPLE : GL_TEXTURE_2D;
    swl_u32 colorAttachment, depthAttachment;
    glCreateTextures(textureType, 1, &colorAttachment);
    glBindTexture(textureType, colorAttachment);
    if (isMultisampled)
    {
	glTexImage2DMultisample(textureType, samplesCount, GL_RGBA8, w, h, GL_TRUE);
    }
    else
    {
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	TextureParams(textureType);
    }
    glBindTexture(textureType, 0);

    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, textureType, colorAttachment, 0);

    // DOCS: Depth
    glCreateTextures(textureType, 1, &depthAttachment);
    glBindTexture(textureType, depthAttachment);
    if (isMultisampled)
    {
	glTexImage2DMultisample(textureType, samplesCount, GL_DEPTH24_STENCIL8, w, h, GL_TRUE);
    }
    else
    {
	glTexImage2D(textureType, 0, GL_DEPTH24_STENCIL8, w, h, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, NULL);
	TextureParams(textureType);
    }
    glBindTexture(textureType, 0);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, textureType, depthAttachment, 0);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    *pColorAttachment = colorAttachment;
    *pDepthAttachment = depthAttachment;

}

void
_swl_framebuffer_create(SwlWindow* pWindow, swl_u32* pFramebuffer, swl_u32* pColorAttachment, swl_u32* pDepthAttachment, swl_i32 isMultisampled, swl_i32 samplesCount)
{
    swl_i32 w = pWindow->Size.Width,
	h = pWindow->Size.Height;

    swl_u32 framebuffer;
    glCreateFramebuffers(1, &framebuffer);

    _swl_framebuffer_create_texture(pWindow, framebuffer, pColorAttachment, pDepthAttachment, isMultisampled, samplesCount);

    *pFramebuffer = framebuffer;
}

swl_i32
swl_renderer_texture_create(void* pData, swl_v2 size, swl_i32 channels)
{
    GLenum dataFormat, internalFormat;
    if (channels == 1)
    {
#if 1
	internalFormat = GL_RED;
	dataFormat = GL_RED;
#else
	internalFormat = GL_ALPHA;
	dataFormat = GL_ALPHA;
#endif
    }
    if (channels == 3)
    {
	internalFormat = GL_RGB;
	dataFormat = GL_RGB;
    }
    else if (channels == 4)
    {
	internalFormat = GL_RGBA;
	dataFormat = GL_RGBA;
    }

    swl_i32 width = size.X, height = size.Y;

    swl_u32 textureId;
    glCreateTextures(GL_TEXTURE_2D, 1, &textureId);
    glBindTexture(GL_TEXTURE_2D, textureId);
    glTexParameteri(textureId, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(textureId, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(textureId, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE/*GL_REPEAT*/);
    // y clamp to edge
    glTexParameteri(textureId, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE/*GL_REPEAT*/);
    glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, dataFormat, GL_UNSIGNED_BYTE, pData);
    glGenerateMipmap(GL_TEXTURE_2D);

    glBindTexture(GL_TEXTURE_2D, 0);

    return textureId;
}

swl_u32
_swl_shader_compile(const char* pVertexShader, const char* pFragmentShader)
{

#define gle(f) ({f;swl_i32 error = glGetError();if (error > 0)printf("Error: f:%s %d - %s\n", #f, error, gl_error_to_str(error));})

    swl_u32 vertexShader = glCreateShader(GL_VERTEX_SHADER);
    gle(glShaderSource(vertexShader, 1, &pVertexShader, NULL););
    gle(glCompileShader(vertexShader););
    {
	swl_i32 is_vert_success = 1;
	glGetProgramiv(vertexShader, GL_COMPILE_STATUS, &is_vert_success);
	if (!is_vert_success)
	{
	    char infoLog[512] = {};
	    glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
	    printf("Vert-InfoLog: %s\n", infoLog);
	}
    }

    swl_u32 fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader, 1, &pFragmentShader, NULL);
    glCompileShader(fragmentShader);
    {
	swl_i32 is_frag_success = 1;
	glGetProgramiv(fragmentShader, GL_COMPILE_STATUS, &is_frag_success);
	if (!is_frag_success)
	{
#define tsize 2048
	    char infoLog[tsize] = {};
	    glGetShaderInfoLog(fragmentShader, tsize, NULL, infoLog);
	    printf("Frag-InfoLog: %s\n", infoLog);
	}
    }

    swl_u32 shaderProg = glCreateProgram();
    glAttachShader(shaderProg, vertexShader);
    glAttachShader(shaderProg, fragmentShader);
    glLinkProgram(shaderProg);

    glDetachShader(shaderProg, vertexShader);
    glDetachShader(shaderProg, fragmentShader);
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    {
	swl_i32 success;
	glGetProgramiv(shaderProg, GL_LINK_STATUS, &success);
	if (!success)
	{
	    char infoLog[512] = {};
	    glGetProgramInfoLog(shaderProg, 512, NULL, infoLog);
	    printf("GL_LINK_STATUS: InfoLog: %s\n", infoLog);
	}
    }

    return shaderProg;
}

swl_u32
_swl_gl_compile_shader()
{
    const char* pVertexShader =
	"#version 330 core\n"
	"\n"
	"layout (location = 0) in vec3 Position;    \n"
	"layout (location = 1) in vec4 Color;       \n"
	"layout (location = 2) in vec2 Uv;          \n"
	"layout (location = 3) in vec2 Indices;     \n"
	"                                             \n"
	"out vec4 v_Color;                            \n"
	"out vec2 v_Uv;                               \n"
	"flat out int v_TextureIndex;                 \n"
	"flat out int v_IsFont;                       \n"
	"                                             \n"
	"uniform mat4 u_VP;                           \n\n"

	"void main()\n"
	"{\n"
	"    gl_Position = u_VP * vec4(Position,1.0); \n"
	"    v_Color = Color;                         \n"
	"    v_Uv = Uv;                               \n"
	"    v_TextureIndex = int(Indices.x);         \n"
	"    v_IsFont = int(Indices.y);               \n"
	"}\0";

    const char* pFragmentShader =
	"#version 330 core\n"
	"\n"
	"out vec4 Color;\n\n"

	"in vec4 v_Color;            \n"
	"in vec2 v_Uv;               \n"
	"flat in int v_TextureIndex; \n"
	"flat in int v_IsFont;       \n\n"

	"uniform float u_Width;            \n"
	"uniform float u_EdgeTransition;   \n"
	"uniform sampler2D u_Textures[10]; \n\n"

	"void main()                       \n"
	"{                                 \n"
	"    vec4 text;"

	"switch (v_TextureIndex)           \n"
	"{\n"

	"case 0: text = texture(u_Textures[0], v_Uv); break; \n"
	"case 1: text = texture(u_Textures[1], v_Uv); break; \n"
	"case 2: text = texture(u_Textures[2], v_Uv); break; \n"
	"case 3: text = texture(u_Textures[3], v_Uv); break; \n"
	"case 4: text = texture(u_Textures[4], v_Uv); break; \n"
	"case 5: text = texture(u_Textures[5], v_Uv); break; \n"
	"case 6: text = texture(u_Textures[6], v_Uv); break; \n"
	"case 7: text = texture(u_Textures[7], v_Uv); break; \n"
	"case 8: text = texture(u_Textures[8], v_Uv); break; \n"
	"case 9: text = texture(u_Textures[9], v_Uv); break; \n"

	"\n"
	"}\n\n"

	"if (v_IsFont == 1)                 \n"
	"{                                  \n"
	"    float dist = 1.0 - text.r;\n"
	"    float alpha = 1.0 - smoothstep(u_Width, u_Width + u_EdgeTransition, dist);\n"
#if 1
	"if (alpha < 0.001) {                 \n"
	"    discard;                       \n"
	"}                                  \n"
	"else {                             \n"
	"    Color = vec4(v_Color.xyz, alpha);\n"
	"}                                  \n"
#else
	"    Color = vec4(v_Color.xyz, 1);\n"

#endif

	"}\n"
	"else                               \n"
	"{                                  \n"
	"    Color = text * v_Color;        \n"
	"}\n"
	/* "Color.r = v_IsFont;\n" */
	/* "Color.g = 0;\n" */
	/* "Color.b = 0;\n" */
	/* "Color.a = 1;\n" */

	"                                   \n"
	"}\0";

    swl_u32 shaderProg = _swl_shader_compile(pVertexShader, pFragmentShader);
    return shaderProg;
}

swl_u32
_swl_gl_compile_framebuffer_shader()
{
    const char* pVertexShader =
	"#version 330 core\n"
	"\n"
	"layout (location = 0) in vec3 Position;    \n"
	"layout (location = 1) in vec2 Uv;          \n"

	"out vec2 v_Uv;\n\n"

	"void main()\n"
	"{\n"
	"    gl_Position =  vec4(Position,1.0); \n"
	"    v_Uv = Uv; \n"
	"}\0";

    const char* pFragmentShader =
	"#version 330 core\n"
	"\n"
	"out vec4 Color;\n\n"
	"in vec2 v_Uv;\n\n"

	"uniform sampler2D u_FramebufferTexture; \n\n"

	"void main()                                     \n"
	"{                                               \n"
	"    Color = texture(u_FramebufferTexture, v_Uv);\n"
	"}\0";

    swl_u32 shaderProg = _swl_shader_compile(pVertexShader, pFragmentShader);
    return shaderProg;
}

static void*
_swl_file_read(const char* pPath, swl_size* pSize)
{
    FILE* pFile = fopen(pPath, "r");
    if (pFile == NULL)
    {
	*pSize = 0;
	return NULL;
    }

    fseek(pFile, 0, SEEK_END);
    swl_size fileSize = ftell(pFile);
    fseek(pFile, 0, SEEK_SET);

    void* pBuf = gSwlInit.MemoryAllocate(fileSize);
    fread(pBuf, fileSize, 1, pFile);
    *pSize = fileSize;

    return pBuf;
}

typedef struct SwlCharChar
{
    swl_v2 UV;
    swl_v2 Size;
    swl_f32 FullWidth;
    swl_f32 FullHeight;
    swl_i32 XOffset;
    swl_i32 YOffset;
    swl_i32 Character;
} SwlCharChar;

typedef struct SwlGlyphProcessingSetting
{
    swl_u8* bitmap;
    stbtt_fontinfo Info;

    swl_f32 Scale;
    swl_f32 pixelDistScale;

    swl_i32 bitmapWidth;  //= 512,
    swl_i32 bitmapHeight; //= 512,
    swl_i32 lineHeight;   //= settings.FontSize;

    swl_i32 padding;
    swl_i32 onEdgeValue; /* DOCS: 0..255 */
    swl_i32 globalXOffset;
    swl_i32 globalYOffset;
    swl_i32 maxHeight;
    swl_i32 uvy;
    swl_i32 globalMaxHeight;

    swl_i32 i;

    SwlCharChar* charChars;

} SwlGlyphProcessingSetting;

static void
_swl_font_glyph_processing(SwlGlyphProcessingSetting* pSet, swl_i32 unicode)
{
    swl_i32 w, h, xoff, yoff;

    swl_u8* singleCharacter = stbtt_GetCodepointSDF(&pSet->Info, pSet->Scale, unicode/*(i32)L'А'*/, pSet->padding, pSet->onEdgeValue, pSet->pixelDistScale, &w, &h, &xoff, &yoff);
    swl_i32 offset = 0, roffset = 0;

    if ((pSet->globalXOffset + w) >= pSet->bitmapWidth)
    {
	pSet->globalXOffset = 0;
	pSet->globalYOffset += pSet->maxHeight * pSet->bitmapWidth;
	pSet->uvy += pSet->maxHeight;
	pSet->maxHeight = 0;
    }

    if (h > pSet->maxHeight)
    {
	pSet->maxHeight = h;
    }

    if (h > pSet->globalMaxHeight)
    {
	pSet->globalMaxHeight = h;
    }

    swl_i32 ascent, descent, lineGap;
    stbtt_GetFontVMetrics(&pSet->Info, &ascent, &descent, &lineGap);

    SwlCharChar charchar = {
	.UV = {
	    .X = ((swl_f32) pSet->globalXOffset) / pSet->bitmapWidth,
	    .Y = ((swl_f32) pSet->uvy) / pSet->bitmapHeight
	},
	.Size = {
	    .X  = ((swl_f32) w) / pSet->bitmapWidth,
	    .Y = ((swl_f32) h) / pSet->bitmapHeight
	},
	.FullWidth = w,
	.FullHeight = h,
	.XOffset = xoff,
	.YOffset = h + yoff,
	.Character = unicode,
    };
    /* printf("%lc: %d\n", i, xoff); */
    array_push(pSet->charChars, charchar);

    for (swl_i32 j = 0; j < h; ++j)
    {
	memcpy(pSet->bitmap + pSet->globalXOffset + pSet->globalYOffset + offset, singleCharacter + roffset, w);
	offset += pSet->bitmapWidth;
	roffset += w;
    }

    stbtt_FreeSDF(singleCharacter, NULL);

    pSet->globalXOffset += w;

}

typedef struct SwlCharRecord
{
    i32 Key;
    SwlCharChar Value;
} SwlCharRecord;

typedef struct SwlFont
{
    SwlCharRecord* hCharTable;
    void* pBitmap;
    swl_i32 FontTextureId;
    swl_i32 Index;
    swl_v2 Size;
    swl_i32 FontSize;
} SwlFont;

static SwlFont gSwlFont = {};

void*
swl_renderer_font_get_bitmap()
{
    return gSwlFont.pBitmap;
}

static void
swl_stbi__vertical_flip(void* image, int w, int h, int bytes_per_pixel)
{
    int row;
    size_t bytes_per_row = (size_t)w * bytes_per_pixel;
    u8 temp[2048];
    u8 *bytes = (u8 *)image;

    for (row = 0; row < (h>>1); row++) {
	u8 *row0 = bytes + row*bytes_per_row;
	u8 *row1 = bytes + (h - row - 1)*bytes_per_row;
	// swap row0 with row1
	size_t bytes_left = bytes_per_row;
	while (bytes_left) {
	    size_t bytes_copy = (bytes_left < sizeof(temp)) ? bytes_left : sizeof(temp);
	    memcpy(temp, row0, bytes_copy);
	    memcpy(row0, row1, bytes_copy);
	    memcpy(row1, temp, bytes_copy);
	    row0 += bytes_copy;
	    row1 += bytes_copy;
	    bytes_left -= bytes_copy;
	}
    }
}

void
swl_renderer_font_load(SwlFontSettings* pSettings)
{
    const char* pFontPath = pSettings->pFontPath;
    swl_size bitmapWidth  = pSettings->BitmapSize.X;
    swl_size bitmapHeight = pSettings->BitmapSize.Y;
    swl_i32 lineHeight = pSettings->FontSize;

    swl_size fileSize;
    void* pFileData = _swl_file_read(pFontPath, &fileSize);

    stbtt_fontinfo info;
    if (!stbtt_InitFont(&info, pFileData, 0))
    {
	vassert(0 && "Font init failed!");
    }

    swl_f32 scale = stbtt_ScaleForPixelHeight(&info, lineHeight);

    swl_size bitmapSize = 4*bitmapWidth * bitmapHeight;
    void* pBitmap = gSwlInit.MemoryAllocate(bitmapSize);

    swl_f32 padding = pSettings->Padding; // 2;
    swl_f32 onEdgeValue = pSettings->OnEdgeValue; // 155 /*0..255*/;
    swl_f32 pixelDistScale = onEdgeValue / padding;

    SwlGlyphProcessingSetting glyphSet = {
	.bitmap = pBitmap,
	.Info = info,
	.Scale = scale,
	.pixelDistScale = pixelDistScale,

	.bitmapWidth = bitmapWidth,
	.bitmapHeight = bitmapHeight,
	.lineHeight = lineHeight,
	.padding = padding,
	.onEdgeValue = onEdgeValue, /* 0..255 */
    };

    for (swl_i32 j = 0; j < pSettings->CodePointsCount; ++j)
    {
	swl_i32 unicode = pSettings->aCodePoints[j];
	_swl_font_glyph_processing(&glyphSet, unicode);
    }

    swl_i32 maxYOffset = -1.0f;
    SwlCharRecord* charTable = NULL;
    for (swl_i32 i = 0; i < array_count(glyphSet.charChars); ++i)
    {
	SwlCharChar charchar = glyphSet.charChars[i];
	if (charchar.YOffset > maxYOffset)
	{
	    maxYOffset = charchar.YOffset;
	}

	swl_i32 code = charchar.Character;
	hash_put(charTable, code, charchar);
    }

    //swl_stbi__vertical_flip(pBitmap, glyphSet.bitmapWidth, glyphSet.bitmapHeight, 1);

    swl_i32 fontId = swl_renderer_texture_create(pBitmap, (swl_v2){glyphSet.bitmapWidth, glyphSet.bitmapHeight}, 1);

    SwlFont fontInfo = {
	//.Name = string(path_get_name(settings.Path)),
	.hCharTable = charTable,
	.pBitmap = pBitmap,
	.Size = { bitmapWidth , bitmapHeight },
	.FontSize = lineHeight,
	.FontTextureId = fontId,
	.Index = -1,
    };

    gSwlFont = fontInfo;
}

#define SwlOffsetOf(T, field) (swl_size)(&(((T*)0)->field))

SwlRenderPipe
_swl_2d_pipe_create(SwlWindow* pWindow)
{
    const swl_i32 vertexBufferSize = MB(10);
    const swl_i32 indexBufferSize = MB(10);

    swl_u32 vertexBufferId;
    glGenBuffers(1, &vertexBufferId);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId);
    glBufferData(GL_ARRAY_BUFFER, vertexBufferSize, NULL /*Not set data yet*/, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    swl_u32 indexBufferId;
    glGenBuffers(1, &indexBufferId);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferId);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexBufferSize, NULL /*Not set data yet*/, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    swl_u32 vertexArray;
    glGenVertexArrays(1, &vertexArray);
    glBindVertexArray(vertexArray);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferId);
    {
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, 0, sizeof(SwlVertex2D), 0);

	glEnableVertexAttribArray(1);
	swl_size offset = SwlOffsetOf(SwlVertex2D, Color);
	glVertexAttribPointer(1, 4, GL_FLOAT, 0, sizeof(SwlVertex2D), (const void*) offset);

	glEnableVertexAttribArray(2);
	swl_size coff = SwlOffsetOf(SwlVertex2D, Uv);
	glVertexAttribPointer(2, 2, GL_FLOAT, 0, sizeof(SwlVertex2D), (const void*) coff);

	glEnableVertexAttribArray(3);
	swl_size tioff = SwlOffsetOf(SwlVertex2D, Indices);
	glVertexAttribPointer(3, 2, GL_FLOAT, 0, sizeof(SwlVertex2D), (const void*) tioff);
    }

    // DOCS: Unbind everything
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    // very small texture
#define DefTexW 8
#define DefTexH 8
#define DefTexSize DefTexW*DefTexH
    swl_i32 byteArr[DefTexSize] = {};
    for (swl_i32 i = 0; i < DefTexSize; ++i)
	byteArr[i] = 255 << 24 | 255 << 16 | 255 << 8 | 255;
    //                  A         B          G      R
    swl_i32 defaultTexture = swl_renderer_texture_create((void*)byteArr, (swl_v2){DefTexW,DefTexH}, 4);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, defaultTexture);

    swl_u32 shaderProg = _swl_gl_compile_shader();
    swl_i32 uVpLocation = glGetUniformLocation(shaderProg, "u_VP");

    // Create orthocamera
    swl_m4 ortho = swl_m4_ortho(0, pWindow->Size.Width, 0, pWindow->Size.Height, 0, 1000);
    swl_m4 view = swl_m4_translate_identity((swl_v3){0,0,1.0f});
    swl_m4 viewProj = swl_m4_mul(ortho, view);

    // write some draw stuff
    glUseProgram(shaderProg);
    glUniformMatrix4fv(uVpLocation, 1, 0, &viewProj.M[0][0]);

    swl_i32 widthLocation = glGetUniformLocation(shaderProg, "u_Width");
    swl_i32 edgeLocation = glGetUniformLocation(shaderProg, "u_EdgeTransition");
    swl_i32 textLocation = glGetUniformLocation(shaderProg, "u_Textures");

    if (widthLocation == -1 || edgeLocation == -1 || textLocation == -1)
    {
	printf("Uniform setup!!!\n\n\n");
    }

    SwlRenderPipe renderPipe = {
	.VertexBuffer = vertexBufferId,
	.IndexBuffer = indexBufferId,
	.VertexArray = vertexArray,
	.DefaultTexture = defaultTexture,
	.TextureOffset = 1,
	.Shader = shaderProg,
	.WidthLocation = widthLocation,
	.EdgeTransitionLocation = edgeLocation,
	.TexturesLocation = textLocation,
    };

    return renderPipe;
}

SwlFramePipe
_swl_frame_pipe_create(SwlWindow* pWindow)
{
#if 1
    f32 v = 1.0f;
#else
    f32 v = pWindow->Size.Width / pWindow->Size.Height;
#endif

    swl_f32 vertices[] = {
	/*vertices       uv  */
	-v, -v, 0,       0, 0,
	-v,  v, 0,       0, 1,
	 v,  v, 0,       1, 1,

	 v,  v, 0,       1, 1,
	 v, -v, 0,       1, 0,
	-v, -v, 0,       0, 0,
    };
    swl_u32 vertexBuffer;
    glGenBuffers(1, &vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), NULL /*Not set data yet*/, GL_STATIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);

    swl_u32 vertexArray;
    glGenVertexArrays(1, &vertexArray);
    glBindVertexArray(vertexArray);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    {
	typedef struct FrameVert
	{
	    swl_v3 Pos;
	    swl_v2 Uv;
	} FrameVert;
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, 0, sizeof(FrameVert), 0);

	glEnableVertexAttribArray(1);
	swl_size coff = SwlOffsetOf(FrameVert, Uv);
	glVertexAttribPointer(1, 2, GL_FLOAT, 0, sizeof(FrameVert), (const void*) coff);
    }
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    swl_u32 shader = _swl_gl_compile_framebuffer_shader();

    //DOCS: Create framebuffer
    swl_u32 framebuffer, color, depth;
    _swl_framebuffer_create(pWindow, &framebuffer, &color, &depth, 0, 0);

    SwlFramePipe framePipe = {
	.Shader = shader,
	.Vertex = vertexBuffer,
	.VertexArray = vertexArray,
	.Framebuffer = framebuffer,
	.ColorAttachment = color,
	.DepthAttachment = depth,
    };

    return framePipe;
}


void
swl_renderer_resize(SwlWindow* pWindow)
{
    SwlFramePipe* pFramePipe = &gGl.FramePipe;

    swl_u32 color, depth;
    _swl_framebuffer_create_texture(pWindow, pFramePipe->Framebuffer, &color, &depth, 0, 0);

    f32 w = pWindow->Size.Width;
    f32 h = pWindow->Size.Height;
    glViewport(0, 0, w, h);

    pFramePipe->ColorAttachment = color;
    pFramePipe->DepthAttachment = depth;

    SwlX11Backend* pBack = (SwlX11Backend*) pWindow->pBackend;
    XFlush(pBack->pDisplay);
}

void
swl_renderer_create(SwlWindow* pWindow)
{
    _swl_gl_load(pWindow);

    // DOCS: Create vertex buffer
    SwlRenderPipe pipe2d = _swl_2d_pipe_create(pWindow);
    SwlFramePipe pipeFrame = _swl_frame_pipe_create(pWindow);

    SwlGl swlGl = (SwlGl) {
	.RenderPipe = pipe2d,
	.FramePipe = pipeFrame,
    };

    gGl = swlGl;
}

void
swl_renderer_set_swap_iterval(SwlWindow* pWindow, swl_i32 interval)
{
    SwlX11Backend* pBack = (SwlX11Backend*) pWindow->pBackend;
    swl_i32 defaultScreen = DefaultScreen(pBack->pDisplay);
    gGlContext.SwapInterval((void*)pBack->pDisplay, defaultScreen, interval);
}

void
swl_renderer_clear_color(SwlWindow* pWindow, swl_f32 r, swl_f32 g, swl_f32 b, swl_f32 a)
{
    gGl.ClearColor.R = r;
    gGl.ClearColor.G = g;
    gGl.ClearColor.B = b;
    gGl.ClearColor.A = a;
}

void
swl_renderer_draw_rectangle(SwlWindow* pWindow, swl_v3 position, swl_v2 size, swl_v4 color)
{
    SwlGl* pGl = &gGl;
    SwlRenderPipe* pRenderPipe = &gGl.RenderPipe;

    swl_size voffset = pRenderPipe->VertexOffset * sizeof(SwlVertex2D);
    swl_size ioffset = pRenderPipe->IndexOffset * sizeof(swl_u32);

    swl_f32 x = position.X, y = position.Y, z = position.Z,
	w = size.X, h = size.Y;

    glBindBuffer(GL_ARRAY_BUFFER, pRenderPipe->VertexBuffer);
    // note: set buffer's data
    SwlVertex2D vertices[] = {
	[0] = (SwlVertex2D) {.Position={x, y, z},.Color=color},
	[1] = (SwlVertex2D) {.Position={x, y + h, z},.Color=color},
	[2] = (SwlVertex2D) {.Position={x + w, y + h, z},.Color=color},
	[3] = (SwlVertex2D) {.Position={x + w, y, z},.Color=color},
    };
    glBufferSubData(GL_ARRAY_BUFFER, voffset, sizeof(vertices), vertices);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pRenderPipe->IndexBuffer);
    swl_u32 ind = pRenderPipe->VertexOffset;
    swl_u32 indices[] = {
	0+ind, 1+ind, 2+ind, 2+ind, 3+ind, 0+ind
    };
    glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, ioffset, sizeof(indices), indices);

    pRenderPipe->VertexOffset += 4;
    pRenderPipe->IndexOffset  += 6;

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void
swl_renderer_draw_texture_uv(SwlWindow* pWindow, swl_v3 position, swl_v2 size, swl_v4 color, swl_i32 texture, swl_v2 uvs[4])
{
    SwlGl* pGl = &gGl;
    SwlRenderPipe* pRenderPipe = &gGl.RenderPipe;

    swl_size voffset = pRenderPipe->VertexOffset * sizeof(SwlVertex2D);
    swl_size ioffset = pRenderPipe->IndexOffset * sizeof(swl_u32);

    swl_f32 x = position.X, y = position.Y, z = position.Z,
	w = size.X, h = size.Y;

    glBindBuffer(GL_ARRAY_BUFFER, pRenderPipe->VertexBuffer);
    // note: set buffer's data
    swl_i32 textInd = pRenderPipe->TextureOffset;
    ++pRenderPipe->TextureOffset;

    const swl_f32 sx = 0.0f, ex = 0.5f,
	sy = 0.5f, ey = 1.0f;

    SwlVertex2D vertices[] = {
	[0] = (SwlVertex2D) {
	    .Position = {x, y, z},
	    .Color = color,
	    .Uv = uvs[0],
	    .Indices = {textInd, -1337},
	},
	[1] = (SwlVertex2D) {
	    .Position = {x, y + h, z},
	    .Color = color,
	    .Uv = uvs[1],
	    .Indices = {textInd, -1337},
	},
	[2] = (SwlVertex2D) {
	    .Position = {x + w, y + h, z},
	    .Color = color,
	    .Uv = uvs[2],
	    .Indices = {textInd, -1337},
	},
	[3] = (SwlVertex2D) {
	    .Position={x + w, y, z},
	    .Color=color,
	    .Uv= uvs[3],
	    .Indices = {textInd, -1337},
	},
    };
    glBufferSubData(GL_ARRAY_BUFFER, voffset, sizeof(vertices), vertices);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pRenderPipe->IndexBuffer);
    swl_u32 ind = pRenderPipe->VertexOffset;
    swl_u32 indices[] = {
	0+ind, 1+ind, 2+ind, 2+ind, 3+ind, 0+ind
    };
    glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, ioffset, sizeof(indices), indices);

    pRenderPipe->VertexOffset += 4;
    pRenderPipe->IndexOffset  += 6;

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    glActiveTexture(GL_TEXTURE0 + textInd);
    glBindTexture(GL_TEXTURE_2D, texture);

}

void
swl_renderer_draw_texture(SwlWindow* pWindow, swl_v3 position, swl_v2 size, swl_v4 color, swl_i32 texture)
{
    swl_v2 uvs[4] = {
	[0] = (swl_v2) { 0, 0 },
	[1] = (swl_v2) { 0, 1 },
	[2] = (swl_v2) { 1, 1 },
	[3] = (swl_v2) { 1, 0 },
    };

    swl_renderer_draw_texture_uv(pWindow, position, size, color, texture, uvs);
}

void
swl_renderer_draw_text(SwlWindow* pWindow, swl_i32* pText, swl_size textLength, swl_v3 position, swl_v4 color)
{
    SwlGl* pGl = &gGl;
    SwlRenderPipe* pRenderPipe = &gGl.RenderPipe;

    swl_size voffset = pRenderPipe->VertexOffset * sizeof(SwlVertex2D);
    swl_size ioffset = pRenderPipe->IndexOffset * sizeof(swl_u32);

    swl_f32 x = position.X, y = position.Y; //, w = size.X, h = size.Y;

    glBindBuffer(GL_ARRAY_BUFFER, pRenderPipe->VertexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pRenderPipe->IndexBuffer);

    // note: set buffer's data
    if (gSwlFont.Index == -1)
    {
	glActiveTexture(GL_TEXTURE0 + pRenderPipe->TextureOffset);
	glBindTexture(GL_TEXTURE_2D, gSwlFont.FontTextureId);

	// todo: set uniform
	glUniform1f(pRenderPipe->WidthLocation, 0.45f);
	glUniform1f(pRenderPipe->EdgeTransitionLocation, 0.24f);

	gSwlFont.Index = pRenderPipe->TextureOffset;
	++pRenderPipe->TextureOffset;
    }

    swl_i32 textInd = gSwlFont.Index;

    SwlCharRecord* hTable = gSwlFont.hCharTable;

    swl_f32 offsetX = 0.0f, firstCharHeight,
	vertexOffset = pRenderPipe->VertexOffset,
	indexOffset = pRenderPipe->IndexOffset,
	vBufferOffset = voffset, iBufferOffset = ioffset;
    for (swl_i32 i = 0; i < textLength; ++i)
    {
	swl_i32 unicode = pText[i];

	swl_f32 xhOffset = 0.0f,
	    x0 = 0.0f, x1 = 0.0f,
	    y0 = 0.0f, y1 = 0.0f;

	if (unicode == ' ')
	{
	    // todo: impl space width calculation
	    SwlCharChar cc = hash_get(hTable, (u32)'?');
	    xhOffset = cc.FullWidth;
	    offsetX += xhOffset;
	    if (i == 0)
	    {
		firstCharHeight = cc.FullHeight;
	    }

	    continue;
	}

	if (hash_geti(hTable, unicode) == -1)
	{
	    continue;
	}

	SwlCharChar charChar = hash_get(hTable, unicode);
	if (i == 0)
	{
	    firstCharHeight = charChar.FullHeight;
	}

	swl_f32 scale = 1;
	swl_f32 yCharOffset = scale * charChar.YOffset;
	// swl_f32 vOffset = charChar.FullHeight - yCharOffset;
	xhOffset = charChar.FullWidth;

	x0 = position.X + offsetX;
	x1 = x0 + xhOffset;

	// note: it works
	y1 = position.Y + yCharOffset - 14;
	y0 = y1 - charChar.FullHeight; //firstCharHeight;

	swl_f32 startX = charChar.UV.X;
	swl_f32 startY = charChar.UV.Y;
	swl_f32 endX   = charChar.UV.X + charChar.Size.X;
	swl_f32 endY   = charChar.UV.Y + charChar.Size.Y;

	offsetX += xhOffset;

	SwlVertex2D vertices[] = {
	    [0] = (SwlVertex2D) {
		.Position = {x0, y0, position.Z},
		.Color = color,
		.Uv = {startX, startY},
		.Indices = {textInd, 1},
	    },
	    [1] = (SwlVertex2D) {
		.Position = {x0, y1, position.Z},
		.Color = color,
		.Uv = {startX, endY},
		.Indices = {textInd, 1},
	    },
	    [2] = (SwlVertex2D) {
		.Position = {x1, y1, position.Z},
		.Color = color,
		.Uv = {endX, endY},
		.Indices = {textInd, 1},
	    },
	    [3] = (SwlVertex2D) {
		.Position={x1, y0, position.Z},
		.Color=color,
		.Uv={endX, startY},
		.Indices = {textInd, 1},
	    },
	};

	glBufferSubData(GL_ARRAY_BUFFER, vBufferOffset, sizeof(vertices), vertices);

	swl_u32 ind = vertexOffset;
	swl_u32 indices[] = {
	    0+ind, 1+ind, 2+ind, 2+ind, 3+ind, 0+ind
	};
	glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, iBufferOffset, sizeof(indices), indices);

	vertexOffset  += 4;
	indexOffset   += 6;
	vBufferOffset += sizeof(vertices);
	iBufferOffset += sizeof(indices);
    }

    pRenderPipe->VertexOffset = vertexOffset;
    pRenderPipe->IndexOffset  = indexOffset;

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void
swl_renderer_flush(SwlWindow* pWindow)
{
    SwlX11Backend* pBack = (SwlX11Backend*) pWindow->pBackend;

    SwlGl gl = gGl;
    SwlRenderPipe* pRenderPipe = &gGl.RenderPipe;
    SwlFramePipe* pFramePipe   = &gGl.FramePipe;

    glBindFramebuffer(GL_FRAMEBUFFER, pFramePipe->Framebuffer);
    {
	glEnable(GL_DEPTH_TEST);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(gl.ClearColor.R, gl.ClearColor.G, gl.ClearColor.B, gl.ClearColor.A);

	swl_i32 arr[] = {0,1,2,3,4,5,6,7,8,9};
	glUniform1iv(pRenderPipe->TexturesLocation, 10, arr);

	glUseProgram(pRenderPipe->Shader);
	glBindVertexArray(pRenderPipe->VertexArray);
	glBindBuffer(GL_ARRAY_BUFFER, pRenderPipe->VertexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pRenderPipe->IndexBuffer);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, pRenderPipe->DefaultTexture);

	glDrawElements(GL_TRIANGLES, pRenderPipe->IndexOffset, GL_UNSIGNED_INT, NULL);

	pRenderPipe->VertexOffset = 0;
	pRenderPipe->IndexOffset = 0;
	pRenderPipe->TextureOffset = 1;
	gSwlFont.Index = -1;

	glUseProgram(0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    {
	glDisable(GL_DEPTH_TEST);

	glUseProgram(pFramePipe->Shader);
	glBindVertexArray(pFramePipe->VertexArray);
	glBindBuffer(GL_ARRAY_BUFFER, pFramePipe->Vertex);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, pFramePipe->ColorAttachment);

	glDrawArrays(GL_TRIANGLES, 0, 6);

	// Unbind all
	glUseProgram(0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    gGlContext.SwapBuffers((void*)pBack->pDisplay, pBack->Window);
}

void
swl_renderer_destroy(SwlWindow* pWindow)
{
    //todo:
    /* if (pLibGL) */
/*     { */
/* #if SWL_LINUX == 1 */
/*	dlclose(pLibGL); */
/* #elif SWL_WINDOWS == 1 */
/*	FreeLibrary((HMODULE) libGL); */
/* #endif */
/*	pLibGL = NULL; */
/*     } */
}

swl_f32
swl_renderer_get_text_width(SwlWindow* pWindow, swl_i32* pText, swl_i32 textLength)
{
    swl_f32 width = 0.0f;

    /* typedef struct SwlFont */
    /* { */
    /*     SwlCharRecord* hCharTable; */
    /*     void* pBitmap; */
    /*     swl_i32 FontTextureId; */
    /*     swl_i32 Index; */
    /*     swl_v2 Size; */
    /*     swl_i32 FontSize; */
    /* } SwlFont; */

    SwlCharRecord* hTable = gSwlFont.hCharTable;

    for (swl_i32 i = 0; i < textLength; ++i)
    {
	swl_i32 code = pText[i];

	if (code == (swl_i32)' ')
	{
	    SwlCharChar cc = hash_get(hTable, (u32)'?');
	    width += cc.FullWidth;
	}
	else if (hash_geti(hTable, code) != -1)
	{
	    SwlCharChar chch = hash_get(hTable, code);
	    width += chch.FullWidth;
	}

	/* v2 UV; */
	/* v2 Size; */
	/* f32 FullWidth; */
	/* f32 FullHeight; */
	/* i32 XOffset; */
	/* i32 YOffset; */
	/* i32 Character; */
    }


    return width;
}


swl_m4
swl_m4_ortho(swl_f32 l, swl_f32 r, swl_f32 b, swl_f32 t, swl_f32 n, swl_f32 f)
{
    swl_m4 rm = {};

    rm.M00 = 2.f / (r - l);
    rm.M11 = 2.f / (b - t);
    rm.M22 = 1.f / (f - n);
    rm.M30 = -(r + l) / (r - l);
    rm.M31 = -(b + t) / (b - t);
    rm.M32 = -n / (f - n);
    rm.M33 = 1.f;

    return rm;
}

swl_m4
swl_m4_translate_identity(swl_v3 tr)
{
    swl_m4 m;
    m.M00 = 1.0f; m.M01 = 0.0f; m.M02 = 0.0f; m.M03 = 0.0f;
    m.M10 = 0.0f; m.M11 = 1.0f; m.M12 = 0.0f; m.M13 = 0.0f;
    m.M20 = 0.0f; m.M21 = 0.0f; m.M22 = 1.0f; m.M23 = 0.0f;
    m.M30 = 0.0f; m.M31 = 0.0f; m.M32 = 0.0f; m.M33 = 1.0f;

    m.M30 = tr.X;
    m.M31 = tr.Y;
    m.M32 = tr.Z;
    m.M33 = 1.0f;

    return m;
}

swl_m4
swl_m4_mul(swl_m4 m1, swl_m4 m2)
{
    swl_m4 r;
    /*
      Column major order for OpenGL

      1 2 3 4     1 2 3 4
      2 2 3 3  *  2 2 3 3
      4 4 2 1     4 4 2 1
      3 3 1 4     3 3 1 4

    */

    swl_f32 l00 = m1.M00, l01 = m1.M01, l02 = m1.M02, l03 = m1.M03,
	l10 = m1.M10, l11 = m1.M11, l12 = m1.M12, l13 = m1.M13,
	l20 = m1.M20, l21 = m1.M21, l22 = m1.M22, l23 = m1.M23,
	l30 = m1.M30, l31 = m1.M31, l32 = m1.M32, l33 = m1.M33,

	r00 = m2.M00, r01 = m2.M01, r02 = m2.M02, r03 = m2.M03,
	r10 = m2.M10, r11 = m2.M11, r12 = m2.M12, r13 = m2.M13,
	r20 = m2.M20, r21 = m2.M21, r22 = m2.M22, r23 = m2.M23,
	r30 = m2.M30, r31 = m2.M31, r32 = m2.M32, r33 = m2.M33;

    // column
    r.M00 = l00 * r00 + l10 * r01 + l20 * r02 + l30 * r03;
    r.M01 = l01 * r00 + l11 * r01 + l21 * r02 + l31 * r03;
    r.M02 = l02 * r00 + l12 * r01 + l22 * r02 + l32 * r03;
    r.M03 = l03 * r00 + l13 * r01 + l23 * r02 + l33 * r03;

    r.M10 = l00 * r10 + l10 * r11 + l20 * r12 + l30 * r13;
    r.M11 = l01 * r10 + l11 * r11 + l21 * r12 + l31 * r13;
    r.M12 = l02 * r10 + l12 * r11 + l22 * r12 + l32 * r13;
    r.M13 = l03 * r10 + l13 * r11 + l23 * r12 + l33 * r13;

    r.M20 = l00 * r20 + l10 * r21 + l20 * r22 + l30 * r23;
    r.M21 = l01 * r20 + l11 * r21 + l21 * r22 + l31 * r23;
    r.M22 = l02 * r20 + l12 * r21 + l22 * r22 + l32 * r23;
    r.M23 = l03 * r20 + l13 * r21 + l23 * r22 + l33 * r23;

    r.M30 = l00 * r30 + l10 * r31 + l20 * r32 + l30 * r33;
    r.M31 = l01 * r30 + l11 * r31 + l21 * r32 + l31 * r33;
    r.M32 = l02 * r30 + l12 * r31 + l22 * r32 + l32 * r33;
    r.M33 = l03 * r30 + l13 * r31 + l23 * r32 + l33 * r33;

    return r;
}

#endif // SWL_RENDERER

#endif // SIMPLE_WINDOW_LIBRARY_H
