#include "SimpleInfo.h"

#include <Core/Types.h>
#include <stdio.h>

SystemInfo gSystemInfo = (SystemInfo) {

    .Os =
#if defined(PLATFORM_LINUX)
    OsType_Linux
#elif defined(PLATFORM_WINDOWS)
    OsType_Windows
#else
#error "Platform not supported!"
#endif
    ,

    .System = 0,/* if 1 => Big else if 0 => Little */

    .Float =
#if defined(FLOAT_32_BIT)
    FloatType_32Bit
#elif defined(FLOAT_64_BIT)
    FloatType_64Bit
#else
#error "FloatType not supported!"
#endif
    ,

    .Build =
#if defined(BUILD_DEBUG)
    BuildType_Debug
#elif defined(BUILD_RELEASE)
    BuildType_Release
#else
#error "BuildConfig not supported!"
#endif
    ,

};


const char*
os_type_to_string(OsType type)
{
    switch (type)
    {
    case OsType_Linux: return "Linux";
    case OsType_Windows: return "Windows";

    default: return "";
    }
}

const char*
system_type_to_string(SystemType type)
{
    switch (type)
    {
    case SystemType_LittleEndian: return "LittleEndian";
    case SystemType_BigEndian: return "BigEndian";

    default: return "";
    }
}

const char*
float_type_to_string(FloatType type)
{
    switch (type)
    {
    case FloatType_32Bit: return "32-bit";
    case FloatType_64Bit: return "64-bit";

    default: return "";
    }
}

const char*
build_type_to_string(BuildType type)
{
    switch (type)
    {
    case BuildType_Debug: return "Debug";
    case BuildType_Release: return "Release";

    default: return "";
    }
}

void
system_info_print()
{
    gSystemInfo.System = SYSTEM_BIG;
    SystemInfo info = gSystemInfo;
    printf("--------------------------------------\n");
    printf("| ℹ️ System info 🛡\n");
    printf("|      OS:         %s\n", os_type_to_string(info.Os));
    printf("|      System:     %s\n", system_type_to_string(info.System));
    printf("|      Float:      %s\n", float_type_to_string(info.Float));
    printf("|      Build:      %s\n", build_type_to_string(info.Build));
    printf("|            🐨 🐼\n");
    printf("_____________________________________\n");
}
