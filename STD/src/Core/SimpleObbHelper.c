#include "SimpleObbHelper.h"

#include <Core/SimpleMath.h>

void
simple_obb_helper_ss_to_ray(v2 mouse, v2 screen, m4 view, m4 proj, v3* pOrigin, v3* pDirection)
{

    v2 half = v2_new(0.5f, 0.5f);
    v2 converted = v2_mulv(v2_sub(v2_div(mouse, screen), half), 2.0f);

    v4 lRayStart_NDC = v4_new(converted.X, // [-1,+1]
			      converted.Y, // [-1,+1]
			      -1.0f, // The near plane maps to Z=-1 in Normalized Device Coordinates
			      +1.0f);

    v4 lRayEnd_NDC = v4_new(converted.X, converted.Y, 0, 1);

    // The Projection matrix goes from Camera Space to NDC.
    // So inverse(ProjectionMatrix) goes from NDC to Camera Space.
    m4 iproj = m4_inverse(proj);

    // The View Matrix goes from World Space to Camera Space.
    // So inverse(ViewMatrix) goes from Camera Space to World Space.
    m4 iview = m4_inverse(view);

    v4 lRayStart_camera = m4_mul_v4(iproj, lRayStart_NDC);
    lRayStart_camera = v4_mulv(lRayStart_camera, 1/lRayStart_camera.W);

    v4 lRayStart_world = m4_mul_v4(iview, lRayStart_camera);
    lRayStart_world = v4_mulv(lRayStart_world, 1/lRayStart_world.W);

    v4 lRayEnd_camera = m4_mul_v4(iproj, lRayEnd_NDC);
    lRayEnd_camera = v4_mulv(lRayEnd_camera, 1/lRayEnd_camera.W);

    v4 lRayEnd_world = m4_mul_v4(iview, lRayEnd_camera);
    lRayEnd_world = v4_mulv(lRayEnd_world, 1/lRayEnd_world.W);

    // Faster way (just one inverse)
    //glm::mat4 M = glm::inverse(ProjectionMatrix * ViewMatrix);
    //glm::vec4 lRayStart_world = M * lRayStart_NDC; lRayStart_world/=lRayStart_world.w;
    //glm::vec4 lRayEnd_world   = M * lRayEnd_NDC  ; lRayEnd_world  /=lRayEnd_world.w;

    v4 diff = v4_sub(lRayEnd_world, lRayStart_world);
    v3 lRayDir_world = v3_normalize(v3_v4(diff));

    *pOrigin = v3_v4(lRayStart_world);
    *pDirection = lRayDir_world;
}

i32
simple_obb_helper_is_collide(
    v3 rayOrigin,
    v3 rayDirection,     // Ray direction (NOT target position!), in world space. Must be normalize()'d.
    v3 aabbMin,          // Minimum X,Y,Z coords of the mesh when not transformed at all.
    v3 aabbMax,          // Maximum X,Y,Z coords. Often aabb_min*-1 if your mesh is centered, but it's not always the case.
    m4 pModelMatrix,       // Transformation applied to the mesh (which will thus be also applied to its bounding box)
    float* intersectionDistance // Output : distance between ray_origin and the intersection with the OBB
    )
{
    // Intersection method from Real-Time Rendering and Essential Mathematics for Games

    float tMin = 0.0f;
    float tMax = 100000.0f;

    v3 OBBposition_worldspace = v3_v4(pModelMatrix.V[3]);

    v3 delta = v3_sub(OBBposition_worldspace, rayOrigin);

    // Test intersection with the 2 planes perpendicular to the OBB's X axis
    {
	v3 xaxis = v3_v4(pModelMatrix.V[0]);
	float e = v3_dot(xaxis, delta);
	float f = v3_dot(rayDirection, xaxis);

	if ( fabs(f) > 0.001f ){ // Standard case

	    float t1 = (e+aabbMin.X)/f; // Intersection with the "left" plane
	    float t2 = (e+aabbMax.X)/f; // Intersection with the "right" plane
	    // t1 and t2 now contain distances betwen ray origin and ray-plane intersections

	    // We want t1 to represent the nearest intersection,
	    // so if it's not the case, invert t1 and t2
	    if (t1>t2){
		//Swap(t1,t2);
		float w=t1;t1=t2;t2=w; // swap t1 and t2
	    }

	    // tMax is the nearest "far" intersection (amongst the X,Y and Z planes pairs)
	    if ( t2 < tMax )
		tMax = t2;
	    // tMin is the farthest "near" intersection (amongst the X,Y and Z planes pairs)
	    if ( t1 > tMin )
		tMin = t1;

	    // And here's the trick :
	    // If "far" is closer than "near", then there is NO intersection.
	    // See the images in the tutorials for the visual explanation.
	    if (tMax < tMin )
		return 0;

	}else{ // Rare case : the ray is almost parallel to the planes, so they don't have any "intersection"
	    if(-e+aabbMin.X > 0.0f || -e+aabbMax.X < 0.0f)
		return 0;
	}
    }


    // Test intersection with the 2 planes perpendicular to the OBB's Y axis
    // Exactly the same thing than above.
    {
	v3 yaxis = v3_v4(pModelMatrix.V[1]);
	float e = v3_dot(yaxis, delta);
	float f = v3_dot(rayDirection, yaxis);

	if (fabs(f) > 0.001f)
	{

	    float t1 = (e+aabbMin.Y)/f;
	    float t2 = (e+aabbMax.Y)/f;

	    if (t1>t2){float w=t1;t1=t2;t2=w;}

	    if ( t2 < tMax )
		tMax = t2;
	    if ( t1 > tMin )
		tMin = t1;
	    if (tMin > tMax)
		return 0;

	}
	else
	{
	    if(-e+aabbMin.Y > 0.0f || -e+aabbMax.Y < 0.0f)
		return 0;
	}
    }


    // Test intersection with the 2 planes perpendicular to the OBB's Z axis
    // Exactly the same thing than above.
    {
	v3 zaxis = v3_v4(pModelMatrix.V[2]);
	float e = v3_dot(zaxis, delta);
	float f = v3_dot(rayDirection, zaxis);

	if ( fabs(f) > 0.001f )
	{

	    float t1 = (e+aabbMin.Z)/f;
	    float t2 = (e+aabbMax.Z)/f;

	    if (t1>t2){float w=t1;t1=t2;t2=w;}

	    if ( t2 < tMax )
		tMax = t2;
	    if ( t1 > tMin )
		tMin = t1;
	    if (tMin > tMax)
		return 0;

	}
	else
	{
	    if(-e+aabbMin.Z > 0.0f || -e+aabbMax.Z < 0.0f)
		return 0;
	}
    }

    *intersectionDistance = tMin;

    return 1;
}
