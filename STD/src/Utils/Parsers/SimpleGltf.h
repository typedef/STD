#ifndef SIMPLE_GLTF_H
#define SIMPLE_GLTF_H

#include <Core/Types.h>


/*
  DOCS: Better naming

  sgltf_parse_json
  sglft_new
*/


typedef struct SimpleGltfAsset
{
    const char* pCopyright;
    const char* pGenerator;
    const char* pVersion;
} SimpleGltfAsset;

// DOCS: Accessor
typedef enum SimpleGltfComponentType
{
    SimpleGltfComponentType_Byte = 5120,
    SimpleGltfComponentType_UnsignedByte = 5121,
    SimpleGltfComponentType_Short = 5122,
    SimpleGltfComponentType_UnsignedShort = 5123,
    SimpleGltfComponentType_UnsignedInt = 5125,
    SimpleGltfComponentType_Float = 5126,
} SimpleGltfComponentType;

typedef enum SimpleGltfAccessorType
{
    SimpleGltfAccessorType_Scalar = 0,
    SimpleGltfAccessorType_Vec2,
    SimpleGltfAccessorType_Vec3,
    SimpleGltfAccessorType_Vec4,
    SimpleGltfAccessorType_Mat2,
    SimpleGltfAccessorType_Mat3,
    SimpleGltfAccessorType_Mat4,
} SimpleGltfAccessorType;

typedef struct SimpleGltfAccessor
{
    i32 BufferView;
    i32 ByteOffset;
    SimpleGltfComponentType ComponentType;
    i32 Normalized; // by default false
    i32 Count;
    SimpleGltfAccessorType Type;
    real* aMax;
    real* aMin;
} SimpleGltfAccessor;

void simple_gltf_new(const char* pGltfPath);

typedef struct SimpleGltfMeshVertex
{
    v3 Position;
    v3 Normal;
    v4 Color;
    v2 Uv;
} SimpleGltfMeshVertex;

typedef struct SimpleGltfMeshIndex
{
    union
    {
	// DOCS: Save memory if possible
	// todo: create array_grow_from_raw_pointer or smth
	u16* aIndices16;
	u32* aIndices32;
    };
} SimpleGltfMeshIndex;

typedef struct SimpleGltfMesh
{
    SimpleGltfMeshVertex* aVertices;
    SimpleGltfMeshIndex Index;
} SimpleGltfMesh;





typedef struct SimpleGltfFile
{

} SimpleGltfFile;

#endif // SIMPLE_GLTF_H
