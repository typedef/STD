#include "SimpleGltf.h"

#include <Core/SimpleStandardLibrary.h>
#include "SimpleFastJson.h"


/*

  // DOCS:

  1) Lazy load

  FileApproach:
  * read file and save content in a string
  * parse it with sfj
  * read binary data *.bin

  BinaryApproach:
  * ?

  2) Actual parsing with allocating


  // One function doing all the job, for now.
  SimpleGltfFile gltfFile = simple_gltf_new(asset_model("SimpleModel/SimpleModel.gltf"));


  https://gltf-viewer.donmccurdy.com/
 */

typedef struct SimpleGltfAssetSettings
{
} SimpleGltfAssetSettings;

typedef enum SimpleGltfGpuBuffer
{
    SimpleGltfGpuBuffer_VertexBuffer = 34962,
    SimpleGltfGpuBuffer_IndexBuffer = 34963,
} SimpleGltfGpuBuffer;

static const char*
simple_gltf_gpu_buffer_to_string(SimpleGltfGpuBuffer buffer)
{
    switch (buffer)
    {
    case SimpleGltfGpuBuffer_VertexBuffer: return "VertexBuffer";
    case SimpleGltfGpuBuffer_IndexBuffer: return "IndexBuffer";
    }
}

typedef struct SimpleGltfBufferView
{
    i32 BufferIndex;
    i64 ByteLength;
    i64 ByteOffset;
    SimpleGltfGpuBuffer Target;
} SimpleGltfBufferView;

typedef struct SimpleGltfBuffer
{
    i64 ByteLength;
    const char* pUri;
} SimpleGltfBuffer;

typedef struct SimpleGltfParseInput
{
    const char* pJson;
    i64 JsonLength;
    SFJToken* aTokens;
    i64 TokensCount;
    i64 Index;
} SimpleGltfParseInput;

typedef struct SimpleGltfJsonResult
{
    SimpleArena* pArena;
    SFJToken* aTokens;
    i64 TokensCount;
    const char* pText;
    i64 TextLength;
    //SFJSetting Settings;
} SimpleGltfJsonResult;


SimpleGltfJsonResult
sgltf_json_result_parse(const char* pGltfPath)
{
    u64 size;
    char* pFileContent = file_read_string_ext(pGltfPath, &size);

    SimpleArena* pArena = simple_arena_create(2 * size);
    memory_set_arena(pArena);

    i32 tokensCnt = 2*size;
    SFJToken* aTokens = NULL;
    array_reserve(aTokens, tokensCnt);

    SFJSetting set = {
	.aTokens = aTokens,
	.pText = pFileContent,
	.TextLength = size,
	.TokensCount = tokensCnt
    };

    sfj_parse_json(&set);

    if (set.ErrorType != 0)
    {
	// handle error
	GERROR("It's failed!\n");
    }

    memory_set_arena(NULL);

    SimpleGltfJsonResult sgltfJsonResult = {
	.pArena = pArena,
	.aTokens = aTokens,
	.TokensCount = set.TokensCount,
	.pText = set.pText,
	.TextLength = set.TextLength,
    };

    return sgltfJsonResult;
}

void
sgltf_json_result_destroy(SimpleGltfJsonResult jsonResult)
{
    simple_arena_destroy(jsonResult.pArena);
}

const char*
sfj_helper_get_string(const char* pJson, SFJToken* aTokens, i64 index, i32 isAlloc, i64* pLength)
{
    SFJToken token = aTokens[index];
    i32 len = sfj_token_length(token);
    const char* pStr = pJson + token.Start;
    if (isAlloc)
	pStr = simple_string_copy(pJson + token.Start, len, len);
    if (pLength != NULL)
	*pLength = len;
    return pStr;
}

SimpleGltfAsset
simple_gltf_asset_parse(SimpleGltfParseInput* pInput)
{
    SimpleGltfAsset gltfAsset = {};

    i32 startIndex = 0;
    SFJToken* aTokens = &pInput->aTokens[1];

    for (i32 i = 0; i < 6; i += 2)
    {
	const char* pKey = sfj_helper_get_string(pInput->pJson, aTokens, i, 0, NULL);

	if (string_compare_length(pKey, "copyright", 9))
	{
	    gltfAsset.pCopyright = sfj_helper_get_string(pInput->pJson, aTokens, i+1, 1, NULL);
	}
	else if (string_compare_length(pKey, "generator", 9))
	{
	    gltfAsset.pGenerator = sfj_helper_get_string(pInput->pJson, aTokens, i+1, 1, NULL);
	}
	else if (string_compare_length(pKey, "version", 7))
	{
	    gltfAsset.pVersion = sfj_helper_get_string(pInput->pJson, aTokens, i+1, 1, NULL);
	}
    }

    if (gltfAsset.pCopyright == NULL)
    {
	gltfAsset.pCopyright = simple_string_new("Unknown");
    }

    return gltfAsset;
}

i32
simple_gltf_scenes_parse(SimpleGltfParseInput* pInput)
{
    SFJToken arrToken = pInput->aTokens[0];
    if (arrToken.TokenType != SFJTokenType_Array)
    {
	GERROR("Not array\n");
	return -1;
    }

    SFJToken objToken = pInput->aTokens[1];
    if (objToken.TokenType != SFJTokenType_Object)
    {
	GERROR("Not object\n");
	return -1;
    }

    for (i32 i = 2; i <= 4; i+=2)
    {
	SFJToken otherToken = pInput->aTokens[i];
	if (otherToken.TokenType == SFJTokenType_Key && string_compare_length(pInput->pJson + otherToken.Start, "nodes", 5))
	{
	    SFJToken nodesArrToken = pInput->aTokens[i + 1];
	    if (nodesArrToken.TokenType == SFJTokenType_Array)
	    {
		SFJToken value = pInput->aTokens[i + 2];
		i32 rootNode = string_to_i32_length((char*)pInput->pJson + value.Start, sfj_token_length(value));
		return rootNode;
	    }
	}
    }

    return -1;
}

void
sgltf_meshes_parse(SimpleGltfParseInput* pInput)
{

}

SimpleGltfBufferView*
sgltf_buffer_views_parse(SimpleGltfParseInput* pInput)
{
    SimpleGltfBufferView* aBufferViews = NULL;

    SFJToken arrToken = pInput->aTokens[0];

    for (i64 i = 1; i < pInput->TokensCount; ++i)
    {
	SFJToken token = pInput->aTokens[i];
	if (token.End >= arrToken.End)
	{
	    break;
	}

	if (token.TokenType == SFJTokenType_Object)
	{
	    SimpleGltfBufferView bufferView = {};

	    i64 j   = i + 1;
	    i64 end = j + 8;
	    for (; j < end; j += 2)
	    {
		// DOCS: pKey = ["buffer", "byteLength", "byteOffset", "target"
		i64 keyLength, valueLength;

		const char* pKey = sfj_helper_get_string(pInput->pJson, pInput->aTokens, j, 0, &keyLength);
		const char* pValue = sfj_helper_get_string(pInput->pJson, pInput->aTokens, j + 1, 0, &valueLength);

		switch (keyLength)
		{

		case 6:
		{
		    if (string_compare_length(pKey, "buffer", 6))
		    {
			bufferView.BufferIndex =
			    string_to_i32_length((char*)pValue, valueLength);
		    }
		    else if (string_compare_length(pKey, "target", 6))
		    {
			char lastChar = pValue[valueLength - 1];
			if (lastChar == '2')
			{
			    bufferView.Target = SimpleGltfGpuBuffer_VertexBuffer;
			}
			else if (lastChar == '3')
			{
			    bufferView.Target = SimpleGltfGpuBuffer_IndexBuffer;
			}
		    }
		    break;
		}

		case 10:
		{
		    if (string_compare_length(pKey, "byteLength", 10))
		    {
			bufferView.ByteLength = string_to_i32_length((char*)pValue, valueLength);
		    }
		    else if (string_compare_length(pKey, "byteOffset", 10))
		    {
			bufferView.ByteOffset = string_to_i32_length((char*)pValue, valueLength);
		    }

		    break;
		}

		default:
		    break;
		}

	    }

	    array_push(aBufferViews, bufferView);
	}
    }

    // array_foreach(aBufferViews, GINFO("BufferView: index=%d length=%d offset=%d target=%d\n", item.BufferIndex, item.ByteLength, item.ByteOffset, item.Target));

    return aBufferViews;
}

SimpleGltfBuffer
sgltf_buffer_parse(SimpleGltfParseInput* pInput)
{
    SimpleGltfBuffer buffer = {};
    SFJToken arrToken = pInput->aTokens[0];

    for (i64 i = 1; i < pInput->TokensCount; ++i)
    {
	SFJToken token = pInput->aTokens[i];
	if (token.End >= arrToken.End)
	{
	    break;
	}

	if (token.TokenType == SFJTokenType_Object)
	{
	    i64 j   = i + 1;
	    i64 end = j + 8;
	    for (; j < end; j += 2)
	    {
		// DOCS: pKey = ["buffer", "byteLength", "byteOffset", "target"
		i64 keyLength, valueLength;

		const char* pKey = sfj_helper_get_string(pInput->pJson, pInput->aTokens, j, 0, &keyLength);

		switch (keyLength)
		{

		case 3:
		{
		    if (string_compare_length(pKey, "uri", 3))
		    {
			const char* pValue =
			    sfj_helper_get_string(
				pInput->pJson, pInput->aTokens, j + 1, 1, &valueLength);
			buffer.pUri = pValue;
		    }

		    break;
		}

		case 10:
		{
		    if (string_compare_length(pKey, "byteLength", 10))
		    {
			const char* pValue =
			    sfj_helper_get_string(
				pInput->pJson, pInput->aTokens, j + 1, 0, &valueLength);
			buffer.ByteLength =
			    string_to_i32_length((char*)pValue, valueLength);
		    }
		    break;
		}

		default:
		    break;
		}
	    }

	}
    }

    return buffer;
}

void*
sgltf_parse_array(SimpleGltfParseInput* pInput)
{
    SFJToken arr = pInput->aTokens[0];
    if (arr.TokenType != SFJTokenType_Array)
    {
	return NULL;
    }

    i32 isReal = 0;
    SFJToken first = pInput->aTokens[1];
    if (first.TokenType == SFJTokenType_Float || first.TokenType == SFJTokenType_Float_Exp)
    {
	isReal = 1;
    }

    if (isReal)
    {
	for (i32 i = 1; i < pInput->TokensCount; ++i)
	{
	    SFJToken token = pInput->aTokens[i];

	    if (token.End >= first.End)
	    {
		break;
	    }

	    i64 valueLength;
	    const char* pValue = sfj_helper_get_string(pInput->pJson, pInput->aTokens, i, 0, &valueLength);

	}
    }

    return NULL;
}

SimpleGltfAccessor*
sgltf_accessor_parse(SimpleGltfParseInput* pInput)
{
    SimpleGltfAccessor* aAccessors = NULL;

    SFJToken arrToken = pInput->aTokens[0];

    for (i64 i = 1; i < pInput->TokensCount; ++i)
    {
	SFJToken token = pInput->aTokens[i];
	if (token.End >= arrToken.End)
	{
	    break;
	}

	if (token.TokenType == SFJTokenType_Object)
	{
	    i32 iterInd = i;
	    SFJToken iterToken = token;
	    i32 iterTokenLength = sfj_token_length(iterToken);

	    while (iterToken.End < arrToken.End)
	    {
		SimpleGltfAccessor accessor = {};

		i64 keyLength, valueLength;

		const char* pKey = sfj_helper_get_string(pInput->pJson, pInput->aTokens, iterInd, 0, &keyLength);
		char* pValue = (char*) sfj_helper_get_string(pInput->pJson, pInput->aTokens, iterInd, 0, &valueLength);
		char c = *pKey;

		switch (c)
		{

		case 'b':
		{
		    if (iterTokenLength == 10 && string_compare_length(pValue, "bufferView", 10))
		    {
			accessor.BufferView = string_to_i32_length(pValue, valueLength);
		    }
		    else if (iterTokenLength == 10 && string_compare_length(pValue, "byteOffset", 10))
		    {
			accessor.ByteOffset = string_to_i32_length(pValue, valueLength);
		    }

		    break;
		}

		case 'c':
		{
		    if (iterTokenLength == 13 && string_compare_length(pValue, "componentType", 13))
		    {
			accessor.ComponentType = string_to_i32_length(pValue, valueLength);
		    }
		    else if (iterTokenLength == 5 && string_compare_length(pValue, "count", 5))
		    {
			accessor.Count = string_to_i32_length(pValue, valueLength);
		    }
		    break;
		}

		case 'n':
		{
		    if (iterTokenLength == 10 && string_compare_length(pValue, "normalized", 10))
		    {
			accessor.Normalized = 0; // string_to_i32_length(pValue, valueLength);
		    }
		    break;
		}

		case 't':
		{
		    if (iterTokenLength == 4 && string_compare_length(pValue, "type", 4))
		    {
			char fc = pValue[0];

			switch (fc)
			{

			case 'S':
			{
			    accessor.Type = SimpleGltfAccessorType_Scalar;
			    break;
			}

			case 'V':
			{
			    char ec = pValue[valueLength - 1];
			    switch (ec)
			    {
			    case '2':
				accessor.Type =  SimpleGltfAccessorType_Vec2;
				break;

			    case '3':
				accessor.Type =  SimpleGltfAccessorType_Vec3;
				break;

			    case '4':
				accessor.Type =  SimpleGltfAccessorType_Vec4;
				break;
			    }
			    break;
			}

			case 'M':
			{
			    char ec = pValue[valueLength - 1];
			    switch (ec)
			    {
			    case '2':
				accessor.Type =  SimpleGltfAccessorType_Mat2;
				break;

			    case '3':
				accessor.Type =  SimpleGltfAccessorType_Mat3;
				break;

			    case '4':
				accessor.Type =  SimpleGltfAccessorType_Mat4;
				break;
			    }
			    break;
			}

			}


		    }
		    break;
		}

		case 'm':
		{
		    if (pValue[1] == 'i') // note: min
		    {
			accessor.aMin = NULL;
		    }
		    else if (pValue[1] == 'a') // note: max
		    {
			accessor.aMax = NULL;
		    }
		    break;
		}

		}

		/* real* aMax; */
		/* real* aMin; */

		iterInd += 2;
		iterToken = pInput->aTokens[iterInd];

		array_push(aAccessors, accessor);
	    }

	}
    }

    return aAccessors;
}

//todo:
void
sgltf_parse(SimpleGltfJsonResult jsonResult)
{
    // analize tokens, parse into gltf
    SFJToken rootToken = jsonResult.aTokens[0];
    if (rootToken.TokenType != SFJTokenType_Object || jsonResult.TokensCount <= 1)
    {
	GWARNING("Invalid gltf file\n");
    }

    for (i32 i = 1; i < jsonResult.TokensCount; ++i)
    {
	SFJToken token = jsonResult.aTokens[i];
	SFJTokenType type = token.TokenType;

	if (token.Parent != 0 || type != SFJTokenType_Key)
	    continue;

	i32 tokenLength = sfj_token_length(token);
	const char* pTokenStr = token.Start + jsonResult.pText;
	//GINFO("%s:%.*s\n", sfj_token_type_to_string(type), length, pTokenStr);
	vguard(type == SFJTokenType_Key);

	char c = *pTokenStr;

	SimpleGltfParseInput parseInput = {
	    .pJson = jsonResult.pText,
	    .JsonLength = jsonResult.TextLength,
	    .aTokens = &jsonResult.aTokens[i+1],
	    .TokensCount = jsonResult.TokensCount,
	    .Index = i,
	};



	switch (tokenLength)
	{

	case 5:
	{
	    switch (c)
	    {

	    case 'a':
	    {
		if (string_compare_length(pTokenStr, "asset", 5))
		{
		    SimpleGltfAsset gltfAsset = simple_gltf_asset_parse(&parseInput);

		    GINFO("Copyright: %s\nGenerator: %s\nVersion: %s\n", gltfAsset.pCopyright, gltfAsset.pGenerator, gltfAsset.pVersion);
		}

		break;
	    }

	    case 's':
	    {
		if (c == 's' && string_compare_length(pTokenStr, "scene", 5))
		{
		    i32 currentScene = -1;

		    SFJToken nextToken = jsonResult.aTokens[i+1];
		    if (nextToken.TokenType == SFJTokenType_Int)
		    {
			const char* pValueText = nextToken.Start + jsonResult.pText;
			currentScene = string_to_i32_length((char*)pValueText, sfj_token_length(nextToken));
			GINFO("Scene: %d\n", currentScene);
		    }
		    else
		    {
			GERROR("Can't parse current scene\n");
		    }
		}

		break;
	    }

	    case 'm':
	    {
		if (string_compare_length(pTokenStr, "meshes", 5))
		{

		}

		break;
	    }

	    }

	    break;
	}

	case 6:
	{
	    if (string_compare_length(pTokenStr, "scenes", 6))
	    {
		// todo: parse scenes
		i32 rootNode =
		    simple_gltf_scenes_parse(&parseInput);
		GINFO("Root Node: %d\n", rootNode);
	    }

	    break;
	}

	case '7':
	{
	    if (c == 'b' && string_compare_length(pTokenStr, "buffers", 7))
	    {
		SimpleGltfBuffer buffer = sgltf_buffer_parse(&parseInput);
		GINFO("Buffer: byte-len:%d uri:%s\n", buffer.ByteLength, buffer.pUri);
	    }
	    break;
	}

	case 9:
	{
	    if (c == 'a' && string_compare_length(pTokenStr, "accessors", 9))
	    {

	    }

	    break;
	}

	case 10:
	{
	    if (tokenLength == 10 && string_compare_length(pTokenStr, "animations", 10))
	    {

	    }

	    break;
	}

	case 11:
	{
	    if (c == 'b' && string_compare_length(pTokenStr, "bufferViews", 11))
	    {
		SimpleGltfBufferView* aBufferViews =
		    sgltf_buffer_views_parse(&parseInput);
	    }
	    break;
	}

	}

	switch (c)
	{

	case 's':
	{
	    break;
	}

	case 'n':
	{
	    // todo: parse nodes
	    GINFO("%.*s\n", sfj_token_length(token), pTokenStr);
	    break;
	}

	case 'm':
	{
	    // todo: parse materials, meshes
	    GINFO("%.*s\n", sfj_token_length(token), pTokenStr);



	    break;
	}

	case 'i':
	{
	    // todo: parse images
	    GINFO("%.*s\n", sfj_token_length(token), pTokenStr);
	    break;
	}

	case 'b':
	{
	    // note DONE:
	    GINFO("%.*s\n", sfj_token_length(token), pTokenStr);
	    break;
	}

	default:
	{
	    GINFO("%.*s\n", sfj_token_length(token), pTokenStr);
	    break;
	}

	}

    }

}


void
simple_gltf_new(const char* pGltfPath)
{
    SimpleGltfFile gltfFile = {};

    SimpleGltfJsonResult jsonResult = sgltf_json_result_parse(pGltfPath);

    sgltf_parse(jsonResult);

    // NOTE: Clean up
    sgltf_json_result_destroy(jsonResult);

    //return gltfFile;
}
