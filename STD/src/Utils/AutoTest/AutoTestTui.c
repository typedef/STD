#include "AutoTestTui.h"

#include <Core/Types.h>
#include <Core/SimpleStandardLibrary.h>
#include <Core/SimpleMath.h>
#include <Utils/Terminal/Tui.h>
#include <Utils/AutoTest/AutoTest.h>


typedef enum TuiPanelState
{
    TuiPanelState_Files = 0,
    TuiPanelState_Functions,
    TuiPanelState_TestRead,
} TuiPanelState;

typedef struct TuiScrollBox
{
    v2i Position;
    v2i Size;
    i32 LinesCount;
    i32 CurrentLine;
    i32 OffsetLine;
} TuiScrollBox;

static v2i gStartFunctionsPanel = {1,6};
static i32 gInputUpdate = 1;
const i32 MAX_RECORDS = 15;

static SimpleMutex gMutex = {};
static TuiPanelState gState = TuiPanelState_Files;
static TuiScrollBox gFileBox = {};
static TuiScrollBox gFunctionBox = {};
static TuiScrollBox gContentBox = {};

void auto_test_on_input_base(i32 key);

static i32 gAppRun = 1;


static void
_tui_box(v2i pos, v2i size)
{
    i32 ex = pos.X + size.X;
    i32 ey = pos.Y + size.Y;

    for (i32 x = pos.X; x < ex; ++x)
    {
	tui_printxy(pos.Y, x, "-");
	tui_printxy(ey, x, "-");
    }

    for (i32 y = (pos.Y+1); y < ey; ++y)
    {
	tui_printxy(y, pos.X, "|");
	tui_printxy(y, ex, "|");
    }
}

static void
_tui_scroll_box_render(TuiScrollBox* pBox)
{
    _tui_box(pBox->Position, pBox->Size);
}

typedef enum TuiShowResultMode
{
    TuiShowResultMode_All = 0,
    TuiShowResultMode_Error = 1,
    TuiShowResultMode_Success = 2,
    TuiShowResultMode_Count
} TuiShowResultMode;

static i32 gShowSuccess = 0;

static TuiShowResultMode gTuiCurrentMode = TuiShowResultMode_All;
static TuiShowResultMode gTuiShowResultModes[] = {
    [TuiShowResultMode_All] = TuiShowResultMode_Error,
    [TuiShowResultMode_Error] = TuiShowResultMode_Success,
    [TuiShowResultMode_Success] = TuiShowResultMode_All,
};

static void
_tui_scroll_box_input(TuiScrollBox* pBox, i32 key)
{
    i32 wind = pBox->CurrentLine - 1;
    i32 sind = pBox->CurrentLine + 1;
    i32 max  = pBox->LinesCount - 1;

    i32 isHandled = 1;

    switch (key)
    {

    case 'w':
    {
	pBox->CurrentLine = Max(wind, 0);

	if (pBox->OffsetLine > 0 && pBox->CurrentLine == 0)
	{
	    pBox->OffsetLine = Max(pBox->OffsetLine - 1, 0);
	}

	break;
    }

    case 's':
    {
	i32 maxLine = Min(pBox->Size.Height - 2, max);
	pBox->CurrentLine = Min(sind, maxLine);
	if (pBox->OffsetLine < pBox->LinesCount && pBox->CurrentLine == maxLine)
	{
	    pBox->OffsetLine = Min(pBox->OffsetLine + 1, pBox->LinesCount-1);
	}
	break;
    }

    case 'e':
    {
	gTuiCurrentMode = gTuiShowResultModes[gTuiCurrentMode];
	break;
    }

    case 'p':
    {
	gAppRun = 0;
	break;
    }

    case Key_Return:
    case Key_Backspace:
	break;

    default:
    {
	isHandled = 0;
	break;
    }

    }

    if (isHandled)
	auto_test_on_input_base(key);
}

static i32
_tui_record(TuiScrollBox* pScrollBox, AutoTestRecord* aRecords)
{
    pScrollBox->LinesCount = 0;
    array_foreach(aRecords,
		  pScrollBox->LinesCount += (1 + string_count_of(item.Message, '\n'))
	);

    v2i size = pScrollBox->Size;
    v2i pos = pScrollBox->Position;
    i32 currentLine = pScrollBox->CurrentLine;
    i32 recordsCount = size.Height - 1;
    i32 height = size.Height - 1;

    i32 ly = pos.Y + 1;
    i32 lx = pos.X + 1;
    i32 offset = pScrollBox->OffsetLine;

    i32 so = offset;
    i32 recsCount = array_count(aRecords);
    i32 eo = offset + recsCount;

    _tui_box(pos, size);

    for (i32 i = 0; i < height; ++i)
    {
	tui_printxy(ly, lx, "%0.2d|", i + offset);
	++ly;
    }

    ly = pos.Y + 1;

    for (i32 i = so; i < eo; ++i)
    {
	if (i >= recsCount || (i - so) >= height)
	    break;
	AutoTestRecord autoTestRec = aRecords[i];

	i32 recordsMargin = 3 + 2;

	char* msg = autoTestRec.Message;
	i32 code = autoTestRec.Code;
	i32 linesCnt = 1;
	i32 newLinesCnt = string_count_of(msg, '\n');

	linesCnt += newLinesCnt;
	gContentBox.LinesCount += newLinesCnt;

	if (linesCnt == 1)
	{
	    if (code)
	    {
		if (gTuiCurrentMode == TuiShowResultMode_All || gTuiCurrentMode == TuiShowResultMode_Success)
		    tui_printxy(ly, lx + recordsMargin, "%s ["
				GREEN("%d") "]",
				msg, code);
	    }
	    else
	    {
		if (gTuiCurrentMode == TuiShowResultMode_All || gTuiCurrentMode == TuiShowResultMode_Error)
		    tui_printxy(ly, lx + recordsMargin, "%s ["
				RED("%d") "]",
				msg, code);
	    }
	}
	else
	{
	    //todo: rework
	    char** splited = string_split(msg, '\n');

	    i32 scnt = array_count(splited);
	    for (i32 s = 0; s < scnt; ++s)
	    {
		char* split = splited[s];
		tui_printxy(ly, lx + recordsMargin, "%s", split);

		++ly;
	    }

	    array_free(splited);
	}

	++ly;
    }

    return 0;
}

void
auto_test_tui_init(AutoTestFile* aTestFile)
{
    gMutex = simple_thread_mutex_create();

    gFileBox = (TuiScrollBox) {
	.Position = v2i_new(1, 5),
	.Size = v2i_new(95, 40),
	.LinesCount = array_count(aTestFile),
    };

    gFunctionBox = (TuiScrollBox) {
	.Position = v2i_new(1, 5),
	.Size = v2i_new(95, 10),
    };

    gContentBox = (TuiScrollBox) {
	.Position = v2i_new(1, 16),
	.Size = v2i_new(95, 20),
    };
}

void
auto_test_on_input_base(i32 key)
{
    gInputUpdate = 1;
}

void*
auto_test_input(void* data)
{
    while (1)
    {
	if (!gAppRun)
	    break;

	i32 key = tui_getkey();
	if (key == -1)
	    continue;

	simple_thread_mutex_lock(&gMutex);

	if (gState == TuiPanelState_Files)
	{
	    _tui_scroll_box_input(&gFileBox, key);
	}
	else if (gState == TuiPanelState_Functions)
	{
	    _tui_scroll_box_input(&gFunctionBox, key);
	}
	else if (gState == TuiPanelState_TestRead)
	{
	    _tui_scroll_box_input(&gContentBox, key);
	}

	switch (key)
	{
	case Key_Return:
	{
	    TuiPanelState gMoveForwardSwitch[] = {
		[TuiPanelState_Files] = TuiPanelState_Functions,
		[TuiPanelState_Functions] = TuiPanelState_TestRead,
		[TuiPanelState_TestRead] = TuiPanelState_TestRead,
	    };

	    gState = gMoveForwardSwitch[gState];
	    gInputUpdate = 1;

	    break;
	}

	case Key_Backspace:
	{
	    TuiPanelState gMoveBackwardSwitch[] = {
		[TuiPanelState_Files] = TuiPanelState_Files,
		[TuiPanelState_Functions] = TuiPanelState_Files,
		[TuiPanelState_TestRead] = TuiPanelState_Functions,
	    };

	    if (gState == TuiPanelState_Functions || gState == TuiPanelState_TestRead)
	    {
		gContentBox.CurrentLine = 0;
		gContentBox.OffsetLine = 0;
		gFunctionBox.CurrentLine = 0;
		gFunctionBox.OffsetLine = 0;
	    }

	    gState = gMoveBackwardSwitch[gState];
	    gInputUpdate = 1;

	    break;
	}

	}

	simple_thread_mutex_unlock(&gMutex);

    }

    return NULL;
}

void*
auto_test_draw(void* data)
{
    AutoTestFile* aFile = (AutoTestFile*) data;

    while (1)
    {
	if (!gAppRun)
	    break;

	simple_timer_mssleep(32);
	simple_thread_mutex_lock(&gMutex);
	auto_test_tui_draw_files_list(aFile);
	simple_thread_mutex_unlock(&gMutex);
    }

    return NULL;
}

void
auto_test_get_success_errors(AutoTestFile autoFile, i32* pS, i32* pE)
{
    i32 successed = 0;
    i32 error = 0;

    array_foreach(autoFile.aFunctions,
		  if (item.Success)
		      ++successed;
		  else
		      ++error;
	);

    *pS = successed;
    *pE = error;
}

void
_auto_test_navigation_panel(v2i curPos, i32 isBackspaceVisible)
{
    const i32 gEnterOffset = 72;
    i32 ypos = curPos.Y-1;
    i32 xpos = curPos.X;

    if (isBackspaceVisible)
    {
	tui_printxy(ypos, xpos,
		    "<-- '" YELLOW("Backspace")"' - return");
    }

    xpos += (5 + 9 + 10);
    xpos += 12;
    tui_printxy(ypos, xpos, "'" YELLOW("w") "' - next '" YELLOW("s") "' - prev");

    tui_printxy(ypos, curPos.X + gEnterOffset, "'" YELLOW("Enter")"' - dive into -->");
}

// BUG: It broken by using this
// gFunctionBox.LinesCount = array_count(autoFile.aFunctions);
// i32 fcnt = array_count(autoFile.aFunctions);
// if (f >= gFunctionBox.LinesCount-1) break;
v2i
_auto_test_draw_functions(AutoTestFile* aTestFiles)
{
    v2i posToSet = {};

    v2i curPos = gFunctionBox.Position;
    _tui_scroll_box_render(&gFunctionBox);

    AutoTestFile autoFile = aTestFiles[gFileBox.CurrentLine];

    gFunctionBox.LinesCount = array_count(autoFile.aFunctions);

    i32 successed, error;
    auto_test_get_success_errors(autoFile, &successed, &error);

    _auto_test_navigation_panel(curPos, 1);

    tui_printxy(curPos.Y, curPos.X + 30, GREEN("#")" %s [" GREEN("s") ":%d|"RED("e")":%d| t:%d]", autoFile.Name, successed, error, array_count(autoFile.aFunctions));
    ++curPos.Y;

    i32 offset = gFunctionBox.OffsetLine;
    i32 cnt = gFunctionBox.Size.Height - 1;
    i32 so = gFunctionBox.OffsetLine;
    i32 eo = gFunctionBox.OffsetLine + gFunctionBox.LinesCount;

    for (i32 f = so; f < eo; ++f)
    {
	if ((f - offset) >= cnt)
	    break;

	AutoTestFunction autoTestFunction = autoFile.aFunctions[f];
	i32 xpos = curPos.X + 1;

	if (gFunctionBox.CurrentLine == f)
	{
	    tui_printxy(curPos.Y, xpos, ">>>");
	    xpos += 3;

	    posToSet = v2i_new(1, curPos.Y);
	}

	if (autoTestFunction.Success)
	{
	    tui_printxy(curPos.Y, xpos, GREEN("%s"), autoTestFunction.Name);
	}
	else
	{
	    tui_printxy(curPos.Y, xpos, RED("%s"), autoTestFunction.Name);
	}

	if (gFunctionBox.CurrentLine == f)
	{
	    xpos += string_length(autoTestFunction.Name);
	    tui_printxy(curPos.Y, xpos, "<<<");
	}

	++curPos.Y;
    }

    AutoTestFunction autoFunc = autoFile.aFunctions[gFunctionBox.CurrentLine];
    _tui_record(&gContentBox, autoFunc.aTestRecords);

    ++curPos.Y;

    return posToSet;
}

void
auto_test_tui_draw_files_list(AutoTestFile* aTestFiles)
{
    if (!gInputUpdate)
	return;

    gInputUpdate = 0;

    tui_clear();

    tui_printxy(1, 30, "#####################################");
    tui_printxy(2, 30, "##########   AUTO TEST's   ##########");
    tui_printxy(3, 30, "#####################################");

    v2i posToSet = {};

    if (gState == TuiPanelState_Files)
    {
	v2i curPos = gFileBox.Position;

	_auto_test_navigation_panel(curPos, 0);

	_tui_scroll_box_render(&gFileBox);

	i32 posy = curPos.Y + 1;
	for (i32 f = 0; f < gFileBox.LinesCount; ++f)
	{
	    AutoTestFile autoFile = aTestFiles[f];

	    i32 successed, error;
	    auto_test_get_success_errors(autoFile, &successed, &error);

	    i32 posx = curPos.X + 1;
	    if (gFileBox.CurrentLine == f)
	    {
		tui_printxy(posy, posx, GREEN(" #")" %s [" GREEN("s") ":%d|"RED("e")":%d| t:%d]", autoFile.Name, successed, error, array_count(autoFile.aFunctions));

		posToSet.X = 1;
		posToSet.Y = posy;
	    }
	    else
	    {
		tui_printxy(posy, posx, WHITE("#")" %s [" GREEN("s") ":%d|"RED("e")":%d| t:%d]", autoFile.Name, successed, error, array_count(autoFile.aFunctions));
	    }

	    ++posy;
	}

    }
    else if (gState == TuiPanelState_Functions)
    {
	posToSet = _auto_test_draw_functions(aTestFiles);
    }
    else if (gState == TuiPanelState_TestRead)
    {
	_auto_test_draw_functions(aTestFiles);
	posToSet = v2i_new(1, gContentBox.Position.Y + gContentBox.CurrentLine + 1);
    }

    tui_printxy(posToSet.Y, posToSet.X, "");
    tui_flush();

}
