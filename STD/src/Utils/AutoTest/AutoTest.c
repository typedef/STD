#include "AutoTest.h"
#include "Core/Types.h"

#include <stdlib.h>
#include <Core/SimpleStandardLibrary.h>
#include <Core/SimpleMath.h>


static AutoTestGlobal gAutoTestGlobal = {};

force_inline i32
f32_equal(f32 v0, f32 v1)
{
    real r = real_equal(v0, v1);
    return r;
}

force_inline i32
f32_equal_epsilon(f32 v0, f32 v1, f32 epsilon)
{
    real r = real_equal_epsilon(v0, v1, epsilon);
    return r;
}

void
auto_test_init(u64 size)
{
    SimpleArena* pNewArena = simple_arena_create(size);
    gAutoTestGlobal.pArena = pNewArena;
}

AutoTestGlobal*
auto_test_get()
{
    return &gAutoTestGlobal;
}

#define auto_test_array_push(a, item)                   \
    ({                                                  \
	memory_set_arena(gAutoTestGlobal.pArena);       \
	array_push(a, item);                            \
	memory_set_arena(NULL);                         \
    })

void
_auto_test_set_file(const char* file)
{
    memory_set_arena(gAutoTestGlobal.pArena);

    char* fileName = path_get_filename(file);

    i32 ind = array_index_of(gAutoTestGlobal.aFiles, string_compare(item.Name, fileName));
    if (ind != -1)
    {
	memory_free(fileName);
	return;
    }

    AutoTestFile autoTestFile = {
	.Name = fileName,
    };

    i64 cnt = array_count(gAutoTestGlobal.aFiles);
    auto_test_array_push(gAutoTestGlobal.aFiles, autoTestFile);

    AutoTestFile* pAutoTestFile = &gAutoTestGlobal.aFiles[cnt];
    gAutoTestGlobal.pAutoTestFile = pAutoTestFile;

    memory_set_arena(NULL);
}

AutoTestFunction*
_auto_test_get_function(const char* functionName)
{
    vguard_not_null(gAutoTestGlobal.pAutoTestFile);

    i64 ind = array_index_of(gAutoTestGlobal.pAutoTestFile->aFunctions, string_compare(item.Name, functionName));
    if (ind == -1)
	return NULL;

    return &gAutoTestGlobal.pAutoTestFile->aFunctions[ind];
}

void
_auto_test_set_function(const char* functionName)
{
    memory_set_arena(gAutoTestGlobal.pArena);

    gAutoTestGlobal.FunctionName = functionName;

    AutoTestFunction* pAutoTestFunc = _auto_test_get_function(functionName);
    if (pAutoTestFunc != NULL)
	return;

    AutoTestFunction autoTestFunction = {
	.Name = string(functionName),
	.Success = 1,
    };

    auto_test_array_push(gAutoTestGlobal.pAutoTestFile->aFunctions, autoTestFunction);

    memory_set_arena(NULL);
}

void
_auto_test_add_record(i32 conditionResult, const char* file, const char* condition)
{
    memory_set_arena(gAutoTestGlobal.pArena);

    const char* functionName = gAutoTestGlobal.FunctionName;
    vguard_not_null(functionName);
    AutoTestFunction* pAutoTestFunc = _auto_test_get_function(functionName);
    vguard_not_null(pAutoTestFunc);

    AutoTestRecord autoTestRecord = {
	.Code = conditionResult,
	.Message = string(condition)
    };
    auto_test_array_push(pAutoTestFunc->aTestRecords, autoTestRecord);

    if (conditionResult)
    {
	++gAutoTestGlobal.Info.SuccessCount;
    }
    else
    {
	++gAutoTestGlobal.Info.ErrorsCount;
	pAutoTestFunc->Success = 0;
    }

    ++gAutoTestGlobal.Info.Count;

    memory_set_arena(NULL);
}


void
auto_test_condition(i32 condition, const char* cstr, const char* file)
{
    _auto_test_add_record(condition, file, cstr);
}

void
auto_test_i32_value(i32 v, const char* cstr, const char* file)
{
    char i32s[512] = {};
    snprintf(i32s, 512, "%s%d", cstr, v);
    _auto_test_add_record(1, file, i32s);
}

void
auto_test_i32_equal(i32 v0, i32 v1, const char* cstr0, const char* cstr1, const char* file)
{
    char condition[512] = {};
    snprintf(condition, 512, "%s(%d) == %s(%d)", cstr0, v0, cstr1, v1);
    _auto_test_add_record(v0 == v1, file, condition);
}

void
auto_test_f32_value(f32 v, const char* cstr, const char* file)
{
    char condition[512] = {};
    snprintf(condition, 512, "%s%f", cstr, v);
    _auto_test_add_record(1, file, condition);
}

void
auto_test_f32_equal(f32 v0, f32 v1, const char* cstr, const char* file)
{
    f32 temp = f32_equal(v0, v1);
    _auto_test_add_record(temp, file, cstr);
}

void
auto_test_string_equal(char* s0, char* s1, const char* cstr, const char* file)
{
    i32 temp = 0;
    if (s0 != NULL && s1 != NULL)
    {
	temp = string_compare(s0, s1);
    }

    _auto_test_add_record(temp, file, cstr);
}

i32
auto_test_string_equal_length(char* a, char* b, i32 len, const char* file, const char* condition)
{
    i32 temp = 0;
    if (a != NULL && b != NULL)
    {
	temp = string_compare_w_length(a, b, len, len);
    }

    _auto_test_add_record(temp, file, condition);

    return temp;
}

void
auto_test_string_value(const char* s, const char* cstr, const char* file)
{
    i32 isSuc = 0;
    char* scon = (char*) "NULL";
    if (s)
    {
	scon = string_concat(cstr, s);
	isSuc = 1;
    }

    _auto_test_add_record(isSuc, file, scon);
}

void
auto_test_string_value_length(const char* s, i32 len, const char* cstr, const char* file)
{
#define SIZE KB(512)
    char buf[SIZE] = {};
    snprintf(buf, SIZE, "%s%.*s", cstr, len, s);

    _auto_test_add_record(s != NULL, file, buf);
}

void
auto_test_v2_equal(v2 v0, v2 v1, const char*v0s, const char*v1s, const char* pFile)
{
    i32 isEqual = f32_equal(v0.X, v1.X) && f32_equal(v0.Y, v1.Y);

    char buf[512] = {};
    snprintf(buf, 512, "%s(%f, %f) == %s(%f, %f)", v0s, v0.X, v0.Y, v1s, v1.X, v1.Y);
    _auto_test_add_record(isEqual, pFile, buf);
}

void
auto_test_v2_value(v2 v, const char*vs, const char* pFile)
{
    char buf[128] = {};
    snprintf(buf, 128, "%s(%f, %f)", vs, v.X, v.Y);
    _auto_test_add_record(1, pFile, buf);
}


void
auto_test_v3_equal(v3 v0, v3 v1, const char*v0s, const char*v1s, const char* pFile)
{
    i32 isEqual = f32_equal(v0.X, v1.X) && f32_equal(v0.Y, v1.Y) && f32_equal(v0.Z, v1.Z);

    char buf[512] = {};
    snprintf(buf, 512, "%s(%f, %f, %f) == %s(%f, %f, %f)", v0s, v0.X, v0.Y, v0.Z, v1s, v1.X, v1.Y, v1.Z);
    _auto_test_add_record(isEqual, pFile, buf);
}

void
auto_test_v3_value(v3 v, const char*vs, const char* pFile)
{
    char buf[128] = {};
    snprintf(buf, 128, "%s(%f, %f, %f)", vs, v.X, v.Y, v.Z);
    _auto_test_add_record(1, pFile, buf);
}

void
auto_test_v4_equal(v4 v0, v4 v1, const char*v0s, const char*v1s, const char* pFile)
{
    i32 isEqual = f32_equal(v0.X, v1.X) && f32_equal(v0.Y, v1.Y) && f32_equal(v0.Z, v1.Z) && f32_equal(v0.W, v1.W);

    char buf[512] = {};
    snprintf(buf, 512, "%s(%f, %f, %f, %f) == %s(%f, %f, %f, %f)", v0s, v0.X, v0.Y, v0.Z, v0.W, v1s, v1.X, v1.Y, v1.Z, v1.W);
    _auto_test_add_record(isEqual, pFile, buf);
}

void
auto_test_v4_value(v4 v, const char*vs, const char* pFile)
{
    char buf[128] = {};
    snprintf(buf, 128, "%s(%f, %f, %f, %f)", vs, v.X, v.Y, v.Z, v.W);
    _auto_test_add_record(1, pFile, buf);
}

void
auto_test_m4_value(m4 m, const char* cstr0, const char* cstr1, const char* file)
{
    char* ind = "    ";
    char sb[511];
    sprintf(sb, "M4 ( %s ):\n%s%.2f %.2f %.2f %.2f\n%s%.2f %.2f %.2f %.2f\n%s%.2f %.2f %.2f %.2f\n%s%.2f %.2f %.2f %.2f", cstr0,
	    ind, m.M[0][0], m.M[0][1], m.M[0][2], m.M[0][3],
	    ind, m.M[1][0], m.M[1][1], m.M[1][2], m.M[1][3],
	    ind, m.M[2][0], m.M[2][1], m.M[2][2], m.M[2][3],
	    ind, m.M[3][0], m.M[3][1], m.M[3][2], m.M[3][3]);

    auto_test_string_value(sb, cstr1, __FILE__);
}

void
auto_test_m4_equal(m4 m0, m4 m1, const char* cstr, const char* file)
{
    i32 isEqual = 1;

    for (i32 r = 0; r < 4; r++)
    {
	for (i32 c = 0; c < 4; c++)
	{
	    f32 v0 = m0.M[r][c];
	    f32 v1 = m1.M[r][c];
	    i32 localIsEqual = f32_equal_epsilon(v0, v1, 1E-2f);
	    if (!localIsEqual)
	    {
		GERROR("%d\n", localIsEqual);
	    }
	    else
	    {
		GSUCCESS("%d\n", localIsEqual);
	    }

	    isEqual *= localIsEqual;
	}
    }

    _auto_test_add_record(isEqual, file, cstr);
}

i32
_auto_test_register_dependency(AutoTestDependency dependency)
{
    vguard_not_null(dependency.Name);
    vguard_not_null(dependency.OnInitialize);

    i64 ind = shash_geti(gAutoTestGlobal.pDependencies, dependency.Name);
    if (ind != -1)
	return 0;

    memory_set_arena(gAutoTestGlobal.pArena);

    dependency.IsInitialize = 0;
    shash_put(gAutoTestGlobal.pDependencies, dependency.Name, dependency);

    memory_set_arena(NULL);

    return 1;
}

AutoTestDependStatusType
_auto_test_depend(const char* pName)
{
    vguard_not_null(pName);

    i64 ind = shash_geti(gAutoTestGlobal.pDependencies, pName);
    if (ind == -1)
	return AutoTestDependStatusType_Error_CauseNotRegistered;

    AutoTestDependency dep = shash_get(gAutoTestGlobal.pDependencies, pName);
    if (dep.IsInitialize)
	return AutoTestDependStatusType_Warn_AlreadyInit;

    memory_set_arena(gAutoTestGlobal.pArena);

    dep.OnInitialize();
    dep.IsInitialize = 1;

    // DOCS: Update flag on init
    shash_put(gAutoTestGlobal.pDependencies, dep.Name, dep);

    memory_set_arena(NULL);

    return AutoTestDependStatusType_Init;
}

void
_auto_test_depend_valid(AutoTestDependStatusType type, const char* pFile, i32 line)
{
    const char* typeStr = NULL;
    switch (type)
    {
    case AutoTestDependStatusType_Error_CauseNotRegistered:
	typeStr = "Not registered";
	break;
    case AutoTestDependStatusType_Warn_AlreadyInit:
	typeStr = "Already initialized";
	break;
    case AutoTestDependStatusType_Init:
	typeStr = "Init";
	break;

    }

    vguard_not_null(typeStr);

    if (type != AutoTestDependStatusType_Init)
    {
	GINFO("error:%s f:%s l:%d\n", typeStr, pFile, line);
	vguard(0 && "Not initialized");
    }
}
