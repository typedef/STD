#include "AutoTestUi.h"

#include <Utils/AutoTest/AutoTest.h>
#include <Core/SimpleStandardLibrary.h>

#include "AutoTestTui.h"

i32
auto_test_ui_draw_file_list(AutoTestFile* aFiles)
{
    auto_test_tui_init(aFiles);
    simple_thread_create(auto_test_input, KB(5), NULL);
    simple_thread_create(auto_test_draw, KB(5), aFiles);

    return 0;
}
