#ifndef TUI_H
#define TUI_H

int tui_getkey();
void tui_printxy(int y, int x, const char *format, ...);
void tui_flush();
void tui_clear();

#define Key_LButton 0x01  /* Левая кнопка мыши */
#define Key_RButton 0x02  /* Правая кнопка мыши */
#define Key_Cancel 0x03   /* Обработка контроля и прерывания */
#define Key_MButton 0x04  /* Средняя кнопка мыши */
#define Key_Backspace 127     /* Backspace */
#define Key_Tab 0x09      /* TAB - клавиша */
#define Key_Clear 0x0C    /* Клавиша CLEAR */
#define Key_Return 0x0D   /* Клавиша ВВОД */
#define Key_Shift 0x10    /* Клавиша SHIFT */
#define Key_Ctrl 0x11     /* Клавиша CTRL */
#define Key_Menu 0x12     /* ALT - клавиша */
#define Key_Capital 0x14  /* Клавиша CAPS LOCK */
#define Key_Escape 0x1B   /* Клавиша ESC */
#define Key_Space 0x20    /* ПРОБЕЛ */
#define Key_Left 0x25     /* КЛАВИША СТРЕЛКА ВЛЕВО */
#define Key_Up 0x26       /* КЛАВИША СТРЕЛКА ВВЕРХ */
#define Key_Right 0x27    /* КЛАВИША СТРЕЛКА ВПРАВО */
#define Key_Down 0x28     /* КЛАВИША СТРЕЛКА ВНИЗ */
#define Key_Delete 0x2E   /* Клавиша DEL */

#endif // TUI_H
