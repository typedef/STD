#include "StdScene.h"
#include <Core/SimpleStandardLibrary.h>


static StdScene gScene = {};

void
std_scene_create(StdSceneSettings set)
{
    sva_add(gScene.aCameraEntities, set.MainCamera);

    gScene.aVisibleEntities = set.aVisibleEntities;
}

void
std_scene_destroy()
{
    sva_free(gScene.aVisibleEntities);
}

void
std_scene_add_camera(StdCameraEntity camera)
{
    sva_add(gScene.aCameraEntities, camera);
}

void
std_scene_add_visible_entity(StdVisibleEntity visibleEntity)
{
    i64 ind = sva_index_of(gScene.aVisibleEntities, item.Base.Id == visibleEntity.Base.Id);
    if (ind != -1)
    {
	return;
    }

    sva_add(gScene.aVisibleEntities, visibleEntity);
}

StdCameraEntity*
std_scene_get_main_camera()
{
    i32 i, count = sva_count(gScene.aCameraEntities);
    for (i = 0; i < count; ++i)
    {
	StdCameraEntity* pCamera = &gScene.aCameraEntities[i];
	if (pCamera->IsMain)
	    return pCamera;
    }

    // note: this should *never* happen (again)
    vguard(0);
    return NULL;
}

v3
std_scene_get_main_position()
{
    return std_scene_get_main_camera()->Base.Position;
}

StdVisibleEntity*
std_scene_get_visible_entities()
{
    return gScene.aVisibleEntities;
}
