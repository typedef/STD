#ifndef STD_CAMERA_ENTITY_H
#define STD_CAMERA_ENTITY_H

#include <Core/Types.h>
#include <EntitySystem/StdEntity.h>


typedef struct StdCameraEntitySettings
{
    i8 IsMain;
    v2 WindowSize;
    v3 Position;
} StdCameraEntitySettings;

typedef struct StdCameraEntity
{
    StdEntity Base;
    m4 View;
    m4 Projection;
    m4 ViewProjection;
    v2 WindowSize;

    i32 Zoom;

    // DOCS: Flags
    i8 IsCameraMoved;
    i8 IsZMoved;
    i8 IsMain;
} StdCameraEntity;

StdCameraEntity std_camera_entity_new(StdCameraEntitySettings set);
void std_camera_entity_move(StdCameraEntity* pEntity, v2 newPos);
void std_camera_entity_resize(StdCameraEntity* pEntity, v2 newWindowSize);
void std_camera_entity_update(StdCameraEntity* pEntity);
void std_camera_entity_move_update(StdCameraEntity* pEntity, v2 newPos);

#endif // STD_CAMERA_ENTITY_H
