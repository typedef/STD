#ifndef STD_VISIBLE_ENTITY_H
#define STD_VISIBLE_ENTITY_H

#include <EntitySystem/StdEntity.h>

typedef struct StdVisibleEntitySettings
{
    v3 Position;
    v2 Size;
    v4 Color;
    i64 AssetId; // is it index to an array?
} StdVisibleEntitySettings;

typedef struct StdVisibleEntity
{
    StdEntity Base;
    v3 Position;
    v2 Size;
    v4 Color;
    i64 AssetId; // is it index to an array?
} StdVisibleEntity;

StdVisibleEntity std_visible_entity_new(StdVisibleEntitySettings set);
void std_visible_entity_destroy(StdVisibleEntity entity);

#endif // STD_VISIBLE_ENTITY_H
