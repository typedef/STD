#include "StdEntity.h"
#include <Core/SimpleStandardLibrary.h>


typedef struct StdDeletedStorage
{
    i64* aIds;
} StdDeletedStorage;

static StdDeletedStorage gDeletedStorage = {};
static i64 gLastId = 0;


StdEntity
std_entity_new()
{
    StdEntity stdEntity = {};

    i64 id;
    if (array_count(gDeletedStorage.aIds) > 0)
    {
        id = array_pop(gDeletedStorage.aIds);
    }
    else
    {
        id = gLastId;
        ++gLastId;
    }

    stdEntity.Id = id;

    return stdEntity;
}

void
std_entity_destroy(StdEntity entity)
{
    array_push(gDeletedStorage.aIds, entity.Id);
}
