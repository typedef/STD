#ifndef STD_ENTITY_H
#define STD_ENTITY_H

#include <Core/Types.h>


typedef struct StdEntity
{
    i64 Id;
    v3 Position;
} StdEntity;

StdEntity std_entity_new();
void std_entity_destroy(StdEntity entity);

#endif // STD_ENTITY_H
