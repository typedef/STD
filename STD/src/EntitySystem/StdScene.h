#ifndef STD_SCENE_H
#define STD_SCENE_H

#include <EntitySystem/StdCameraEntity.h>
#include <EntitySystem/StdVisibleEntity.h>


typedef struct StdSceneSettings
{
    StdCameraEntity MainCamera;
    StdVisibleEntity* aVisibleEntities;
} StdSceneSettings;

typedef struct StdScene
{
    StdCameraEntity* aCameraEntities;
    StdVisibleEntity* aVisibleEntities;
} StdScene;


void std_scene_create(StdSceneSettings set);
void std_scene_destroy();
void std_scene_add_camera(StdCameraEntity camera);

void std_scene_add_visible_entity(StdVisibleEntity visibleEntity);

StdCameraEntity* std_scene_get_main_camera();
v3 std_scene_get_main_position();
StdVisibleEntity* std_scene_get_visible_entities();

#endif // STD_SCENE_H
