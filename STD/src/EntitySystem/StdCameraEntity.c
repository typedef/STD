#include "StdCameraEntity.h"
#include <Core/SimpleStandardLibrary.h>
#include <Core/SimpleMath.h>
#include <Core/SimpleMathIO.h>


m4
_calc_projection(v2 windowSize)
{
#if 1
    m4 orthoProjection = orthographic(0, windowSize.X, 0, windowSize.Y, 0.1f, 200.0f);
#else
    f32 aspectRatio = windowSize.X/windowSize.Y;
    m4 orthoProjection = orthographic(
	0, aspectRatio ,
	0, aspectRatio,
	0.1, 200.0f);
#endif

    return orthoProjection;
}

StdCameraEntity
std_camera_entity_new(StdCameraEntitySettings set)
{
    StdCameraEntity stdCameraEntity = {
	.Base = std_entity_new(),
	.Projection = _calc_projection(set.WindowSize),
	.WindowSize = set.WindowSize,
	.IsMain = set.IsMain,
    };

    // docs: calc matrices
    stdCameraEntity.IsCameraMoved = 1;
    stdCameraEntity.Zoom = 1;
    std_camera_entity_update(&stdCameraEntity);

    return stdCameraEntity;
}


void
std_camera_entity_move(StdCameraEntity* pEntity, v2 newPos)
{
    v3 oldPos = pEntity->Base.Position;
    pEntity->Base.Position = v3_v2(v2_add(oldPos.XY, newPos));
    pEntity->IsCameraMoved = 1;
}

void
std_camera_entity_resize(StdCameraEntity* pEntity, v2 newWindowSize)
{
    pEntity->Projection = _calc_projection(newWindowSize);
}

void
std_camera_entity_update(StdCameraEntity* pEntity)
{
    if (!pEntity->IsCameraMoved)
	return;
    pEntity->IsCameraMoved = 0;

    pEntity->View = m4_translate_identity(pEntity->Base.Position);
    pEntity->ViewProjection = m4_mul(pEntity->Projection, pEntity->View);
}

void
std_camera_entity_move_update(StdCameraEntity* pEntity, v2 newPos)
{
    std_camera_entity_move(pEntity, newPos);
    std_camera_entity_update(pEntity);
}
