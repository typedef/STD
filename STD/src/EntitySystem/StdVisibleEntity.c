#include "StdVisibleEntity.h"


StdVisibleEntity
std_visible_entity_new(StdVisibleEntitySettings set)
{
    StdVisibleEntity entity = {
	.Base = std_entity_new(),
	.Position = set.Position,
	.Size = set.Size,
	.Color = set.Color,
	.AssetId = set.AssetId,
    };

    return entity;
}

void
std_visible_entity_destroy(StdVisibleEntity entity)
{
    std_entity_destroy(entity.Base);
}
