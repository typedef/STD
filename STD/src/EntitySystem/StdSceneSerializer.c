#include "StdSceneSerializer.h"
#include "EntitySystem/StdVisibleEntity.h"

#include <Core/Types.h>
#include <Core/SimpleStandardLibrary.h>
#include <EntitySystem/StdEntity.h>
#include <EntitySystem/StdScene.h>


typedef struct SerialBuffer
{
    u64 Offset;
    u64 Size;
    void* pData;
} SerialBuffer;

void
serial_buffer_add(SerialBuffer* pBuffer, void* pData, u64 dataSize)
{
    if ((pBuffer->Size - pBuffer->Offset) <= dataSize)
    {
	u64 newSize = Max(dataSize, pBuffer->Size) * 2 + 1;

	// realoc
	void* pNewMemory = memory_allocate(newSize);
	memcpy(pNewMemory, pData, dataSize);
	memory_free(pBuffer->pData);

	pBuffer->Size = newSize;
	pBuffer->pData = pNewMemory;
    }

    memcpy(pBuffer->pData + pBuffer->Offset, pData, dataSize);
    pBuffer->Offset += dataSize;
}

SerialBuffer*
_entity_serialize(SerialBuffer* pBuffer, StdEntity entity)
{
    serial_buffer_add(pBuffer, &entity.Id, sizeof(i64));
    serial_buffer_add(pBuffer, &entity.Position, sizeof(v3));
    /* StdEntity Base; */
    /* /\**\/i64 Id; */
    /* /\**\/v3 Position; */
    return pBuffer;
}

SerialBuffer*
_visible_entity_serialize(SerialBuffer* pBuffer, StdVisibleEntity entity)
{
    /* string_builder_appendf(pSb, "") */
    /* v3 Position; */
    /* v2 Size; */
    /* v4 Color; */
    /* i64 Id; // is it index to an array? */

    return NULL;
}

void
std_scene_serialize(StdScene* pScene)
{
    i64 i, count = array_count(pScene->aVisibleEntities);
    for (i = 0; i < count; ++i)
    {

    }
}

void
std_scene_deserialize()
{
}
