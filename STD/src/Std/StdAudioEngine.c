#include "StdAudioEngine.h"

#include <Deps/miniaudio.h>
#include <Core/SimpleStandardLibrary.h>


static ma_engine* pEngine = NULL;
static ma_engine_config gConfig = {};

void
std_audio_engine_init()
{
    pEngine = memory_allocate(sizeof(ma_engine));
    gConfig = ma_engine_config_init();

    ma_result result = ma_engine_init(&gConfig, pEngine);
    if (result != MA_SUCCESS)
    {
	GERROR("Failed to initialize engine!\n");
	vassert_break();
    }
}

void
std_audio_engine_deinit()
{
    ma_engine_uninit(pEngine);
    memory_free(pEngine);
}

ma_engine*
std_audio_engine_get()
{
    return pEngine;
}
