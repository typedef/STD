#ifndef STD_INPUT_H
#define STD_INPUT_H

void std_input_init();
void std_input_deinit();

#define std_input_is_key_pressed(key) _std_input_is_key_pressed((int)key)
int _std_input_is_key_pressed(int key);
#define std_input_is_mouse_key_pressed(key) _std_input_is_mouse_key_pressed((int)key)
#define std_input_is_mouse_key_released(key) _std_input_is_mouse_key_released((int)key)

int _std_input_is_mouse_key_pressed(int key);
int _std_input_is_mouse_key_released(int key);

#endif // STD_INPUT_H
