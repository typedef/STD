#include "StdRenderer.h"
#include "vulkan_core.h"

#include <Std/StdTypes.h>
#include <Core/SimpleStandardLibrary.h>
#include <Core/SimpleVulkanLibrary.h>
#include <Core/SimpleAtlasFont.h>
#include <Core/SimpleMath.h>
#include <Std/StdAssetManager.h>
#include <Deps/stb_image.h>

#include <math.h>


typedef struct StdImageKeyValue
{
    const char* Key;
    void* Value;
} StdImageKeyValue;

#define Temp_Floor(x) ((int) floor(x))



/*
  #############################################
  DOCS: Rect Pipe
  #############################################
*/

typedef struct StdRectPipeSettings
{
    i32 MaxItems;
} StdRectPipeSettings;

typedef struct RectVertex
{
    v3 Position;
    v4 Color;
} RectVertex;

typedef struct RectPipeline
{
    SvlPipeline* pHandle;
    RectVertex* aVertices;
    i32* aIndices;
} RectPipeline;


/*
  #############################################
  DOCS: Image Pipe
  #############################################
*/

typedef struct StdImagePipeSettings
{
    i32 MaxItems;
    i64 MaxImagesCount;
} StdImagePipeSettings;

typedef struct ImageVertex
{
    v3 Position;
    v4 Color;
    v2 Uv;
    i32 TextureIndex;
} ImageVertex;

typedef struct StdImageMap
{
    i64 AssetId;
    i64 GpuId;
    v2 Size;
} StdImageMap;

typedef struct ImagePipeline
{
    // todo: ?
    SvlPipeline* pHandle;
    ImageVertex* aVertices;
    VkDescriptorImageInfo* aImageInfos;
    StdImageMap* aAssetIdToGpuId;
    i64 TextureSlotsCount;
} ImagePipeline;


/*
  #############################################
  DOCS: Ui Pipe
  #############################################
*/

typedef struct StdDrawCall
{
    i32 ViewportIndex;
    i32 Stride;
    i64 IndicesCount;
    // note: not bigger then MaxItemsCount
    void* pVertices;
} StdDrawCall;


/*
  #############################################
  DOCS: Rect Ui Pipe
  #############################################
*/

typedef struct StdRectUiPipeline
{
    SvlPipeline* pHandle;
    StdDrawCall* aDrawCalls;
} StdRectUiPipeline;


/*
  #############################################
  DOCS: Image Ui Pipe
  #############################################
*/

typedef struct StdImageUiPipeline
{
    SvlPipeline* pHandle;
    StdDrawCall* aDrawCalls;
    VkDescriptorImageInfo* aImageInfos;
    StdImageMap* aAssetIdToGpuId;
    i64 TextureSlotsCount;
} StdImageUiPipeline;


/*
  #############################################
  DOCS: Image Ui Pipe
  #############################################
*/

typedef struct FontVertex {
    v3 Position;
    v4 Color;
    v2 Uv;
    i32 FontIndex;
} FontVertex;

typedef struct StdFontUiPipeline
{
    SvlPipeline* pHandle;
    StdDrawCall* aDrawCalls;
    i64 MaxCharsCount;
    i64 MaxFontsCount;
} StdFontUiPipeline;

typedef struct StdUi
{
    StdRectUiPipeline RectPipe;
    StdImageUiPipeline ImagePipe;
    StdFontUiPipeline FontPipe;

    i64 CurrentViewportIndex;
    StdViewport* aViewports;
    v2 WindowSize;
} StdUi;



/*
  #############################################
  DOCS: Renderer
  #############################################
*/

typedef struct StdRenderer
{
    i8 IsInitialized;
    v4 ClearColor;
    i32 MaxItemsCount;

    // DOCS: Svl instance
    SvlInstance Instance;

    // DOCS: Main render pass
    svl_i32 RenderPassId;
    //svl_i32 AliasingRenderPassId;

    // DOCS: Pipes
    RectPipeline RectPipe;
    ImagePipeline ImagePipe;
    //FontPipeline FontPipe;

    // DOCS: Ui Pipes
    StdUi Ui;
} StdRenderer;

typedef struct FontUbo
{
    f32 Width;
    f32 EdgeTransition;
} FontUbo;

static StdRenderer* gStd = NULL;

// todo: add to fragment shader
// #extension GL_EXT_nonuniform_qualifier : require

/*
  #############################################
  DOCS: HELPERS
  #############################################
*/

void svl_draw_render_pass(SvlInstance* pInstance, SvlRenderPass* pRenderPass);
void svl_write_default_index_buffer(SvlInstance* pInstance, SvlPipeline* pPipe, u64 vertexSize, i64 maxInstanceCount);

/*
  #############################################
  DOCS: Rectangle pipeline
  #############################################
*/

typedef struct StdUiPipeSettings
{
    i32 MaxRectItems;
    i32 MaxImageItems;
    i32 MaxTextItems;
    v2i WindowSize;
} StdUiPipeSettings;

void std_renderer_ui_pipe_create(SvlInstance* pInstance, StdUiPipeSettings set);


/*
  #############################################
  DOCS: Rectangle pipeline
  #############################################
*/

void
std_renderer_rect_pipe_create(SvlInstance* pInstance, StdRectPipeSettings set)
{
    const i32 stagesCount = 2;

    SvlShaderStage aStages[] = {
	[0] = (SvlShaderStage) {
	    .Type = SvlShaderType_Vertex,
	    .ShaderSourcePath = resource_shader("Std.vert"),
	    .DescriptorsCount = 1,
	    .aDescriptors = {
		[0] = {
		    .pName = "u_CameraUbo",
		    .Type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
		    .Count = 1,
		    .Size = sizeof(m4)
		},
	    }
	},

	[1] = (SvlShaderStage) {
	    .Type = SvlShaderType_Fragment,
	    .ShaderSourcePath = resource_shader("Std.frag"),
	    .DescriptorsCount = 0,
	    .aDescriptors = {},
	}
    };

    SvlShaderAttribute aAttributes[] = {
	[0] = {
	    .Format = VK_FORMAT_R32G32B32_SFLOAT,
	    .Offset = OffsetOf(RectVertex, Position),
	},
	[1] = {
	    .Format = VK_FORMAT_R32G32B32A32_SFLOAT,
	    .Offset = OffsetOf(RectVertex, Color),
	},
    };

    SvlPipelineSettings pipeSets = {
	.Flags = SvlPipelineFlags_DepthStencil | SvlPipelineFlags_BackFaceCulling,

	// DOCS: Render pipeline description
	.Stride = sizeof(RectVertex),

	.StagesCount = stagesCount,
	.aStages = aStages,

	.AttributesCount = 2,
	.aAttributes = aAttributes,

	.PolygonMode = VK_POLYGON_MODE_FILL,

	// DOCS: Some shader constants
	.MaxInstanceCount = set.MaxItems,
    };

    // todo: move buffer creation inside pipe_create
    SvlPipeline* pSvlPipeline = svl_pipeline_create(pInstance, pipeSets);
    if (pSvlPipeline == NULL)
    {
	if (pInstance->Error == SvlErrorType_Pipeline
	    || pInstance->Error == SvlErrorType_Pipeline_Layout)
	{
	    GERROR("Can't create SvlPipeline with svl_pipeline_create!\n");
	    vguard(0);
	}

	vguard(0);
    }

    svl_write_default_index_buffer(pInstance, pSvlPipeline, sizeof(RectVertex), set.MaxItems);

    gStd->RectPipe = (RectPipeline) {
	.pHandle = pSvlPipeline,
    };

}


/*
  #############################################
  DOCS: Image pipeline
  #############################################
*/

void
std_renderer_image_pipe_create(SvlInstance* pInstance, StdImagePipeSettings set)
{
    i64 maxItemsCount = set.MaxItems;
    i64 maxImagesCount = set.MaxImagesCount;
    //GINFO("Maxitemscount: %d\n", maxItemsCount);

    SvlShaderStage aStages[] = {
	[0] = (SvlShaderStage) {
	    .Type = SvlShaderType_Vertex,
	    .ShaderSourcePath = resource_shader("StdImage.vert"),
	    .DescriptorsCount = 1,
	    .aDescriptors = {
		[0] = {
		    .pName = "u_CameraUbo",
		    .Type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
		    .Count = 1,
		    .Size = sizeof(m4)
		},
	    }
	},

	[1] = (SvlShaderStage) {
	    .Type = SvlShaderType_Fragment,
	    .ShaderSourcePath = resource_shader("StdImage.frag"),
	    .DescriptorsCount = 1,
	    .aDescriptors = {
		[0] = {
		    .pName = "u_Textures",
		    .Type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
		    .Count = maxImagesCount,
		},
	    }
	}
    };

    SvlShaderAttribute aAttributes[] = {
	[0] = {
	    .Format = VK_FORMAT_R32G32B32_SFLOAT,
	    .Offset = OffsetOf(ImageVertex, Position),
	},

	[1] = {
	    .Format = VK_FORMAT_R32G32B32A32_SFLOAT,
	    .Offset = OffsetOf(ImageVertex, Color),
	},

	[2] = {
	    .Format = VK_FORMAT_R32G32_SFLOAT,
	    .Offset = OffsetOf(ImageVertex, Uv),
	},

	[3] = {
	    .Format = VK_FORMAT_R32_SINT,
	    .Offset = OffsetOf(ImageVertex, TextureIndex),
	},
    };

    SvlPipelineSettings pipeSets = {
	.Flags = SvlPipelineFlags_DepthStencil | SvlPipelineFlags_BackFaceCulling,

	// DOCS: Render pipeline description
	.Stride = sizeof(ImageVertex),

	.StagesCount = ArrayCount(aStages),
	.aStages = aStages,

	.AttributesCount = ArrayCount(aAttributes),
	.aAttributes = aAttributes,

#if 0
	.PolygonMode = VK_POLYGON_MODE_LINE,
#else
	.PolygonMode = VK_POLYGON_MODE_FILL,
#endif

	// DOCS: Some shader constants
	.MaxInstanceCount = maxItemsCount,
    };

    SvlPipeline* pSvlPipeline = svl_pipeline_create(pInstance, pipeSets);
    if (pSvlPipeline == NULL)
    {
	if (pInstance->Error == SvlErrorType_Pipeline)
	{
	    GERROR("Can't create SvlPipeline with svl_pipeline_create!\n");
	    vguard(0);
	}

	vguard(0);
    }

    svl_write_default_index_buffer(pInstance, pSvlPipeline, sizeof(ImageVertex), set.MaxItems);

    VkDescriptorImageInfo* aImageInfos = NULL;
    { // DOCS: Crete image infos buffer
	aImageInfos = sva_reserve(aImageInfos, maxItemsCount);
    }

    gStd->ImagePipe = (ImagePipeline) {
	.aImageInfos = aImageInfos,
	.pHandle = pSvlPipeline,
	.TextureSlotsCount = maxImagesCount,
    };

}


/*
  #############################################
  DOCS: Ui Base Pipe
  #############################################
*/

#define Max3(e0, e1, e2)                        \
    ({                                          \
	__typeof__(e0) emax;                    \
						\
	if (e0 > e1 && e0 > e2)                 \
	    emax = e0;                          \
	else if (e1 > e0 && e1 > e2)            \
	    emax = e1;                          \
	else if (e2 > e0 && e2 > e1)            \
	    emax = e2;                          \
	else                                    \
	    emax = e0;                          \
						\
	e0;                                     \
    })


void
std_renderer_rect_ui_pipe_create(SvlInstance* pInstance, i32 maxRectItems)
{
    i64 maxInstanceCount = maxRectItems;// GINFO("Maxitemscount: %d\n", maxItemsCount);

    SvlShaderStage aStages[] = {
	[0] = (SvlShaderStage) {
	    .Type = SvlShaderType_Vertex,
	    .ShaderSourcePath = resource_shader("Std.vert"),
	    .DescriptorsCount = 1,
	    .aDescriptors = {
		[0] = {
		    .pName = "u_CameraUbo",
		    .Type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
		    .Count = 1,
		    .Size = sizeof(m4)
		},
	    }
	},

	[1] = (SvlShaderStage) {
	    .Type = SvlShaderType_Fragment,
	    .ShaderSourcePath = resource_shader("Std.frag"),
	    .DescriptorsCount = 0,
	    .aDescriptors = {}
	}
    };

    SvlShaderAttribute aAttributes[] = {
	[0] = {
	    .Format = VK_FORMAT_R32G32B32_SFLOAT,
	    .Offset = OffsetOf(RectVertex, Position),
	},

	[1] = {
	    .Format = VK_FORMAT_R32G32B32A32_SFLOAT,
	    .Offset = OffsetOf(RectVertex, Color),
	}
    };

    SvlPipelineSettings pipeSets = {
	.Flags = SvlPipelineFlags_DepthStencil | SvlPipelineFlags_BackFaceCulling,
	// DOCS: Render pipeline description
	.Stride = sizeof(RectVertex),
	.StagesCount = ArrayCount(aStages),
	.aStages = aStages,
	.AttributesCount = ArrayCount(aAttributes),
	.aAttributes = aAttributes,
	.PolygonMode = VK_POLYGON_MODE_FILL,
	// DOCS: Some shader constants
	.MaxInstanceCount = maxInstanceCount,
    };

    SvlPipeline* pSvlPipeline = svl_pipeline_create(pInstance, pipeSets);
    if (pSvlPipeline == NULL)
    {
	if (pInstance->Error == SvlErrorType_Pipeline)
	{
	    GERROR("Can't create SvlPipeline with svl_pipeline_create!\n");
	    vguard(0);
	}

	vguard(0);
    }

    svl_write_default_index_buffer(pInstance, pSvlPipeline, sizeof(RectVertex), maxInstanceCount);

    gStd->Ui.RectPipe = (StdRectUiPipeline) {
	.pHandle = pSvlPipeline,
    };

}

void
std_renderer_image_ui_pipe_create(SvlInstance* pInstance, i32 maxItems, i32 maxImages)
{
    i64 maxRectItemsCount = maxItems;
    i64 maxImagesCount = maxImages;
    i64 maxInstanceCount = Max(maxRectItemsCount, maxImagesCount);

    SvlShaderStage aStages[] = {
	[0] = (SvlShaderStage) {
	    .Type = SvlShaderType_Vertex,
	    .ShaderSourcePath = resource_shader("StdImage.vert"),
	    .DescriptorsCount = 1,
	    .aDescriptors = {
		[0] = {
		    .pName = "u_CameraUbo",
		    .Type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
		    .Count = 1,
		    .Size = sizeof(m4)
		},
	    }
	},

	[1] = (SvlShaderStage) {
	    .Type = SvlShaderType_Fragment,
	    .ShaderSourcePath = resource_shader("StdImage.frag"),
	    .DescriptorsCount = 1,
	    .aDescriptors = {
		[0] = {
		    .pName = "u_Textures",
		    .Type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
		    .Count = 100,
		},
	    }
	}
    };

    SvlShaderAttribute aAttributes[] = {
	[0] = {
	    .Format = VK_FORMAT_R32G32B32_SFLOAT,
	    .Offset = OffsetOf(ImageVertex, Position),
	},

	[1] = {
	    .Format = VK_FORMAT_R32G32B32A32_SFLOAT,
	    .Offset = OffsetOf(ImageVertex, Color),
	},

	[2] = {
	    .Format = VK_FORMAT_R32G32_SFLOAT,
	    .Offset = OffsetOf(ImageVertex, Uv),
	},

	[3] = {
	    .Format = VK_FORMAT_R32_SINT,
	    .Offset = OffsetOf(ImageVertex, TextureIndex),
	},
    };

    SvlPipelineSettings pipeSets = {
	.Flags = SvlPipelineFlags_DepthStencil | SvlPipelineFlags_BackFaceCulling,
	// DOCS: Render pipeline description
	.Stride = sizeof(ImageVertex),
	.StagesCount = ArrayCount(aStages),
	.aStages = aStages,
	.AttributesCount = ArrayCount(aAttributes),
	.aAttributes = aAttributes,
	.PolygonMode = VK_POLYGON_MODE_FILL,

	// DOCS: Some shader constants
	.MaxInstanceCount = maxInstanceCount,
    };

    SvlPipeline* pSvlPipeline = svl_pipeline_create(pInstance, pipeSets);
    if (pSvlPipeline == NULL)
    {
	if (pInstance->Error == SvlErrorType_Pipeline)
	{
	    GERROR("Can't create SvlPipeline with svl_pipeline_create!\n");
	    vguard(0);
	}

	vguard(0);
    }

    svl_write_default_index_buffer(pInstance, pSvlPipeline, sizeof(ImageVertex), maxInstanceCount);

    VkDescriptorImageInfo* aImageInfos = NULL;
    { // DOCS: Crete image infos buffer
	aImageInfos = sva_reserve(aImageInfos, maxInstanceCount);
    }

    gStd->Ui.ImagePipe = (StdImageUiPipeline) {
	.aImageInfos = aImageInfos,
	.pHandle = pSvlPipeline,
	.TextureSlotsCount = maxImagesCount,
    };

}

// todo: different font atlases for dif fonts
// todo: font atlas index
void
std_renderer_font_ui_pipe_create(SvlInstance* pInstance, i32 maxItems, i32 maxCharItems)
{
    // todo: isnt it the same variable
    i64 maxRectItemsCount = maxItems;
    i64 maxCharItemsCount = maxCharItems;
    i64 maxInstanceCount = Max(maxRectItemsCount, maxCharItemsCount);

    const i32 c_fonts_count = 100;

    SvlShaderStage aStages[] = {
	[0] = (SvlShaderStage) {
	    .Type = SvlShaderType_Vertex,
	    .ShaderSourcePath = resource_shader("StdUiFont.vert"),
	    .DescriptorsCount = 1,
	    .aDescriptors = {
		[0] = {
		    .pName = "u_CameraUbo",
		    .Type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
		    .Count = 1,
		    .Size = sizeof(m4)
		},
	    }
	},
	[1] = (SvlShaderStage) {
	    .Type = SvlShaderType_Fragment,
	    .ShaderSourcePath = resource_shader("StdUiFont.frag"),
	    .DescriptorsCount = 2,
	    .aDescriptors = {
		[0] = {
		    .pName = "u_FontUbo",
		    .Type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
		    .Count = 1,
		    .Size = sizeof(FontUbo),
		},

		[1] = {
		    .pName = "u_FontAtlas",
		    .Type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
		    .Count = c_fonts_count,
		}
	    }
	}
    };

    SvlShaderAttribute aAttributes[] = {
	[0] = {
	    .Format = VK_FORMAT_R32G32B32_SFLOAT,
	    .Offset = OffsetOf(FontVertex, Position),
	},

	[1] = {
	    .Format = VK_FORMAT_R32G32B32A32_SFLOAT,
	    .Offset = OffsetOf(FontVertex, Color),
	},

	[2] = {
	    .Format = VK_FORMAT_R32G32_SFLOAT,
	    .Offset = OffsetOf(FontVertex, Uv),
	},

	[3] = {
	    .Format = VK_FORMAT_R32_SINT,
	    .Offset = OffsetOf(FontVertex, FontIndex),
	},
    };

    SvlPipelineSettings pipeSets = {
	.Flags = SvlPipelineFlags_DepthStencil | SvlPipelineFlags_BackFaceCulling,

	// DOCS: Render pipeline description
	.Stride = sizeof(FontVertex),

	.StagesCount = ArrayCount(aStages),
	.aStages = aStages,

	.AttributesCount = ArrayCount(aAttributes),
	.aAttributes = aAttributes,

	.PolygonMode = VK_POLYGON_MODE_FILL,

	// DOCS: Some shader constants
	.MaxInstanceCount = maxInstanceCount,
    };

    SvlPipeline* pSvlPipeline = svl_pipeline_create(pInstance, pipeSets);
    if (pSvlPipeline == NULL)
    {
	if (pInstance->Error == SvlErrorType_Pipeline)
	{
	    GERROR("Can't create SvlPipeline with svl_pipeline_create!\n");
	    vguard(0);
	}

	vguard(0);
    }

    svl_write_default_index_buffer(pInstance, pSvlPipeline, sizeof(FontVertex), maxInstanceCount);

    gStd->Ui.FontPipe = (StdFontUiPipeline) {
	.pHandle = pSvlPipeline,
	.MaxCharsCount = maxCharItems,
	.MaxFontsCount = c_fonts_count
    };
}

void
std_renderer_ui_pipe_create(SvlInstance* pInstance, StdUiPipeSettings set)
{
    // DOCS: Set default viewport

    StdViewport defaultViewport = {
	.X0 = 0, .Y0 = 0,
	.Width = set.WindowSize.Width, .Height = set.WindowSize.Height
    };

    //GINFO("Default viewport %d %d %d %d\n", defaultViewport.X0, defaultViewport.Y0, defaultViewport.X1, defaultViewport.Y1);

    StdViewport* aViewports = NULL;
    sva_add(aViewports, defaultViewport);

    std_renderer_rect_ui_pipe_create(pInstance, set.MaxRectItems);
    std_renderer_image_ui_pipe_create(pInstance, set.MaxRectItems, set.MaxImageItems);
    std_renderer_font_ui_pipe_create(pInstance, set.MaxRectItems, set.MaxTextItems);

    gStd->Ui.CurrentViewportIndex = 0;
    gStd->Ui.aViewports = aViewports;
    gStd->Ui.WindowSize = (v2) { set.WindowSize.X, set.WindowSize.Y };

#if 0
    i64 maxRectItemsCount = set.MaxRectItems;
    i64 maxImageItemsCount = set.MaxImageItems;
    i64 maxTextItemsCount = set.MaxTextItems;

    i64 maxInstanceCount = Max3(maxRectItemsCount, maxImageItemsCount, maxTextItemsCount);
    // GINFO("Maxitemscount: %d\n", maxItemsCount);

    SvlShaderStage aStages[] = {
	[0] = (SvlShaderStage) {
	    .Type = SvlShaderType_Vertex,
	    .ShaderSourcePath = resource_shader("StdUi.vert"),
	    .DescriptorsCount = 1,
	    .aDescriptors = {
		[0] = {
		    .pName = "u_CameraUbo",
		    .Type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
		    .Count = 1,
		    .Size = sizeof(m4)
		},
	    }
	},

	[1] = (SvlShaderStage) {
	    .Type = SvlShaderType_Fragment,
	    .ShaderSourcePath = resource_shader("StdUi.frag"),
	    .DescriptorsCount = 4,
	    .aDescriptors = {
		[0] = {
		    .pName = "u_Settings",
		    .Type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
		    .Count = 1,
		    .Size = sizeof(i32)
		},
		[1] = {
		    .pName = "u_Visible",
		    .Type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
		    .Count = 10,
		    .Size = sizeof(v4),
		},
		[2] = {
		    .pName = "u_FontAtlas",
		    .Type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
		    .Count = 1,
		},
		[3] = {
		    .pName = "u_Textures",
		    .Type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
		    .Count = 100,
		},
	    }
	}
    };

    SvlShaderAttribute aAttributes[] = {
	[0] = {
	    .Format = VK_FORMAT_R32G32B32_SFLOAT,
	    .Offset = OffsetOf(UiVertex, Position),
	},

	[1] = {
	    .Format = VK_FORMAT_R32G32B32A32_SFLOAT,
	    .Offset = OffsetOf(UiVertex, Color),
	},

	[2] = {
	    .Format = VK_FORMAT_R32G32_SFLOAT,
	    .Offset = OffsetOf(UiVertex, Uv),
	},

	[3] = {
	    .Format = VK_FORMAT_R32_SINT,
	    .Offset = OffsetOf(UiVertex, TextureIndex),
	},

	[4] = {
	    .Format = VK_FORMAT_R32_SINT,
	    .Offset = OffsetOf(UiVertex, ViewportIndex),
	},
    };

    SvlPipelineSettings pipeSets = {
	.Flags = SvlPipelineFlags_DepthStencil | SvlPipelineFlags_BackFaceCulling,

	// DOCS: Render pipeline description
	.Stride = sizeof(ImageVertex),

	.StagesCount = ArrayCount(aStages),
	.aStages = aStages,

	.AttributesCount = ArrayCount(aAttributes),
	.aAttributes = aAttributes,

#if 0
	.PolygonMode = VK_POLYGON_MODE_LINE,
#else
	.PolygonMode = VK_POLYGON_MODE_FILL,
#endif

	// DOCS: Some shader constants
	.MaxInstanceCount = maxInstanceCount,
    };

    SvlPipeline* pSvlPipeline = svl_pipeline_create(pInstance, pipeSets);
    if (pSvlPipeline == NULL)
    {
	if (pInstance->Error == SvlErrorType_Pipeline)
	{
	    GERROR("Can't create SvlPipeline with svl_pipeline_create!\n");
	    vguard(0);
	}

	vguard(0);
    }


    VkDescriptorImageInfo* aImageInfos = NULL;
    { // DOCS: Crete image infos buffer
	aImageInfos = sva_reserve(aImageInfos, maxInstanceCount);
    }

    StdViewport defaultViewport = {
	.StartX = 0, .StartY = 0,
	.EndX = set.WindowSize.X, .EndY = set.WindowSize.Y
    };

    StdViewport* aViewports = NULL;
    sva_add(aViewports, defaultViewport);

    gStd->UiPipe = (StdUiPipeline) {
	.aImageInfos = aImageInfos,
	.pHandle = pSvlPipeline,
	.aViewports = aViewports,
	// todo: ????
	//.MaxCharsCount
    };
#endif
}


void
std_renderer_create(StdRendererSettings* pSettings)
{
    v2i windowSize = pSettings->WindowSize;
    i32 maxItemsCount = pSettings->MaxItemsCount;
    i32 maxImageCount = pSettings->MaxImageCount;
    i32 maxCharsCount = pSettings->MaxCharsCount;
    vguard(windowSize.X > 0 && windowSize.Y > 0);
    vguard(maxItemsCount > 0);
    i8 isDebug = pSettings->IsDebug;
    i8 isVsync = pSettings->IsVsync;

    SvlSettings svlSet = {
	.IsDebug = isDebug,
	.IsVsync = isVsync,
	.IsPostProcess = 0,
	.IsDevicePropsPrintable = 1,
	// in vulkan 1.1 we have problem with VK_DESCRIPTOR_SET_LAYOUT_CREATE_UPDATE_AFTER_BIND_POOL_BIT
	.VulkanVersion = VK_MAKE_API_VERSION(0, 1, 3, 0),
	// note: it should be inlined with svl backend ptr
	.pWindowBackend = pSettings->pWindowBackend,
	.pName = "Demo app",
	.Extent = (SvlExtent) {
	    .Width = windowSize.Width,
	    .Height = windowSize.Height,
	},

	.Paths = {
	    .pRootDirectory = path_get_current_directory()
	},

    };

    SvlInstance svlInstance = svl_create(&svlSet);
    if (!svlInstance.IsInitialized)
    {
	//printf(SvlTerminalRed("Error")"\n");
	return;
    }

    SvlRenderPass svlRenderPass = svl_default_2d_render_pass_create(&svlInstance);
    //SvlRenderPass defaultRenderPass = svl_default_render_pass_create(&svlInstance);
    if (svlInstance.Error != 0)
    {
	GERROR("Failed render pass creation!\n");

	// GINFO("image views count: %ld\n", sva_count(svlRenderPass.aImageViews));
	// GINFO("swap images count: %ld\n", sva_count(svlInstance.aSwapImageViews));

	vassert_break();
	return;
    }


    // NOTE: renderer part
    // NOTE: renderer part
    // NOTE: renderer part

    gStd = (StdRenderer*) memory_allocate(sizeof(StdRenderer));
    gStd->Instance = svlInstance;
    gStd->RenderPassId = svlRenderPass.Id;
    //gStd->AliasingRenderPassId = defaultRenderPass.Id;
    gStd->MaxItemsCount = maxItemsCount;
    gStd->IsInitialized = 1;

    std_renderer_rect_pipe_create(&svlInstance, (StdRectPipeSettings) { .MaxItems = maxItemsCount });
    std_renderer_image_pipe_create(&svlInstance, (StdImagePipeSettings) { .MaxItems = maxItemsCount, .MaxImagesCount = maxImageCount });
    //std_renderer_font_pipe_create(&svlInstance, (StdImagePipeSettings) { .MaxItems = maxCharsCount });

    std_renderer_ui_pipe_create(&svlInstance, (StdUiPipeSettings) {
	    .MaxRectItems = maxItemsCount,
	    .MaxImageItems = maxImageCount,
	    .MaxTextItems = maxCharsCount,

	    .WindowSize = windowSize
	});
}

void
std_renderer_destroy()
{
    svl_destroy(&gStd->Instance);
    memory_free(gStd);
}

void
std_renderer_prepare_for_destruction()
{
    vkDeviceWaitIdle(gStd->Instance.Device);
}

void
std_renderer_set_clear_color(v4 clr)
{
    // note: we need this?
    gStd->ClearColor = clr;

    SvlRenderPass* pRenderPass = svl_render_pass(&gStd->Instance, gStd->RenderPassId);
    pRenderPass->ClearColor = (VkClearColorValue) { clr.R, clr.G, clr.B, clr.A };
}

void
std_renderer_set_camera(m4* pViewProjection)
{
    SvlPipeline* aPipes[] = {
	gStd->RectPipe.pHandle,
	gStd->ImagePipe.pHandle,
    };

    for (svl_i32 p = 0; p < ArrayCount(aPipes); ++p)
    {
	SvlPipeline* pPipe = aPipes[p];
	SvlUniform* pCameraUniform = svl_pipeline_get_uniform(pPipe, "u_CameraUbo");

	if (pCameraUniform)
	    svl_uniform_set(&gStd->Instance, pPipe, pCameraUniform, pViewProjection, 0, sizeof(m4));
    }
}

/*
  DOCS: Expect aImageViews[0] == default_white_texture

  NOTE: Usage

  asset_manager_load_asset();
  asset_manager_load_asset();
  asset_manager_load_asset();

  i64 texturesCount = 0;
  void* aTexturesVoid = NULL;
  asset_manager_get_all_textures(&aTexturesVoid, &texturesCount);

  std_renderer_set_textures(aTexturesVoid, texturesCount);

*/

void
base_set_images(SvlPipeline* pPipe, StdImageMap** paAssetIdToGpuId, VkDescriptorImageInfo** paImageInfos, StdTexture* aTextures, i32 texturesCount, i32 bindingIndex, i64 availableSlots)
{
    VkDescriptorImageInfo* aImageInfos = *paImageInfos;
    StdImageMap* aAssetIdToGpuId = *paAssetIdToGpuId;

    vassert_not_null(aImageInfos);

    sva_clear(aImageInfos);
    sva_clear(aAssetIdToGpuId);

    for (i64 i = 0; i < texturesCount; ++i)
    {
	if (i >= availableSlots)
	{
	    // todo: print warning
	    break;
	}

	StdTexture rec = aTextures[i];

	StdImageMap map = {
	    .AssetId = rec.Id,
	    .GpuId = i,
	    .Size = rec.Size
	};
	sva_add(aAssetIdToGpuId, map);

	VkDescriptorImageInfo imageInfo = {
	    .sampler = rec.Handle.Sampler,
	    .imageView = rec.Handle.View,
	    .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
	};

	aImageInfos[i] = imageInfo;
    }

    // DOCS: Set all unused image infos to 0
    if (texturesCount < availableSlots)
    {
	for (i64 i = texturesCount; i < availableSlots; ++i)
	{
	    aImageInfos[i] = aImageInfos[0];
	}
    }

    //GINFO(":UPdate availableSlots :%d\n", availableSlots);

    VkWriteDescriptorSet writeDescrSet = {
	.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
	.dstSet = pPipe->Binding.DescriptorSet,
	.dstBinding = bindingIndex, // 1
	.dstArrayElement = 0,
	.descriptorCount = availableSlots,
	.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
	.pImageInfo = aImageInfos,
    };

    vkUpdateDescriptorSets(gStd->Instance.Device, 1, &writeDescrSet, 0, NULL);

    *paImageInfos = aImageInfos;
    *paAssetIdToGpuId = aAssetIdToGpuId;
}

void
std_renderer_set_textures(StdTexture* aTextures, i32 texturesCount)
{
    i64 availableTextureSlots   = gStd->ImagePipe.TextureSlotsCount;
    i64 uiAvailableTextureSlots = gStd->Ui.ImagePipe.TextureSlotsCount;

    //GINFO("Ac : %ld\n uiAc: %ld\n", availableTextureSlots, uiAvailableTextureSlots);

    base_set_images(gStd->ImagePipe.pHandle,
		    &gStd->ImagePipe.aAssetIdToGpuId,
		    &gStd->ImagePipe.aImageInfos,
		    aTextures, texturesCount, 1, availableTextureSlots);

    base_set_images(gStd->Ui.ImagePipe.pHandle,
		    &gStd->Ui.ImagePipe.aAssetIdToGpuId,
		    &gStd->Ui.ImagePipe.aImageInfos,
		    aTextures, texturesCount, 1, uiAvailableTextureSlots);
}

SvlInstance*
std_renderer_get_instance()
{
    return &gStd->Instance;
}

void
std_renderer_update()
{
    if (!gStd->IsInitialized)
	return;

    SvlInstance* pInstance = &gStd->Instance;

    /*

      for (renderPass in renderPasses) {
      for (pipe in pipes) {
      for (viewport in viewports) {

      }
      }
      }


    */

    { // DOCS: Set buffers first
	// DOCS: Rect pipe
	if (sva_count(gStd->RectPipe.aVertices) > 0)
	{
	    svl_size rectSize = sva_count(gStd->RectPipe.aVertices) * sizeof(gStd->RectPipe.aVertices[0]);
	    svl_buffer_set_data(pInstance, &gStd->RectPipe.pHandle->Vertex, gStd->RectPipe.aVertices, 0, rectSize);
	    gStd->RectPipe.pHandle->IsActive = 1;
	}
	else
	{
	    gStd->RectPipe.pHandle->IsActive = 0;
	}

	// DOCS: Image pipe
	if (sva_count(gStd->ImagePipe.aVertices) > 0)
	{
	    svl_size imageSize = sva_count(gStd->ImagePipe.aVertices) * sizeof(gStd->ImagePipe.aVertices[0]);
	    svl_buffer_set_data(pInstance, &gStd->ImagePipe.pHandle->Vertex, gStd->ImagePipe.aVertices, 0, imageSize);
	    gStd->ImagePipe.pHandle->IsActive = 1;
	}
	else
	{
	    gStd->ImagePipe.pHandle->IsActive = 0;
	}
    }

    { // DOCS: Process main/single render pass
	SvlRenderPass* pDefaultRenderPass = &pInstance->aRenderPasses[0];
	svl_draw_render_pass(pInstance, pDefaultRenderPass);
    }


}

void
std_renderer_window_resized(v2 newSize)
{
    /* SvlInstance* pInstance = &gStd->Instance; */
    /* SvlRenderPass* pRenderPass = &pInstance->aRenderPasses[0]; */

    /* SvlSwapchainRecreateSettings set = { */
    /*  .Width  = newSize.Width, */
    /*  .Height = newSize.Height */
    /* }; */
    /* svl_swapchain_recreate(pInstance, set); */
}

void
std_renderer_create_texture_ext(i32 width, i32 height, i32 channels, void* pData, void* pSvlTexture)
{
    SvlInstance* pInstance = &gStd->Instance;

    SvlTextureSettings set = {
	.Width = width,
	.Height = height,
	.Channels = channels,
	.pData = pData,

	.Flags = SvlTextureFlags_Nearest | SvlTextureFlags_ClampToBorder
    };

    SvlTexture svlTexture = svl_texture_create(pInstance, set);
    if (pInstance->Error != 0)
    {
	printf("Error: svl_texture_create()\n");
	vassert_break();
    }

    *((SvlTexture*)pSvlTexture) = svlTexture;
}

void
std_renderer_destroy_texture(SvlTexture* pSvlTexture)
{
    svl_texture_destroy(&gStd->Instance, pSvlTexture);
}

void
std_renderer_draw_rect_ext(v3 position, v2 size, v4 colors[4])
{
    v2 p0 = v2_new(position.X, position.Y);

    RectVertex vertices[] = {
	[0] = (RectVertex) {
	    .Position = v3_new(p0.X, p0.Y + size.Y, position.Z),
	    .Color = colors[0],
	},
	[1] = (RectVertex) {
	    .Position = v3_new(p0.X, p0.Y, position.Z),
	    .Color = colors[1],
	},
	[2] = (RectVertex) {
	    .Position = v3_new(p0.X + size.X, p0.Y, position.Z),
	    .Color = colors[2],
	},
	[3] = (RectVertex) {
	    .Position = v3_new(p0.X + size.X, p0.Y + size.Y, position.Z),
	    .Color = colors[3],
	},
    };

    RectPipeline* pPipe = &gStd->RectPipe;
    sva_add(pPipe->aVertices, vertices[0]);
    sva_add(pPipe->aVertices, vertices[1]);
    sva_add(pPipe->aVertices, vertices[2]);
    sva_add(pPipe->aVertices, vertices[3]);

    pPipe->pHandle->VerticesCount += 4;
    pPipe->pHandle->IndicesCount += 6;

    //svl_buffer_set_data(&gStd->Instance, &pPipe->Vertex, vertices, vbufferOffset, sizeof(vertices));
}

void
std_renderer_draw_rect(v3 position, v2 size, v4 color)
{
    v4 colors[4] = {
	color, color, color, color
    };
    std_renderer_draw_rect_ext(position, size, colors);
}

// todo: rename textureInd -> textureAssetId
void
std_renderer_draw_texture_ext(v3 position, v2 size, v4 aColors[4], v2 aUvs[4], i64 assetId)
{
    i64 ind = sva_index_of(gStd->ImagePipe.aAssetIdToGpuId, item.AssetId == assetId);
    if (ind == -1)
	return;

    StdImageMap image = gStd->ImagePipe.aAssetIdToGpuId[ind];
    i64 gpuId = image.GpuId;
    if (size.X <= 0 || size.Y <= 0)
    {
	real scale = -size.X;
	size = image.Size;
	size = v2_mulv(size, scale);
    }

    v3 p0 = v3_new(position.X, position.Y, position.Z);

    ImageVertex vertices[] = {
	[0] = (ImageVertex) {
	    .Position = v3_new(p0.X, p0.Y + size.Y, p0.Z),
	    .Color = aColors[0],
	    .Uv = aUvs[0],
	    .TextureIndex = gpuId,
	},
	[1] = (ImageVertex) {
	    .Position = v3_new(p0.X, p0.Y, p0.Z),
	    .Color = aColors[1],
	    .Uv = aUvs[1],
	    .TextureIndex = gpuId,
	},
	[2] = (ImageVertex) {
	    .Position = v3_new(p0.X + size.X, p0.Y, p0.Z),
	    .Color = aColors[2],
	    .Uv = aUvs[2],
	    .TextureIndex = gpuId,
	},
	[3] = (ImageVertex) {
	    .Position = v3_new(p0.X + size.X, p0.Y + size.Y, p0.Z),
	    .Color = aColors[3],
	    .Uv = aUvs[3],
	    .TextureIndex = gpuId,
	},
    };

    ImagePipeline* pPipe = &gStd->ImagePipe;
    sva_add(pPipe->aVertices, vertices[0]);
    sva_add(pPipe->aVertices, vertices[1]);
    sva_add(pPipe->aVertices, vertices[2]);
    sva_add(pPipe->aVertices, vertices[3]);

    //GINFO("verticesCount: %d\n", pPipe->VerticesCount);
    /* i64 vbufferOffset = pPipe->VerticesCount * sizeof(ImageVertex); */
    /* svl_buffer_set_data(&gStd->Instance, &pPipe->Vertex, vertices, vbufferOffset, sizeof(vertices)); */

    pPipe->pHandle->VerticesCount += 4;
    pPipe->pHandle->IndicesCount += 6;
}

void
std_renderer_draw_texture(v3 position, v2 size, i64 assetId)
{
    v4 whiteColors[] = {
	v4_new(1,1,1,1),
	v4_new(1,1,1,1),
	v4_new(1,1,1,1),
	v4_new(1,1,1,1),
    };
    v2 aUvs[] = {
	v2_new(0, 0),
	v2_new(0, 1),
	v2_new(1, 1),
	v2_new(1, 0),
    };

    std_renderer_draw_texture_ext(position, size, whiteColors, aUvs, assetId);
}

void
std_renderer_draw_image(v3 position, v2 size, i64 assetId)
{
    v4 whiteColors[] = {
	v4_new(1,1,1,1),
	v4_new(1,1,1,1),
	v4_new(1,1,1,1),
	v4_new(1,1,1,1),
    };
    v2 aUvs[] = {
	v2_new(0, 0),
	v2_new(0, 1),
	v2_new(1, 1),
	v2_new(1, 0),
    };

    std_renderer_draw_texture_ext(position, size, whiteColors, aUvs, assetId);
}

void
std_renderer_draw_color_image(v3 position, v2 size, v4 color, i64 assetId)
{
    v4 whiteColors[] = { color, color, color, color };
    v2 aUvs[] = {
	v2_new(0, 0),
	v2_new(0, 1),
	v2_new(1, 1),
	v2_new(1, 0),
    };

    std_renderer_draw_texture_ext(position, size, whiteColors, aUvs, assetId);
}

void
std_renderer_draw_color_image_uv(v3 position, v2 size, v4 color, i64 assetId, v2 aUvs[4])
{
    v4 whiteColors[] = { color, color, color, color };
    std_renderer_draw_texture_ext(position, size, whiteColors, aUvs, assetId);
}


/*
  #############################################
  DOCS: Ui Drawing
  #############################################
*/

i32
std_viewport_equal(StdViewport v0, StdViewport v1)
{
    i32 isEqual = (v0.X0 == v1.X0
		   && v0.Y0 == v1.Y0
		   && v0.Width == v1.Width
		   && v0.Height == v1.Height);
    return isEqual;
}

i64
_get_viewport_index(StdDrawCall* aDrawCalls, StdViewport viewport)
{
    i64 i, count = sva_count(aDrawCalls);
    for (i = 0; i < count; ++i)
    {
	StdDrawCall drawCall = aDrawCalls[i];
	StdViewport stdViewport = gStd->Ui.aViewports[drawCall.ViewportIndex];

	if (std_viewport_equal(viewport, stdViewport))
	{
	    return i;
	}
    }

    return -1;
}

void
add_draw_call(StdDrawCall** paDrawCalls, i32 stride, i64 count, i64 indicesCount, void* pVertices)
{
    StdDrawCall* aDrawCalls = *paDrawCalls;

    i64 drawCallIndex = sva_index_of(aDrawCalls, item.ViewportIndex == gStd->Ui.CurrentViewportIndex);
    if (drawCallIndex == -1)
    {
	// DOCS: Create new array and store into structure
	void* aVertices = NULL;
	sva_add_multiple(aVertices, pVertices, stride, count);

	StdDrawCall drawCall = {
	    .ViewportIndex = gStd->Ui.CurrentViewportIndex,
	    .Stride = stride,
	    .IndicesCount = indicesCount,
	    // note: not bigger then MaxItemsCount
	    .pVertices = aVertices
	};
	sva_add(aDrawCalls, drawCall);
	goto AddDrawCallReturn;
    }

    StdDrawCall* pDrawCall = &aDrawCalls[drawCallIndex];
    sva_add_multiple(pDrawCall->pVertices, pVertices, stride, count);
    pDrawCall->IndicesCount += indicesCount;

AddDrawCallReturn:
    *paDrawCalls = aDrawCalls;
}

void
std_renderer_set_ui_camera(m4* pViewProjection)
{
    SvlRenderPass* pRenderPass = svl_render_pass(&gStd->Instance, gStd->RenderPassId);

    { // DOCS: Rect pipe
	SvlPipeline* pPipe = gStd->Ui.RectPipe.pHandle;
	SvlUniform* pCameraUniform = svl_pipeline_get_uniform(pPipe, "u_CameraUbo");
	svl_uniform_set(&gStd->Instance, pPipe, pCameraUniform, pViewProjection, 0, sizeof(m4));
    }

    { // DOCS: Image pipe
	SvlPipeline* pPipe = gStd->Ui.ImagePipe.pHandle;
	SvlUniform* pCameraUniform = svl_pipeline_get_uniform(pPipe, "u_CameraUbo");
	svl_uniform_set(&gStd->Instance, pPipe, pCameraUniform, pViewProjection, 0, sizeof(m4));
    }


    { // DOCS: Font pipe
	SvlPipeline* pPipe = gStd->Ui.FontPipe.pHandle;
	SvlUniform* pCameraUniform = svl_pipeline_get_uniform(pPipe, "u_CameraUbo");
	svl_uniform_set(&gStd->Instance, pPipe, pCameraUniform, pViewProjection, 0, sizeof(m4));
    }
}

// todo: use default scissors
void
std_renderer_set_ui_viewport(StdViewport viewport)
{
    // DOCS: Check/Add viewport to all pipes.
    i64 viewportIndex = sva_index_of(gStd->Ui.aViewports, std_viewport_equal(item, viewport));
    if (viewportIndex == -1)
    {
	viewportIndex = sva_count(gStd->Ui.aViewports);

	f32 height = gStd->Ui.WindowSize.Height;
	viewport.Y0 = MinMax(height - viewport.Y0, 0, height);
	sva_add(gStd->Ui.aViewports, viewport);
    }

    gStd->Ui.CurrentViewportIndex = viewportIndex;
}

void
std_renderer_set_ui_fonts()
{
    SvlPipeline* pPipe = gStd->Ui.FontPipe.pHandle;
    StdFont* aFonts = std_asset_manager_get_all_fonts();

    SvlTexture* aTextures = NULL;
    sva_foreach(aFonts,
		sva_add(aTextures, item.Atlas));

    StdImageMap* aImageMap = NULL;
    VkDescriptorImageInfo* aImageInfos = NULL;

    i64 availableSlots = gStd->Ui.FontPipe.MaxFontsCount;
    sva_reserve(aImageInfos, availableSlots);

    i64 fontsCount = sva_count(aFonts);
    for (i64 i = 0; i < fontsCount; ++i)
    {
	if (i >= availableSlots)
	{
	    // todo: print warning
	    break;
	}

	StdFont font = aFonts[i];

	VkDescriptorImageInfo imageInfo = {
	    .sampler = font.Atlas.Sampler,
	    .imageView = font.Atlas.View,
	    .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
	};

	aImageInfos[i] = imageInfo;
    }

    // DOCS: Set all unused image infos to 0
    if (fontsCount < availableSlots)
    {
	for (i64 i = fontsCount; i < availableSlots; ++i)
	{
	    aImageInfos[i] = aImageInfos[0];
	}
    }

    VkWriteDescriptorSet writeDescrSet = {
	.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
	.pNext = NULL,
	.dstSet = pPipe->Binding.DescriptorSet,
	.dstBinding = 2,
	.dstArrayElement = 0,
	.descriptorCount = availableSlots,
	.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
	.pImageInfo = aImageInfos,
	.pBufferInfo = NULL,
	.pTexelBufferView = NULL
    };

    vkUpdateDescriptorSets(gStd->Instance.Device, 1, &writeDescrSet, 0, NULL);
}

/*
  #############################################
  DOCS: Rectangle pipeline
  #############################################
*/

void
std_renderer_draw_ui_rect(v3 position, v2 size, v4 color)
{
    v2 p0 = v2_new(position.X, position.Y);

    RectVertex vertices[] = {
	[0] = {
	    .Position = v3_new(p0.X, p0.Y + size.Y, position.Z),
	    .Color = color,
	},
	[1] = {
	    .Position = v3_new(p0.X, p0.Y, position.Z),
	    .Color = color,
	},
	[2] = {
	    .Position = v3_new(p0.X + size.X, p0.Y, position.Z),
	    .Color = color,
	},
	[3] = {
	    .Position = v3_new(p0.X + size.X, p0.Y + size.Y, position.Z),
	    .Color = color,
	},
    };

    add_draw_call(&gStd->Ui.RectPipe.aDrawCalls, sizeof(RectVertex), 4, 6, vertices);
}

void
std_renderer_draw_ui_empty_rect(v3 position, v2 size, real thickness, v4 color)
{
    // upper
    std_renderer_draw_ui_rect(position, v2_new(size.X, thickness), color);

    // left
    std_renderer_draw_ui_rect(position, v2_new(thickness, size.Y), color);

    // right
    v3 rpos = v3_new(position.X + size.X - thickness,
		     position.Y,
		     position.Z);
    std_renderer_draw_ui_rect(rpos, v2_new(thickness, size.Y), color);

    // down
    v3 dpos = v3_new(position.X,
		     position.Y + size.Y - thickness,
		     position.Z);
    std_renderer_draw_ui_rect(dpos, v2_new(size.X, thickness), color);
}

void
std_renderer_draw_ui_line(v3 start, v3 end, real thickness, v4 color)
{
    /*
      sx
      _
      \\
       \\
	\\
	 \\
	  \\
	   -
     */

    real sx = start.X;
    real sy = start.Y;
    real ex = end.X;
    real ey = end.Y;
    real z = start.Z;

    real dx = (f32_abs(sx-ey) < 1 ? thickness : 0);
    real dy = (f32_abs(sy-ey) < 1 ? thickness : 0);

    RectVertex vertices[] = {
	[0] = {.Position={ex, ey+thickness, z},.Color=color},
	[1] = {.Position={sx, sy+thickness, z},.Color=color},
	[2] = {.Position={sx+thickness, sy, z},.Color=color},
	[3] = {.Position={ex+thickness, ey, z},.Color=color},
    };

    add_draw_call(&gStd->Ui.RectPipe.aDrawCalls, sizeof(RectVertex), 4, 6, vertices);
}

void
std_renderer_draw_ui_image_ext(v3 position, v2 size, i64 assetId, v2 aUvs[4], v4 color)
{
    i64 ind = sva_index_of(gStd->Ui.ImagePipe.aAssetIdToGpuId, item.AssetId == assetId);
    if (ind == -1)
	return;

    StdImageMap image = gStd->Ui.ImagePipe.aAssetIdToGpuId[ind];
    i64 gpuId = image.GpuId;
    if (size.X <= 0 || size.Y <= 0)
    {
	real scale = -size.X;
	size = image.Size;
	size = v2_mulv(size, scale);
    }

    v3 p0 = v3_new(position.X, position.Y, position.Z);

    ImageVertex vertices[] = {
	[0] = (ImageVertex) {
	    .Position = v3_new(p0.X, p0.Y + size.Y, p0.Z),
	    .Color = color,
	    .Uv = aUvs[0],
	    .TextureIndex = gpuId,
	},
	[1] = (ImageVertex) {
	    .Position = v3_new(p0.X, p0.Y, p0.Z),
	    .Color = color,
	    .Uv = aUvs[1],
	    .TextureIndex = gpuId,
	},
	[2] = (ImageVertex) {
	    .Position = v3_new(p0.X + size.X, p0.Y, p0.Z),
	    .Color = color,
	    .Uv = aUvs[2],
	    .TextureIndex = gpuId,
	},
	[3] = (ImageVertex) {
	    .Position = v3_new(p0.X + size.X, p0.Y + size.Y, p0.Z),
	    .Color = color,
	    .Uv = aUvs[3],
	    .TextureIndex = gpuId,
	},
    };

    add_draw_call(&gStd->Ui.ImagePipe.aDrawCalls, sizeof(ImageVertex), 4, 6, vertices);
}

void
std_renderer_draw_ui_image(v3 position, v2 size, i64 assetId)
{
    v2 aUvs[] = {
	v2_new(0, 0),
	v2_new(0, 1),
	v2_new(1, 1),
	v2_new(1, 0),
    };
    std_renderer_draw_ui_image_ext(position, size, assetId, aUvs, v4_new(1,1,1,1));
}


void
std_renderer_draw_ui_text_ext(v3 pos, v4 color, i32* pText, i64 textLength, const char* pPath, i32 fontSize)
{
    StdFontUiPipeline* pPipe = &gStd->Ui.FontPipe;
    StdFont* aFonts = std_asset_manager_get_all_fonts();

    if (sva_count(aFonts) < 1)
    {
	// throw an error
	GERROR("No font is loaded into the system!\n");
	vassert_break();
	return;
    }

    // DOCS: If font is default, ignore everything but range
    const char* pSelectedPath;
    if (pPath != NULL)
    {
	pSelectedPath = pPath;
    }
    else
    {
	pSelectedPath = NULL;
    }

    i32 selectedFontSize;
    if (fontSize > 0)
    {
	selectedFontSize = fontSize;
    }
    else
    {
	selectedFontSize = 32;
    }

    // DOCS: Render-related
    f32 offsetX = pos.X,
	scale = 1.0f,
	firstCharHeight;

    StdFont* pFont = NULL;
    SafCharTable* hCharTable = NULL;
    f32 c_width_multiplier, c_height_multiplier;
    i32 rangeMin, rangeMax;
    i32 ind;

    // todo: load english font with 0..127 range as default
    // todo: place default font to resources
    i32 defaultFontIndex = sva_index_of(aFonts, item.Size == selectedFontSize && hash_geti(item.Handle.hCharTable, 32) != -1);
    vguard(defaultFontIndex != -1 && "Add space to font!");
    StdFont* pDefaultFont = &aFonts[defaultFontIndex];
    i32 defaultRangeMin = pDefaultFont->Handle.RangeMin;
    //SafChar defaultSafChar = hash_get(pDefaultFont->Handle.hCharTable, defaultRangeMin+1);
    v2 spaceMetrics = saf_font_get_space_metrics(pDefaultFont->Handle);

    for (i32 i = 0; i < textLength; ++i)
    {
	i32 charCode = (i32) pText[i];

	i32 needsNewFont = (charCode != 32) && (pFont == NULL || (charCode < rangeMin || charCode > rangeMax));
	if (needsNewFont)
	{
	    ind = sva_index_of(
		aFonts,
		charCode >= item.Handle.RangeMin &&
		charCode <= item.Handle.RangeMax &&
		(pSelectedPath == NULL || string_compare(pSelectedPath, item.pPath)) &&
		(selectedFontSize == item.Handle.FontSize)
		);

	    if (ind == -1)
		continue;

	    pFont = &aFonts[ind];
	    hCharTable = pFont->Handle.hCharTable;
	    c_width_multiplier = 1.0f / pFont->Handle.Size.Width;
	    c_height_multiplier = 1.0f / pFont->Handle.Size.Height;
	    rangeMin = pFont->Handle.RangeMin;
	    rangeMax = pFont->Handle.RangeMax;
	}

	SafChar safChar;
	if (hash_geti(hCharTable, charCode) != -1)
	{
	    safChar = hash_get(hCharTable, charCode);
	}
	else if (charCode == ' ' /* 32 */)
	{
	    // todo: think about it
	    // note: it's placeholder, we need
	    offsetX += spaceMetrics.X;
	    continue;
	}
	else
	{
	    DO_ONES(GERROR("Unknown font character %lc %d!\n", charCode, charCode));
	    continue;
	}

	// DOCS: Для зарегистрированных символов

	i32 roundX = Temp_Floor(offsetX + safChar.Xoff + 0.5f);
	i32 roundY = Temp_Floor(pos.Y - safChar.Yoff /* - 0.5f */);

	f32 cw = safChar.X1 - safChar.X0;
	f32 ch = safChar.Y1 - safChar.Y0;

	f32 x0 = roundX;
	f32 x1 = x0 + cw;
	f32 y1 = roundY;
	f32 y0 = roundY - ch;

	offsetX += safChar.Xadvance;

	v3 p0 = /* 0 0 */ v3_new(x0, y0, pos.Z);
	v3 p1 = /* 0 1 */ v3_new(x0, y1, pos.Z);
	v3 p2 = /* 1 1 */ v3_new(x1, y1, pos.Z);
	v3 p3 = /* 1 0 */ v3_new(x1, y0, pos.Z);

	// DOCS: Uvs
	f32 startX = safChar.X0 * c_width_multiplier,
	    startY = safChar.Y1 * c_width_multiplier,
	    endX   = safChar.X1 * c_height_multiplier,
	    endY   = safChar.Y0 * c_height_multiplier;

	v2 uv0 = v2_new(startX, startY);
	v2 uv1 = v2_new(startX, endY);
	v2 uv2 = v2_new(endX, endY);
	v2 uv3 = v2_new(endX, startY);

	FontVertex aVertices[4] = {
	    [0] = {
		.Position = p0,
		.Color = color,
		.Uv = uv0,
		.FontIndex = ind,
	    },
	    [1] = {
		.Position = p1,
		.Color = color,
		.Uv = uv1,
		.FontIndex = ind,
	    },
	    [2] = {
		.Position = p2,
		.Color = color,
		.Uv = uv2,
		.FontIndex = ind,
	    },
	    [3] = {
		.Position = p3,
		.Color = color,
		.Uv = uv3,
		.FontIndex = ind,
	    },
	};

	add_draw_call(&gStd->Ui.FontPipe.aDrawCalls, sizeof(FontVertex), 4, 6, aVertices);

    }
}

void
std_renderer_draw_ui_text_w_fontsize(v3 position, v4 color, char* pText, i32 fontSize)
{
    i64 textLength = simple_core_utf8_length(pText);

    // DOCS: If text < 1K, we are not allocating memory
#define StaticSize 1024

    if (textLength < StaticSize)
    {
	i32 aStatic[StaticSize] = {};
	simple_core_utf8_get_unicode_text(aStatic, pText, NULL);
	std_renderer_draw_ui_text_ext(position, color, aStatic, textLength, NULL, fontSize);
    }
    else
    {
	i32* aUnicodes = (i32*) memory_allocate(textLength * sizeof(i32));
	simple_core_utf8_get_unicode_text(aUnicodes, pText, NULL);
	std_renderer_draw_ui_text_ext(position, color, aUnicodes, textLength, NULL, fontSize);
	memory_free(aUnicodes);
    }

#undef StaticSize
}

void
std_renderer_draw_ui_text(v3 position, v4 color, char* pText)
{
    std_renderer_draw_ui_text_w_fontsize(position, color, pText, 0);
}

void
std_draw_calls_debug(StdDrawCall* aDrawCalls)
{
    // note: debug info
    for (i32 i = 0; i < sva_count(aDrawCalls); ++i)
    {
	StdDrawCall drawCall = aDrawCalls[i];
	GINFO("\nDrawCall: %d\n", i);
	printf("\tViewportIndex: %d\n\tStride: %d\n\tIndicesCoutn: %lld\n", drawCall.ViewportIndex, drawCall.Stride, drawCall.IndicesCount);

	/* RectVertex* aVertices = (RectVertex*)drawCall.pVertices; */
	/* printf("\tVertices (%lld): \n", sva_count(aVertices)); */
	/* sva_foreach(aVertices, */
	/*	    printf("\t[%lld] %0.2f %0.2f %0.2f\n", iter, item.Position.X, item.Position.Y, item.Position.Z); */
	/*     ); */
	printf("\n");
    }
}

void
process_ui_pipe(SvlInstance* pInstance, SvlPipeline* pPipe, StdDrawCall* aDrawCalls)
{
    if (sva_count(aDrawCalls) <= 0)
	return;

    VkCommandBuffer cmd = pInstance->Cmd.Buffer;

#define DEBUG_DRAW_CALL 1
#if DEBUG_DRAW_CALL == 1
    std_draw_calls_debug(aDrawCalls);
#endif // DEBUG_DRAW_CALL == 1

    vkCmdBindPipeline(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, pPipe->Handle);
    vkCmdBindDescriptorSets(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, pPipe->Layout, 0, 1, &pPipe->Binding.DescriptorSet, 0, NULL);

    // DOCS: it can be here until we use different shape
    vkCmdBindIndexBuffer(cmd, pPipe->Index.Gpu, 0, VK_INDEX_TYPE_UINT32);

    VkViewport viewport = {
	.x = 0,
	.y = 0,
	.width = pInstance->Resolution.width,
	.height = pInstance->Resolution.height,
	.minDepth = 0,
	.maxDepth = 1
    };
    vkCmdSetViewport(cmd, 0, 1, &viewport);

    u64 offset = 0;
    i64 drawCallsCount = sva_count(aDrawCalls);
    //slog_warn("DRaw calls count %ld\n", drawCallsCount);
    for (i32 d = 0; d < drawCallsCount; ++d)
    {
	StdDrawCall* pDrawCall = &aDrawCalls[d];
	if (sva_count(pDrawCall->pVertices) <= 0)
	{
	    //GWARNING("Skip\n");
	    continue;
	}

	//slog_warn("DC  %ld\n", drawCallsCount);

	u64 size = pDrawCall->Stride * sva_count(pDrawCall->pVertices);
	svl_buffer_set_data(pInstance, &pPipe->Vertex, pDrawCall->pVertices, offset, size);
	VkDeviceSize offsets[1] = { offset };
	vkCmdBindVertexBuffers(cmd, 0, 1, &pPipe->Vertex.Gpu, offsets);
	offset += size;

	StdViewport view = gStd->Ui.aViewports[pDrawCall->ViewportIndex];
	VkRect2D scissor = {
	    .offset = { view.X0, view.Y0 },
	    .extent = (VkExtent2D) {
		.width = view.Width,
		.height = view.Height
	    }
	};
	vkCmdSetScissor(cmd, 0, 1, &scissor);

	vkCmdDrawIndexed(pInstance->Cmd.Buffer, pDrawCall->IndicesCount, 1, 0, 0, 0);

	sva_clear(pDrawCall->pVertices);
	pDrawCall->IndicesCount = 0;
    }

}

void
svl_draw_render_pass(SvlInstance* pInstance, SvlRenderPass* pRenderPass)
{
    svl_frame_start(pInstance, &pRenderPass->Frame);

    VkCommandBuffer cmd = pInstance->Cmd.Buffer;
    VkExtent2D vkExtent = pInstance->Resolution;

    /*
      DOCS(typedef): Recording Commands BEGIN
    */

    VkCommandBufferBeginInfo commandBufferBeginInfo = {
	.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
	.pNext = NULL,
	.flags = 0,
	.pInheritanceInfo = NULL,
    };

    VkResult beginCmdResult = vkBeginCommandBuffer(cmd, &commandBufferBeginInfo);
    // todo: log result beginCmdResult on debug only

    VkClearValue aClearValues[2] = {
	[0] = (VkClearValue) {
	    .color = pRenderPass->ClearColor,
	},
	[1] = (VkClearValue) {
	    .depthStencil = { 1.0f, 0.0f }
	}
    };

    VkFramebuffer currentFramebuffer = pRenderPass->aFramebuffers[pRenderPass->Frame.ImageIndex];

    VkRenderPassBeginInfo renderPassBeginInfo = {
	.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
	.pNext = NULL,
	.renderPass = pRenderPass->Handle,
	.framebuffer = currentFramebuffer,
	.renderArea = {
	    .offset = { 0, 0 },
	    .extent = vkExtent
	},
	.clearValueCount = 2,
	.pClearValues = aClearValues
    };
    vkCmdBeginRenderPass(cmd, &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

    // DOCS: Base pipes
    for (svl_i32 p = 0; p < pRenderPass->PipelinesCount; ++p)
    {
	SvlPipeline* pPipe = pRenderPass->apPipelines[p];
	if (pPipe->VerticesCount <= 0 || pPipe->IndicesCount <= 0 || pPipe->IsActive == 0)
	    continue;

	//DOCS PIPELINE
	//TODO(typedef): add for (view in viewports)
	VkViewport viewport = {
	    .x = 0,
	    .y = 0,
	    .width = vkExtent.width,
	    .height = vkExtent.height,
	    .minDepth = 0,
	    .maxDepth = 1
	};
	vkCmdSetViewport(cmd, 0, 1, &viewport);

	//TODO(typedef): set in pipeline
	VkRect2D scissor = {
	    .offset = { 0, 0 },
	    .extent = vkExtent
	};
	vkCmdSetScissor(cmd, 0, 1, &scissor);

	vkCmdBindPipeline(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, pPipe->Handle);
	vkCmdBindDescriptorSets(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, pPipe->Layout, 0, 1, &pPipe->Binding.DescriptorSet, 0, NULL);

	VkDeviceSize offsets[1] = { 0 };
	vkCmdBindVertexBuffers(cmd, 0, 1, &pPipe->Vertex.Gpu, offsets);
	vkCmdBindIndexBuffer(cmd, pPipe->Index.Gpu, 0, VK_INDEX_TYPE_UINT32);

	vkCmdDrawIndexed(cmd, pPipe->IndicesCount, 1, 0, 0, 0);
    }

    { // DOCS: Ui pipes
	// DOCS: Rect pipeline
	process_ui_pipe(&gStd->Instance, gStd->Ui.RectPipe.pHandle, gStd->Ui.RectPipe.aDrawCalls);
	// DOCS: Image pipeline
	process_ui_pipe(&gStd->Instance, gStd->Ui.ImagePipe.pHandle, gStd->Ui.ImagePipe.aDrawCalls);
	// DOCS: Font pipeline
	process_ui_pipe(&gStd->Instance, gStd->Ui.FontPipe.pHandle, gStd->Ui.FontPipe.aDrawCalls);

    }

    vkCmdEndRenderPass(cmd);
    vkEndCommandBuffer(cmd);

    svl_frame_end(pInstance, &pRenderPass->Frame);

    { // DOCS: Clear frame data

	gStd->RectPipe.pHandle->VerticesCount = 0;
	gStd->RectPipe.pHandle->IndicesCount = 0;
	gStd->ImagePipe.pHandle->VerticesCount = 0;
	gStd->ImagePipe.pHandle->IndicesCount = 0;

	gStd->Ui.RectPipe.pHandle->VerticesCount = 0;
	gStd->Ui.RectPipe.pHandle->IndicesCount = 0;
	gStd->Ui.ImagePipe.pHandle->VerticesCount = 0;
	gStd->Ui.ImagePipe.pHandle->IndicesCount = 0;
	gStd->Ui.FontPipe.pHandle->VerticesCount = 0;
	gStd->Ui.FontPipe.pHandle->IndicesCount = 0;

	sva_clear(gStd->RectPipe.aVertices);
	sva_clear(gStd->ImagePipe.aVertices);

	sva_clear(gStd->Ui.RectPipe.aDrawCalls);
	sva_clear(gStd->Ui.ImagePipe.aDrawCalls);
	sva_clear(gStd->Ui.FontPipe.aDrawCalls);

	sva_clear(gStd->Ui.aViewports);

	gStd->Ui.CurrentViewportIndex = 0;
    }
}

/*
  #############################################
  DOCS: HELPERS (MB PUT INTO SVL LIBRARY)
  #############################################
*/

void
svl_write_default_index_buffer(SvlInstance* pInstance, SvlPipeline* pPipe, u64 vertexSize, i64 maxInstanceCount)
{
    u64 ibufCount = maxInstanceCount * 6;
    u64 vbufSize = (maxInstanceCount * 4) * vertexSize;
    u64 ibufSize = ibufCount * sizeof(u32);

    svl_pipeline_create_vertex_buffer(pInstance, pPipe, vbufSize);
    svl_pipeline_create_index_buffer(pInstance, pPipe, ibufSize);

    // docs: only for 2d logic
    u32* aIndices = NULL;
    {
	sva_reserve(aIndices, ibufCount);
	sva_header(aIndices)->Count = ibufCount;
	for (i32 i = 0, ind = 0; i < ibufCount; i += 6, ind += 4)
	{
	    aIndices[i    ] = ind + 0; //0,1,2,2,3,0
	    aIndices[i + 1] = ind + 1;
	    aIndices[i + 2] = ind + 2;
	    aIndices[i + 3] = ind + 2;
	    aIndices[i + 4] = ind + 3;
	    aIndices[i + 5] = ind + 0;
	}
    }

    svl_buffer_set_data(pInstance, &pPipe->Index, aIndices, 0, ibufSize);

    sva_free(aIndices);
}
