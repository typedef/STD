#include "StdTypes.h"

#include <Core/SimpleStandardLibrary.h>
#include <Core/SimpleAtlasFont.h>


v2
saf_font_get_metrics_generic(StdFont* aFonts, char* pText, i32 fontSize)
{
    vassert_not_null(aFonts);
    vassert_not_null(pText);

    i64 textLength = simple_core_utf8_length(pText);
    i32* aUnicodeText = (i32*) memory_allocate(textLength * sizeof(i32));
    simple_core_utf8_get_unicode_text(aUnicodeText, pText, &textLength);
    vassert_not_null(aUnicodeText);

    f32 width  = 0.0f;
    f32 height = 0.0f;
    const i32 is_newline_char = (i32)'\n';
    const i32 is_whitespace_char = (i32)' ';

    SafFont* pFont = NULL;
    i32 rangeMin, rangeMax;

    for (i32 i = 0; i < textLength; ++i)
    {
	i32 wc = aUnicodeText[i];

	if (pFont == NULL || wc < rangeMin || wc > rangeMax)
	{
	    i32 ind = sva_index_of(
		aFonts,
		wc >= item.Handle.RangeMin &&
		wc <= item.Handle.RangeMax &&
		(fontSize == 0 || fontSize == item.Handle.FontSize));

	    if (ind == -1)
		continue;

	    pFont = &aFonts[ind].Handle;
	    rangeMin = pFont->RangeMin;
	    rangeMax = pFont->RangeMax;
	}

	SafChar safChar = hash_get(pFont->hCharTable, (i32)wc);
	i32 tableInd = table_index(pFont->hCharTable);

	if (wc == is_newline_char)
	{
	    //height += safChar.->MaxHeight + value.Offset.Y;
	}
	else if (wc != is_whitespace_char)
	{
	    if (tableInd == -1)
	    {
		wchar_t charBuf[2] = {
		    [0] = wc,
		    [1] = '\0'
		};
		GERROR("Can't find %d symbol in hash table!\n", (i32) wc);
		GERROR("i: %d ind: %d wci: %d char: %ls\n", i, tableInd, wc, charBuf);
		vguard(tableInd != -1 && "No char!");
	    }

	    width += safChar.Xadvance;

	    f32 cHeight = (safChar.Y1 - safChar.Y0);
	    height = Max(height, cHeight);
	}
	else
	{
	    v2 spaceMetrics = saf_font_get_space_metrics(*pFont);
	    width  += spaceMetrics.X;
	}
    }

    v2 metrics = {width, height};

    memory_free(aUnicodeText);

    return metrics;
}
