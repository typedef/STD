#include "StdUi.h"
#include "Core/Types.h"
#include <Core/SimpleApplication.h>
#include <Core/SimpleMath.h>
#include <Core/SimpleMathIO.h>
#include <Core/SimpleStandardLibrary.h>
#include <Core/SimpleAtlasFont.h>
#include <stdlib.h>


/*
  TODO:
  * aWidgets should be replaced by aCustoms
  WIP:
  * comment all aWidgets code

  NOTE:
  * никакой элемент не должен зависеть от размера панели (чтобы не создавать рекурсивный рост)

*/

// DOCS: Internal

static i32
IsKeyReleased(StdUiKey key)
{
    return std_ui_is_key_released(key);
}

static i32
IsKeyPressed(StdUiKey key)
{
    return std_ui_is_key_pressed(key);
}


/*     #####################################
  DOCS ##       Forward-Declaration       ##
       #####################################*/



StdUiPanel* std_ui_panel_get_by_id(i32 i);
StdUiDiv std_ui_panel_div_get(StdUiPanel* pPanel, StdUiId id);
i32 std_ui_panel_get_index_by_name(const char* pPanelName);


force_inline i64
std_ui_panel_get_id_by_name(const char* pPanelName)
{
    i64 id = simple_core_string_hash((char*) pPanelName, string_length(pPanelName));
    return id;
}

typedef struct StdUi
{
    i32 IsInitialized;
    StdUiDisplay Display;
    //SimpleFont* pFont;
    StdUiStyle Style;
    StdUiInput Input;
    StdUiCache Cache;
    StdUiInteractive Interactive;

    StdUiPiv NextPiv;

    // DOCS: FramesCounter
    i64 FrameCount;

    // DOCS: Sound
    StdUiId HoverSoundId;

    // todo: Delete
    StdUiCustomStorage CustomStorage;
    // note: Instead of CustomStorage
    StdUiWidgetStorage WidgetStorage;

    StdUiStyleCache StyleStorage;

    SimpleArena* pArena;
} StdUi;

struct GlobalItem
{
    StdUi Ui;

    // DOCS: Back-end related
    StdUiFuncs Backend;
    StdUiDebugConfig DebugConfig;

    struct {const char* Key; char* Value;}* hAllocatedStrings;
};

static struct GlobalItem g = {};


StdUiStyle
std_ui_style_new()
{
    StdUiStyle style = {};

    const v2 def_panel_size = v2_new(30, 30);

    v4 grayColor = v4_new(0.11, 0.11, 0.11, 1);
    v4 darkerGrayColor = v4_new(0.09, 0.09, 0.09, 1);
    v4 ultraDarkColor = v4_new(0.01f, 0.01f, 0.01f, 1);
    v4 assDarkColor = v4_new(0.001f, 0.001f, 0.001f, 1);

    StdUiStyleItem panel = {
	.Color = {
	    .Background = ultraDarkColor,
	    .Hovered = ultraDarkColor,
	    .Clicked = ultraDarkColor,
	},
	.Size = {
	    .Min = def_panel_size,
	    // NOTE: temporary
	    .Max = v2_new(1000, 1000),
	    .Hovered = def_panel_size,
	    .Clicked = def_panel_size,
	}
    };

    style.Items[StdUiStyleItemType_Panel] = panel;

    {
	v4 background = v4_new(20.0/255, 21.0/255, 26.0/255, 1.0f);
	v4 hovered = v4_new(40.0/255, 42.0/255, 53.0/255, 1.0f);
	v4 clicked = hovered;

	StdUiStyleItem widgetButton = {
	    .Color = {
		.Background = background,
		.Hovered = hovered,
		.Clicked = clicked,
	    },
	    .Margin = v2_new(15, 15),
	};

	style.Items[StdUiStyleItemType_Widget_Button] = widgetButton;

    }


    return style;
}


/*     #####################################
  DOCS ##        Std Ui Layout NEW        ##
       #####################################*/

void
std_ui_style_add(char* pStyleName, StdUiShapeStyle shapeStyle)
{
    i32 ind = shash_geti(g.Ui.StyleStorage.hWidgetStyle, pStyleName);
    if (ind == -1)
    {
	shapeStyle.pName = pStyleName;
	shash_put(g.Ui.StyleStorage.hWidgetStyle, pStyleName, shapeStyle);
    }
}

StdUiShapeStyle
std_ui_style_get(char* pStyleName)
{
    i32 ind = shash_geti(g.Ui.StyleStorage.hWidgetStyle, pStyleName);
    if (ind != -1)
    {
	return g.Ui.StyleStorage.hWidgetStyle[ind].Value;
    }

    slog_error("No style registered with %s name\n", pStyleName);
    vguard(0);
    return (StdUiShapeStyle) {};
}

void
std_ui_piv(StdUiPiv piv)
{
    g.Ui.NextPiv = piv;
}

/*     #####################################
  DOCS ##      END Std Ui Layout NEW      ##
       #####################################*/


StdUiInput
std_ui_input_new()
{
    StdUiInput input = {};
    return input;
}

void
std_ui_input_clear(StdUiInput* pInput)
{
    pInput->MousePositionChanged = 0;
    pInput->LeftKeyWasPressed = 0;
    pInput->IsInputAlreadyHandled = 0;
    //pInput->Mode = SuiModeType_Insert;
    pInput->CharTyped = 0;

    pInput->ScrollY = 0;

    memset(pInput->aKeys, 0, StdUiKey_Count * sizeof(StdUiKeyData));
}

/*     #####################################
  DOCS ##          Std Ui Base            ##
       #####################################*/

void
std_ui_create(StdUiSettings set)
{
    g.Ui.IsInitialized = 1;

    g.Ui.Display = (StdUiDisplay) {
	.Size = set.DisplaySize
    };

    g.Ui.Style = std_ui_style_new();
    g.Ui.Input = std_ui_input_new();
    g.Ui.FrameCount = 0;

    // note: make it bigger if necessary
    g.Ui.pArena = simple_arena_create(MB(5));
    memory_set_arena(g.Ui.pArena);
    memory_unbind_current_arena();
}

void
std_ui_destroy()
{
}

void
std_ui_frame_begin()
{
    // DOCS: clear layout divs
    i64 panelsCount = sva_count(g.Ui.Cache.Panels);
    for (i64 i = 0; i < panelsCount; ++i)
    {
	StdUiPanel* pPanel = std_ui_panel_get_by_id(i);

#if !1
	i32 divsCnt = sva_count(pPanel->aDivs);
	GINFO("PanelId %lld Divcnt: %d\n", pPanel->Id, divsCnt);
	for (i32 d = 0; d < divsCnt; ++d)
	{
	    StdUiDiv div = pPanel->aDivs[d];
	    printf("div->Width, Height (%f %f)\n", div.Size.X, div.Size.Y);
	}
#endif

	//slog_error("Clean divs %d\n", sva_count(pPanel->aDivs));
	sva_clear(pPanel->aDivs);

	pPanel->CurrentDiv = 0;
    }

}

i32
std_ui_is_key_released(StdUiKey key)
{
    StdUiKeyData keyData = g.Ui.Input.aKeys[key];
    i32 isReleased = (keyData.IsHandled != 1)
	&& (keyData.State == StdUiKeyState_Released);
    return isReleased;
}

i32
std_ui_is_key_pressed(StdUiKey key)
{
    StdUiKeyData keyData = g.Ui.Input.aKeys[key];
    i32 isPressed = (keyData.IsHandled != 1)
	&& (keyData.State == StdUiKeyState_Pressed);
    return isPressed;
}

void
std_ui_frame_end()
{
    ++g.Ui.FrameCount;
    sva_clear(g.Ui.Cache.Panels);
}

StdUiStyle*
std_ui_get_style()
{
    return &g.Ui.Style;
}

StdUiInput*
std_ui_get_input()
{
    return &g.Ui.Input;
}

StdUiDisplay*
std_ui_get_display()
{
    return &g.Ui.Display;
}


/*     #####################################
  DOCS ##          Std Ui Debug           ##
       #####################################*/

static StdUiDebug gDebug;

void
std_ui_debug(StdUiDebug* pDebug)
{
    gDebug = *pDebug;
}

void
_validate_style_for_custom_geometries()
{
    i32 flag = helper_fofr();
    if (flag)
	return;

    i64 panelsCount = sva_count(g.Ui.Cache.Panels);
    for (i32 p = 0; p < panelsCount; ++p)
    {
	StdUiPanel* pPanel = std_ui_panel_get_by_id(p);

	i64 customsCount = sva_count(pPanel->CustomCache.aCustoms);
	for (i32 c = 0; c < customsCount; ++c)
	{
	    StdUiCustom custom = pPanel->CustomCache.aCustoms[c];
	    for (i32 cg = 0; cg < sva_count(custom.aGeometries); ++cg)
	    {
		StdUiGeometry geom = custom.aGeometries[cg];
		i32 ind = shash_geti(g.Ui.CustomStorage.hRegisteredStyles, geom.pName);
		if (ind == -1 && geom.GeometryType != StdUiGeometryType_Text)
		{
		    GWARNING("Style is not registered for %s!\n", geom.pName);
		}

	    }
	}
    }

}

void
std_ui_debug_validation()
{
    _validate_style_for_custom_geometries();
}

/*     #####################################
  DOCS ##        Std Ui Animation         ##
       #####################################*/

// todo: refactor to widget
void
std_ui_reset_animate_rectangle(char* pName)
{
    StdUiPanel* pPanel = std_ui_panel_get();
    i64 id = 0;//std_ui_custom_get_id(pName);

    i32 ind = sva_index_of(pPanel->AnimationCache.aAnimationRects, item.Id == id);
    if (ind == -1)
    {
	return;
    }

    StdUiAnimationRect* pRect = &pPanel->AnimationCache.aAnimationRects[ind];
    pRect->Percent = 0;
    simple_timer_reset(&pRect->Timer);
}

void
std_ui_animate_rectangle(StdUiAnimationRect rect, v2* pSize, v2* pPosition)
{
    StdUiPanel* pPanel = std_ui_panel_get();

    // IsWidget;
    i32 ind = sva_index_of(pPanel->AnimationCache.aAnimationRects, item.Id == rect.Id);
    if (ind == -1)
    {
	ind = sva_count(pPanel->AnimationCache.aAnimationRects);
	sva_add(pPanel->AnimationCache.aAnimationRects, rect);
    }

    StdUiAnimationRect* pRect = &pPanel->AnimationCache.aAnimationRects[ind];

    real timestep = simple_application_get_stats().Timestep;

    if (simple_timer_interval(&pRect->Timer, timestep) && pRect->Percent <= 1)
    {
	pRect->Percent += 0.01;
    }

    v2 current = v2_lerpx(pRect->Start, pRect->End, pRect->Percent);
    pRect->AnimationSize = current;

    if (rect.Type == StdUiAnimationRectType_CenterToEdge)
    {
	real finalX = pRect->Start.X  - current.X/2 + pRect->End.X/2;
	pRect->AnimationPos = v2_new(finalX, 0);
    }

    *pSize     = pRect->AnimationSize;
    *pPosition = pRect->AnimationPos;
}


/*     #####################################
  DOCS ##         Std Interactive         ##
       #####################################*/

i32
std_ui_is_hovered(v3 pos, v2 size)
{
    v2 mousePos = g.Ui.Input.MousePosition;
    if (mousePos.X >= pos.X
	&& mousePos.X <= (pos.X + size.Width)
	&& mousePos.Y <= pos.Y
	&& mousePos.Y >= (pos.Y - size.Height))
    {
	return 1;
    }

    return 0;
}

StdUiState
std_ui_is_pressed(v3 pos, v2 size, StdUiId id)
{
    StdUiInteractive* pInteractive = &g.Ui.Interactive;

    if (pInteractive->ActiveId == 0 &&
	pInteractive->HotId == 0
	&& std_ui_is_hovered(pos, size))
    {
	pInteractive->HotId = id;

	if (IsKeyPressed(StdUiKey_Mouse_Left_Button))
	{
	    pInteractive->ActiveId = id;
	    pInteractive->IsLeftKeyWasPressed = 1;

	    return StdUiState_Clicked;
	}

	return StdUiState_Hovered;
    }

    //slog_error("hot: %d active %d \n", pInteractive->HotId, pInteractive->ActiveId);

    return StdUiState_None;
}

// DOCS: It's grab mech for panel
// DO NOT REMOVE IT FROM CODE BASE
void
std_ui_click_behavior(v3 pos, v2 size, StdUiId id, StdUiState* pState)
{
    StdUiState state = *pState;
    StdUiInput* pInput = &g.Ui.Input;

    //TODO reorder if and elif
    // DOCS(typedef): Some State Logic
    if (state != StdUiState_Grabbed
	&& state != StdUiState_Clicked)
    {
	*pState = std_ui_is_pressed(pos, size, id);
    }
    else if (state == StdUiState_Clicked)
    {
	*pState = StdUiState_Grabbed;
	pInput->IsInGrabState = 1;
	// todo: fo why we need this one
	pInput->GrabPosition = v2_v3(pos);
	pInput->GrabMousePosition = pInput->MousePosition;
    }

    // note: make button flick
    /* if (IsKeyReleased(StdUiKey_Mouse_Left_Button)) */
    /* { */
    /*	*pState = StdUiState_None; */
    /*	pInput->IsInGrabState = 0; */
    /* } */
}

/*     #####################################
  DOCS ##          Std Ui Panel           ##
       #####################################*/

void
std_ui_set_current_panel(i64 ind)
{
    // TODO: Validate id
    vassert(ind != -1);

    g.Ui.Cache.CurrentPanelInd = ind;
}

typedef struct CalcPositionSettings
{
    StdUiPanel* pPanel;
    StdUiCustom* pCustom;
    StdUiCustom* pPrevCustom;
    StdUiDiv Div;
    StdUiDiv PrevDiv;
    i32 Index;
} CalcPositionSettings;

static void
_display_new_line(CalcPositionSettings set)
{
    if (set.Index < 0)
	return;

    StdUiDiv div = set.Div;
    StdUiDiv prevDiv = set.PrevDiv;
    StdUiCustom* pCustom = set.pCustom;
    StdUiCustom* pPrevCustom = set.pPrevCustom;
    StdUiPanel* pPanel = set.pPanel;

    // docs: first in a div or generally a first one
    if (pPrevCustom == NULL || prevDiv.Id != div.Id)
    {
	pCustom->Metrics.Position = div.Position;

	pCustom->Metrics.Position.X += div.Padding.X;
	pCustom->Metrics.Position.Z -= 0.1f;
    }
    else
    {
	// docs: isnt first in a div
	if (prevDiv.Id == div.Id)
	{
	    pCustom->Metrics.Position = pPrevCustom->Metrics.Position;
	    pCustom->Metrics.Position.Y -= (pPrevCustom->Metrics.Size.Height + div.Padding.Y);
	}
    }

}

void
std_ui_panel_calc_layout(StdUiPanel* pPanel)
{
    v3 nextPos = v3_addz(pPanel->Position, -0.1);
    i64 widgetsCount = sva_count(pPanel->WidgetCache.aWidgets);
    i32 divsCount = sva_count(pPanel->aDivs);

    // slog_warn("divs.cnt: %d\n", divsCount);

    f32 panelZ = pPanel->Position.Z; //slog_warn("panelZ: %f\n", panelZ);

    for (i32 d = 0; d < divsCount; ++d)
    {
	StdUiDiv div = pPanel->aDivs[d];
	v2 offset = {};

	for (i32 i = 0; i < widgetsCount; ++i)
	{
	    StdUiWidget* pWidget = &pPanel->WidgetCache.aWidgets[i];
	    StdUiDiv widgetDiv = std_ui_div_get(pWidget->LayoutId);

	    if (widgetDiv.Id != div.Id)
		continue;

	    StdUiWidgetType* pType = std_ui_get_widget_type_by_id(pWidget->TypeId);

	    f32 xPadding = widgetDiv.Padding.X * widgetDiv.Absolute.Size.Width;
	    f32 yPadding = widgetDiv.Padding.Y * widgetDiv.Absolute.Size.Height;

	    f32 widgetZ = widgetDiv.Absolute.Position.Z - 0.1;

	    v3 offsetPos = v3_new(
		widgetDiv.Absolute.Position.X + offset.X,
		widgetDiv.Absolute.Position.Y - offset.Y - yPadding,
		widgetZ
		);

	    // slog_warn("Pos: %v3\n", offsetPos);

	    pWidget->Position = offsetPos;

	    switch (widgetDiv.Mode)
	    {
	    case StdUiMode_NewLine:
	    {
		offset.Y += pWidget->Size.Height + yPadding;
		break;
	    }

	    case StdUiMode_SameLine:
	    {
		offset.X += pWidget->Size.Width + xPadding;
		break;
	    }
	    }

	}

    }


}

StdUiPanel*
std_ui_panel_get()
{
    i32 ind = g.Ui.Cache.CurrentPanelInd;
    if (ind == -1)
	return NULL;

    StdUiPanel* pSuiPanel = &g.Ui.Cache.hPanelsTable[ind].Value;
    vguard_not_null(pSuiPanel);

    return pSuiPanel;
}


StdUiPanel*
std_ui_panel_get_by_id(i32 i)
{
    i64 ind = g.Ui.Cache.Panels[i];
    StdUiPanel* pPanel = &g.Ui.Cache.hPanelsTable[ind].Value;
    return pPanel;
}

void
std_ui_panel_apply_layout_new(StdUiPanel* pCurrentPanel)
{
    StdUiPiv piv = g.Ui.NextPiv;//slog_warn("%s piv.pos %v3\n", pCurrentPanel->pLabel, piv.Position);
    v2 displaySize = g.Ui.Display.Size;

    f32 z;
    if (piv.Position.Z < 0)
    {
	z = -piv.Position.Z + 500;
    }
    else if (piv.Position.Z == 0)
    {
	z = 1 + sva_count(g.Ui.Cache.Panels);
    }
    else
    {
	z = piv.Position.Z;
    }

    v2 panelSize = v2_mul(piv.Size, displaySize);
    v3 panelPos = v3_new(
	displaySize.Width * piv.Position.X,
	displaySize.Height - displaySize.Height * piv.Position.Y,
	z);

    pCurrentPanel->Size = panelSize;
    pCurrentPanel->Position = panelPos;
}

StdUiDiv
std_ui_panel_div_get(StdUiPanel* pPanel, StdUiId id)
{
#if defined(BUILD_DEBUG)
    i64 divsCnt = sva_count(pPanel->aDivs);
    if (id < 0 || id >= divsCnt)
    {
	GERROR("Id out of range %d\n", id);
	vguard(0);
    }
#endif

    return pPanel->aDivs[id];
}

i32
std_ui_panel_get_index_by_name(const char* pLabel)
{
    i32 ind = shash_geti(g.Ui.Cache.hPanelsTable, pLabel);
    return ind;
}

i32
std_ui_panel_begin(StdUiPanelSettings set)
{
    // TODO: validate set

    i32 panelIndex = std_ui_panel_get_index_by_name(set.pUtfLabel);

    StdUiPanel* pCurrentPanel = NULL;
    if (panelIndex == -1)
    {
	if (table_count(g.Ui.Cache.hPanelsTable) >= 2)
	{
	    //i64 ind = std_ui_panel_get_index_by_name(set.pUtfLabel);
	    //slog_error("table contains more then 2 panels (%d)\n %d %d\n", table_count(g.Ui.Cache.hPanelsTable), panelIndex, ind);
	}

	// DOCS: Add new panel
	char* pLabel = simple_string_new(set.pUtfLabel);
	StdUiPanel stdUiPanel = {
	    .pLabel = pLabel,
	    .PanelState = StdUiPanelState_Opened,
	    .Flags = set.Flags,
	};
	shash_put(g.Ui.Cache.hPanelsTable, pLabel, stdUiPanel);
	panelIndex = table_index(g.Ui.Cache.hPanelsTable);

	slog_info("New panel { %s }\n", g.Ui.Cache.hPanelsTable[panelIndex].Value.pLabel);
    }

    std_ui_set_current_panel(panelIndex);
    pCurrentPanel = std_ui_panel_get();

    if (pCurrentPanel->PanelState == StdUiPanelState_Closed)
	return 0;

    sva_add(g.Ui.Cache.Panels, panelIndex);

    // DOCS: Apply layout
    std_ui_panel_apply_layout_new(pCurrentPanel);
    //std_ui_panel_apply_layout(pCurrentPanel);

    { // DOCS: Set default style div
	StdUiDiv defaultDiv = {
	    //v2 Margin;
	    /*
	      DOCS: Applied for first element in the div:
	      . NewLine  -> Padding.X
	      . SameLine -> Padding.Y
	    */
	    //v2 Padding;

	    .Position = v3_new(0.01, 0.01, -0.2),
	    .Size = v2_new(0.8, 0.8),
	    .Padding = v2_new(0, 0),
	    .WidgetSize = v2_new(0.8, 0.25),
	    .Mode = StdUiMode_NewLine,
	    .FontSize = 32,
	};

	// todo: uncomment
	std_ui_div(defaultDiv);
    }

    // DOCS: Disable all customs
    sva_foreach_ptr(pCurrentPanel->WidgetCache.aWidgets, item->Flags = StdUiWidgetFlags_Dead);
    //for (i32 i = 0; i < sva_count(pCurrentPanel->CustomCache.aCustoms); ++i)
    //pCurrentPanel->CustomCache.aCustoms[i].Flags.IsInUse = 0;

    return 1;
}

void
std_ui_panel_end()
{
    StdUiPanel* pCurrentPanel = std_ui_panel_get();
    vguard_not_null(pCurrentPanel);

    if (pCurrentPanel == NULL || pCurrentPanel->PanelState == StdUiPanelState_Closed)
    {
	goto StdUiPanelEndLabel;
    }

    // DOCS: Layout for widgets
    std_ui_panel_calc_layout(pCurrentPanel);

    // todo: is blocked by other
    // todo: If widget is (hovered || clicked) ignore panel (hovered || clicked) mech

StdUiPanelEndLabel:
    g.Ui.Cache.CurrentPanelInd = -1;
}


/*     #####################################
  DOCS ##          Std Ui Layout         ##
       #####################################*/

i64
std_ui_div(StdUiDiv div)
{
    //static i32 divCallsCount = 0; slog_warn("divs call count: %d\n", ++divCallsCount);

    const i32 c_font_size = 32;

    StdUiPanel* pCurrentPanel = std_ui_panel_get();
    v3 panelPos = pCurrentPanel->Position;
    v2 panelSize = pCurrentPanel->Size;
    i32 id = sva_count(pCurrentPanel->aDivs);

    div.Id = id;
    div.FontSize = div.FontSize != 0 ? div.FontSize : c_font_size;
    div.Absolute.Size = v2_mul(panelSize, div.Size);
    //if (helper_tofr())
    //slog_warn("Div size: %v2\n", div.Absolute.Size);
    div.Absolute.Position = v3_new(
	panelPos.X + panelSize.X * div.Position.X,
	panelPos.Y - panelSize.Y * div.Position.Y,
	panelPos.Z - div.Position.Z
	);
    sva_add(pCurrentPanel->aDivs, div); //slog_info("Cnt: %d\n", sva_count(pCurrentPanel->aDivs));

    pCurrentPanel->CurrentDiv = id;

    return id;
}

StdUiDiv
std_ui_div_get(StdUiId id)
{
    StdUiPanel* pPanel = std_ui_panel_get();

#if defined(BUILD_DEBUG)
    i64 divsCnt = sva_count(pPanel->aDivs);
    if (id < 0 || id >= divsCnt)
    {
	GERROR("Id out of range %d\n", id);
	vguard(0);
    }
#endif

    return pPanel->aDivs[id];
}


/*     #####################################
  DOCS ##         Std Ui Widget           ##
       #####################################*/

// DOCS: Internal

force_inline i64
_calc_id_from_name(const char* pName)
{
    i64 size = simple_core_utf8_size((char*)pName);
    i64 id   = simple_core_string_hash((char*)pName, size);
    return id;
}

StdUiWidget*
std_ui_panel_get_widget(StdUiPanel* pPanel, char* pLabel)
{
    i64 labelHash = _calc_id_from_name(pLabel);

    i32 i, count = sva_count(pPanel->WidgetCache.aWidgets);
    for (i = 0; i < count; ++i)
    {
	StdUiWidget* pWidget = &pPanel->WidgetCache.aWidgets[i];
#if 1
	if (pWidget->Id == labelHash)
#else
	if (string_compare(pWidget->pName, pLabel))
#endif
	{
	    return pWidget;
	}
    }

    return NULL;
}

StdUiWidgetType*
std_ui_get_widget_type_by_id(i64 typeId)
{
    i64 count = sva_count(g.Ui.WidgetStorage.aWidgetTypes);
    for (i32 i = 0; i < count; ++i)
    {
	StdUiWidgetType* pType = &g.Ui.WidgetStorage.aWidgetTypes[i];
	if (pType->Id == typeId)
	{
	    return pType;
	}
    }

    return NULL;
}

StdUiWidgetData
std_ui_widget_data(StdUiWidgetData data)
{
    memory_bind_arena(g.Ui.pArena);

    StdUiWidgetData aData = {};
    if (data.pText != NULL)
    {
	aData.pText = simple_string_new(data.pText);
    }

    memory_unbind_current_arena();

    return aData;
}

StdUiWidget*
std_ui_panel_add_widget(StdUiPanel* pPanel, StdUiWidgetConfig config)
{
    // DOCS: validate config fields
    StdUiWidgetType* pWidgetType =
	std_ui_get_widget_type_by_id(config.TypeId);
    if (pWidgetType == NULL || config.pName == NULL)
    {
	// DOCS: Validation failed
	slog_warn("Wtf, failed widget creation because typeId is not registered or config.pName is NULL!\n");
	return NULL;
    }

    i64 id = _calc_id_from_name(config.pName);

    // todo: add div
    //std_ui_get_text_metrics(config.pName, 32);

    StdUiWidget widget = {
	.Id = id,
	.TypeId = config.TypeId,
	.Flags = StdUiWidgetFlags_Alive,
	.pName = simple_string_new(config.pName),

	.AssetId = config.AssetId,
	.ItemId = config.ItemId,

	.State = StdUiState_None,
	.ShapeStateInd = -1,

	//.Data = std_ui_widget_data(config.Data)
    };

    i64 widgetInd = sva_count(pPanel->WidgetCache.aWidgets);
    sva_add(pPanel->WidgetCache.aWidgets, widget);
    return &pPanel->WidgetCache.aWidgets[widgetInd];
}


// DOCS: Public

StdUiWidgetType
std_ui_widget_type_new(StdUiWidgetTypeConfig config)
{
    StdUiShape* aShapes = NULL;
    for (i32 i = 0; i < config.ShapesCount; ++i)
    {
	StdUiShape shape = config.aShapes[i];
	// note/todo: should we allocate it??
	shape.pStyleName = std_ui_new_string(shape.pStyleName);
	sva_add(aShapes, shape);
    }

    v2 max = v2_new(-1000, -1000);
    v2 min = v2_new( 1000,  1000);
    for (i32 i = 0; i < config.ShapesCount; ++i)
    {
	StdUiShape shape = config.aShapes[i];
	v2 pos = shape.Position.XY;

	if (pos.X > max.X)
	    max.X = pos.X + shape.Size.X;
	if (pos.Y > max.Y)
	    max.Y = pos.Y;

	if (pos.X < min.X)
	    min.X = pos.X;
	if (pos.Y < min.Y)
	    min.Y = pos.Y - shape.Size.Y;
    }

    v2 size = v2_new(max.X - min.X, max.Y - min.Y);

    StdUiWidgetType type = {
	.Id = sva_count(g.Ui.WidgetStorage.aWidgetTypes),
	.aShapes = aShapes,
	.Metrics = {
	    .DiffSize = size,
	    .Min  = min,
	    .Max  = max,
	},
	.pName = simple_string_new(config.pName),
    };

    sva_add(g.Ui.WidgetStorage.aWidgetTypes, type);

    return type;
}

// note: on data changed, call it once again
void
std_ui_widget_calc_size(StdUiWidget* pWidget)
{
    StdUiPanel* pPanel = std_ui_panel_get();
    StdUiDiv div = std_ui_panel_div_get(pPanel, pWidget->LayoutId);
    v2 widgetSize = v2_mul(div.Absolute.Size, div.WidgetSize);
    pWidget->Size = widgetSize;

    //slog_warn("Size: %v2\n", widgetSize);
}

StdUiState
std_ui_widget(StdUiWidgetConfig config)
{
    StdUiPanel* pCurrentPanel = std_ui_panel_get();

    StdUiWidget* pWidget = std_ui_panel_get_widget(pCurrentPanel, (char*) config.pName);
    if (pWidget != NULL)
    {
	pWidget->Flags = StdUiWidgetFlags_Alive;
	pWidget->LayoutId = pCurrentPanel->CurrentDiv;
    }
    else
    {
	//slog_info("Add new widget!\n");
	pWidget = std_ui_panel_add_widget(pCurrentPanel, config);
	pWidget->LayoutId = pCurrentPanel->CurrentDiv;
	std_ui_widget_calc_size(pWidget);
    }

    pWidget->AssetId = config.AssetId;
    pWidget->ItemId = config.ItemId;
    // todo: make sure current div is updated correctly

    return pWidget->State;
}

void
std_ui_process_input()
{
    StdUiInteractive* pInteractive = &g.Ui.Interactive;
    pInteractive->HotId = 0;
    pInteractive->ActiveId = 0;
    pInteractive->HotPanelId = 0;

    i32 panelsCount = sva_count(g.Ui.Cache.Panels);
    i32 isInputAlreadyProcessed = 0;

    for (i32 p = 0; p < panelsCount; ++p)
    {
	StdUiPanel* pPanel = std_ui_panel_get_by_id(p);
	StdUiWidget* aWidgets = pPanel->WidgetCache.aWidgets;
	i32 widgetsCount = sva_count(aWidgets);
	for (i32 w = 0; w < widgetsCount; ++w)
	{
	    StdUiWidget* pWidget = &aWidgets[w];
	    pWidget->State = StdUiState_None;
	}
    }

    for (i32 p = 0; p < panelsCount; ++p)
    {
	StdUiPanel* pPanel = std_ui_panel_get_by_id(p);

	v3 pos  = pPanel->Position;
	v2 size = pPanel->Size;
	StdUiWidget* aWidgets = pPanel->WidgetCache.aWidgets;

	i32 skipPanel = 0;
	if (isInputAlreadyProcessed || !std_ui_is_hovered(pos, size))
	{
	    skipPanel = 1;
	}

	i32 widgetsCount = sva_count(aWidgets);
	for (i32 w = 0; w < widgetsCount; ++w)
	{
	    StdUiWidget* pWidget = &aWidgets[w];

	    i32 shouldSkipInput = skipPanel || pWidget->Flags == StdUiWidgetFlags_Disabled;
	    if (shouldSkipInput)
		continue;

	    // DOCS: Process widget input
	    v3 widgetPos = pWidget->Position;
	    StdUiWidgetType* pWidgetType =
		std_ui_get_widget_type_by_id(pWidget->TypeId);
	    v2 widgetSize = pWidget->Size;

	    std_ui_click_behavior(widgetPos, widgetSize, pWidget->Id, &pWidget->State);

	    if (pWidget->State != StdUiState_None)
	    {
		//slog_info("Widget state: %s\n", std_ui_state_to_string(pWidget->State));
		// do smth with pWidget->ShapeStateInd
		//pCustom->Flags.GeometryStateIndex = i;

		g.Ui.Cache.pActivePanelLabel = pPanel->pLabel;
		isInputAlreadyProcessed = 1;
		goto ProcessInputEndLabel;
	    }
	} // for w:widgetsCount
    } // for p:panelsCount

ProcessInputEndLabel:
    std_ui_input_clear(&g.Ui.Input);

#if 0
    StdUiInteractive* pInteractive = &g.Ui.Interactive;
    if (IsKeyReleased(StdUiKey_Mouse_Left_Button))
    {
	pInteractive->ActiveId = 0;
	//NOTE(typedef): We can ignore this assign but i want this to be 0
	pInteractive->HotId = 0;
	pInteractive->ActivePanelId = 0;
	pInteractive->HotPanelId = 0;
    }
#endif // 0
}


/*     #####################################
  DOCS ##          Std Ui Utils           ##
       #####################################*/

v2
std_ui_get_text_metrics(char* pText, i32 fontSize)
{
    return g.Backend.GetTextMetrics(pText, fontSize, NULL);
}


/*     #####################################
  DOCS ##         Std Ui Helper           ##
       #####################################*/

char*
std_ui_new_string(const char* pString)
{
    if (!pString)
	return NULL;

    i64 ind = shash_geti(g.hAllocatedStrings, pString);
    if (ind == -1)
    {
	//slog_warn("Unknown string %s\n", pString);
	char* pNewString = simple_string_new(pString);
	shash_put(g.hAllocatedStrings, pString, pNewString);
	ind = table_index(g.hAllocatedStrings);
    }

    return g.hAllocatedStrings[ind].Value;
}


/*     #####################################
  DOCS ##         Std Ui Backend          ##
       #####################################*/


StdUiFuncs
std_ui_backend_get_funcs()
{
    return g.Backend;
}

static v4
GetColorByState(StdUiState state, StdUiStyleItem styleItem)
{
    switch (state)
    {
    case StdUiState_Hovered:
	return styleItem.Color.Hovered;
	break;

    case StdUiState_Clicked:
	return styleItem.Color.Clicked;
	break;

    default:
	return styleItem.Color.Background;
    }
}

static v4
GetWidgetColorByState(StdUiState state, StdUiShapeStyle style)
{
    switch (state)
    {
    case StdUiState_Hovered:
	return style.Color.Hovered;
	break;

    case StdUiState_Clicked:
	return style.Color.Clicked;
	break;

    default:
	return style.Color.Idle;
    }
}


void
std_ui_backend_new(StdUiBackendSettings set)
{
    g.Backend = set.Funcs;
    g.DebugConfig = set.DebugConfig;
}


void std_ui_backend_draw_panel(StdUiPanel* pPanel);
// todo: remove
void std_ui_backend_draw_custom(StdUiPanel* pPanel, StdUiCustom* pCustom);
void std_ui_backend_draw_widget(StdUiPanel* pPanel, StdUiWidget* pWidget);
void std_ui_backend_draw_div(StdUiDiv div);

void
std_ui_backend_draw()
{
    //sva_foreach(g.Ui.Cache.Panels, slog_warn("panel ind: %d %s\n", item, std_ui_panel_get_by_id(iter)->pLabel));

    i32 panelsCount = sva_count(g.Ui.Cache.Panels);
    for (i32 p = 0; p < panelsCount; ++p)
    {
	StdUiPanel* pPanel = std_ui_panel_get_by_id(p);
	//slog_success("panel->Label: %s %d\n", pPanel->pLabel, count);

	if (pPanel->PanelState == StdUiPanelState_Closed)
	    continue;

	std_ui_backend_draw_panel(pPanel);

	if (g.DebugConfig.IsDivDebugMode)
	{
	    i32 divCount = sva_count(pPanel->aDivs);
	    for (i32 d = 0; d < divCount; ++d)
	    {
		StdUiDiv div = pPanel->aDivs[d];

		i32 ind =
		    sva_index_of(pPanel->WidgetCache.aWidgets, item.LayoutId == div.Id);
		if (ind != -1)
		{
		    v3 modified_pos = v3_addz(div.Absolute.Position, -0.2);
		    g.Backend.DrawEmptyRect(modified_pos, div.Absolute.Size, 2.0, v4_new(0.78, 0.23, 0.23, 1.0));
		}
	    }
	}

	i64 widgetsCount = sva_count(pPanel->WidgetCache.aWidgets);
	//slog_info("widgets count %d\n", widgetsCount);
	for (i32 w = 0; w < widgetsCount; ++w)
	{
	    StdUiWidget* pWidget = &pPanel->WidgetCache.aWidgets[w];
	    std_ui_backend_draw_widget(pPanel, pWidget);
	}

    }
}

void
std_ui_backend_sound()
{
    i32 i, count = sva_count(g.Ui.Cache.Panels);
    for (i = 0; i < count; ++i)
    {
	StdUiPanel* pPanel = std_ui_panel_get_by_id(i);

	if (pPanel->PanelState == StdUiPanelState_Closed)
	    continue;

	// todo: implement sound logic for CustomsCache.aCustoms
	/* i64 i, count = sva_count(pPanel->aWidgets); */
	/* for (i = 0; i < count; ++i) */
	/* { */
	/*     StdUiWidget* pWidget = &pPanel->aWidgets[i]; */
	/*     if (pWidget->State == StdUiState_Hovered && g.Ui.HoverSoundId != pWidget->Id) */
	/*     { */
	/*	g.Backend.PlayHoverSound(); */
	/*	g.Ui.HoverSoundId = pWidget->Id; */
	/*	//void (*PlayClickSound)(); */
	/*     } */
	/* } */
    }
}

void
std_ui_backend_draw_panel(StdUiPanel* pPanel)
{
    v2 xy = pPanel->Position.XY;
    g.Backend.SetViewport(xy.X, xy.Y, pPanel->Size.Width, pPanel->Size.Height);

    StdUiStyleItem styleItem = g.Ui.Style.Items[StdUiStyleItemType_Panel];

    v4 panelColor = GetColorByState(pPanel->State, styleItem);
    if (BitNotAnd(pPanel->Flags, StdUiPanelFlags_Invisible))
    {
	//slog_error("%v3 %v2\n", pPanel->Position, pPanel->Size);
	g.Backend.DrawRect(pPanel->Position, pPanel->Size, panelColor);
    }

    //slog_error("pos: %v3 size: %v2\n", pPanel->Position, pPanel->Size);
}

static v4
std_ui_custom_get_color_by_state(char* pName, StdUiState state, StdUiCustomStyle style)
{
    if (style.IsSingleColor)
	return style.Color;

    switch (state)
    {
    case StdUiState_Clicked: return style.ClickedColor;
    case StdUiState_Hovered: return style.HoveredColor;
    case StdUiState_Grabbed: return style.ClickedColor;
    default: return style.Color;
    }
}

StdUiCustomStyle
std_ui_geometry_get_style(StdUiGeometry geometry)
{
    StdUiCustomStyle style = shash_get(g.Ui.CustomStorage.hRegisteredStyles, geometry.pName);

    if (table_index(g.Ui.CustomStorage.hRegisteredStyles) != -1)
	return style;

    switch (geometry.GeometryType)
    {

    case StdUiGeometryType_Text:
    {
	StdUiCustomStyle otherStyle = {
	    .Color = v4_new(1, 1, 1, 1),
	    .HoveredColor = v4_new(1, 0, 0, 1),
	    .ClickedColor = v4_new(1, 0, 0, 1),
	};

	return otherStyle;
    }

    default:
    {
	vassert_break();
	return (StdUiCustomStyle) {};
    }

    }
}

void
std_ui_backend_draw_widget(StdUiPanel* pPanel, StdUiWidget* pWidget)
{
    StdUiWidgetType* pWidgetType =
	std_ui_get_widget_type_by_id(pWidget->TypeId);

    StdUiDiv div = std_ui_panel_div_get(pPanel, pWidget->LayoutId);

    i64 shapesCount = sva_count(pWidgetType->aShapes);
    for (i32 s = 0; s < shapesCount; ++s)
    {
	StdUiShape shape = pWidgetType->aShapes[s];
	StdUiShapeStyle style =
	    std_ui_style_get(shape.pStyleName);
	v4 color = GetWidgetColorByState(pWidget->State, style);

	v3 pos = v3_new(
	    shape.Position.X * pWidget->Position.X,
	    shape.Position.Y * pWidget->Position.Y,
	    shape.Position.Z + pWidget->Position.Z
	    );
	v2 size = pWidget->Size;
	i32 fontSize = div.FontSize>0 ? div.FontSize * shape.FontSizeRatio : 1;

	switch (shape.Type)
	{

	case StdUiShapeType_Rect:
	{
	    g.Backend.DrawRect(pos, size, color);
	    break;
	}

	case StdUiShapeType_EmptyRect:
	{
	    f32 thickness = 1.2;
	    //slog_info("color %v4\n", color);
	    g.Backend.DrawEmptyRect(pos, size, thickness, color);
	    break;
	}

	case StdUiShapeType_Text:
	{
	    v2 metrics = std_ui_get_text_metrics(pWidget->pName, div.FontSize);
	    if (shape.Align == StdUiTextAlign_Custom)
	    {
		// note: align by text top,
		// so we need some Y offset

		v3 wpos = pWidget->Position;
		v2 wsiz = pWidget->Size;

		f32 sy = wpos.Y - metrics.Height;

		v3 pos = v3_new(
		    pWidget->Position.X + shape.Position.X * pWidget->Size.X,
		    sy - shape.Position.Y * pWidget->Size.Y,
		    shape.Position.Z + pWidget->Position.Z
		    );

		g.Backend.DrawText(pWidget->pName, fontSize, pos, color);
	    }
	    else if (shape.Align == StdUiTextAlign_Center)
	    {
		f32 centerX = (size.Width / 2) - metrics.Width/2;
		f32 centerY = (size.Height / 2) - metrics.Height/2 + metrics.Height;

		v3 textPos = pWidget->Position;
		f32 offsetY = (size.Y - metrics.Y) / 2;
		f32 wa = size.Width - metrics.Width;
		textPos.X = pWidget->Position.X + centerX;
		textPos.Y = pWidget->Position.Y - centerY;
		textPos.Z = pWidget->Position.Z - 0.3f;
		g.Backend.DrawText(pWidget->pName, fontSize, textPos, color);

		// todo: make debugger tool like that
		//std_ui_backend_text_visual(pWidget->pName, textPos, div.FontSize, g.Backend.DrawRect);
	    }

	    break;
	}

	case StdUiShapeType_Image:
	{
	    v3 imagePos = v3_addz(pWidget->Position, -0.5);
	    v2 imageSize = pWidget->Size;
	    g.Backend.DrawImage(imagePos, imageSize, v4_rgb(255, 255, 255), pWidget->AssetId);
	    break;
	}

	case StdUiShapeType_AtlasImage:
	{
	    v2 sizeOffset = v2_mul(pWidget->Size, shape.Position.XY);
	    sizeOffset.Y = -sizeOffset.Y;
	    v3 imagePos = v3_add(pWidget->Position, v3_v2v(sizeOffset, shape.Position.Z));
	    v2 imageSize = v2_mul(pWidget->Size, shape.Size);
	    g.Backend.DrawImageAtlasItem(imagePos, imageSize, v4_rgb(255, 255, 255), pWidget->AssetId, pWidget->ItemId);
	    break;
	}

	default:
	    break;
	}
    }
}

void
std_ui_backend_text_visual(char* pName, v3 pos, i32 fontSize, void (*DrawRectCallback)(v3 pos, v2 size, v4 color))
{
    i32 nameLength = simple_string_length(pName);
    v4 aColors[] = {
	v4_rgb(100, 73, 23),
	v4_rgb(93, 123, 73),
	v4_rgb(43, 53, 63),
	v4_rgb(143, 153, 163),
	v4_rgb(63, 43, 53),
	v4_rgb(163, 143, 153),
	v4_rgb(63, 53, 43),
	v4_rgb(163, 153, 143),
    };
    v3 nameTextPos = v3_addz(pos, 0.1);

    for (i32 i = 0; i < nameLength; ++i)
    {
	char ch = pName[i];
	char aChars[] = {ch, 0};

	v2 chMetrics = std_ui_get_text_metrics(aChars, fontSize);
	v3 offsetTextPos = v3_addy(nameTextPos, chMetrics.Y);
	DrawRectCallback(offsetTextPos, chMetrics, aColors[i % ArrayCount(aColors)]);

	nameTextPos.X += chMetrics.X;
    }
}
