#include "StdAtlas.h"
#include "Std/StdAssetManager.h"

#include <stdlib.h>
#include <time.h>
#include <Core/SimpleStandardLibrary.h>
#include <Core/SimpleMath.h>
#include <Std/StdRenderer.h>
#include <Deps/stb_image.h>
#include <Deps/stb_image_write.h>


StdAtlas
std_atlas_new(const char* pPath)
{
#define AtlasBuildDebug 0

    // DOCS: Parse meta from name
    char* pName = path_get_name_wo_extension(pPath);

#if defined(BUILD_DEBUG)
    if (string_index_of(pName, '-') == -1)
    {
	GERROR("pPath is wrong for atlas %s\n", pPath);
    }
#endif // BUILD_DEBUG

    char** aNameItems = string_split((char*) pName, '-');
    i32 width = string_to_i32(aNameItems[1]);
    i32 height = string_to_i32(aNameItems[2]);
    v2 itemSize = v2_new(width, height);

    // DOCS: Create OpenGL texture
    i64 imageId = std_asset_manager_load_image(pPath);
    StdTexture image = {};
    std_asset_manager_get_image(imageId, &image);

    StdAtlas atlas = {
	.TextureId = imageId,
	.pName = simple_string_new(pPath),
	.Size = image.Size,
	.aAtlasItems = NULL,
    };

    StdAtlasItem* aAtlasItems = NULL;
    i32 rowsCount = atlas.Size.Y / itemSize.Y;
    i32 colsCount = atlas.Size.X / itemSize.X;
    i32 itemsCount = colsCount * rowsCount;

#if AtlasBuildDebug == 1
    GINFO("ItemsCount: %d %d %d\n", itemsCount, rowsCount, colsCount);
#endif

    const real rx = itemSize.X / atlas.Size.X;
    const real ry = itemSize.Y / atlas.Size.Y;

    for (i32 r = 0; r < rowsCount; ++r)
    {
	for (i32 c = 0; c < colsCount; ++c)
	{
	    real sx, ex, sy, ey;

	    if (rowsCount == 1)
	    {
		sx = rx * c;
		ex = rx * (c + 1);
		sy = ry * r + 0.01;
		ey = ry * (r + 1);
	    }
	    else
	    {
		sx = rx * c;
		ex = rx * (c + 1);
		sy = ry * r;
		ey = ry * (r + 1);
	    }

	    v2 min = v2_new(sx, sy);
	    v2 max = v2_new(ex, ey);

#if AtlasBuildDebug == 1
	    GINFO("s %f %f\n", sx, sy);
	    GINFO("e %f %f\n", ex, ey);
#endif

#define ItemNameSize 2049
	    char itemName[ItemNameSize] = {};
	    snprintf(itemName, ItemNameSize, "%s%d%d", aNameItems[0], r, c);
#undef ItemNameSize

	    StdAtlasItem item = {
		.Size = itemSize,
		.pName = simple_string_new(itemName),
		.UvMin = min,
		.UvMax = max,
	    };

	    sva_add(aAtlasItems, item);
	}
    }

    atlas.aAtlasItems = aAtlasItems;

    sva_foreach(aNameItems, memory_free(item));
    sva_free(aNameItems);
    memory_free(pName);

#undef AtlasBuildDebug

    return atlas;
}
