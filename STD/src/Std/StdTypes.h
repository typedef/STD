#ifndef STD_TYPES_H
#define STD_TYPES_H

#include <Core/Types.h>
#include <Core/SimpleVulkanLibrary.h>
#include <Core/SimpleAtlasFont.h>


typedef i64 StdTextureId;
typedef i64 StdFontId;

typedef struct StdTexture
{
    StdTextureId Id;
    char* pName;
    v2 Size;
    i32 Channels;
    SvlTexture Handle;
} StdTexture;

typedef struct StdFont
{
    StdFontId Id;
    i32 Size;
    char* pPath;
    SafFont Handle;
    SvlTexture Atlas;
} StdFont;

v2 saf_font_get_metrics_generic(StdFont* aFonts, char* pText, i32 fontSize);

typedef struct StdGeneratedTexture
{
    char* pName;
    void* pData;
    v2 Size;
    i32 Channels;
} StdGeneratedTexture;

typedef struct StdAtlasItem
{
    char* pName;
    v2 Size;
    v2 UvMin;
    v2 UvMax;
} StdAtlasItem;

typedef struct StdAtlasRegistration
{
    v2 Size;
    const char* pPath;
} StdAtlasRegistration;

typedef struct StdAtlasSettings
{
    v2 Size;
    const char** aPaths;
} StdAtlasSettings;

typedef struct StdAtlas
{
    i64 Id;
    i64 TextureId;
    char* pName;
    v2 Size;
    StdAtlasItem* aAtlasItems;
} StdAtlas;

typedef struct StdAtlasWithBinary
{
    StdAtlas Atlas;
    void* pData;
} StdAtlasWithBinary;

#endif // STD_TYPES_H
