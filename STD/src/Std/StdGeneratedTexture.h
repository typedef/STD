#ifndef STD_GENERATED_TEXTURE_H
#define STD_GENERATED_TEXTURE_H

#include <Std/StdTypes.h>


StdGeneratedTexture std_generated_texture_new(const char* pName, v2 size, i32 channels, void* pData);
void std_generated_texture_destroy(StdGeneratedTexture generatedTexture);
StdGeneratedTexture std_generated_texture_create_ground();

#endif // STD_GENERATED_TEXTURE_H
