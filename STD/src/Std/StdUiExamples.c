#include "StdUiExamples.h"

#include <Core/SimpleApplication.h>
#include <Core/SimpleAtlasFont.h>
#include <Core/SimpleStandardLibrary.h>
#include <Core/SimpleWindowLibrary.h>
#include <Core/Types.h>
#include <Core/SimpleMath.h>
//#include <Std/StdAssetManager.h>
#include <Std/StdAtlas.h>
#include <Std/StdRenderer.h>
#include <Std/StdInput.h>
#include <Std/StdUi.h>

#include <Std/StdUiCustom.h>

#if 0


// todo: not working
// todo: update names for the flags
#define CREATE_GIZMO 0
// todo: not working WIP
#define CREATE_INVENTORY 0
// todo: not working
#define CREATE_HUD_QUICK_SLOTS 1
#define CREATE_HUD_HERO 0


/*
  DOCS: Example of gizmo usage
*/
void std_gizmo_update();

/*
  DOCS: HUD - helth/mana bars + quick slots
*/
void std_ui_hud_update();

/*
  DOCS: Inventory - grid of items, different tabs with animation
*/
void std_ui_inventory_update();

void
std_ui_example()
{
    if (helper_tofr())
    {
	std_ui_custom_init();

	//SwlWindow* pWindow = simple_application_get_window();
	//std_opengl_set_swap_interval(pWindow, 1);
	StdUiDebug debug = {
	    //.ShouldDrawDivArea = 1,
	};
	std_ui_debug(&debug);

	//std_gl_renderer_add_font(resource_font("NotoSans.ttf"), 16);
	//std_gl_renderer_add_font(resource_font("NotoSans.ttf"), 48);
    }

    //std_ui_example_main_inventory();

#if CREATE_GIZMO == 1
    std_gizmo_update();
#endif

#if CREATE_INVENTORY == 1
    std_ui_inventory_update();
#endif

    std_ui_hud_update();
}

void
std_gizmo_update()
{
    v2 outputValue = v2_new(500, 500);
    StdUiGizmoSettings gizmoSettings = {
	.pLabel = "Гизмо",
	.Flags = StdUiGizmoFlags_Position
    };
    StdUiState state = std_ui_gizmo(gizmoSettings, &outputValue);
}

/*
  DOCS: HUD - helth/mana bars + quick slots
*/

StdUiState
std_ui_hud_state_item(const char* pSlotName, const char* pBarName, char* pState, f32 stateValue, f32 maxStateValue)
{
    const f32 max_health_width = 200;
    const f32 health_bar_height = 20;

    StdUiGeometry healthSlot = {
	.pName = (char*) pSlotName,
	.GeometryType = StdUiGeometryType_Rect,
	.Position = v3_new(2, 0, 0),
	.Size = v2_new(max_health_width, health_bar_height),
    };

    f32 ratio = stateValue / maxStateValue;
    f32 healthBarWidth = ratio * max_health_width;

    StdUiGeometry healthBar = {
	.pName = (char*) pBarName,
	.GeometryType = StdUiGeometryType_Rect,
	.Position = v3_new(2, 0, -0.1),
	.Size = v2_new(healthBarWidth, health_bar_height),
    };

    StdUiGeometry* aGeometries = NULL;
    sva_add(aGeometries, healthSlot);
    sva_add(aGeometries, healthBar);

    StdUiCustom inventBtn = {
	.pName = pState,
	.aGeometries = aGeometries,
	.Flags.ShouldUpdate = 0,
    };

    StdUiState state = std_ui_custom(inventBtn);
    sva_free(aGeometries);

    return state;
}

typedef struct InventoryItem
{
    char* pName;
    i64 AssetId;
    i32 Index;
    i32 Count;
} InventoryItem;

StdUiState
std_ui_inventory_item(InventoryItem inventoryItem)
{
    const real metric_size = 75.0f;

    i32 geomCount = 2;
    StdUiGeometry emptyCell = (StdUiGeometry) {
	.pName = "Ячейка предмета",
	    .GeometryType = StdUiGeometryType_Rect,
	    .Position = v3_new(0,0,0),
	    .Size = v2_new(metric_size, metric_size)
    };

    StdUiGeometry usedCell;
    StdUiGeometry cellCounter;
    if (inventoryItem.AssetId != -1)
    {
	usedCell = (StdUiGeometry) {
	    .pName = "Используемая ячейка предмета",//
	    .GeometryType = StdUiGeometryType_Image_AtlasItem,
	    .Position = v3_new((metric_size - metric_size/1.5) / 2, -5, -0.2),
	    .Size = v2_new(metric_size/1.5, metric_size/1.5),

	    .Image = {
		.AssetId = inventoryItem.AssetId,
		.Index = inventoryItem.Index,
	    }

	};

	char text[4] = ">99";
	if (inventoryItem.Count < 100)
	    snprintf(text, 4, "%d", inventoryItem.Count);
	else
	{
	    text[0] = '>'; text[1] = '9'; text[2] = '9';
	}

	char* pText = std_ui_new_string(text);
	v2 metrics = std_ui_get_text_metrics(pText, 32);

	cellCounter = (StdUiGeometry) {
	    .pName = "Номер ячейки предмета",
	    .ExtraData = { .pUtf8Text = pText },
	    .GeometryType = StdUiGeometryType_Text,
	    .FontSize = 16,
	    .Position = v3_new(/* metric_size - metrics.X +  */5,
			       -metric_size + metrics.Y,
			       -0.3),
	};

	geomCount = 4;
    }

    StdUiGeometry border = {
	.pName = "Рамка ячейки предмета",
	.GeometryType = StdUiGeometryType_EmptyRect,
	.Position = v3_new(0, 0, -0.2),
	.Size = v2_new(metric_size, metric_size)
    };

    StdUiGeometry* aGeometries = NULL;
    sva_add(aGeometries, emptyCell);
    sva_add(aGeometries, border);
    sva_add(aGeometries, usedCell);
    sva_add(aGeometries, cellCounter);

    StdUiCustom inventBtn = {
	.pName = inventoryItem.pName,
	.aGeometries = aGeometries,
    };

    StdUiState state = std_ui_custom(inventBtn);
    sva_free(aGeometries);

    return state;
}

StdUiState
std_ui_hud_button(char* pName, i32 index, i32 isActive, i32 shouldUpdate)
{
    v2 boxSize = {75, 75};

    StdUiGeometry emptyCell = (StdUiGeometry) {
	.pName = "Ячейка предмета",
	.GeometryType = StdUiGeometryType_Rect,
	.Position = v3_new(0,0,0),
	.Size = boxSize
    };

    // FIXME: здесь возникает достаточно досадная особенность кеширования
    // - необходимость отслеживать обновления
    char* pStyleName = "Рамка ячейки предмета";
    if (isActive)
    {
	pStyleName = "Рамка ячейки предмета (Активная)";
    }

    StdUiGeometry border = {
	.pName = pStyleName,
	.GeometryType = StdUiGeometryType_EmptyRect,
	.Position = v3_new(0, 0, -0.2),
	.Size = boxSize
    };

    char slotText[4] = {};
    snprintf(slotText, 4, "%d", index);

    char* pSlotText = std_ui_new_string(slotText);
    v2 metrics = std_ui_get_text_metrics(pSlotText, 16);

    StdUiGeometry slotNumber = {
	.pName = pSlotText,
	.ExtraData = {
	    .pUtf8Text = pSlotText,
	},
	.GeometryType = StdUiGeometryType_Text,
	.FontSize = 16,
	.Position = v3_new(2, 0, -0.1),
    };

    StdUiGeometry* aGeometries = NULL;
    sva_add(aGeometries, emptyCell);
    sva_add(aGeometries, border);
    sva_add(aGeometries, slotNumber);

    StdUiCustom inventBtn = {
	.pName = pName,
	.aGeometries = aGeometries,
	.Flags.ShouldUpdate = shouldUpdate,
    };

    StdUiState state = std_ui_custom(inventBtn);
    sva_free(inventBtn.aGeometries);

    return state;
}

void
std_ui_hud_inventory_items(char* pName, i32* pActiveIndex)
{
    //std_ui_playout(StdUiPlayout_Bottom);
    static i32 shouldUpdate = 0;

    if (std_ui_panel_begin((StdUiPanelSettings) {
		.pUtfLabel = "Hud Buttons",
	    }))
    {
	StdUiDiv div = {
	    .Position = { 0., 0. },
	    .Margin   = { 0, 0 },
	    .Padding  = { 1, 0 },
	    .Display  = StdUiDivDisplay_SameLine,
	};
	std_ui_div(div);

	i32 activeIndex = *pActiveIndex;
	i32 itemCount = 9;
	for (i32 i = 1; i <= itemCount; ++i)
	{
	    char buf[15] = {};
	    snprintf(buf, 15, "Ячейка%d", i);

	    i32 isActive = activeIndex == i;

	    if (std_ui_hud_button(buf, i, isActive, shouldUpdate) == StdUiState_Clicked)
	    {
		GINFO("Clicked %s\n", buf);
		*pActiveIndex = i;
		shouldUpdate = 2;
	    }
	}

	if (shouldUpdate)
	{
	    --shouldUpdate;
	}
    }

    std_ui_panel_end();

    SwlWindow* pWindow = simple_application_get_window();
    for (i32 i = (i32)SwlKey_1; i <= (i32)SwlKey_9; ++i)
    {
	if (pWindow->aKeys[i] == SwlAction_Press)
	{
	    *pActiveIndex = i;
	    shouldUpdate = 2;
	    break;
	}
    }

}


void
_hud_quick_slots()
{
    static i32 activeIndex;
    if (helper_tofr())
    {
	activeIndex = 3;

	real cv = 7/255.0f;
	StdUiCustomStyle itemCellStyle = {
	    .Color        = v4_new(cv, cv, cv, 1),
	    .HoveredColor = v4_new(0.45, 0.13, 0.13, 1),
	    .ClickedColor = v4_new(0.35, 0.03, 0.03, 1)
	};
	std_ui_custom_style("Ячейка предмета", itemCellStyle);

	v4 borderColor = v4_new(37/255.0f, 37/255.0f, 42/255.0f, 1);
	StdUiCustomStyle borderCellStyle = {
	    .Color        = borderColor,
	    .HoveredColor = borderColor,
	    .ClickedColor = borderColor,
	    .Thickness = 2,
	};

	std_ui_custom_style("Рамка ячейки предмета", borderCellStyle);
	borderCellStyle.Color = v4_new(0.75, 0.43, 0.43, 1);
	borderCellStyle.HoveredColor = v4_new(0.75, 0.43, 0.43, 1);
	std_ui_custom_style("Рамка ячейки предмета (Активная)", borderCellStyle);
    }

    std_ui_hud_inventory_items("UI предметов", &activeIndex);
    // GINFO("%d\n", activeIndex);
}

void
_hero_hud()
{
    if (helper_tofr())
    {
	const f32 cv = 7/255.0f;
	StdUiCustomStyle healthSlotStyle = {
	    .IsSingleColor = 1,
	    .Color         = v4_new(cv, cv, cv, 1),
	};
	std_ui_custom_style("State Slot", healthSlotStyle);

	v4 activeColor = v4_new(0.75, 0.43, 0.43, 1);
	StdUiCustomStyle healthBarStyle = {
	    .IsSingleColor = 1,
	    .Color         = activeColor,
	};
	std_ui_custom_style("Health Bar", healthBarStyle);

	v4 manaColor = v4_new(0.43, 0.43, 0.75, 1);
	StdUiCustomStyle manaBarStyle = {
	    .IsSingleColor = 1,
	    .Color         = manaColor,
	};
	std_ui_custom_style("Mana Bar", manaBarStyle);
    }

    //std_ui_playout(StdUiPlayout_TopLeft);
    static i32 shouldUpdate = 0;

    if (std_ui_panel_begin((StdUiPanelSettings) {
		.pUtfLabel = "Hud State",
	    }))
    {
	StdUiDiv div = {
	    .Margin = { 0, 15 },
	    .Display = StdUiDivDisplay_NewLine,
	};
	std_ui_div(div);

	// todo: fix size calculations
	std_ui_hud_state_item("State Slot", "Health Bar", "Health", 70, 100);

	StdUiDiv div2 = {
	    .Margin = { 0, 35 },
	    .Display = StdUiDivDisplay_NewLine,
	};
	std_ui_div(div2);
	std_ui_hud_state_item("State Slot", "Mana Bar", "Mana", 30, 100);
	//std_ui_hud_state_item("Mana2", 30, 100);


    }

    std_ui_panel_end();
}

void
std_ui_hud_update()
{
#if CREATE_HUD_QUICK_SLOTS == 1
    _hud_quick_slots();
#endif // CREATE_HUD_QUICK_SLOTS

#if CREATE_HUD_HERO == 1
    _hero_hud();
#endif // CREATE_HUD_HERO

}


/*
  DOCS: Inventory - grid of items, different tabs with animation
*/

StdUiState
std_ui_custom_tab_button(char* pName, i32* pIsActive)
{
    StdUiGeometry* aGeometries = NULL;
    i8 shouldUpdate = helper_tofr();

    StdUiGeometry text = {
	.pName = "InvTabName",
	.ExtraData = { .pUtf8Text = pName },
	.Id = std_ui_custom_get_id(pName),
	.GeometryType = StdUiGeometryType_Text,
	.Position = v3_new(0, 0, 0),
    };

    sva_add(aGeometries, text);

    // note: this is absolute garbage
    i32 isActive = *pIsActive;
    if (isActive)
    {
	shouldUpdate = 1;
	//v2 textMetrics = std_gl_renderer_get_text_metrics_u8(pName, 32);
	i64 id = std_ui_custom_get_id(pName);

	v2 size = {};
	v2 pos  = {};
	if (isActive == 2)
	{
	    std_ui_animate_rectangle(
		(StdUiAnimationRect) {
		    .Id = id,
		    //.End = textMetrics,
		    .Type = StdUiAnimationRectType_CenterToEdge,
		    .Timer = (SimpleTimer) {
			.WaitSeconds = 0.0005,
		    }
		},
		&size, &pos);
	}

	StdUiGeometry inventoryActive = {
	    .pName = "Inventory Active",
	    .Id = id,
	    .GeometryType = StdUiGeometryType_Rect,
	    .Position = v3_new(pos.X, -35, 0),
	    .Size = v2_new(size.X /* textMetrics.X */, 10)
	};

	sva_add(aGeometries, inventoryActive);
    }
    else
    {
	StdUiCustom* pCustom = std_ui_panel_get_custom(std_ui_panel_get(), pName);
	if (pCustom != NULL && sva_count(aGeometries) == 2)
	{
	    shouldUpdate = 1;
	    //GERROR("UPDATE\n");
	}

	std_ui_reset_animate_rectangle(pName);
    }

    StdUiState prevState = (StdUiState) *pIsActive;

    StdUiCustom menuTabButton = {
	.pName = pName,
	.Flags.ShouldUpdate = shouldUpdate
    };

    StdUiState state = std_ui_custom(menuTabButton);
    if (prevState != StdUiState_Clicked)
    {
	*pIsActive = state;
	if (state == StdUiState_None)
	{
	    // GINFO("Not active anymore!\n");
	}
    }

    sva_free(aGeometries);

    return state;
}

StdUiState
std_custom_button(char* pLabel)
{
    if (helper_tofr())
    {
	// init style
	StdUiCustomStyle btnBackStyle = {
	    .Color        = v4_new(0.75, 0.43, 0.43, 1),
	    .HoveredColor = v4_new(0.45, 0.13, 0.13, 1),
	    .ClickedColor = v4_new(0.35, 0.03, 0.03, 1)
	};
	std_ui_custom_style("Кнопка", btnBackStyle);

	StdUiCustomStyle btnTextStyle = {
	    .Color        = v4_new(0.75, 0.75, 0.75, 1),
	};
	std_ui_custom_style("Текст Кнопки", btnTextStyle);
    }

    StdUiPanel* pPanel = std_ui_panel_get();
    // DOCS: Check for existance
    StdUiCustom* pCustom = std_ui_panel_get_custom(pPanel, pLabel);
    if (pCustom == NULL)
    {
	if (string_compare(pLabel, "Play"))
	{
	    vguard_call_once();
	}

	StdUiDiv div = std_ui_div_get(pPanel->CurrentDiv);
	if (div.FontSize <= 0)
	    div.FontSize = 32;

	v2 metrics = std_ui_get_text_metrics(pLabel, div.FontSize);
	const f32 pretty_offset = div.FontSize/2.0f;
	metrics.X += pretty_offset;
	metrics.Y += pretty_offset;

	/*
	  user:
	  Geom* aGeoms = geom_create();
	  add_custom(aGeoms);
	*/

	StdUiGeometry* aGeometries = NULL;

	StdUiGeometry rectGeom = {
	    .GeometryType = StdUiGeometryType_Rect,
	    .pName = "Кнопка",
	    .Size = metrics
	};

	StdUiGeometry textGeom = {
	    .GeometryType = StdUiGeometryType_Text,
	    .pName = "Текст Кнопки",
	    .ExtraData = { .pUtf8Text = pLabel },
	    .Position = v3_new(pretty_offset/2, -pretty_offset/4, -0.2),
	};

	// todo: std_ui_geometry_add();
	sva_add(aGeometries, rectGeom);
	sva_add(aGeometries, textGeom);

	StdUiCustom custom = {
	    .pName = pLabel,
	    .aGeometries = aGeometries,
	};

	// DOCS: Creating new button
	pCustom = std_ui_panel_add_custom(pPanel, custom);

	sva_free(aGeometries);
    }
    else
    {
	//sva_foreach(pCustom->aGeometries, printf("%.*s\n", simple_core_utf8_length(item.pName), item.pName));
    }

    pCustom->Flags.IsInUse = 1;
    pCustom->LayoutId = pPanel->CurrentDiv;

    StdUiState state = std_ui_custom_process_input(pCustom);

    return state;
}

void
std_ui_inventory_update()
{
    static i64 gBottleAtlas;
    static i64 gOtherBottleAtlas;

    if (helper_tofr())
    {
	GINFO("Styles for inventory initialized!\n");
	// docs: initialization code
	char* pBottlePath = "Assets/Interface/Inventory/Bottles/Bottle-32-32.png";
	//gBottleAtlas = std_asset_manager_load_atlas_by_id(pBottlePath);

	real cv = 7/255.0f;
	StdUiCustomStyle itemCellStyle = {
	    .Color        = v4_new(cv, cv, cv, 1),
	    .HoveredColor = v4_new(0.45, 0.13, 0.13, 1),
	    .ClickedColor = v4_new(0.35, 0.03, 0.03, 1)
	};
	std_ui_custom_style("Ячейка предмета", itemCellStyle);

	v4 borderColor = v4_new(37/255.0f, 37/255.0f, 42/255.0f, 1);
	StdUiCustomStyle borderCellStyle = {
	    .Color        = borderColor,
	    .HoveredColor = borderColor,
	    .ClickedColor = borderColor,
	    .Thickness = 2,
	};

	std_ui_custom_style("Рамка ячейки предмета", borderCellStyle);
	borderCellStyle.Color = v4_new(0.75, 0.43, 0.43, 1);
	borderCellStyle.HoveredColor = v4_new(0.75, 0.43, 0.43, 1);
	std_ui_custom_style("Рамка ячейки предмета (Активная)", borderCellStyle);

	StdUiCustomStyle tabLabelStyle = {
	    .Color        = v4_new(1,1,1,1),
	    .HoveredColor = v4_new(0.75, 0.43, 0.43, 1),
	    .ClickedColor = v4_new(0.75, 0.43, 0.43, 1),
	};
	std_ui_custom_style("InvTabName" , tabLabelStyle);

	StdUiCustomStyle useItemCellStyle = {
	    .Color        = v4_new(1,1,1,1),
	    .HoveredColor = v4_new(1,0.7,0.7,1),
	    .ClickedColor = v4_new(1,1,1,1),
	    .Thickness = 2,
	};
	std_ui_custom_style("Используемая ячейка предмета", useItemCellStyle);

	StdUiCustomStyle itemCellNumber = {
	    .Color        = v4_new(1,1,1,1),
	    .HoveredColor = v4_new(1,0.7,0.7,1),
	    .ClickedColor = v4_new(1,1,1,1),
	};
	std_ui_custom_style("Номер ячейки предмета", itemCellNumber);

	StdUiCustomStyle counterCellStyle = {
	    .Color        = v4_new(1,1,1,1),
	    .HoveredColor = v4_new(1,0.7,0.7,1),
	    .ClickedColor = v4_new(1,1,1,1),
	    .Thickness = 2,
	};
	std_ui_custom_style("Кол-во ячеек предмета", counterCellStyle);

	StdUiCustomStyle inventoryStyle = {
	    .Color        = v4_new(0.75, 0.43, 0.43, 1),
	    .HoveredColor = v4_new(0.45, 0.13, 0.13, 1),
	    .ClickedColor = v4_new(0.35, 0.03, 0.03, 1)
	};
	std_ui_custom_style("Inventory Active", inventoryStyle);


    }

#if 1

    if (std_ui_panel_begin((StdUiPanelSettings) { .pUtfLabel = "sas" }))
    {
	StdUiPanel* pPanel = std_ui_panel_get();
	pPanel->Position.XY = v2_new(250, 650);

	StdUiDiv div = (StdUiDiv) {
	    .Margin   = { 15, 15 },
	    .Padding  = { 5, 25 },
	    .Display  = StdUiDivDisplay_NewLine,
	};
	std_ui_div(div);

	StdUiState state = std_custom_button("Play");
	if (state == StdUiState_Clicked)
	{
	    GINFO("Clicked\n");
	}

	if (std_custom_button("Продолжить игру") == StdUiState_Clicked)
	{
	    GINFO("Clicked\n");
	}

	std_ui_div((StdUiDiv) {
		.Margin   = { 0, 10 },
		.Padding  = { 25, 5 },
		.Display  = StdUiDivDisplay_NewLine_Stretch_ForMaxWidget,
	    });

	if (std_custom_button("Настройки") == StdUiState_Clicked)
	{
	    GINFO("Clicked\n");
	}

	if (std_custom_button("Об Игре") == StdUiState_Clicked)
	{
	    GINFO("Clicked\n");
	}

	/* std_ui_div((StdUiDiv) { */
	/*	.Margin   = { 0, 10 }, */
	/*	.Padding  = { 25, 5 }, */
	/*	.Display  = StdUiDivDisplay_SameLine, */
	/*     }); */
	/* std_custom_button("X"); */
	/* std_custom_button("Y"); */


	if (helper_fofr())
	{
	    DO_ONES(
		GINFO("CUSTOM SHIT\n\n\n\n");
		StdUiPanel* pPanel = std_ui_panel_get();
		for (i32 i = 0; i < sva_count(pPanel->CustomCache.aCustoms); ++i)
		{
		    StdUiCustom* pCustom = &pPanel->CustomCache.aCustoms[i];

		    if (pCustom->Id == 0)
		    {
			GERROR("pCustom->Id is invalid %d %s\n", pCustom->Id, pCustom->pName);
		    }

		    v3 p = pCustom->Metrics.Position;
		    v2 s = pCustom->Metrics.Size;
		    GINFO(RED("Pos")" %s %0.0f %0.0f %0.0f, "RED("Size")" %0.0f %0.0f\n", pCustom->pName, p.X, p.Y, p.Z, s.X, s.Y);
		}
		);
	}

    }

    std_ui_panel_end();

    return;

#endif // fix bug

    if (std_ui_panel_begin((StdUiPanelSettings) {
		.pUtfLabel = "Main Inventory",
		//.Flags = StdUiPanelFlags_Invisible
	    }))
    {

	StdUiDiv div = (StdUiDiv) {
	    .Position = { 0., 0. },
	    .Margin   = { 15, 5 },
	    .Padding  = { 15, 0 },
	    .Display  = StdUiDivDisplay_SameLine,
	};
	std_ui_div(div);

	static i32 isInventoryActive = 2;
	static i32 isCharacterActive = 0;

	StdUiState inventoryState = std_ui_custom_tab_button("Инвентарь", &isInventoryActive);
	if (inventoryState == StdUiState_Clicked)
	{
	    GINFO("Selected Инвентарь\n");
	    isCharacterActive = 0;

	}
	else if (inventoryState == StdUiState_Hovered)
	{
	    // GINFO("Hovered Инвентарь %d\n", isInventoryActive);
	}

	StdUiState characterState = std_ui_custom_tab_button("Персонаж", &isCharacterActive);
	if (characterState == StdUiState_Clicked)
	{
	    isInventoryActive = 0;
	}

	/*
	  NOTE:

	  нужно создавать BorderedAreaWidget?


	  Нужно сделать так, чтобы каждый новый див получал отступ от предыдущего дива.

	*/

	// TODO: Грид чтобы запихнуть все элементы в 1 div

	if (isInventoryActive == 2)
	{
	    {
		StdUiDiv dDiv = (StdUiDiv) {
		    .Position = {  0,  0 },
		    .Margin   = { 35,  15 },
		    .Padding  = { 5, 0 },
		    .Display  = StdUiDivDisplay_SameLine,
		};
		std_ui_div(dDiv);

		{
		    InventoryItem itemBottle = {
			.pName = "Зелье",
			.AssetId = gBottleAtlas,
			.Index = 0,
			.Count = 122
		    };
		    std_ui_inventory_item(itemBottle);
		}

		{
		    InventoryItem itemBottle = {
			.pName = "Зелье2",
			.AssetId = gBottleAtlas,
			.Index = 8,
			.Count = 2
		    };
		    std_ui_inventory_item(itemBottle);
		}

		{
		    InventoryItem itemBottle = {
			.pName = "Зелье3",
			.AssetId = gBottleAtlas,
			.Index = 12,
			.Count = 1
		    };
		    std_ui_inventory_item(itemBottle);
		}

		for (i32 c = 3; c < 7; ++c)
		{
		    char buf[128] = {};
		    snprintf(buf, 128, "La %d %d", 0, c);
		    InventoryItem item = {
			.pName = buf,
			.AssetId = -1,
			.Count = 0
		    };
		    std_ui_inventory_item(item);
		}
	    }

	    for (i32 r = 1; r < 2; ++r)
	    {
		StdUiDiv dDiv = (StdUiDiv) {
		    .Position = {  0,  0 },
		    .Margin   = { 35,  5 },
		    .Padding  = { 5, 0 },
		    .Display  = StdUiDivDisplay_SameLine,
		};
		std_ui_div(dDiv);

		for (i32 c = 0; c < 7; ++c)
		{
		    char buf[128] = {};
		    snprintf(buf, 128, "La %d %d", r, c);
		    InventoryItem item = {
			.pName = buf,
			.AssetId = -1,
			.Count = 0
		    };
		    std_ui_inventory_item(item);
		}
	    }
	}
    }

    std_ui_panel_end();

}
#endif // #if 0
