#include "StdUiCustom.h"
#include <stdio.h>
#include <Core/SimpleStandardLibrary.h>
#include <Core/SimpleMath.h>
#include <Core/SimpleMathIO.h>
#include <EntitySystem/StdScene.h>
#include <Std/StdUi.h>

#if 0
static StdUiFuncs gStdUiFuncs = {};

/*     #####################################
  DOCS ##      Std Ui Custom Widgets      ##
       #####################################*/


void std_ui_custom_widget_init_style();

void
std_ui_custom_init()
{
    gStdUiFuncs = std_ui_backend_get_funcs();
    std_ui_custom_widget_init_style();
}

void
std_ui_custom_widget_init_style()
{
    const f32 alpha_value = 0.7f;

    // DOCS: initialize the styles
    StdUiCustomStyle xGizmoStyle = {
	.Color        = v4_new(0.75, 0.43, 0.43, alpha_value),
	.HoveredColor = v4_new(0.45, 0.13, 0.13, alpha_value),
	.ClickedColor = v4_new(0.35, 0.03, 0.03, alpha_value)
    };
    std_ui_custom_style("Гизмо X", xGizmoStyle);

    StdUiCustomStyle yGizmoStyle = {
	.Color        = v4_new(0.43, 0.75, 0.43, alpha_value),
	.HoveredColor = v4_new(0.13, 0.45, 0.13, alpha_value),
	.ClickedColor = v4_new(0.03, 0.35, 0.03, alpha_value)
    };
    std_ui_custom_style("Гизмо Y", yGizmoStyle);

    StdUiCustomStyle xyGizmoStyle = {
	.Color        = v4_new(0.43, 0.43, 0.75, alpha_value),
	.HoveredColor = v4_new(0.13, 0.13, 0.45, alpha_value),
	.ClickedColor = v4_new(0.03, 0.03, 0.35, alpha_value)
    };
    std_ui_custom_style("Гизмо XY", xyGizmoStyle);

    /* StdUiCustomStyle gizmoDragStyle = { */
    /*	.Color        = v4_new(0.45, 0.45, 0.45, 1), */
    /*	.HoveredColor = v4_new(0.45, 0.45, 0.45, 1), */
    /*	.ClickedColor = v4_new(0.45, 0.45, 0.45, 1) */
    /* }; */
    /* std_ui_custom_style("Гизмо Drag 0", xGizmoStyle); */
    /* std_ui_custom_style("Line 0", xGizmoStyle); */
}

StdUiState
std_ui_custom_gizmo(StdUiGizmoSettings settings, v2 position, v2* pOutputValue)
{
    // DOCS: Build Gizmo

    char* pLabel = settings.pLabel;
    StdUiGizmoFlags flags = settings.Flags;

    StdUiCustom gizmo = { .pName = pLabel, };

    if (BitAnd(flags, StdUiGizmoFlags_X))
    {
	StdUiGeometry gizmoX = {
	    .pName = "Гизмо X",
	    .Id = std_ui_custom_get_id("Гизмо X"),
	    .GeometryType = StdUiGeometryType_Rect,
	    .Position = v3_new(0, 0, 0),
	    .Size = v2_new(100, 10),
	};

	sva_add(gizmo.aGeometries, gizmoX);
    }

    if (BitAnd(flags, StdUiGizmoFlags_Y))
    {
	StdUiGeometry gizmoY = {
	    .pName = "Гизмо Y",
	    .Id = std_ui_custom_get_id("Гизмо Y"),
	    .GeometryType = StdUiGeometryType_Rect,
	    .Position = v3_new(0, +100, 0),
	    .Size = v2_new(10, 100),
	};
	sva_add(gizmo.aGeometries, gizmoY);
    }

    if (BitAnd(flags, StdUiGizmoFlags_XY))
    {
	StdUiGeometry gizmoXY = {
	    .pName = "Гизмо XY",
	    .Id = std_ui_custom_get_id("Гизмо XY"),
	    .GeometryType = StdUiGeometryType_Rect,
	    .Position = v3_new(15, 25, 0),
	    .Size = v2_new(20, 20),
	};
	sva_add(gizmo.aGeometries, gizmoXY);
    }

    StdUiState state = std_ui_custom(gizmo);
    if (state == StdUiState_Grabbed)
    {
	StdUiInput* pInput = std_ui_get_input();

	v3 gp = v3_v2v(pInput->GrabMousePosition, 1.0); //v3_print(gp);
	v3 mp = v3_v2v(pInput->MousePosition, 1.0); //v3_print(mp);
	// docs: calc pos
	v3 cameraPos = std_scene_get_main_position(); // todo: apply camera transfor for vx, vy
	real vx = position.X + (mp.X - gp.X);//mp.X;
	real vy = position.Y + (mp.Y - gp.Y);

	gStdUiFuncs.DrawLine(gp, mp, 2, v4_new(1,1,1,1));

	StdUiCustom* pGizmo = std_ui_panel_get_custom(std_ui_panel_get(), pLabel);

	char* pName = pGizmo->aGeometries[pGizmo->Flags.GeometryStateIndex].pName;
	i64 nameLength = string_length(pName);
	char lc = pName[nameLength-1];
	char plc = pName[nameLength-2];

	switch (lc)
	{

	case 'X':
	{
	    // DOCS: X

#define TextBufferSize 128
	    char buf[TextBufferSize] = {};
	    i32 textLength = snprintf(buf, TextBufferSize, "p:x:%0.0f", vx);
	    v3 gizmoTextPos = v3_add(pGizmo->Metrics.Position, v3_new(40, 40, 0));
	    v4 textColor = v4_new(1., 0.7, 0.7, 1.0);
	    gStdUiFuncs.DrawText(buf, 32, gizmoTextPos, textColor);

	    *pOutputValue = v2_new(vx, pGizmo->Metrics.Position.Y);
	    break;
	}

	case 'Y':
	{
	    if (plc != 'X')
	    {
		// DOCS: Y
#define TextBufferSize 128
		char buf[TextBufferSize] = {};
		i32 textLength = snprintf(buf, TextBufferSize, "p:y:%0.0f", vy);
		v3 gizmoTextPos = v3_add(pGizmo->Metrics.Position, v3_new(10, 60, 0));
		v4 textColor = v4_new(0.7, 1., 0.7, 1.0);
		gStdUiFuncs.DrawText(buf, 32, gizmoTextPos, textColor);

		*pOutputValue = v2_new(pGizmo->Metrics.Position.X, vy);
	    }
	    else
	    {
		// DOCS: XY
#define TextBufferSize 128
		char buf[TextBufferSize] = {};
		i32 textLength = snprintf(buf, TextBufferSize, "p:%0.0f %0.0f", vx, vy);
		v3 gizmoTextPos = v3_add(pGizmo->Metrics.Position, v3_new(40, 40, 0));
		v4 textColor = v4_new(0.7, 0.7, 1., 1.0);
		gStdUiFuncs.DrawText(buf, 32, gizmoTextPos, textColor);

		*pOutputValue = v2_new(vx, vy);
	    }
	    break;
	}

	} // switch (lc)

    }

    sva_free(gizmo.aGeometries);

    return state;
}

StdUiState
std_ui_gizmo(StdUiGizmoSettings settings, v2* pOutputValue)
{
    static i32 isGrabbed = 0;
    static v2 newPosValue = {};

    StdUiState state = StdUiState_None; //_std_ui_gizmo(settings, pOutputValue);

    v2 pos = *pOutputValue;

    if (std_ui_panel_begin((StdUiPanelSettings) {
		.pUtfLabel = settings.pLabel,
		.Flags = StdUiPanelFlags_Invisible
	    }))
    {
	StdUiPanel* pPanel = std_ui_panel_get();
	pPanel->Position.XY = pos;

	StdUiDiv div = (StdUiDiv) {
	    .Position = { 0., 0. },
	    .Margin = { 0, 0 },
	};
	std_ui_div(div);

	// todo: validate on
	// DOCS: Can be delta or relative position
	state = std_ui_custom_gizmo(settings, pos, pOutputValue);
    }

    std_ui_panel_end();

    if (state == StdUiState_Grabbed)
    {
	isGrabbed = 1;
	newPosValue = *pOutputValue;

	return StdUiState_Grabbed;
    }

    StdUiInput* pInput = std_ui_get_input();
    ;
    if (isGrabbed && pInput->aKeys[StdUiKey_Mouse_Left_Button].State == StdUiKeyState_Released)
    {
	*pOutputValue = newPosValue;
	return StdUiState_Changed;
    }

    return state;
}
#endif // #if 0
