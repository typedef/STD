#ifndef STD_ATLAS_ANIMATION_H
#define STD_ATLAS_ANIMATION_H

#include <Std/StdTypes.h>


typedef struct StdAtlasAnimationSettings
{
    real SecondsForOneIter;
    StdAtlas Atlas;
} StdAtlasAnimationSettings;

typedef struct StdAtlasAnimation
{
    i32 CurrentItem;
    i32 MaxItemsCount;
    SimpleTimer Timer;
    StdAtlas Atlas;
} StdAtlasAnimation;

/*
  frame0 : CI: 0, MIC: 10
  frame60: CI: 1, MIC: 10


  i32 updateCount = simple_timer_interval(pTimerData, timestep);
  if (updateCount)
  {
      ++atlasAnim.CurrentItem;
      if (atlasAnim.CurrentItem > MaxItemsCount)
      {
          atlasAnim.CurrentItem = 0;
      }
  }

*/

StdAtlasAnimation std_atlas_animation_new(StdAtlasAnimationSettings set);
void std_atlas_animation_update(StdAtlasAnimation* pAnim, real timestep);

#endif // STD_ATLAS_ANIMATION_H
