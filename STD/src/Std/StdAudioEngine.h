#ifndef STD_AUDIO_ENGINE_H
#define STD_AUDIO_ENGINE_H

struct ma_engine;


void std_audio_engine_init();
void std_audio_engine_deinit();
struct ma_engine* std_audio_engine_get();

#endif // STD_AUDIO_ENGINE_H
