#include "StdAtlasAnimation.h"
#include <Core/SimpleStandardLibrary.h>


StdAtlasAnimation
std_atlas_animation_new(StdAtlasAnimationSettings set)
{
    if (set.SecondsForOneIter <= 0.0001)
    {
        set.SecondsForOneIter = 1.0;
    }

    StdAtlasAnimation atlasAnim = {
        .CurrentItem = 0,
        .MaxItemsCount = array_count(set.Atlas.aAtlasItems),
        .Timer = (SimpleTimer) {
            .WaitSeconds = set.SecondsForOneIter / array_count(set.Atlas.aAtlasItems),
        },
        .Atlas = set.Atlas
    };

    return atlasAnim;
}

void
std_atlas_animation_update(StdAtlasAnimation* pAnim, real timestep)
{
    i32 shouldUpdate = simple_timer_interval(&pAnim->Timer, timestep);

    if (shouldUpdate)
    {
        ++pAnim->CurrentItem;
        if (pAnim->CurrentItem >= pAnim->MaxItemsCount)
        {
            pAnim->CurrentItem = 0;
        }
    }
}
