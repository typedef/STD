#include "StdGeneratedTexture.h"
#include <Std/StdTypes.h>
#include <Core/SimpleMath.h>
#include <Core/SimpleStandardLibrary.h>


StdGeneratedTexture
std_generated_texture_new(const char* pName, v2 size, i32 channels, void* pData)
{
    StdGeneratedTexture stdGeneratedTexture = {
	.pName = simple_string_new(pName),
	.Size = size,
	.Channels = channels,
	.pData = pData,
    };

    return stdGeneratedTexture;
}

void
std_generated_texture_destroy(StdGeneratedTexture generatedTexture)
{
    memory_free(generatedTexture.pData);
    memory_free(generatedTexture.pName);
}

StdGeneratedTexture
std_generated_texture_create_ground()
{
    v2 size = v2_new(128, 128);
    i32 channels = 4;
    void* pData = memory_allocate(size.X * size.Y * channels);

    SimpleColor* pWrite = (SimpleColor*) pData;
    for (i32 y = 0; y < size.Y; ++y)
    {
	for (i32 x = 0; x < size.X; ++x)
	{
	    if (y < to_i32(size.Y/5))
	    {
		// DOCS: Green
		*pWrite = (SimpleColor) {62, 180, 62, 255};
	    }
	    else
	    {
		//*pWrite = SimpleColor_RGBA(120, 81, 67, 255);
		*pWrite = (SimpleColor) {120, 81, 67, 255};
	    }

	    ++pWrite;
	}
    }

    StdGeneratedTexture generatedTexture = std_generated_texture_new(
	    "GlGroundTexture", size, channels, pData);

    return generatedTexture;
}
