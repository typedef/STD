#include "StdInput.h"

#include <Core/SimpleApplication.h>
#include <Core/Types.h>
#include <Core/SimpleWindowLibrary.h>


static SwlWindow* pCurrentWindow;

void
std_input_init()
{
    pCurrentWindow = simple_application_get_window();
}

void
std_input_deinit()
{
    if (!pCurrentWindow)
	return;
}

i32
_std_input_is_key_pressed(SwlKey key)
{
#if defined(BUILD_DEBUG)
    vassert_not_null(pCurrentWindow);
#endif

    SwlAction action = pCurrentWindow->aKeys[key];
    return action == SwlAction_Press;
}

int
_std_input_is_mouse_key_pressed(int intKey)
{
    SwlMouseKey key = (SwlMouseKey) intKey;

#if defined(BUILD_DEBUG)
    vassert_not_null(pCurrentWindow);
#endif

    SwlAction action = pCurrentWindow->aMouseKeys[key];
    return action == SwlAction_Press;
}

int
_std_input_is_mouse_key_released(int intKey)
{
    SwlMouseKey key = (SwlMouseKey) intKey;

#if defined(BUILD_DEBUG)
    vassert_not_null(pCurrentWindow);
#endif

    SwlAction action = pCurrentWindow->aMouseKeys[key];
    return action == SwlAction_Release;
}
