#ifndef STD_RENDERER_H
#define STD_RENDERER_H

#include <Core/Types.h>
#include <Std/StdAtlas.h>


struct SvlTexture;
struct SvlInstance;
struct StdTexture;

typedef struct StdViewport
{
    i32 X0;
    i32 Y0;
    i32 Width;
    i32 Height;
} StdViewport;

typedef struct StdRendererSettings
{
    v2i WindowSize;
    void* pWindowBackend;
    i32 MaxItemsCount;
    i32 MaxImageCount;
    i32 MaxCharsCount;
    i8 IsDebug;
    i8 IsVsync;
} StdRendererSettings;

typedef struct StdFontSettings
{
    i8 IsUpdateFont;
    i8 IsForceAdd;
    const char* pFontPath;
    i32 FontSize;
} StdFontSettings;

// DOCS: Craetion/Deletion
void std_renderer_create(StdRendererSettings* pSettings);
void std_renderer_destroy();
void std_renderer_prepare_for_destruction();

// DOCS: set/get properties and resources
void std_renderer_set_clear_color(v4 color);
void std_renderer_set_camera(m4* pViewProjection);
void std_renderer_set_textures(struct StdTexture* aTextures, i32 texturesCount);
struct SvlInstance* std_renderer_get_instance();

// DOCS: Update
void std_renderer_update();
void std_renderer_window_resized(v2 newSize);

// DOCS: UTILS
void std_renderer_create_texture_ext(i32 width, i32 height, i32 channels, void* pData, void* pSvlTexture);
void std_renderer_destroy_texture(struct SvlTexture* pSvlTexture);

// DOCS: Drawing
void std_renderer_draw_rect_ext(v3 position, v2 size, v4 colors[4]);
void std_renderer_draw_rect(v3 position, v2 size, v4 color);
void std_renderer_draw_image(v3 position, v2 size, i64 assetId);
void std_renderer_draw_color_image(v3 position, v2 size, v4 color, i64 assetId);
void std_renderer_draw_color_image_uv(v3 position, v2 size, v4 color, i64 assetId, v2 aUvs[4]);

// DOCS: Ui Drawing
void std_renderer_set_ui_camera(m4* pViewProjection);
void std_renderer_set_ui_viewport(StdViewport viewport);
void std_renderer_set_ui_fonts();
void std_renderer_draw_ui_rect(v3 position, v2 size, v4 color);
void std_renderer_draw_ui_empty_rect(v3 position, v2 size, real thickness, v4 color);
void std_renderer_draw_ui_line(v3 start, v3 end, real thickness, v4 color);
void std_renderer_draw_ui_image(v3 position, v2 size, i64 assetId);
void std_renderer_draw_ui_image_ext(v3 position, v2 size, i64 assetId, v2 aUvs[4], v4 color);
void std_renderer_draw_ui_text(v3 position, v4 color, char* pText);
void std_renderer_draw_ui_text_w_fontsize(v3 position, v4 color, char* pText, i32 fontSize);
void std_renderer_draw_ui_text_ext(v3 pos, v4 color, i32* pText, i64 textLength, const char* pPath, i32 fontSize);

#endif // STD_RENDERER_H
