#ifndef STD_UI_H
#define STD_UI_H

//#include "Std/StdAssetManager.h"
#include <Core/Types.h>


// DOCS: Forward
struct SimpleFont;
typedef i64 StdUiId;


typedef enum StdUiState
{
    StdUiState_None    = 0     ,
    StdUiState_Hovered = 1 << 0,
    StdUiState_Clicked = 1 << 1,
    StdUiState_Grabbed = 1 << 2,
    StdUiState_Changed = 1 << 3,
} StdUiState;

static const char*
std_ui_state_to_string(StdUiState state)
{
    switch (state)
    {
    case StdUiState_None: return "None";

    case StdUiState_Hovered: return "Hovered";
    case StdUiState_Clicked: return "Clicked";
    case StdUiState_Grabbed: return "Grabbed";
    case StdUiState_Changed: return "Changed";

    default: "Undefined";
    }
}


/*     #####################################
  DOCS ##         Std Ui Custom           ##
       #####################################*/


typedef enum StdUiGeometryType
{
    StdUiGeometryType_None = 0,
    StdUiGeometryType_Rect,
    StdUiGeometryType_EmptyRect,
    StdUiGeometryType_Image,
    StdUiGeometryType_Image_AtlasItem,
    StdUiGeometryType_Text,
    StdUiGeometryType_Count,
} StdUiGeometryType;

typedef struct StdUiGeometry
{
    StdUiGeometryType GeometryType;
    StdUiId Id;
    char* pName;

    // DOCS: Position is relative to StdUiCustom
    v3 Position;
    v2 Size;
    i8 FontSize;

    // todo: set name for texture and atlas
    struct {
	i32 Index;
	i64 AssetId;
    } Image;

    // note: hover && click behaviour
    StdUiState State;

    union
    {
	char* pUtf8Text;
    } ExtraData;
} StdUiGeometry;

typedef struct StdUiCustomInteractive
{
    StdUiState State;
    i32 ActiveIndex;
} StdUiCustomInteractive;

#define GeometryCount 4

typedef struct StdUiCustom
{
    struct {
	i8 IsInUse;
	i8 IsDisabled;
	i8 ShouldUpdate;
	// todo: не уверен, что это лучший способ,
	// чтобы хендлить состояния hover && click :(
	i8 GeometryStateIndex;

	// todo: А это вообще спорный момент
	// нужно придумать эффективный механизм
	// модификации закешированного StdCustom
	i8 IsDynamicSize;
    } Flags;

    // DOCS: Unique name; Hash(pName) == Id
    char* pName;
    // DOCS: Unique id
    StdUiId Id;
    // DOCS: Base Layouting Part
    StdUiId LayoutId;
    struct {
	v3 Position;
	v2 Size;
    } Metrics;

    //i64 GeometriesCount;
    StdUiGeometry* aGeometries;//[GeometryCount];
} StdUiCustom;

typedef struct StdUiCustomCache
{
    StdUiCustom* aCustoms;
} StdUiCustomCache;

struct StdUiWidget;

typedef struct StdUiWidgetCache
{
    struct StdUiWidget* aWidgets;
} StdUiWidgetCache;

typedef struct StdUiCustomStyle
{
    i8 IsSingleColor;
    v4 Color;
    v4 HoveredColor;
    v4 ClickedColor;
    real Thickness;
} StdUiCustomStyle;

typedef struct StdUiCustomStorage
{
    struct { char* Key; StdUiCustomStyle Value; }* hRegisteredStyles;
} StdUiCustomStorage;



typedef struct StdUiDebug
{
    i8 ShouldDrawDivArea;
} StdUiDebug;

/*     #####################################
  DOCS ##         Std Ui Layout           ##
       #####################################*/

typedef enum StdUiStyleItemType
{
    StdUiStyleItemType_ = 0,

    StdUiStyleItemType_Panel,

    StdUiStyleItemType_Widget_Button,
    StdUiStyleItemType_Widget_Button_Text,

    StdUiStyleItemType_Count,
} StdUiStyleItemType;

typedef struct StdUiStyleColorItem
{
    union
    {
	v4 Background;
	v4 Foreground;
    };
    v4 Hovered;
    v4 Clicked;
} StdUiStyleColorItem;

typedef struct StdUiStyleSizeItem
{
    v2 Min;
    v2 Max;
    v2 Hovered;
    v2 Clicked;
} StdUiStyleSizeItem;

typedef struct StdUiStyleItem
{
    StdUiStyleColorItem Color;
    StdUiStyleSizeItem Size;
    v2 Margin;
} StdUiStyleItem;

typedef struct StdUiStyle
{
    // todo: fix this garbage name
    StdUiStyleItem Items[StdUiStyleItemType_Count];
} StdUiStyle;


/*     #####################################
  DOCS ##         Std Ui Layout NEW       ##
       #####################################*/

typedef struct StdUiStyleColor
{
    v4 Idle;
    v4 Hovered;
    v4 Clicked;
} StdUiStyleColor;

// note: for_each shape
typedef struct StdUiShapeStyle
{
    char* pName;
    StdUiStyleColor Color;
    // no positioning. no transformation
} StdUiShapeStyle;

typedef struct StdUiStyleCache
{
    struct {char* Key; StdUiShapeStyle Value;}* hWidgetStyle;
} StdUiStyleCache;

void std_ui_style_add(char* pStyleName, StdUiShapeStyle shapeStyle);
StdUiShapeStyle std_ui_style_get(char* pStyleName);

typedef struct StdUiPiv
{
    /* DOCS: Relative to the screen */
    v3 Position;
    /* DOCS: Relative to the screen */
    v2 Size;
} StdUiPiv;

void std_ui_piv(StdUiPiv piv);

/*     #####################################
  DOCS ##      END Std Ui Layout NEW      ##
       #####################################*/


typedef enum StdUiKey
{
    StdUiKey_Undefined = 0,

    StdUiKey_Mouse_Left_Button,
    StdUiKey_Mouse_Right_Button,
    StdUiKey_Mouse_Middle_Button,

    StdUiKey_Tab,
    StdUiKey_Backspace,

    StdUiKey_LeftArrow,
    StdUiKey_RightArrow,
    StdUiKey_UpArrow,
    StdUiKey_DownArrow,
    StdUiKey_Home,
    StdUiKey_End,

    StdUiKey_Delete,


    StdUiKey_Count,
} StdUiKey;

typedef enum StdUiKeyState
{
    StdUiKeyState_None = 0,
    StdUiKeyState_Pressed,
    StdUiKeyState_Released,
} StdUiKeyState;

typedef struct StdUiKeyData
{
    i8 IsHandled;
    StdUiKeyState State;
} StdUiKeyData;

typedef struct StdUiInput
{
    v2 MousePosition;
    v2 PrevMousePosition;
    v2 GrabPosition;
    v2 GrabMousePosition;
    i8 MousePositionChanged;
    i8 LeftKeyWasPressed;
    i32 CharTyped;

    f32 ScrollY;

    i8 IsInputAlreadyHandled;
    i8 IsInGrabState;

    /* SuiWidgetId LastFocusedID; */
    //SuiWidgetId FocusID;
    /* i32 TextIndexPosition; */
    /* SuiModeType Mode; */

    StdUiKeyData aKeys[StdUiKey_Count];
} StdUiInput;

// DOCS: Panel

typedef enum StdUiPanelFlags
{
    StdUiPanelFlags_None          = 1 << 0,
    StdUiPanelFlags_Invisible     = 1 << 1,

} StdUiPanelFlags;

typedef struct StdUiPanelSettings
{
    char* pUtfLabel;
    StdUiPanelFlags Flags;
} StdUiPanelSettings;

typedef enum StdUiPanelState
{
    StdUiPanelState_None = 0,
    StdUiPanelState_Opened,
    StdUiPanelState_Closed,
    StdUiPanelState_Collapsed,
} StdUiPanelState;

// Layout

/*
  sameline, sameline+margin
  newline, newline+margin, newline_stretch_ForPanel, newline_stretch_ForMaxWidget

*/

// todo: refactor this
typedef enum StdUiDivDisplay
{
    // docs: DONE (at least i think so)
    StdUiDivDisplay_NewLine  = 1 << 0,
    // WIP bug
    StdUiDivDisplay_NewLine_Stretch_ForMaxWidget = 1 << 2 | StdUiDivDisplay_NewLine,

    // todo:
    StdUiDivDisplay_SameLine = 1 << 1,

    // todo: wip


    // todo:
    StdUiDivDisplay_RelativeToPanel,
} StdUiDivDisplay;

typedef struct StdUiLayoutBox
{
    i32 Id;
    // DOCS: Ref to font storage
    i32 FontId;

    v3 Position;
    v2 Size;
    // DOCS: Offset from .NextPosition
    v2 Margin;
    // DOCS: Padding for widgets inside box
    v2 Padding;

    StdUiDivDisplay Display;
} StdUiLayoutBox;

typedef enum StdUiMode
{
    StdUiMode_NewLine = 0,
    StdUiMode_SameLine,
} StdUiMode;

typedef struct StdUiDiv
{
    i64 Id;

    /* DOCS: Position and Size is relative to panel */
    v3 Position;
    v2 Size;

    /* DOCS: Don't set.
       Position and Size already calculated in pixels. */
    struct
    {
	v3 Position;
	v2 Size;
    } Absolute;

    StdUiMode Mode;

    // DOCS: Relative values 0.1 = 10%, 1.0 = 100%
    v2 WidgetSize;

    /* DOCS: Also relative
       Sets offset for elements
     */
    v2 Padding;

    i32 FontSize;
    // DOCS: -1 means no border at all, otherwise id of the border in a style table
} StdUiDiv;

typedef enum StdUiAnimationRectType
{
    StdUiAnimationRectType_LeftRight = 0,
    StdUiAnimationRectType_CenterToEdge,
} StdUiAnimationRectType;

typedef struct StdUiAnimationRect
{
    i64 Id;
    real Percent;
    v2 Start;
    v2 End;
    v2 AnimationPos;
    v2 AnimationSize;
    StdUiAnimationRectType Type;
    SimpleTimer Timer;
} StdUiAnimationRect;

typedef struct StdUiAnimationCache
{
    StdUiAnimationRect* aAnimationRects;
} StdUiAnimationCache;

typedef struct StdUiPanel
{
    //StdUiId Id;
    // DOCS: Label - id
    char* pLabel;
    StdUiPanelState PanelState;
    StdUiPanelFlags Flags;

    StdUiState State;

    v3 Position;
    v2 Size;

    // note: removed, use only CustomCache.aCustoms
    //StdUiWidget* aWidgets;
    // StdUiLayout Layout;
    // DOCS: Layout
    i32 CurrentDiv;
    StdUiDiv* aDivs;

    // DOCS: Animation
    StdUiAnimationCache AnimationCache;

    // DOCS: Custom ui elements, like HUD
    StdUiCustomCache CustomCache;
    // DOCS: Widgets, stored in cache
    StdUiWidgetCache WidgetCache;
} StdUiPanel;

typedef struct StdUiCache
{
    char* pActivePanelLabel;
    StdUiId FocusPanelInd;
    i32 CurrentPanelInd;
    // DOCS: It's panel indices from PanelsTable
    // todo: rename to aPanels
    i32* Panels;

    // DOCS: We store all panels Here as { Label, SuiPanel }
    struct {const char* Key; StdUiPanel Value;}* hPanelsTable;
} StdUiCache;

typedef struct StdUiInteractive
{
    i64 ActiveId;
    i64 HotId;

    StdUiId ActivePanelId;
    StdUiId HotPanelId;

    i8 IsLeftKeyWasPressed;
} StdUiInteractive;

typedef struct StdUiDisplay
{
    v2 Size;
} StdUiDisplay;

typedef struct StdUiSettings
{
    v2 DisplaySize;
} StdUiSettings;


/*     #####################################
  DOCS ##          Std Ui Base            ##
       #####################################*/

void std_ui_create(StdUiSettings set);
void std_ui_destroy();
void std_ui_frame_begin();
void std_ui_frame_end();

i32 std_ui_is_key_released(StdUiKey key);
i32 std_ui_is_key_pressed(StdUiKey key);

StdUiStyle* std_ui_get_style();
StdUiInput* std_ui_get_input();
StdUiDisplay* std_ui_get_display();


/*     #####################################
  DOCS ##          Std Ui Debug           ##
       #####################################*/

void std_ui_debug(StdUiDebug* pDebug);
void std_ui_debug_validation();


/*     #####################################
  DOCS ##        Std Ui Animation         ##
       #####################################*/

void std_ui_reset_animate_rectangle(char* pName);
void std_ui_animate_rectangle(StdUiAnimationRect rect, v2* pSize, v2* pPosition);


/*     #####################################
  DOCS ##          Std Ui Panel           ##
       #####################################*/

StdUiPanel* std_ui_panel_get();
StdUiPanel* std_ui_pagel_get_by_id(i32 i);
i32 std_ui_panel_begin(StdUiPanelSettings set);
void std_ui_panel_end();


/*     #####################################
  DOCS ##          Std Ui Layout         ##
       #####################################*/

i64 std_ui_div(StdUiDiv div);
StdUiDiv std_ui_div_get(StdUiId id);


/*     #####################################
  DOCS ##         Std Ui Widgets          ##
       #####################################*/

/*
  ```WidgetType``` like a structure.
  Widget is instance of ```struct WidgetType```.
  StdUiWidgetTypeId == i64, just integer.
*/

typedef enum StdUiShapeType
{
    StdUiShapeType_None = 0,
    StdUiShapeType_Rect,
    StdUiShapeType_EmptyRect,
    StdUiShapeType_Text,
    StdUiShapeType_Image,
    StdUiShapeType_AtlasImage,

    StdUiShapeType_Count,
} StdUiShapeType;

typedef enum StdUiTextAlign
{
    // DOCS: Horizontal and vertical
    StdUiTextAlign_Center = 0,
    StdUiTextAlign_Custom,
} StdUiTextAlign;

// note: Shape > Geometry
typedef struct StdUiShape
{
    StdUiShapeType Type;
    v3 Position;//Offset;
    v2 Size;
    StdUiTextAlign Align;
    char* pStyleName;
    // DOCS: Default is 1.0, means 100% of default Div.FontSize
    f32 FontSizeRatio;
} StdUiShape;

typedef struct StdUiWidgetType
{
    StdUiId Id;
    StdUiShape* aShapes;

    struct {
	v2 DiffSize;
	v2 Min;
	v2 Max;
    } Metrics;

    //note: do we need this???
    char* pName;
} StdUiWidgetType;

// note: Config > Settings
typedef struct StdUiWidgetTypeConfig
{
    StdUiShape* aShapes;
    i64 ShapesCount;
    const char* pName;
} StdUiWidgetTypeConfig;

StdUiWidgetType std_ui_widget_type_new(StdUiWidgetTypeConfig config);
StdUiWidgetType* std_ui_get_widget_type_by_id(i64 typeId);

typedef union StdUiWidgetData
{
    char* pText;
} StdUiWidgetData;

typedef struct StdUiWidgetConfig
{
    i64 TypeId;
    const char* pName;
    i64 AssetId;
    i64 ItemId;
} StdUiWidgetConfig;

typedef enum StdUiWidgetFlags
{
    StdUiWidgetFlags_Dead = 0,
    StdUiWidgetFlags_Alive,
    StdUiWidgetFlags_Disabled,
} StdUiWidgetFlags;

typedef struct StdUiWidget
{
    i64 Id;
    i64 TypeId;
    StdUiWidgetFlags Flags;
    char* pName;

    i64 AssetId;
    i64 ItemId;

    // WIP:
    StdUiState State;
    i8 ShapeStateInd;

    // DOCS/WIP: Base Layouting Part
    v3 Position;
    v2 Size;
    StdUiId LayoutId;

    // WIP:
    StdUiWidgetData Data;
} StdUiWidget;

StdUiState std_ui_widget(StdUiWidgetConfig config);

void std_ui_text_middle();

typedef struct StdUiWidgetStorage
{
    // todo: add style hash table
    StdUiWidgetType* aWidgetTypes;
} StdUiWidgetStorage;

void std_ui_process_input();

// todo: StdUiWidget.h


/*     #####################################
  DOCS ##          Std Ui Utils           ##
       #####################################*/

v2 std_ui_get_text_metrics(char* pText, i32 fontSize);


/*     #####################################
  DOCS ##         Std Ui Helper           ##
       #####################################*/

char* std_ui_new_string(const char* pString);


/*     #####################################
  DOCS ##         Std Ui Backend          ##
       #####################################*/

typedef struct StdUiFuncs
{
    void (*DrawRect)(v3 position, v2 size, v4 color);
    void (*DrawEmptyRect)(v3 position, v2 size, real thickness, v4 color);
    void (*DrawImage)(v3 position, v2 size, v4 color, i64 textureId);
    void (*DrawImageAtlasItem)(v3 position, v2 size, v4 color, i64 textureId, i32 index);
    void (*DrawText)(char* pText, i32 fontSize, v3 position, v4 glyphColor);
    void (*DrawItems)(v3* positions, v2* uvs, u64 itemsCount, u64 indicesCount, i32 isFont, v4 color, i32 textureId);
    void (*DrawCircle)(v3 position, v2 size, v4 color, f32 thikness, f32 fade, i32 multiplier);
    void (*DrawLine)(v3 start, v3 end, f32 thickness, v4 color);

    void (*SetViewport)(i32 x0, i32 y0, i32 width, i32 height);

    v2 (*GetTextMetrics)(char* pText, i32 fontSize, char* pFontPath);

    void (*Flush)();

    void (*SetVisibleRect)(v2 min, v2 max);

    // DOCS: Sound-related
    void (*PlayClickSound)();
    void (*PlayHoverSound)();
} StdUiFuncs;

StdUiFuncs std_ui_backend_get_funcs();

typedef struct StdUiDebugConfig
{
    i8 IsButtonDrawn;
    i8 IsTextDrawn;
    i8 IsCheckboxDrawn;
    i8 IsSliderDrawn;
    i8 IsImageDrawn;
    i8 IsFontDebug;

    i8 IsDivDebugMode;
} StdUiDebugConfig;

typedef struct StdUiBackendSettings
{
    StdUiFuncs Funcs;
    StdUiDebugConfig DebugConfig;
} StdUiBackendSettings;

void std_ui_backend_new(StdUiBackendSettings set);
void std_ui_backend_draw();
void std_ui_backend_sound();

/*
  DOCS: Debug utils
*/

void std_ui_backend_text_visual(char* pName, v3 pos, i32 fontSize, void (*DrawRect)(v3 pos, v2 size, v4 color));

#endif // STD_UI_H
