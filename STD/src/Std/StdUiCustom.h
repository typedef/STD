#ifndef STD_UI_CUSTOM_H
#define STD_UI_CUSTOM_H


#include "StdUi.h"


/*     #####################################
  DOCS ##      Std Ui Custom Widgets      ##
       #####################################*/

void std_ui_custom_init();

typedef enum StdUiGizmoFlags
{
    StdUiGizmoFlags_None                  = 0     ,
    StdUiGizmoFlags_DeltaValue            = 1 << 0,
    StdUiGizmoFlags_RelativePositionValue = 1 << 1,
    StdUiGizmoFlags_X                     = 1 << 2,
    StdUiGizmoFlags_Y                     = 1 << 3,
    StdUiGizmoFlags_XY                    = 1 << 4,
    StdUiGizmoFlags_Delta                 = StdUiGizmoFlags_DeltaValue | StdUiGizmoFlags_X | StdUiGizmoFlags_Y | StdUiGizmoFlags_XY,
    StdUiGizmoFlags_Position              = StdUiGizmoFlags_RelativePositionValue | StdUiGizmoFlags_X | StdUiGizmoFlags_Y | StdUiGizmoFlags_XY,
} StdUiGizmoFlags;

typedef struct StdUiGizmoSettings
{
    char* pLabel;
    StdUiGizmoFlags Flags;
} StdUiGizmoSettings;

StdUiState std_ui_gizmo(StdUiGizmoSettings settings, v2* pOutputValue);

#endif // STD_UI_CUSTOM_H
