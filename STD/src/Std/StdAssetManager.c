#include "StdAssetManager.h"
#include "Std/StdAtlas.h"
#include <Core/SimpleAtlasFont.h>
#include <Core/SimpleStandardLibrary.h>
#include <Core/SimpleVulkanLibrary.h>
#include <Std/StdRenderer.h>

#include <Deps/stb_image.h>


typedef struct ImageStorage
{
    // u32* aHandles; // note: this not visible to user-space
    // u32* aVulkanHandles; // note: this not visible to user-space
    StdTexture* aTextures;

    /*
      DOCS:
      Key - path_to_source_image
      Value - asset_id
    */
    struct {const char* Key; i64 Value;}* hRegistrationTable;
} ImageStorage;


typedef struct TextStorage
{
    StdFont* aFonts;
} TextStorage;

typedef struct AtlasStorage
{
    StdAtlas* aAtlases;
    struct {const char* Key; i64 Value;}* hRegistrationTable;
} AtlasStorage;

typedef struct StdAssetManager
{
    ImageStorage Image;
    TextStorage Text;
    AtlasStorage Atlas;
} StdAssetManager;

static StdAssetManager gStdAssetManager = {};


/*     #####################################
  DOCS ##        Forward Declaration      ##
       #####################################*/

ImageStorage image_storage_create();
void image_storage_destroy(ImageStorage* pStorage);

TextStorage text_storage_create();
void text_storage_destroy(TextStorage* pStorage);

AtlasStorage atlas_storage_create();
void atlas_storage_destroy(AtlasStorage* pStorage);


/*     #####################################
  DOCS ##        Std Asset Manager        ##
       #####################################*/

void
std_asset_manager_create()
{
    gStdAssetManager.Image = image_storage_create();
    gStdAssetManager.Text = text_storage_create();
    gStdAssetManager.Atlas = atlas_storage_create();

    // load default_font
    // check for existance default_font

    const char* pFontPath = resource_font("MartianMono-sWdRg.ttf");

    if (!path_is_file_exist(pFontPath))
    {
	slog_error("No default font in directory %s\n", path_get_directory(pFontPath));
	vguard(0);
	return;
    }

    //std_asset_manager_load_font(pFontPath, );
}

void
std_asset_manager_destroy()
{
    image_storage_destroy(&gStdAssetManager.Image);
    text_storage_destroy(&gStdAssetManager.Text);
    atlas_storage_destroy(&gStdAssetManager.Atlas);
}


/*     #####################################
  DOCS ##            Textures             ##
       #####################################*/

i64
std_asset_manager_load_image(const char* pPath)
{
    i64 ind = shash_geti(gStdAssetManager.Image.hRegistrationTable, pPath);
    if (ind == -1)
    {
	i64 textureId = sva_count(gStdAssetManager.Image.aTextures) + 1;

	i32 w, h, c, d = 4;
	void* pData = stbi_load(pPath, &w, &h, &c, d);
	if (!pData)
	{
	    printf("Texture is not found %s\n", pPath);
	    vassert_break();
	}

	SvlTexture image;
	std_renderer_create_texture_ext(w, h, c, pData, (void*)&image);

	StdTexture stdTexture = {
	    .Id = textureId,
	    .pName = simple_string_new(pPath),
	    .Size = (v2) { w, h },
	    .Channels = c,
	    .Handle = image
	};

	sva_add(gStdAssetManager.Image.aTextures, stdTexture);
	shash_put(gStdAssetManager.Image.hRegistrationTable, pPath, textureId);

	ind = table_index(gStdAssetManager.Image.hRegistrationTable);
    }

    i64 id = gStdAssetManager.Image.hRegistrationTable[ind].Value;
    return id;
}

i64
std_asset_manager_load_raw_image(const char* pPath, v2 size, i32 channels, void* pData)
{
    i64 ind = shash_geti(gStdAssetManager.Image.hRegistrationTable, pPath);
    if (ind == -1)
    {
	i64 textureId = sva_count(gStdAssetManager.Image.aTextures);

	SvlTexture image;
	std_renderer_create_texture_ext(size.X, size.Y, channels, pData, (void*)&image);

	StdTexture stdTexture = {
	    .Id = textureId,
	    .pName = simple_string_new(pPath),
	    .Size = size,
	    .Channels = channels,
	    .Handle = image
	};

	sva_add(gStdAssetManager.Image.aTextures, stdTexture);
	shash_put(gStdAssetManager.Image.hRegistrationTable, pPath, textureId);

	ind = table_index(gStdAssetManager.Image.hRegistrationTable);
    }

    i64 id = gStdAssetManager.Image.hRegistrationTable[ind].Value;
    return id;
}

void
std_asset_manager_get_image(i64 id, void* pTexture)
{
    --id;
    StdTexture* aImages = gStdAssetManager.Image.aTextures;

    if (id >= 0 && id < sva_count(aImages))
	*((StdTexture*)pTexture) = aImages[id];
    else
	pTexture = NULL;
}

StdTexture*
std_asset_manager_get_all_images()
{
    return gStdAssetManager.Image.aTextures;
}


/*     #####################################
  DOCS ##              Atlas              ##
       #####################################*/

StdAtlas
std_asset_manager_load_atlas(const char* pPath)
{
    i64 ind = shash_geti(gStdAssetManager.Atlas.hRegistrationTable, pPath);
    if (ind == -1)
    {
	i64 atlasId = sva_count(gStdAssetManager.Atlas.aAtlases);

	StdAtlas atlas = std_atlas_new(pPath);
	atlas.Id = atlasId;

	sva_add(gStdAssetManager.Atlas.aAtlases, atlas);
	shash_put(gStdAssetManager.Atlas.hRegistrationTable, pPath, atlasId);

	ind = table_index(gStdAssetManager.Atlas.hRegistrationTable);
    }

    i64 id = gStdAssetManager.Atlas.hRegistrationTable[ind].Value;

    return std_asset_manager_get_atlas(id);
}

StdAtlas
std_asset_manager_get_atlas(i64 id)
{
    if (id < 0 || id >= sva_count(gStdAssetManager.Atlas.aAtlases))
    {
	slog_error("Wrong id(%ld) for atlas!\n", id);
	return (StdAtlas) {.pName=NULL};
    }

    return gStdAssetManager.Atlas.aAtlases[id];
}


/*     #####################################
  DOCS ##             Fonts               ##
       #####################################*/

StdFont*
_std_am_get_font(const char* pPath, i32 fontSize, i32 rangeMin, i32 rangeMax)
{
    i32 fontsCount = sva_count(gStdAssetManager.Text.aFonts);
    for (i64 i = 0; i < fontsCount; ++i)
    {
	StdFont* pFont = &gStdAssetManager.Text.aFonts[i];
	if (pFont->Handle.RangeMin == rangeMin &&
	    pFont->Handle.RangeMax == rangeMax &&
	    pFont->Size == fontSize &&
	    string_compare(pFont->pPath, pPath))
	{
	    return pFont;
	}
    }

    return NULL;
}

i64
std_asset_manager_load_font(const char* pFontPath, i32 fontSize, i32 rangeMin, i32 rangeMax)
{
    StdFont* pFont = _std_am_get_font(pFontPath, fontSize, rangeMin, rangeMax);
    if (pFont != NULL)
	return pFont->Id;

    // DOCS: Load new font
    SafFontSettings fontSet = {
	.pPath = pFontPath,
	.FontSize = fontSize,

	.RangeMin = rangeMin,
	.RangeMax = rangeMax,
    };
    SafFont simpleFont = saf_font_create(fontSet);

    i32 w, h, c, d = 4;
    void* pData = stbi_load(simpleFont.pAtlasPath, &w, &h, &c, 4);
    vguard(c == 4);

    SvlTexture atlasTexture = {};
    std_renderer_create_texture_ext(w, h, c, pData, (void*)&atlasTexture);

    StdFont font = (StdFont) {
	.Size = fontSize,
	.pPath = simple_string_new(pFontPath),
	.Handle = simpleFont,
	.Atlas = atlasTexture,
    };

    StdFontId id = sva_count(gStdAssetManager.Text.aFonts);
    font.Id = id;
    sva_add(gStdAssetManager.Text.aFonts, font);

    return id;
}

StdFont*
std_asset_manager_get_font(i64 id)
{
    i64 fontsCount = sva_count(gStdAssetManager.Text.aFonts);
    if (id >= 0 && id < fontsCount)
	return &gStdAssetManager.Text.aFonts[id];
    return NULL;
}

StdFont*
std_asset_manager_get_all_fonts()
{
    return gStdAssetManager.Text.aFonts;
}

void
std_asset_manager_load_default_font_for_all_sizes()
{
    const char* pDefaultFontPath = resource_font("MartianMono-sWdRg.ttf");

    i32 i, count = sva_count(gStdAssetManager.Text.aFonts);
    for (i = 0; i < count; ++i)
    {
	StdFont font = gStdAssetManager.Text.aFonts[i];
	if (string_compare(font.pPath, pDefaultFontPath))
	    continue;

	std_asset_manager_load_font(pDefaultFontPath, font.Size, 0, 127);
    }
}


/*     #####################################
  DOCS ##        Forward-declared         ##
       #####################################*/

ImageStorage
image_storage_create()
{
    ImageStorage storage = {};
    return storage;
}

void
image_storage_destroy(ImageStorage* pStorage)
{
    for (i64 i = 0; i < sva_count(pStorage->aTextures); ++i)
    {
	StdTexture texture = pStorage->aTextures[i];
	std_renderer_destroy_texture(&texture.Handle);
    }

    sva_free(pStorage->aTextures);
    table_free(pStorage->hRegistrationTable);
}

TextStorage
text_storage_create()
{
    TextStorage storage = {};
    return storage;
}

void
text_storage_destroy(TextStorage* pStorage)
{
    i64 fontsCount = sva_count(pStorage->aFonts);
    for (i64 i = 0; i < fontsCount; ++i)
    {
	StdFont stdFont = pStorage->aFonts[i];
	simple_string_destroy(stdFont.pPath);
	saf_font_destroy(stdFont.Handle);
	std_renderer_destroy_texture(&stdFont.Atlas);
    }

    sva_free(pStorage->aFonts);
}

AtlasStorage
atlas_storage_create()
{
    AtlasStorage storage = {};
    return storage;
}

void
atlas_storage_destroy(AtlasStorage* pStorage)
{
    // todo:
    //StdAtlas* aAtlases;
    //struct {const char* Key; i64 Value;}* hRegistrationTable;

    sva_free(pStorage->aAtlases);
    table_free(pStorage->hRegistrationTable);
}
