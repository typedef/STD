#ifndef STD_ASSET_MANAGER_H
#define STD_ASSET_MANAGER_H

#include <Std/StdTypes.h>


/*     #####################################
  DOCS ##        Std Asset Manager        ##
       #####################################*/

void std_asset_manager_create();
void std_asset_manager_destroy();


/*     #####################################
  DOCS ##             Shorts              ##
       #####################################*/

#define std_am_load_image(p)			\
    ({ std_asset_manager_load_image(p); })
#define std_am_get_image(i,p)			\
    ({ std_asset_manager_get_image(i,p); })


/*     #####################################
  DOCS ##        Textures/Images          ##
       #####################################*/
// note: texture's ids starts with id == 1

i64 std_asset_manager_load_image(const char* pPath);
i64 std_asset_manager_load_raw_image(const char* pKey, v2 size, i32 channels, void* pData);
void std_asset_manager_get_image(i64 id, void* pTexture);
StdTexture* std_asset_manager_get_all_images();


/*     #####################################
  DOCS ##              Atlas              ##
       #####################################*/

StdAtlas std_asset_manager_load_atlas(const char* pPath);
StdAtlas std_asset_manager_get_atlas(i64 id);


/*     #####################################
  DOCS ##             Fonts               ##
       #####################################*/

i64 std_asset_manager_load_font(const char* pPath, i32 fontSize, i32 rangeMin, i32 rangeMax);
StdFont* std_asset_manager_get_font(i64 id);
StdFont* std_asset_manager_get_all_fonts();
void std_asset_manager_load_default_font_for_all_sizes();

#endif // STD_ASSET_MANAGER_H
