#include "StdEditor.h"
#include <Core/SimpleApplication.h>
#include <Core/SimpleWindowLibrary.h>


typedef struct StdEditor
{
    i64 SelectedIndex;
} StdEditor;

static StdEditor gEditor = {};


void
std_editor_attach()
{
    gEditor.SelectedIndex = -1;
}

void
std_editor_destroy()
{

}

void
std_editor_update()
{

}


void
std_editor_ui()
{

}

void
std_editor_event(SwlEvent* pEvent)
{

}

void
std_editor_register()
{
    SimpleLayer layer = {
	.OnAttach = std_editor_attach,
	.OnDestroy = std_editor_destroy,
	.OnUpdate = std_editor_update,
	.OnUi = std_editor_ui,
	.OnEvent = std_editor_event,
    };

    simple_application_add_layer(layer);
}
