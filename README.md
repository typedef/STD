# STD

STD - simple two dimensional game engine. Writtern in C99.

Starting date of development 03.05.2024.

todo:
* fix ui multiple panels bug
* Editor

todo (WINDOWS):
* Windows support for swl


feature list:
* New base layer for applications
* SpriteAnimation's
* Simple Standard Library (SimpleString type based on utf-8, with convertion from/to wchar*)
* Simple Window Library (Xlib, xcb:wip, win64 api:wip)
* Vulkan Renderer (rectangles, textures, utf8 text rendering)

std ui:
* panels
* widgets
* removed: custom ui elements (already built gizmo using this tech + demo examples)
