configurations
{
  "Debug",
  "Release",
  "Dist"
}

outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

project "Chipmunk2D"
    cdialect "C99"
    kind "StaticLib"
    language "C"
    staticruntime "on"
    targetdir ("bin/" .. outputdir .. "/Chipmunk2D")
    objdir ("bin/Intermidiates/" .. outputdir .. "/Chipmunk2D")
    architecture "x64"
    
    files
    {
       "src/**.h",
       "src/**.c",
    }

    defines
    {
    }

    includedirs
    {
       "src",
       "src/include",
    }

    filter "system:linux"
      defines { "PLATFORM_LINUX" }
      links
      {
         "m",
      }

    filter "system:windows"
      defines { "PLATFORM_WINDOWS", "_CRT_SECURE_NO_WARNINGS" }

    filter "configurations:Debug"
      defines { "ENGINE_DEBUG" }
      symbols "On"

    filter "configurations:Release"
      defines { "ENGINE_RELEASE" }
      optimize "Speed"
