configurations
{
  "Debug",
  "Release",
  "Dist"
}

outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

project "MiniAudio"
    cdialect "C99"
    kind "StaticLib"
    language "C"
    staticruntime "on"
    targetdir ("bin/" .. outputdir .. "/MiniAudio")
    objdir ("bin/Intermidiates/" .. outputdir .. "/MiniAudio")
    architecture "x64"

    files
    {
      "src/Deps/miniaudio.c"
    }

    filter "configurations:Debug"
      symbols "On"

    filter "configurations:Release"
      buildoptions { "-O3" }
      optimize "Speed"
