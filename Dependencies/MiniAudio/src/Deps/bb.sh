#!/bin/bash

startTime=$(date +%s%3N)
clang -c -o mini.o miniaudio.c

endTime=$(date +%s%3N)
diffTime=$(( $endTime - $startTime ))
echo "[Builds $1 in $diffTime ms]"
