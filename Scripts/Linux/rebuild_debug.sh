#!/bin/bash

startRebuildTime=$(date +%s%3N)

./clean_main_projects.sh
./generate_premake.sh $1 $2
./generate_project.sh $2
./build_debug.sh $1 $1

endRebuildTime=$(date +%s%3N)
diffRebuildTime=$(( $endRebuildTime - $startRebuildTime ))
echo "Rebuild $1 takes $diffRebuildTime ms"
