#!/bin/bash

## go to dbs and run deploy script
cd ../../assets/databases/
sqlite3 Localization ".read Localization.sql"

## returning to engine root directory
cd ../../../
