#!/bin/bash

cd ../..

startTime=$(date +%s%3N)

# for linker debugging: ld -lvulkan2 -verbose
# for linker LD_DEBUG=libs include before make (https://man7.org/linux/man-pages/man8/ld.so.8.html)
bear --append -- make config=debug -j16
#make config=debug -j16
buildResult=$?
if [ $buildResult -eq 0 ]
then
    echo ""
    echo "[Build was successful!]"
    echo ""
else
    echo ""
    echo "[Failed to build project!]"
    echo ""
fi
endTime=$(date +%s%3N)
diffTime=$(( $endTime - $startTime ))

if [ $buildResult -eq 0 ]
then
    echo ""
    echo "[Builds $1 in $diffTime ms]"
    echo ""
    echo ""
    echo "[Run project]"
    bin/Debug-linux-x86_64/$2/$2
fi
