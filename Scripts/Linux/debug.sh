#!/bin/bash

cd ../../

cp -R assets $2/
cp -R resources $2/

premake5 --file=$1 --cc=clang codelite
codelite STD.workspace

#kdevelop -d gdb bin/Debug-linux-x86_64/$1/$1
