#!/bin/bash

echo "Cleaning dependencies .."

cd ../..

# read Dependencies
dependenciesDirs[0]="stb"
dependenciesDirs[1]="MiniAudio"

function file_delete_if_exist() {
    if [ -e $1 ]; then
	rm $1
    fi
}

function dir_delete_if_exist() {
    if [[ -d $1 ]]; then
	rm -rf $1
    fi
}

for dependencyDir in "Dependencies"/*
do
    dir="${dependencyDir}/"
    echo $dir
    dir_delete_if_exist "$dir/bin"
    file_delete_if_exist "$dir/${dependencyDir}.mk"
    file_delete_if_exist "$dir/${dependencyDir}.project"
    file_delete_if_exist "$dir/${dependencyDir}.txt"
    file_delete_if_exist "$dir/compile_flags.txt"
    file_delete_if_exist "$dir/Makefile"
done
