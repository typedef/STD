#!/bin/tcc -run
#include <stdio.h>
#include <sys/stat.h>

int
main(int argc, char** argv)
{
    if (argc != 3)
    {
	printf("Usage: compare_file_modification_time \"path/to/first/file_or_dir\" \"path/to/second/file_or_dir\"\n");
    }

    struct stat fileItemInfo;
    stat(argv[1], &fileItemInfo);
    size_t modificationTime0 = (size_t)fileItemInfo.st_mtime;

    int result1 = stat(argv[2], &fileItemInfo);
    if (result1 == -1)
    {
	printf("1\n");
	return 0;
    }
    size_t modificationTime1 = (size_t)fileItemInfo.st_mtime;

    if (modificationTime0 > modificationTime1)
    {
	printf("1\n");
    }
    else if (modificationTime0 < modificationTime1)
    {
	printf("-1\n");
    }
    else
    {
	printf("0\n");
    }

    return 0;
}
