#!/bin/bash

echo "Cleaning main projects .."

cd ../..

files[0]="Makefile"
files[1]="Engine.workspace"
files[2]="tags"
files[3]="TAGS"

mainDirs[0]="Engine"

projectsDirs[0]="SandboxApp"
projectsDirs[1]="Helma"
projectsDirs[2]="ModelSandbox"
projectsDirs[3]="EcsSandbox"
projectsDirs[4]="UiSandbox"
projectsDirs[5]="VulkanSandbox"

binDirs[0]="bin"
binDirs[1]="bin-int"
binDirs[2]=".codelite"
binDirs[3]=".build-debug"

function file_delete_if_exist() {
    if [ -e $1 ]; then
	rm $1
    fi
}

function dir_delete_if_exist() {
    if [[ -d $1 ]]; then
	rm -rf $1
    fi
}

for file in ${files[*]}
do
    file_delete_if_exist $file
done

for projectDir in ${mainDirs[*]}
do
    echo "Clean project dir ${projectDir}"

    #if [[ -d $projectDir && -e "${projectDir}/Makefile" ]]; then
    #	rm -rf "${projectDir}/"
    #fi

    dir_delete_if_exist "Dependencies/${projectDir}/bin"
    dir_delete_if_exist "Dependencies/${projectDir}/bin-int"
    file_delete_if_exist "Dependencies/${projectDir}/Makefile"

    dir_delete_if_exist "${projectDir}/assets"
    dir_delete_if_exist "${projectDir}/resources"

    file_delete_if_exist "${projectDir}/${projectDir}.mk"
    file_delete_if_exist "${projectDir}/${projectDir}.project"
    file_delete_if_exist "${projectDir}/${projectDir}.txt"
    file_delete_if_exist "${projectDir}/imgui.ini"
    file_delete_if_exist "${projectDir}/compile_flags.txt"
    file_delete_if_exist "${projectDir}/Makefile"

done

pwd
for projectDir in "Projects"/*
do
    echo "Clean ${projectDir}"

    #if [[ -d $projectDir && -e "${projectDir}/Makefile" ]]; then
    #	rm -rf "${projectDir}/"
    #fi

    file_delete_if_exist "Projects/${projectDir}/${projectDir}.mk"
    file_delete_if_exist "Projects/${projectDir}/${projectDir}.project"
    file_delete_if_exist "Projects/${projectDir}/${projectDir}.txt"
    file_delete_if_exist "Projects/${projectDir}/imgui.ini"
    file_delete_if_exist "Projects/${projectDir}/compile_flags.txt"
    file_delete_if_exist "Projects/${projectDir}/Makefile"

done


for binDir in ${binDirs[*]}
do
    dir_delete_if_exist $binDir
done
