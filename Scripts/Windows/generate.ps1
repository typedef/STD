cd ../..

Write-Host "Generating premake.lua file"
$projectName     = $args[0]
$premakeFileName = $args[1]
if (Test-Path $premakeFileName) {
	Remove-Item $premakeFileName
}
$templateContent = Get-Content "Templates/template-project-premake.lua"
$templateContent = $templateContent.Replace("%ProjectName%", $projectName)
$templateContent | Out-File $premakeFileName -encoding utf8

Write-Host "Generating project premake file"
$expr = "premake5 --file=" + $premakeFileName + " --cc=clang gmake2"
Invoke-Expression $expr