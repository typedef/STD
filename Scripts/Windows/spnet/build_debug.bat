@echo off

cd ../../..

rmdir /s /q bin

mkdir bin
mkdir bin\\Debug

clang STD\src\Core\Types.c STD\src\Core\SimpleStandardLibrary.c Projects\spnet\src\Server\SpnetServer.c Projects\spnet\src\spnet_demo.c Dependencies\MiniAudio\src\Deps\miniaudio.c  -DPLATFORM_WINDOWS -D_CRT_SECURE_NO_WARNINGS -ISTD\src\ -IProjects\spnet\src\ -IDependencies\MiniAudio\src\ -lShlwapi -luser32 -liphlpAPI -lws2_32.lib -lwinmm -o bin/Debug/spnet.exe

bin\\Debug\\spnet.exe

cd Scripts\Windows\spnet
