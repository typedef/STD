#include "Std/StdAtlasAnimation.h"
#include "Std/StdRenderer.h"
#include <Core/Types.h>
#include <Core/SimpleStandardLibrary.h>
#include <Core/SimpleApplication.h>
#include <Core/SimpleMath.h>
#include <Core/SystemInfo.h>
#include <Core/SimpleWindowLibrary.h>
#include <Std/StdAssetManager.h>


#define art(pPath)				\
    ({						\
	"Assets/Art/" pPath;			\
    })

typedef struct Global
{
    StdAtlasAnimation WarriorAnimation;
} Global;

static Global g = {};

void
td_layer_attach()
{
    StdAtlas warriorAtlas = std_asset_manager_load_atlas(
	art("Units/Warrior_Blue-64-64.png"));

    StdAtlasAnimation warriorAtlasAnim = std_atlas_animation_new(
	(StdAtlasAnimationSettings)
	{
	    .SecondsForOneIter = 0.1,
	    .Atlas = warriorAtlas
	});

    g.WarriorAnimation = warriorAtlasAnim;

    StdTexture* aImages = std_asset_manager_get_all_images();
    std_renderer_set_textures(aImages, sva_count(aImages));
    slog_warn("ImagesCount: %ld\n", sva_count(aImages));
}

void
td_layer_update()
{
    SimpleRuntimeStats stats = simple_application_get_stats();


    StdAtlasAnimation anim = g.WarriorAnimation;
    StdAtlas warriorAtlas = anim.Atlas;
    StdAtlasItem item = warriorAtlas.aAtlasItems[anim.CurrentItem];

    v2 aUvs[4] = {
	[0] = item.UvMin,
	[1] = v2_new(item.UvMin.X, item.UvMax.Y),
	[2] = item.UvMax,
	[3] = v2_new(item.UvMax.X, item.UvMin.Y),
    };
    std_renderer_draw_ui_image_ext(v3_new(500, 500, 1), v2_new(100, 100), warriorAtlas.Id, aUvs, v4_new(1,1,1,1));

    std_atlas_animation_update(&g.WarriorAnimation, stats.Timestep);
}

void
td_layer_event(SwlEvent* pEvent)
{

}

void
td_layer_destroy()
{

}

void
td_layer_add_to_application()
{
    SimpleLayer tdLayer = {
	.pName = "Top Down Game Layer",

	.OnAttach = td_layer_attach,
	.OnUpdate = td_layer_update,
	.OnEvent = td_layer_event,
	.OnDestroy = td_layer_destroy
    };

    simple_application_add_layer(tdLayer);
}
