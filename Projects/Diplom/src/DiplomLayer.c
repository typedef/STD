#include <Core/Types.h>
#include <Core/SimpleApplication.h>
#include <Core/SimpleMath.h>
#include <Core/SystemInfo.h>

#include <TopDownBattle/TopDownBattleLayer.h>

i32
main()
{
    system_info_print();

    SimpleApplicationSettings appSet = (SimpleApplicationSettings) {
	.pName = "Mira Schieder's Diplom",

	.ArgsCount = 0,
	.aArgs = NULL,

	.IsDebug = 1,
	.IsVsync = 1,
	.ScreenMode = SimpleScreenMode_Custom,

	.Size = v2_new(2560, 1349),
	.MinSize = v2_new(2560, 1349),
	.Position = v2_new(0, 0)
    };
    simple_application_create(&appSet);

    td_layer_add_to_application();

    simple_application_run();
    simple_application_destroy();

    return 0;
}
