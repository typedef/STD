#include <Core/SimpleApplication.h>
#include <Core/SimpleStandardLibrary.h>
#include <Core/SystemInfo.h>
#include <Core/Types.h>
#include <locale.h>

#include "StdUiSandbox.h"

i32
main()
{
    setlocale(LC_ALL, ".UTF8");
    system_info_print();

    v2 size = (v2) { 1600, 1000 };
    SimpleApplicationSettings set = {
	.pName = simple_string_format("StdUi-окно %0.0f %0.0f", size.X, size.Y),//"Hello world app",

	.ArgsCount = 0,
	.aArgs = NULL,

	.IsDebug = 1,

	.IsVsync = 1,
	.Size = size,
	.MinSize = {150, 100}
    };

    simple_application_create(&set);

    SimpleLayer sandboxLayer = {
	.pName = simple_string_new("StdUi"),
	.OnAttach = std_ui_sandbox_on_attach,
	.OnUpdate = std_ui_sandbox_on_update,
	.OnUi = std_ui_sandbox_on_ui_render,
	.OnEvent = std_ui_sandbox_on_event,
	.OnDestroy = std_ui_sandbox_on_destroy,
    };

    simple_application_add_layer(sandboxLayer);

    simple_application_run();
    simple_application_destroy();

    return 0;

}
