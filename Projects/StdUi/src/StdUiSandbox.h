#ifndef STD_UI_SANDBOX_H
#define STD_UI_SANDBOX_H

struct SwlEvent;


void std_ui_sandbox_on_attach();
void std_ui_sandbox_on_attach_finished();
void std_ui_sandbox_on_update();
void std_ui_sandbox_on_ui_render();
void std_ui_sandbox_on_event(struct SwlEvent* pEvent);
void std_ui_sandbox_on_destroy();

#endif // STD_UI_H
