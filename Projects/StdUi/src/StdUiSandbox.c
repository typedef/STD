#include "StdUiSandbox.h"

#include <Core/Types.h>
#include <Core/SimpleStandardLibrary.h>
#include <Core/SimpleWindowLibrary.h>
#include <Core/SimpleMath.h>
#include <Core/SimpleMathIO.h>
#include <Core/SimpleApplication.h>
#include <Core/SimpleAtlasFont.h>

#include <Std/StdAssetManager.h>
#include <Std/StdAudioEngine.h>
#include <Std/StdRenderer.h>
#include <Std/StdUi.h>
#include <Std/StdUiExamples.h>
#include <Std/StdAtlas.h>
#include <Std/StdAtlasAnimation.h>
#include <Std/StdUi.h>

#include <Deps/stb_image.h>
#include <Deps/miniaudio.h>


static u32 gB3MainMenu;
static u32 gBF3MainMenu;

#include <stdarg.h>



typedef struct StdUiDefault
{
    StdUiWidgetType Button;
    StdUiWidgetType QuickButton;
    StdUiWidgetType JustText;
} StdUiDefault;


struct GlobalItem
{
    StdUiDefault Default;
    i64 SomeImageId;
    StdAtlas BottleAtlas;
};

static struct GlobalItem g = {};

void
std_ui_sandbox_on_attach()
{
    //slog_auto_tests();
    //slog_error("attach\n");

    SwlWindow* pWindow = simple_application_get_window();
    std_asset_manager_load_font(resource_font("MartianMono-sWdRg.ttf"), 8, 0, 127);
    std_asset_manager_load_font(resource_font("MartianMono-sWdRg.ttf"), 24, 0, 127);
    std_asset_manager_load_font(resource_font("MartianMono-sWdRg.ttf"), 16, 0, 127);
    std_asset_manager_load_font(resource_font("MartianMono-sWdRg.ttf"), 32, 0, 127);
    std_asset_manager_load_font(resource_font("MartianMono-sWdRg.ttf"), 32, 1040, 1103);

    /* // Japanese-style punctuation ( 3000 - 303f) */
    /* std_asset_manager_load_font(resource_font("NotoSansJP.ttf"), 32, 0x3000, 0x303f); */
    /* // Hiragana ( 3040 - 309f) */
    /* std_asset_manager_load_font(resource_font("NotoSansJP.ttf"), 32, 0x3040, 0x309f); */
    /* // Katakana ( 30a0 - 30ff) */
    /* std_asset_manager_load_font(resource_font("NotoSansJP.ttf"), 32, 0x30A0, 0x30FF); */
    /* // Full-width roman characters and half-width katakana ( ff00 - ffef) */
    /* std_asset_manager_load_font(resource_font("NotoSansJP.ttf"), 32, 0xff00, 0xffef); */
    /* std_asset_manager_load_font(resource_font("NotoSansJP.ttf"), 32, 0x4E00, 0x9FFF); */

    std_renderer_set_ui_fonts();

    g.SomeImageId = std_asset_manager_load_image("Assets/Castles/Castle0.png");
    g.BottleAtlas = std_asset_manager_load_atlas("Assets/Interface/Inventory/Bottles/Bottle-32-32.png");

    //std_opengl_set_swap_interval(pWindow, 1);

    /* StdSoundId soundId = std_asset_manager_load_sound_by_id( */
    /*	"Assets/Interface/ClickSound.mp3"); */

    /* ma_sound* pSound = NULL; */
    /* if (soundId != -1) */
    /* { */
    /*	pSound = std_asset_manager_get_sound(soundId); */
    /*	ma_sound_start(pSound); */
    /* } */
    /* else */
    /* { */
    /*	GWARNING("not working!\n"); */
    /* } */

    StdTexture* aTextures = std_asset_manager_get_all_images();
    std_renderer_set_textures(aTextures, sva_count(aTextures));
}

void std_ui_sandbox_on_attach_finished() {}


void
std_ui_sandbox_on_update()
{
    //std_gl_renderer_set_clear_color(v4_new(0.014,0.014,0.114, 1));

    SwlWindow* pWindow = simple_application_get_window();
    swl_v2 mousePosition = swl_window_get_cursor_position(pWindow);
    //GINFO("Mouse Pos: %f.%f\n", mousePosition.X, mousePosition.Y);

}

void
std_ui_demo_widgets_register()
{
    if (!helper_tofr())
	return;

    StdUiShapeStyle btnBack = {
	.Color = {
	    .Idle = v4_rgb(7, 7, 7),
	    .Hovered = v4_rgb(40, 42, 53),
	    .Clicked = v4_rgb(40, 42, 53)
	}
    };
    std_ui_style_add("StdUi_Btn_Background", btnBack);

    StdUiShapeStyle btnBorder = {
	.Color = {
	    .Idle = v4_rgb(191.25, 109.65, 109.65),
	    .Hovered = v4_rgb(200.25, 109.65, 109.65),
	    .Clicked = v4_rgb(200.25, 109.65, 109.65)
	}
    };
    std_ui_style_add("StdUi_Btn_Border", btnBorder);

    StdUiShapeStyle btnText = {
	.Color = {
	    .Idle = v4_rgb(211, 211, 211),
	    .Hovered = v4_rgb(244, 244, 244),
	    .Clicked = v4_rgb(244, 244, 244)
	}
    };
    std_ui_style_add("StdUi_Btn_Text", btnText);

    StdUiShapeStyle quickBtnIndex = {
	.Color = {
	    .Idle = v4_rgb(107, 107, 107),
	    .Hovered = v4_rgb(244, 244, 244),
	    .Clicked = v4_rgb(244, 244, 244)
	}
    };
    std_ui_style_add("StdUi_QuickBtn_Index", quickBtnIndex);

    /*
      DOCS: style names:
      vampire punch(пунш) - neon pink theme
    */
    StdUiShape aShapes[] = {
	[0] = {
	    .Type = StdUiShapeType_Rect,
	    .Position = { .X = 1, .Y = 1, .Z = -0.1f },
	    .Size = v2_new(100, 50),
	    .pStyleName = "StdUi_Btn_Background"
	},
	[1] = {
	    .Type = StdUiShapeType_EmptyRect,
	    .Position = { .X = 1, .Y = 1, .Z = -0.2f },
	    .Size = v2_new(100, 50),
	    .pStyleName = "StdUi_Btn_Border"
	},
	[2] = {
	    .Type = StdUiShapeType_Text,
	    .Align = StdUiTextAlign_Center,
	    .Position = { .X = 0.1, .Y = 0.1, .Z = -0.3f },
	    .pStyleName = "StdUi_Btn_Text"
	}
    };
    StdUiWidgetTypeConfig btnConfig = {
	.aShapes = aShapes,
	.ShapesCount = 3,
	.pName = "StdUiDefault_Btn"
    };

    g.Default.Button = std_ui_widget_type_new(btnConfig);

    StdUiShapeStyle quickBtnImageStyle = {};
    std_ui_style_add("StdUi_Image", quickBtnImageStyle);

    StdUiShape aQuickShapes[] = {
	[0] = {
	    .Type = StdUiShapeType_Rect,
	    .Position = { .X = 1, .Y = 1, .Z = -0.1f },
	    .Size = v2_new(100, 50),
	    .pStyleName = "StdUi_Btn_Background"
	},
	[1] = {
	    .Type = StdUiShapeType_EmptyRect,
	    .Position = { .X = 1, .Y = 1, .Z = -0.2f },
	    .Size = v2_new(100, 50),
	    .pStyleName = "StdUi_Btn_Border"
	},
	[2] = {
	    .Type = StdUiShapeType_Text,
	    .Align = StdUiTextAlign_Custom,
	    .FontSizeRatio = 0.75,
	    .Position = { .X = 0.1, .Y = 0.0, .Z = -0.5f },
	    .pStyleName = "StdUi_QuickBtn_Index"
	},
	[3] = {
	    .Type = StdUiShapeType_AtlasImage,
	    .Align = StdUiTextAlign_Center,
	    .Position = { .X = 0.15, .Y = 0.15, .Z = -0.4f },
	    .Size = v2_new(0.7, 0.7),
	    .pStyleName = "StdUi_Image"
	}
    };

    StdUiWidgetTypeConfig quickBtnConfig = {
	.aShapes = aQuickShapes,
	.ShapesCount = 4,
	.pName = "StdUiDefault_QuickBtn"
    };
    g.Default.QuickButton = std_ui_widget_type_new(quickBtnConfig);

    StdUiShape justTextShapes[] = {
	(StdUiShape) {
	    .Type = StdUiShapeType_Text,
	    .Position = { .X = 0, .Y = 0, .Z = -0.1f },
	    .pStyleName = "StdUi_Btn_Text"
	}
    };
    StdUiWidgetTypeConfig justTextConfig = {
	.aShapes = justTextShapes,
	.ShapesCount = ArrayCount(justTextShapes),
	.pName = "StdUiDefault_JustText"
    };
    g.Default.JustText = std_ui_widget_type_new(quickBtnConfig);

}

void
std_ui_demo_simple_main_menu()
{
    std_ui_piv((StdUiPiv) {
	    .Position = {0.375, 0.2, 0},
	    .Size = {0.25, 0.6}
	});

    // divs bug
    StdUiPanelSettings set = {
	.pUtfLabel = "Panel label",
	.Flags = StdUiPanelFlags_Invisible,
    };
    if (std_ui_panel_begin(set))
    {
	StdUiPanel* pPanel = std_ui_panel_get();

	i64 divId = std_ui_div((StdUiDiv) {
	    .Position = v3_new(0.05, 0.05, -0.2),
	    .Size = v2_new(0.9, 0.9),
	    .Padding = v2_new(0, 0.05),
	    .WidgetSize = v2_new(1.0, 0.283),
	    .Mode = StdUiMode_NewLine,
	    .FontSize = 32,
	});

	//StdUiDiv div = std_ui_div_get(divId);
	//slog_success("pos: %v3\n", div.Absolute.Position);
	//slog_success("size: %v2\n", div.Absolute.Size);

	StdUiState state = std_ui_widget((StdUiWidgetConfig) {
		.TypeId = g.Default.Button.Id,
		.pName = "Play",
	    });
	if (state == StdUiState_Clicked)
	{
	    slog_error("Clicked play!\n");
	}

	state = std_ui_widget((StdUiWidgetConfig) { g.Default.Button.Id, "Options" });
	if (state == StdUiState_Clicked)
	{
	    slog_error("Clicked options\n");
	}

	state = std_ui_widget((StdUiWidgetConfig) { g.Default.Button.Id, "Exit" });

	if (state == StdUiState_Clicked)
	{
	    slog_error("Clicked exit\n");
	}

    }
    std_ui_panel_end();
}

void
std_ui_demo_simple_tabs()
{
    std_ui_piv((StdUiPiv) {
	    .Position = {0.1, 0.2, 0},
	    .Size = {0.8, 0.6}
	});

    StdUiPanelSettings set = {
	.pUtfLabel = "Panel label",
	.Flags = StdUiPanelFlags_None,
    };
    if (std_ui_panel_begin(set))
    {
	v3 divStartPos = v3_new(0.05, 0.05, -0.2);

	i64 tabDivId = std_ui_div((StdUiDiv) {
		.Position = divStartPos,
		.Size = v2_new(0.9, 0.1),
		.Padding = v2_new(0.0, 0.0),
		.WidgetSize = v2_new(0.2, 1),
		.Mode = StdUiMode_SameLine,
		.FontSize = 8,
	    });

	StdUiDiv taskLabelDiv = std_ui_div_get(tabDivId);

	static i32 selected = 0;

	StdUiState state = std_ui_widget((StdUiWidgetConfig) {
		.TypeId = g.Default.Button.Id,
		.pName = "Задания",
	    });
	if (state == StdUiState_Clicked)
	{
	    slog_error("Clicked play!\n");
	    selected = 0;
	}

	state = std_ui_widget((StdUiWidgetConfig) { g.Default.Button.Id, "Активные" });
	if (state == StdUiState_Clicked)
	{
	    slog_error("Clicked options\n");
	    selected = 1;
	}

	state = std_ui_widget((StdUiWidgetConfig) { g.Default.Button.Id, "Выполненные" });

	if (state == StdUiState_Clicked)
	{
	    slog_error("Clicked exit\n");
	    selected = 2;
	}

	v2 taskLabelSize = taskLabelDiv.Size;
	v3 taskLabelPos = v3_addy(divStartPos, taskLabelSize.Height);

	std_ui_div((StdUiDiv) {
		.Position = v3_new(0.05, 0.25, -0.3),
		.Size = v2_new(0.15, 0.7),
		.Mode = StdUiMode_NewLine,
		.WidgetSize = v2_new(1, 0.15),
		.Padding = v2_new(0.0,0.0),
		.FontSize = 32
	    });

	switch (selected)
	{
	case 0:
	{
	    std_ui_widget((StdUiWidgetConfig) { g.Default.Button.Id, "Убить 5 волков" });
	    std_ui_widget((StdUiWidgetConfig) { g.Default.Button.Id, "Задача012" });
	    break;
	}

	case 1:
	{
	    break;
	}

	case 2:
	{
	    break;
	}
	}
    }
    std_ui_panel_end();
}

typedef struct ToCheckData {
    i32 Selected;
} ToCheckData;

static ToCheckData toCheck = {};

void
std_ui_demo_quick_slot()
{
    static char buf[32] = {};
    snprintf(buf, 32, "%d", toCheck.Selected);

#if 1
    std_ui_piv((StdUiPiv) {
	    .Position = {0.1, 0.9, 1},
	    .Size = {0.8, 0.1}
	});

    StdUiPanelSettings set = {
	.pUtfLabel = "Quick slots panel",
	.Flags = StdUiPanelFlags_None,
    };
    if (std_ui_panel_begin(set))
    {
	std_ui_div((StdUiDiv) {
		.Position = v3_new(0.0, 0.0, -0.2),
		.Size = v2_new(1, 1),
		.Padding = v2_new(0.0005, 0.0),
		.WidgetSize = v2_new(0.1, 1),
		.Mode = StdUiMode_SameLine,
		.FontSize = 32,
	    });

	static i32 selected = 0;
	static i32 itemAsset = 0;

	StdUiState state = std_ui_widget((StdUiWidgetConfig) { g.Default.QuickButton.Id, "0", .AssetId = g.BottleAtlas.Id, .ItemId = toCheck.Selected });
	if (state == StdUiState_Clicked)
	{
	    selected = 0;
	}

	state = std_ui_widget((StdUiWidgetConfig) { g.Default.QuickButton.Id, .ItemId = 8, .pName = "1" });
	if (state == StdUiState_Clicked)
	{
	    selected = 1;
	}

	std_ui_widget((StdUiWidgetConfig) { g.Default.QuickButton.Id, .ItemId = 12, .pName = "3" });

	std_ui_widget((StdUiWidgetConfig) { g.Default.QuickButton.Id, .ItemId = 22, .pName = "4" });

	state = std_ui_widget((StdUiWidgetConfig) { g.Default.QuickButton.Id, .ItemId = 34, .pName = "5" });

	state = std_ui_widget((StdUiWidgetConfig) { g.Default.QuickButton.Id, .ItemId = 26, .pName = "6" });
	state = std_ui_widget((StdUiWidgetConfig) { g.Default.QuickButton.Id, .ItemId = 18, .pName = "7" });

	if (state == StdUiState_Clicked)
	{
	    selected = 2;
	}

    }
    std_ui_panel_end();
#endif


#if 1
    std_ui_piv((StdUiPiv) {
	    .Position = v3_new(0.1, 0.1, 3),
	    .Size = v2_new(0.8, 0.1)
	});
    if (std_ui_panel_begin((StdUiPanelSettings) { .pUtfLabel="To check panel", .Flags = StdUiPanelFlags_None }))
    {
	std_ui_div((StdUiDiv) {
		.Position = v3_new(0.1, 0.1, 0.2),
		.Size = v2_new(1, 1),
		.Mode = StdUiMode_NewLine,
		.WidgetSize = v2_new(0.3, 0.3),
		.Padding = v2_new(0.1, 0.1),
		.FontSize = 32,
	    });

	std_ui_widget((StdUiWidgetConfig) {
		.TypeId = g.Default.Button.Id,
		.pName = "JUST DO-DO IT"
	    });

	/* std_ui_widget((StdUiWidgetConfig) { */
	/*	.TypeId = g.Default.JustText.Id, */
	/*	.pName = buf */
	/*     }); */

    }
    std_ui_panel_end();
#endif // 0


}


void
std_ui_sandbox_on_ui_render()
{
#if 0
    std_ui_example();
#endif

    if (helper_tofr())
    {
	std_ui_demo_widgets_register();
    }


    //std_ui_demo_simple_main_menu();
    //std_ui_demo_simple_tabs();
    std_ui_demo_quick_slot();
}

void
std_ui_sandbox_on_event(struct SwlEvent* pEvent)
{
    if (pEvent->Type == SwlEventType_KeyPress)
    {
	SwlKeyPressEvent* pKey = ((SwlKeyPressEvent*)pEvent);

	if (pKey->Key == SwlKey_Left)
	{
	    toCheck.Selected = MinMax(toCheck.Selected -1, 0, sva_count(g.BottleAtlas.aAtlasItems)-1);
	}
	else if (pKey->Key == SwlKey_Right)
	{
	    toCheck.Selected = MinMax(toCheck.Selected + 1, 0, sva_count(g.BottleAtlas.aAtlasItems)-1);

	    slog_warn("Selected : %d\n", toCheck.Selected);
	}
    }
}

void
std_ui_sandbox_on_destroy()
{
}
