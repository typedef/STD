  Mb create local ecs here? Or use global??

  simple_ecs_world();

  SlvPerson -> Id PosComp
  x Should not have 3d mesh, audio, animation and other visualizer stuff

```c
struct PosComp {
   v3 Position;
   v3 Rotation;
}
  ```

  How to impl input in this Logic/Visualizer system?

```csharp
  Persons* aPersons = GetPersons();
  Persons* aCulledPersons = _cpu_culling_system(aPersons);

  foreach (var person in aCulledPersons)
  {
	  // is it all we need?
	  var pos = person.GetPosition();
	  var rot = person.GetRotation();
	  var bodyId = person.GetBodyId();
	  var actId = person.GetActionId();
	  // Push data to renderer
	  renderer.Push(pos, rot, bodyId, actId);
	  // actId - persons *ALWAYS* have some animation attached

	  // Push data to audio engine
	  audioEngine.Push(pos, rot, actId);
  }
```

Can we change action from inside ?

```csharp
void
physics_detect_collision()
{
	// ...

	foreach (var personCollider in aShapes)
	{
		if (IsShapeCollide(personCollider, ballCollider))
		{
			Notify(personCollider, ballCollider, personCollider.CalcPowerOnCollision(personCollider));
		}
	}

	// ...
}

void
game_on_physics_notify(PhysicsCollider* pPersonCollider, PhysicsCollider* pCollider, PhysicsPower* pPower)
{
	SlvPerson* pPerson = GetPersonForCollider(pPersonCollider);
	SlvObject* pBall = (is_obj(pCollider) ? GetObjectForCollider(pCollider) : NULL);
	SlvPhysicalPower power = GetWorldInfluence(pPower);
	// we can use physics_system here
	slv_person_set_physics_motion(pPerson, pBall, power);
}

1:
void
slv_person_set_physics_motion(SlvPerson* pPerson, SlvObject* pObject, SlvPhysicalPower power)
{
	if (slv_physical_power_is_big(power))
	{
		// mb this can be get damage system
		if (pObject->Type == SlvObjectType_Ball)
		{
			// mb we should call slv_physical_state_machine();
			// slv_physical_system()
			slv_person_set_state(pPerson, SlvState_HitByTheBall, pObject->Id);
		}
	}
}

2:



void
slv_person_set_physics_motion(SlvPerson* pPerson, SlvObject* pObject, SlvPhysicalPower power)
{
	if (slv_physical_power_is_big(power))
	{
		person_damage_system_hit_by_object(pPerson, pObject, power);
	}
}

void
person_damage_system_hit_by_object(SlvPerson* pPerson, SlvObject* pObject, SlvPhysicalPower power)
{
	if (pObject->Type == SlvObjectType_Ball)
	{
		// mb we dont need a concrete object in state, only HitByObject
		slv_person_set_state(pPerson, SlvState_HitByTheBall, pObject->Id);
	}
}

```




How to connect Logic and Visualizer in one system?
