#ifndef SLV_WORLD_H
#define SLV_WORLD_H

/*
  Mb create local ecs here? Or use global?

  simple_ecs_world();

  SlvPerson -> Id PosComp
  x Should not have 3d mesh, audio, animation and other visualizer stuff

  PosComp:
  ^ v3 Position;
  ^ v3 Rotation;

  How to impl input in this Logic/Visualizer system?

  Persons* aPersons = GetPersons();
  Persons* aCulledPersons = _cpu_culling_system(aPersons);

  foreach (var person in aCulledPersons)
  {
      // is it all we need?
      var pos = person.GetPosition();
      var rot = person.GetRotation();
      var bodyId = person.GetBodyId();
      var actId = person.GetActionId();
      // Push data to renderer
      renderer.Push(pos, rot, bodyId, actId);
      // actId - persons *ALWAYS* have some animation attached

      // Push data to audio engine
      audioEngine.Push(pos, rot, actId);
  }

  Can we change action from inside


*/

typedef struct SlvWorld
{
} SlvWorld;

SlvWorld slv_world_new();

#endif // SLV_WORLD_H
