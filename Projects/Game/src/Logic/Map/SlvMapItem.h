#ifndef SLV_MAP_ITEM_H
#define SLV_MAP_ITEM_H

/*

  Person can walk from Building0 to Building1 by Road0:
  Building0 ----- Road0 ----- Building1
  Building0 ----- Road0 ----- Building1

  Map MapItem

  --------
  --------
  --HrrH--
  --------
  --HrrH--
  --------
  --HrrH--
  --------
  --HrrH--
  --------

*/

#include <Core/Types.h>

typedef enum SlvMapItemType
{
    SlvMapItemType_Ground = 0,
    SlvMapItemType_Building,
    SlvMapItemType_Road,
    SlvMapItemType_Count,
} SlvMapItemType;

typedef struct SlvMapItem
{
    SlvMapItemType Type;
    v3 Position;
} SlvMapItem;

#endif // SLV_MAP_ITEM_H
