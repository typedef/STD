#ifndef SLV_MAP_H
#define SLV_MAP_H

/*

  Map is done by block division.
  All map setup into table with block Block** tBlocks;

  tBlocks[0][0] = B00

  Search for bar for person in block table:
  Start condition:
  * Person in B11.
  * Bar in B12.

  // this can be optimized to LinearViewOnRealCollection
  // note: its already linear
  // note: inside the block
  var aBlocks = GetNeareastBlocks(B11);
  aBlocks == [B00, B01, B02, B10, B12, B20, B21, B22].size == 8.

  var bars = [];
  foreach (var block in aBlocks) {
      foreach (var mapItem in block.aMapItems) {
          if (mapItem.Type == MapItemType_Bar) {
              // .. do the job here
              bars.Add(mapItem);
          }
      }
  }

  var distance = 10000;
  var bar = null;
  bars.ForEach(b => if (b.Distance(Player) < distance){ distance = b.Distance(Player); bar = b});




  B00:         B01:          B02:
  --------     --------      --------
  --------     --------      --------
  --HrrH--     --HrrH--      --HrrH--
  --------     --------      --------
  --HrrH--     --HrrH--      --HrrH--
  --------     --------      --------
  --HrrH--     --HrrH--      --HrrH--
  --------     --------      --------
  --HrrH--     --HrrH--      --HrrH--
  --------     --------      --------

  B10:         B11:          B12:
  --------     --------      --------
  --------     --------      --------
  --HrrH--     --HrrH--      --HrrH--
  --------     --------      --------
  --HrrH--     --HrrH--      --HrrH--
  --------     --------      --------
  --HrrH--     --HrrH--      --HrrH--
  --------     --------      --------
  --HrrH--     --HrrH--      --HrrH--
  --------     --------      --------

  B20:         B21:          B22:
  --------     --------      --------
  --------     --------      --------
  --HrrH--     --HrrH--      --HrrH--
  --------     --------      --------
  --HrrH--     --HrrH--      --HrrH--
  --------     --------      --------
  --HrrH--     --HrrH--      --HrrH--
  --------     --------      --------
  --HrrH--     --HrrH--      --HrrH--
  --------     --------      --------

 */

#include "SlvBlock.h"

#include <Core/Types.h>

typedef struct SlvMapSetting
{
    // note: in blocks
    i32 Width;
    i32 Height;
    i32 BlockSize;
} SlvMapSetting;

typedef struct SlvMap
{
    v2i Size;
    i32 BlockSize;
    // todo: create double array data structure Array<Array<SlvBlock>>
    /* DOCS:
       height = array_count(tBlocks)
       width = array_count(tBlocks[0])
    */
    SlvBlock** tBlocks;
} SlvMap;

SlvMap slv_map_new(SlvMapSetting slvMapSetting);
void slv_map_destroy(SlvMap map);

v2 slv_map_size(SlvMap* pMap);

void slv_map_set_item(SlvMap* pMap, SlvMapItem mapItem);

#endif // SLV_CITY_H
