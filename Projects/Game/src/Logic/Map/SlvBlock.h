#ifndef SLV_BLOCK_H
#define SLV_BLOCK_H

#include "SlvMapItem.h"

typedef struct SlvBlock
{
    SlvMapItem* aMapItems;
} SlvBlock;

SlvBlock slv_block_new();

#endif // SLV_BLOCK_H
