#include "SlvDateTime.h"

#include <stdio.h>

/*
  DOCS(typedef):
  January(Январь) : 31
  February(Февраль) : 28(Наш вариант)/29
  March(Март) : 31
  April (Апр) : 30
  May (Май) : 31
  June (Июнь) : 30
  July (Июль) : 31
  August (Авг) : 31
  September (Сен) : 30
  October (Окт) : 31
  November (Нояб) : 30
  December (Дек) : 31
*/
static i32 DaysInMonth[12] = {
    31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
};

#define Month0InSeconds(x)                  (31*SlvDays(1)) + x
#define Month1InSeconds(x)   Month0InSeconds(28*SlvDays(1)) + x
#define Month2InSeconds(x)   Month1InSeconds(31*SlvDays(1)) + x
#define Month3InSeconds(x)   Month2InSeconds(30*SlvDays(1)) + x
#define Month4InSeconds(x)   Month3InSeconds(31*SlvDays(1)) + x
#define Month5InSeconds(x)   Month4InSeconds(30*SlvDays(1)) + x
#define Month6InSeconds(x)   Month5InSeconds(31*SlvDays(1)) + x
#define Month7InSeconds(x)   Month6InSeconds(31*SlvDays(1)) + x
#define Month8InSeconds(x)   Month7InSeconds(30*SlvDays(1)) + x
#define Month9InSeconds(x)   Month8InSeconds(31*SlvDays(1)) + x
#define Month10InSeconds(x)  Month9InSeconds(30*SlvDays(1)) + x
#define Month11InSeconds(x) Month10InSeconds(31*SlvDays(1)) + x

static i32 SecondsInMonth[12] = {
    Month0InSeconds(0),
    Month1InSeconds(0),
    Month2InSeconds(0),
    Month3InSeconds(0),
    Month4InSeconds(0),
    Month5InSeconds(0),
    Month6InSeconds(0),
    Month7InSeconds(0),
    Month8InSeconds(0),
    Month9InSeconds(0),
    Month10InSeconds(0),
    Month11InSeconds(0)
};

SlvDateTime
slv_date_time_new(SlvTime time, SlvDate date)
{
    SlvDateTime dateTime = (SlvDateTime) {
        .Time = time,
        .Date = date,
        .MS = 0.0f
    };

    return dateTime;
}

void
slv_date_time_add_ms(SlvDateTime* pDateTime, f32 ms)
{
    pDateTime->MS += ms;
    const f32 cms = 1000.0f;
    if (pDateTime->MS > cms)
    {
        i32 seconds = i32(pDateTime->MS / cms);
        pDateTime->MS -= (seconds * cms);

        if (pDateTime->Time.Seconds < 59)
        {
            pDateTime->Time.Seconds += seconds;
        }
        else
        {
            pDateTime->Time.Seconds = (pDateTime->Time.Seconds + seconds) % 60;
            ++pDateTime->Time.Minutes;
        }

        if (pDateTime->Time.Minutes >= 60)
        {
            pDateTime->Time.Minutes = pDateTime->Time.Minutes % 60;
            ++pDateTime->Time.Hours;
        }
        if (pDateTime->Time.Hours >= 24)
        {
            pDateTime->Time.Hours = pDateTime->Time.Hours % 24;
            ++pDateTime->Date.Day;
            ++pDateTime->Time.Seconds;
        }

        i32 month       = pDateTime->Date.Month - 1;
        i32 monthMaxDay = DaysInMonth[month];
        if (pDateTime->Date.Day > monthMaxDay)
        {
            pDateTime->Date.Day = 1;
            ++pDateTime->Date.Month;
        }

        if (pDateTime->Date.Month > 12)
        {
            pDateTime->Date.Month = 1;
            ++pDateTime->Date.Year;
        }

    }
}

char*
slv_date_time_to_short_string(SlvDateTime dt)
{
    static char buf[35] = {};
    snprintf(buf, 35, "%0.2d:%0.2d:%0.2d", dt.Hours, dt.Minutes, dt.Seconds);
    return (char*) buf;
}

char*
slv_date_time_to_string(SlvDateTime dt)
{
    static char buf[35] = {};
    snprintf(buf, 35, "%0.2d:%0.2d:%0.2d %0.2d.%0.2d.%0.4d", dt.Hours, dt.Minutes, dt.Seconds, dt.Day, dt.Month, dt.Year);
    return (char*) buf;
}

i64
slv_date_time_total_seconds(SlvDateTime dt)
{
    i64 result =
        SlvHours(dt.Time.Hours)
        + SlvMinutes(dt.Time.Minutes)
        + dt.Time.Seconds
        + SlvDays(dt.Date.Day)
        + SecondsInMonth[dt.Date.Month]
        + SlvYears(dt.Date.Year);

    return result;
}
