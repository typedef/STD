#ifndef SLV_DATE_TIME_H
#define SLV_DATE_TIME_H

#include <Core/Types.h>

#define SlvMinutes(m) (60  * (m))
#define SlvHours(h)   (60  * SlvMinutes(h))
#define SlvDays(d)    (24  * SlvHours(d))
#define SlvYears(y)   (365 * SlvHours(y))

typedef struct SlvTime
{
    i8 Hours;
    i8 Minutes;
    i8 Seconds;
} SlvTime;

typedef struct SlvDate
{
    i8 Day;
    i8 Month;
    i16 Year;
} SlvDate;

typedef struct SlvDateTime
{
    union
    {
        struct {
            i8 Hours;
            i8 Minutes;
            i8 Seconds;
        };

        SlvTime Time;
    };

    union {
        struct {
            i8 Day;
            i8 Month;
            i16 Year;
        };

        SlvDate Date;
    };

    f32 MS;
} SlvDateTime;

SlvDateTime slv_date_time_new(SlvTime time, SlvDate date);
void slv_date_time_add_ms(SlvDateTime* pDateTime, f32 ms);
char* slv_date_time_to_short_string(SlvDateTime dt);
char* slv_date_time_to_string(SlvDateTime dt);
i64 slv_date_time_total_seconds(SlvDateTime dt);

#endif // SLV_DATE_TIME_H
