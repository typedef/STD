#include "SlvDateTimeTest.h"

#include <AutoTest/AutoTest.h>

#include <Logic/Time/SlvDateTime.h>

#if !defined(SLV_DATE_TIME_H)
#error "SUKA!!"
#endif

void
slv_date_time_diff()
{
    SlvDateTime slvDateTime =
        slv_date_time_new(
            (SlvTime) {.Hours = 8, .Minutes = 20, .Seconds = 0},
            (SlvDate) {.Day = 2, .Month = 2, .Year = 1985});

    slv_date_time_add_ms(&slvDateTime, 500);
    slv_date_time_add_ms(&slvDateTime, 500);
    slv_date_time_add_ms(&slvDateTime, 500);

    AutoTest_I32_Value(slvDateTime.Seconds);
    AutoTest_I32_Equal(slvDateTime.Seconds, 1);
}

void
slv_date_time_auto_test_process()
{
    AutoTest_File();

    AutoTest_Func(slv_date_time_diff());
}
