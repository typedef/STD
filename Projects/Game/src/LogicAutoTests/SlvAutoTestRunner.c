#include "SlvAutoTestRunner.h"

#include <AutoTest/AutoTest.h>
#include <AutoTest/AutoTestUi.h>

#include "Time/SlvDateTimeTest.h"
#include "Map/SlvMapTest.h"

void
slv_auto_test_runner_process()
{
    // functions here
    slv_date_time_auto_test_process();
    slv_map_test();

    AutoTestGlobal* pAutoTestGlobal = auto_test_get();
    auto_test_ui_draw_file_list(pAutoTestGlobal->aFiles);

}
