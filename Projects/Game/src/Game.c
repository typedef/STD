#include "Game.h"

#include <Engine.h>

#include <Core/SimpleStandardLibrary.h>
#include <Math/SimpleMath.h>
#include <EntitySystem/SimpleEcs.h>

#define SWL_IMPLEMENTATION
#define SVL_CRASH_ON_ERROR 1
#define SWL_X11_BACKEND 1
#define SWL_RENDERER
#include <Core/SimpleWindowLibrary.h>

#include <LogicAutoTests/SlvAutoTestRunner.h>

#define GAME_RUN_TEST 0

// Game Demo Related
//static GameWorld sGameWorld;

/*
  Slv - Simple Little Virtual


  SlvWorld - handle people, time.

  SlvCity - instead of 'Map'
  SlvCityItem (|SlvBuilding) - handle individual part of city:
  road, house, market, police station and other
  ?Road
  ?RoadItem (|PhysicalItem) - it can be physical object (PhysicalObject)
  ?Building

  SlvTime - handle date and time of game world.

  SlvPeople - handle internal/external human stuff (mind, body, etc)
  SlvMind - handle thought process and desicion making.
  SlvBody - handle body

*/


/* typedef struct SlvWorld */
/* { */

/* } SlvWorld; */

/* typedef struct SlvWorld */
/* { */

/* } SlvWorld; */

#include <Deps/stb_image.h>

static SwlWindow gSwlWindow = {};
static i32 gAppRun = 1;

void
my_event_callback(SwlEvent* pEvent)
{
    if (pEvent->Type == SwlEventType_WindowResized)
    {
        swl_renderer_resize(&gSwlWindow);
        //printf("Size: %d %d\n", gSwlWindow.Size.Width, gSwlWindow.Size.Height);
    }

    if (pEvent->Type == SwlEventType_MousePress
        || pEvent->Type == SwlEventType_MouseRelease)
    {
        SwlMousePressEvent* pMouseKey = (SwlMousePressEvent*) pEvent;
        printf("IsHandled: %d Type: %d Key: %d\n", pEvent->IsHandled, pEvent->Type, pMouseKey->Key);
    }

    if (pEvent->Type == SwlEventType_KeyPress)
    {
        SwlKeyPressEvent* pPressEvent = (SwlKeyPressEvent*) pEvent;
        if (pPressEvent->Key == SwlKey_Escape)
        {
            gAppRun = 0;
        }
    }
}

i32
_swl_load_texture(const char* pPath, i32* pW, swl_i32* pH, swl_i32* pC, void** pData)
{
    swl_i32 width, height, channels;
    *pData = (void*) stbi_load(pPath, &width, &height, &channels, 0);
    if (*pData == NULL)
    {
        return 0;
    }

    *pW = width;
    *pH = height;
    *pC = (swl_i8)channels;
    //stbi_image_free(data);
    return 1;
}

#define to_v3(sv3) ((v3)(*((v3*)&sv3)))
#define to_swl_v3(cv3) ((swl_v3)(*((swl_v3*)&cv3)))

void
_draw_btn(swl_v3 pos, swl_v2 size, wchar* pText, size_t textSize)
{
    swl_v4 backgrounColor = (swl_v4) {0,1,1,1};
    swl_v4 foregroundColor = (swl_v4) {1,1,1,1};

    v3 textPos = v3_add(to_v3(pos), v3_new(0,0,1));

    /* swl_renderer_draw_rectangle( */
    /*     &gSwlWindow, */
    /*     pos, size, */
    /*     backgrounColor); */
    swl_renderer_draw_text(&gSwlWindow,
                           (swl_i32*)pText, textSize,
                           to_swl_v3(textPos),
                           foregroundColor);
}

void
_main_menu()
{
    wchar* pPlayBtnText = L"Play";
    _draw_btn((swl_v3) {100,100,1}, (swl_v2) {150, 50}, pPlayBtnText, 4);

}

void
my_update_loop()
{
    swl_init((SwlInit) { .MemoryAllocate = malloc, .MemoryFree = free, });
    SwlWindowSettings set = {
        .pName = "Swl Window test",
        .Position = {.X = 200, .Y = 100},
        .Size = {
            .Width = 1920,//968,
            .Height = 1061,
            .MinHeight = 150,
            .MinWidth = 150
        },
    };
    swl_window_create(&gSwlWindow, set);

    swl_window_set_event_call(&gSwlWindow, my_event_callback);

    swl_renderer_create(&gSwlWindow);
    swl_renderer_set_swap_iterval(&gSwlWindow, 1);

    i32 codePoints[] = {
        '!', '"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?', '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '[', '\\', ']', '^', '_', '`', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '{', '|', '}', '~'
    };

    SwlFontSettings fontSet = {
        .pFontPath = asset_font("NotoSans.ttf"),
        .FontSize = 68,
        .BitmapSize = (swl_v2) { 640, 640 },
        .aCodePoints = codePoints,
        .CodePointsCount = ARRAY_COUNT(codePoints),
    };
    swl_renderer_font_load(&fontSet);
    void* pBitmap = swl_renderer_font_get_bitmap();
    swl_i32 fontId = swl_renderer_texture_create(pBitmap, (swl_v2){fontSet.BitmapSize.X,fontSet.BitmapSize.Y}, 1);
    /* printf("Here\n\n\n"); */

    swl_i32 textureId;
    {
        swl_i32 w, h, c;
        void* pData = NULL;
        swl_i32 isSuccess = _swl_load_texture(
            asset_texture("crimson_stem_top.png"),
            &w, &h, &c, &pData);
        if (!isSuccess)
        {
            printf("Could not load texture \n");
        }
        textureId = swl_renderer_texture_create(pData, (swl_v2){w, h}, c);
    }

    srand(time(NULL));
    swl_f32 r = 0.0654f, g = 0.268f, b = 0.4523f,
        ri = 0.0531f, gi=0.0203f, bi=.103f;
    f64 timestep = 0.111116f;
    f64 prevTime = simple_timer_get_time();
    f32 x = 0;
    while (gAppRun)
    {
        f64 newTime = simple_timer_get_time();
        timestep = newTime - prevTime;
        prevTime = newTime;
        // printf("timestep: %f\n", timestep);

        r += timestep * ri;
        g += timestep * gi;
        b += timestep * bi;

        //printf("r: %f g: %f b: %f\n", r, g, b);

        swl_renderer_clear_color(&gSwlWindow, r, g, b, 1.0f);
        swl_update(&gSwlWindow);

        _main_menu();

        swl_renderer_flush(&gSwlWindow);

    }

    swl_window_destroy(&gSwlWindow);
    application_close();
}

void
game_on_attach()
{

#if GAME_RUN_TEST == 1

    slv_auto_test_runner_process();

#else
    my_update_loop();
#endif


    /* GameWorldSettings set = { */
    /*     .TimeMultiplier = 8, */
    /*     .StartDate = game_time_new_full(8, 27, 30, 31, 03, 1985), */
    /*     .pEcsWorld = ecs_world_create() */
    /* }; */
    /* sGameWorld = game_world_create(set); */

    /* visualizer_create(&sGameWorld); */

    /*
      Human Personality Matrix (HPM)

      Matrix that encodes personality:
      M = ( 0.10984f ... 0.916002f
      ...
      0.723356002f ... 0.200091200f
      )
    */



}

void
game_on_update(f32 timestep)
{
    //scene_update();
    /* game_world_update(&sGameWorld, timestep); */
    /* visualizer_render(timestep); */
}

void
game_on_ui_render()
{
    //visualizer_ui();
}

void
game_on_event(Event* event)
{
    //visualizer_on_event(event);

    switch (event->Category)
    {

    case EventCategory_Key:
    {
        if (event->Type != EventType_KeyPressed)
            break;

        KeyPressedEvent* keyEvent = (KeyPressedEvent*) event;
        if (keyEvent->KeyCode == KeyType_Escape)
        {
            application_close();
            event->IsHandled = 1;
        }

        break;
    }

    case EventCategory_Window:
    {
        if (event->Category == EventCategory_Window)
        {
            switch (event->Type)
            {

            case EventType_WindowShouldBeClosed:
                application_close();
                event->IsHandled = 1;
                break;
            }
        }
    }

    default:
        break;

    }

}

void
game_on_destroy()
{
    //game_world_destroy(sGameWorld);
    //visualizer_destroy();
}











void
trash()
{
    #if 0
    static bool mindVisible = true;
    if (igBegin("Mind", &mindVisible, ImGuiWindowFlags_None))
    {
        MindComponent* mind = GetComponentRecordData(MindComponent);
        igText("CurrentEntity: %d", mind->CurrentEntity);

        igText("Needs:");
        igText("Food: %0.3f", mind->Needs.Food);
        igText("Toilet: %0.3f", mind->Needs.Toilet);
        igText("Social: %0.3f", mind->Needs.Social);
        igText("Energy: %0.3f", mind->Needs.Energy);
        igText("Hygiene: %0.3f", mind->Needs.Hygiene);
        igText("Fun: %0.3f", mind->Needs.Fun);
        igText("Instincts: %0.3f", mind->Needs.Instincts);
        igText("Ambitions: %0.3f", mind->Needs.Ambitions);

        igText("[Current Module]");
        igText("Type: %s", module_type_to_string(mind->CurrentModule.Type));
        igText("CurrentActivity: %s", module_type_to_string(mind->CurrentModule.CurrentActivity));

    }
    igEnd();

    static bool timeVisible = true;
    if (igBegin("Time", &timeVisible, ImGuiWindowFlags_None))
    {
        char buf[65];
        game_time_to_string(buf, sGameWorld.CurrentDate);
        igText("TimeMultiplier: %d", sGameWorld.TimeMultiplier);
        igText("Time: %s", buf);
        igText("WorldTimestep: %0.3f ms", sGameWorld.WorldTimestep * 1000);
    }
    igEnd();
#endif

}


/*

  System Oriented Design

update()
{
    ai_system_update():
    for (i32 i = 0; i < minds.Count; ++i)
    {
        minds[i].Update();
    }

    optimization ...
    let playerFrustum = GetPlayerFrustum();
    worldBuildings = world_building_update(playerFrustum);
    assetsTable = assets_table_update(playerFrustum);
    asset_system_set_visibility(worldBuildings, assetsTable, playerFrustrum);

    // Optimized worldBuilding and assets, get only what is visible for player.
    physics_system_update() depends on RigidBodyComponent, SoftBodyComponent;
    animation_system_update() depends on AnimationComponent;
    graphics_system_update() depends on ModelComponent;


}


some_player_code()
{
    let model: Model = GetPlayerComponent<ModelComponent>();
    foreach (let mesh in model.Meshes)
    {
        if (mesh.IsLeg())
        {
            mesh.SetDamagedTexture();
        }
    }
}















 */
