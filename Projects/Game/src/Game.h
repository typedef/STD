#ifndef GAME_H
#define GAME_H

#include <Core/Types.h>

struct Event;

void game_on_attach();
void game_on_update(f32 timestep);
void game_on_ui_render();
void game_on_event(struct Event* event);
void game_on_destroy();

#endif // GAME_H
