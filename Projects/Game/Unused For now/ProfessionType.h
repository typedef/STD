#ifndef PROFESSION_TYPE_H
#define PROFESSION_TYPE_H

typedef enum ProfessionType
{
    ProfessionType_None = 0,
    ProfessionType_DefaultWorker,
    ProfessionType_Count
} ProfessionType;

#endif
