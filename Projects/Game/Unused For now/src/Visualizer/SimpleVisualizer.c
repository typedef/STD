#include "SimpleVisualizer.h"

#include <Application/Application.h>
#include <Core/GameWorld/GameWorld.h>
#include <EntitySystem/Components/Base/CameraComponent.h>
#include <EntitySystem/Components/Base/TransformComponent.h>
#include <EntitySystem/Components/3D/StaticModelComponent.h>
#include <EntitySystem/SimpleEcs.h>
#include <Editor/EditorViewport.h>
#include <Graphics/Vulkan/VulkanSimpleRenderer.h>
#include <Math/SimpleMath.h>
#include <AssetManager/AssetManager.h>
#include <InputSystem/SimpleInput.h>
#include <Utils/SimpleStandardLibrary.h>
#include <UI/Suif.h>
#include <Editor/EditorPanels.h>


// Inside game
#include "Core/Components/Ai/MindComponent.h"
#include "MapGenerator.h"

static Renderer3dStatic gRenderer3dStatic = {};
static m4 gMainModelTransform = {};
static CameraComponent gCameraComponent = {};
struct EditorViewport gEditorViewport = {};
static StaticModel gSkinnedModel = {};
static StaticModel gCubeModel = {};

static GameWorld* gGameWorld = NULL;

void
_simple_visualizer_camera_updated(struct CameraComponent* pCamera)
{
    vsr_3d_update_camera_ubo(&gRenderer3dStatic, &pCamera->Base);
}

void
visualizer_create(struct GameWorld* pGameWorld)
{
    gGameWorld = pGameWorld;

    CameraComponentCreateInfo createInfo = {
	.IsCameraMoved = 1,

	.Settings = {
	    .Type = CameraType_PerspectiveSpectator,
	    .Position = v3_new(2.0f, 16.0f, 38.0f),
	    .Front = v3_new(0, 0, -1),
	    .Up    = v3_new(0, 1,  0),
	    .Right = v3_new(1, 0,  0),
	    .Yaw = 0,
	    .Pitch = 0,
	    .PrevX = 0,
	    .PrevY = 0,
	    .SensitivityHorizontal = 0.35f,
	    .SensitivityVertical = 0.45f
	},

	.Perspective = {
	    .Near = 0.001f,
	    .Far = 4000.0f,
	    .AspectRatio = application_get_window()->AspectRatio,
	    // NOTE(typedef): in degree
	    .Fov = 75.0f
	},

	.Spectator = {
	    .Speed = 5.0f,
	    .Zoom = 0
	}
    };

    gCameraComponent =
	camera_component_new_ext(createInfo);
    camera_component_update(&gCameraComponent);
    editor_viewport_create(&gEditorViewport, &gCameraComponent, _simple_visualizer_camera_updated);

    Renderer3dStaticSettings settings = {
	.MaxVerticesCount = I32M(4, 000, 000),
	.MaxInstancesCount = I32T(2, 000),
	.pCamera = &gCameraComponent.Base
    };
    vsr_3d_static_create(&gRenderer3dStatic, &settings);

    //gSkinnedModel = *((StaticModel*) asset_manager_load_asset(AssetType_StaticModel, asset_model("CesiumMan/CesiumMan.gltf")));
    //gMainModelTransform = m4_translate_identity(v3_new(-3,-2,-1));
    //gCubeModel = *((StaticModel*) asset_manager_load_asset(AssetType_GeneratedModel_Cube, NULL));

    // NOTE() Game objects
    EcsWorld* pEcsWorld = gGameWorld->pEcsWorld;
    map_generator_create(pGameWorld, pEcsWorld);

    ComponentId transformId = ecs_component_get_id_by_name("TransformComponent");
    ComponentId staticModelId = ecs_component_get_id_by_name("StaticModelComponent");

    EcsQuery objectsToRender = ecs_get_query_w_cache(TransformComponent, StaticModelComponent);
    for (i32 i = 0; i < array_count(objectsToRender.Entities); ++i)
    {
	EntityRecord entityRecord = objectsToRender.Entities[i];

	TransformComponent* ptc = (TransformComponent*) GetComponentById(entityRecord.Id, transformId);
	StaticModelComponent* psmc = (StaticModelComponent*) GetComponentById(entityRecord.Id, staticModelId);

	vsr_3d_static_draw_model(&gRenderer3dStatic, psmc->Model, ptc->Matrix);
    }


    i32 imageIndex = vsr_get_image_index();
    vsr_3d_flush(&gRenderer3dStatic, imageIndex);


}

void
visualizer_render(f32 timestep)
{

    editor_viewport_on_update(&gEditorViewport, timestep);
    if (gEditorViewport.IsActive)
	return;

}

static WideString timeLabel = { .Buffer = L"ТаймСкейл", .Length = 9 };

void
visualizer_ui()
{
    editor_panel_camera(&gCameraComponent, &gEditorViewport);

    i32 isVisible = 1;
    if (sui_panel_begin_l(&timeLabel, &isVisible, SuiPanelFlags_Movable))
    {
	GameTime time = gGameWorld->CurrentDate;

	f32 timeScale = (f32) gGameWorld->TimeMultiplier;
	SuiState state = sui_slider_f32("ВремяДата", &timeScale, v2_new(1.0f, 10.0f));
	if (state == SuiState_Changed)
	{
	    gGameWorld->TimeMultiplier = (i32) timeScale;
	}

	sui_text("%0.2d:%0.2d:%0.2d", time.Hours, time.Minutes, time.Seconds);

	sui_text("%0.2d.%0.2d.%0.4d", time.Day, time.Month, time.Year);

	if (sui_button("Кнопка", v2_new(100, 50)) == SuiState_Clicked)
	{
	    GINFO("Pressed!\n");
	}

    }
    sui_panel_end();

    EcsWorld* pWorld = gGameWorld->pEcsWorld;
    EntityId controlledEntity = pWorld->EStorage.Records[0].Id;
    i32 isStateVisible = 1;
    static WideString stateLabel = { .Buffer = L"Состояние", .Length = 9 };
    if (sui_panel_begin_l(&stateLabel, &isStateVisible, SuiPanelFlags_Movable))
    {
	MindComponent* mind = GetComponent(controlledEntity, MindComponent);
	Needs needs = mind->Needs;

	sui_text("Food: %0.2f", needs.Food);
	sui_text("Toilet: %0.2f", needs.Toilet);
	sui_text("Social: %0.2f", needs.Social);
	sui_text("Energy: %0.2f", needs.Energy);
	sui_text("Hygiene: %0.2f", needs.Hygiene);
	sui_text("Fun: %0.2f", needs.Fun);
	sui_text("Instincts: %0.2f", needs.Instincts);
	sui_text("Ambitions: %0.2f", needs.Ambitions);

    }
    sui_panel_end();

#if 1
    i32 isItVisible = 1;
    //if (sui_panel_begin("Some panel with text", &isItVisible, SuiPanelFlags_Movable))
    // BUG: crash "Строчка Текста"
    if (sui_panel_begin("ANewTexter", &isItVisible, SuiPanelFlags_Movable))
    {
	sui_text("Ambitions: %0.2f", 12.0f);
    }
    sui_panel_end();
#endif

#if 1
    if (sui_panel_begin_l(&timeLabel, &isVisible, SuiPanelFlags_Movable))
    {
	if (sui_button("Кнопка1", v2_new(150, 75)) == SuiState_Clicked)
	{
	    GINFO("Pressed!\n");
	}

	if (sui_button("Кнопка22", v2_0()) == SuiState_Hovered)
	{
	}
    }
    sui_panel_end();

    if (sui_panel_begin("Лалал", NULL, SuiPanelFlags_WithOutHeader))
    {
	if (sui_button("Кнопка для нажатия", v2_0()) == SuiState_Clicked)
	{
	    GINFO("Btn clicked!\n");
	}
    }
    sui_panel_end();
#endif

}

void
visualizer_on_event(struct Event* pEvent)
{
    editor_viewport_on_event(&gEditorViewport, pEvent);
}

void
visualizer_destroy(void)
{
    vsr_3d_static_destroy(&gRenderer3dStatic);
}
