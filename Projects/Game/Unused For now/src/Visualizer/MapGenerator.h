#ifndef MAP_GENERATOR_H
#define MAP_GENERATOR_H

struct GameWorld;
struct EcsWorld;

void map_generator_create(struct GameWorld* pGameWorld, struct EcsWorld* pEcsWorld);

#endif // MAP_GENERATOR_H
