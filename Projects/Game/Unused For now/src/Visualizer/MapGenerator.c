#include "MapGenerator.h"

#include <Core/GameWorld/GameWorld.h>

#include <AssetManager/AssetManager.h>
#include <EntitySystem/SimpleEcs.h>
#include <Math/SimpleMath.h>
#include <Utils/SimpleStandardLibrary.h>

#include <EntitySystem/Components/Base/CameraComponent.h>
#include <EntitySystem/Components/Base/TransformComponent.h>
#include <EntitySystem/Components/3D/StaticModelComponent.h>

static v2i
_map_generator_find_x_position(char* map, char x)
{
    v2i result = {};

    i32 row = 0;
    i32 col = 0;

    char* ptr = map;
    char c = *ptr;
    while (c != '\0')
    {
	if (c == '\n')
	{
	    ++row;
	    col = 0;
	}
	else if (c == x)
	{
	    break;
	}
	else
	{
	    ++col;
	}

	++ptr;
	c = *ptr;
    }

    result.X = col;
    result.Y = row;

    return result;
}

void
map_generator_create(struct GameWorld* pGameWorld, struct EcsWorld* pEcsWorld)
{
    char* mapAscii =
	"****************\n"
	"****************\n"
	"****************\n"
	"****************\n"
	"****************\n"
	"****************\n"
	"****************\n"
	"********P*******\n"
	"****************\n"
	"****************\n"
	"****************\n"
	"****************\n"
	"****************\n"
	"**W*************\n"
	"****************\n"
	;

    i32 cellSize = 3;

    i32 mapWidthCellCount = string_index_of(mapAscii, '\n');
    i32 mapHeightCellCount = string_count_of(mapAscii, '\n');

    i32 mapWidth  = mapWidthCellCount * cellSize;
    i32 mapHeight = mapHeightCellCount * cellSize;

    //GWARNING("%d %d\n", mapWidthCellCount, mapHeightCellCount);
    //GWARNING("%d %d\n", mapWidth, mapHeight);

    // DOCS(): Hero placement
    v2i heroPosition = _map_generator_find_x_position(mapAscii, 'P');
    //GERROR("X = %d Y = %d\n", heroPosition.X, heroPosition.Y);
    //vguard(heroPosition.X == 8 && heroPosition.Y == 7);
    v2 startGroundPos = v2_new(0.0 - mapWidth / 2, 0.0 - mapHeight / 2);
    EntityRecord firstPersonRecord = pGameWorld->pEcsWorld->EStorage.Records[0];
    TransformComponent* ptc = GetComponent(firstPersonRecord.Id, TransformComponent);
    ptc->Translation = v3_new(startGroundPos.X + heroPosition.X * cellSize, 0.1, startGroundPos.Y + heroPosition.Y * cellSize);
    ptc->Rotation = v3_new(0, 0, 0);
    ptc->Scale = v3_new(1, 1, 1);
    ptc->Matrix = m4_transform(ptc->Translation, ptc->Rotation, ptc->Scale);

    // DOCS(): Making ground
    {
	EntityId groundEntity = ecs_entity("Ground");
	StaticModelComponent groundSmc = {
	    .Model = *((StaticModel*) asset_manager_load_asset(AssetType_GeneratedModel_Cube, NULL))
	};
	TransformComponent groundTc = transform_component_new(
	    v3_new(0,0,0), v3_new(0,0,0), v3_new(mapWidth, 0.1, mapHeight));

	ecs_entity_add_component(groundEntity, TransformComponent, groundTc);
	ecs_entity_add_component(groundEntity, StaticModelComponent, groundSmc);
    }

    // DOCS(): Making work place
    {
	v2i workPosition = _map_generator_find_x_position(mapAscii, 'W');
	GERROR("X=%d Y=%d\n", workPosition.X, workPosition.Y);
	EntityId workEntity = ecs_entity("Work");

	StaticModel* pNewCubeModel = asset_manager_copy(AssetType_GeneratedModel_Cube, "Work Cube Model");

	struct MeshMaterial yellowMaterial = {
	    .IsMetallicExist = 1,
	    .Name = "Yellow Mat Color",
	    .Metallic = {
		.MetallicFactor = 0.0f,
		.RoughnessFactor = 0.0f,
		.BaseColor = v4_new(0, 1, 1, 1),
		.BaseColorTexture = -1,
		.MetallicRoughnessTexture = -1
	    }
	};
	i64 newMatId = asset_manager_load_material_id(yellowMaterial);
	pNewCubeModel->Meshes[0].MaterialId = newMatId;
	StaticModelComponent groundSmc = {
	    .Model = *pNewCubeModel
	};

	groundSmc.Model.Meshes[0].MaterialId = newMatId;
	TransformComponent groundTc = transform_component_new(
	    v3_new(startGroundPos.X + workPosition.X * cellSize, cellSize*0.5f, startGroundPos.Y + workPosition.Y * cellSize), v3_new(0,0,0), v3_new(1, 3, 1));

	ecs_entity_add_component(workEntity, TransformComponent, groundTc);
	ecs_entity_add_component(workEntity, StaticModelComponent, groundSmc);
    }

}
