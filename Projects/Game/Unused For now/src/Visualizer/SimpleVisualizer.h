#ifndef SIMPLE_VISUALIZER_H
#define SIMPLE_VISUALIZER_H

#include <Utils/Types.h>

struct GameWorld;
struct Event;

void visualizer_create(struct GameWorld* pGameWorld);
void visualizer_render(f32 timestep);
void visualizer_ui();
void visualizer_on_event(struct Event* pEvent);
void visualizer_destroy(void);

#endif // SIMPLE_VISUALIZER_H
