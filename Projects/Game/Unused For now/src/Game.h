#ifndef GAME_H
#define GAME_H

#include <Engine.h>

void game_on_attach();
void game_on_update(f32 timestep);
void game_on_ui_render();
void game_on_event(Event* event);
void game_on_destroy();

#endif // GAME_H
