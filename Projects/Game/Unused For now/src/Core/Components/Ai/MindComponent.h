#ifndef MIND_COMPONENT_H
#define MIND_COMPONENT_H

#include <EntitySystem/SimpleEcs.h>
#include <Core/Ai/Module/Module.h>
#include <Core/Ai/Base/Needs.h>

struct AiData;

typedef struct MindComponentSettings
{
    EntityId CurrentEntity;
} MindComponentSettings;

typedef struct MindComponent
{
    EntityId CurrentEntity;
    Needs Needs;
    Module CurrentModule;
} MindComponent;

MindComponent mind_create(MindComponentSettings set);
void mind_interrupt(Module* module, struct AiData* aiData);
void mind_update(MindComponent* mind);

#endif // MIND_COMPONENT_H
