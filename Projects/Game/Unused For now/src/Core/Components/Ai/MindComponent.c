#include "MindComponent.h"
#include "Utils/SimpleStandardLibrary.h"

#include <Core/Ai/Module/Module.h>
#include <Core/Ai/Base/AiContext.h>
#include <Core/Ai/Base/AiData.h>

MindComponent
mind_create(MindComponentSettings set)
{
    MindComponent mind = {
	.CurrentEntity = set.CurrentEntity,
	.Needs = needs_create(),
	.CurrentModule = ai_context_get_module(ModuleType_Idle)
    };

    return mind;
}

void
mind_interrupt(Module* module, AiData* aiData)
{

}

void
mind_update(MindComponent* mind)
{
    //_mind_process_urgent_modules(mind);

    AiData aiData = {
	.Needs = &mind->Needs,
	.WorldTimestep = 0.01f,
	.CurrentEntityId = mind->CurrentEntity,
	.RelatedEntityIds = NULL
    };
    module_update(&mind->CurrentModule, &aiData);

    //needs_component_update(&mind->Needs);

}
