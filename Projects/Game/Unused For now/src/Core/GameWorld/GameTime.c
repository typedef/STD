#include "GameTime.h"

#include <Utils/stb_sprintf.h>
#include <Utils/SimpleStandardLibrary.h>

/* NOTE(typedef): Global vars stuff */
GameTimeSecondsChangedDelegate* SecondsSubscribers = NULL;


/*
  DOCS(typedef):
  January(Январь) : 31
  February(Февраль) : 28(Наш вариант)/29
  March(Март) : 31
  April (Апр) : 30
  May (Май) : 31
  June (Июнь) : 30
  July (Июль) : 31
  August (Авг) : 31
  September (Сен) : 30
  October (Окт) : 31
  November (Нояб) : 30
  December (Дек) : 31
*/
static i32 DaysInMonth[12] = {
    31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
};

#define Month0InSeconds(x)                   (31*Days(1)) + x
#define Month1InSeconds(x)   Month0InSeconds((28*Days(1))) + x
#define Month2InSeconds(x)   Month1InSeconds((31*Days(1))) + x
#define Month3InSeconds(x)   Month2InSeconds((30*Days(1))) + x
#define Month4InSeconds(x)   Month3InSeconds((31*Days(1))) + x
#define Month5InSeconds(x)   Month4InSeconds((30*Days(1))) + x
#define Month6InSeconds(x)   Month5InSeconds((31*Days(1))) + x
#define Month7InSeconds(x)   Month6InSeconds((31*Days(1))) + x
#define Month8InSeconds(x)   Month7InSeconds((30*Days(1))) + x
#define Month9InSeconds(x)   Month8InSeconds((31*Days(1))) + x
#define Month10InSeconds(x)  Month9InSeconds((30*Days(1))) + x
#define Month11InSeconds(x) Month10InSeconds((31*Days(1))) + x

static i32 SecondsInMonth[12] = {
    Month0InSeconds(0),
    Month1InSeconds(0),
    Month2InSeconds(0),
    Month3InSeconds(0),
    Month4InSeconds(0),
    Month5InSeconds(0),
    Month6InSeconds(0),
    Month7InSeconds(0),
    Month8InSeconds(0),
    Month9InSeconds(0),
    Month10InSeconds(0),
    Month11InSeconds(0)
};


GameTime
game_time_new_full(i8 h, i8 m, i8 s, i8 day, i8 month, i16 year)
{
    GameTime time = (GameTime) {
	.Hours = h,
	.Minutes = m,
	.Seconds = s,
	.Day = day,
	.Month = month,
	.Year = year,
	.MS = 0.0f
    };

    return time;
}

GameTime
game_time_new(i8 h, i8 m, i8 s)
{
    i32 day, month, year;
    return game_time_new_full(h, m, s, day=0, month=0, year=0);
}

i8
game_time_subsribe_seconds_delegate(GameTimeSecondsChangedDelegate delegate)
{
    i32 exist = array_index_of(SecondsSubscribers, item == delegate) != -1;
    if (exist)
    {
	return 0;
    }

    array_push(SecondsSubscribers, delegate);

    return 1;
}

i8
game_time_unsubsribe_seconds_delegate(GameTimeSecondsChangedDelegate delegate)
{
    i32 notExist = array_index_of(SecondsSubscribers, item == delegate) == -1;
    if (notExist)
    {
	return 0;
    }

    array_remove(SecondsSubscribers, delegate);

    return 1;
}

void
game_time_destroy_seconds_delegats()
{
    array_free(SecondsSubscribers);
}

i32
game_time_close_to_with_diff(GameTime this, GameTime other, i32 seconds)
{
    i64 thisSeconds;
    i64 otherSeconds;
    if (this.Year == 0 || other.Year == 0)
    {
	thisSeconds = game_time_get_total_seconds_short(this);
	otherSeconds = game_time_get_total_seconds_short(other);
    }
    else
    {
	thisSeconds = game_time_get_total_seconds(this);
	otherSeconds = game_time_get_total_seconds(other);
    }

    i64 diff = i32_abs(thisSeconds - otherSeconds);
    if (diff <= seconds)
    {
	return 1;
    }

    return 0;
}

void
game_time_add_second(GameTime* time, i8 second)
{
    if (time->Seconds < 59)
    {
	time->Seconds += second;
    }
    else
    {
	time->Seconds = (time->Seconds + second) % 60;
	++time->Minutes;
    }

    if (time->Minutes >= 60)
    {
	time->Minutes = time->Minutes % 60;
	++time->Hours;
    }
    if (time->Hours >= 24)
    {
	time->Hours = time->Hours % 24;
	++time->Day;
	++time->Seconds;
    }

    i32 month       = time->Month - 1;
    i32 monthMaxDay = DaysInMonth[month];
    if (time->Day > monthMaxDay)
    {
	time->Day = 1;
	++time->Month;
    }

    if (time->Month > 12)
    {
	time->Month = 1;
	++time->Year;
    }

    array_foreach(SecondsSubscribers, item(time); );
}

void
game_time_add_ms(GameTime* time, f32 ms)
{
    time->MS += ms;
    const f32 cms = 1000.0f;
    if (time->MS > cms)
    {
	i32 seconds = i32(time->MS / cms);
	time->MS -= (seconds * cms);

	game_time_add_second(time, seconds);
    }
}

void
game_time_to_short_string(char* buf, GameTime gameTime)
{
    stbsp_sprintf(buf, "%0.2d:%0.2d:%0.2d", gameTime.Hours, gameTime.Minutes, gameTime.Seconds);
}
void
game_time_to_string(char* buf, GameTime gameTime)
{
    stbsp_sprintf(buf, "%0.2d:%0.2d:%0.2d %0.2d.%0.2d.%0.4d", gameTime.Hours, gameTime.Minutes, gameTime.Seconds, gameTime.Day, gameTime.Month, gameTime.Year);
}

i64
game_time_diff(GameTime this, GameTime other)
{
    i64 thisTotal  = game_time_get_total_seconds_short(this);
    i64 otherTotal = game_time_get_total_seconds_short(other);
    i64 diff = ABS(thisTotal - otherTotal);
    return diff;
}

i64
game_time_diff_minutes(GameTime this, GameTime other)
{
    i64 diff = game_time_diff(this, other) / 60;
    return diff;
}

i64
game_time_diff_hours(GameTime this, GameTime other)
{
    i64 diff = game_time_diff_minutes(this, other) / 60;
    return diff;
}

i64
game_time_diff_days(GameTime this, GameTime other)
{
    i64 diff = game_time_diff_hours(this, other) / 24;
    return diff;
}

i64
game_time_diff_years(GameTime this, GameTime other)
{
    i64 diff = game_time_diff_days(this, other) / 365;
    return diff;
}

i64
game_time_get_total_seconds_short(GameTime gt)
{
    /*
      DOCS(typedef)
      Minutes: 60         sec
      Hours:   3600       sec
      Day:     86400      sec
      Month:   2 678 400  sec
      Year:    32140800   sec
    */
    i64 result = gt.Hours * 3600 + gt.Minutes * 60 + gt.Seconds;
    return result;
}

i64
game_time_get_total_seconds(GameTime gt)
{
    i64 result = Hours(gt.Hours) + Minutes(gt.Minutes) + gt.Seconds
	+ Days(gt.Day)
	+ SecondsInMonth[gt.Month]
	+ Years(gt.Year);
    return result;
}
