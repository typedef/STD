#include "GameWorld.h"

#include <AssetManager/AssetManager.h>
#include <Core/Ai/Base/AiContext.h>
#include <Core/Components/AllComponents.h>
#include <EntitySystem/SimpleEcs.h>
#include <EntitySystem/Components/Base/TransformComponent.h>
#include <EntitySystem/Components/3D/StaticModelComponent.h>
#include <Math/SimpleMath.h>
#include <Utils/SimpleStandardLibrary.h>

#include <Utils/Guard.h>

void
temp_game_time_printer(GameTime* time)
{
#if 0
    printf("\r");
    if (time->Seconds < 10)
    {
	printf("%d:%d:%d%d %d.%d.%d",
	       time->Hours, time->Minutes, 0, time->Seconds,
	       time->Day, time->Month, time->Year);
    }
    else
    {
	printf("%d:%d:%d %d.%d.%d",
	       time->Hours, time->Minutes, time->Seconds,
	       time->Day, time->Month, time->Year);
    }

    fflush(stdout);
#endif
}

void
_game_world_ecs_init(GameWorldSettings set)
{
    ecs_world_register_component(set.pEcsWorld, MindComponent);
    ecs_world_register_component(set.pEcsWorld, TransformComponent);
    ecs_world_register_component(set.pEcsWorld, StaticModelComponent);
}

void
_game_population_init(GameWorldSettings set)
{
    EntityId firstPersonEntity = ecs_world_entity(set.pEcsWorld, "First Person");

    MindComponentSettings mindSet = {
	.CurrentEntity = firstPersonEntity,
    };
    MindComponent mind = mind_create(mindSet);
    ecs_world_entity_add_component(set.pEcsWorld, firstPersonEntity, MindComponent, mind);

    TransformComponent tc = transform_component_new(v3_new(0, 0, 0), v3_new(0, 0, 0), v3_new(1, 1, 1));
    ecs_world_entity_add_component(set.pEcsWorld, firstPersonEntity, TransformComponent, tc);

    StaticModelComponent smc = {
	.Model = *((StaticModel*) asset_manager_load_asset(AssetType_StaticModel, asset_model("AnimatedModel/AnimatedModel.gltf")))
    };
    ecs_world_entity_add_component(set.pEcsWorld, firstPersonEntity, StaticModelComponent, smc);
}

GameWorld
game_world_create(GameWorldSettings set)
{
    ai_context_init();
    _game_world_ecs_init(set);
    _game_population_init(set);

    GameWorld gameWorld = {
	.TimeMultiplier = set.TimeMultiplier,
	.CurrentDate = set.StartDate,
	.pEcsWorld = set.pEcsWorld
    };

    GameTimeSecondsChangedDelegate delegate;
    if (!game_time_subsribe_seconds_delegate(temp_game_time_printer))
    {
	GERROR("Can't subscribe seconds delegate!\n");
	vassert_break();
    }

    return gameWorld;
}

void
game_world_update(GameWorld* gameWorld, f32 timestep)
{
    // NOTE(typedef): updating GameWorld time
    gameWorld->WorldTimestep = gameWorld->TimeMultiplier * timestep;
    const f32 toMs = 1000;
    game_time_add_ms(&gameWorld->CurrentDate, gameWorld->WorldTimestep * toMs);

    // EcsQuery query = ecs_get_query(TransformComponent, StaticModelComponent);

    MindComponent* mind = GetComponentRecordData(MindComponent);
    i32 i, count = array_count(mind);
    for (i = 0; i < count; ++i)
    {
	MindComponent* pMind = &mind[i];
	mind_update(pMind);
    }

}


void
game_world_destroy(GameWorld gameWorld)
{
    printf("\n");
}
