#ifndef GAME_TIME_H
#define GAME_TIME_H

#include <Utils/Types.h>

#define Minutes(m) (60 * (m))
#define Hours(h) (60 * Minutes(h))
#define Days(d) (24 * Hours(d))
#define Years(y) (365 * Hours(y))


typedef struct GameTime
{
    i8 Hours;
    i8 Minutes;
    i8 Seconds;
    i8 Day;
    i8 Month;
    i16 Year;
    f32 MS;
} GameTime;

typedef void (*GameTimeSecondsChangedDelegate)(GameTime* time);

/* 6 20 30 1 1 1980*/
GameTime game_time_new_full(i8 h, i8 m, i8 s, i8 day, i8 month, i16 year);
GameTime game_time_new(i8 h, i8 m, i8 s);

i8 game_time_subsribe_seconds_delegate(GameTimeSecondsChangedDelegate delegate);
i8 game_time_unsubsribe_seconds_delegate(GameTimeSecondsChangedDelegate delegate);
void game_time_destroy_seconds_delegats();

i32  game_time_close_to_with_diff(GameTime this, GameTime other, i32 seconds);
void game_time_add_second(GameTime* time, i8 second);
void game_time_add_ms(GameTime* time, f32 ms);
void game_time_to_short_string(char* buf, GameTime gameTime);
void game_time_to_string(char* buf, GameTime gameTime);

i64 game_time_diff(GameTime this, GameTime other);
i64 game_time_diff_minutes(GameTime this, GameTime other);
i64 game_time_diff_hours(GameTime this, GameTime other);
i64 game_time_diff_days(GameTime this, GameTime other);
i64 game_time_diff_years(GameTime this, GameTime other);

i64 game_time_get_total_seconds_short(GameTime gt);
i64 game_time_get_total_seconds(GameTime gt);

#endif // GAME_TIME_H
