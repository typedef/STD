#ifndef GAME_WORLD_H
#define GAME_WORLD_H

#include <Utils/Types.h>

#include "GameTime.h"

struct EcsWorld;

typedef struct GameWorldSettings
{
    i32 TimeMultiplier;
    GameTime StartDate;
    struct EcsWorld* pEcsWorld;
} GameWorldSettings;

typedef struct GameWorld
{
    i32 TimeMultiplier;
    GameTime CurrentDate;
    struct EcsWorld* pEcsWorld;

    f32 WorldTimestep;

} GameWorld;

GameWorld game_world_create(GameWorldSettings settings);
void game_world_update(GameWorld* gameWorld, f32 timestep);

void game_world_destroy(GameWorld gameWorld);

#endif // GAME_WORLD_H
