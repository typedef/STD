#ifndef AI_CONTEXT_H
#define AI_CONTEXT_H

struct AiData;
enum ActivityType;
enum ModuleType;
struct Module;

/*
  DOCS(typedef): Во внешний мир от активностей мы кидаем только этот делегат
*/
typedef void (*ActivityDelegate)(struct AiData* activityData);

void ai_context_init();

ActivityDelegate ai_context_get_activity(enum ActivityType type);
struct Module ai_context_get_module(enum ModuleType type);

#endif // AI_CONTEXT_H
