#ifndef AI_CONTEXT_ACTIVITY_REGISTRATION_H
#define AI_CONTEXT_ACTIVITY_REGISTRATION_H

struct AiData;
typedef void (*ActivityDelegate)(struct AiData* activityData);

void ai_context_activity_register_all(ActivityDelegate* pActivityRegistration);

#endif // AI_CONTEXT_ACTIVITY_REGISTRATION_H
