#include "Needs.h"

Needs
needs_create()
{
    Needs needs = {
	.Food      = NEEDS_MAX,
	.Toilet    = NEEDS_MAX,
	.Social    = NEEDS_MAX,
	.Energy    = NEEDS_MAX,
	.Hygiene   = NEEDS_MAX,
	.Fun       = NEEDS_MAX,
	.Instincts = NEEDS_MAX / 2,
	.Ambitions = NEEDS_MAX / 2
    };

    return needs;
}

void
needs_update(Needs* needs)
{
    needs->Food      = MinMax(needs->Food     , 0, NEEDS_MAX);
    needs->Toilet    = MinMax(needs->Toilet   , 0, NEEDS_MAX);
    needs->Social    = MinMax(needs->Social   , 0, NEEDS_MAX);
    needs->Energy    = MinMax(needs->Energy   , 0, NEEDS_MAX);
    needs->Hygiene   = MinMax(needs->Hygiene  , 0, NEEDS_MAX);
    needs->Fun       = MinMax(needs->Fun      , 0, NEEDS_MAX);
    needs->Instincts = MinMax(needs->Instincts, 0, NEEDS_MAX);
    needs->Ambitions = MinMax(needs->Ambitions, 0, NEEDS_MAX);
}

i32
needs_is_any_critical(Needs* pNeeds, struct NeedsEffects* effects)
{
    const f32 defaultPercentThreshold = NeedsPercent(10);

    for (i32 i = 0; i < NeedsType_Count; ++i)
    {
	f32 value = pNeeds->AsArray[i];
	if (value < defaultPercentThreshold)
	    return 1;
    }

    return 0;
}
