#ifndef NEEDS_H
#define NEEDS_H

#include <Utils/Types.h>

#define NEEDS_MAX 100.0f
//BUG(typedef): mb we dont need this, would it be 100.0f always or ..
#define NeedsPercent(p) ((100.0f / NEEDS_MAX) * p)

typedef enum NeedsType
{
    NeedsType_Food = 0,
    NeedsType_Toilet,
    NeedsType_Social,
    NeedsType_Energy,
    NeedsType_Hygiene,
    NeedsType_Fun,
    NeedsType_Instincts,
    NeedsType_Ambitions,

    NeedsType_Count
} NeedsType;

typedef union Needs
{
    struct
    {
	/* Initialize range 0 .. NEEDS_MAX */
	f32 Food;
	f32 Toilet;
	f32 Social;
	f32 Energy;
	f32 Hygiene;
	f32 Fun;
	f32 Instincts;
	f32 Ambitions;
    };

    f32 AsArray[NeedsType_Count];

} Needs;


/*
  DOCS(typedef): Default effects behaviour
  Percent: 0 <= Needs.Toilet <= 1.0f
  Value  : 1.0f < Needs.Toilet <= NEEDS_MAX
*/
struct NeedsEffects
{
    Needs Needs;
};

Needs needs_create();
void needs_update(Needs* needs);
i32 needs_is_any_critical(Needs* pNeeds, struct NeedsEffects* effects);

#endif // NEEDS_H
