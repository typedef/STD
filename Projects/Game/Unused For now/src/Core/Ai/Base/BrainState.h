#ifndef BRAIN_STATE_H
#define BRAIN_STATE_H

typedef struct BrainState
{
    f32 Corruption;
    f32 Lust;
    f32 Goodness;
} BrainState;

#endif // BRAIN_STATE_H
