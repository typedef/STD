#include "AiContext.h"

#include <Utils/SimpleStandardLibrary.h>

#include <Core/Components/Ai/MindComponent.h>
#include <Core/Ai/Module/Module.h>
#include <Core/Ai/Module/ModuleType.h>

static AiContextData ContextData;
static Module RegisteredModules[ModuleType_Count];
static ActivityDelegate RegisteredActivities[ActivityType_Count];

// DOCS(typedef): this should be always after Registered* part
#include "AiContextActivityRegistration.h"
#include "AiContextModuleRegistration.h"

void
ai_context_init()
{
    AiContextData contextData = {
	.OnInterruptDelegate = mind_interrupt
    };
    ContextData = contextData;

    ai_context_activity_register_all((ActivityDelegate*) RegisteredActivities);
    ai_context_module_register_all((Module*) &RegisteredModules, &ContextData);
}

ActivityDelegate
ai_context_get_activity(ActivityType type)
{
    vassert_not_null(RegisteredActivities[type] && "Activity delegate is not registered!");
    return RegisteredActivities[type];
}

Module
ai_context_get_module(ModuleType type)
{
    return RegisteredModules[type];
}
