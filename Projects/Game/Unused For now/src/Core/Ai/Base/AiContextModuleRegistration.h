#ifndef AI_CONTEXT_MODULE_REGISTRATION_H
#define AI_CONTEXT_MODULE_REGISTRATION_H

struct Module;
struct AiContextData;

void ai_context_module_register_all(struct Module* pRegisteredModules, struct AiContextData* pAiContextData);

#endif // AI_CONTEXT_MODULE_REGISTRATION_H
