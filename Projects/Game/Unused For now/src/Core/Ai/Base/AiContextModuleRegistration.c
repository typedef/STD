#include "AiContextModuleRegistration.h"

#include <Core/Ai/Module/ModuleType.h>
#include <Core/Ai/Module/Module.h>
#include <Core/Ai/Base/AiData.h>
#include <Utils/Guard.h>

// DOCS(typedef): Base modules, goes in urgent queue
#include <Core/Ai/Module/Modules/Base/FoodModule.h>
#include <Core/Ai/Module/Modules/Base/FunModule.h>
#include <Core/Ai/Module/Modules/Base/HygieneModule.h>
#include <Core/Ai/Module/Modules/Base/IdleModule.h>
#include <Core/Ai/Module/Modules/Base/PhoneModule.h>
#include <Core/Ai/Module/Modules/Base/SleepModule.h>
#include <Core/Ai/Module/Modules/Base/ToiletModule.h>
#include <Core/Ai/Module/Modules/Base/WorkModule.h>

typedef void (*ModuleCreateDelegate)(Module* pModule, ModuleCreateSettings* pSettings);

void
ai_context_module_register(Module* pRegisteredModules, ModuleType type, ModuleCreateDelegate moduleCreate, AiContextData* pAiContextData)
{
    //BUG(typedef): we are here
    Module newModule = {};
    ModuleCreateSettings set = {
	.OnInterrupt = pAiContextData->OnInterruptDelegate
    };
    moduleCreate(&newModule, &set);
    pRegisteredModules[type] = newModule;
}

void
ai_context_module_register_all(Module* pRegisteredModules, AiContextData* pAiContextData)
{
    ai_context_module_register(pRegisteredModules, ModuleType_Idle, idle_module_create, pAiContextData);
    ai_context_module_register(pRegisteredModules, ModuleType_Go, idle_module_create, pAiContextData);
    ai_context_module_register(pRegisteredModules, ModuleType_Sleep,   sleep_module_create, pAiContextData);
    ai_context_module_register(pRegisteredModules, ModuleType_Toilet,  toilet_module_create, pAiContextData);
    ai_context_module_register(pRegisteredModules, ModuleType_Food,    food_module_create, pAiContextData);
    ai_context_module_register(pRegisteredModules, ModuleType_Hygiene, hygiene_module_create, pAiContextData);

    ai_context_module_register(pRegisteredModules, ModuleType_Work, work_module_create, pAiContextData);
    ai_context_module_register(pRegisteredModules, ModuleType_Fun,   fun_module_create, pAiContextData);

    ai_context_module_register(pRegisteredModules, ModuleType_Instincts,   fun_module_create, pAiContextData);
    ai_context_module_register(pRegisteredModules, ModuleType_Ambitions,   fun_module_create, pAiContextData);

    ai_context_module_register(pRegisteredModules, ModuleType_Phone, phone_module_create, pAiContextData);

    for (i32 i = ModuleType_Idle; i < ModuleType_Count; ++i)
    {
	Module module = pRegisteredModules[i];
	//GERROR("%s\n", module_type_to_string(i));

	vassert(module.OnUpdate && module.OnInterrupt && "Module registration failed, not all modules registered!!!");
    }
}
