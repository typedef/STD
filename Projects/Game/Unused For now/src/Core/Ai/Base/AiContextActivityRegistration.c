#include "AiContextActivityRegistration.h"

#include <Utils/Types.h>
#include <Utils/Guard.h>

#include <Core/Ai/Base/AiData.h>
#include <Core/Ai/Activity/ActivityType.h>
#include <Core/Ai/Activity/Base/Bath.h>
#include <Core/Ai/Activity/Base/Food.h>
#include <Core/Ai/Activity/Base/Fun.h>
#include <Core/Ai/Activity/Base/Go.h>
#include <Core/Ai/Activity/Base/Idle.h>
#include <Core/Ai/Activity/Base/Sleep.h>
#include <Core/Ai/Activity/Base/Toilet.h>
#include <Core/Ai/Activity/Base/Work.h>

void
ai_context_activity_register(ActivityDelegate* pActivityRegistration, ActivityType type, ActivityDelegate delegate)
{
    vassert_not_null(delegate);

    pActivityRegistration[type] = delegate;
}

void
ai_context_activity_register_all(ActivityDelegate* pRegisteredActivities)
{
    // ActivityType_Idle
    ai_context_activity_register(pRegisteredActivities, ActivityType_Idle, idle_activity_update);

    ai_context_activity_register(pRegisteredActivities, ActivityType_Go_Walk, go_walk_activity_update);

    // ActivityType_Sleep
    ai_context_activity_register(pRegisteredActivities, ActivityType_Sleep_Begin, sleep_activity_update);
    ai_context_activity_register(pRegisteredActivities,  ActivityType_Sleep_FallASleep, sleep_activity_update);
    ai_context_activity_register(pRegisteredActivities, ActivityType_Sleep, sleep_activity_update);
    ai_context_activity_register(pRegisteredActivities,  ActivityType_Sleep_WakeUp, sleep_activity_update);
    ai_context_activity_register(pRegisteredActivities,  ActivityType_Sleep_End, sleep_activity_update);

    // ActivityType_Toilet
    ai_context_activity_register(pRegisteredActivities, ActivityType_Toilet, toilet_activity_update);

    // ActivityType_Food
    ai_context_activity_register(pRegisteredActivities, ActivityType_Food, food_activity_update);

    // ActivityType_Bath0
    ai_context_activity_register(pRegisteredActivities, ActivityType_Bath, bath_activity_update);

    // ActivityType_Fun
    ai_context_activity_register(pRegisteredActivities, ActivityType_Fun_Find      , idle_activity_update);
    ai_context_activity_register(pRegisteredActivities, ActivityType_Fun           , fun_activity_update);
    ai_context_activity_register(pRegisteredActivities, ActivityType_Fun_Talk      , fun_talk_activity_update);
    ai_context_activity_register(pRegisteredActivities, ActivityType_Fun_Phone_Talk, fun_phone_talk_activity_update);

    ai_context_activity_register(pRegisteredActivities, ActivityType_Instincts, fun_activity_update);
    ai_context_activity_register(pRegisteredActivities, ActivityType_Ambitions, fun_activity_update);

    // ActivityType_Phone
    ai_context_activity_register(pRegisteredActivities, ActivityType_Phone_Begin   , idle_activity_update);
    ai_context_activity_register(pRegisteredActivities, ActivityType_Phone_Talk    , idle_activity_update);
    ai_context_activity_register(pRegisteredActivities, ActivityType_Phone_Talk_Fun, fun_phone_talk_activity_update);
    ai_context_activity_register(pRegisteredActivities, ActivityType_Phone_End     , idle_activity_update);

    // NOTE(bies): Placeholders for now
    ai_context_activity_register(pRegisteredActivities, ActivityType_Work0, work0_activity_update);
    ai_context_activity_register(pRegisteredActivities, ActivityType_HomeSweetHome, work0_activity_update);

    for (i32 i = ActivityType_Idle; i < ActivityType_Count; ++i)
    {
	ActivityDelegate activityDelegate = pRegisteredActivities[i];
	if (activityDelegate == NULL)
	{
	    printf("Activity Not Registered: %s\n", activity_type_to_string((ActivityType)i));
	    vassert(0 && "Activity registration failed, not all activity type registered!!!");
	}
    }
}
