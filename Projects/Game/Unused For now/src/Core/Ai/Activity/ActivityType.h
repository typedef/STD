#ifndef ACTIVITY_TYPE_H
#define ACTIVITY_TYPE_H

typedef enum ActivityType
{
    ActivityType_Idle = 0,

    ActivityType_Go_Walk,


    /*###################################
      ###################################
		Urgent Modules
      ###################################
      ###################################*/

    // Activities for sleep
    ActivityType_Sleep_Begin,
    ActivityType_Sleep_FallASleep,
    ActivityType_Sleep,
    ActivityType_Sleep_WakeUp,
    ActivityType_Sleep_End,

    ActivityType_Toilet,

    ActivityType_Food,

    ActivityType_Bath,

    ActivityType_Fun,
    ActivityType_Fun_Find,
    ActivityType_Fun_Talk,
    ActivityType_Fun_Phone_Talk,

    ActivityType_Instincts,
    ActivityType_Ambitions,
    /*###################################
      ###################################
	      Urgent Modules End
      ###################################
      ###################################*/

    ActivityType_Work0,
    ActivityType_HomeSweetHome,

    ActivityType_Phone_Begin,
    ActivityType_Phone_Talk,
    ActivityType_Phone_Talk_Fun,
    ActivityType_Phone_End,

    ActivityType_Count
} ActivityType;

const char* activity_type_to_string(ActivityType activityType);

#endif // ACTIVITY_TYPE_H
