void
toilet_activity_update(AiData* ad)
{
    const f32 ToiletPointStep  = 100.0f / (0.1f * 60 * 60);

    ad->Needs->Toilet  += ad->WorldTimestep * ToiletPointStep;
    ad->Needs->Hygiene -= ad->WorldTimestep * (ToiletPointStep * 10);
    ad->Needs->Fun     += ad->WorldTimestep * ToiletPointStep;
}
