void
go_walk_activity_update(AiData* ad)
{
    const f32 GoPointStep = 100.0f / (12.0f * 60 * 60);

    ad->Needs->Food    -= ad->WorldTimestep * GoPointStep;
    ad->Needs->Toilet  -= ad->WorldTimestep * GoPointStep;
    ad->Needs->Social  -= ad->WorldTimestep * GoPointStep;
    ad->Needs->Energy  -= ad->WorldTimestep * GoPointStep;
    ad->Needs->Hygiene -= ad->WorldTimestep * GoPointStep;
    ad->Needs->Fun     -= ad->WorldTimestep * GoPointStep;
}
