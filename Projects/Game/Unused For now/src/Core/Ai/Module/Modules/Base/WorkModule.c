#include "WorkModule.h"

#include <Core/Ai/Module/Module.h>
#include <Core/Ai/Activity/ActivityType.h>
#include <Core/Ai/Base/AiData.h>

void
work_module_update(Module* module, AiData* pAiData)
{
    // NOTE(typedef): for now doing nothing
    module->OnInterrupt(module, pAiData);
}

void
work_module_create(Module* pModule, ModuleCreateSettings* pSettings)
{
    ModuleSettings set = {
	.Type = ModuleType_Work,
	.CurrentActivity = ActivityType_Work0,
	.OnUpdate = work_module_update,
	.OnInterrupt = pSettings->OnInterrupt
    };

    Module module = module_create(set);
    *pModule = module;
}
