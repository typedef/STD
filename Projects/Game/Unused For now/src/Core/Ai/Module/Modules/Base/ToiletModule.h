#ifndef TOILET_MODULE_H
#define TOILET_MODULE_H

struct Module;
struct ModuleCreateSettings;

void toilet_module_create(struct Module* pModule, struct ModuleCreateSettings* pSettings);

#endif // TOILET_MODULE_H
