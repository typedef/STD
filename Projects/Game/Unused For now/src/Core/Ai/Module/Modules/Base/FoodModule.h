#ifndef FOOD_MODULE_H
#define FOOD_MODULE_H

struct Module;
struct ModuleCreateSettings;

void food_module_create(struct Module* pModule, struct ModuleCreateSettings* pSettings);

#endif // FOOD_MODULE_H
