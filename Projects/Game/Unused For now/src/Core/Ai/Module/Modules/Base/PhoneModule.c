#include "PhoneModule.h"

#include <Core/Ai/Module/Module.h>
#include <Core/Ai/Activity/ActivityType.h>
#include <Core/Ai/Base/AiData.h>

void
phone_module_update(Module* module, AiData* pAiData)
{
    module->OnInterrupt(module, pAiData);
}

void
phone_module_create(Module* pModule, ModuleCreateSettings* pSettings)
{
    ModuleSettings set = {
	.Type = ModuleType_Phone,
	.CurrentActivity = ActivityType_Phone_Begin,
	.OnUpdate = phone_module_update,
	.OnInterrupt = pSettings->OnInterrupt
    };

    Module module = module_create(set);
    *pModule = module;
}
