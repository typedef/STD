#include "SleepModule.h"

#include <Core/Ai/Module/Module.h>
#include <Core/Ai/Activity/ActivityType.h>
#include <Core/Ai/Base/AiData.h>

void
sleep_module_update(Module* module, AiData* pAiData)
{
    switch (module->CurrentActivity)
    {
    case ActivityType_Sleep_Begin:
	module->CurrentActivity = ActivityType_Sleep_FallASleep;
	break;
    case ActivityType_Sleep_FallASleep:
	if (pAiData->Needs->Energy >= NeedsPercent(12))
	    module->CurrentActivity = ActivityType_Sleep;
	break;
    case ActivityType_Sleep:
	if (pAiData->Needs->Energy >= NeedsPercent(97))
	    module->CurrentActivity = ActivityType_Sleep_WakeUp;
	break;
    case ActivityType_Sleep_WakeUp:
	if (pAiData->Needs->Energy >= NeedsPercent(95))
	    module->CurrentActivity = ActivityType_Sleep_End;
	break;
    case ActivityType_Sleep_End:
	module->OnInterrupt(module, pAiData);
	break;
    }
}

void
sleep_module_create(Module* pModule, ModuleCreateSettings* pSettings)
{
    ModuleSettings set = {
	.Type = ModuleType_Sleep,
	.CurrentActivity = ActivityType_Sleep_Begin,
	.OnUpdate = sleep_module_update,
	.OnInterrupt = pSettings->OnInterrupt
    };

    Module module = module_create(set);
    *pModule = module;
}
