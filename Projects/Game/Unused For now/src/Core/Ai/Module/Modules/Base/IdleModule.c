#include "IdleModule.h"
#include "Core/Ai/Base/Needs.h"

#include <Core/Ai/Module/Module.h>
#include <Core/Ai/Activity/ActivityType.h>
#include <Core/Ai/Base/AiData.h>


void
idle_module_update(Module* module, AiData* pAiData)
{
    //Empty
    if (pAiData->Needs->Toilet < NeedsPercent(80))
	module->OnInterrupt(module, pAiData);
}

void
idle_module_create(Module* pModule, ModuleCreateSettings* pSettings)
{
    ModuleSettings set = {
	.Type = ModuleType_Idle,
	.CurrentActivity = ActivityType_Idle,
	.OnUpdate = idle_module_update,
	.OnInterrupt = pSettings->OnInterrupt
    };

    Module module = module_create(set);
    *pModule = module;
}
