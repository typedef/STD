#ifndef FUN_MODULE_H
#define FUN_MODULE_H

struct Module;
struct ModuleCreateSettings;

void fun_module_create(struct Module* pModule, struct ModuleCreateSettings* pSettings);

#endif // FUN_MODULE_H
