#ifndef SLEEP_MODULE_H
#define SLEEP_MODULE_H

struct Module;
struct ModuleCreateSettings;

void sleep_module_create(struct Module* pModule, struct ModuleCreateSettings* pSettings);

#endif // SLEEP_MODULE_H
