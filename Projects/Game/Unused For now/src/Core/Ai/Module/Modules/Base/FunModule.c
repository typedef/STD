#include "FunModule.h"

#include <Core/Ai/Module/Module.h>
#include <Core/Ai/Activity/ActivityType.h>
#include <Core/Ai/Base/AiData.h>

void
fun_module_update(Module* module, AiData* pAiData)
{
    module->OnInterrupt(module, pAiData);
}

void
fun_module_create(Module* pModule, ModuleCreateSettings* pSettings)
{
    ModuleSettings set = {
	.Type = ModuleType_Fun,
	.CurrentActivity = ActivityType_Fun_Find,
	.OnUpdate = fun_module_update,
	.OnInterrupt = pSettings->OnInterrupt
    };

    Module module = module_create(set);
    *pModule = module;
}
