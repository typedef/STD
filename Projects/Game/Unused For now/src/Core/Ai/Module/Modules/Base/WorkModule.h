#ifndef WORK_MODULE_H
#define WORK_MODULE_H

struct Module;
struct ModuleCreateSettings;

void work_module_create(struct Module* pModule, struct ModuleCreateSettings* pSettings);

#endif // WORK_MODULE_H
