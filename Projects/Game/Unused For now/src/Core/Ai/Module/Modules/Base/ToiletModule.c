#include "ToiletModule.h"

#include <Core/Ai/Module/Module.h>
#include <Core/Ai/Activity/ActivityType.h>
#include <Core/Ai/Base/AiData.h>

void
toilet_module_update(Module* module, AiData* pAiData)
{
    if (pAiData->Needs->Toilet >= NeedsPercent(95))
	module->OnInterrupt(module, pAiData);
}

void
toilet_module_create(Module* pModule, ModuleCreateSettings* pSettings)
{
    ModuleSettings set = {
	.Type = ModuleType_Toilet,
	.CurrentActivity = ActivityType_Toilet,
	.OnUpdate = toilet_module_update,
	.OnInterrupt = pSettings->OnInterrupt
    };

    Module module = module_create(set);
    *pModule = module;
}
