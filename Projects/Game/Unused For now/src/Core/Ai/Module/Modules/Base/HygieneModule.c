#include "HygieneModule.h"

#include <Core/Ai/Module/Module.h>
#include <Core/Ai/Activity/ActivityType.h>
#include <Core/Ai/Base/AiData.h>

void
hygiene_module_update(Module* module, AiData* pAiData)
{
    if (pAiData->Needs->Hygiene >= NeedsPercent(95))
	module->OnInterrupt(module, pAiData);
}

void
hygiene_module_create(Module* pModule, ModuleCreateSettings* pSettings)
{
    ModuleSettings set = {
	.Type = ModuleType_Hygiene,
	.CurrentActivity = ActivityType_Bath,
	.OnUpdate = hygiene_module_update,
	.OnInterrupt = pSettings->OnInterrupt
    };

    Module module = module_create(set);
    *pModule = module;
}
