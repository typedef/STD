#ifndef PHONE_MODULE_H
#define PHONE_MODULE_H

struct Module;
struct ModuleCreateSettings;

void phone_module_create(struct Module* pModule, struct ModuleCreateSettings* pSettings);

#endif // PHONE_MODULE_H
