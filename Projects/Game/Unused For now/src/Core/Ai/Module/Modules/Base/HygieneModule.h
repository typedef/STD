#ifndef HYGIENE_MODULE_H
#define HYGIENE_MODULE_H

struct Module;
struct ModuleCreateSettings;

void hygiene_module_create(struct Module* pModule, struct ModuleCreateSettings* pSettings);

#endif // HYGIENE_MODULE_H
