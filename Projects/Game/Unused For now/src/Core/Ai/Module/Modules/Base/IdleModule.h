#ifndef IDLE_MODULE_H
#define IDLE_MODULE_H

struct Module;
struct ModuleCreateSettings;

void idle_module_create(struct Module* pModule, struct ModuleCreateSettings* pSettings);

#endif // IDLE_MODULE_H
