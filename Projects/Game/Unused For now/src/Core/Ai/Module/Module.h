#ifndef MODULE_H
#define MODULE_H

#include <Utils/Types.h>
#include <Core/Ai/Module/ModuleType.h>
#include <Core/Ai/Activity/ActivityType.h>

struct Module;
struct AiData;

typedef void (*ModuleOnUpdateDelegate) (struct Module* module, struct AiData* pAiData);
typedef void (*ModuleOnInterruptDelegate)(struct Module* module, struct AiData* pAiData);

typedef struct ModuleCreateSettings
{
    ModuleOnInterruptDelegate OnInterrupt;
} ModuleCreateSettings;

//NOTE() PLace it here for now
typedef struct AiContextData
{
    ModuleOnInterruptDelegate OnInterruptDelegate;
} AiContextData;

typedef struct ModuleSettings
{
    enum ModuleType Type;
    enum ActivityType CurrentActivity;
    ModuleOnUpdateDelegate OnUpdate;
    ModuleOnInterruptDelegate OnInterrupt;
} ModuleSettings;

typedef struct Module
{
    enum ModuleType Type;
    enum ActivityType CurrentActivity;

    // NOTE(typedef): some delegates
    ModuleOnUpdateDelegate OnUpdate;
    ModuleOnInterruptDelegate OnInterrupt;
} Module;

struct Module module_create(struct ModuleSettings);
i32 module_get_priority(enum ModuleType);
void module_update(struct Module*, struct AiData*);
void module_register_all();

#endif // MODULE_H
