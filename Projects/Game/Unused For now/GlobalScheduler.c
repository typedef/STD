#include "GlobalScheduler.h"

#include "Person.h"

/*

		 #####################################
		 #####################################
			   GlobalScheduler.h
		 #####################################
		 #####################################
*/
void
scheduler_table_add_person(SchedulerTable* schedulerTable, PersonID personID, ModuleType moduleType)
{
    array_push(schedulerTable->IDs, personID);
    array_push(schedulerTable->Modules, moduleType);
}
void
scheduler_table_remove_person(SchedulerTable* schedulerTable, PersonID personID)
{
    i32 count = array_count(schedulerTable->IDs);
    array_for(count,
	      PersonID id = schedulerTable->IDs[i];
	      if (id == personID)
	      {
		  array_remove_at(schedulerTable->IDs, i);
		  array_remove_at(schedulerTable->Modules, i);
		  return;
	      });
}
i32
global_scheduler_get_possible_index_of_time(GlobalScheduler* globalScheduler, GameTime eventTime)
{
    i32 i,
	count = array_count(globalScheduler->Times);
    i64 eventTotalSeconds = game_time_get_total_seconds_short(eventTime);
    for (i = 0; i < count; ++i)
    {
	GameTime gameTime = globalScheduler->Times[i];
	i64 gameTimeTotalSeconds = game_time_get_total_seconds_short(gameTime);

	if (eventTotalSeconds < gameTimeTotalSeconds)
	{
	    return i;
	}
    }

    return -1;
}
i32
global_scheduler_get_index_of_time(GlobalScheduler* globalScheduler, GameTime eventTime)
{
    i32 i,
	count = array_count(globalScheduler->Times);
    i64 eventTotalSeconds = game_time_get_total_seconds(eventTime);
    for (i = 0; i < count; ++i)
    {
	GameTime gameTime = globalScheduler->Times[i];
	i64 gameTimeTotalSeconds = game_time_get_total_seconds(gameTime);

	if (StructEquals(gameTime, eventTime))
	{
	    return i;
	}
    }

    return -1;
}
void
global_scheduler_calculate_current_index(GlobalScheduler* globalScheduler, GameTime currentTime)
{
    // GameTime(8 , 30, 0)
    // GameTime(17, 30, 0)

    i32 minIndex = 0;
    i64 minValue = 123456789123;

    i64 i64_abs(i64 input) { return input < 0 ? -input : input; }

    array_foreach(
	globalScheduler->Times,
	i64 diff;
	if (item.Year == 0)
	{
	    diff = i64_abs(game_time_get_total_seconds_short(currentTime)
		       - game_time_get_total_seconds_short(item));
	}
	else
	{
	    diff = i64_abs(game_time_get_total_seconds(currentTime)
		       - game_time_get_total_seconds(item));
	}

	if (diff < minValue)
	{
	    minIndex = i;
	    minValue = diff;
	}
	);

    globalScheduler->CurrentTimeIndex = minIndex;
    globalScheduler->PreviousTimeIndex = minIndex - 1;
}
void
global_scheduler_set_next_index(GlobalScheduler* globalScheduler)
{
    globalScheduler->PreviousTimeIndex = globalScheduler->CurrentTimeIndex;
    ++globalScheduler->CurrentTimeIndex;
    i32 timesCount = array_count(globalScheduler->Times);
    if (globalScheduler->CurrentTimeIndex >= timesCount)
    {
	globalScheduler->CurrentTimeIndex = 0;
    }
}
void
global_scheduler_update(VirtualWorld* world, GlobalScheduler* globalScheduler, GameTime currentTime)
{
    GameTime schedulerNextTime =
	globalScheduler->Times[globalScheduler->CurrentTimeIndex];

    if (!game_time_close_to_with_diff(schedulerNextTime, currentTime, GT_MINUTES(30)))
    {
	return;
    }

    //GERROR("Curr: %d Prev: %d!\n", globalScheduler->CurrentTimeIndex, globalScheduler->PreviousTimeIndex);

    SchedulerTable table =
	globalScheduler->Tables[globalScheduler->CurrentTimeIndex];

    array_foreach(table.IDs,
		  person_add_module(&world->Population[item], table.Modules[i]););

    global_scheduler_set_next_index(globalScheduler);
}
void
global_scheduler_register(GlobalScheduler* globalScheduler, GameTime eventTime, PersonID id, ModuleType moduleType)
{
    i32 ind = global_scheduler_get_index_of_time(globalScheduler, eventTime);
    if (ind == -1)
    {
	SchedulerTable newTable = SchedulerTable();
	scheduler_table_add_person(&newTable, id, moduleType);

	ind = global_scheduler_get_possible_index_of_time(globalScheduler, eventTime);
	if (ind == -1)
	{
	    array_push(globalScheduler->Times, eventTime);
	    array_push(globalScheduler->Tables, newTable);
	}
	else
	{
	    array_push_at(globalScheduler->Times, ind, eventTime);
	    array_push_at(globalScheduler->Tables, ind, newTable);
	}
    }
    else
    {
	scheduler_table_add_person(&globalScheduler->Tables[ind], id, moduleType);
    }
}
void
global_scheduler_default_worker(GlobalScheduler* globalScheduler, PersonID id)
{
    global_scheduler_register(globalScheduler, GameTime(8, 29, 0), id, ModuleType_Hygiene);
    global_scheduler_register(globalScheduler, GameTime(8, 30, 0), id, ModuleType_Sleep);
    global_scheduler_register(globalScheduler, GameTime(8, 31, 0), id, ModuleType_Food);
    global_scheduler_register(globalScheduler, GameTime(8, 32, 0), id, ModuleType_Work);
    global_scheduler_register(globalScheduler, GameTime(17, 30, 0), id, ModuleType_HomeSweetHome);
}
void
global_scheduler_destroy(GlobalScheduler* scheduler)
{
    array_free(scheduler->Times);
    array_free(scheduler->Tables);
}
