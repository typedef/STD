#include "HelmaEnumGenerator.h"

#include "Helma.h"
#include "HelmaParserHelper.h"

HelmaEnumType*
helma_get_enum_type(const char* codeWEnum)
{
    HelmaEnumType* result = NULL;

#define optr_skip(ptr)				\
    ({						\
	char* optr = ptr;			\
	while (*optr != '\n' && *optr != '\0')	\
	{					\
	    ++optr;				\
	}					\
	optr;					\
    })

    char* ptr = (char*) codeWEnum;
    char c = *ptr;
    while (*ptr != '\0')
    {
	if (c == 't' && string_compare_length(ptr, "typedef", 7))
	{
	    char** enumItems = NULL;
	    char* enumName = NULL;

	    ptr += 8;
	    if (string_compare_length(ptr, "enum", 4))
	    {
		ptr += 5;

		char* optr = optr_skip(ptr);
		size_t length = optr - ptr;
		enumName = string_copy(ptr, length);

		skip_to_char(ptr, '{');
		++ptr;

		c = *ptr;
		while (c != '}')
		{
		    if (_is_char(c))
		    {
			optr = ptr;
			while (_is_enum_acceptable(*optr))
			    ++optr;

			length = optr - ptr;
			char* enumItem = string_copy(ptr, length);
			array_push(enumItems, enumItem);

			ptr += length;
		    }
		    else if (c == '/')
		    {
			skip_comments(ptr);
		    }
		    else if (c == ' ' || c == '\n' || c == '\t')
		    {
			while (c == ' ' || c == '\n' || c == '\t')
			{
			    ++ptr;
			    c = *ptr;
			}
			--ptr;
		    }

		    ++ptr;
		    c = *ptr;
		}

		vassert(array_count(enumItems) > 0 && "Enum items should be > 0!");
		vassert_not_null(enumName);

		HelmaEnumType type = {
		    .Name = enumName,
		    .Items = enumItems
		};
		array_push(result, type);
	    }
	}

	++ptr;
	c = *ptr;
    }

    return result;
}

char*
helma_generate_enum_h(char* typeName, char** typeItems, const char* tab4, char* enumToConvention, char* upperDefine)
{
    char* dotHFileContent = NULL;

    string_builder_appendf(dotHFileContent, "#ifndef %s_H\n", upperDefine);
    string_builder_appendf(dotHFileContent, "#define %s_H\n\n", upperDefine);
    string_builder_appendf(dotHFileContent, "typedef enum %s\n{\n", typeName);

    i32 i, count = array_count(typeItems);
    for (i = 0; i < count; ++i)
    {
	char* typeItem = typeItems[i];
	string_builder_appendf(dotHFileContent, "%s%s_%s = %d,\n", tab4, typeName, typeItem, i);
    }

    string_builder_appendf(dotHFileContent, "%s%s_Count = %d\n} %s;\n\n", tab4, typeName, i, typeName);
    char* groupNameToLower = string_to_lower(typeName);
    string_builder_appendf(dotHFileContent, "const char* %s_to_string(%s %s);\n", enumToConvention, typeName, groupNameToLower);
    string_builder_appendf(dotHFileContent, "const char* %s_to_short_string(%s %s);\n", enumToConvention, typeName, groupNameToLower);
    string_builder_appendf(dotHFileContent, "\n#endif // %s_H\n", upperDefine);

    return dotHFileContent;
}

char*
helma_generate_enum_c(char* typeName, const char* dotHContent)
{
    char* dotCFileContent = NULL;

    string_builder_appendf(dotCFileContent, "#include \"%s.h\"\n", typeName);
    string_builder_appends(dotCFileContent, "#include <Utils/Types.h>\n\n");

    char* enumToStringCode = helma_generate_enum_to_string(dotHContent);
    char* enumToShortStringCode = helma_generate_enum_to_short_string(dotHContent);
    string_builder_appends(dotCFileContent, enumToStringCode);
    string_builder_appends(dotCFileContent, "\n\n");
    string_builder_appends(dotCFileContent, enumToShortStringCode);

    return dotCFileContent;
}

HelmaGeneratedEnumContent
helma_generate_enum(char* typeName, char** typeItems)
{
    char* dotHFileContent = NULL;

    char* enumToConvention = helma_string_to_convention(typeName, string_length(typeName), '_');
    char* upperDefine = string_to_upper(enumToConvention);
    const char* tab4 = "    ";

    char* dotHContent = helma_generate_enum_h(typeName, typeItems, tab4, enumToConvention, upperDefine);
    char* dotCContent = helma_generate_enum_c(typeName, (const char*) dotHContent);

    memory_free(upperDefine);
    memory_free(enumToConvention);

    GINFO("dotHFileContent: %s\n", dotHContent);
    GINFO("dotCFileContent: %s\n", dotCContent);

    HelmaGeneratedEnumContent content = {
	.DotH = dotHContent,
	.DotC = dotCContent,
    };

    return content;
}

static char*
_helma_generate_enum_to_base(HelmaEnumType* enumTypes, char* shortWord/*short_*/)
{
    char* sb = NULL;

    i32 i, count = array_count(enumTypes);
    for (i = 0; i < count; ++i)
    {
	HelmaEnumType enumType = enumTypes[i];
	char* functionName = helma_string_to_convention((const char*)enumType.Name, string_length(enumType.Name), '_');
	const char* tab4 = "    ";
	const char* tab8 = "        ";
	string_builder_appendf(sb, "const char*\n%s_to_%sstring(%s ", functionName, IfNullThen(shortWord, ""), enumType.Name);
	enumType.Name[0] = char_to_lower(enumType.Name[0]);
	string_builder_appendf(sb, "%s)\n{\n", enumType.Name);

	string_builder_appendf(sb, "%sswitch (%s)\n%s{\n", tab4, enumType.Name, tab4);
	i32 i, count = array_count(enumType.Items);
	for (i = 0; i < count; ++i)
	{
	    char* enumItem = enumType.Items[i];
	    if (shortWord == NULL)
	    {
		string_builder_appendf(sb, "%scase %s: return \"%s\";\n", tab8, enumItem, enumItem);
	    }
	    else
	    {
		char* enumShortItem = string_substring(enumItem, string_index_of(enumItem, '_') + 1);
		string_builder_appendf(sb, "%scase %s: return \"%s\";\n", tab8, enumItem, enumShortItem);
		memory_free(enumShortItem);
	    }
	}

	string_builder_appendf(sb, "%s}\n\n%svassert_break();\n%sreturn \"\";\n}", tab4, tab4, tab4);

	array_free_w_item(enumType.Items);
    }

    return sb;
}

char*
helma_generate_enum_to_string(const char* input)
{
    HelmaEnumType* enumTypes = helma_get_enum_type(input);
    char* result = _helma_generate_enum_to_base(enumTypes, NULL);
    return result;
}

char*
helma_generate_enum_to_short_string(const char* input)
{
    HelmaEnumType* enumTypes = helma_get_enum_type(input);
    char* result = _helma_generate_enum_to_base(enumTypes, "short_");
    return result;
}

char*
helma_generate_enum_to_string_ext(HelmaEnumType* enumTypes)
{
    char* result = _helma_generate_enum_to_base(enumTypes, NULL);
    return result;
}

char*
helma_generate_enum_to_short_string_ext(HelmaEnumType* enumTypes)
{
    char* result = _helma_generate_enum_to_base(enumTypes, "short_");
    return result;
}
