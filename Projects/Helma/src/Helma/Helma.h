#ifndef HELMA_H
#define HELMA_H

#include <Core/SimpleStandardLibrary.h>

char* helma_string_to_convention(const char* str, size_t length, char conventionChar);

#endif // HELMA_H
