#include "HelmaParserHelper.h"

#include <Core/SimpleStandardLibrary.h>

#pragma clang diagnostic ignored "-Wformat"


const char*
variable_type_to_string(VariableType variableType)
{
    switch (variableType)
    {
        case VariableType_Void: return "VariableType_Void";
        case VariableType_I8: return "VariableType_I8";
        case VariableType_I32: return "VariableType_I32";
        case VariableType_U8: return "VariableType_U8";
        case VariableType_U32: return "VariableType_U32";
        case VariableType_F32: return "VariableType_F32";
        case VariableType_Bool: return "VariableType_Bool";
        case VariableType_ConstCharPtr: return "VariableType_ConstCharPtr";
        case VariableType_CharPtr: return "VariableType_CharPtr";
        case VariableType_Count: return "VariableType_Count";
    }

    vassert_break();
    return "";
}

/*
  NOTE(typedef): mb _is_keyword()
*/
i32
_is_typedef_keyword(char* ptr)
{
    if (string_compare_length(ptr, "typedef", 7))
        return 1;
    return 0;
}

i32
_is_type(char* ptr, VariableType* type)
{
    /*
      NOTE(typedef): default types check
    */
    for (i32 i = 0; i < VariableType_Count; ++i)
    {
        const char* defType = DefaultType[i];
        if (string_compare_length(ptr, defType, string_length(defType)))
        {
            *type = (VariableType) i;
            //printf("Type: %s\n", variable_type_to_string((VariableType) i));
            return 1;
        }
    }

    /*
      NOTE(typedef): work for default types only for now
    */
    return 0;
}

i32
_is_enum_acceptable(char c)
{
    if (_is_char(c) || c == '_' || (c >= '0' && c <= '9'))
        return 1;
    return 0;
}

i32
_is_identifier(char* ptr, char** identifier)
{
    char c = *ptr;
    if (helma_is_skipable(c))
    {
        return 0;
    }

    char* optr = ptr;
    while (c != '(' && c != '\0' && c != ' ')
    {
        ++optr;
        c = *optr;
    }

    size_t length = optr - ptr;
    printf("Identifier: %.*s\n", length, ptr);

    *identifier = string_copy((const char*)ptr, length);

    return 1;
}
