#ifndef HELMA_ENUM_GENERATOR_H
#define HELMA_ENUM_GENERATOR_H

typedef struct HelmaEnumType
{
    char* Name;
    char** Items;
} HelmaEnumType;

typedef struct HelmaGeneratedEnumContent
{
    char* DotH;
    char* DotC;
} HelmaGeneratedEnumContent;

HelmaEnumType* helma_get_enum_type(const char* codeWEnum);
char* helma_generate_enum_h(char* typeName, char** typeItems, const char* tab4, char* enumToConvention, char* upperDefine);
char* helma_generate_enum_c(char* typeName, const char* dotHContent);
HelmaGeneratedEnumContent helma_generate_enum(char* typeName, char** typeItems);

char* helma_generate_enum_to_string(const char* input);
char* helma_generate_enum_to_short_string(const char* input);
char* helma_generate_enum_to_string_ext(HelmaEnumType* enumTypes);
char* helma_generate_enum_to_short_string_ext(HelmaEnumType* enumTypes);

#endif // HELMA_ENUM_GENERATOR_H
