#include "ProjectAnalizer.h"

#include <stdio.h>

#include <Core/SimpleStandardLibrary.h>
#include <Helma/HelmaParserHelper.h>

static char
get_char_before_newline(char* ptr)
{
    while (*ptr != '\n' && *ptr != '\0')
    {
        ++ptr;
    }

    return *(ptr - 1);
}

force_inline char*
skip_typedef(char* ptr)
{
    if (*ptr == 't' && string_compare_length(ptr, "typedef", 7))
    {
        ptr += 8;

        if (string_compare_length(ptr, "struct", 6))
        {
            ptr += 7;

            char lc = get_char_before_newline(ptr);
            if (lc == ';') /* 1. declaration */
            {
                skip_line(ptr);
            }
            else /* 2. definition */
            {
                char c = *ptr;
                i32 flag = 0;

                while (c != ';' || !flag)
                {
                    if (c == '}')
                        flag = 1;

                    ++ptr;
                    c = *ptr;
                }

                /* skip ';' */
                ++ptr;
            }
        }
    }

    return ptr;
}

static char*
string_to_convention(const char* str, size_t length, char conventionChar)
{
    i32 upperCount = string_count_upper(str);
    if (upperCount <= 0)
    {
        vassert_break();
        return (char*)str;
    }

    size_t newLength = length + upperCount - 1;
    char* nstr = memory_allocate(newLength * sizeof(char));
    memset(nstr, '\0', newLength * sizeof(char));
    char* ptr = (char*) nstr;
    char c;
    char* rptr = (char*)str;
    char* eptr = rptr + newLength;

    typedef enum LocalFlag {
        LocalFlag_IsFirst = 0,
        LocalFlag_IsSecondary = 1,
        LocalFlag_IsThird = 2
    } LocalFlag;

    LocalFlag flag = LocalFlag_IsFirst;
    while (rptr != eptr)
    {
        c = *rptr;

        if (char_is_upper(c) && flag != LocalFlag_IsThird)
        {
            if (flag == LocalFlag_IsFirst)
            {
                flag = LocalFlag_IsSecondary;
                *ptr = char_to_lower(c);
                ++rptr;
            }
            else
            {
                flag = LocalFlag_IsThird;
                *ptr = conventionChar;
            }
        }
        else
        {
            flag = LocalFlag_IsSecondary;
            *ptr = char_to_lower(c);
            ++rptr;
        }

        ++ptr;
    }

    return nstr;
}


char**
get_source_files(char** allFiles, char* directory)
{
    char** filesToSearch = (char**) path_directory_get_directories(directory);

    for (i32 i = 0; i < array_count(filesToSearch); ++i)
    {
        //GINFO("File: %s\n", filesToSearch[i]);
        char* fileName = filesToSearch[i];
        char* ext = ielement_extension(fileName);
        //GINFO("ext: %s\n", ext);

        if ((ext[0] == 'c' || ext[0] == 'h') && (ext[1] == '\0'))
        {
            array_push(allFiles, fileName);
        }
    }

    array_free(filesToSearch);

    return allFiles;
}

char**
parse_includes(char* path)
{
    char** includeFiles = NULL;

    char* fileContent = file_read_string(path);
    char* ptr = fileContent;

    vassert(sizeof(char) == 1 && "Can't work with no 1 byte char!");

    do
    {
        if (*ptr == '#')
        {
            if (string_compare_length((ptr + 1), "include", 7))
            {
                ptr = ptr + 8;
                i32 isOpeningQuotationMark = 1;
                char c = *ptr;
                while (c == ' ' || c == '\t')
                {
                    ++ptr;
                    c = *ptr;
                }

                if (c == '<' || (c == '\"' && isOpeningQuotationMark))
                {
                    isOpeningQuotationMark = 0;

                    char* mptr = ptr + 1;
                    i32 includeLength = 0;
                    while (*mptr != '>' && *mptr != '\"')
                    {
                        ++includeLength;
                        ++mptr;
                    }

                    vassert(includeLength > 0 && "Wrong Include Length!");
                    char* includePath = (char*) memory_allocate(includeLength + 1);
                    memcpy(includePath, ptr + 1, includeLength);
                    includePath[includeLength] = '\0';
                    array_push(includeFiles, includePath);

                    skip_line(ptr);
                }
                else
                {
                    vassert(0 && "Some weird situation!");
                }
            }
            else
            {
                skip_line(ptr);
            }
        }

        if (*ptr != '#' && *ptr != '\n')
            break;

        //printf("%c", *ptr);
        ++ptr;
    } while (*ptr != '\0');

    memory_free(fileContent);

    return includeFiles;
}

/*
  NOTE(typedef): temporary struct
*/
typedef struct FileIncludes
{
    IElement* File;
    char** IncludeFiles;
} FileIncludes;

typedef struct FunctionMeta
{
    VariableType ReturnType;
    /*
      NOTE(typedef): If NULL then func(void)
    */
    VariableType* Args;
    char* Identifier;
} FunctionMeta;
force_inline void
function_meta_print(FunctionMeta meta, const char* tab)
{
    printf("%s-", tab, variable_type_to_string(meta.ReturnType), meta.Identifier);
    printf("(");
    i32 i, count = array_count(meta.Args);
    for (i = 0; i < count; ++i)
    {
        VariableType arg = meta.Args[i];
        printf("%s", variable_type_to_string(arg));
        if (i != (count - 1))
            printf(",");
    }

    printf(")");
}

typedef struct FileLink
{
    i32 IsFileExternal;
    union
    {
        IElement* ElementPtr;
        char* ExternalFileName;
    };
    FunctionMeta* FunctionDefinitions;
    FunctionMeta* FunctionImplementation;

} FileLink;

char*
find_directory_file(char** allFiles, char* fileToFind)
{
    char* shortName = (char*)path_get_name(fileToFind);
    size_t shortNameLength = string_length(shortName);

    i32 f, count = array_count(allFiles);
    for (f = 0; f < count; ++f)
    {
        char* file = allFiles[f];
        char* sfile = (char*)path_get_name(file);
        size_t sfileLength = string_length(sfile);
        if (string_compare_w_length(sfile, shortName, sfileLength, shortNameLength))
        {
            char* dir = path_get_directory(file);
            return dir;
        }
    }

    return NULL;
}

FunctionMeta*
parse_functions_definition(const char* filePath)
{
    FunctionMeta* functions = NULL;

    char* text = file_read_string(filePath);
    char* ptr = text;
    while (*ptr != '\0')
    {
        VariableType varType;

        /* skip_empty(ptr); */
        /* skip_preproc(ptr); */

        if (_is_typedef_keyword(ptr))
        {
            skip_to_char(ptr, '}');
            skip_to_char(ptr, ';');
            ++ptr;
        }

        while (*ptr == '\n')
            ++ptr;

        skip_comments(ptr);
        ptr = skip_typedef(ptr);

        if (helma_is_skipable(*ptr))
        {
            skip_line(ptr);
            continue;
        }

        if (_is_type(ptr, &varType))
        {
            skip_to_char(ptr, ' ');
            ++ptr;

            char* identifier = NULL;
            if (_is_identifier(ptr, &identifier))
            {
                skip_to_char(ptr, '(');

                ++ptr;
                VariableType* args = NULL;

                while (*ptr != ')')
                {
                    VariableType argType;
                    if (_is_type(ptr, &argType))
                    {
                        array_push(args, argType);
                    }

                    ++ptr;
                }

                FunctionMeta functionMeta = {
                    .ReturnType = varType,
                    .Identifier = identifier,
                    .Args = args
                };

                array_push(functions, functionMeta);
            }
        }
        else
        {
            skip_line(ptr);
        }
    }

    memory_free(text);

    return functions;
}

FunctionMeta*
parse_functions_implementation(const char* filePath)
{
    FunctionMeta* functions = NULL;

    //char* text = file_read_string(filePath);

    return functions;
}

FileLink
generate_file_link(char* root, char* fileInclude, char** allFiles)
{
    char* rootDir = find_directory_file(allFiles, fileInclude);
    if (rootDir == NULL)
    {
        FileLink link = {
            .IsFileExternal = 1,
            .ExternalFileName = fileInclude
        };
        return link;
    }

    IElement* ielementPtr = ielement_header(ielement(rootDir, path_get_name(fileInclude)));
    memory_free(rootDir);

    FunctionMeta* definitions = NULL;
    FunctionMeta* implementation = NULL;

    const char* ext = path_get_extension(fileInclude);
    if (string_compare_length(ext, ".h", 2))
    {
        if (string_compare(path_get_name(fileInclude), "Viewport.h"))
        {
            i32 w = 0;
        }
        definitions = parse_functions_definition(ielementPtr->AbsolutePath);
    }
    else
    {
        implementation = parse_functions_implementation(ielementPtr->AbsolutePath);
    }

    FileLink link = {
        .IsFileExternal = 0,
        .ElementPtr = ielementPtr,
        .FunctionDefinitions = definitions,
        .FunctionImplementation = implementation
    };
    return link;
}

static char**
find_all_files(const char* rootPath, const char* directoriesExclude[], i32 excludeCount)
{
    char** allFiles = NULL;
    char** allDirs = NULL;
    array_push(allDirs, (char*)rootPath);

    // Find All Files step
    for (i32 d = 0; d < array_count(allDirs); ++d)
    {
        char* projectDirectory = allDirs[d];

        i32 skipDirectory = 0;
        for (i32 e = 0; e < excludeCount; ++e)
        {
            if (string_compare(directoriesExclude[e], path_get_name(projectDirectory)))
            {
                skipDirectory = 1;
                break;
            }
        }

        if (skipDirectory)
            continue;

        allFiles = get_source_files(allFiles, projectDirectory);

        char** directoriesToSearch = (char**)
            path_directory_get_directories(projectDirectory);
        if (array_any(directoriesToSearch))
        {
            for (i32 i = 0; i < array_count(directoriesToSearch); ++i)
            {
                array_push(allDirs, directoriesToSearch[i]);
            }

            array_free(directoriesToSearch);
        }
    }

    array_free(allDirs);

    return allFiles;
}

LinkedFile*
simple_parse(const char* rootPath)
{
    GINFO("It just works!\n");

    const char* excludeDirs[] = {
        "Dependencies",
        "assets",
        "SimpleTest",
        "SimpleEditor",
        "Scripts",
        "resources",
        "docs",
        "bin" /*8*/
    };
    char** allFiles = find_all_files(rootPath, excludeDirs, 8);
    //array_foreach(allFiles, printf("File: %s\n", item););
    //vassert_break();

    FileIncludes* fileWithIncludes = NULL;

    // Define step
    i32 count = array_count(allFiles);
    GINFO("Count: %d\n", count);
    for (i32 i = 0; i < count; ++i)
    {
        char* file = allFiles[i];
        //GINFO("File %s\n", file);
        FileIncludes fileIncludes = {
            .File = ielement_header(file),
            .IncludeFiles = parse_includes(file)
        };

        array_push(fileWithIncludes, fileIncludes);
    }

    //--Generating links
    LinkedFile* linkedFiles = NULL;
    for (i32 i = 0; i < array_count(fileWithIncludes); ++i)
    {
        FileIncludes file = fileWithIncludes[i];

        LinkedFile linkedFile = {
            .File = file.File,
            .Links = NULL
        };
        for (i32 f = 0; f < array_count(file.IncludeFiles); ++f)
        {
            char* fileInclude = file.IncludeFiles[f];
            FileLink link = generate_file_link((char*)rootPath, fileInclude, allFiles);
            array_push(linkedFile.Links, link);
        }

        array_push(linkedFiles, linkedFile);
    }

    for (i32 i = 0; i < array_count(linkedFiles); ++i)
    {
        LinkedFile linkedFile = linkedFiles[i];

        //linkedFile
        //printf("File: %s\n", linkedFile.File->AbsolutePath);
        for (i32 f = 0; f < array_count(linkedFile.Links); ++f)
        {
            FileLink link = linkedFile.Links[f];
            /* if (!link.IsFileExternal) */
            /* { */
            /*	if (link.FunctionDefinitions != NULL) */
            /*	{ */
            /*	    array_foreach(link.FunctionDefinitions, */
            /*			  function_meta_print(item, "         ");); */
            /*	} */

            /*	printf("    - %s [0]\n", link.ElementPtr->AbsolutePath); */
            /* } */
            /* else */
            /* { */
            /*	printf("    - %s [1]\n", link.ExternalFileName); */
            /* } */
        }
    }

    array_free(allFiles);

    return linkedFiles;
}

/* Types Parser */


TypeInfo*
parse_file_types(char* text, TypeInfo* types)
{
    char* ptr = text;
    char c = *ptr;

    while (c != '\0')
    {
        if (string_compare_length(ptr, "#endif", 6))
        {
            i32 w = 0;
        }
        if (c != 't')
        {
            skip_line(ptr);
        }
        else if (string_compare_length(ptr, "typedef", 7))
        {
            ptr += 7;
            skip_empty(ptr);
            if (string_compare_length(ptr, "struct", 6))
            {
                ptr += 6;
                skip_empty(ptr);
                char* optr = ptr;
                skip_to_separator(optr);
                size_t length = optr - ptr;
                TypeInfo info = {
                    .Type = StructType_Struct,
                    .Name = string_copy(ptr, length)
                };

                i32 i, exist = 0, count = array_count(types);
                for (i = 0; i < count; ++i)
                {
                    if (string_compare(types[i].Name, info.Name))
                    {
                        exist = 1;
                        break;
                    }
                }

                if (!exist)
                {
                    array_push(types, info);
                }
            }
            else if (string_compare_length(ptr, "enum", 4))
            {
                ptr += 4;
                skip_empty(ptr);
                char* optr = ptr;
                skip_to_separator(optr);
                size_t length = optr - ptr;
                TypeInfo info = {
                    .Type = StructType_Enum,
                    .Name = string_copy(ptr, length)
                };
                array_push(types, info);
            }
        }

        ++ptr;
        c = *ptr;
    }

    return types;
}

TypeInfo*
simple_parse_types(const char* root)
{
    const char* excludeDirs[] = {
        "Dependencies",
        "assets",
        "SimpleTest",
        "SimpleEditor",
        "Scripts",
        "resources",
        "docs",
        "bin" /*8*/
    };
    char** allFiles = find_all_files(root, excludeDirs, 8);
    GINFO("Files Count: %d\n", array_count(allFiles));
    /* for (i32 i = 0; i < array_count(allFiles); ++i) */
    /* { */
    /*	printf("file: %s\n", allFiles[i]); */
    /* } */

    TypeInfo* typenames = NULL;

    i32 i, count = array_count(allFiles);
    for (i = 0; i < count; ++i)
    {
        char* file = allFiles[i];
        char* text = file_read_string(file);
        /* if (string_compare(path_get_name(file), "KeyCodes.h")) */
        /* { */
        /*    i32 w = 0; */
        /*    printf("%d\n", w); */
        /* } */
        //GINFO("Working with file: %s\n", file);
        typenames = parse_file_types(text, typenames);
    }

    array_free(allFiles);

    return typenames;
}
