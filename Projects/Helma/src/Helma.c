#include <string.h>

#include "ProjectAnalizer.h"
#include "TemplateCreator.h"
#include <Core/Types.h>
#include <Core/SimpleStandardLibrary.h>


i32
main()
{

    const char* pName = "StrategyZero";
    ProjectInfo info = {
	.DirectoryWhereCreate = path_get_current_directory(),
	.ProjectName = pName,
	.ProjectNameLength = string_length(pName)
    };
    i32 isProjectCreated = project_create(info);


#if 0
    char* result = helma_generate_enum_to_string("InputText");
#endif
    /* char* result = helma_generate_enum_to_string(file_read_string("/home/bies/Data/programming/C/SimpleEnginePrivate/Game/src/Game/Core/AI/ActivityType.h")); */
    /* printf("Result: %s\n", result); */

#if 0
    const char* root = "/home/bies/Data/programming/C/SimpleEnginePrivate/";

    {
	TimeState timeState;
	profiler_start(&timeState);
	LinkedFile* linkedFile = simple_parse(root);
	profiler_end(&timeState);
	GINFO("Time: %ld ms\n", profiler_get_milliseconds(&timeState));
	vassert_break();
    }

#endif

    return 0;
}

void
helma_on_attach_finished()
{
}

void
helma_on_update(f32 timestep)
{
}

char InputText[KB(100)];
char DestText[KB(100)];
char* EnumToStringTime = NULL;

static void
enum_to_string_ui()
{
#if 0
    static bool enumToStringVisible = true;
    if (igBeginTabItem("EnumToString", &enumToStringVisible, ImGuiTabItemFlags_None))
    {
	i32 width = 650;
	i32 height = 750;
	i32 btnWidth = 50;

	igInputTextMultiline("##source", InputText, ARRAY_COUNT(InputText), ImVec2(width, height), ImGuiInputTextFlags_None, NULL, NULL);

	igSameLine(width + 25, 50);

	static i32 isBtnPressed = 0;
	static TimeState timeState;
	if (igButton("=>", ImVec2(btnWidth, 50)))
	{
	    profiler_start(&timeState);

	    if (string_length(InputText) > 0)
	    {
		char* result = helma_generate_enum_to_string(InputText);
		if (result != NULL)
		{
		    memcpy(DestText, result, string_length(result) + 1);
		    string_builder_free(result);
		}
	    }
	    profiler_end(&timeState);

	    EnumToStringTime = profiler_get_string(&timeState);
	    isBtnPressed = 1;
	}

	igSameLine(width + 25 + btnWidth + 50, 50);

	igInputTextMultiline("##dest", DestText, ARRAY_COUNT(DestText), ImVec2(width, height), ImGuiInputTextFlags_None, NULL, NULL);

	igText("Input Length: %d", string_length(InputText));
	igSameLine(width + 25 + btnWidth + 50, 50);
	igText("Dest Length: %d", string_length(DestText));

	if (isBtnPressed)
	    igText("Process in %s", EnumToStringTime);

	igEndTabItem();
    }
#endif
}

static void
parse_structs_ui()
{
#if 0
    static bool parseStructsVisible = true;
    if (igBeginTabItem("ParseStructs", &parseStructsVisible, ImGuiTabItemFlags_None))
    {
	static char buf[512];
	igInputText("File Path", buf, ARRAY_COUNT(buf), ImGuiInputTextFlags_None, NULL, NULL);
	ImVec2 size;
	igGetItemRectSize(&size);
	igSameLine(size.x + 25, 50);

	static TimeState timeState;
	static TypeInfo* typenames = NULL;
	static char* timeString = NULL;
	if (igButton("Get Structs", ImVec2(0,0)))
	{
	    if (string_length(buf) > 0 && path_is_directory_exist(buf))
	    {
		profiler_start(&timeState);
		typenames = simple_parse_types(buf);
		profiler_end(&timeState);
		timeString = profiler_get_string(&timeState);
	    }
	}

	igText("Typenames (%d):", array_count(typenames));
	igSameLine(0, 150);
	igText("Proccess time: %s", timeString);
	i32 i, count = array_count(typenames);
	for (i = 0; i < count; ++i)
	{
	    TypeInfo info = typenames[i];
	    igText("%s %s", struct_type_to_string(info.Type), info.Name);
	}


	igEndTabItem();
    }

#endif
}

static void
parser_ui()
{
#if 0

    static bool parserVisible = true;
    if (igBeginTabItem("Parser", &parserVisible, ImGuiTabItemFlags_None))
    {
	static char buf[512];
	igInputText("File Path", buf, ARRAY_COUNT(buf), ImGuiInputTextFlags_None, NULL, NULL);
	ImVec2 size;
	igGetItemRectSize(&size);
	igSameLine(size.x + 25, 50);

	static TimeState timeState;
	static LinkedFile* linkedFile = NULL;
	static char* timeString = NULL;
	if (igButton("Parse Dirs", ImVec2(0,0)))
	{
	    if (string_length(buf) > 0 && path_is_directory_exist(buf))
	    {
		profiler_start(&timeState);
		linkedFile = simple_parse(buf);
		profiler_end(&timeState);
		timeString = profiler_get_string(&timeState);
	    }
	}

	igEndTabItem();
    }

#endif
}

void
helma_on_ui_render()
{
#if 0
    WideString wstr = wide_string_news(L"Создание проектов");
    i32 isVisible = 1;

    if (sui_panel_begin(&wstr, &isVisible, SuiPanelFlags_Movable))
    {
	WideString format = wide_string_news(L"Текущая директория: %s");
	sui_text(&format, path_get_current_directory());

	WideString inputLabel = wide_string_news(L"Имя проекта");
	wchar buf[256] = L"Project0";
	sui_input_string(&inputLabel, buf, wcslen(buf), 256);
	WideString btnLabel = wide_string_news(L"Сгенерировать");
	if (sui_button(&btnLabel, v2_new(50, 150)) == SuiState_Clicked)
	{
	    size_t actualLength = wcslen(buf);
	    if (actualLength > 0)
	    {
		const char* DirectoryWhereCreate;
		const char* ProjectName;
		size_t ProjectNameLength;
		ProjectCodeSnippets Snippets;

		char asciBuf[256] = {};
		for (i32 i = 0; i < 256; ++i)
		{
		    asciBuf[i] = (char) buf[i];
		}

		ProjectInfo info = {
		    .DirectoryWhereCreate = path_get_current_directory(),
		    .ProjectName = asciBuf,
		    .ProjectNameLength = actualLength
		};

		i32 isProjectCreated = project_create(info);
	    }
	}

    }
    sui_panel_end();

#endif
}

#if 0
void
helma_on_event(Event* event)
{
    switch (event->Category)
    {

    case EventCategory_Key:
    {
	if (event->Type != EventType_KeyPressed)
	    break;

	KeyPressedEvent* keyEvent = (KeyPressedEvent*) event;
	if (keyEvent->KeyCode == KeyType_Escape)
	{
	    application_close();
	    event->IsHandled = 1;
	}

	break;
    }

    case  EventCategory_Window:
    {
	if (event->Type == EventType_WindowShouldBeClosed)
	{
	    application_close();
	}
	break;
    }

    }
}
#endif

void
helma_on_destroy()
{
}
