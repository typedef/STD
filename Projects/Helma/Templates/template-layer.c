#include "${ProjectName}.h"
#include <EntryPoint.h>

void
create_user_application(UserApplicationSettings* pSettings)
{
    application_init(2000, 1200, "${ProjectName}");

    Layer ${ProjectNameCamel}Layer = {0};
    ${ProjectNameCamel}Layer.Name = "${ProjectName} Layer";
    ${ProjectNameCamel}Layer.OnAttach = ${ProjectNameConvLower}_on_attach;
    ${ProjectNameCamel}Layer.OnUpdate = ${ProjectNameConvLower}_on_update;
    ${ProjectNameCamel}Layer.OnUIRender = ${ProjectNameConvLower}_on_ui_render;
    ${ProjectNameCamel}Layer.OnEvent = ${ProjectNameConvLower}_on_event;
    ${ProjectNameCamel}Layer.OnDestoy = ${ProjectNameConvLower}_on_destroy;

    application_push_layer(${ProjectNameCamel}Layer);
}
