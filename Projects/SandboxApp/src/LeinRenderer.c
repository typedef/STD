#include "LeinRenderer.h"
#include "Core/SimpleAtlasFont.h"

// todo: write renderer base on svl specific for lein
#include <Std/StdRenderer.h>
#include <Std/StdAssetManager.h>
#include <Core/SimpleMath.h>
#include <Core/SimpleStandardLibrary.h>
#include <Core/SimpleWindowLibrary.h>
#include <EntitySystem/StdCameraEntity.h>


struct GlobalItem
{
    i64 DefaultImage;
    StdCameraEntity Camera;
    LeinFont Font;
};

static struct GlobalItem g = {};


void
lein_renderer_create(LeinRendererSettings settings)
{
    StdRendererSettings set = {
	// DOCS:
	.WindowSize = settings.WindowSize,
	.pWindowBackend = settings.pWindowBackend,
	// todo: Tweak it
	.MaxItemsCount = 200,
	.MaxCharsCount = 2000,
	// todo: true for now
	.IsDebug = 1,
	// DOCS: Always true
	.IsVsync = 1,
    };
    std_renderer_create(&set);

    i64 defaultImageId = std_asset_manager_load_image("Assets/Atlas/Ground-9-21.png");
    StdTexture* aTextures = std_asset_manager_get_all_images();
    std_renderer_set_textures(aTextures, array_count(aTextures));

    StdCameraEntity camera;
    camera = std_camera_entity_new((StdCameraEntitySettings) {
	    .WindowSize = { settings.WindowSize.Width, settings.WindowSize.Height },
	    .IsMain = 1
	});
    g.Camera = camera;
    std_renderer_set_camera(&camera.ViewProjection);

    // todo: monospace font
    LeinFont defaultFont = {
	//.pName = resource_font("NotoSans.ttf"),
	//.pName = resource_font("UbuntuMono-R.ttf"),
	.pName = resource_font("MartianMono-sWdRg.ttf"),
	.Size = 24,
    };
    i64 fontId = std_asset_manager_load_font(defaultFont.pName, defaultFont.Size);
    std_renderer_set_font((StdFontSettings) {
	    .FontSize = defaultFont.Size,
	    .pFontPath = defaultFont.pName,
	    .IsUpdateFont = 1,
	    .Width = .6f,
	    .EdgeTransition = 0.1f,
	    .IsUpdateConfig = 1,
	});
    defaultFont.Handle = std_asset_manager_get_font(fontId)->Handle;
    g.Font = defaultFont;

    g.DefaultImage = defaultImageId;
}

void
lein_renderer_destroy()
{
    std_renderer_destroy();
}

void
lein_renderer_set_color(v4 color)
{
    std_renderer_set_clear_color(color);
}

void
lein_renderer_flush()
{
    const f32 c_z_value = 100.0f;

    //std_renderer_draw_color_image(v3_new(0,0,c_z_value), v2_new(10,10), v4_new(0,0,0,0), g.DefaultImage);
    //std_renderer_draw_text_utf8(v3_new(0,0,c_z_value), v4_new(0,0,0,0), "Def");
    //std_renderer_draw_rect(v3_new(0,0,c_z_value), v2_new(10,10), v4_new(0,0,0,0));
    std_renderer_update();
}

void
lein_renderer_draw_rect(v3 pos, v2 size, v4 color)
{
    std_renderer_draw_rect(pos, size, color);
}

void
lein_renderer_draw_text(v3 pos, v4 color, char* pText)
{
    std_renderer_draw_text_utf8(pos, color, pText);
}

v2
lein_renderer_get_text_metrics(char* pText)
{
    return saf_font_get_metrics(g.Font.Handle, pText);
}

i32
lein_renderer_get_max_height()
{
    return g.Font.Handle.MaxHeight;
}
