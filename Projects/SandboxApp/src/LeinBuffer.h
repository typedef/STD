#ifndef LEIN_BUFFER_H
#define LEIN_BUFFER_H

#include <Core/Types.h>


typedef struct LeinBufferFileSettings
{
    const char* pPath;
    i64 StartsFromLine;
    i64 LinesCount;
    v3 Position;
} LeinBufferFileSettings;

typedef struct LeinBuffer
{
    // DOCS: Basic file info
    struct
    {
	const char* pPath;
	const char* pName;
	u64 TotalBytes;
    };

    // DOCS: All file lines
    struct
    {
	char** aLines;
    };

    // DOCS: State
    struct
    {
	i64 CurrentLine;
	i64 CurrentColumn;
    };

    v3 Position;

} LeinBuffer;

LeinBuffer lein_buffer_new_from_file(LeinBufferFileSettings set);
void lein_buffer_draw(LeinBuffer buffer);

#endif // LEIN_BUFFER_H
