#if 0

#include "Sandbox.h"
#include "Application/SimpleApplication.h"
#include "Core/SimpleStandardLibrary.h"
#include "Core/Types.h"
//#include "EntitySystem/Components/CameraComponent.h"

#include <Core/SimpleWindowLibrary.h>
#include <Std/StdRenderer.h>
#include <Std/StdTypes.h>
#include <Std/StdAtlas.h>
#include <Math/SimpleMath.h>
#include <Math/SimpleMathIO.h>
// todo: Move to StdAssetManager
#include <Std/StdVulkanLib.h>
#include <Deps/stb_image.h>


static CameraComponent gCameraComponent = {};

void
sandbox_on_attach()
{
    logger_set_flags(LoggerFlags_PrintType | LoggerFlags_PrintFunc | LoggerFlags_PrintLine);

    GINFO("Testing sel - ecs library.\n");
    GLOG("Log sel - ecs library.\n");
    GDEBUG("Debuging sel - ecs library.\n");
    GERROR("Error\n");
    GWARNING("Warn\n");

    //printf("%%\n");
    //for_testing();
    //vguard(0);

    printf("dist: %f\n", sqrt(v2_dot(v2_new(256, 256), v2_new(210, 256))));
    const char* pDirName = "TestFilesForAtlas";
    if (path_is_directory_exist(pDirName))
    {
    }

    const char** aFiles = path_directory_get_files(pDirName);
    StdAtlasSettings atlasSet = {
	.Size = v2_new(1024, 1024),
	.aPaths = aFiles,
    };
    StdAtlasWithBinary atlasWithBin = std_atlas_combine_images(&atlasSet);
    //stbi_write_png("Atlas.png", atlasWithBin.Atlas.Size.Width, atlasWithBin.Atlas.Size.Height, 4, atlasWithBin.pData, 0);

    //generate_images(pDirName, v2i_new(1024, 1024), 1);
    //concat_images(pDirName, v2_new(1024, 1024));

    //vguard(0);

    //logger_to_file();

    SwlWindow* pWindow = simple_application_get_window();

    CameraComponentCreateInfo createInfo = {
	.IsCameraMoved = 1,
	.Base = 0,

	.Settings = {
	    .Type = CameraType_OrthographicSpectator,
	    .Position = v3_new(0, 0, 1.0f),
	    .Front = v3_new(0, 0, -1),
	    .Up    = v3_new(0, 1,  0),
	    .Right = v3_new(1, 0,  0),
	    .SensitivityHorizontal = 0.35f,
	    .SensitivityVertical = 0.45f
	},

	.Orthographic = {
	    .Near = 0.1f,
	    .Far = 1000.0f,
	    .Right = (f32) pWindow->Size.Width,
	    .Top = (f32) pWindow->Size.Height
	},

	.Spectator = {.Speed = 5.0f,}
    };

    gCameraComponent =
	camera_component_new_ext(createInfo);
    camera_component_update(&gCameraComponent);


    SwlResolutions swlResolutions = swl_window_get_resolutions(pWindow);
    for (i32 i = 0; i < swlResolutions.Count; ++i)
    {
	if (i > 2) break;
	SwlResolution res = swlResolutions.aResolutions[i];
	printf("Resolution{%d}: %d x %d\n", i, res.Width, res.Height);
    }

    StdRendererSettings set = {
	.WindowSize = {pWindow->Size.Width, pWindow->Size.Height},
	.pWindowBackend = pWindow->pBackend,
	.MaxItemsCount = 100,
	.MaxCharsCount = 100,
	.IsDebug = 1,
	.IsVsync = 1,
    };
    std_renderer_create(&set);
    std_renderer_set_clear_color(v4_new(0.004321, 0.00806007, 0.007, 1));
    std_renderer_set_camera(&gCameraComponent.Base);

    { // todo: put in asset manager
	SvlTexture defaultTexture = {};
#if 0
#define TextureWidth 32
#define TextureHeight 32
#define DefaultTextureSize (TextureWidth * TextureHeight)
	u32 aData[DefaultTextureSize] = {};
	for (i32 i = 0; i < DefaultTextureSize; ++i)
	{
	    aData[i] = SimpleColor_RGBA(255, 255, 255, 255);
	}
	std_renderer_create_image_ext(TextureWidth, TextureHeight, 4, (void*)aData, (void*)&defaultTexture);
#else
	{
	    const char* defPath = resource_texture("white.png");
	    i32 w, h, c, d = 4;
	    void* pDefData = stbi_load(defPath, &w, &h, &c, d);
	    std_renderer_create_texture_ext(w, h, c, pDefData, (void*)&defaultTexture);
	}

#endif

	const char* path = "/home/bies/BigData/programming/C/SimpleGameEngine/assets/textures/atlas/RPGpack_sheet.png";
	i32 w, h, c, d = 4;
	void* pData = stbi_load(path, &w, &h, &c, d);
	SvlTexture rpgTexture = {};
	std_renderer_create_texture_ext(w, h, c, pData, (void*)&rpgTexture);

	SvlTexture forAnimations = {};
	{ // DOCS: Animation wip
	    const char* pAnimPath = "/home/bies/Загрузки/log/Animations/Individual Sprites/adventurer-idle-00.png";
	    i32 w, h, c, d = 4;
	    void* pData = stbi_load(pAnimPath, &w, &h, &c, d);
	    vguard_not_null(pData);
	    std_renderer_create_texture_ext(w, h, c, pData, (void*)&forAnimations);
	    stbi_image_free(pData);
	}

	SvlTexture dynamicAtlas = {};
	std_renderer_create_texture_ext(
	    atlasWithBin.Atlas.Size.X,
	    atlasWithBin.Atlas.Size.Y,
	    4,
	    atlasWithBin.pData, (void*)&dynamicAtlas);


	SvlTexture aTextures[] = {
	    defaultTexture,
	    rpgTexture,
	    forAnimations,
	    dynamicAtlas,
	};
	std_renderer_set_textures(aTextures, 4);

	for (i32 i = 0; i < ArrayCount(aTextures); ++i)
	{
	    //std_renderer_destroy_texture(&aTextures[i]);
	}
    }



    /*
    //todo: move to blockService

    create() {
    image = std_renderer_add_image("View.png");

    // inside blockLogicService
    simple_asset_manager_load_image("View.png");

    for (const char* pImage: aNotMarkedImages) {
    simple_asset_manager_unload_image(pImage); //"asset/ts/View.png"
    }
    }

    class blockLogicService {
    void onCameraPositionUpdate(v3* pNewCameraPosition) {
    SimpleChunks* aChunks = GetLoadedChunks();

    if (!simple_chunk_is_inside(aChunks, pNewCameraPosition))
    {
    SimpleChunks* aNewChunks = updateChunk();
    updateAssets(aNewChunks);
    }
    }

    void updateAssets(SimpleChunks* aNewChunks) {
    BlockAssetsPack* pPack = GetAssetsForChunks(aNewChunks);

    pPack->aTextures.forEach(t => asset_manager_mark_asset(SimpleAssetType_Texture, t));

    // do it for every SimpleAssetType

    simple_asset_manager_load_only_marked();
    }
    }

    class SimpleAssetManager {
    void simple_asset_manager_load_only_marked() {
    // ...

    SvlImageView* aImages = NULL;
    aMarkedTexture.forEach(t => aImages.add(GetImage(t)));

    std_renderer_set_textures(aImages);
    }
    }

    */
}

void
sandbox_on_update()
{
    SwlWindow* pWindow = simple_application_get_window();
    SimpleRuntimeStats runtimeStats = simple_application_get_stats();
    //GINFO("Timestep: %f\n", runtimeStats.Timestep);

    { // TODO: impl helper function, to make it 1 func call
	char* pUtf8Text = "Возможности рендера STD-Движка!";
	i32 aSymbols[128] = {};
	SimpleString text = simple_string(pUtf8Text);
	for (i32 i = 0, ind = 0; i < text.Size;)
	{
	    i32 size = 0;
	    u32 codePoint = simple_core_utf8_decode(text.pData + i, &size);
	    aSymbols[ind] = codePoint;
	    ++ind;
	    i += size;
	}

	v2 metrics = std_renderer_get_text_metrics(aSymbols, text.Length);

	std_renderer_draw_string_ext(v3_new(pWindow->Size.Width/2.0 - metrics.Width/2, 800, 0),
				     v4_new(1,1,1,0),
				     aSymbols,
				     text.Length);
    }

    v4 whiteColors[] = {
	v4_new(1,1,1,1),
	v4_new(1,1,1,1),
	v4_new(1,1,1,1),
	v4_new(1,1,1,1),
    };
    v2 aUvs[] = {
	v2_new(0, 0),
	v2_new(0, 1),
	v2_new(1, 1),
	v2_new(1, 0),
    };

    std_renderer_draw_texture_ext(v3_new(1050, 110, 1), v2_new(500, 500), whiteColors, aUvs, 3);

    v4 colors[] = {
	[0] = v4_new(0.1f, 0.587f, 0.178f, 1),
	[1] = v4_new(0.41, 0.641, 0.571, 1),
	[2] = v4_new(0.51, 0.681, 0.871, 1),
	[3] = v4_new(0.91, 0.281, 0.171, 1)
    };

    std_renderer_draw_rect_ext(v3_new(150, 210, 0), v2_new(250, 200), colors);
    std_renderer_draw_rect(v3_new(450, 210, 0), v2_new(250, 200), v4_new(0.85, 0.74, 0.45, 1.0));
    //std_renderer_draw_texture_ext(v3_new(450, 210, 0), v2_new(250, 200), v4_new(0.85, 0.74, 0.45, 1.0));
    //todo: impl better way of using texture concrete id-var instead of const 1
    std_renderer_draw_texture_ext(v3_new(550, 510, 0), v2_new(250, 200), whiteColors, aUvs, 1);


    f32 cof = 0.1f;
    v2 aUvs2[] = {
	v2_new(0, 0),
	v2_new(0, cof),
	v2_new(cof, cof),
	v2_new(cof, 0),
    };
    std_renderer_draw_texture_ext(v3_new(750, 510, 0), v2_new(250, 200), whiteColors, aUvs2, 1);


    { // DOCS: Animation wip
	v4 whiteColors[] = {
	    v4_new(1,1,1,1),
	    v4_new(1,1,1,1),
	    v4_new(1,1,1,1),
	    v4_new(1,1,1,1),
	};

	f32 sx = 0.0, ex = 1.0;

	v2 aUvs[] = {
	    v2_new(sx, 0),
	    v2_new(sx, 1),
	    v2_new(ex, 1),
	    v2_new(ex, 0),
	};
	//todo: impl better way of using texture concrete id-var instead of const 1
	f32 scale = 5;
	std_renderer_draw_texture_ext(v3_new(950, 310, 2), v2_new(scale*50, scale*37), whiteColors, aUvs, 2 /*textureId*/);
    }


    std_renderer_update();

    i32 isCameraMoved = 0;
    real posToAdd = 500 * runtimeStats.Timestep;

    // note: just for testing camera update in renderer
    if (pWindow->aKeys[SwlKey_D] == SwlAction_Press)
    {
	//GINFO("Pressed D!\n");
	gCameraComponent.Settings.Position.X -= 500 * runtimeStats.Timestep;
	isCameraMoved = 1;
    }
    if (pWindow->aKeys[SwlKey_A] == SwlAction_Press)
    {
	gCameraComponent.Settings.Position.X += 500 * runtimeStats.Timestep;
	isCameraMoved = 1;
    }
    if (pWindow->aKeys[SwlKey_W] == SwlAction_Press)
    {
	gCameraComponent.Settings.Position.Y -= 500 * runtimeStats.Timestep;
	isCameraMoved = 1;
    }
    if (pWindow->aKeys[SwlKey_S] == SwlAction_Press)
    {
	gCameraComponent.Settings.Position.Y += 500 * runtimeStats.Timestep;
	isCameraMoved = 1;
    }

    if (isCameraMoved)
    {
	gCameraComponent.IsCameraMoved = 1;
	camera_component_update(&gCameraComponent);
	std_renderer_set_camera(&gCameraComponent.Base);
    }

}

void
sandbox_on_event(SwlEvent* pEvent)
{
    if (pEvent->Type == SwlEventType_WindowResized)
    {
	SwlWindowResizeEvent* pResizedEvent = (SwlWindowResizeEvent*) pEvent;
	std_renderer_window_resized(pResizedEvent->Width, pResizedEvent->Height);
    }
    else if (pEvent->Type == SwlEventType_KeyPress)
    {
	SwlKeyPressEvent* pKeyEvent = (SwlKeyPressEvent*) pEvent;
	if (pKeyEvent->Key == SwlKey_F11)
	{
	    SwlWindow* pWindow = simple_application_get_window();
	    swl_window_set_fullscreen(pWindow);
	}
    }

}

void
sandbox_on_ui()
{

}

void
sandbox_on_destroy()
{
    std_renderer_destroy();
}

#endif
