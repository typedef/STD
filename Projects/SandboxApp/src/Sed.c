#include <Sed.h>

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h> // STDIN_FILENO
#include <termios.h>
#include <ctype.h>
#include <stdarg.h>

// DOCS: Linux-deps
#include <sys/ioctl.h>
#include <Core/SimpleStandardLibrary.h>


#define IsCtrl(k) ((k) & 0x1f)

#define SedNone() ({(SedInputResult){.Type = SedInputResultType_None }; })
#define SedCharacter(cha) ({(SedInputResult){.Type = SedInputResultType_Character, .Key = cha }; })
#define SedExit() ({(SedInputResult){.Type = SedInputResultType_Exit }; })



/*#####################################
  ##       Sed Terminal-Backend      ##
  #####################################
*/

typedef struct SedTerminal
{
    struct termios State;
} SedTerminal;




typedef enum SedKey
{
    SedKey_None,
    SedKey_Escape,
    SedKey_Left = 999,
    SedKey_Right,
    SedKey_Up,
    SedKey_Down,
    SedKey_PageUp,
    SedKey_PageDown,
    SedKey_Home,
    SedKey_End,
    SedKey_Del,
} SedKey;

typedef enum SedInputResultType
{
    SedInputResultType_None = 0,
    SedInputResultType_Character,
    SedInputResultType_Exit,
} SedInputResultType;

typedef struct SedInputResult
{
    SedInputResultType Type;
    SedKey Key;
} SedInputResult;

typedef struct SedBuffer
{
    const char* pName;
    sed_v2 Position;
    sed_v2 Size;
    sed_v2 Offset;
    // DOCS: string_builder
    sed_i32 Line;
    sed_i32 Col;
    sed_i32 MaxCol;
    sed_i32 SavedCol;
    char** aLines;
    //char* pData;
} SedBuffer;

typedef struct SedBufferSettings
{
    const char* pName;
    sed_v2 Position;
    sed_v2 Size;
    sed_i32 MaxCol;
    char** aLines;
} SedBufferSettings;


typedef struct SedCursor
{
    sed_i32 X;
    sed_i32 Y;
} SedCursor;

typedef struct SedVersion
{
    i8 Main;
    i8 Major;
    i8 Minor;
} SedVersion;

typedef struct SedEditor
{
    sed_i32 CurrentBuffer;
    // note: data buffer
    char* pBuffer;
    SedBuffer* aBuffers;
    SedVersion Version;
    SedCursor MainCursor;
} SedEditor;


static SedEditor gEditor = {};
static SedTerminal gTerminal = {};


SedBuffer
sed_buffer_create(SedBufferSettings set)
{
    SedBuffer sedBuffer = {
	.pName = set.pName,
	.Position = set.Position,
	.Size = set.Size,
	.MaxCol = set.MaxCol,
	.aLines = set.aLines,
    };

    return sedBuffer;
}

void
sed_buffer_append_line(SedBuffer* pBuffer, char* pStr, sed_i32 len)
{
    char* sb = NULL;
    string_builder_appendsl(sb, pStr, len);
    sed_i32 colsCount = string_builder_count(sb);
    array_push(pBuffer->aLines, sb);
    pBuffer->MaxCol = Max(pBuffer->MaxCol, colsCount);
}

void
sed_buffer_draw(SedBuffer* pBuffer)
{
    sed_v2 pos = pBuffer->Position;

    sed_v2 size = pBuffer->Size;

    sed_i32 linesPresented = 0;
    sed_i32 count = array_count(pBuffer->aLines);
    for (sed_i32 i = pBuffer->Offset.Y; i < count; ++i)
    {
	if (linesPresented > size.Y)
	    break;

	sed_editor_move_cursor(pos.X, pos.Y);

	char* pLine = pBuffer->aLines[i];
	char* ptr = pLine + pBuffer->Offset.X;

	sed_i32 cnt = string_builder_count(pLine);
	sed_i32 len;
	if (pBuffer->Offset.X >= cnt)
	    len = 0;
	else
	    len = Min(size.X, cnt);

	// todo: rework process of writing to editor_buffer
	if (len > 0)
	{
	    string_builder_appendsl(gEditor.pBuffer, ptr, len);
	}
	else
	{
	    //string_builder_appendsl(gEditor.pBuffer, "\n\r", 2);
	    //sed_editor_move_cursor(pBuffer->Position.X, pos.Y);
	    //ystring_builder_appends(gEditor.pBuffer, " \0");
	}

	++pos.Y;
	++linesPresented;
    }
}

char*
sed_buffer_get_current_line(SedBuffer* pBuffer)
{
    if (pBuffer->Line >= array_count(pBuffer->aLines))
	return NULL;
    return pBuffer->aLines[pBuffer->Line];
}

sed_i32
sed_buffer_get_max_col(SedBuffer* pBuffer)
{
    if (pBuffer->Line >= array_count(pBuffer->aLines))
	return 0;

    sed_i32 contentWidth = string_builder_count(pBuffer->aLines[pBuffer->Line]);
    return contentWidth;
}

sed_i32
sed_buffer_get_current_line_width(SedBuffer* pBuffer)
{
    if (pBuffer->Line >= array_count(pBuffer->aLines))
	return 0;
    return string_builder_count(pBuffer->aLines[pBuffer->Line]) - 1;
}

sed_i32
sed_buffer_get_max_width(SedBuffer* pBuffer)
{
    sed_i32 contentWidth = sed_buffer_get_current_line_width(pBuffer);
    sed_i32 sizeX = pBuffer->Size.X - 1;
    sed_i32 valueX = Min(contentWidth, sizeX);
    return valueX;
}

sed_i32
sed_buffer_get_cursor_end_x(SedBuffer* pBuffer)
{
    sed_i32 maxCursorX = sed_buffer_get_max_width(pBuffer);
    sed_i32 endX = pBuffer->Position.X + maxCursorX;
    return endX;
}

sed_i32
sed_buffer_get_max_height(SedBuffer* pBuffer)
{
    sed_i32 contentHeight = array_count(pBuffer->aLines);
    sed_i32 sizeY = pBuffer->Size.Y;
    sed_i32 valueX = Min(contentHeight, sizeY);
    return valueX;
}

char**
sed_buffer_open_file(const char* pFilePath)
{
    if (!path_is_file_exist(pFilePath))
    {
	// todo: impl logs
	printf("No file!\n");
	return NULL;
    }

    sed_u64 fileLen = 0;
    char* pContent = file_read_string_ext(pFilePath, &fileLen);

    char** aLines = NULL;

    // parse
    // TODO: rework it
    char* ptr = pContent;
    char* start = ptr;
    while (*ptr != '\0')
    {
	if (*ptr == '\n')
	{
	    char* sb = NULL;

	    if (*start == '\n')
	    {
		string_builder_appendsl(sb, "\r\n", 2);
	    }
	    else
	    {
		u64 lineLen = ptr - start;
		string_builder_appendsl(sb, start, lineLen);
	    }

	    array_push(aLines, sb);
	    ++ptr;
	    start = ptr;
	}
	else
	{
	    ++ptr;
	}
    }

    memory_free(pContent);

    return aLines;
}

void
sed_editor_init()
{
    gEditor.Version = (SedVersion) { 0, 0, 1 };

    sed_v2 size = sed_terminal_get_size();
    SedBuffer scratchBuffer = sed_buffer_create(
	(SedBufferSettings) {
	    .pName = "**Scratch**",
	    .Position = {5, 5},
	    .Size = {size.X-10, size.Y - 10},
	    .MaxCol = sed_terminal_get_size().X
	});

    const char* pStr0 = "Hello, data!";
    const char* pStr1 = "Hello, code!";
    sed_buffer_append_line(&scratchBuffer, (char*)pStr0, string_length(pStr0));
    sed_buffer_append_line(&scratchBuffer, (char*)pStr1, string_length(pStr1));
    array_push(gEditor.aBuffers, scratchBuffer);

    sed_v2 p = scratchBuffer.Position;
    gEditor.MainCursor = (SedCursor) { p.X-1, p.Y-1 };

}

void
sed_editor_add_string(char* pStr, sed_i32 len)
{
    string_builder_appendsl(gEditor.pBuffer, pStr, len);
}

void
sed_editor_add_string_format(const char* pFormat, ...)
{
    va_list args;
    va_start(args, pFormat);
#define BufSize 256
    char buf[BufSize] = {};
    sed_i32 nbytes = vsnprintf(buf, BufSize, pFormat, args);
    sed_editor_add_string(buf, nbytes);
    va_end(args);
}

void
sed_editor_clear()
{
    string_builder_clear(gEditor.pBuffer);
}

SedBuffer*
sed_editor_get_current_buffer()
{
    //todo: validate index
    return &gEditor.aBuffers[gEditor.CurrentBuffer];
}

void
sed_terminal_init()
{
    tcgetattr(STDIN_FILENO, &gTerminal.State);
    atexit(sed_terminal_deinit);
}

void
sed_terminal_deinit()
{
    tcsetattr(STDIN_FILENO, TCSAFLUSH, &gTerminal.State);

    sed_editor_move_cursor(1,1);
    write(STDIN_FILENO, "\x1b[2J", 4);
}

void
sed_terminal_enable_raw_mode()
{
    struct termios raw;
    tcgetattr(STDIN_FILENO, &raw);
    raw.c_iflag &= ~(IXON | ICRNL | BRKINT | INPCK | ISTRIP);
    raw.c_oflag &= ~(OPOST);
    raw.c_cflag |= (CS8);
    raw.c_lflag &= ~(ECHO | ICANON | ISIG | IEXTEN);

    raw.c_cc[VMIN] = 0;
    raw.c_cc[VTIME] = 1;

    tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw);
}

/*
  v2 Position, Size=BufferSize, ContentSize;
  constrains:
  Size > 0
  ContentSize > 0
  BufferSize != Size;

  Cursor MainCursor; // struct Cursor { sed_i32 X, Y; }
  Cursor -> [Position, Position + Min(Size, ContentSize)]

  Col, Line;

  v2 Offset;

  MoveRight()
  {
      sed_i32 maxCol = sed_buffer_get_max_width(pBuffer);
      if (maxCol == 0) return;

      sed_i32 endX = Position.X + maxCol;

      if (x < endX)
      {
	  MainCursor.X = Min(MainCursor.X + 1, endX);
	  // DOCS: Record current column in buffer
	  pBuffer->Col = Min(pBuffer->Col + 1, maxCol);
      }
      else
      {
	  x;

      }
  }


  void sed_editor_move_cursor_down();void sed_editor_move_cursor_down();void sed_editor_move_cursor_down();void sed_editor_move_cursor_down();void sed_editor_move_cursor_down();
*/

void sed_editor_move_cursor_down();

// BUG: Fix new \r\n problem

// NOTE: Seems like it works :)
void
sed_editor_move_cursor_right()
{
    SedBuffer* pBuffer = sed_editor_get_current_buffer();
    sed_i32 maxCol = sed_buffer_get_max_col(pBuffer);

    // DOCS: Handle -> not for NULL row
    if (maxCol == 0)
	return;

    sed_i32 nextCol = pBuffer->Col + 1;
    if (nextCol <= maxCol)
    {
	// DOCS: Process logic on current line
	pBuffer->Col = nextCol;

	sed_i32 endX = sed_buffer_get_cursor_end_x(pBuffer);
	sed_i32 nx = gEditor.MainCursor.X + 1;

	// DOCS: Record current column in buffer
	if (nx >= endX)
	{
	    sed_i32 diff = pBuffer->Col - pBuffer->Size.X;
	    pBuffer->Offset.X = Max(diff, 0);
	}

	// DOCS: Clamp it, never bigger then max
	gEditor.MainCursor.X = Min(nx, endX);
    }
    else
    {
	// DOCS: Move on lower line / next row
	pBuffer->Col = 0;
	pBuffer->Offset.X = 0;

	// DOCS: Set cursor pos in buffer
	gEditor.MainCursor.X = pBuffer->Position.X - 1;
	sed_editor_move_cursor_down();
    }
}

void sed_editor_move_cursor_up();

// BUG: move left on 0 line give bug-behavior
void
sed_editor_move_cursor_left()
{
    SedBuffer* pBuffer = sed_editor_get_current_buffer();

    if (pBuffer->Col > 0)
    {
	gEditor.MainCursor.X = Max(gEditor.MainCursor.X - 1, pBuffer->Position.X - 1);

	if (pBuffer->Col <= pBuffer->Offset.X)
	    pBuffer->Offset.X = Max(pBuffer->Offset.X - 1, 0);
	// DOCS: Basic logic
	pBuffer->Col = Max(pBuffer->Col - 1, 0);

    }
    else
    {
	sed_editor_move_cursor_up();

	// todo: move to prev/upper row
	pBuffer->Col = sed_buffer_get_max_col(pBuffer);
	pBuffer->Offset.X = Max(pBuffer->Col - pBuffer->Size.X, 0);
	gEditor.MainCursor.X = sed_buffer_get_cursor_end_x(pBuffer);
    }

}

void
sed_move_with_adjustment_to_width(SedBuffer* pBuffer)
{
    if (1)
	return;

    // DOCS: Handle Move on lower line with less-width
    sed_i32 cursorX = gEditor.MainCursor.X;
    sed_i32 px = pBuffer->Position.X;
    sed_i32 w = sed_buffer_get_max_width(pBuffer);
    sed_i32 contentX = px + w; // DOCS: Value X in content str

    // DOCS: Save prev old big col-value, that is bigger then new
    if (cursorX > contentX)
    {
	pBuffer->SavedCol = cursorX;
	gEditor.MainCursor.X = contentX - pBuffer->Offset.Y;
    }
    // DOCS: Apply saved value, and then drop it
    else if (pBuffer->SavedCol != -1 && cursorX < pBuffer->SavedCol)
    {
	gEditor.MainCursor.X = Min(pBuffer->SavedCol, contentX);
	pBuffer->SavedCol = -1;
    }
}

void
sed_editor_move_cursor_down()
{
    SedBuffer* pBuffer = sed_editor_get_current_buffer();

    sed_i32 maxy = sed_buffer_get_max_height(pBuffer);
    sed_i32 y = gEditor.MainCursor.Y,
	    ny = y + 1;
    sed_i32 endY = pBuffer->Position.Y + maxy - 1;
    sed_i32 linesCount = array_count(pBuffer->aLines);

    if (ny <= endY)
    {
	gEditor.MainCursor.Y = ny;
    }
    // DOCS: do offset logic
    else if (linesCount > pBuffer->Size.Y)
    {
	pBuffer->Offset.Y = Min(pBuffer->Offset.Y + 1, linesCount - pBuffer->Size.Y);
    }

    // DOCS: Record current line in buffer
    pBuffer->Line = Min(pBuffer->Line + 1, linesCount);

    // DOCS: Additional movement logic
    sed_move_with_adjustment_to_width(pBuffer);
}

void
sed_editor_move_cursor_up()
{
    SedBuffer* pBuffer = sed_editor_get_current_buffer();

    sed_v2 start = pBuffer->Position;
    sed_i32 y = gEditor.MainCursor.Y;

    //if (pBuffer->)
    gEditor.MainCursor.Y = Max(y-1, start.Y-1);

    // DOCS: Record current line in buffer
    sed_i32 line = pBuffer->Line;
    pBuffer->Line = Max(line-1, 0);

    // DOCS: Scroll Up
    if (pBuffer->Offset.Y > 0 && (line - pBuffer->Offset.Y) == 0)
    {
	sed_i32 newOffset = pBuffer->Offset.Y - 1;
	pBuffer->Offset.Y = Max(newOffset, 0);
    }

    sed_move_with_adjustment_to_width(pBuffer);

}

void
sed_editor_move_cursor_begin()
{
    SedBuffer* pBuffer = sed_editor_get_current_buffer();
    pBuffer->Col = 0;
    pBuffer->Offset.X = 0;
    gEditor.MainCursor.X = pBuffer->Position.X-1;
}

void
sed_editor_move_cursor_end()
{
    SedBuffer* pBuffer = sed_editor_get_current_buffer();
    gEditor.MainCursor.X = sed_buffer_get_cursor_end_x(pBuffer);
    pBuffer->Col = sed_buffer_get_max_col(pBuffer);
    pBuffer->Offset.X = Max(pBuffer->Col - pBuffer->Size.X, 0);
}

sed_i32
sed_editor_is_file_open(const char* pFilePath)
{
    sed_i32 ind = array_index_of(gEditor.aBuffers, string_compare(item.pName, pFilePath));
    if (ind != -1)
	return 1;
    return 0;
}

void
sed_draw_log_buffer(const char* pFormat, ...)
{
    sed_editor_move_cursor(1, sed_terminal_get_size().Y-1);

    va_list args;
    va_start(args, pFormat);
#define BufSize 256
    char buf[BufSize] = {};
    sed_i32 nbytes = vsnprintf(buf, BufSize, pFormat, args);
    sed_editor_add_string(buf, nbytes);
    va_end(args);
}

SedInputResult
sed_terminal_handle_input()
{
    char chars[4] = {};

    sed_i32 byteCount = read(STDIN_FILENO, &chars[0], 1);
    if (byteCount == 0)
    {
	//printf("NULL\r\n");
	return SedNone();
    }

    char c = chars[0];
    if (c == '\x1b')
    {
	if (read(STDIN_FILENO, &chars[1], 1) != 1)
	    return SedCharacter(SedKey_Escape);

	/* if (chars[1] == '\x6E') */
	/* {//27 */
	/*     return SedCharacter(SedKey_Escape); */
	/* } */

	if (read(STDIN_FILENO, &chars[2], 1) != 1)
	    return SedCharacter(c);

	if (chars[1] == '[')
	{
	    c = chars[2];
	    if (c >= '0' && c <= '9')
	    {
		if (read(STDIN_FILENO, &chars[3], 1) != 1)
		    return SedCharacter(chars[0]);
		if (chars[3] == '~')
		{
		    switch (chars[2])
		    {
		    case '1': return SedCharacter(SedKey_Home);
		    case '3': return SedCharacter(SedKey_Del);
		    case '4': return SedCharacter(SedKey_End);
		    case '5': return SedCharacter(SedKey_PageUp);
		    case '6': return SedCharacter(SedKey_PageDown);
		    case '7': return SedCharacter(SedKey_Home);
		    case '8': return SedCharacter(SedKey_End);
		    }
		}
	    }
	    else
	    {
		switch (chars[2])
		{
		case 'A': return SedCharacter(SedKey_Up);
		case 'B': return SedCharacter(SedKey_Down);
		case 'C': return SedCharacter(SedKey_Right);
		case 'D': return SedCharacter(SedKey_Left);
		case 'H': return SedCharacter(SedKey_Home);
		case 'F': return SedCharacter(SedKey_End);
		}
	    }
	}
	else if (chars[1] == 'O')
	{
	    switch (chars[2])
	    {
	    case 'H': return SedCharacter(SedKey_Home);
	    case 'F': return SedCharacter(SedKey_End);
	    }
	}
    }

    if (iscntrl(c))
    {
	///printf("Cntr: %d\r\n", c);
    }
    else
    {
	// printf("%d (%c)\r\n", c, c);
    }


    return SedCharacter(chars[0]);
}

void
sed_editor_move_cursor(sed_i32 x, sed_i32 y)
{
    char buf[32] = {};
    sed_i32 nbytes = snprintf(buf, 32, "\x1b[%d;%dH", y, x);
    sed_editor_add_string(buf, nbytes);
}

void
sed_editor_back_cursor()
{
    SedBuffer* pBuffer = sed_editor_get_current_buffer();
    sed_editor_move_cursor(gEditor.MainCursor.X + 1, gEditor.MainCursor.Y + 1);
}

void
sed_editor_refresh_screen(SedInputResult input)
{
    // DOCS: Hide cursor
    sed_editor_add_string("\x1b[?25l", 6);

    //write(STDOUT_FILENO, "\x1b[2J", 4);
    sed_editor_move_cursor(1,1);

    SedBuffer* pCurBuffer = sed_editor_get_current_buffer();

    // DOCS: Do the rendering work
    {
	sed_v2 windowSize = sed_terminal_get_size();

	// DOCS: Draw row numbers
	for (sed_i32 y = 0; y < windowSize.Y; ++y)
	{
	    sed_editor_add_string("\x1b[K", 3);

	    /* char buf[32] = {}; */
	    /* sed_i32 bytesCnt = snprintf(buf, 32, "%d", y+1); */
	    /* write(STDOUT_FILENO, buf, bytesCnt); */
	    /* sed_editor_add_string(buf, bytesCnt); */

	    if (y != (windowSize.Y-1))
		sed_editor_add_string("\r\n", 2);
	}

	{ // DOCS: render garbage
	    sed_i32 ex = pCurBuffer->Position.X + pCurBuffer->Size.X;
	    for (sed_i32 y = pCurBuffer->Position.Y;
		 y < pCurBuffer->Position.Y + pCurBuffer->Size.Y;
		 ++y)
	    {
		sed_editor_move_cursor(ex, y);
		sed_editor_add_string("|", 1);
	    }
	}

	// DOCS: Draw current buffer content
	sed_i32 linesCount = array_count(pCurBuffer->aLines);
	if (linesCount == 0)
	{
	    // DOCS: Draw Sed message with version
	    sed_editor_move_cursor(windowSize.X/2 - 5, windowSize.Y / 3);
	    SedVersion ver = gEditor.Version;
	    sed_editor_add_string_format("Sed -- v:%d.%d.%d", ver.Main, ver.Major, ver.Minor);
	}
	else
	{
	    // todo: impl proper view
	    sed_buffer_draw(pCurBuffer);
	}
    }

    SedCursor mainCursor = gEditor.MainCursor;
    sed_draw_log_buffer("key-%0.4d l%d:c%d size: %d,%d %s",
			input.Key,
			pCurBuffer->Line, pCurBuffer->Col,
			pCurBuffer->Size.X, pCurBuffer->Size.Y,
			pCurBuffer->pName
	);

    sed_editor_back_cursor();

    // DOCS: Show cursor
    sed_editor_add_string("\x1b[?25h", 6);

    // todo: impl proper view
    write(STDOUT_FILENO, gEditor.pBuffer, string_builder_count(gEditor.pBuffer));

    sed_editor_clear();
}

sed_v2
sed_terminal_get_size()
{
    struct winsize ws;
    if (ioctl(STDOUT_FILENO, TIOCGWINSZ, &ws) == -1 || ws.ws_col == 0)
    {
	if (write(STDOUT_FILENO, "\x1b[999C\x1b[999B", 12) != 12)
	    return (sed_v2) {};

	sed_v2 r = sed_terminal_get_cursor_position();
	// note: temp
	sed_terminal_handle_input();
	return r;
    }

    return (sed_v2){ws.ws_col, ws.ws_row};
}

sed_v2
sed_terminal_get_cursor_position()
{
    sed_v2 r = {};
    char buf[32];
    sed_u32 i = 0;

    if (write(STDOUT_FILENO, "\x1b[6n", 4) != 4)
	return r;

    while (i < (sizeof(buf) - 1))
    {
	if (read(STDIN_FILENO, &buf[i], 1) != 1)
	    break;
	if (buf[i] == 'R')
	    break;
	i++;
    }
    buf[i] = '\0';

    // DOCS: Parsing
    if (buf[0] != '\x1b' || buf[1] != '[')
	return r;

    sed_i32 w, h;
    sed_i32 result = sscanf(&buf[2], "%d;%d", &h, &w);
    if (result != 2)
	return r;
    r = (sed_v2) { .X = w, .Y = h };

    return r;
}

void
sed_editor_process_input(SedKey key)
{
    switch (key)
    {
    case SedKey_Left:
	sed_editor_move_cursor_left();
	break;
    case SedKey_Right:
	sed_editor_move_cursor_right();
	break;
    case SedKey_Up:
	sed_editor_move_cursor_up();
	break;
    case SedKey_Down:
	sed_editor_move_cursor_down();
	break;

    case SedKey_Home:
	sed_editor_move_cursor_begin();
	break;
    case SedKey_End:
	sed_editor_move_cursor_end();
	break;

    default:
	break;
    }

}


void
sed_run()
{
    printf("Sed runing\n");

    sed_editor_init();

    sed_terminal_init();
    sed_terminal_enable_raw_mode();

    while (1)
    {
	sed_editor_back_cursor();

	SedInputResult input = sed_terminal_handle_input();

	if (input.Key ==
	    SedKey_Escape
	    /* IsCtrl('q') */)
	{
	    write(STDOUT_FILENO, "\x1b[2J", 4);
	    write(STDOUT_FILENO, "\x1b[H", 3);
	    break;
	}
	else if (input.Key == IsCtrl('o'))
	{
	    const char* pFilePath = "/home/bies/BigData/programming/C/STD/text.c";
	    if (!sed_editor_is_file_open(pFilePath))
	    {
		char** aLines = sed_buffer_open_file(pFilePath);
		sed_i32 colMax = 0;
		array_foreach(
		    aLines,
		    sed_i32 colCnt = string_builder_count(item);
		    if (colCnt > colMax)
			colMax = colCnt);

		sed_v2 size = sed_terminal_get_size();
		SedBuffer newBuffer = sed_buffer_create(
		    (SedBufferSettings) {
			.pName = pFilePath,
			.Position = {5, 5},
			.Size = {size.X-10, size.Y - 10},
			.MaxCol = colMax,
			.aLines = aLines,
		    });

		sed_i32 ind = array_count(gEditor.aBuffers);
		array_push(gEditor.aBuffers, newBuffer);
		gEditor.CurrentBuffer = ind;

		// DOCS: Set cursor pos to start of the buffer on first load
		sed_v2 p = newBuffer.Position;
		gEditor.MainCursor = (SedCursor){p.X-1, p.Y-1};
	    }
	}

	sed_editor_process_input(input.Key);

	sed_editor_refresh_screen(input);
    }

}
