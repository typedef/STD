/* #include "Sandbox.h" */
/* #include <EntryPoint.h> */

#include <stdio.h>
#include <stdlib.h>
#include <Core/Types.h>
#include <Core/SystemInfo.h>
#include <Core/SimpleStandardLibrary.h>
#include <Core/SimpleApplication.h>
#include "Sandbox.h"
#include <locale.h>
#include "Sed.h"

#include "Lein.h"


i32
main()
{
    setlocale(LC_ALL, ".UTF8");
    // sed_run();

    lein_create((LeinSettings) {
	    .WindowPosition = {25, 50},
	    .WindowSize = {1080, 1080},
	});

    lein_process_runtime();
    lein_destroy();

    return 1;

#if 0
    system_info_print();

    const char* pStr2 = "Привет, мир!";
    printf("%s\n", pStr2);
    SimpleString* pName = simple_string(pStr2);
    printf("%.*s\n", pName->Size, pName->pData);

    SimpleApplicationSettings set = {
	.pName = pStr2,//"Hello world app",

	.ArgsCount = 0,
	.aArgs = NULL,

	.IsDebug = 1,

	.IsVsync = 1,
	.Width = 1000,
	.Height = 900,
    };

    simple_application_create(&set);

    SimpleLayer sandboxLayer = {
	.Name = simple_string("Sandbox"),
	.OnAttach = sandbox_on_attach,
	.OnUpdate = sandbox_on_update,
	.OnEvent = sandbox_on_event,
	.OnUi = sandbox_on_ui,
	.OnDestroy = sandbox_on_destroy,
    };
    simple_application_add_layer(sandboxLayer);

    simple_application_run();
    simple_application_destroy();

    return 0;
#endif
}

#if 0
void
create_user_application(UserApplicationSettings* pSettings)
{
    ApplicationSettings appSettings = {
	.Width = 2000,
	.Height = 1200,
	.IsInitOpenGl = 1,
	.IsInitImGui = 0,
	.Name = "Sandbox",
    };
    application_create(appSettings);

    Layer sandboxLayer = {
	.Name = "Sandbox Layer",
	.OnAttach = sandbox_on_attach,
	.OnUpdate = sandbox_on_update,
	.OnUIRender = sandbox_on_ui_render,
	.OnEvent = sandbox_on_event,
	.OnDestoy = sandbox_on_destroy
    };

    application_push_layer(sandboxLayer);
}

#endif
