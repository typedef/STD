#ifndef SED_H
#define SED_H

typedef char sed_i8;
typedef int sed_i32;
typedef unsigned int sed_u32;
typedef unsigned long long sed_u64;

typedef struct sed_v2
{
    sed_i32 X;
    sed_i32 Y;
} sed_v2;

void sed_run();

void sed_terminal_init();
void sed_terminal_deinit();
void sed_terminal_enable_raw_mode();
void sed_terminal_clear_screen();
sed_v2 sed_terminal_get_size();
sed_v2 sed_terminal_get_cursor_position();

// Editor
void sed_editor_move_cursor(sed_i32 x, sed_i32 y);

#endif // SED_H
