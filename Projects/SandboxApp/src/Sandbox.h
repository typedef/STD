#ifndef SANDBOX_H
#define SANDBOX_H

#include <Core/Types.h>


struct SwlEvent;

void sandbox_on_attach();
void sandbox_on_update();
void sandbox_on_event(struct SwlEvent* pEvent);
void sandbox_on_ui();
void sandbox_on_destroy();

#endif // SANDBOX_H
