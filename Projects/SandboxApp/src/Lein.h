#ifndef LEIN_H
#define LEIN_H

#include <Core/Types.h>


typedef struct LeinSettings
{
    v2i WindowSize;
    v2i WindowPosition;
} LeinSettings;

void lein_create(LeinSettings settings);
void lein_process_runtime();
void lein_destroy();

#endif // LEIN_H
