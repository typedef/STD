#ifndef LEIN_RENDERER_H
#define LEIN_RENDERER_H

#include <Core/Types.h>
#include <Core/SimpleAtlasFont.h>


typedef struct LeinRendererSettings
{
    v2i WindowSize;
    void* pWindowBackend;
} LeinRendererSettings;

typedef struct LeinFont
{
    char* pName;
    i32 Size;
    SafFont Handle;
} LeinFont;


void lein_renderer_create(LeinRendererSettings settings);
void lein_renderer_destroy();
void lein_renderer_set_color(v4 color);
void lein_renderer_flush();

void lein_renderer_draw_rect(v3 pos, v2 size, v4 color);
void lein_renderer_draw_text(v3 pos, v4 color, char* pText);

v2 lein_renderer_get_text_metrics(char* pText);
i32 lein_renderer_get_max_height();

#endif // LEIN_RENDERER_H
