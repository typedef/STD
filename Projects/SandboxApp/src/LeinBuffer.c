#include "LeinBuffer.h"
#include "Core/SimpleStandardLibrary.h"
#include "LeinRenderer.h"
#include <Core/SimpleMath.h>
#include <stdio.h>


/*
  char** aVisibleLines;

  char** aAllLines;


 */

char**
f()
{
    return NULL;
}


LeinBuffer
lein_buffer_new_from_file(LeinBufferFileSettings set)
{
    u64 size;
    char* pFileContent = file_read_string_ext(set.pPath, &size);
    char** aLines = string_split(pFileContent, '\n');

    LeinBuffer buffer = {
	// DOCS: Basic file info
	.pPath = set.pPath,
	.pName = path_get_name(set.pPath),
	.TotalBytes = size,

	// DOCS: All file lines
	.aLines = aLines,

	// DOCS: State
	.CurrentLine = 1,
	.CurrentColumn = 0,

	.Position = set.Position,
    };

    return buffer;
}

void
lein_buffer_draw(LeinBuffer buffer)
{
    i32 fontMaxHeight = lein_renderer_get_max_height();

    v2 lineZeroMetrics = lein_renderer_get_text_metrics(buffer.aLines[0]);

    // DOCS: Config for buffer
    const f32 c_line_margin_y = 15;
    const f32 c_line_margin_x = 5;

    f32 buffer_pos_x = buffer.Position.X + c_line_margin_x;
    f32 buffer_pos_y = buffer.Position.Y - lineZeroMetrics.Y - c_line_margin_y;

    v3 bufferStartPos = { buffer_pos_x, buffer_pos_y, 10.1 };
    v2 bufferSize = {600, 750};
    v4 bufferBackgroundColor = v4_new(0.01, 0.01, 0.01, 1.0);
    lein_renderer_draw_rect(v3_add(bufferStartPos, v3_new(0,-bufferSize.Y, 0)), bufferSize, bufferBackgroundColor);

    v4 bufferTextColor = v4_new(1, 1, 1, 1);

    for (i64 i = 0; i < array_count(buffer.aLines); ++i)
    {
	bufferStartPos.Z -= 0.1;

	char* pLine = buffer.aLines[i];
	v2 lineMetrics = lein_renderer_get_text_metrics(pLine);
	// draw characters ...
	lein_renderer_draw_text(bufferStartPos, bufferTextColor, pLine);

	bufferStartPos.Y -= (lineMetrics.Y + c_line_margin_y);
    }
}
