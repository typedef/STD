#include "Lein.h"
#include "LeinRenderer.h"
#include "Std/StdAssetManager.h"
#include "Std/StdRenderer.h"
#include <Core/SimpleMath.h>
#include <Core/SimpleWindowLibrary.h>
#include <Core/SimpleStandardLibrary.h>
#include "LeinBuffer.h"


struct GlobalItem
{
    i32 IsAppRun;
    SwlWindow Window;

    i64 DefaultImage;
    i64 ActiveBufferIndex;

    LeinBuffer* aBuffers;
};


static struct GlobalItem g = {};


void lein_window_on_event(SwlEvent* pEvent);


void
lein_create(LeinSettings settings)
{
    const i32 c_min_width = 150;
    const i32 c_min_height = 150;

    swl_init((SwlInit) {
	    .MemoryAllocate = malloc,
	    .MemoryFree = free,
	});

    SwlWindowSettings set = {
	.pName = "Lein editor",
	.NameLength = 11,
	.Size = {
	    .Width = (swl_i32) settings.WindowSize.Width,
	    .Height = (swl_i32) settings.WindowSize.Height,
	    .MinWidth = (swl_i32) c_min_width,
	    .MinHeight = (swl_i32) c_min_height,
	},
	.Position = {
	    .X = settings.WindowPosition.X,
	    .Y = settings.WindowPosition.Y
	}
    };

    swl_window_create(&g.Window, set);

    swl_window_set_event_call(&g.Window, lein_window_on_event);

    // DOCS: Renderer
    lein_renderer_create((LeinRendererSettings) {
	    .WindowSize = settings.WindowSize,
	    .pWindowBackend = g.Window.pBackend
	});


    // todo: impl
    LeinBuffer defaultBuffer = {

    };

}


void
lein_process_runtime()
{
    g.IsAppRun = 1;

    const f32 c_very_dark_color = 0.001f;

    while (g.IsAppRun)
    {
	lein_renderer_set_color(v4_new(c_very_dark_color, c_very_dark_color, c_very_dark_color, 1.0));

	// DOCS: Present active buffer
	{
	    if (g.aBuffers != NULL && g.ActiveBufferIndex >= 0 && g.ActiveBufferIndex < array_count(g.aBuffers))
	    {
		LeinBuffer activeBuffer = g.aBuffers[g.ActiveBufferIndex];
		lein_buffer_draw(activeBuffer);
	    }
	}

	lein_renderer_flush();
	swl_window_update(&g.Window);

	// DOCS: Close button was pressed
	if (!g.Window.IsAlive)
	{
	    g.IsAppRun = 0;
	}
    }

}

void
lein_destroy()
{
    swl_window_destroy(&g.Window);
}

void
lein_window_on_event(SwlEvent* pEvent)
{
    switch (pEvent->Type)
    {

    case SwlEventType_KeyPress:
    {
	SwlKeyPressEvent* pKeyPress = (SwlKeyPressEvent*) pEvent;
	if (pKeyPress->Key == SwlKey_Escape)
	{
	    g.IsAppRun = 0;
	    GINFO("Close lein app\n");
	}
	else if (pKeyPress->Key == SwlKey_F1)
	{
	    const char* pPath = "/home/bies/BigData/programming/C/Cafk/Cafk/src/EntryPoin.c";

	    if (g.aBuffers == NULL || (array_index_of(g.aBuffers, string_compare(item.pPath, pPath)) == -1))
	    {
		LeinBuffer newBuffer =  lein_buffer_new_from_file((LeinBufferFileSettings) {
		    .pPath = pPath,
		    .StartsFromLine = 1,
		    .LinesCount = 10,

		    // DOCS: Hardcoded for now
		    .Position = v3_new(0, g.Window.Size.Height - 15, 10),
		});

		i32 newBufferInd = array_count(g.aBuffers);
		array_push(g.aBuffers, newBuffer);
		g.ActiveBufferIndex = newBufferInd;

		GINFO("Open buffer\n");
	    }

	}
	break;
    }



    default:
	break;
    }
}
