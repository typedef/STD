#include "SpnetServer.h"
#include "Core/SimpleStandardLibrary.h"
#include <stdlib.h>

#if defined(PLATFORM_LINUX)
#include <sys/select.h>
#include <netinet/in.h>
#elif defined(PLATFORM_WINDOWS)
#endif // PLATFORM_WINDOWS


static Socket gServerSocket = {};
static i32 gServerRunning = 0;
static SpnetServer gServer = {};
static const char* gConnection = "cmd_con";

void*
spnet_server_thread(void* pData)
{
    SpnetServerSettings settings = *((SpnetServerSettings*)pData);

    SocketSettings set = (SocketSettings) {
	.Type = SocketType_UDP,
	.IpType = IpType_V4,
	.Address = {
	    .Ip = settings.Ip,
	    .Port = settings.Port,
	}
    };

    gServerSocket = socket_new(set);
    socket_make_async(&gServerSocket);

    i32 bindResult = socket_bind(&gServerSocket);
    if (!bindResult)
    {
	GERROR("Can't bind\n");
	return NULL;
    }

    GSUCCESS("Server started on endpoint %s:%d ...\n", ip_to_user_string(gServerSocket.Address.sin_addr.s_addr), htons(gServerSocket.Address.sin_port));

    gServerRunning = 1;
    fd_set readSet;

    while (gServerRunning)
    {
	FD_ZERO(&readSet);
	FD_SET(gServerSocket.Descriptor, &readSet);

	i32 selectResult = select(gServerSocket.Descriptor + 1, &readSet, NULL, NULL, NULL);
	if (!selectResult)
	{
	    // todo: handle error
	    perror("[select]");
	}

	if (FD_ISSET(gServerSocket.Descriptor, &readSet))
	{
	    char buf[1024] = {};
	    struct sockaddr_in clientAddr = {};
	    i32 recvResult = socket_recv_from_ext(gServerSocket.Descriptor, buf, 7, MSG_PEEK, &clientAddr);
	    i32 ind = array_index_of(gServer.aClients, item.ServerAddress.sin_addr.s_addr == clientAddr.sin_addr.s_addr);

	    if (ind == -1)
	    {
		if (string_compare_length(buf, gConnection, 7))
		{
		    recvResult = socket_recv_from_ext(gServerSocket.Descriptor, buf, 7, 0, &clientAddr);

		    Socket client = socket_new((SocketSettings){
			    .ServerAddress = {
				.Ip = ntohl(clientAddr.sin_addr.s_addr),
				.Port = ntohs(gServerSocket.Address.sin_port)
			    },
			    .IpType = IpType_V4,
			    .Type = SocketType_UDP,
			});
		    array_push(gServer.aClients, client);

		    GSUCCESS("New Client %s:%d ...\n", ip_to_user_string(clientAddr.sin_addr.s_addr), ntohs(gServerSocket.Address.sin_port));
		}
		else
		{
		    GINFO("unknown client send data %s!\n", ip_to_user_string(clientAddr.sin_addr.s_addr));
		    socket_recv_from_ext(gServerSocket.Descriptor, buf, 1024, 0, &clientAddr);
		}
	    }
	    else
	    {
		GINFO("Already registered user sent data\n");

		do
		{
		    recvResult =
			socket_recv_from_ext(gServerSocket.Descriptor, buf, 1024, 0, &clientAddr);
		    if (settings.RecvDataCallback)
			settings.RecvDataCallback(buf, recvResult);
		}
		while (recvResult > 0);
	    }

	}

    }

    GWARNING("Server is going down..\n");

    return NULL;
}

SimpleThread
spnet_server_start(SpnetServerSettings settings)
{
    SpnetServerSettings* pSettings = (SpnetServerSettings*)calloc(1, sizeof(SpnetServerSettings));
    *pSettings = settings;
    SimpleThread serverThread = simple_thread_create(spnet_server_thread, KB(100), pSettings);
    return serverThread;
}

void
spnet_server_sent(void* pData, i64 dataSize)
{
    i32 i, count = array_count(gServer.aClients);
    for (i = 0; i < count; ++i)
    {
	Socket client = gServer.aClients[i];
	//GINFO("Client %s:%d\n", ip_to_user_string(client.ServerAddress.sin_addr.s_addr), ntohs(client.ServerAddress.sin_port));
	socket_send_to(&client, pData, dataSize);
    }
}

void
spnet_server_sent_to(u32 ip, u16 port, void* pData, i64 dataSize)
{
    Socket socket = socket_new((SocketSettings) {
	.Type = SocketType_UDP,
	.IpType = IpType_V4,
	.ServerAddress = {
	.Ip = ip,
	.Port = port,
	}
    });

    socket_send_to(&socket, pData, dataSize);
}
