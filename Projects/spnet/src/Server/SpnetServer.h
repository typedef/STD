#ifndef SPNET_SERVER_H
#define SPNET_SERVER_H

#include <Core/Types.h>
#include <Core/SimpleStandardLibrary.h>


typedef struct SpnetServer
{
    Socket* aClients;
} SpnetServer;

typedef struct SpnetServerSettings
{
    u32 Ip;
    u16 Port;
    void (*RecvDataCallback)(void* pData, i64 size);
} SpnetServerSettings;

SimpleThread spnet_server_start(SpnetServerSettings settings);
void spnet_server_sent(void* pData, i64 dataSize);
void spnet_server_sent_to(u32 ip, u16 port, void* pData, i64 dataSize);

#endif // SPNET_SERVER_H
