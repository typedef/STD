#include <Core/SimpleStandardLibrary.h>
#include <Server/SpnetServer.h>
#include <Deps/miniaudio.h>


static ma_device device;
static ma_engine engine;
static ma_pcm_rb rb;

void
callback_recv_data(void* pData, i64 dataSize)
{
#if defined(PLATFORM_LINUX)
    printf("data[%lld]:%s\n", dataSize, (char*) pData);
#elif defined(PLATFORM_WINDOWS)
    printf("data[%lld]:%s\n", dataSize, (char*) pData);
#endif // PLATFORM_WINDOWS
}

void
callback_send_data(ma_device* pDevice, void* pFramesOut, const void* pFramesIn, ma_uint32 frameCount)
{
    /* We need to write to the ring buffer. Need to do this in a loop. */
    ma_uint32 framesWritten = 0;
    while (framesWritten < frameCount)
    {
	void* pMappedBuffer;
	ma_uint32 framesToWrite = frameCount - framesWritten;

	 ma_result result = ma_pcm_rb_acquire_write(&rb, &framesToWrite, &pMappedBuffer);
	if (result != MA_SUCCESS) {
	    break;
	}

	if (framesToWrite == 0) {
	    break;
	}

	/* Copy the data from the capture buffer to the ring buffer. */
	ma_copy_pcm_frames(pMappedBuffer, ma_offset_pcm_frames_const_ptr_f32(pFramesIn, framesWritten, pDevice->capture.channels), framesToWrite, pDevice->capture.format, pDevice->capture.channels);

	result = ma_pcm_rb_commit_write(&rb, framesToWrite);
	if (result != MA_SUCCESS) {
	    break;
	}

	framesWritten += framesToWrite;

	spnet_server_sent(pMappedBuffer, framesToWrite);

    }
}

void*
spnet_temp_connection_to_user(void* pData)
{
    (void)pData;

    u32 ip;
    u16 port;

    do {
	printf("Write address in format 'ip:port'\n");

	char buf[22] = {};
	scanf("%21s", buf);

	i32 isValidAddress = socket_parse_ip_port(buf, &ip, &port);
	if (!isValidAddress)
	{
	    GERROR("Ip isn't valid %s\n", buf);
	}
	else
	{
	    GINFO("Connection to: %s:%d\n", ip_to_string(ip), port);
	    break;
	}
    }
    while(1);

    spnet_server_sent_to(ip, port, "cmd_con", 7);

    return NULL;
}

i32
main(i32 argc, char** argv)
{
    GINFO("Hello, net world!\n");

    { // DOCS: audio related
#if 0
	ma_device_config deviceConfig;
	/* Initialize the capture device. */
	deviceConfig = ma_device_config_init(ma_device_type_capture);
	deviceConfig.capture.format = ma_format_f32;
	deviceConfig.dataCallback = callback_send_data;

	ma_result result = ma_device_init(NULL, &deviceConfig, &device);
	if (result != MA_SUCCESS)
	{
	    printf("Failed to initialize capture device.");
	    return -1;
	}

	/* Initialize the ring buffer. */
	result = ma_pcm_rb_init(device.capture.format, device.capture.channels, device.capture.internalPeriodSizeInFrames * 5, NULL, NULL, &rb);
	if (result != MA_SUCCESS)
	{
	    printf("Failed to initialize the ring buffer.");
	    return -1;
	}

	/*
	  Ring buffers don't require a sample rate for their normal operation, but we can associate it
	  with a sample rate. We'll want to do this so the engine can resample if necessary.
	*/
	ma_pcm_rb_set_sample_rate(&rb, device.sampleRate);

	result = ma_engine_init(NULL, &engine);
	if (result != MA_SUCCESS) {
	    printf("Failed to initialize the engine.");
	    return -1;
	}

#endif
    }

    // note: для тестирования нужно вынести порты в отдельные переменные
    i32 serverPort = 9009;
    i32 clientPort = 1337;

    socket_init();
    socket_set_debug_mode();

    // todo: impl server
    SimpleThread serverThread = spnet_server_start((SpnetServerSettings){
	    .Ip = socket_get_ip(),
	    .Port = serverPort,
	    .RecvDataCallback = callback_recv_data,
	});

    // todo: impl client

    // todo: connect to other host, for now in sep thread
    SimpleThread connectionThread = simple_thread_create(spnet_temp_connection_to_user, KB(1), NULL);

    // note: client main loop
    while (1)
    {
	// GINFO("Its working\n");
	char msg[128] = {};
	i32 bytesCnt = snprintf(msg, 128, "hello, from %d", serverPort);

	u32 ip = ip_as_integer(192,168,1,152);
	u16 port = 9009;
	//spnet_server_sent_to(ip, port, msg, bytesCnt);

	spnet_server_sent(msg, bytesCnt);

	simple_timer_sleep(1);
    }

    i32 dd;
    scanf("%d", &dd);

    return 0;
}
