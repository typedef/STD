#include "StdEditor.h"
#include "Application/SimpleApplication.h"
#include "Core/SimpleStandardLibrary.h"
#include "Core/Types.h"
#include "EntitySystem/StdCameraEntity.h"

#include <Core/SimpleWindowLibrary.h>
#include <Std/StdRenderer.h>
#include <Std/StdTypes.h>
#include <Std/StdAtlas.h>
#include <Math/SimpleMath.h>
#include <Math/SimpleMathIO.h>
#include <Std/StdAssetManager.h>
#include <Std/StdTextureRecord.h>


static i32 gAppRun = 1;
static SwlWindow gSwlWindow = {};
//static CameraComponent gCameraComponent = {};
static StdCameraEntity gCamera = {};

static i64 TheOnlyOneTextureWeHave = 0;
static i64 Other = 0;

/*
  todo: fix bug: Flickering texture while camera moving

  Reason: Presently I'm storing camera information in uniforms that get updated at the beginning of the frame using map/unmap/flush. The problem is that I haven't added the necessary fences to prevent this update from happening while the previous frame is still rendering so it changes at arbitrary times during the render since the camera updates aren't in the command queue.
 */

void
std_editor_on_attach()
{
    SwlWindow* pWindow = simple_application_get_window();

    gCamera = std_camera_entity_new((StdCameraEntitySettings) {
            .WindowSize = { pWindow->Size.Width, pWindow->Size.Height }
        });
    std_camera_entity_move_update(&gCamera, v3_new(0,0,1));

    StdRendererSettings set = {
        .WindowSize = {pWindow->Size.Width, pWindow->Size.Height},
        .pWindowBackend = pWindow->pBackend,
        .MaxItemsCount = 100,
        .MaxCharsCount = 100,
        .IsDebug = 1,
        .IsVsync = 1,
    };
    std_renderer_create(&set);
    std_renderer_set_clear_color(v4_new(0.004321, 0.00806007, 0.007, 1));
    std_renderer_set_camera(&gCamera.ViewProjection);

    {
        i64 defId = std_asset_manager_load_texture_by_id(resource_texture("white.png"));
        StdTextureRecord whiteRec = {};
        std_asset_manager_get_texture(defId, &whiteRec);

        //const char* path = "/home/bies/BigData/programming/C/SimpleGameEngine/assets/textures/atlas/RPGpack_sheet.png";
        const char* sheet = "/home/bies/BigData/programming/C/SimpleGameEngine/assets/textures/atlas/RPGpack_sheet.png";
        i64 id = std_asset_manager_load_texture_by_id(sheet);
        vguard(id != -1);
        StdTextureRecord sheetRec = {};
        std_asset_manager_get_texture(id, &sheetRec);

        const char* warPath = "/home/bies/Загрузки/log/Animations/Knight/Colour1/NoOutline/120x80_PNGSheets/_Idle.png";
        i64 warId = std_asset_manager_load_texture_by_id(warPath);
        vguard(warId != -1);
        StdTextureRecord warRec = {};
        std_asset_manager_get_texture(warId, &warRec);

        TheOnlyOneTextureWeHave = id;
        Other = warId;

        StdTextureRecord aTextures[] = {
            [0] = whiteRec,
            [1] = sheetRec,
            [2] = warRec,
        };

        std_renderer_set_textures(aTextures, ArrayCount(aTextures));
    }
}

void
std_editor_on_update()
{
    SwlWindow* pWindow = simple_application_get_window();
    SimpleRuntimeStats runtimeStats = simple_application_get_stats();

    wchar_t* pText = L"Привет";
    std_renderer_draw_string_ext(v3_new(150, 900, 1), v4_new(1, 1, 0, 0), pText, 6);
    std_renderer_draw_texture(v3_new(150, 110, 1), v2_new(100, 110), TheOnlyOneTextureWeHave);
    std_renderer_draw_texture(v3_new(950, 700, 2), v2_new(-1, 0), TheOnlyOneTextureWeHave);
    std_renderer_draw_texture(v3_new(1050, 110, 3), v2_new(-2, 0), Other);
    std_renderer_draw_rect(v3_new(950, 340, 3), v2_new(150, 150), v4_new(0, 1, 0, 1));

    std_renderer_update();


    i32 isCameraMoved = 0;
    real speed = 250;
    if (pWindow->aKeys[SwlKey_Shift] == SwlAction_Press)
    {
        speed = speed*10;
    }
    real posToAdd = speed * runtimeStats.Timestep;

    // note: just for testing camera update in renderer
    if (pWindow->aKeys[SwlKey_D] == SwlAction_Press)
    {
        std_camera_entity_move(&gCamera, v3_negativec(posToAdd, 0, 0));
    }
    if (pWindow->aKeys[SwlKey_A] == SwlAction_Press)
    {
        std_camera_entity_move(&gCamera, v3_new(posToAdd, 0, 0));
    }
    if (pWindow->aKeys[SwlKey_W] == SwlAction_Press)
    {
        std_camera_entity_move(&gCamera, v3_negativec(0, posToAdd, 0));
    }
    if (pWindow->aKeys[SwlKey_S] == SwlAction_Press)
    {
        std_camera_entity_move(&gCamera, v3_new(0, posToAdd, 0));
    }

    if (gCamera.IsCameraMoved)
    {
        std_camera_entity_update(&gCamera);
        std_renderer_set_camera(&gCamera.ViewProjection);
    }

}

void
std_editor_on_event()
{

}

void
std_editor_on_ui()
{

}

void
std_editor_on_destroy()
{

}
