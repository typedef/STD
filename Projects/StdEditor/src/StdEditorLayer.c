#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <Core/Types.h>
#include <Core/SystemInfo.h>
#include <Core/SimpleStandardLibrary.h>
#include <Application/SimpleApplication.h>
#include "StdEditor.h"
#include <Math/SimpleMath.h>


int
main(int argc, char** apArgs)
{
    setlocale(LC_ALL, ".UTF8");
    system_info_print();

    char** aArgs = NULL;
    for (i32 i = 0; i < argc; ++i)
    {
	array_push(aArgs, apArgs[i]);
    }

    const char* pEditorName = "Std - движок";
    SimpleApplicationSettings set = {
	.pName = pEditorName,
	.ArgsCount = argc,
	.aArgs = aArgs,
	.IsDebug = 1,
	.IsVsync = 1,
	.Size = v2_new(1000, 900),
    };

    simple_application_create(&set);

    SimpleLayer stdEditorLayer = {
	.pName = simple_string_new("StdEditor"),
	.OnAttach = std_editor_on_attach,
	.OnUpdate = std_editor_on_update,
	.OnEvent = std_editor_on_event,
	.OnUi = std_editor_on_ui,
	.OnDestroy = std_editor_on_destroy,
    };
    simple_application_add_layer(stdEditorLayer);

    simple_application_run();
    simple_application_destroy();


    return 0;
}
