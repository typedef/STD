#ifndef STD_EDITOR_H
#define STD_EDITOR_H

void std_editor_on_attach();
void std_editor_on_update();
void std_editor_on_event();
void std_editor_on_ui();
void std_editor_on_destroy();

#endif // STD_EDITOR
