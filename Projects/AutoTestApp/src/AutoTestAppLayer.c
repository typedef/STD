#include "AutoTests/Utils/SimpleGltfTest.h"
#include "Utils/AutoTest/AutoTest.h"
#include "Utils/AutoTest/AutoTestUi.h"
#include <stdio.h>
#include <stdlib.h>
#include <Core/Types.h>
#include <Core/SystemInfo.h>
#include <Core/SimpleStandardLibrary.h>
#include <Application/SimpleApplication.h>
#include <locale.h>

#include <AutoTests/Core/SimpleStringTest.h>


i32
main()
{
    setlocale(LC_ALL, ".UTF8");
    system_info_print();

    //at_simple_string_test();
    at_simple_gltf_test();

    AutoTestGlobal* pAutoTestGlobal = auto_test_get();
    // note: disable visualization for now
    //auto_test_ui_draw_file_list(pAutoTestGlobal->aFiles);

    return 0;
}
