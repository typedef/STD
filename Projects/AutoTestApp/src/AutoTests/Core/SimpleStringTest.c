#include "SimpleStringTest.h"
#include <stdlib.h>
#include <Utils/AutoTest/AutoTest.h>
#include <Core/Types.h>
#include <Core/SimpleStandardLibrary.h>


void
simple_string_create()
{
    AutoTest_Condition(1 != 0);

    char* pAllocatedString = simple_string_new("Hello, Привет, Salve");
    AutoTest_String_Value(pAllocatedString);

    const i32 alloc_size = 10;
    char* pCheckAllocation = (char*) malloc(alloc_size);
    for (i32 i = 0; i < alloc_size; ++i)
    {
	*pCheckAllocation = i;
	++pCheckAllocation;
    }

    simple_string_destroy(pAllocatedString);
}

void
at_simple_string_test()
{
    AutoTest_File();

    AutoTest_Func(simple_string_create());
}
