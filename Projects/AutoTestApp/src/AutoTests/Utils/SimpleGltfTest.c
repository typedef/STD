#include "SimpleGltfTest.h"
#include <Utils/Parsers/SimpleGltf.h>
#include <Utils/AutoTest/AutoTest.h>


void
simple_gltf_parse_test()
{
#if 0
    const char* pGltfPath = "/home/bies/BigData/programming/C/SimpleGameEngine/assets/models/CesiumMan/CesiumMan2.gltf";
#else
    const char* pGltfPath = "/home/bies/BigData/programming/C/SimpleGameEngine/assets/models/CesiumMan/cube.gltf";
#endif

    simple_gltf_new(pGltfPath);
}


void
at_simple_gltf_test()
{
    AutoTest_File();

    AutoTest_Func(simple_gltf_parse_test());
}
