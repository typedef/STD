#ifndef VK_SANDBOX_H
#define VK_SANDBOX_H

struct SwlEvent;

void vk_sandbox_on_attach();
void vk_sandbox_on_attach_finished();
void vk_sandbox_on_update();
void vk_sandbox_on_ui();
void vk_sandbox_on_event(struct SwlEvent* event);
void vk_sandbox_on_destroy();

#endif // VK_SANDBOX_H
