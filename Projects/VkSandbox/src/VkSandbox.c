#include "VkSandbox.h"
#include "Core/SimpleAtlasFont.h"
#include "Core/Types.h"
#include "Std/StdAtlasAnimation.h"

#include <Core/SimpleApplication.h>
#include <Core/SimpleStandardLibrary.h>
#include <Core/SimpleWindowLibrary.h>
#include <Core/SimpleVulkanLibrary.h>
#include <Core/SimpleMath.h>
#include <Std/StdRenderer.h>
#include <Std/StdAssetManager.h>
#include <EntitySystem/StdCameraEntity.h>
#include <EntitySystem/StdScene.h>
#include <Deps/stb_image.h>
#include <Core/SimpleMathIO.h>


/*
  todo:
  1. blending for two transperant rectangles

*/

#define RECT_IMAGE_DEMO 0
#define ATLAS_DEMO 0
#define UI_FONT_DEMO 1
#define UI_IMAGE_DEMO 0
#define UI_EMPTY_RECT_DEMO 0

struct GlobalItems
{
    i64 BfImageId;
    i64 GroundAtlasId;
    StdAtlasAnimation KnightAnimation;

    i32 CurrentSlide;
};

static struct GlobalItems g = {};

void
vk_sandbox_on_attach()
{
    SwlWindow* pWindow = simple_application_get_window();

    StdRendererSettings set = {
	.WindowSize = {pWindow->Size.Width, pWindow->Size.Height},
	    .pWindowBackend = pWindow->pBackend,
	    .MaxItemsCount = 1000,
	    .MaxCharsCount = 10000,
	    .MaxImageCount = 100,
	    .IsDebug = 1,
	    .IsVsync = 1,
    };

    GINFO("Create renderer\n");
    std_renderer_create(&set);

    StdCameraEntity* pMainCamera = std_scene_get_main_camera();
    std_renderer_set_camera(&pMainCamera->ViewProjection);

    { // DOCS: Set ui camera
	StdCameraEntitySettings set = {
	    .IsMain = 0,
	    .WindowSize = {pWindow->Size.Width, pWindow->Size.Height},
	    .Position = {}
	};
	StdCameraEntity uiCamera = std_camera_entity_new(set);
	//std_scene_add_camera(uiCamera);
	std_renderer_set_ui_camera(&uiCamera.ViewProjection);

	m4_print(uiCamera.ViewProjection, "uiViewProj");
    }

#if UI_FONT_DEMO == 1
    {

	// DOCS: Add all fonts
	const char* pDefaultFont = resource_font("MartianMono-sWdRg.ttf");

	// DOCS: russian language
	std_asset_manager_load_font(pDefaultFont, 20, 1040, 1103);
	std_asset_manager_load_font(pDefaultFont, 32, 1040, 1103);
	std_asset_manager_load_font(pDefaultFont, 60, 1040, 1103);

	// Japanese-style punctuation ( 3000 - 303f)
	std_asset_manager_load_font(resource_font("NotoSansJP.ttf"), 32, 0x3000, 0x303f);
	// Hiragana ( 3040 - 309f)
	std_asset_manager_load_font(resource_font("NotoSansJP.ttf"), 32, 0x3040, 0x309f);
	// Katakana ( 30a0 - 30ff)
	std_asset_manager_load_font(resource_font("NotoSansJP.ttf"), 32, 0x30A0, 0x30FF);
	// Full-width roman characters and half-width katakana ( ff00 - ffef)
	//std_asset_manager_load_font(resource_font("NotoSansJP.ttf"), 32, 0xff00, 0xffef);
//	std_asset_manager_load_font(resource_font("NotoSansJP.ttf"), 32, 0x4E00, 0x9FFF);
	std_asset_manager_load_default_font_for_all_sizes();

	std_renderer_set_ui_fonts();
    }
#endif // UI_FONT_DEMO == 1

#if RECT_IMAGE_DEMO == 1 || UI_IMAGE_DEMO == 1

    g.BfImageId = std_asset_manager_load_image("Assets/Interface/BF3.png");
    g.GroundAtlasId = std_asset_manager_load_image("Assets/Atlas/Ground-9-21.png");

#endif // RECT_IMAGE_DEMO == 1

#if ATLAS_DEMO == 1
    g.KnightAnimation = std_atlas_animation_new((StdAtlasAnimationSettings) {
	    .SecondsForOneIter = 2.,
	    .Atlas = std_atlas_new("Assets/Atlas/Idle-120-80.png")
	});
#endif

    {
	StdTexture* aTextures = std_asset_manager_get_all_images();
	if (sva_count(aTextures))
	    std_renderer_set_textures(aTextures, sva_count(aTextures));
    }

}

void
vk_sandbox_on_destroy()
{

}

void wg_controller_update();

typedef struct SliderItem {
    f32 StartX;
    f32 StartY;
} DemoItem;

typedef void (*FontSliderDelegate)();

void
font_slide_intro()
{
    SwlWindow* pWindow = simple_application_get_window();
    f32 width  = pWindow->Size.Width;
    f32 height = pWindow->Size.Height;

    const char* pDemoLabel = "Демонстрация рендеринга шрифта.";
    std_renderer_draw_ui_text_w_fontsize(v3_new(width/2-550, height - 80, 3), v4_new(1, 1, 1, 1), (char*)pDemoLabel, 60);
}

void
font_c_the_programming_language()
{
    /* DOCS: Ritchi and Kernigan Book C THE PROGRAMMING LANGUAGE */

    DemoItem rkBook = {
	.StartX = 50,
	.StartY = 900,
    };

    const char* pTitle = "Язык программирования Си";
    v3 titlePos = v3_new(rkBook.StartX, rkBook.StartY, 3);
    std_renderer_draw_ui_text_w_fontsize(titlePos, v4_rgb(54, 95, 145), (char*)pTitle, 60);

    v3 pPrefacePos = v3_addy(titlePos, -100);
    const char* pPreface = "Предисловие";
    std_renderer_draw_ui_text(pPrefacePos, v4_rgb(54, 95, 145), (char*)pPreface);

    f32 firstLineMargin = 50;
    f32 lineHeight = 35;
    f32 paragraphMargin = 2*lineHeight;
    v4 bookTextColor = v4_rgb(255, 255, 255);

    v3 startPos = v3_addy(pPrefacePos, -firstLineMargin);

#define ShouldMakeParagraph(xi) (xi == 4)

    char* aLines[] = {
	// DOCS: Paragraph 0
	[0] = "С момента публикации в 1978 г. книги \"Язык программирования Си\" в мире компьютеров произошла",
	[1] = "революция. Большие машины стали еще больше, а возможности персональных ЭВМ теперь сопоставимы",
	[2] = "с возможностями больших машин десятилетней давности. Язык Си за это время также изменился,",
	[3] = "хотя и не очень сильно; что же касается сферы применения Си, то она далеко вышла за рамки его",
	[4] = "начального назначения как инструментального языка операционной системы UNIX.",

	// DOCS: Paragraph 1
	[5] = "Рост популярности Си, накапливающиеся с годами изменения, создание компиляторов коллективами",
	[6] = "разработчиков, ранее не причастных к проектированию языка, — все это послужило стимулом к",
	[7] = "более точному и отвечающему времени определению языка по сравнению с первым изданием книги.",
	[8] = "В 1983 г. Американский институт национальных стандартов (American National Standards Institute",
	[9] = "- ANSI) учредил комитет, перед которым была поставлена цель выработать \"однозначное и",
	[10] = "машинно-независимое определение языка Си\", полностью сохранив при этом его стилистику.",
	[11] = "Результатом работы этого комитета и явился стандарт ANSI языка Си."
    };

    v3 linePos = startPos;

    i32 linesCnt = ArrayCount(aLines);
    for (i32 l = 0; l < linesCnt; ++l)
    {
	char* pLineText = aLines[l];
	std_renderer_draw_ui_text_w_fontsize(linePos, bookTextColor, pLineText, 20);
	if (ShouldMakeParagraph(l))
	{
	    linePos.Y -= paragraphMargin;
	}
	else
	{
	    linePos.Y -= lineHeight;
	}
    }

    std_renderer_draw_ui_rect(v3_new(700, 700, 3), v2_new(200, 200), v4_new(1,1,1,1));
}

void
font_slider_on_event(SwlEvent* pEvent)
{
    if (pEvent->Type != SwlEventType_KeyPress)
	return;

    SwlKeyPressEvent* pKeyEvent = (SwlKeyPressEvent*) pEvent;
    switch(pKeyEvent->Key)
    {
    case SwlKey_0:
	g.CurrentSlide = 0;
	break;
    case SwlKey_1:
	g.CurrentSlide = 1;
	break;

    default:
	break;
    }
}

void
font_slider_on_update()
{
    if (helper_tofr())
    {
	g.CurrentSlide = 0;
    }

    static FontSliderDelegate aRegisteredSlides[] = {
	[0] = font_slide_intro,
	[1] = font_c_the_programming_language,
    };

    if (g.CurrentSlide >= 0 && g.CurrentSlide < ArrayCount(aRegisteredSlides))
    {
	FontSliderDelegate sliderAction = aRegisteredSlides[g.CurrentSlide];
	sliderAction();
    }
}

void
font_demo()
{
    font_slider_on_update();

    /* std_renderer_draw_ui_text(v3_new(900, 650, 3), v4_new(0.6, 0.65, 0.55, 1), "Say my name! Heisenberg. You're goddamn right!"); */
    /* std_renderer_draw_ui_text_w_fontsize(v3_new(90, 750, 3), v4_new(0.6, 0.65, 0.55, 1), "Heisenberg. You're goddamn right!", 80); */
    /* { */
    /*	v3 p = v3_new(650, 550, 3); */
    /*	v4 c = v4_new(0.6, 0.65, 0.55, 1); */
    /*	std_renderer_draw_ui_text(p, c, "И немного юникода 大きなお尻 ookina oshiri - большая удача ;)"); */
    /* } */


    if (0&&helper_tofr())
    {
	i32 aUnicodes[5] = {};
	i64 unicodesCount = 0;
	simple_core_utf8_get_unicode_text(aUnicodes, "大", &unicodesCount);

	for (i32 i = 0; i < unicodesCount; ++i)
	{
	    i32 codepoint = aUnicodes[i];
	    printf("codepoint: %d (hex:%x)\n", codepoint,codepoint);
	}
    }


    //std_renderer_draw_ui_text(v3_new(850, 350, 3), v4_new(1,1,1,1), "こんにちは");

}

void
vk_sandbox_on_update()
{
    SimpleRuntimeStats stats = simple_application_get_stats();

    // GERROR("No blending it's a bug\n");

    std_renderer_set_clear_color(v4_new(0.0009904321, 0.000990806007, 0.0099007, 1));

#if RECT_IMAGE_DEMO == 1

    std_renderer_draw_rect(v3_new(1250, 250, 5), v2_new(250, 250), v4_new(0.74, 0.85, 0.45, 0.8));
    std_renderer_draw_rect(v3_new(1250, 50, 1), v2_new(250, 250), v4_new(0.85, 0.74, 0.45, 0.1));

    std_renderer_draw_image(v3_new(550, 250, 0.2), v2_new(288, 336), g.GroundAtlasId);
    std_renderer_draw_image(v3_new(750, 650, 0.1), v2_new(385, 91), g.BfImageId);

#endif // RECT_IMAGE_DEMO == 1


#if ATLAS_DEMO == 1
    // DOCS: Atlas items
    {
	std_atlas_animation_update(&g.KnightAnimation, stats.Timestep);

	StdAtlasItem item = g.KnightAnimation.Atlas.aAtlasItems[g.KnightAnimation.CurrentItem];
	v3 p = v3_new(100, 100, 0.1);
	v2 s = v2_new(250, 250);
	v4 c = v4_new(1,1,1,1);

	v2 aUvs[] = {
	    v2_new(item.UvMin.X, item.UvMin.Y),
	    v2_new(item.UvMin.X, item.UvMax.Y),
	    v2_new(item.UvMax.X, item.UvMax.Y),
	    v2_new(item.UvMax.X, item.UvMin.Y),
	};

	std_renderer_draw_color_image_uv(p, s, c, g.KnightAnimation.Atlas.TextureId, aUvs);
    }
#endif

#if UI_FONT_DEMO == 1
    // DOCS: Ui text
    {
	font_demo();

#if 0
	std_renderer_draw_ui_text(v3_new(1100, 900, 3), v4_new(1, 0, 0, 1), "Cruel world has been fucked up!");
	std_renderer_draw_ui_text(v3_new(900, 650, 3), v4_new(0.6, 0.65, 0.55, 1), "Say my name! Heisenberg. You're goddamn right!");
	std_renderer_draw_ui_text_w_fontsize(v3_new(90, 750, 3), v4_new(0.6, 0.65, 0.55, 1), "Heisenberg. You're goddamn right!", 80);
	{
	    v3 p = v3_new(650, 550, 3);
	    v4 c = v4_new(0.6, 0.65, 0.55, 1);
	    std_renderer_draw_ui_text(p, c, "И немного юникода 大きなお尻 ookina oshiri - большая удача ;)");
	}
	std_renderer_draw_ui_text(v3_new(850, 350, 3), v4_new(1,1,1,1), "こんにちは");

	std_renderer_set_ui_viewport((StdViewport) {
		.X0 = 0, .Y0 = 1100,
		.Width = 550, .Height = 800,
	    });
#endif // 0

    }
#endif // UI_FONT_DEMO == 1

#if UI_IMAGE_DEMO == 1
    { // DOCS: Ui rect/image
	std_renderer_set_ui_viewport((StdViewport) {
		.X0 = 0, .Y0 = 1100,
		.Width = 550, .Height = 800,
	    });
	std_renderer_draw_ui_image(v3_new(250, 300, 1), v2_new(250, 250), g.BfImageId);
	std_renderer_draw_ui_image(v3_new(250, 700, 1), v2_new(250, 250), g.BfImageId);
	std_renderer_draw_ui_rect(v3_new(50, 310, 2), v2_new(3000, 3000), v4_new(0,1,0,1));
	std_renderer_draw_ui_rect(v3_new(154, 444, 0.2), v2_new(120, 150), v4_new(0,1,1,1));
    }
#endif // UI_IMAGE_DEMO == 1


#if UI_EMPTY_RECT_DEMO == 1
    std_renderer_draw_ui_empty_rect(v3_new(50, 50, 2.21), v2_new(250, 250), 2, v4_new(1,1,1,1));
    std_renderer_draw_ui_empty_rect(v3_new(50, 650, 2.21), v2_new(250, 250), 1, v4_new(1,1,1,1));
    std_renderer_draw_ui_empty_rect(v3_new(50, 650, 2.21), v2_new(250, 250), 25, v4_new(1,1,1,1));

#endif // UI_EMPTY_RECT_DEMO == 1

    //wg_controller_update();
}

void
vk_sandbox_on_ui()
{

}


void
vk_sandbox_on_event(SwlEvent* pEvent)
{
    font_slider_on_event(pEvent);
}


void
wg_controller_update()
{
    static struct {
	i32 EnableDebug;
	v2 Speed;
    } gWgController = { .EnableDebug = 0, .Speed = {1500, 1200} };

    SwlWindow* pWindow = simple_application_get_window();
    SimpleRuntimeStats stats = simple_application_get_stats();
    StdCameraEntity* pCamera = std_scene_get_main_camera();

    i8 isChanged = 0;
    f32 speedModifier = 1.0f;

    if (pWindow->aKeys[SwlKey_Shift] == SwlAction_Press)
    {
	speedModifier = 10.0f;
    }

    if (pWindow->aKeys[SwlKey_A] == SwlAction_Press)
    {
	std_camera_entity_move(pCamera, v2_new(gWgController.Speed.X * stats.Timestep * speedModifier, 0));
	isChanged = 1;
    }
    if (pWindow->aKeys[SwlKey_D] == SwlAction_Press)
    {
	std_camera_entity_move(pCamera, v2_new(-gWgController.Speed.X * stats.Timestep * speedModifier, 0));
	isChanged = 1;
    }

    if (pWindow->aKeys[SwlKey_W] == SwlAction_Press)
    {
	std_camera_entity_move(pCamera, v2_new(0, -gWgController.Speed.Y * stats.Timestep * speedModifier));
	isChanged = 1;
    }
    if (pWindow->aKeys[SwlKey_S] == SwlAction_Press)
    {
	std_camera_entity_move(pCamera, v2_new(0, gWgController.Speed.Y * stats.Timestep * speedModifier));
	isChanged = 1;
    }

    if (isChanged)
    {
	std_camera_entity_update(pCamera);
	std_renderer_set_camera(&pCamera->ViewProjection);
    }

    if (gWgController.EnableDebug)
    {
	// docs: pos.X is negativ shift
	v3 pos = pCamera->Base.Position;
	//v3_print(pos);

	char buf[256]={};
	snprintf(buf, 256, "%0.1f %0.1f %0.1f", ((pos.X!=0)?-pos.X:pos.X), pos.Y, pos.Z);

	v3 shiftedPos = v3_new(500-pos.X, pos.Y + 1100, 2);
	shiftedPos.Y = pWindow->Size.Height - shiftedPos.Y;
	//std_gl_renderer_draw_string(shiftedPos, v4_new(0.7,0.7,0.7,1), buf, 32);
    }
}
