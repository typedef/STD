#include <Core/Types.h>
#include <Core/SimpleStandardLibrary.h>
#include <Core/SimpleApplication.h>
#include <Core/SystemInfo.h>
#include <locale.h>
#include "VkSandbox.h"


i32
main()
{
    setlocale(LC_ALL, ".UTF8");
    system_info_print();

    SimpleApplicationSettings set = {
	.pName = simple_string_new("Привет, мир!"),
	.IsDebug = 1,
	.IsVsync = 1,
	.Size = (v2) { 1920, 1080 },
    };

    simple_application_create(&set);

    SimpleLayer vkSandboxLayer = {
	.pName = simple_string_new("VkSandbox"),
	.OnAttach  = vk_sandbox_on_attach,
	.OnUpdate  = vk_sandbox_on_update,
	.OnUi      = vk_sandbox_on_ui,
	.OnEvent   = vk_sandbox_on_event,
	.OnDestroy = vk_sandbox_on_destroy,
    };
    simple_application_add_layer(vkSandboxLayer);

    simple_application_run();
    simple_application_destroy();

    return 0;
}
