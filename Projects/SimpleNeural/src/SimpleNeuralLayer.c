#include <stdio.h>
#include <stdlib.h>
#include <Core/Types.h>
#include <Core/SystemInfo.h>
#include <Core/SimpleStandardLibrary.h>
#include <locale.h>
#include <Math/SimpleMath.h>


/*

  DOCS: Simple Markov Chain - smc (Smc)

*/

typedef struct SmcWord
{
    char* pWord;
    i64 Counter;
} SmcWord;

typedef struct SmcCounterItem
{
    const char* Key;
    SmcWord* Value;
} SmcCounterItem;

force_inline i32
smc_helper_is_end_word_char(char c)
{
    if (c == '.' || c == '?' || c == '!')
	return 1;
    return 0;
}

force_inline i32
smc_helper_is_word_char(char c)
{
    if (smc_helper_is_end_word_char(c) || c == ',' || c == '-')
	return 0;
    return 1;
}

i32
smc_string_compare(char* pStr0, char* pStr1, i64 str0Length, i64 str1Length)
{
    if (str0Length != str1Length)
	return 0;

    char* ptr0 = pStr0;
    char* ptr1 = pStr1;

    for (i64 i = 0; i < str0Length; ++i)
    {
	if (*ptr0 != *ptr1)
	    return 0;

	++ptr0;
	++ptr1;
    }

    return 1;
}

char**
smc_string_split_length(char* input, u64 inputLength, char splitCharacter)
{
    char** result = NULL;

    if (string_index_of(input, splitCharacter) == -1)
    {
	char* pInput = simple_string_copy(input, inputLength, inputLength);
	array_push(result, pInput);
	return result;
    }

    i64 wordBeginIndex = 0;
    i64 isWordIndexSet = 0;

    for (i64 i = 0; i < inputLength; ++i)
    {
	char c = input[i];
	i32 isWord = smc_helper_is_word_char(c);

	// DOCS: For punctuation mark
	if (!isWord)
	{
	    if (isWordIndexSet)
	    {
		i64 prevWordLength = i - wordBeginIndex;
		char* pPrevWord = simple_string_copy(input + wordBeginIndex, prevWordLength, prevWordLength);
		array_push(result, pPrevWord);
	    }

	    char* pPunctuationMark = simple_string_copy(&input[i], 1, 1);
	    array_push(result, pPunctuationMark);

	    isWordIndexSet = 0;
	}
	else if (c != splitCharacter && !isWordIndexSet)
	{
	    wordBeginIndex = i;
	    isWordIndexSet = 1;
	}
	else if (isWordIndexSet)
	{
	    if (c == splitCharacter)
	    {
		isWordIndexSet = 0;

		i64 wordLength = i - wordBeginIndex;
		char* pWord = simple_string_copy(input + wordBeginIndex, wordLength, wordLength);
		array_push(result, pWord);
	    }
	    else if (i == (inputLength - 1))
	    {
		char* pWord = simple_string_new(&input[i]);
		array_push(result, pWord);
	    }
	}

    }

    return result;
}


// note: done
char**
smc_words_read(char* pData, i64 length, i32 isDataWasPreprocessedAlready)
{
    char** aSplitted;
    if (isDataWasPreprocessedAlready)
    {
	aSplitted = string_split_length(pData, length, ' ');
    }
    else
    {
	aSplitted = smc_string_split_length(pData, length, ' ');
    }

    // DOCS: Preprocessing block
    char** aPreprocessedWords = NULL;
    i64 cnt = array_count(aSplitted);
    for (i64 i = 0; i < cnt; ++i)
    {
	char* pWord = aSplitted[i];
	i64 wordLength = string_length(pWord);

	char* preprocessedWord = simple_string_copy(pWord, wordLength, wordLength);

	// DOCS: Check if string already exists in an array
	i64 wordInd = array_index_of(
	    aPreprocessedWords,
	    smc_string_compare(item, preprocessedWord, simple_string_length(item), simple_string_length(preprocessedWord)));

	if (wordInd != -1)
	{
	    // note: interning works
	    char* pExistingWord = aPreprocessedWords[wordInd];
	    array_push(aPreprocessedWords, pExistingWord);

	    simple_string_destroy(preprocessedWord);
	}
	else
	{
	    array_push(aPreprocessedWords, preprocessedWord);
	}
    }

    if (isDataWasPreprocessedAlready)
    {
	array_foreach(aSplitted, memory_free(item));
    }
    else
    {
	array_foreach(aSplitted, simple_string_destroy(item));
    }

    array_free(aSplitted);

    return aPreprocessedWords;
}

#if 0

typedef struct SmcWord
{
    char* pWord;
    i64 Counter;
} SmcWord;

typedef struct SmcCounterItem
{
    const char* Key;
    SmcWord* Value;
} SmcCounterItem;

#endif

SmcCounterItem*
smc_build_counter_table(char** aWords)
{
    SmcCounterItem* hWordCounter = NULL;

    i32 count = array_count(aWords) - 1;
    for (i64 i = 0; i < count; ++i)
    {
	char* pWord = aWords[i];
	if (smc_helper_is_end_word_char(*pWord))
	{
	    continue;
	}

	char* pNextWord = aWords[i + 1];

	i64 ind = shash_geti(hWordCounter, pWord);
	if (ind == -1)
	{
	    // DOCS: Add new word, list<word>
	    SmcWord* aSmcWord = NULL;
	    SmcWord smcWord = {
		.pWord = pNextWord,
		.Counter = 1,
	    };
	    array_push(aSmcWord, smcWord);

	    shash_put(hWordCounter, pWord, aSmcWord);
	}
	else // if (0)
	{
	    // DOCS: Add to existing word, or increment

	    SmcWord* aSmcWord = hWordCounter[ind].Value;
	    i64 wordCount = array_count(aSmcWord);

	    i64 existingIndex = -1;

	    for (i64 j = 0; j < wordCount; ++j)
	    {
		SmcWord* pSmcWord = &aSmcWord[j];
		if (pSmcWord->pWord == pNextWord)
		{
		    ++pSmcWord->Counter;
		    existingIndex = j;
		    break;
		}
	    }

	    if (existingIndex == -1)
	    {
		SmcWord newWord = {
		    .pWord = pNextWord,
		    .Counter = 1,
		};
		array_push(aSmcWord, newWord);

		hWordCounter[ind].Value = aSmcWord;
	    }

	}
    }

    return hWordCounter;
}

void
smc_run()
{
    setlocale(LC_ALL, ".UTF8");
    system_info_print();

    i64 length;
    char* pData = file_read_string_ext("/home/bies/MidData/Assets/Text/SmcTrainingData/Td0.txt", &length);
    string_replace_character(pData, '\n', ' ');

#if 0
    char** aWords = smc_string_split_length((char*)pData, length, ' ');
    array_foreach(aWords, GINFO("word: %s\n", item));

#else
    char** aWords = smc_words_read((char*)pData, length, 0);
    array_foreach(aWords, GINFO("word: %s\n", item));


    GINFO("aWords: %p %d\n", aWords, aWords[2] == aWords[3]);



    SmcCounterItem* hWordCounter = smc_build_counter_table(aWords);

    for (i64 i = 0; i < table_capacity(hWordCounter); ++i)
    {
	SmcCounterItem wordItem = hWordCounter[i];
	if (wordItem.Key == NULL)
	    continue;

	char* pWord = (char*) wordItem.Key;

	SmcWord* aWords = shash_get(hWordCounter, pWord);

	printf("Word: %s\n", pWord);
	array_foreach(aWords, printf(" - %s - %lld\n", item.pWord, item.Counter));
    }
#endif

}

// DOCS: Cosine similarity / коэффициент Отиаи
// косинус угла между векторами
real
sn_cosine_similarity(real* pV0, real* pV1, i64 vectorLength)
{
    real dot = 0.0;
    real normV0 = 0.0;
    real normV1 = 0.0;

    for (i64 i = 0; i < vectorLength; ++i)
    {
	real v0 = *pV0;
	real v1 = *pV1;

	dot += (v0 * v1);
	normV0 += (v0 * v0);
	normV1 += (v1 * v1);

	++pV0;
	++pV1;
    }

    real result = dot / (real_sqrt(normV0) * real_sqrt(normV1));
    return result;
}

/*

  DOCS: Classification

*/

#include <Math/SimpleMath.h>


void
sn_softmax_array(real* pVector, i64 vectorLength, real* pDestination)
{
    if (pVector == NULL || pDestination == NULL || vectorLength == 0)
	return;

    real* pVectWrite = pVector;
    real* pDestWrite = pDestination;
    real sum = 0.0;

    for (i64 i = 0; i < vectorLength; ++i)
    {
	real veci = *pVectWrite;
	real expPow = pow(Math_E, veci);

	*pDestWrite = expPow;
	sum += expPow;

	++pDestWrite;
	++pVectWrite;
    }

    pDestWrite = pDestination;

    for (i64 i = 0; i < vectorLength; ++i)
    {
	real expPow = *pDestWrite;
	*pDestWrite = expPow / sum;
	++pDestWrite;
    }
}

i32
main()
{
    real v0[] = {-0.4, 0.8};
    real v1[] = {-0.3, 0.2};
    real v2[] = {-0.5, -0.4};

    real s0 = sn_cosine_similarity(v0, v1, 2);
    real s1 = sn_cosine_similarity(v0, v2, 2);

    GINFO("Similarity %d %f\n", 0, s0);
    GINFO("Similarity %d %f\n", 1, s1);

    real aVec[4] = {4.01, -50, 0, 1};
    sn_softmax_array(aVec, 4, aVec);

    for (i64 i = 0; i < 4; ++i)
    {
	GINFO("aVec[%d] = %f\n", i, aVec[i]);
    }

    return 0;
}
