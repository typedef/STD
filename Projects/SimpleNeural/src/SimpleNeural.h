#ifndef SIMPLE_NEURAL_H
#define SIMPLE_NEURAL_H


struct SwlEvent;

void simple_neural_on_attach();
void simple_neural_on_destroy();
void simple_neural_on_update();
void simple_neural_on_ui();
void simple_neural_on_event(struct SwlEvent* pEvent);

#endif // SIMPLE_NEURAL_H
