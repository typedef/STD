#include "SimpleNeural.h"

#include <Core/Types.h>
#include <Core/SimpleWindowLibrary.h>
#include <Core/SimpleStandardLibrary.h>


void
simple_neural_on_attach()
{
}

void
simple_neural_on_destroy()
{
}

void
simple_neural_on_update()
{

}

void
simple_neural_on_ui()
{

}

void
simple_neural_on_event(SwlEvent* pEvent)
{
}
