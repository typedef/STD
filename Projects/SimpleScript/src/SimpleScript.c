#include <stdio.h>
#include <stdlib.h>
#include <string.h>


/*
  simple-script:
  1. generate .sh/.bat scripts from premake5.lua
  2. generate .sh/.bat scripts from code

 */

/*
  ############################################
  DOCS: Configurations
  ############################################
*/
//#define ENGINE_DEBUG
#if !defined(PLATFORM_LINUX) && !defined(PLATFORM_WINDOWS)
#define PLATFORM_LINUX
#endif

#if defined(PLATFORM_LINUX)
#include <unistd.h>
#endif


#ifndef TYPES_H
#define TYPES_H

/*
  ############################################
  DOCS: Declare our own types as preprocessors
  ############################################
*/

typedef char i8;
typedef short i16;
typedef int i32;
typedef long long int i64;
typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned long long u64;
typedef float f32;
typedef double f64;


// todo: validate sizeof(i8) == 1, etc
/*
  ############################################
  DOCS: Convert values
  ############################################
*/

#define to_i8(x)  ((i8)(x))
#define to_i16(x) ((i16)(x))
#define to_i32(x) ((i32)(x))
#define to_i64(x) ((i64)(x))
#define to_u8(x)  ((u8)(x))
#define to_u16(x) ((u16)(x))
#define to_u32(x) ((u32)(x))
#define to_u64(x) ((u64)(x))
#define to_f32(x) ((f32)(x))
#define to_f64(x) ((f64)(x))

#define I8_MAX 127
#define I16_MAX 32767
#define I32_MAX 2147483647
#define I64_MAX (9223372036854775807LL) /*9 223 372 036 854 775 807=9e+18*/
#define U8_MAX  255
#define U16_MAX 65535
#define U32_MAX 4294967295
#define U64_MAX (18446744073709551615ULL) /*18 446 744 073 709 551 615=1.8*e+19*/
#define F32_MAX (340282346638528859811704183484516925440.000000)
#define F64_MAX (340282346638528859811704183484516925440.000000)

#define I32_MAX_HALF 1123241323

/*
  ############################################
  DOCS: Platform definition
  ############################################
*/

#define DEBUG_WO_FORCE_INLINE 0
#if DEBUG_WO_FORCE_INLINE == 1
#warning "Slow mode enabled!\n #define DEBUG_WO_FORCE_INLINE 1"
#endif

#if defined(PLATFORM_WINDOWS)
#define force_inline static
#elif defined(PLATFORM_LINUX)
#define force_inline static inline __attribute((always_inline))
#endif

/*
  ############################################
  DOCS: Float precision, by default 32 bit
  #define FLOAT_64_BIT for better accuracy
  ############################################
*/

// todo: make 64 bit work
//#define FLOAT_64_BIT

#if !defined(FLOAT_64_BIT)
#define FLOAT_32_BIT
#endif

/*
  ############################################
  DOCS: Build configuration
  ############################################
*/

// todo: use BUILD_DEBUG instead of ENGINE_DEBUG in lib code
#if defined(ENGINE_DEBUG)
#define BUILD_DEBUG
#elif defined(ENGINE_RELEASE)
#define BUILD_RELEASE
#endif

/*
  ############################################
  DOCS: System byte order
  ############################################
*/

#if !defined(SYSTEM_BIG) && !defined(SYSTEM_LIT)
/*🐨
  1 in endian system:
  16                0
  BigEndian   : 1000 0000 0000 0000
  0                16
  LittleEndian: 0000 0000 0000 0001

  general order: 123 => little: 0000 0123
  big   : 0123 0000
*/

#define SYSTEM_LIT (*((u8*) &(u32){1}))
#define SYSTEM_BIG (!(SYSTEM_LIT))

#endif




/*
  #############################################
  DOCS: Helper methods abs, elvis, swap, minmax
  #############################################
*/

#define i32_abs(x) ({ i32 temp = (x); temp < 0 ? -temp : temp; })
#define f32_abs(x)				\
    ({						\
        union {f32 a; u32 b; } r;		\
        r.a = x;				\
        r.b &= 0x7fffffff;			\
        r.a;					\
    })

#define IfFalseThen(x, t)                               \
    ({ __typeof__((x)) xv = (x); (xv) ? xv : (t); })
#define IfNullThen(x, t)                                        \
    ({ __typeof__((x)) xv = (x); (xv != NULL) ? xv : (t); })
#define IsSingEqual(x, y)			\
    ({	((x * y) >= 0) ? 1 : 0;	})

#define Swap(x, y)                                      \
    ({ __typeof__(x) temp = x; x = y; y = temp; })
#define Max(x,y)                                                        \
    ({ __typeof__(x) l = x; __typeof__(y) r = y; (l > r) ? l : r; })
#define Min(x,y)                                                        \
    ({ __typeof__(x) l = x; __typeof__(y) r = y; (l > r) ? r : l; })
#define MinMax(x, min, max)                                             \
    ({ __typeof__(x) l = x; __typeof__(min) mn = min; __typeof__(max) mx = max; ((l > mx) ? mx : (l < mn ? mn : l)); })
#define MinMaxV2(x, v2v) MinMax(x, v2v.Min, v2v.Max)

#define Align(byte) __attribute__((aligned(byte)))
#define KB(x) ((i64)1024 * (i64)x)
#define MB(x) ((i64)1024 * KB(x))
#define GB(x) ((i64)1024 * MB(x))
#define ABS(x)					\
    ({ ((x) > 0) ? (x) : -(x); })
#define ToKB(x) (((f64) x) / 1024)
#define ToMB(x) (((f64) TOKB(x)) / 1024)
#define ToGB(x) (((f64) TOMB(x)) / 1024)
#define ToString(x) #x
#define OffsetOf(Type, Field) ( (u64) (&(((Type*)0)->Field)) )
#define AlignOf(Type) ((u64)&(((struct{char c; Type i;}*)0)->i))
#define ArrayCount(x) (sizeof(x) / sizeof(x[0]))


/*
  ############################################
  DOCS: Structure types
  ############################################
*/

typedef struct SimpleString
{
    i32 Length;
    i32 Size;
    char* pData;
} SimpleString;

typedef enum ResultType
{
    ResultType_Error = 0,
    ResultType_Success = 1,
    ResultType_Warning = 2
} ResultType;

typedef struct SimpleColor //((r << 24) | (g << 16) | (b << 8) | (a))
{
    u8 R;
    u8 G;
    u8 B;
    u8 A;
} SimpleColor;

#define SimpleColor_R(r) ((u32) (((SimpleColor*) (r))->R))
#define SimpleColor_G(g) ((u32) (((SimpleColor*) (g))->G))
#define SimpleColor_B(b) ((u32) (((SimpleColor*) (b))->B))
#define SimpleColor_A(a) ((u32) (((SimpleColor*) (a))->A))
#define SimpleColor_RGBA(r, g, b, a) ((a << 24) | (b << 16) | (g << 8) | (r))

#if defined(FLOAT_32_BIT)
typedef f32 real;
#else
typedef f64 real;
#endif

typedef real v3a[3];
typedef real v4a[4];
typedef v3a m3a[3];
typedef v4a m4a[4];

typedef struct v2
{
    union
    {
        struct { real X; real Y; };
        struct { real Width; real Height; };
        struct { real Min; real Max; };
        real V[2];
    };
} v2;

typedef struct v2i
{
    union
    {
        struct { i32 X; i32 Y; };
        struct { i32 Width; i32 Height; };
        struct { i32 Min; i32 Max; };
        i32 V[2];
    };
} v2i;

typedef struct v3
{
    union
    {
        struct { real X; real Y; real Z; };
        struct { real R; real G; real B; };
        struct { real Hue; real Saturation; real Value; };
        struct { real Pitch; real Yaw; real Roll; };
        struct { v2 XY; real T3; };
        struct { real T0; v2 YZ; };
        real V[3];
    };
} v3;

typedef struct v3i
{
    union
    {
        struct { i32 X; i32 Y; i32 Z; };
        i32 V[3];
    };
} v3i;

typedef struct v4
{
    union
    {
        struct { real X; real Y; real Z; real W; };
        struct { real R; real G; real B; real A; };
        real V[4];
    };
} v4;

typedef struct v4i
{
    union
    {
        struct { i32 X; i32 Y; i32 Z; i32 W; };
        struct { i32 R; i32 G; i32 B; i32 A; };
        i32 V[4];
    };
} v4i;

typedef struct m3
{
    union
    {
        struct
        {
            real M00; real M01; real M02;
            real M10; real M11; real M12;
            real M20; real M21; real M22;
        };
        struct
        {
            real R0[3];
            real R1[3];
            real R2[3];
        };
        m3a M;
    };
} m3;

typedef struct m4
{
    union
    {
        struct
        {
            real M00; real M01; real M02; real M03;
            real M10; real M11; real M12; real M13;
            real M20; real M21; real M22; real M23;
            real M30; real M31; real M32; real M33;
        };
        struct
        {
            real R0[4];
            real R1[4];
            real R2[4];
            real R3[4];
        };
        v4 V[4];
        m4a M;
    };
} m4;

typedef struct quat
{
    union
    {
        struct { real X; real Y; real Z; real W; };
        v4 V4;
        real V[4];
    };
} quat;

typedef struct Aabb
{
    v3 Min;
    v3 Max;
} Aabb;

typedef struct SimpleArena
{
    i64 Id;
    u64 Offset;
    u64 Size;
    i32 Line;
    const char* File;
    void* Data;
} SimpleArena;

typedef struct IString
{
    i32 Length;
    char* Buffer;
} IString;

typedef struct IStringPool
{
    IString** Strings;
} IStringPool;

/*
  ############################################
  DOCS: Conditional break, assert, guard
  ############################################
*/

static void
_vbreak(const char* msg, const char* file, int line, const char* condition)
{
    printf("[ASSERT] %s:%d condition: %s is false!\n", file, line, condition);
    volatile int* cptr = NULL;
    *cptr = 0;
}


#if defined(BUILD_DEBUG)
#define vassert(a)							\
    ({									\
        if (!(a))							\
        {								\
            _vbreak("[ASSERT] %s:%d condition: %s is false!\n", __FILE__, __LINE__, #a); \
        }								\
    })

#define vassert_not_null(ptr) vassert(ptr != NULL)
#define vassert_null(ptr) vassert((ptr == NULL))
#define vassert_break() vassert(0 && "We shouldn't get here!!!")
#else // BUILD_DEBUG
#define vassert(a) ((void)0)
#define vassert_not_null(ptr) ((void)0)
#define vassert_null(ptr) ((void)0)
#define vassert_break() ((void)0)
#define vassert_null_offset(ptr) ((void)0)
#endif // BUILD_RELEASE

/*
  DOCS(typedef): Always exist, independent of the build config
*/
#define vguard(ptr)							\
    ({									\
        if (!(ptr))							\
        {								\
            _vbreak("[GUARD] %s:%d condition: %s is false!\n", __FILE__, __LINE__, #ptr); \
        }								\
    })
#define vguard_not_null(ptr)			\
    ({						\
        vguard(ptr != NULL && #ptr);		\
    })
#define vguard_null(ptr)			\
    ({						\
        vguard(ptr == NULL);			\
    })
#define vguard_legacy(func)				\
    ({							\
        vguard(0 && #func " - legacy function!");	\
    })
#define vguard_not_impl()			\
    ({						\
        vguard(0 && "not implementsd!");	\
    })

/*
  #############################################
  DOCS: Asset/Resource path
  #############################################
*/

// note: not sure we will need this
#define BASE_ASSET_PATH "assets"
#define asset(path) BASE_ASSET_PATH path
#define asset_shader(shader) asset("/shaders/") shader
#define asset_texture(texture) asset("/textures/") texture
#define asset_cubemap(cubemap) asset("/cubemaps/") cubemap
#define asset_font(font) asset("/fonts/") font
#define asset_model(model) asset("/models/") model
#define asset_sound(sound) asset("/sounds/") sound
#define asset_scene(scene) asset("/scenes/") scene
#define asset_db(db) asset("/databases/") db
#define asset_localization(local) asset("/localization/") local

#define BASE_RESOURCE_PATH "Resources"
#define resource(path) BASE_RESOURCE_PATH path
#define resource_shader(path) resource("/Shaders/") path
#define resource_font(path) resource("/Fonts/") path
#define resource_texture(path) resource("/Textures/") path
#define resource_model(path) resource("/Models/") path


/*
  #############################################
  DOCS: Dont now what to do with this
  #############################################
*/

#define void_to_i32(ptr) ((ptr) ? (*((i32*)ptr)) : ({vassert(0 && "Wrong argument void_to_i32!"); 0; }))
#define void_to_f32(ptr) (*((f32*)ptr))
#define void_to_string(ptr) ((const char*)ptr)
#define i32_to_void_ptr(i32v) ((void*)(&(i32v)))
#ifndef NULL
#define NULL ((void*)0)
#endif
#define EMPTY(type) (type){0}
#define I32T(t, h) t##h
#define I32M(m, t, h) m##t##h
#define I32B(b, m, t, h) b##m##t##h


#endif // Types.h

#ifndef IO_FILE_H
#define IO_FILE_H

char*
io_file_read_string_ext(const char* filePath, u64* length)
{
    FILE* file;
    char* result;
    i32 fileLength;

    file = fopen(filePath, "r");
    if (file)
    {
        fseek(file, 0, SEEK_END);
        fileLength = (ftell(file));
        fseek(file, 0, SEEK_SET);
        result = malloc((fileLength + 1) * sizeof(char));

        fread(result, sizeof(char), (fileLength), file);
        result[fileLength] = '\0';

        fclose(file);

        *length = fileLength;
        return((char*)result);
    }

    *length = 0;
    return NULL;
}

#endif // IO_FILE_H

#ifndef SISC_ARRAY_H
#define SISC_ARRAY_H

typedef struct SiScArrayHeader
{
    i32 ElementSize;
    i64 Count;
    i64 Capacity;
    void* Buffer;
} SiScArrayHeader;

static i32 StartSize = 1;

#define sisc_array_header(b) ((SiScArrayHeader*) (((char*)b) - sizeof(SiScArrayHeader)))
#define sisc_array_count(b) ((b != NULL) ? sisc_array_header(b)->Count : 0)
#define sisc_array_cap(b) ((b != NULL) ? sisc_array_header(b)->Capacity : 0)

#define sisc_array_push(b, ...)                                         \
    {                                                                   \
        if ((b) == NULL || sisc_array_count(b) >= sisc_array_cap(b))    \
        {                                                               \
            (b) = sisc_array_grow((const void*)b, sizeof(__typeof__(b[0]))); \
        }                                                               \
                                                                        \
        b[sisc_array_count(b)] = (__VA_ARGS__);                         \
        ++sisc_array_header(b)->Count;                                  \
    }

#define sisc_array_pop(b)                                               \
    ({                                                                  \
        vassert_not_null((b));                                          \
                                                                        \
        SiScArrayHeader* pHdr = sisc_array_header(b);                   \
        i64 count = pHdr->Count;                                        \
                                                                        \
        vassert (count > 0 && "Array is empty!");                       \
                                                                        \
        __typeof__((b)[0]) item = (b)[count - 1];                       \
        memset(((void*)b) + (pHdr->Count - 1) * pHdr->ElementSize, 0, pHdr->ElementSize); \
        --pHdr->Count;                                                  \
                                                                        \
        item;                                                           \
    })

static void*
sisc_array_grow(const void* array, i32 elementSize)
{
    if (array == NULL)
    {
        i32 elementsCount = 1;
        SiScArrayHeader* newHeader = (SiScArrayHeader*) malloc(elementsCount * elementSize + sizeof(SiScArrayHeader));
        newHeader->Buffer = (void*) (((u8*)newHeader) + sizeof(SiScArrayHeader));
        newHeader->Count = 0;
        newHeader->Capacity = elementsCount;
        newHeader->ElementSize = elementSize;

        return newHeader->Buffer;
    }

    u64 newCapacity = 2 * sisc_array_cap(array) + 1;
    u64 newSize = newCapacity * elementSize + sizeof(SiScArrayHeader);
    SiScArrayHeader* header = sisc_array_header(array);

    SiScArrayHeader* newHeader = NULL;
    newHeader = (SiScArrayHeader*) malloc(newSize);
    newHeader->Buffer = ((char*)newHeader) + sizeof(SiScArrayHeader);
    newHeader->ElementSize = elementSize;
    newHeader->Count = header->Count;
    newHeader->Capacity = newCapacity;

    u64 copySize = header->Count * elementSize;
    memcpy(newHeader->Buffer, array, copySize);
    vassert(copySize < newSize && "CopySize >= NewSize!!!!!!");

    free(header);

    return newHeader->Buffer;
}

#endif // SISC_ARRAY_H

#ifndef SIMPLE_STRING_H
#define SIMPLE_STRING_H

i64
string_length(const char* str)
{
    vassert_not_null(str);
    char* ptr;
    for (ptr = (char*) str; *ptr != '\0'; ++ptr);
    return (i64) ((u64)(ptr - str));
}

i32
string_compare_length(const char* left, const char* right, i64 length)
{
    vassert_not_null(left);
    vassert_not_null(right);

    i64 i = 0;
    char* ptrl = (char*) left;
    char* ptrr = (char*) right;
    for ( ;i < length; )
    {
        char lc = *ptrl;
        char rc = *ptrr;
        if (lc != rc)
        {
            return 0;
        }

        ++i;
        ++ptrl;
        ++ptrr;
    }

    return 1;
}

i32
simple_string_index_of_string(SimpleString input, SimpleString string)
{
    i32 inputLength, stringLength;

    inputLength = input.Length;
    stringLength = string.Length;

    if (inputLength <= 0 || stringLength <= 0)
    {
        return -1;
    }

    char fc = string.pData[0];

    for (i32 i = 0; i < inputLength; ++i)
    {
        char c = input.pData[i];

        i32 flag = 0;
        if (c == fc)
        {
            flag = 1;
            for (i32 j = 1; j < stringLength; ++j)
            {
                if (input.pData[i + j] != string.pData[j])
                {
                    flag = -1;
                    break;
                }
            }
        }

        if (flag == 1)
            return i;
    }

    return -1;
}

i32
simple_string_equals(SimpleString s0, SimpleString s1)
{
    if (s0.Size != s1.Size)
        return 0;

    if (s0.pData == NULL || s1.pData == NULL || s0.Size == 0 || s1.Size == 0)
        return 0;

    if (memcmp(s0.pData, s1.pData, s0.Size) == 0)
        return 1;

    return 0;
}

SimpleString
simple_string_add_string(SimpleString input, char* pStr, i32 len)
{
    i32 newLen = input.Size + len;
    char* pAllocated = (char*) malloc(newLen + 1);
    if (input.pData)
        memcpy(pAllocated, input.pData, input.Size);
    memcpy(pAllocated + input.Size, pStr, len);
    pAllocated[newLen] = '\0';

    if (input.pData)
    {
        free(input.pData);
    }

    len = input.Size + len;
    SimpleString str = {
        .Length = len,
        .Size = len,
        .pData = pAllocated
    };

    return str;
}

i32
string_index_of_string(const char* input, const char* string)
{
    vassert(input && "input is NULL or undefined!");
    vassert(string && "string is NULL or undefined!");

    i32 i, j, flag, inputLength, stringLength;

    vassert(string != NULL);

    inputLength = string_length(input);
    stringLength = string_length(string);

    if (inputLength <= 0 || stringLength <= 0)
    {
        return -1;
    }

    flag = -1;
    for (i = 0; i < inputLength; ++i)
    {
        for (j = 0; j < stringLength; ++j)
        {
            if (input[i + j] == string[j])
            {
                flag = 1;
            }
            else
            {
                flag = -1;
                break;
            }
        }

        if (flag == 1)
        {
            return i;
        }
    }

    return -1;
}

SimpleString
simple_string(SimpleString str)
{
    i64 len = str.Length;
    u64 size = str.Size;

    char* pData = (char*) malloc(size + 1);
    memcpy(pData, str.pData, size);
    pData[size] = '\0';

    return (SimpleString) {
        .Length = str.Length,
        .Size = str.Size,
        .pData = pData,
    };
}

#endif // SIMPLE_STRING_H

#ifndef SIMPLE_PATH_H
#define SIMPLE_PATH_H

typedef enum Path
{
    PATH_IS_SOMETHING = 0,
    PATH_IS_FILE,
    PATH_IS_DIRECTORY
} Path;

#if defined(PLATFORM_LINUX)

#include <sys/dir.h>
#include <sys/types.h>
#include <sys/stat.h>

extern int scandir(const char *restrict dirp,
                   struct dirent ***restrict namelist,
                   int (*filter)(const struct dirent *),
                   int (*compar)(const struct dirent **,
                                 const struct dirent **));

u8
path(const char* path)
{
    struct stat fileInfo;

    if (stat(path, &fileInfo) != 0)
    {
        return PATH_IS_SOMETHING;
    }

    if (S_ISDIR(fileInfo.st_mode))
    {
        return PATH_IS_DIRECTORY;
    }
    else if (S_ISREG(fileInfo.st_mode))
    {
        return PATH_IS_FILE;
    }

    return PATH_IS_SOMETHING;
}

force_inline char
char_to_lower(char character)
{
    if (character >= 'A' && character <= 'Z')
        return (character - 'A' + 'a');
    else
        return character;
}

static i32
path_string_comparer(const struct dirent** a, const struct dirent** b)
{
    char* left = (char*)(*a)->d_name;
    char* right = (char*)(*b)->d_name;
    u32 leftLength = string_length(left);
    u32 rightLength = string_length(right);

    for (u32 i = 0; i < leftLength; i++)
    {
        char l = char_to_lower(left[i]);
        char r = char_to_lower(right[i]);

        if (l < r)
        {
            return 1;
        }
        else if (l > r)
        {
            return -1;
        }
    }

    return (rightLength - leftLength);
}

char*
string_new(const char* pStr, i32 len)
{
    char* pAllocated = malloc(len + 1);
    memcpy(pAllocated, pStr, len);
    pAllocated[len]='\0';
    return pAllocated;
}

char*
path_combine(const char* left, const char* right)
{
    vassert_not_null(left );
    vassert_not_null(right);

    /*
      1. Adding paths:
      * path_combine("Image/", "a.png");
      * path_combine("Image/", "/a.png");
      * path_combine("Image/", "./a.png");

      */

    i64 leftLength = string_length(left);
    i64 rightLength = string_length(right);

    char lc = left[leftLength-1];
    i32 isLeftSlashed = (lc == '/' || lc == '\\');

    i32 len = 0;
    char buf[2049] = {};
    if (isLeftSlashed)
    {
        len = snprintf(buf, 2049, "%s%s", left, right);
    }
    else
    {
        len = snprintf(buf, 2049, "%s/%s", left, right);
    }

    char* pAllocated = string_new(buf, len);
    return pAllocated;
}

i32
path_is_directory_exist(const char* pPath)
{
#if defined(PLATFORM_LINUX)
    struct stat buf;
    i32 result = stat(path, &buf);
    return result != -1;
#elif defined(PLATFORM_WINDOWS)
    DWORD result = GetFileAttributesA(path);
    return (result != INVALID_FILE_ATTRIBUTES && (result & FILE_ATTRIBUTE_DIRECTORY));
#endif
}

#if defined(PLATFORM_LINUX)

i32
platform_directory_create(const char* name)
{
    /*
      #define S_IRWXU 0000700    RWX mask for owner
      #define S_IRUSR 0000400    R for owner
      #define S_IWUSR 0000200    W for owner
      #define S_IXUSR 0000100    X for owner

      #define S_IRWXG 0000070    RWX mask for group
      #define S_IRGRP 0000040    R for group
      #define S_IWGRP 0000020    W for group
      #define S_IXGRP 0000010    X for group

      #define S_IRWXO 0000007    RWX mask for other
      #define S_IROTH 0000004    R for other
      #define S_IWOTH 0000002    W for other
      #define S_IXOTH 0000001    X for other

      #define S_ISUID 0004000    set user id on execution
      #define S_ISGID 0002000    set group id on execution
      #define S_ISVTX 0001000    save swapped text even after use
    */
    i32 result = mkdir(name, S_IRWXU);
    if (result != 0)
    {

        return 0;
    }

    return 1;
}

#elif defined(PLATFORM_WINDOWS)

i32
platform_directory_create(const char* name)
{
    if (!CreateDirectoryA(name, NULL))
        return 0;
    return 1;
}

#endif

i32
path_directory_create(const char* path)
{
    return platform_directory_create(path);
}

force_inline const char**
_directory_get(const char* directory, i32 elemCode)
{
    const char** elements = NULL;
    struct dirent** namelist = NULL;
    i32 n = scandir(directory, &namelist, 0, path_string_comparer);

    while (n > 0)
    {
        const char* dName = namelist[n - 1]->d_name;

        if (dName[0] == '.')
        {
            --n;
            continue;
        }

        char* absolutePath = path_combine(directory, dName);
        if (path(absolutePath) == elemCode)
        {
            sisc_array_push(elements, absolutePath);
        }

        free(namelist[n - 1]);

        --n;
    }

    free(namelist);

    return elements;
}

const char**
path_directory_get_files(const char* directory)
{
    const char** files = _directory_get(directory, PATH_IS_FILE);
    return files;
}

const char**
path_directory_get_directories(const char* directory)
{
    const char** dirs = _directory_get(directory, PATH_IS_DIRECTORY);
    return dirs;
}

#elif defined(PLATFORM_WINDOWS)

u8
path(const char* path)
{
    DWORD fileAttribute = GetFileAttributes(path);
    if (fileAttribute == INVALID_FILE_ATTRIBUTES)
    {
        GERROR("GetFileAttributes failed!\n");
        vassert_break();
    }

    if (fileAttribute == FILE_ATTRIBUTE_DIRECTORY)
    {
        return PATH_IS_DIRECTORY;
    }
    else
    {
        return PATH_IS_FILE;
    }
}

static char**
_directory_get(const char* directory, i32 elemCode)
{
    char** elements = NULL;
    WIN32_FIND_DATA findData;
    HANDLE firstFile = FindFirstFile(directory, &findData);
    if (firstFile == INVALID_HANDLE_VALUE)
    {
        return NULL;
    }

    do
    {
        if (elemCode == 0)
        {
            if (!(findData.dwFileAttributes & elemCode))
            {
                sisc_array_push(elements, findData.cFileName);
            }
        }
        else
        {
            if (findData.dwFileAttributes & elemCode)
            {
                sisc_array_push(elements, findData.cFileName);
            }
        }
    } while (FindNextFile(firstFile, &findData) != 0);

    return elements;
}

const char**
path_directory_get_files(const char* directory)
{
    const char** files = _directory_get(directory, 0);
    return files;
}

const char**
path_directory_get_directories(const char* directory)
{
    const char** dirs = _directory_get(directory, FILE_ATTRIBUTE_DIRECTORY);
    return dirs;
}
#endif // PLATFORM_WINDOWS

const char*
path_get_ext(const char* pPath, i32 len)
{
    for (i32 i = (len - 1); i >= 0; --i)
    {
        if (pPath[i] == '.')
        {
            return &pPath[i];
        }
    }

    return NULL;
}

SimpleString*
io_get_all_files_by_mask(char* directory, char* ext, i32 extLen)
{
    SimpleString* aFileStrings = NULL;

    char** aDirs = NULL;

    sisc_array_push(aDirs, directory);

    do
    {
        char* dir = sisc_array_pop(aDirs);
        const char** aFiles = path_directory_get_files(dir);
        for (i32 i = 0; i < sisc_array_count(aFiles); ++i)
        {
            const char* pFile = aFiles[i];

            i32 len = string_length(pFile);
            const char* pExt = path_get_ext(pFile, len);
            if (!pExt || (memcmp(pExt, ext, extLen) != 0))
                continue;

            SimpleString file = { .Length = len, .pData = (char*)pFile };
            sisc_array_push(aFileStrings, file);
        }

        const char** aTemp = path_directory_get_directories(dir);
        for (i32 d = 0; d < sisc_array_count(aTemp); ++d)
        {
            char* temp = (char*)aTemp[d];
            sisc_array_push(aDirs, temp);
        }

    } while (sisc_array_count(aDirs) > 0);

    return aFileStrings;
}

char*
path_get_name_wo_ext(const char* pPath)
{
    i32 len = string_length(pPath);

    i32 dotInd = -1, slashInd = -1;
    for (i32 i = (len - 1); i >= 0; --i)
    {
        char c = pPath[i];
        if (c == '.')
        {
            dotInd = i;
        }
        else if (c == '/' || c == '\\')
        {
            slashInd = i;
        }

        if (dotInd != -1 && slashInd != -1)
            break;
    }

    if (dotInd == -1 || slashInd == -1)
        return NULL;

    i32 nameLen = dotInd - slashInd - 1;
    if (nameLen <= 0)
        return NULL;

    char* pAllocated = malloc(nameLen + 1);
    memcpy(pAllocated, pPath + slashInd + 1, nameLen);
    pAllocated[nameLen] = '\0';

    return pAllocated;
}


const char*
path_get_current_directory()
{
    static char CurrentDirectory[2048] = {};

#if defined(PLATFORM_LINUX)
    if (CurrentDirectory[0] == '\0')
        getcwd(CurrentDirectory, 4096);
#elif defined(PLATFORM_WINDOWS)
    if (CurrentDirectory[0] == '\0')
        GetModuleFileName(NULL, CurrentDirectory, MAX_PATH);
#endif

    return (const char*)CurrentDirectory;
}

char*
path_get_directory(const char* pPath)
{
    i32 ind = -1;

    i32 len = string_length(pPath);
    for (i32 i = (len - 1); i >= 0; --i)
    {
        char c = pPath[i];
        if (c == '/')
        {
            ind = i;
            break;
        }
    }

    if (ind == -1 || (ind == (len - 1)))
        return NULL;

    i32 dirLen = ind;
    char* pAllocated = (char*) malloc(dirLen);
    memcpy(pAllocated, pPath, dirLen);

    return pAllocated;
}

#endif // SIMPLE_PATH_H

typedef enum SiScResultType
{
    SiScResultType_Error = 0,
    SiScResultType_Success
} SiScResultType;

const char*
sisc_result_type_to_string(SiScResultType type)
{
    switch (type)
    {
    case SiScResultType_Error: return "Error";
    case SiScResultType_Success: return "Success";
    }
}

typedef enum SiScConfiguration
{
    SiScConfiguration_Debug = 0,
    SiScConfiguration_Release,
    SiScConfiguration_Dist,
} SiScConfiguration;

const char*
sisc_configuration_to_string(SiScConfiguration conf)
{
    switch (conf)
    {
    case SiScConfiguration_Debug: return "Debug";
    case SiScConfiguration_Release: return "Release";
    case SiScConfiguration_Dist: return "Dist";
    }
}

typedef enum SiScPlatform
{
    SiScPlatform_Linux = 0,
    SiScPlatform_Windows,
} SiScPlatform;

const char*
sisc_platform_to_string(SiScPlatform platform)
{
    switch (platform)
    {
    case SiScPlatform_Linux: return "linux";
    case SiScPlatform_Windows: return "windows";
    }

    return NULL;
}

typedef enum SiScArchitecture
{
    SiScArchitecture_x86_64 = 0,
} SiScArchitecture;

const char*
sisc_architecture_to_string(SiScArchitecture arch)
{
    switch (arch)
    {
    case SiScArchitecture_x86_64: return "x86_64";
    }

    return NULL;
}

typedef struct SiScBuildConfig
{
    SiScConfiguration Configuration;
    SiScPlatform Platform;
    SiScArchitecture Architecture;
} SiScBuildConfig;

typedef struct SiScProjectSettings
{
    SimpleString Input;
    SimpleString Directory;
    SimpleString CurrentDirectory;
    SiScBuildConfig BuildConfig;
} SiScProjectSettings;

typedef enum SiScProjectType
{
    SiScProjectType_ConsoleApp = 0,
    SiScProjectType_StaticLib,
    SiScProjectType_DynamicLib
} SiScProjectType;

const char*
sisc_project_type_to_string(SiScProjectType type)
{
    switch (type)
    {
    case SiScProjectType_ConsoleApp: return "ConsoleApp";
    case SiScProjectType_StaticLib: return "StaticLib";
    case SiScProjectType_DynamicLib: return "DynamicLib";
    }

    return NULL;
}

typedef enum SiScLanguage
{
    SiScLanguage_C = 0,
    SiScLanguage_Cpp,
} SiScLanguage;

const char*
sisc_project_language_to_string(SiScLanguage language)
{
    switch (language)
    {
    case SiScLanguage_C  : return "C";
    case SiScLanguage_Cpp: return "Cpp";
    }

    return NULL;
}

typedef enum SiScLanguageVersion
{
    SiScLanguageVersion_C99 = 0,
    SiScLanguageVersion_C89,
    SiScLanguageVersion_C11,
    SiScLanguageVersion_C17,
    SiScLanguageVersion_C23,

    SiScLanguageVersion_Cpp2003,
    SiScLanguageVersion_Cpp11,
    SiScLanguageVersion_Cpp14,
    SiScLanguageVersion_Cpp17,
    SiScLanguageVersion_Cpp23
} SiScLanguageVersion;

const char*
sisc_language_version_to_string(SiScLanguageVersion version)
{
    switch (version)
    {

    case SiScLanguageVersion_C99: return "C99";
    case SiScLanguageVersion_C89: return "C89";
    case SiScLanguageVersion_C11: return "C11";
    case SiScLanguageVersion_C17: return "C17";
    case SiScLanguageVersion_C23: return "C23";

    case SiScLanguageVersion_Cpp2003: return "C++2003";
    case SiScLanguageVersion_Cpp11: return "C++11";
    case SiScLanguageVersion_Cpp14: return "C++14";
    case SiScLanguageVersion_Cpp17: return "C++17";
    case SiScLanguageVersion_Cpp23: return "C++23";

    }

    return NULL;
}

typedef struct SiScParseSettings
{
    const char* pPath;
    SiScBuildConfig BuildConfig;
    SimpleString CurrentDirectory;
} SiScParseSettings;

typedef struct SiScFile
{
    i32 IsRecursion;
    // todo: or not todo: mb use SimpleString
    union {
        const char* pFullPath;
        const char* pDirectory;
    };
    const char* pExt;
} SiScFile;

typedef struct SiScProject SiScProject;

struct SiScProject
{
    SimpleString Name;
    SimpleString Location;
    SimpleString CurrentDirectory;
    SiScConfiguration* aConfigurations;
    // todo: remove it, because it's temp field
    SimpleString DirectoryName;
    SimpleString Target;
    SimpleString ObjDir;
    //SimpleString* aDeps;
    SiScProject* aDeps;
    SimpleString* aFiles;
    SimpleString* aIncludes;
    SiScLanguage Language;
    SiScLanguageVersion LanguageVersion;
    SiScProjectType Type;

};

typedef struct SiScParseResult
{
    SiScResultType ResultType;

    SimpleString WorkspaceName;
} SiScParseResult;

SimpleString
sisc_parse_string(char** pPtr)
{
    char* ptr = *pPtr;
    ++ptr;
    char* start = ptr;

    while (*ptr != '\"')
        ++ptr;

    u64 len = ptr - start;

    SimpleString str = {
        .Length = len,
        .Size = len,
        .pData = start
    };

    *pPtr = ptr;

    return str;
}

#define SkipToChar(ptri, ch)                    \
    ({                                          \
        i32 indi = 0;                           \
                                                \
        while (*ptri != ch)                     \
        {                                       \
            ++ptri;                             \
            ++indi;                             \
        }                                       \
                                                \
        indi;                                   \
    })

SimpleString*
parse_deps_group(char** pPtr, i32 groupDepsInd)
{
    SimpleString* aDeps = NULL;

    char* ptr = *pPtr;

    SkipToChar(ptr, '\n');

    i32 projInd = string_index_of_string(ptr, "project");
    i32 groupChunkLen = projInd;
    SimpleString groupChunk = {.Length = groupChunkLen, .Size = groupChunkLen, .pData = ptr};
    SimpleString includeStr = {.Length = 9, .Size = 9, .pData = "include \""};

    i64 ind = 0;
    char c = *ptr;
    while (c != '\0' || ind > groupChunkLen)
    {
        if (string_compare_length(ptr, "project", 7))
        {
            break;
        }

        if (string_compare_length(ptr, "include", 7))
        {
            ptr += 7;
            ind += 7;

            ind = SkipToChar(ptr, '\"');

            // todo: here
            SimpleString deps = sisc_parse_string(&ptr);
            ind += deps.Length;

            SimpleString allocated = simple_string(deps);

            sisc_array_push(aDeps, allocated);
        }

        ++ind;
        ++ptr;
        c = *ptr;
    }

    *pPtr = ptr;
    return aDeps;
}

SiScProject
sisc_parse_project(SiScProjectSettings projectSet)
{
    SiScProject siscProject = {
        .CurrentDirectory = projectSet.CurrentDirectory
            };

    SimpleString input = projectSet.Input;

    SimpleString conf = { .Length = 14, .pData = "configurations" };
    i32 confInd = simple_string_index_of_string(input, conf);
    if (confInd != -1)
    {
        SiScConfiguration* aConfigurations = NULL;

        char* ptr = input.pData + confInd + 1;

        SimpleString debug = {.Length=5, .Size=5, .pData="Debug"};
        SimpleString release = {.Length=7, .Size=7, .pData="Release"};
        SimpleString dist = {.Length=4, .Size=4, .pData="Dist"};

        char c = *ptr;
        while (c != '}')
        {
            if (c == '\"')
            {
                SimpleString config = sisc_parse_string(&ptr);
                if (simple_string_equals(config, debug))
                {
                    sisc_array_push(aConfigurations, SiScConfiguration_Debug);
                }
                else if (simple_string_equals(config, release))
                {
                    sisc_array_push(aConfigurations, SiScConfiguration_Release);
                }
                else if (simple_string_equals(config, dist))
                {
                    sisc_array_push(aConfigurations, SiScConfiguration_Dist);
                }

            }

            ++ptr;
            c = *ptr;
        }

        siscProject.aConfigurations = aConfigurations;
    }

    SimpleString language = { .Length = 9, .pData = "language" };
    i32 languageInd = simple_string_index_of_string(input, language);
    if (languageInd != -1)
    {
        char* ptr = input.pData + languageInd + 1;

        SimpleString languageStr = sisc_parse_string(&ptr);
        if (string_compare_length(languageStr.pData, "C", 1))
        {
            siscProject.Language = SiScLanguage_C;
        }
        else if (string_compare_length(languageStr.pData, "Cpp", 3)
                 || string_compare_length(languageStr.pData, "C++", 3))
        {
            siscProject.Language = SiScLanguage_Cpp;
        }
        else
        {
            // DOCS: default option
            siscProject.Language = SiScLanguage_C;
        }

        const char* pData;
        i32 len;

        if (siscProject.Language == SiScLanguage_Cpp)
        {
            pData = "cppdialect";
            len = 10;
        }
        else
        {
            pData = "cdialect";
            len = 8;
        }

        SimpleString languageVersion = { .Length = len, .pData = (char*) pData };
        languageInd = simple_string_index_of_string(input, languageVersion);
        if (languageInd != -1)
        {
            char* ptr = input.pData + languageInd + 1;
            SimpleString dialectStr = sisc_parse_string(&ptr);
            char* pDialect = dialectStr.pData;
            i32 dilectLength = dialectStr.Length;

            if (siscProject.Language == SiScLanguage_Cpp)
            {
                if (string_compare_length(pDialect, "C++2003", 7))
                {
                    siscProject.LanguageVersion = SiScLanguageVersion_Cpp2003;
                }
                else if (string_compare_length(pDialect, "C++11", 5))
                {
                    siscProject.LanguageVersion = SiScLanguageVersion_Cpp11;
                }
                else if (string_compare_length(pDialect, "C++14", 5))
                {
                    siscProject.LanguageVersion = SiScLanguageVersion_Cpp14;
                }
                else if (string_compare_length(pDialect, "C++17", 5))
                {
                    siscProject.LanguageVersion = SiScLanguageVersion_Cpp17;
                }
                else if (string_compare_length(pDialect, "C++23", 5))
                {
                    siscProject.LanguageVersion = SiScLanguageVersion_Cpp23;
                }
            }
            else
            {
                if (string_compare_length(pDialect, "C89", 3))
                {
                    siscProject.LanguageVersion = SiScLanguageVersion_C89;
                }
                else if (string_compare_length(pDialect, "C11", 3))
                {
                    siscProject.LanguageVersion = SiScLanguageVersion_C11;
                }
                else if (string_compare_length(pDialect, "C23", 3))
                {
                    siscProject.LanguageVersion = SiScLanguageVersion_C23;
                }
                else
                {
                    siscProject.LanguageVersion = SiScLanguageVersion_C99;
                }
            }

        }
    }

    SimpleString outputDir = { .Length = 9, .pData = "outputdir" };
    i32 outputDirInd = simple_string_index_of_string(input, outputDir);
    if (outputDirInd != -1)
    {
        char* ptr = input.pData + outputDirInd + 1;
        char c = *ptr;

        while (c != '\0')
        {
            if (c == '\"')
            {
                SimpleString outputDir = sisc_parse_string(&ptr);

                SimpleString finalStr = {};

                SiScBuildConfig config = projectSet.BuildConfig;

                for (i32 i = 0; i < outputDir.Length; ++i)
                {
                    char* nptr = &outputDir.pData[i];
                    char nc = *nptr;
                    if (nc == '%' && (memcmp(nptr+1, "{cfg.buildcfg}", 14) == 0))
                    {
                        const char* pConf = sisc_configuration_to_string(config.Configuration);
                        finalStr = simple_string_add_string(finalStr, (char*)pConf, string_length(pConf));
                        finalStr = simple_string_add_string(finalStr, "-", 1);
                    }
                    else if (nc == '%' && (memcmp(nptr+1, "{cfg.system}", 12) == 0))
                    {
                        const char* pPlatform = sisc_platform_to_string(config.Platform);
                        finalStr = simple_string_add_string(finalStr, (char*)pPlatform, string_length(pPlatform));
                        finalStr = simple_string_add_string(finalStr, "-", 1);
                    }
                    else if (nc == '%' && (memcmp(nptr+1, "{cfg.architecture}", 18) == 0))
                    {
                        const char* pArch = sisc_architecture_to_string(config.Architecture);
                        finalStr = simple_string_add_string(finalStr, (char*)pArch, string_length(pArch));
                    }
                }

                siscProject.DirectoryName = finalStr;

                break;
            }

            ++ptr;
            c = *ptr;
        }
    }

    SimpleString target = { .Length = 6, .pData = "target" };
    i32 targetInd = simple_string_index_of_string(input, target);
    if (targetInd != -1)
    {
        char* ptr = input.pData + targetInd + 1;

        SkipToChar(ptr, '"');
        SimpleString p0 = sisc_parse_string(&ptr);
        ++ptr;
        SkipToChar(ptr, '"');
        SimpleString p1 = sisc_parse_string(&ptr);

        SimpleString dn = siscProject.DirectoryName;
        SimpleString np = {};
        np = simple_string_add_string(np, p0.pData, p0.Length);
        np = simple_string_add_string(np, dn.pData, dn.Length);
        np = simple_string_add_string(np, p1.pData, p1.Length);

        siscProject.Target = np;
    }

    SimpleString objDir = { .Length = 6, .pData = "objdir" };
    i32 objDirInd = simple_string_index_of_string(input, objDir);
    if (objDirInd != -1)
    {
        char* ptr = input.pData + objDirInd + 1;

        SkipToChar(ptr, '"');
        SimpleString p0 = sisc_parse_string(&ptr);
        ++ptr;
        SkipToChar(ptr, '"');
        SimpleString p1 = sisc_parse_string(&ptr);

        SimpleString dn = siscProject.DirectoryName;
        SimpleString np = {};
        np = simple_string_add_string(np, p0.pData, p0.Length);
        np = simple_string_add_string(np, dn.pData, dn.Length);
        np = simple_string_add_string(np, p1.pData, p1.Length);

        siscProject.ObjDir = np;
    }

    SimpleString includeDirs = { .Length = 11, .pData = "includedirs" };
    i32 includeDirsInd = simple_string_index_of_string(input, includeDirs);
    if (includeDirsInd != -1)
    {
        char* ptr = input.pData + includeDirsInd + 1;

        char c = *ptr;
        while (c != '}')
        {
            if (c == '"')
            {
                SimpleString includeDir = sisc_parse_string(&ptr);
                SkipToChar(ptr, '\n');
                SimpleString empty = {};
                empty = simple_string_add_string(empty, includeDir.pData, includeDir.Length);
                sisc_array_push(siscProject.aIncludes, empty);
            }
            else if (string_compare_length(ptr, "VULKAN_SDK_PATH", 15))
            {
                SkipToChar(ptr, '"');
                SimpleString includeDir = sisc_parse_string(&ptr);

                char* pVulkanSDKPath = getenv("VULKAN_SDK");
                i32 len = string_length(pVulkanSDKPath);

                SimpleString empty = {};
                empty = simple_string_add_string(empty, pVulkanSDKPath, len);
                empty = simple_string_add_string(empty, includeDir.pData, includeDir.Length);

                sisc_array_push(siscProject.aIncludes, empty);
            }

            ++ptr;
            c = *ptr;
        }
    }

    SimpleString* aDeps = NULL;

    // todo: Parse Deps
    SimpleString groupDeps = { .Length = 7, .Size = 7, .pData = "group \"" };
    i32 groupDepsInd = simple_string_index_of_string(input, groupDeps);
    if (groupDepsInd != -1)
    {
        char* ptr = input.pData + groupDepsInd + 1;
        aDeps = parse_deps_group(&ptr, groupDepsInd);
    }

    SimpleString projectStr = { .Length = 7, .Size = 7, .pData = "\nproject" };
    i32 projectStrInd = simple_string_index_of_string(input, projectStr);
    if (projectStrInd != -1)
    {
        char* ptr = input.pData + projectStrInd + 1;
        SkipToChar(ptr, '\"');
        SimpleString temp = sisc_parse_string(&ptr);
        SimpleString projectName = simple_string(temp);
        siscProject.Name = projectName;
    }

    const char* pCurrentDir = projectSet.CurrentDirectory.pData;

    // DOCS: Parse location of current project
    SimpleString locationStr = { .Length = 10, .Size = 10, .pData = "location \"" };
    i32 locationStrInd = simple_string_index_of_string(input, locationStr);
    if (locationStrInd != -1)
    {
        char* ptr = input.pData + locationStrInd + 1;
        SkipToChar(ptr, '\"');
        SimpleString projectLocation = sisc_parse_string(&ptr);
        SimpleString empty = {};
        empty = simple_string_add_string(empty, (char*) pCurrentDir, string_length(pCurrentDir));
        empty = simple_string_add_string(empty, (char*) "/", 1);
        empty = simple_string_add_string(empty, projectLocation.pData, projectLocation.Length);

        siscProject.Location = empty;
    }
    else
    {
        siscProject.Location = projectSet.Directory;
    }

    SimpleString kindStr = { .Length = 10, .Size = 10, .pData = "kind \"" };
    i32 kindStrInd = simple_string_index_of_string(input, locationStr);
    if (kindStrInd != -1)
    {
        char* ptr = input.pData + kindStrInd + 1;
        SkipToChar(ptr, '\"');
        SimpleString kind = sisc_parse_string(&ptr);

        char* pKind = kind.pData;
        if (string_compare_length(pKind, "ConsoleApp", 10))
        {
            siscProject.Type = SiScProjectType_ConsoleApp;
        }
        else if (string_compare_length(pKind, "StaticLib", 9))
        {
            siscProject.Type = SiScProjectType_StaticLib;
        }
        else if (string_compare_length(pKind, "DynamicLib", 10))
        {
            siscProject.Type = SiScProjectType_DynamicLib;
        }
    }
    else
    {
        siscProject.Type = SiScProjectType_ConsoleApp;
    }

    // todo: deal with deps first
    i32 depsCount = sisc_array_count(aDeps);
    for (i32 d = 0; d < depsCount; ++d)
    {
        SimpleString dep = aDeps[d];

        char* pCombined = path_combine(pCurrentDir, dep.pData);
        char* pNewCombined = path_combine(pCombined, "premake5.lua");
        u64 fileLen = 0;
        char* pFileContent = io_file_read_string_ext(pNewCombined, &fileLen);

        i32 depLen = string_length(pCombined);

        SiScProject depProject =
            sisc_parse_project((SiScProjectSettings) {
                    .Input = {
                        .Length = fileLen,
                        .Size = fileLen,
                        .pData = pFileContent
                    },
                    .Directory = {.Length = depLen, .Size = depLen, .pData = pCombined},
                    .CurrentDirectory = projectSet.CurrentDirectory,
                    .BuildConfig = projectSet.BuildConfig,
                });

        sisc_array_push(siscProject.aDeps, depProject);

        free(pFileContent);
    }

    // todo: add files for compilation
    if (siscProject.Language == SiScLanguage_C)
    {
        siscProject.aFiles = io_get_all_files_by_mask(siscProject.Location.pData, ".c", 2);
    }
    else
    {
        siscProject.aFiles = io_get_all_files_by_mask(siscProject.Location.pData, ".cpp", 4);
    }

    // todo: remove all memory leaks (mb not)

    return siscProject;
}

void
sisc_project_print(SiScProject siscProject)
{
    printf("=========================== BEGIN\n\n");

    // NOTE: Printing for validation
    printf("Project name: %s\n", siscProject.Name.pData);
    printf("Project location: %s\n", siscProject.Location.pData);
    printf("Project curDir: %s\n", siscProject.CurrentDirectory.pData);
    printf("Project objdir: %s\n", siscProject.ObjDir.pData);
    printf("Project targetdir: %s\n", siscProject.Target.pData);
    printf("Project language: %s\n", sisc_project_language_to_string(siscProject.Language));
    printf("Language version: %s\n",
           sisc_language_version_to_string(
               siscProject.LanguageVersion));
    printf("Project type: %s\n", sisc_project_type_to_string(siscProject.Type));

    for (i32 i = 0; i < sisc_array_count(siscProject.aConfigurations); ++i)
    {
        printf("conf: %s\n", sisc_configuration_to_string(siscProject.aConfigurations[i]));
    }

    for (i32 i = 0; i < sisc_array_count(siscProject.aIncludes); ++i)
    {
        printf("include: %s\n", siscProject.aIncludes[i].pData);
    }

    printf("bin-dir-name: %s\n", siscProject.DirectoryName.pData);

    printf("\n\n===========================\n");
}

void
sisc_project_compile_file(SiScProject* pProject, SimpleString file)
{
    char* pName = path_get_name_wo_ext(file.pData);

#define CmdSize 2049

    char cmd[CmdSize] = {};
    i32 bytesCount =
        snprintf(
            cmd, CmdSize,
            "clang -c -o %s/%s/%s.o",
            pProject->Location.pData,
            pProject->ObjDir.pData,
            pName);

    for (i32 i = 0; i < sisc_array_count(pProject->aIncludes); ++i)
    {
        SimpleString include = pProject->aIncludes[i];
        bytesCount += snprintf(cmd + bytesCount, CmdSize - bytesCount, " -I%s", include.pData);
    }

    snprintf(cmd + bytesCount, CmdSize - bytesCount, " %s", file.pData);

    printf("%s\n", cmd);

    system(cmd);

#undef CmdSize
}

void
sisc_project_compile(SiScProject* pProject)
{
    // todo: impl

    // pProject->CurrentDirectory;
    // pProject->ObjDir;

    for (i32 i = 0; i < sisc_array_count(pProject->aFiles); ++i)
    {
        SimpleString file = pProject->aFiles[i];
        sisc_project_compile_file(pProject, file);
    }

}

SiScParseResult
sisc_parse_premake(SiScParseSettings parseSet)
{
#define ReturnError()                                           \
    ({                                                          \
        (SiScParseResult) { .ResultType=SiScResultType_Error }; \
    })

    u64 fileLen = 0;
    char* pFileContent = io_file_read_string_ext(parseSet.pPath, &fileLen);
    printf("%s\n", pFileContent);

    SimpleString input = (SimpleString) { .Length = fileLen, .pData = pFileContent };
    SimpleString string = (SimpleString) { .Length = 9, .pData = "workspace" };
    i32 ind = simple_string_index_of_string(input, string);
    if (ind == -1)
    {
        return ReturnError();
    }

    char* pDirectory = path_get_directory(parseSet.pPath);
    i32 dirLen = string_length(pDirectory);
    SimpleString directory = {.Length = dirLen, .Size = dirLen, .pData = pDirectory};
    SiScProject siscProject = sisc_parse_project(
        (SiScProjectSettings) {
            .Input = input,
            .Directory = directory,
            .BuildConfig = parseSet.BuildConfig,
            .CurrentDirectory = parseSet.CurrentDirectory,
        });

    sisc_project_print(siscProject);

#if 0
    for (i32 i = 0; i < sisc_array_count(siscProject.aDeps); ++i)
    {
        SiScProject dep = siscProject.aDeps[i];
        printf("dep name: %s\n", dep.Name.pData);
        printf("dep locn: %s\n", dep.Location.pData);

        for (i32 f = 0; f < sisc_array_count(dep.aFiles); ++f)
        {
            SimpleString file = dep.aFiles[f];
            printf("-file: %s\n", file.pData);
        }
    }
#endif

    SiScProject d0 = siscProject.aDeps[0];
    sisc_project_print(d0);
    sisc_project_compile(&d0);


    //siscProject.aDeps[0];

    SiScParseResult res = {
        .ResultType = SiScResultType_Success,
    };

    return res;
}


i32
main(i32 argc, char** aArgs)
{
    // todo: look at this
    // https://mottosso.gitbooks.io/clang/content/building_a_static_library.html

#if 1
    // todo: impl proper CLI
    printf("Hello, world %d %s!\n", argc, aArgs[0]);

    // todo: impl replace(sdk, to_real_path)
    char* pVulkanSDKPath = getenv("VULKAN_SDK");
    printf("VulkanSDK Path: %s\n", pVulkanSDKPath);

    SimpleString curDir = { .Length = 37, .Size = 37, .pData = "/home/bies/BigData/programming/C/STD/" };

    SiScBuildConfig buildConfig = {

        .Configuration = SiScConfiguration_Debug,
#if defined(PLATFORM_LINUX)
        .Platform = SiScPlatform_Linux,
#elif defined(PLATFORM_WINDOWS)
        .Platform = SiScPlatform_Windows,
#endif
        .Architecture = SiScArchitecture_x86_64
    };

    const char* pPath = "/home/bies/BigData/programming/C/STD/premake-gl-sandbox.lua";
    SiScParseSettings parseSet = {
        .pPath = pPath,
        .BuildConfig = buildConfig,
        .CurrentDirectory = curDir,
    };
    SiScParseResult parseRes =
        sisc_parse_premake(parseSet);
    printf("Result: %s\n", sisc_result_type_to_string(parseRes.ResultType));
    // getenv("VULKAN_SDK");

#else

    /* SimpleString dir = {.Length = 65, .pData = "/home/bies/BigData/programming/C/STD/Dependencies/Chipmunk2D/src/"}; */

    /* const char** aFiles = path_directory_get_files(dir.pData); */
    /* for (i32 i = 0; i < sisc_array_count(aFiles); ++i) */
    /* { */
    /*     printf("file: %s\n", aFiles[i]); */
    /* } */
    // path_directory_get_directories(const char* directory);

#if 1
    const char* pDir = "/home/bies/BigData/programming/C/STD/Dependencies/Chipmunk2D/src/";
#else
    const char* pDir = "/home/bies/BigData/programming/C/STD/STD/src";
#endif
    const char* pOut = "/home/bies/BigData/programming/C/STD/bin/Temp";

    if (!path_is_directory_exist(pOut))
    {
        path_directory_create(pOut);
    }

    SimpleString* aFiles = io_get_all_files_by_mask(pDir, ".c", 2);

    const char* pIncludeDir = "/home/bies/BigData/programming/C/STD/Dependencies/Chipmunk2D/src/";

    for (i32 i = 0; i < sisc_array_count(aFiles); ++i)
    {
        SimpleString file = aFiles[i];

        char* pNameWoExt = path_get_name_wo_ext(file.pData);
        printf("file: %s %s\n", file.pData, pNameWoExt);

        char cmd[2049] = {};
        snprintf(cmd, 2049, "clang -c -o %s/%s.o -I%s %s",
                 pOut,
                 pNameWoExt,
                 pIncludeDir,
                 file.pData
            );

        system(cmd);
    }

    //system("clang");

#endif

    return 0;
}
