/* #include "Sandbox.h" */
/* #include <EntryPoint.h> */

#include <stdio.h>
#include <stdlib.h>
#include <Core/Types.h>
#include <Core/SystemInfo.h>
#include <Core/SimpleStandardLibrary.h>
#include <Application/SimpleApplication.h>
#include "GlSandbox.h"
#include <locale.h>


i32
main()
{
    setlocale(LC_ALL, ".UTF8");
    system_info_print();

    SimpleApplicationSettings set = {
	.pName = simple_string_new("Привет, мир!"),

	.ArgsCount = 0,
	.aArgs = NULL,

	.IsDebug = 1,

	.IsVsync = 1,
	.Size = (v2) { 1920, 1080 },
    };

    simple_application_create(&set);

    SimpleLayer sandboxLayer = {
	.pName = simple_string_new("GlSandbox"),
	.OnAttach = gl_sandbox_on_attach,
	.OnUpdate = gl_sandbox_on_update,
	.OnUi = gl_sandbox_on_ui,
	.OnEvent = gl_sandbox_on_event,
	.OnDestroy = gl_sandbox_on_destroy,
    };
    simple_application_add_layer(sandboxLayer);

    simple_application_run();
    simple_application_destroy();

    return 0;
}
