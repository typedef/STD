#include "GlSandbox.h"
#include "Application/SimpleApplication.h"
#include "Core/SimpleFont.h"
#include "Std/StdGlRenderer.h"

#include <Core/Types.h>
#include <Core/SimpleWindowLibrary.h>
#include <Std/StdOpenGL.h>
#include <Math/SimpleMath.h>
#include <Deps/stb_image.h>
#include <Core/SimpleStandardLibrary.h>

#include "EntitySystem/StdCameraEntity.h"
#include "Std/StdUi.h"
#include <Std/StdAtlas.h>
#include <Math/SimpleMathIO.h>
#include <Std/StdAtlasAnimation.h>
#include <EntitySystem/StdScene.h>


static StdCameraEntity gCamera = {};

static u32 TextureTest = 0;
static u32 Khight = 0;
static v2 TextureTestSize;
static v2 KhightSize;
static u32 gAtlasTextureId;
static SimpleFont gFont;

static StdAtlas gStdAtlas = {};
static StdAtlasAnimation gKnightAnim0 = {};
static StdAtlasAnimation gKnightAnim1 = {};


void
gl_sandbox_on_attach()
{
    SwlWindow* pWindow = simple_application_get_window();

    {
	v2 size;
	i32 channels;
	const char* pPath = "/home/bies/BigData/programming/C/SimpleGameEngine/assets/textures/atlas/RPGpack_sheet.png";
	TextureTest = std_gl_renderer_texture_create(pPath, &size, &channels);
    }

    {
	v2 size;
	i32 channels;
	const char* pPath2 = "/home/bies/MidData/Assets/Textures/Animations/Knight/Colour1/NoOutline/120x80_PNGSheets/_Idle.png";
	Khight = std_gl_renderer_texture_create(pPath2, &size, &channels);
    }

    {

	gKnightAnim0 =
	    std_atlas_animation_new(
		(StdAtlasAnimationSettings) {
		    .SecondsForOneIter = 2.0,
		    .Atlas = std_atlas_build_from_file(
			"Assets/Atlas/Idle-120-80.png")
		});

	gKnightAnim1 =
	    std_atlas_animation_new(
		(StdAtlasAnimationSettings) {
		    .SecondsForOneIter = 2.0,
		    .Atlas = std_atlas_build_from_file_ext(
			"Assets/Atlas/Roll-120-80.png")
		});

    }
}

void
gl_sandbox_on_attach_finished()
{
}

void
gl_sandbox_on_update()
{
    std_gl_renderer_set_clear_color(v4_new(0.004321, 0.00806007, 0.007, 1));

    std_gl_renderer_draw_rect(v3_new(1250, 50, 1), v2_new(250, 250), v4_new(0.85, 0.74, 0.45, 1.0));
    std_gl_renderer_draw_rect(v3_new(1250, 1200, 1), v2_new(50, 250), v4_new(0.675, 0.74, 0.45, 1.0));
    std_gl_renderer_draw_rect(v3_new(1250, 1200, 1), v2_new(300, 300), v4_new(0.775, 0.74, 0.45, 0.5));

    std_gl_renderer_draw_texture(v3_new(100, 800, 3), v2_new(350, 350), TextureTest);

    SimpleRuntimeStats stats = simple_application_get_stats();

    // note: for testing
    // std_gl_renderer_draw_rect(v3_new(10, -250, 9), v2_new(500, 500), v4_new(0.8, 0.6, 0.5, 1));

    // DOCS: anim 0
    std_atlas_animation_update(&gKnightAnim0, stats.Timestep);
    std_gl_renderer_draw_atlas_item(v3_new(0,0,10), 5, gKnightAnim0.Atlas.aAtlasItems[gKnightAnim0.CurrentItem]);

    // DOCS: anim 1
    std_atlas_animation_update(&gKnightAnim1, stats.Timestep);
    std_gl_renderer_draw_atlas_item(v3_new(400,0,10), 5, gKnightAnim1.Atlas.aAtlasItems[gKnightAnim1.CurrentItem]);

    // DOCS: anim 2
    std_atlas_animation_update(&gKnightAnim1, stats.Timestep);
    std_gl_renderer_draw_atlas(v3_new(10,0,8), v2_mulv(v2_new(120, 40), 5), v4_new(1,1,1,1), gKnightAnim1.Atlas, gKnightAnim1.CurrentItem);

    std_gl_renderer_draw_string(v3_new(655, 955, 1), v4_new(0.654, 0.756, 0.453, 0), "Привет, мир!", 32);


    SwlWindow* pWindow = simple_application_get_window();
    SimpleRuntimeStats runtimeStats = simple_application_get_stats();
    i32 isCameraMoved = 0;
    real speed = 2500;
    if (pWindow->aKeys[SwlKey_Shift] == SwlAction_Press)
    {
	speed = speed*10;
    }
    real posToAdd = speed * runtimeStats.Timestep;

    StdCameraEntity* pCamera = std_scene_get_main_camera();

    // note: just for testing camera update in renderer
    if (pWindow->aKeys[SwlKey_D] == SwlAction_Press)
    {
	std_camera_entity_move(pCamera, v3_negativec(posToAdd, 0, 0));
	std_camera_entity_update(pCamera);
	std_gl_renderer_set_camera(&pCamera->ViewProjection);
    }
    if (pWindow->aKeys[SwlKey_A] == SwlAction_Press)
    {
	std_camera_entity_move(pCamera, v3_new(posToAdd, 0, 0));
	std_camera_entity_update(pCamera);
	std_gl_renderer_set_camera(&pCamera->ViewProjection);
    }
    if (pWindow->aKeys[SwlKey_W] == SwlAction_Press)
    {
	std_camera_entity_move(pCamera, v3_new(0, posToAdd, 0));
	std_camera_entity_update(pCamera);
	std_gl_renderer_set_camera(&pCamera->ViewProjection);
    }
    if (pWindow->aKeys[SwlKey_S] == SwlAction_Press)
    {
	std_camera_entity_move(pCamera, v3_negativec(0, posToAdd, 0));
	std_camera_entity_update(pCamera);
	std_gl_renderer_set_camera(&pCamera->ViewProjection);
    }

}

void
gl_sandbox_on_ui()
{
    // todo: impl
    /* if (std_ui_panel_begin((StdUiPanelSettings) {.pUtfLabel = "Панель"})) */
    /* { */
    /*	if (std_ui_button((StdUiButton) { .pLabel = "Btn", .Size = v2_new(100, 100) })) */
    /*	{ */
    /*	} */
    /* } */
    /* std_ui_panel_end(); */
}

void
gl_sandbox_on_event(SwlEvent* pEvent)
{
    if (pEvent->Type == SwlEventType_MouseScroll)
    {
	SwlMouseScrollEvent* pScroll = (SwlMouseScrollEvent*) pEvent;
	if (pScroll->Value)
	{
	    StdCameraEntity* pCamera = std_scene_get_main_camera();
	    f32 value = (f32) pScroll->Value;
	    if (value < 0)
	    {
		/* gCamera.Base.Position.Z = 0.1; */
		/* gCamera.IsCameraMoved = 1; */
		/* gCamera.IsZMoved = 1; */
		std_camera_entity_move_update(pCamera, v3_new(0,0,-0.1));
	    }
	    else
	    {
		std_camera_entity_move_update(pCamera, v3_new(0,0,0.1));
	    }

	    std_camera_entity_update(pCamera);
	    std_gl_renderer_set_camera(&pCamera->ViewProjection);
	    //GINFO("Move camera\n");
	    //v3_print(gCamera.Base.Position);
	}
    }
}

void
gl_sandbox_on_destroy()
{
    std_gl_renderer_destroy();
}
