#ifndef GL_SANDBOX_H
#define GL_SANDBOX_H

struct SwlEvent;

void gl_sandbox_on_attach();
void gl_sandbox_on_attach_finished();
void gl_sandbox_on_update();
void gl_sandbox_on_ui();
void gl_sandbox_on_event(struct SwlEvent* event);
void gl_sandbox_on_destroy();

#endif // GL_SANDBOX_H
