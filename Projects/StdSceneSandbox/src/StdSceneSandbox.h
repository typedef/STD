#ifndef STD_SCENE_SANDBOX_H
#define STD_SCENE_SANDBOX_H


struct SwlEvent;

void std_scene_sandbox_on_attach();
void std_scene_sandbox_on_destroy();
void std_scene_sandbox_on_update();
void std_scene_sandbox_on_ui();
void std_scene_sandbox_on_event(struct SwlEvent* pEvent);

#endif // STD_SCENE_SANDBOX_H
