#include <stdio.h>
#include <stdlib.h>
#include <Core/Types.h>
#include <Core/SystemInfo.h>
#include <Core/SimpleStandardLibrary.h>
#include <Core/SimpleApplication.h>
#include "StdSceneSandbox.h"
#include <locale.h>


i32
main()
{
    setlocale(LC_ALL, ".UTF8");
    system_info_print();

    SimpleApplicationSettings set = {
	.pName = "StdSceneSandbox",

	.ArgsCount = 0,
	.aArgs = NULL,

	.IsDebug = 1,
	.IsVsync = 1,
	.Size = (v2) { 2550, 1330 },
    };

    simple_application_create(&set);

    SimpleLayer layer = {
	.pName = simple_string_new("StdSceneSandbox"),
	.OnAttach = std_scene_sandbox_on_attach,
	.OnUpdate = std_scene_sandbox_on_update,
	.OnUi = std_scene_sandbox_on_ui,
	.OnEvent = std_scene_sandbox_on_event,
	.OnDestroy = std_scene_sandbox_on_destroy,
    };
    simple_application_add_layer(layer);

    simple_application_run();
    simple_application_destroy();

    return 0;
}
