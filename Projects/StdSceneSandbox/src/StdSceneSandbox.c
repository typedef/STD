#include "StdSceneSandbox.h"
#include "Std/StdInput.h"

#include <Core/SimpleApplication.h>
#include <Core/SimpleMath.h>
#include <Core/SimpleMathIO.h>
#include <Core/Types.h>
#include <Core/SimpleWindowLibrary.h>
#include <Core/SimpleStandardLibrary.h>
#include <EntitySystem/StdScene.h>
#include <Std/StdAssetManager.h>
#include <Std/StdAtlas.h>
#include <Std/StdAtlasAnimation.h>
#include <Std/StdGeneratedTexture.h>
#include <Std/StdRenderer.h>
#include <Std/StdUi.h>
#include <Std/StdUiCustom.h>
#include <Std/StdUiExamples.h>
#include <EntitySystem/StdVisibleEntity.h>

#include <Layers/GraphicsLayer.h>

// DOCS: WarGame
#include <WarGame/Core/WgWorld.h>
#include <WarGame/Core/WgController.h>


i64 textureId;
i64 tid;
i64 glTextureId;
StdAtlasAnimation gAnimation;
StdAtlas gBottleAtlas;

void
std_scene_sandbox_on_attach()
{
    // swl_window_set_wo_header(simple_application_get_window());

#if 0
    editor_layer_register();

    StdGeneratedTexture generatedTexture = std_generated_texture_create_ground();
    i64 assetId = std_asset_manager_load_texture_raw(
	generatedTexture.pName, generatedTexture.Size, generatedTexture.Channels, generatedTexture.pData);

    StdVisibleEntitySettings groundSet = {
	.Position = v3_new(100, 100, 10),
	.Size = v2_new(100, 50),
	.AssetId = assetId
    };
    StdVisibleEntity ground = std_visible_entity_new(groundSet);

    // std_scene_get_main_camera();
    std_scene_add_visible_entity(ground);
#endif

    wg_controller_new((WgController) { .Speed = { .X = 1500.0f, .Y = 1500.0f }, .EnableDebug = 1});
    wg_world_create();

}

void
std_scene_sandbox_on_destroy()
{
}

void
std_scene_sandbox_on_update()
{
    wg_controller_update();
    wg_world_update();
}

void
std_scene_sandbox_on_ui()
{
}

void
std_scene_sandbox_on_event(SwlEvent* pEvent)
{
    if (pEvent->Type == SwlEventType_KeyPress)
    {
	SwlKeyPressEvent* pKeyPress = (SwlKeyPressEvent*) pEvent;

	switch (pKeyPress->Key)
	{

	case SwlKey_F11:
	{
	    SwlWindow* pWindow = simple_application_get_window();
	    swl_window_set_maximized(pWindow);
	    break;
	}

	}

    }

    wg_controller_event(pEvent);
}
