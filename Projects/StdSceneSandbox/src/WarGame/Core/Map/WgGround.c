#include "WgGround.h"

WgGround
wg_ground_new(WgGroundSettings set)
{
    WgGround wgGround = {
	.Base = wg_entity_new(set.Position),
	.Size = set.Size
    };

    return wgGround;
}
