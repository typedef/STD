#ifndef WG_GROUND_H
#define WG_GROUND_H

#include <WarGame/Core/WgEntity.h>

typedef struct WgGroundSettings
{
    v3 Position;
    v2 Size;
} WgGroundSettings;

typedef struct WgGround
{
    WgEntity Base;
    v2 Size;
} WgGround;

WgGround wg_ground_new(WgGroundSettings set);

#endif // WG_GROUND_H
