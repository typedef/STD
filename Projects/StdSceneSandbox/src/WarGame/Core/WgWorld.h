#ifndef WG_WORLD_H
#define WG_WORLD_H

#include <WarGame/Core/Buildings/WgCastle.h>
#include <WarGame/Core/Map/WgGround.h>
#include <WarGame/Core/Npc/WgPerson.h>

typedef struct WgWorld
{
    WgGround* aGrounds;
    WgCastle* aNpcCastles;

    WgPerson* aPersons;
} WgWorld;

void wg_world_create();
void wg_world_update();

#endif // WG_WORLD_H
