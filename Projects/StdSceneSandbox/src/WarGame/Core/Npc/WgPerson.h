#ifndef WG_PERSON_H
#define WG_PERSON_H

#include <WarGame/Core/WgEntity.h>
#include <WarGame/Core/Ai/WgNpcMind.h>


typedef enum WgPersonType
{
    WgPersonType_None = 0,

    WgPersonType_Knight,

    WgPersonType_Count
} WgPersonType;

typedef struct WgPersonSetting
{
    v3 Position;
    char* pName;
    WgPersonType Type;
} WgPersonSetting;


typedef struct WgPerson
{
    WgEntity Base;
    WgPersonType Type;
    char* pName;

    WgNpcMind Mind;
} WgPerson;

WgPerson wg_person_new(WgPersonSetting set);

#endif // WG_PERSON_H
