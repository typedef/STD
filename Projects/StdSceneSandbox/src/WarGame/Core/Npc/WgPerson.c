#include "WgPerson.h"

WgPerson
wg_person_new(WgPersonSetting set)
{
    WgPerson wgPerson = {
	.Base = wg_entity_new(set.Position),
	.Type = set.Type,
	.pName = set.pName,
	.Mind = wg_npc_mind_create()
    };

    return wgPerson;
}

void
wg_person_update(WgPerson* pWgPerson)
{

}
