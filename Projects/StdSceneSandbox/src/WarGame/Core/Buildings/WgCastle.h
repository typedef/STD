#ifndef WG_CASTLE_H
#define WG_CASTLE_H

#include <WarGame/Core/WgEntity.h>


typedef struct WgCastleSettings
{
    v3 Position;
    char* pName;
} WgCastleSettings;

typedef struct WgCastle
{
    WgEntity Base;
    const char* pName;
} WgCastle;

WgCastle wg_castle_new(WgCastleSettings set);

#endif // WG_CASTLE_H
