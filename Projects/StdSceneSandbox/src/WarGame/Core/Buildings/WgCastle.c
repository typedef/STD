#include "WgCastle.h"


WgCastle
wg_castle_new(WgCastleSettings set)
{
    WgCastle wgCastle = {
	.Base = wg_entity_new(set.Position),
	.pName = set.pName
    };

    return wgCastle;
}
