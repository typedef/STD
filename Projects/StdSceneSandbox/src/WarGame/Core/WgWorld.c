#include "WgWorld.h"
#include "EntitySystem/StdScene.h"
#include "EntitySystem/StdVisibleEntity.h"
#include "Std/StdAssetManager.h"
#include "WarGame/Core/WgController.h"

#include <Core/SimpleApplication.h>
#include <Core/SimpleWindowLibrary.h>
#include <Core/SimpleStandardLibrary.h>
#include <EntitySystem/StdCameraEntity.h>
#include <Core/SimpleMath.h>
#include <Std/StdRenderer.h>
#include <Std/StdAtlas.h>
#include <Std/StdAtlasAnimation.h>

#include "WgRenderer.h"


struct GlobalItem
{
    SwlWindow* pWindow;
    WgWorld World;
};

static struct GlobalItem g = {};

void
wg_world_create()
{
    GINFO("Create wgWorld\n");

    g.pWindow = simple_application_get_window();

    wg_renderer_create();

    const f32 single_edge_width = 30000;

    const f32 firstPosX = 500.0f;
    GINFO("First pos: %f\n", firstPosX);

    { // docs: castle creation
	WgCastle firstCastle = wg_castle_new((WgCastleSettings) {
		.Position = v3_new(firstPosX, 550, 3),
		.pName = "Purple Castle"
	    });

	array_push(g.World.aNpcCastles, firstCastle);
    }

    { // docs: ground creation
	WgGround ground00 = wg_ground_new((WgGroundSettings) {
		.Position = v3_new(-single_edge_width/2, 100, 3),
		.Size = v2_new(single_edge_width, 50),
	    });
	array_push(g.World.aGrounds, ground00);
    }

    { // docs: person creation

	WgPerson priest = wg_person_new((WgPersonSetting) {
		.Position = v3_new(firstPosX, 175, 3-0.1f),
		.pName = "Rojer",
		.Type = WgPersonType_Knight
	    });

	array_push(g.World.aPersons, priest);
    }

    { // todo: place some-where-else
	// docs: load all animations
	/* StdAtlasAnimation knightIdleAnimation = */
	/*     std_atlas_animation_new( */
	/*	(StdAtlasAnimationSettings) { */
	/*	    .SecondsForOneIter = 2.0, */
	/*	    .Atlas = std_atlas_build_from_file( */
	/*		"Assets/Atlas/Idle-120-80.png") */
	/*	}); */
	// todo: impl load animation ??
	//std_asset_manager_load_
	/* GINFO("atlas id: %d\n", knightIdleAnimation.Atlas.TextureId); */
	/* g.aKnightIdleAnimations[WgPersonType_Knight] = knightIdleAnimation; */
    }
}


void
wg_world_update()
{
    wg_renderer_draw_world(&g.World);
}
