#ifndef WG_ENTITY_H
#define WG_ENTITY_H

#include <Core/Types.h>


typedef struct WgEntity
{
    i64 Id;
    v3 Position;
} WgEntity;

WgEntity wg_entity_new(v3 position);

#endif // WG_ENTITY_H
