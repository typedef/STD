#ifndef WG_RENDERER_H
#define WG_RENDERER_H

struct WgWorld;


void wg_renderer_create();
void wg_renderer_draw_world(struct WgWorld* pWorld);

#endif // WG_RENDERER_H
