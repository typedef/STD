#include "WgRenderer.h"

#include "WgWorld.h"

// todo: place into Core
#include <Core/SimpleApplication.h>
// todo: place into Core
#include <Core/SimpleMath.h>
#include <Core/SimpleMathIO.h>
#include <Core/SimpleStandardLibrary.h>
#include <Std/StdRenderer.h>
#include <Core/SimpleWindowLibrary.h>
#include <EntitySystem/StdScene.h>
#include <EntitySystem/StdCameraEntity.h>
#include <Std/StdAssetManager.h>


struct GlobalItem
{
    i64 Castle0;
};

static struct GlobalItem g = {};

void _wg_draw_color_entity(v3 position, v2 size, v4 color);
void _draw_wg_person(WgPerson* pPerson, f32 timestep);


void
wg_renderer_create()
{
    SwlWindow* pWindow = simple_application_get_window();

    StdCameraEntity camera = std_camera_entity_new(
	(StdCameraEntitySettings) {
	    .WindowSize = { pWindow->Size.Width, pWindow->Size.Height },
	    .IsMain = 1
	});
    camera.Base.Position.Z = 1;
    std_camera_entity_update(&camera);
    std_scene_create((StdSceneSettings){
	    .MainCamera = camera,
	    .aVisibleEntities = NULL
	});

    std_renderer_set_font((StdFontSettings) {
	    .FontSize = 32,
	    .pFontPath = resource_font("MartianMono-sWdRg.ttf"),
	    .IsUpdateFont = 1,
	});

    g.Castle0 = std_asset_manager_load_image("Assets/Castles/Castle0.png");

    StdTexture* aTextures = std_asset_manager_get_all_images();
    std_renderer_set_textures(aTextures, sva_count(aTextures));
}

void
wg_renderer_draw_world(WgWorld* pWorld)
{
    SimpleRuntimeStats stats = simple_application_get_stats();

    //std_renderer_set_clear_color(v4_new(0., 0., 0., 1));

    // DOCS: Drawing
    for (i32 i = 0; i < array_count(pWorld->aGrounds); ++i)
    {
	WgGround ground = pWorld->aGrounds[i];
	_wg_draw_color_entity(ground.Base.Position, ground.Size, v4_new(90/255.0f, 51/255.0f, 34/255.0f, 1));
    }

    for (i32 i = 0; i < array_count(pWorld->aNpcCastles); ++i)
    {
	WgCastle castle = pWorld->aNpcCastles[i];

	std_renderer_draw_image(castle.Base.Position, v2_new(-0.6, 0), g.Castle0);
	//const f32 v = 255.0f;
	//_wg_draw_color_entity(castle.Base.Position, v2_new(200, 450), v4_new(102/v, 0, 153/v, 1));
    }

    for (i32 i = 0; i < array_count(pWorld->aPersons); ++i)
    {
	WgPerson* pPerson = &pWorld->aPersons[i];
	//const f32 v = 255.0f;
	//_wg_draw_color_entity(person.Base.Position, v2_new(50, 75), v4_new(102/v, 177/v, 153/v, 1));

	_draw_wg_person(pPerson, stats.Timestep);
    }

    _wg_draw_color_entity(v3_new(0,500,0), v2_new(100, 300), v4_new(0.8, 0.8, 0.8, 1));
    _wg_draw_color_entity(v3_new(100,500,0), v2_new(100, 300), v4_new(0.8, 0.8, 0.8, 1));

    GINFO("Position: %d\n", std_scene_get_main_camera()->Zoom);
    v3_print(std_scene_get_main_camera()->Base.Position);


    //std_renderer_update();
}

void
_wg_draw_color_entity(v3 position, v2 size, v4 color)
{
    std_renderer_draw_rect(position, size, color);
}

void
_draw_wg_person(WgPerson* pPerson, f32 timestep)
{
    v3 pos = pPerson->Base.Position;

    std_renderer_draw_rect(pos, v2_new(50, 150), v4_new(1,1,1,1));
}
