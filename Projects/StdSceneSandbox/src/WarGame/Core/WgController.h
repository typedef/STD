#ifndef WG_CONTROLLER_H
#define WG_CONTROLLER_H

#include <Core/Types.h>


struct SwlEvent;

typedef struct WgController
{
    i32 EnableDebug;
    v2 Speed;
} WgController;

void wg_controller_new(WgController controller);
void wg_controller_update();
void wg_controller_event(struct SwlEvent* pEvent);

#endif // WG_CONTROLLER_H
