#include "WgNpcMind.h"
#include "WarGame/Core/Ai/WgAiTypes.h"
#include "Module/WgModule.h"


WgNpcMind
wg_npc_mind_create(void)
{
    WgNpcMind mind = {
	.CurrentModule = wg_module_create(WgModuleType_Idle),
	.Needs = wg_npc_needs_new(),
    };

    return mind;
}

void
wg_npc_mind_process(WgNpcMindUpdate update)
{
    WgNpcMind* pMind = update.pMind;

    // note: this is ugly
    pMind->CurrentModule.AiData = (WgAiData) {
	.Timestep = update.Timestep,
	.pNeeds = &update.pMind->Needs
    };

    wg_module_update(&pMind->CurrentModule);

}
