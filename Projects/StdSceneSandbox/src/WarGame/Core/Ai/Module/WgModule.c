#include "WgModule.h"
#include "WarGame/Core/Ai/Module/Basic/IdleModule.h"
#include "WarGame/Core/Ai/WgAiTypes.h"


WgModuleData
wg_module_create(WgModuleType type)
{
    WgModuleData wgModule = { .Type = type };
    return wgModule;
}

void
wg_module_update(WgModuleData* pModuleData)
{
    switch (pModuleData->Type)
    {
    case WgModuleType_Idle:
    {
	wg_idle_module_update(pModuleData);
	break;
    }
    }
}
