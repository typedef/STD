#ifndef WG_MODULE_H
#define WG_MODULE_H

#include <WarGame/Core/Ai/WgAiTypes.h>


typedef struct WgModuleSettings
{
    WgModuleType Type;
} WgModuleSettings;

WgModuleData wg_module_create(WgModuleType type);
void wg_module_update(WgModuleData* pModuleData);

#endif // WG_MODULE_H
