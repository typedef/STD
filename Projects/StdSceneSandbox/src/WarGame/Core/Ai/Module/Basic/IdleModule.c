#include "IdleModule.h"
#include <WarGame/Core/Ai/WgAiTypes.h>
#include <WarGame/Core/Ai/Activity/WgActivity.h>


void
wg_idle_module_update(WgModuleData* pData)
{
    switch (pData->CurrentActivity)
    {
    case 0:
	wg_activity_idle(&pData->AiData);
	break;
    }
}
