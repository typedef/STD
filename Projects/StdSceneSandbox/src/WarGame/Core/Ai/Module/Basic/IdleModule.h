#ifndef IDLE_MODULE_H
#define IDLE_MODULE_H

struct WgModuleData;

void wg_idle_module_update(struct WgModuleData* pData);

#endif // IDLE_MODULE_H
