#ifndef WG_NPC_NEEDS_H
#define WG_NPC_NEEDS_H

#include <Core/Types.h>


typedef struct WgNpcNeeds
{
    f32 Food;
} WgNpcNeeds;

WgNpcNeeds wg_npc_needs_new();

#endif // WG_NPC_NEEDS_H
