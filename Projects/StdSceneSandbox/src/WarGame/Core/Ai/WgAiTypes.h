#ifndef WG_AI_TYPES_H
#define WG_AI_TYPES_H

#include <Core/Types.h>
#include "WgNpcNeeds.h"


typedef struct WgAiData
{
    f32 Timestep;
    WgNpcNeeds* pNeeds;
} WgAiData;

typedef enum WgModuleType
{
    WgModuleType_Idle = 0,
} WgModuleType;

// docs: instance of it inside NpcMind
typedef struct WgModuleData
{
    WgModuleType Type;
    i32 CurrentActivity;
    WgAiData AiData;
} WgModuleData;

typedef void (*WgActivityDelegate)(WgAiData* pData);

#endif // WG_AI_TYPES_H
