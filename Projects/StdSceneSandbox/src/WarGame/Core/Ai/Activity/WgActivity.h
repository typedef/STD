#ifndef WG_ACTIVITY_H
#define WG_ACTIVITY_H


struct WgAiData;

typedef enum WgActivity
{
    WgActivity_None = 0,

    WgActivity_Idle,

    // docs: шляться
    WgActivity_HangingAround,

    WgActivity_Count
} WgActivity;

void wg_activity_idle(struct WgAiData* pWgAiData);
void wg_activity_hanging_around(struct WgAiData* pWgAiData);

#endif // WG_ACTIVITY_H
