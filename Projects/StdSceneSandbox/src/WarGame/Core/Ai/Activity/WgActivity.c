#include "WgActivity.h"
#include <WarGame/Core/Ai/WgAiTypes.h>


#define WgHours(x) (x * 3600.0f)

// todo: make one function for all activities,
// or one entry point for activity with many
// force_inline functions

void
wg_activity_idle(WgAiData* pWgAiData)
{
    const f32 idleStep = 100 / WgHours(16.0);

    WgNpcNeeds* p = pWgAiData->pNeeds;
    f32 delta = pWgAiData->Timestep;

    p->Food -= delta * idleStep;
}

void
wg_activity_hanging_around(WgAiData* pWgAiData)
{
    const f32 idleStep = 100 / WgHours(8.0);

    WgNpcNeeds* p = pWgAiData->pNeeds;
    f32 delta = pWgAiData->Timestep;

    p->Food -= delta * idleStep;
}
