#ifndef WG_NPC_MIND_H
#define WG_NPC_MIND_H

#include "WgAiTypes.h"


typedef struct WgNpcMind
{
    WgModuleData CurrentModule;
    WgNpcNeeds Needs;

    // note: created dynamically
    //WgAiData AiData;
} WgNpcMind;

typedef struct WgNpcMindUpdate
{
    WgNpcMind* pMind;
    f32 Timestep;
} WgNpcMindUpdate /* ? */;


WgNpcMind wg_npc_mind_create(void);
void wg_npc_mind_process(WgNpcMindUpdate update);

#endif // WG_NPC_MIND_H
