#include "WgEntity.h"


static i64 gId = 0;

WgEntity
wg_entity_new(v3 position)
{
    WgEntity entity = {
	.Id = gId,
	.Position = position,
    };

    ++gId;

    return entity;
}
