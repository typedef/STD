#include "WgController.h"
#include <Core/SimpleApplication.h>
#include <Core/SimpleWindowLibrary.h>
#include <Core/SimpleMath.h>
#include <Core/SimpleMathIO.h>
#include <Core/SimpleStandardLibrary.h>
#include <Std/StdRenderer.h>
#include <EntitySystem/StdCameraEntity.h>
#include <EntitySystem/StdScene.h>


static WgController gWgController;

void
wg_controller_new(WgController controller)
{
    gWgController = controller;
}

void
wg_controller_update()
{
    SwlWindow* pWindow = simple_application_get_window();
    SimpleRuntimeStats stats = simple_application_get_stats();
    StdCameraEntity* pCamera = std_scene_get_main_camera();

    i8 isChanged = 0;
    f32 speedModifier = 1.0f;

    if (pWindow->aKeys[SwlKey_Shift] == SwlAction_Press)
    {
	speedModifier = 10.0f;
    }

    if (pWindow->aKeys[SwlKey_A] == SwlAction_Press)
    {
	std_camera_entity_move(pCamera, v2_new(gWgController.Speed.X * stats.Timestep * speedModifier, 0));
	isChanged = 1;
    }
    if (pWindow->aKeys[SwlKey_D] == SwlAction_Press)
    {
	std_camera_entity_move(pCamera, v2_new(-gWgController.Speed.X * stats.Timestep * speedModifier, 0));
	isChanged = 1;
    }

    if (pWindow->aKeys[SwlKey_W] == SwlAction_Press)
    {
	std_camera_entity_move(pCamera, v2_new(0, -gWgController.Speed.Y * stats.Timestep * speedModifier));
	isChanged = 1;
    }
    if (pWindow->aKeys[SwlKey_S] == SwlAction_Press)
    {
	std_camera_entity_move(pCamera, v2_new(0, gWgController.Speed.Y * stats.Timestep * speedModifier));
	isChanged = 1;
    }

    if (isChanged)
    {
	std_camera_entity_update(pCamera);
	std_renderer_set_camera(&pCamera->ViewProjection);
    }

    if (gWgController.EnableDebug)
    {
	// docs: pos.X is negativ shift
	v3 pos = pCamera->Base.Position;
	//v3_print(pos);

	char buf[256]={};
	snprintf(buf, 256, "Camera (debug) pos: (%0.0f,  %0.0f,  %0.0f)", ((pos.X!=0)?-pos.X:pos.X), pos.Y, pos.Z);

	// DOCS: stable position on the screen based on camera current position and window size
	v3 shiftedPos = v3_new(100-pos.X, -pos.Y + 0.9*pCamera->WindowSize.Height, 0.1);
	std_renderer_draw_text_utf8(shiftedPos, v4_new(0.7,0.7,0.7,1), buf);
    }
}

void
wg_controller_event(SwlEvent* pEvent)
{
    if (pEvent->Type == SwlEventType_MouseScroll)
    {
	SwlMouseScrollEvent* pScroll = (SwlMouseScrollEvent*) pEvent;
	if (pScroll->Value)
	{
	    StdCameraEntity* pCamera = std_scene_get_main_camera();

	    GINFO("Before pos:\n");
	    v3_print(pCamera->Base.Position);

	    f32 wxz, wyz;

	    // todo: костыль
	    // todo: переписать камеру
	    if (pCamera->Base.Position.Z == 0)
	    {
		pCamera->Base.Position.Z = 3;
	    }

	    f32 value = (f32) pScroll->Value;
	    if (value < 0)
	    {
		pCamera->Zoom -= 1;
		pCamera->Zoom = Max(pCamera->Zoom, 0);
	    }
	    else
	    {
		pCamera->Zoom += 1;
		pCamera->Zoom = Min(pCamera->Zoom, 4);
	    }

	    v2 ws = pCamera->WindowSize;
	    v2 aZooms[] = {
		v2_new(8*ws.X, 8*ws.Y),
		v2_new(4*ws.X, 4*ws.Y),
		v2_new(2*ws.X, 2*ws.Y),
		v2_new(ws.X, ws.Y),
		v2_new(ws.X/2, ws.Y/2),
	    };

	    v2 zoom = aZooms[pCamera->Zoom];
	    wxz = zoom.X;
	    wyz = zoom.Y;

	    pCamera->Projection = orthographic(
		0, wxz,
		0, wyz,
		0.1, 100.0f);

#if 0
	    if (value < 0)
	    {
		pCamera->Base.Position.X += (wxz/2);
		pCamera->Base.Position.Y -= (wyz/2);
	    }
	    else
	    {
		pCamera->Base.Position.X -= (wxz/2);
		pCamera->Base.Position.Y += (wyz/2);
	    }
#endif

	    pCamera->IsCameraMoved = 1;

	    std_camera_entity_update(pCamera);
	    std_renderer_set_camera(&pCamera->ViewProjection);
	}
    }
}
