#ifndef STRATEGY_ZERO_H
#define STRATEGY_ZERO_H


struct SwlEvent;

void strategy_zero_on_attach();
void strategy_zero_on_destroy();
void strategy_zero_on_update();
void strategy_zero_on_ui();
void strategy_zero_on_event(struct SwlEvent* pEvent);

#endif // STRATEGY_ZERO_H
