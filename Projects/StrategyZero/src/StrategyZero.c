#include "StrategyZero.h"

#include <Core/Types.h>
#include <Core/SimpleWindowLibrary.h>
#include <Core/SimpleStandardLibrary.h>
#include <Application/SimpleApplication.h>

#include <Std/StdGlRenderer.h>
#include <Base/SzLocal.h>
#include <Base/Ui/SzMainMenuUi.h>


void
strategy_zero_on_attach()
{
    SwlWindow* pWindow = simple_application_get_window();

    // todo: place in a better place
    {
	std_gl_renderer_add_font(resource_font("NotoSans.ttf"), 16);
	//std_gl_renderer_add_font(resource_font("NotoSans.ttf"), 32);
    }

    sz_local_init(SzLocal_Russian);

}

void
strategy_zero_on_destroy()
{
}

void
strategy_zero_on_update()
{

}

void
strategy_zero_on_ui()
{
    sz_main_menu_ui();
}

void
strategy_zero_on_event(SwlEvent* pEvent)
{
}
