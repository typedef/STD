#include <stdio.h>
#include <stdlib.h>
#include <Core/Types.h>
#include <Core/SystemInfo.h>
#include <Core/SimpleStandardLibrary.h>
#include <Application/SimpleApplication.h>
#include "StrategyZero.h"
#include <locale.h>


i32
main()
{
    setlocale(LC_ALL, ".UTF8");
    system_info_print();

    SimpleApplicationSettings set = {
	.pName = "StrategyZero",

	.ArgsCount = 0,
	.aArgs = NULL,

	.IsDebug = 1,
	.IsVsync = 1,
	.Size = (v2) { 1600, 1200 },
    };

    simple_application_create(&set);

    SimpleLayer layer = {
	.pName = simple_string_new("StrategyZero"),
	.OnAttach = strategy_zero_on_attach,
	.OnUpdate = strategy_zero_on_update,
	.OnUi = strategy_zero_on_ui,
	.OnEvent = strategy_zero_on_event,
	.OnDestroy = strategy_zero_on_destroy,
    };
    simple_application_add_layer(layer);

    simple_application_run();
    simple_application_destroy();

    return 0;
}
