#ifndef SZ_LOCAL_H
#define SZ_LOCAL_H

#include "SzLocalItem.h"

typedef enum SzLocal
{
    SzLocal_English = 0,
    SzLocal_Russian,
} SzLocal;

void sz_local_init(SzLocal local);
char* sz_local_get_string(SzLocalItem item);

#endif // SZ_LOCAL_H
