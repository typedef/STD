#ifndef SZ_LOCAL_ITEM_H
#define SZ_LOCAL_ITEM_H

typedef enum SzLocalItem
{
    SzLocalItem_MainMenu_NewGame,
    SzLocalItem_MainMenu_Continue,
    SzLocalItem_MainMenu_Settings,
    SzLocalItem_MainMenu_About,
    SzLocalItem_MainMenu_Exit,

    SzLocalItem_Count
} SzLocalItem;

#endif // SZ_LOCAL_ITEM_H
