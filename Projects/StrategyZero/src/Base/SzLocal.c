#include "SzLocal.h"
#include "Base/SzLocalItem.h"

#include <Core/SimpleStandardLibrary.h>


typedef struct SzLocalInternal
{
    SzLocal CurrentLocal;
    char** aItems;
} SzLocalInternal;


static SzLocalInternal gLocal;



void _sz_local_init_en();
void _sz_local_init_rus();

void
sz_local_init(SzLocal local)
{
    gLocal.CurrentLocal = local;
    gLocal.aItems = (char**) memory_allocate(sizeof(char*) * SzLocalItem_Count);

    switch (local)
    {
    case SzLocal_English:
	_sz_local_init_en();
	break;

    case SzLocal_Russian:
	_sz_local_init_rus();
	break;

    default:
#if defined(BUILD_DEBUG)
	vassert_break();
#endif // BUILD_DEBUG
	break;
    }
}

char*
sz_local_get_string(SzLocalItem item)
{
#if defined(BUILD_DEBUG)
    vguard(item >= 0 && item <= SzLocalItem_Count && "Item should be in [0, SzLocalItem_Count) range!");
#endif // BUILD_DEBUG

    return gLocal.aItems[item];
}

void
_sz_local_init_en()
{
    gLocal.aItems[SzLocalItem_MainMenu_NewGame] = "New Game";
    gLocal.aItems[SzLocalItem_MainMenu_Continue] = "Continue";
    gLocal.aItems[SzLocalItem_MainMenu_Settings] = "Settings";
    gLocal.aItems[SzLocalItem_MainMenu_About] = "About";
    gLocal.aItems[SzLocalItem_MainMenu_Exit] = "Exit";
}

void
_sz_local_init_rus()
{
    gLocal.aItems[SzLocalItem_MainMenu_NewGame] = "Новая игра";
    gLocal.aItems[SzLocalItem_MainMenu_Continue] = "Продолжить";
    gLocal.aItems[SzLocalItem_MainMenu_Settings] = "Настройки";
    gLocal.aItems[SzLocalItem_MainMenu_About] = "Об игре";
    gLocal.aItems[SzLocalItem_MainMenu_Exit] = "Выход";
}
