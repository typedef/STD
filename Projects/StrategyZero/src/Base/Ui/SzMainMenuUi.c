#include "SzMainMenuUi.h"

#include <Core/SimpleStandardLibrary.h>
#include <Std/StdUi.h>
#include <Math/SimpleMath.h>


StdUiState main_menu_button(char* pName);

void
sz_main_menu_ui()
{
    std_ui_playout(StdUiPlayout_Center);
    //std_ui_panel_set_size(0.25, 0.35);
    //std_ui_panel_set_offset(-0.1, 0);

    if (std_ui_panel_begin((StdUiPanelSettings) {
		.pUtfLabel = "Не выводится",
		.Flags = StdUiPanelFlags_WithOutHeader | StdUiPanelFlags_Invisible,
	    }))

    {
	StdUiPanel* pCurrentPanel = std_ui_panel_get();

	StdUiDiv div = {
	    // DOCS: In relative metrics/percentage
	    // 0.1 == 10%, 0.5 = 50%
	    //.Position = {0.02, 0.01},
	    // DOCS: рассчитывается с учётом padding,
	    // DivSize = (PanelSize - Padding) * Size
	    .Size = { 1, 1 },
	    // todo: rework margin
	    .Margin = { 0, 0 },
	    //.TextPadding = {5},
	    .PanelPadding = { 25, 30 },
	    .TextAlign = StdUiAlign_Left,
	    .ItemAlign = StdUiAlign_Left,
	    .FontSize = 48,

	    .DivPos = StdUiDivPos_NewLine,
	};

	std_ui_wlayout(div);

	if (main_menu_button("КАМПАНИЯ") == StdUiState_Clicked)
	{
	    GINFO("BTN!\n");
	}

	if (main_menu_button("ОПЦИИ") == StdUiState_Clicked)
	{
	    GINFO("BTN!\n");
	}

	/* if (std_ui_button((StdUiButton) { .pLabel = "СОЗДАТЕЛИ" }) == StdUiState_Clicked) */
	/* { */
	/*     GINFO("BTN!\n"); */
	/* } */
	/* if (std_ui_button((StdUiButton) { .pLabel = "ВЫЙТИ" }) == StdUiState_Clicked) */
	/* { */
	/*     GINFO("BTN!\n"); */
	/* } */

    }

    std_ui_panel_end();


}

StdUiState
main_menu_button(char* pName)
{
    if (helper_tofr())
    {
	real cv = 7/255.0f;
	StdUiCustomStyle itemCellStyle = {
	    .Color        = v4_new(0.23f, 0.65f, 0.34, 1),
	    .HoveredColor = v4_new(0.45, 0.13, 0.13, 1),
	    .ClickedColor = v4_new(0.35, 0.03, 0.03, 1)
	};
	std_ui_custom_style("MainMenuBtn", itemCellStyle);
    }

    StdUiGeometry back = {
	.pName = "MainMenuBtn",
	.GeometryType = StdUiGeometryType_Rect,
	.Position = v3_new(0, 0, -0.2),
    };

    StdUiGeometry text = {
	.pName = pName,
	.GeometryType = StdUiGeometryType_Text,
	.FontSize = 32,
	.Position = v3_new(2, 0, -0.1),
    };

    StdUiCustom inventBtn = {
	.pName = pName,
	.aGeometries = { back, text },
	.GeometriesCount = 2,
	.IsDynamicSize = 1
    };

    StdUiState state = std_ui_custom(inventBtn);
    return state;
}
