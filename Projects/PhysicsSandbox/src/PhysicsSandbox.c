#include "PhysicsSandbox.h"
#include "AssetManager/AssetManager.h"
#include "Graphics/Vulkan/Vsr3d.h"
#include "Graphics/Vulkan/Vsr3dCompositor.h"
#include "UI/Suif.h"
#include <Editor/EditorViewport.h>

#include <InputSystem/KeyCodes.h>

static SimpleWindow* CurrentWindow = NULL;
static CameraComponent gCameraComponent = {};
struct EditorViewport gEditorViewport = {};
static Vsr3dS gVsr3dS = {};
static Vsr3dCompositor gVsrCompositor = {};

void _create_camera_component();
void _camera_updated(CameraComponent* pCamera);


void
physics_sandbox_on_attach()
{
    CurrentWindow = application_get_window();

    //kinematics_register();
    //_create_camera_component();

    Vsr3dSSetting vsr3dsSet = {
	.MaxVertices = I32M(1,000,000),
	.MaxInstances = 200,
	.MaxMaterialsCount = 200,
	.MaxTexture = 200,
    };
    //vsr_3d_compositor_create(&gVsrCompositor, &vsr3dsSet, &gCameraComponent.Base);

}

void
physics_sandbox_on_update(f32 timestep)
{
    //editor_viewport_on_update(&gEditorViewport, timestep);

}

void
physics_sandbox_on_ui_render()
{
}

void
physics_sandbox_on_event(Event* event)
{
    //editor_viewport_on_event(&gEditorViewport, event);

    switch (event->Category)
    {

    case EventCategory_Key:
    {
	if (event->Type != EventType_KeyPressed)
	    break;

	KeyPressedEvent* keyEvent = (KeyPressedEvent*) event;
	if (keyEvent->KeyCode == KeyType_Escape)
	{
	    application_close();
	    event->IsHandled = 1;
	}

	break;
    }

    case EventCategory_Window:
    {
	if (event->Type == EventType_WindowShouldBeClosed)
	{
	    application_close();
	}
	break;
    }

    }
}

void
physics_sandbox_on_destroy()
{
    //vsr_3d_compositor_destroy(&gVsrCompositor);
}

void
_camera_updated(CameraComponent* pCamera)
{
    //vsr_3d_compositor_update_camera_ubo(&gVsrCompositor);
}

void
_create_camera_component()
{
    CameraComponentCreateInfo createInfo = {
	.IsCameraMoved = 1,

	.Settings = {
	    .Type = CameraType_PerspectiveSpectator,
	    .Position = v3_new(-5.0f, 0, 12.0f),
	    .Front = v3_new(0, 0, -1),
	    .Up    = v3_new(0, 1,  0),
	    .Right = v3_new(1, 0,  0),
	    .Yaw = 0,
	    .Pitch = 0,
	    .PrevX = 0,
	    .PrevY = 0,
	    .SensitivityHorizontal = 0.35f,
	    .SensitivityVertical = 0.45f
	},

	.Perspective = {
	    .Near = 0.001f,
	    .Far = 1000.0f,
	    .AspectRatio = CurrentWindow->AspectRatio,
	    // NOTE(typedef): in degree
	    .Fov = 75.0f
	},

	.Spectator = {
	    .Speed = 5.0f,
	    .Zoom = 0
	}
    };

    gCameraComponent =
	camera_component_new_ext(createInfo);
    camera_component_update(&gCameraComponent);

    editor_viewport_create(&gEditorViewport, &gCameraComponent, _camera_updated);
    gEditorViewport.OnUpdate = _camera_updated;

}
