#ifndef PHYSICS_SANDBOX_H
#define PHYSICS_SANDBOX_H

#include <Engine.h>

void physics_sandbox_on_attach();
void physics_sandbox_on_attach_finished();
void physics_sandbox_on_update(f32 timestep);
void physics_sandbox_on_ui_render();
void physics_sandbox_on_event(Event* event);
void physics_sandbox_on_destroy();

#endif // PHYSICS_SANDBOX_H
