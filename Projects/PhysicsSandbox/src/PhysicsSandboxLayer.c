#include "PhysicsSandbox.h"
#include <EntryPoint.h>

void
create_user_application(UserApplicationSettings* pSettings)
{
    ApplicationSettings appSettings = {
	.Width = 2000,
	.Height = 1200,
	.IsInitGraphics = 1,
	.IsFrameRatePrinted = 0,
	.Name = "PhysicsSandbox",
	.ArgumentsCount = pSettings->Argc,
	.Arguments = pSettings->Argv,
    };
    application_create(appSettings);

    Layer physicsSandboxLayer = {0};
    physicsSandboxLayer.Name = "PhysicsSandbox Layer";
    physicsSandboxLayer.OnAttach = physics_sandbox_on_attach;
    physicsSandboxLayer.OnUpdate = physics_sandbox_on_update;
    physicsSandboxLayer.OnUIRender = physics_sandbox_on_ui_render;
    physicsSandboxLayer.OnEvent = physics_sandbox_on_event;
    physicsSandboxLayer.OnDestoy = physics_sandbox_on_destroy;

    application_push_layer(physicsSandboxLayer);
}
