#version 450

#extension GL_EXT_nonuniform_qualifier : require

layout(location = 0) in vec4 o_Color;
layout(location = 1) in vec2 o_Uv;
layout(location = 2) in flat int o_FontIndex;

layout(location = 0) out vec4 FragColor;

layout(std140, set = 0, binding = 1) uniform FontUbo
{
    float Width;
    float EdgeTransition;
} u_FontUbo;

layout(set = 0, binding = 2) uniform sampler2D u_FontAtlas[100];


void main()
{
    vec4 textureColor = texture(u_FontAtlas[nonuniformEXT(o_FontIndex)], o_Uv);

    FragColor = textureColor * o_Color;
}
