#version 450

#extension GL_EXT_nonuniform_qualifier : require

layout(location = 0) in vec4 o_Color;
layout(location = 1) in flat int o_ViewportIndex;

layout(location = 0) out vec4 FragColor;

struct VisibleViewport
{
    vec2 Min;
    vec2 Max;
};

layout(std140, set = 0, binding = 2) uniform ViewportsUbo
{
    VisibleViewport aViewports[10];
} u_Visible;

void
main()
{
    VisibleViewport viewport = u_Visible.aViewports[o_ViewportIndex];

    vec2 min = viewport.Min;
    vec2 max = viewport.Max;
    float fx = gl_FragCoord.x;
    float fy = gl_FragCoord.y;

    bool isInside = (fx >= min.x) && (fx <= max.x) && (fy >= min.y) && (fy <= max.y);

    if (!isInside)
    {
	if (u_Settings.IsScissorsDebug != 1)
	    discard;
	FragColor = vec4(1, 0, 0, 1);
	discard;
    }

    FragColor = o_Color;
}
