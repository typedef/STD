#version 450

#extension GL_EXT_nonuniform_qualifier : require

layout(location = 0) in vec4 o_Color;
layout(location = 1) in vec2 o_Uv;
layout(location = 2) in flat int o_TextureIndex;
layout(location = 3) in flat int o_ViewportIndex;

layout(location = 0) out vec4 FragColor;

struct VisibleViewport
{
    vec2 Min;
    vec2 Max;
};

layout(std140, set = 0, binding = 1) uniform SettingsUbo
{
    int IsScissorsDebug;
} u_Settings;

layout(std140, set = 0, binding = 2) uniform ViewportsUbo
{
    VisibleViewport aViewports[10];
} u_Visible;

layout(set = 0, binding = 3) uniform sampler2D u_FontAtlas;
layout(set = 0, binding = 4) uniform sampler2D u_Textures[100];


void main()
{
    VisibleViewport viewport = u_Visible.aViewports[o_ViewportIndex];

    vec2 min = viewport.Min;
    vec2 max = viewport.Max;
    float fx = gl_FragCoord.x;
    float fy = gl_FragCoord.y;

    bool isInside = (fx >= min.x) && (fx <= max.x) && (fy >= min.y) && (fy <= max.y);

    if (!isInside)
    {
	if (u_Settings.IsScissorsDebug != 1)
	    discard;
	FragColor = vec4(1, 0, 0, 1);
	discard;
    }

    int textureIndex = nonuniformEXT(o_TextureIndex);

    switch (textureIndex) {

    case -1: /* DOCS: Rectangle */
	FragColor = o_Color;
	break;

    case -2: /* DOCS: Font */
	vec4 text = texture(u_FontAtlas, o_Uv);
	FragColor = o_Color * text;
	break;

    default: /* DOCS: Textures/Images */
	vec4 textureColor = texture(u_Textures[textureIndex], o_Uv);
	FragColor = o_Color * textureColor;
	break;

    }

}
