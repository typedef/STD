struct MeshMaterialInfo
{
    // 16 byte
    float MetallicFactor;
    float RoughnessFactor;
    float SpecularFactor;
    float GlossinessFactor;

    // 16 byte
    float ClearcoatFactor;
    float ClearcoatRoughnessFactor;
    float TransmissionFactor;
    float ThicknessFactor;

    // 16 byte
    float AttenuationDistance;
    float SheenRoughnessFactor;
    float EmissiveStrength;
    float IridescenceFactor;

    // 16 byte
    float ThicknessMin;
    float ThicknessMax;
    float CutOff;
    float IridescenceIor;

    // 16 byte
    int BaseColorTexture; // TODO THIS
    int MetallicRoughnessTexture;
    int SpecularTexture;
    int ColorTexture;

    // 16 byte
    int DiffuseTexture;
    int SpecularGlossinessTexture;
    int ClearcoatTexture;
    int ClearcoatRoughnessTexture;

    // 16 byte
    int ClearcoatNormalTexture;
    int TransmissionTexture;
    int VolumeThicknessTexture;
    int SheenColorTexture;

    // 16 byte
    int SheenRoughnessTexture;
    int EmissiveTexture;
    int IridescenceTexture;
    int IridescenceThicknessTexture;

    // 16 byte
    vec4 ColorFactor;

    vec3 SpecularGlossinessFactor;
    int Occlusion;

    vec3 AttenuationColor;
    int Normal;

    vec3 SheenColorFactor;
    int Mode;

    vec3 EmissiveFactor;
    float Ior;

    vec4 BaseColor;
    vec4 DiffuseFactor;
};
