#version 450

#extension GL_EXT_nonuniform_qualifier : require

layout(location = 0) in vec4 o_Color;
layout(location = 1) in vec2 o_Uv;
layout(location = 2) in flat int o_TextureIndex;

layout(location = 0) out vec4 FragColor;

layout(set = 0, binding = 1) uniform sampler2D u_Textures[100];


void main()
{
    vec4 text = texture(u_Textures[nonuniformEXT(o_TextureIndex)], o_Uv);
    FragColor = o_Color * text;
}
