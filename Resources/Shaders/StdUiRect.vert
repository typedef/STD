#version 450

layout(location = 0) in vec3 Position;
layout(location = 1) in vec4 Color;

layout(location = 0) out vec4 o_Color;

layout(std140, set = 0, binding = 0) uniform CameraUbo
{
    mat4 ViewProjection;
} u_CameraUbo;

void main()
{
    gl_Position = u_CameraUbo.ViewProjection * vec4(Position, 1.0);
    o_Color = Color;
}
