#version 450

layout(location = 0) in vec3 Position;
layout(location = 1) in vec4 Color;
layout(location = 2) in vec2 Uv;
layout(location = 3) in int FontIndex;

layout(location = 0) out vec4 o_Color;
layout(location = 1) out vec2 o_Uv;
layout(location = 2) out flat int o_FontIndex;

layout(std140, set = 0, binding = 0) uniform CameraUbo
{
    mat4 ViewProjection;
} u_CameraUbo;

void main()
{
    gl_Position = u_CameraUbo.ViewProjection * vec4(Position, 1.0);
    o_Color = Color;
    o_Uv = Uv;
    o_FontIndex = FontIndex;
}
