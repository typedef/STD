#version 450

#extension GL_EXT_nonuniform_qualifier : require

layout(location = 0) in vec4 o_Color;

layout(location = 0) out vec4 FragColor;

void main()
{
    FragColor = o_Color;
}
