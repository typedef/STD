#ifndef ${ProjectNameConvUpper}_H
#define ${ProjectNameConvUpper}_H


struct SwlEvent;

void ${ProjectNameConvLower}_on_attach();
void ${ProjectNameConvLower}_on_destroy();
void ${ProjectNameConvLower}_on_update();
void ${ProjectNameConvLower}_on_ui();
void ${ProjectNameConvLower}_on_event(struct SwlEvent* pEvent);

#endif // ${ProjectNameConvUpper}_H
