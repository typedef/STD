switch (event->Category)
{

case KeyCategory:
{
    if (event->Type != KeyPressed)
	break;

    KeyPressedEvent* keyEvent = (KeyPressedEvent*) event;
    if (keyEvent->KeyCode == KEY_ESCAPE)
    {
	application_close();
	event->IsHandled = 1;
    }

    break;
}

case  EventCategory_Window:
{
    if (event->Type == WindowShouldBeClosed)
    {
	application_close();
    }
    break;
}

}
