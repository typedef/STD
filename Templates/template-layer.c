#include <stdio.h>
#include <stdlib.h>
#include <Core/Types.h>
#include <Core/SystemInfo.h>
#include <Core/SimpleStandardLibrary.h>
#include <Application/SimpleApplication.h>
#include "${ProjectName}.h"
#include <locale.h>


i32
main()
{
    setlocale(LC_ALL, ".UTF8");
    system_info_print();

    SimpleApplicationSettings set = {
	.pName = "${ProjectName}",

	.ArgsCount = 0,
	.aArgs = NULL,

	.IsDebug = 1,
	.IsVsync = 1,
	.Size = (v2) { 1000, 1000 },
    };

    simple_application_create(&set);

    SimpleLayer layer = {
	.pName = simple_string_new("${ProjectName}"),
	.OnAttach = ${ProjectNameConvLower}_on_attach,
	.OnUpdate = ${ProjectNameConvLower}_on_update,
	.OnUi = ${ProjectNameConvLower}_on_ui,
	.OnEvent = ${ProjectNameConvLower}_on_event,
	.OnDestroy = ${ProjectNameConvLower}_on_destroy,
    };
    simple_application_add_layer(layer);

    simple_application_run();
    simple_application_destroy();

    return 0;
}
