workspace "STD"
architecture "x64"
startproject "%ProjectName%"

configurations
{
  "Debug",
  "Release",
  "Dist"
}

-- Set variable inside /etc/environment
VULKAN_SDK_PATH = "/home/bies/BigData/SDKs/Vulkan/1.3.283.0/x86_64/" --os.getenv("VULKAN_SDK")
print("Vulkan SDK Path: " .. VULKAN_SDK_PATH)
assert(VULKAN_SDK_PATH ~= nil, "Can't get Vulkan SDK!")


outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

group "Dependencies"
 include "Dependencies/MiniAudio"
 include "Dependencies/stb"
 include "Dependencies/Chipmunk2D"
 include "STD"

project "%ProjectName%"
    location "Projects/%ProjectName%"
    kind "ConsoleApp"
    language "C"
    cdialect "C99"
    targetdir ("bin/" .. outputdir .. "/%ProjectName%")
    objdir ("bin/Intermidiates/" .. outputdir .. "/%ProjectName%")

    files
    {
      "Projects/%ProjectName%/src/**.h",
      "Projects/%ProjectName%/src/**.c"
    }

    includedirs
    {
       "STD/src",
       "Projects/%ProjectName%/src/",
       "Dependencies",
       "Dependencies/MiniAudio/src/",
       "Dependencies/stb/src/",
       "Dependencies/Chipmunk2D/src/",
       "STD/src/",
       VULKAN_SDK_PATH .. "/include/vulkan",
       VULKAN_SDK_PATH .. "/include/"
    }

    runpathdirs
    {
       VULKAN_SDK_PATH .. "/lib"
    }

    libdirs
    {
       VULKAN_SDK_PATH .. "/lib"
    }

    -- Comment this when not needed
    -- buildoptions
    -- {
    --    "-fsanitize=address"
    -- }

    links
    {
       "STD",
       -- "asan",

       "MiniAudio",
       "stb",
       "Chipmunk2d",
    }

    filter "configurations:Debug"
      defines { "ENGINE_DEBUG" }
      symbols "On"

    filter "configurations:Release"
      defines { "ENGINE_RELEASE" }
      optimize "Speed"

    filter "system:linux"
    defines {
        "PLATFORM_LINUX",
        "SWL_X11_BACKEND",
        "SVL_X11",
    }
    links
    {
       "X11",
       "xcb",
       "Xrandr",

       "m",
       "pthread",
       "vulkan",
       "GL"

    }

    filter "system:windows"
    defines {
    "PLATFORM_WINDOWS",
    "_CRT_SECURE_NO_WARNINGS",
    "SWL_WIN64_BACKEND",
    "SVL_WIN64",
    }
    links {
        "user32",
        "Shlwapi",
        "gdi32",
        "vulkan-1",
        "opengl32"
    }
