#include "${ProjectName}.h"

#include <Core/Types.h>
#include <Core/SimpleWindowLibrary.h>
#include <Core/SimpleStandardLibrary.h>


void
${ProjectNameConvLower}_on_attach()
{
}

void
${ProjectNameConvLower}_on_destroy()
{
}

void
${ProjectNameConvLower}_on_update()
{

}

void
${ProjectNameConvLower}_on_ui()
{

}

void
${ProjectNameConvLower}_on_event(SwlEvent* pEvent)
{
}
